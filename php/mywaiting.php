<?php 
// ----------------------------------------------------------------------------
// PAGE DE VISION GLOBALE DES SHITSTORMS PUBLIEES PAR L'UTILISATEUR AUTHENTIFIE
// ----------------------------------------------------------------------------

if(!isset($_SESSION['connected']) || !$_SESSION['connected']){
    if(isset($GLOBALS['is_included'])) { include('php/info/403.php'); return; }
    else {header("Location:403"); exit();}
}

global $connexion;

require_once(INC_DIR . 'postDelete.php');

$is_mod = $_SESSION['mod'];
$is_answer = ($GLOBALS['url_params'] === 'answers' ? true : false);

if(isset($_GET['answer']) && isActive($_GET['answer'])){
    $is_answer = true;
}

$count = 0;
$is_previous = false;
if(isset($_GET['count'])){
    $count = intval(mysqli_real_escape_string($connexion, $_GET['count']));
    $count = ($count < COUNT_LIMIT_PER_PAGE ? 0 : $count);
    $is_previous = $count >= COUNT_LIMIT_PER_PAGE;
}

echo "
<div class='row'>
    <div class='col s12'>";
        if(!$is_answer){
            echo "<div class='mysub'>";
            $res = mysqli_query($connexion, "SELECT * FROM Waiting WHERE idUsr={$_SESSION['id']} ".($is_mod ? '' : 'AND locked=0')." LIMIT $count,".(COUNT_LIMIT_PER_PAGE+1).";");
            
            if(mysqli_num_rows($res) == 0){
                echo "<h4>Il n'y a aucune shitstorm.</h4>";
                if($is_previous){
                    generateNextPreviousPersonal("profil/".$_SESSION['username']."/waiting", false, true, $count-COUNT_LIMIT_PER_PAGE, 0);
                }
            }
            else{
                $is_next = mysqli_num_rows($res) > COUNT_LIMIT_PER_PAGE;
            
                if($is_previous || $is_next){
                    generateNextPreviousPersonal("profil/".$_SESSION['username']."/waiting", $is_next, $is_previous, $count-COUNT_LIMIT_PER_PAGE, $count+COUNT_LIMIT_PER_PAGE);
                }
            
                $j = 0;
                while(($row = mysqli_fetch_assoc($res)) && $j != COUNT_LIMIT_PER_PAGE){
                    // Récup du premier lien de la shitstorm (les possibles suivants ne sont pas chargés dans le journal)
                    $linksres = mysqli_query($connexion, "SELECT * FROM WaitingLinks WHERE idSub='{$row['idSub']}' ORDER BY idLink ASC;");
                    if(!$linksres || mysqli_num_rows($linksres) == 0){
                        echo "<h5 class='empty'>Impossible de charger les liens correspondants à la shitstorm. Veuillez recharger la page.</h5>";
                        exit();
                    }
                    $linkrow = mysqli_fetch_assoc($linksres);
                    ?>
                    <div class="row">
                        <div class="col s12 l10 offset-l1">
                            <div class="card card-border" style="padding-top: .5em;">
                                <div class="card-image">
                                    <div class='link center'><?php 
                                        $i = 0;
                                        do{
                                            viewLink($linkrow['link'], $row['idSub']."l".$i, null);
                                            $i++;
                                        }while($linkrow = mysqli_fetch_assoc($linksres));
                                    ?></div>
                                </div>
                                <div class="card-content">
                                    <p style="margin-bottom: 1em;"><?php echo detectTransformLink(htmlentities($row['dscr'])); ?></p>
                                        <?php 
                                            echo "<a href='".HTTPS_URL."modify/{$row['idSub']}?waiting=t'><button style='float: left; margin-top: 0.1em; margin-bottom: 1em;'
                                            name='edit' class='btn teal lighten-1'>éditer</button></a>";
                                        ?>
                                        <div style="clear: both;"></div>
                                    <div class="row card-action">
                                        <p style="float: left;">
                                            <a class='notMargin' target='_blank'href='<?= HTTPS_URL ?>shitstorm/<?= $row['idSub'] ?>'>Shitstorm du <?php echo formatDate($row['dateShit'], 0)."</a>";?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $j++;
                }
                if($is_previous || $is_next){
                    generateNextPreviousPersonal("profil/".$_SESSION['username']."/waiting", $is_next, $is_previous, $count-COUNT_LIMIT_PER_PAGE, $count+COUNT_LIMIT_PER_PAGE);
                }
            }
            
            echo "</div>";
        }
        else{
            echo "<div class='myan'>";
            $res = mysqli_query($connexion, "SELECT a.*, s.title FROM WaitAnsw a JOIN Waiting s ON a.idSub=s.idSub WHERE idUsrA={$_SESSION['id']} ".($is_mod ? '' : 'AND a.locked=0')." LIMIT $count,".(COUNT_LIMIT_PER_PAGE+1).";");

            if(mysqli_num_rows($res) == 0){
                echo "<h4>Il n'y a aucune réponse.</h4>";
                if($is_previous){
                    generateNextPreviousPersonal("profil/".$_SESSION['username']."/waiting/answers", false, true, $count-COUNT_LIMIT_PER_PAGE, 0);
                }
            }
            else{
                $is_next = mysqli_num_rows($res) > COUNT_LIMIT_PER_PAGE;

                if($is_previous || $is_next){
                    generateNextPreviousPersonal("profil/".$_SESSION['username']."/waiting/answers", $is_next, $is_previous, $count-COUNT_LIMIT_PER_PAGE, $count+COUNT_LIMIT_PER_PAGE);
                }

                $j = 0;
                while(($row = mysqli_fetch_assoc($res)) && $j != COUNT_LIMIT_PER_PAGE){
                    ?>
                    <div class="row">
                        <div class="col s12 l10 offset-l1">
                            <div class="card card-border" style="padding-top: .5em;">
                                <div class="card-image">
                                    <div class='link center'><?php 
                                        viewLink($row['linkA'], $row['idSubA']."l", null);
                                    ?></div>
                                </div>
                                <div class="card-content">
                                    <p style="margin-bottom: 1em;"><?php echo detectTransformLink(htmlentities($row['dscrA'])); ?></p>
                                    <?php if($row['fromWaitingShitstorm']){ ?>
                                        <p style='font-weight: bold; margin-bottom: 1em;'>
                                            Cette réponse est liée à la shitstorm en attente <span style='font-style: italic;'><?= htmlspecialchars($row['title']) ?></span>
                                        </p>
                                    <?php } ?>
                                        <?php 
                                            echo "<a href='".HTTPS_URL."modify_an/{$row['idSubA']}?waiting=t".($row['fromWaitingShitstorm'] ? '&waiting_shit=t' : '')."'><button style='float: left; margin-top: 0.1em; margin-bottom: 1em;' name='edit' class='btn teal lighten-1'>éditer</button></a>";
                                        ?>
                                        <div style="clear: both;"></div>
                                    <div class="row card-action">
                                        <p style="float: left;"><a class='notMargin' target='_blank' href='#!'>Réponse du <?php echo formatDate($row['dateAn'], 0)."</a>";?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $j++;
                }
                if($is_previous || $is_next){
                    generateNextPreviousPersonal("profil/".$_SESSION['username']."/waiting/answers", $is_next, $is_previous, $count-COUNT_LIMIT_PER_PAGE, $count+COUNT_LIMIT_PER_PAGE);
                }
            }
            echo "</div>";
        }
        ?>
    </div>
</div>
