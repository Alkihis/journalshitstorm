<?php
// -------------------------------------------------------------------
// PAGE DE GESTION DES PARAMETRES DU SITE (Administrateurs uniquement)
// -------------------------------------------------------------------

if(!isset($_SESSION['mod']) || $_SESSION['mod'] <= 1){
    if(isset($GLOBALS['is_included'])) { 
        include(PAGE403); 
        return; 
    }
    else {
        header("Location:/403"); exit();
    }
}
global $connexion;

$GLOBALS['navbar']->addElement('console_admin', 'Console de gestion');

$set = [];

foreach($GLOBALS['settings'] as $key => $v){
    $set[$key] = $v;
} 

$sure = false;

if(isset($_POST['make_offline'])) {
    $mode = isActive($_POST['make_offline']);

    // Get last set
    $q = mysqli_query($connexion, "SELECT idSet FROM sharedSettings ORDER BY idSet DESC LIMIT 0,1;");
    if($q) {
        $idSet = (int)mysqli_fetch_assoc($q)['idSet'];

        $mode = (int)$mode;
        mysqli_query($connexion, "UPDATE sharedSettings SET allowWebsiteAccess='$mode' WHERE idSet='$idSet';");

        $GLOBALS['settings']['allowWebsiteAccess'] = $mode;
    }
}

else if(isset($_POST['submit'])){
    $set['allowPosting'] = (int)(bool)($_POST['allowPosting'] ?? 0);
    $set['allowUserPosting'] = (int)(bool)($_POST['allowUserPosting'] ?? 0);
    $set['allowUnloggedPosting'] = (int)(bool)($_POST['allowUnloggedPosting'] ?? 0);
    $set['allowUserModification'] = (int)(bool)($_POST['allowUserModification'] ?? 0);
    $set['allowAllModification'] = (int)(bool)($_POST['allowAllModification'] ?? 0);
    $set['allowAnswerPosting'] = (int)(bool)($_POST['allowAnswerPosting'] ?? 0);
    $set['allowUserCreation'] = (int)(bool)($_POST['allowUserCreation'] ?? 0);
    
    $set['allowUserLogin'] = (int)(bool)($_POST['allowUserLogin'] ?? 0);
    $set['allowLogin'] = (int)(bool)($_POST['allowLogin'] ?? 0);
    $set['allowStayLogged'] = (int)(bool)($_POST['allowStayLogged'] ?? 0);

    $sure = true;

    if($set == $GLOBALS['settings']){
        echo "<h5>Aucune modification à effectuer.</h5>";
        $sure = false;
    }
    else if(isset($_POST['sure'])){
        $str_query = "INSERT INTO sharedSettings 
        (allowPosting, allowUserPosting, allowUnloggedPosting, allowUserModification, allowAllModification, allowAnswerPosting, allowUserCreation, allowUserLogin, allowLogin, allowStayLogged)
        VALUES ({$set['allowPosting']}, {$set['allowUserPosting']}, {$set['allowUnloggedPosting']}, {$set['allowUserModification']}, 
        {$set['allowAllModification']}, {$set['allowAnswerPosting']}, {$set['allowUserCreation']}, {$set['allowUserLogin']}, {$set['allowLogin']}, {$set['allowStayLogged']});";

        $res = mysqli_query($connexion, $str_query);
        
        $sure = false;
        if($res){
            echo "<h5 class='pass'>Vos paramètres ont été appliqués.</h5>";
        }
    }
}

?>

<div class="row">
    <div class='col s12 l10 offset-l1'>
        <div class='card-user-new border-user' style='height: auto !important; margin-top: 30px;'>
            <form action='#' method='post'>
                <?= ($sure ? "<p class='flow-text''>Êtes-vous sûr ?<br>Voici les paramètres choisis :</p>": '') ?>
                <p style='margin-top: 2em;'>
                    <input type="checkbox" name='allowPosting' class="filled-in" id="allowPosting" <?= ($set['allowPosting'] ? 'checked' : '') ?>>
                    <label for="allowPosting">Autoriser post de shitstorm/commentaire</label>
                </p>
                <div class='divider' style='margin-top: 1em;'></div>

                <p style='margin-top: 1.2em;'>
                    <input type="checkbox" name='allowUserPosting' class="filled-in" id="allowUserPosting" <?= ($set['allowUserPosting'] ? 'checked' : '') ?>>
                    <label for="allowUserPosting">Autoriser post de shitstorm/commentaire par les utilisateurs</label>
                </p>
                <div class='divider' style='margin-top: 1em;'></div>

                <p style='margin-top: 1.2em;'>
                    <input type="checkbox" name='allowUnloggedPosting' class="filled-in" id="allowUnloggedPosting" <?= ($set['allowUnloggedPosting'] ? 'checked' : '') ?>>
                    <label for="allowUnloggedPosting">Autoriser post de shitstorm par les utilisateurs non connectés</label>
                </p>
                <div class='divider' style='margin-top: 1em;'></div>

                <p style='margin-top: 1.2em;'>
                    <input type="checkbox" name='allowUserModification' class="filled-in" id="allowUserModification" <?= ($set['allowUserModification'] ? 'checked' : '') ?>>
                    <label for="allowUserModification">Autoriser la modification de shitstorm par les utilisateurs</label>
                </p>
                <div class='divider' style='margin-top: 1em;'></div>

                <p style='margin-top: 1.2em;'>
                    <input type="checkbox" name='allowAllModification' class="filled-in" id="allowAllModification" <?= ($set['allowAllModification'] ? 'checked' : '') ?>>
                    <label for="allowAllModification">Autoriser la modification de shitstorm</label>
                </p>
                <div class='divider' style='margin-top: 1em;'></div>

                <p style='margin-top: 1.2em;'>
                    <input type="checkbox" name='allowAnswerPosting' class="filled-in" id="allowAnswerPosting" <?= ($set['allowAnswerPosting'] ? 'checked' : '') ?>>
                    <label for="allowAnswerPosting">Autoriser le post de réponse</label>
                </p>
                <div class='divider' style='margin-top: 1em;'></div>

                <p style='margin-top: 1.2em;'>
                    <input type="checkbox" name='allowUserCreation' class="filled-in" id="allowUserCreation" <?= ($set['allowUserCreation'] ? 'checked' : '') ?>>
                    <label for="allowUserCreation">Autoriser la création de compte</label>
                </p>
                <div class='divider' style='margin-top: 1em;'></div>

                <p style='margin-top: 1.2em;'>
                    <input type="checkbox" name='allowStayLogged' class="filled-in" id="allowStayLogged" <?= ($set['allowStayLogged'] ? 'checked' : '') ?>>
                    <label for="allowStayLogged">Permet d'être connecté (! Aucun utilisateur, même admin, sera déconnecté si décoché, ce paramètre devra être modifié par la BDD !)</label>
                </p>
                <div class='divider' style='margin-top: 1em;'></div>

                <p style='margin-top: 1.2em;'>
                    <input type="checkbox" name='allowUserLogin' class="filled-in" id="allowUserLogin" <?= ($set['allowUserLogin'] ? 'checked' : '') ?>>
                    <label for="allowUserLogin">Autoriser les utilisateurs à se connecter</label>
                </p>
                <div class='divider' style='margin-top: 1em;'></div>

                <p style='margin-top: 1.2em;'>
                    <input type="checkbox" name='allowLogin' class="filled-in" id="allowLogin" <?= ($set['allowLogin'] ? 'checked' : '') ?>>
                    <label for="allowLogin">Autoriser la connexion (! Aucun utilisateur, même admin, pourra se connecter si décoché !)</label>
                </p>

                <button name='submit' class='btn card-user-new border-user attenuation right btn-personal'>
                    Enregistrer
                </button>

                <?= ($sure ? '<input type="hidden" name="sure" value="1">' : '') ?>
                <div class='spacer clearb' style='margin-top: 2em;'></div>
            </form>
        </div>
    </div>

    <div class='spacer clearb' style='margin-top: 1em;'></div>

    <div class='col s12 l10 offset-l1'>
        <form method='post' action='#'>
            <button name='make_offline' type='submit' href="#!" value='<?= (int)!$GLOBALS['settings']['allowWebsiteAccess'] ?>' 
                class='waves-effect waves-light btn col s12 <?= ($GLOBALS['settings']['allowWebsiteAccess'] ? 'red' : 'teal') ?> darken-2 btn-personal-mini'>
                Rendre le site <?= ($GLOBALS['settings']['allowWebsiteAccess'] ? 'hors-ligne' : 'en ligne') ?>
            </button>
            <div class='spacer clearb' style='margin-bottom: 2em;'></div>
        </form>
    </div>
</div>
