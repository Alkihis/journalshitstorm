<?php
// ------------------------------------------------------
// PAGE DE GESTION DU COMPTE DE L'UTILISATEUR AUTHENTIFIE
// ------------------------------------------------------

if(!isset($_SESSION['connected'], $_SESSION['id'], $_SESSION['username']) || !$_SESSION['connected']){
    if(isset($GLOBALS['is_included'])) { include('php/info/403.php'); return; }
    else {header("Location:403"); exit();}
}
global $connexion;

$res = mysqli_query($connexion, "SELECT * FROM Users WHERE id='{$_SESSION['id']}';");
if(!$res){
    if(isset($GLOBALS['is_included'])) { include('php/info/403.php'); return; }
    else {header("Location:403"); exit();}
}
$rowUser = mysqli_fetch_assoc($res);

$note_view = $rowUser['defaultView'];
$per_page = $rowUser['perpage'];

// TRAITEMENT DES FORMULAIRES
// --------------------------

if(isset($_GET['token']) && $_GET['token']){
    if(!$rowUser['confirmedMail']){
        // Calcul si le token est encore valide (24H)
        $dateLast = new DateTime($rowUser['lastCMailSent'] . ' UTC');
        $dateLast = $dateLast->add(new DateInterval('P1D')); // Ajoute 1 jour (Période de 1 Day)
        $dateActual = new DateTime(gmdate('Y-m-d H:i:s e')); // e est la timezone (UTC)

        if($dateLast < $dateActual){ // Si ça fait plus de 1 jour depuis le dernier mail
            echo "<h5 class='error'>Le lien n'est plus valide.</h5><h6>Utilisez le module ci-dessous pour renvoyer un e-mail de confirmation.</h6>";
        }
        else{
            $token_mail = $_GET['token'];
            // Le token constitue la chaîne entre le 10è et le 30è caractères du mot de passe (60 chars)
        
            $real_token = md5(substr($rowUser['passw'], 9, 29-9) . $rowUser['lastCMailSent']);
            if($real_token != $token_mail){
                echo "<h5 class='error'>Impossible de vérifier votre compte.</h5><h6>Avez-vous changé de mot de passe depuis l'envoi de l'e-mail de validation ?</h6>";
            }
            else{
                mysqli_query($connexion, "UPDATE Users SET confirmedMail=1 WHERE id='{$rowUser['id']}';");
                $rowUser['confirmedMail'] = true;
                echo "<h5 class='pass'>L'adresse e-mail a été confirmée.</h5>";
            }
        }
        
    }
    else{
        echo "<h5 class='pass'>Votre adresse e-mail a déjà été confirmée.</h5>";
    }
}

if(isset($_POST['changePSW'])){
    $psw = mysqli_real_escape_string($connexion, $_POST['psw']);
    if($psw == $_POST['reppsw'] && $psw != $_POST['newpsw']){
        if(password_verify($psw, $rowUser['passw'])){
            $newpsw = mysqli_real_escape_string($connexion, $_POST['newpsw']);
            if(!preg_match("/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/", $newpsw)){
                echo "<h5>Le mot de passe ne contient pas les prérequis demandés.</h5>";
            }
            else{
                $hashnew = password_hash($newpsw, PASSWORD_BCRYPT);
                $res3 = mysqli_query($connexion, "UPDATE Users SET passw='$hashnew' WHERE id='{$rowUser['id']}';");
                if($res3){
                    echo "<h5>Vos changements ont été pris en compte.</h5>";
                }
                else{
                    echo "<h5 class='error'>Impossible de valider vos changements.</h5>";
                }
            }
        }
        else{
            echo "<h5 class='error'>Votre ancien mot de passe est invalide.</h5>";
        }
    }
    else{
        echo "<h5 class='error'>Les mots de passe ne correspondent pas ou le nouveau mot de passe est identique au précédent.</h5>";
    }
}
elseif(isset($_POST['deleteAccount'], $_POST['confirmPsw']) && isset($_POST['sure']) && isset($_COOKIE['token'])){
    require_once('inc/postDelete.php');

    $idUsr = $_SESSION['id'];
    if(md5($_COOKIE['token'] . $_SESSION['id']) == $_POST['sure']){
        if(password_verify($_POST['confirmPsw'], $rowUser['passw'])){
            if(deleteUser($idUsr)){
                echo "<h4 class='error'>Votre compte a été supprimé.</h4>";
                disconnect();
                return; exit();
            }
        }
        else{
            echo "<h4 class='error'>Impossible de supprimer votre compte. Le mot de passe est incorrect.</h4>";
        }
    }
    else{
        echo "<h3 class='error'>Une erreur est survenue. Avez-vous été redirigé ici ?</h3>";
        echo "<h5 class='error'>Contactez un administrateur pour plus d'informations. Code : 0x55</h5>";
    }
}
elseif(isset($_POST['resetAccount'], $_POST['confirmPsw']) && isset($_POST['sure']) && isset($_COOKIE['token'])){
    require_once('inc/postDelete.php');

    $idUsr = $_SESSION['id'];
    if(md5($_COOKIE['token'] . $_SESSION['id']) == $_POST['sure']){
        if(password_verify($_POST['confirmPsw'], $rowUser['passw'])){
            $delete = false;
            if(!isset($_POST['anonymyse'])){
                $delete = true;
            }

            if(resetUser($idUsr, $delete)){
                echo "<h4 class='error'>Votre compte a été réinitialisé. Vous pouvez recharger cette page.</h4>";
                return; exit();
            }
        }
        else{
            echo "<h4 class='error'>Impossible de réinitialiser votre compte. Le mot de passe est incorrect.</h4>";
        }
    }
    else{
        echo "<h3 class='error'>Une erreur est survenue. Avez-vous été redirigé ici ?</h3>";
        echo "<h5 class='error'>Contactez un administrateur pour plus d'informations. Code : 0x55</h5>";
    }
}

// Navbar
$GLOBALS['navbar']->addElement('profil/'.$_SESSION['username'], 'Mon profil');
$GLOBALS['navbar']->addElement('compte', 'Paramètres');

// DEBUT AFFICHAGE
// ---------------

?>
<!-- <h2 id='titleAccount'>Compte de <?= $_SESSION['username'] ?></h2> -->

<div class='clearb'></div>
<script src='<?= HTTPS_URL ?>js/account.js'></script>

<!-- Onglets -->
<?php $GLOBALS['nav_tab'] = '<div class="row"><ul class="tabs"><div class="col s12 l8 offset-l2">
            <li class="tab col s4"><a class="active" href="#proprietes">Propriétés</a></li>
            <li class="tab col s4"><a href="#my_sub_tab">Mes soumissions</a></li>
            <li class="tab col s4"><a href="#settings">Réglages visuels</a></li>
        </div></ul></div>'; ?>

<div class="row">
    <div class="col s12">
        <div id='proprietes'>
            <div class="col s12 l8 offset-l2"> <?php
                // Affichage card de validation de mot de passe si non validé
                if(!$rowUser['confirmedMail']){ ?>
                    <div class='card card-border hoverable' style='padding: 12px !important; padding-top: 18px !important;'>
                        <div class='col s12'>
                            <h6>Vous n'avez pas <?= ($rowUser['mail'] != '' ? 'confirmé' : 'configuré') ?> votre adresse e-mail.</h6>
                            <?php if($rowUser['mail'] != '') { ?>
                                <p>Une confirmation est nécessaire si vous souhaitez pouvoir réinitialiser votre mot de passe, ainsi que recevoir un e-mail lorsqu'un de vos abonnements poste une shitstorm.</p>
                                <p><a href='#!' onclick='confirmEmail()'>(R)envoyer l'e-mail de confirmation</a></p>
                            <?php }
                            else { ?>
                                <p>Vous pouvez configurer votre adresse e-mail dans la section "Propriétés de votre compte".</p>
                            <?php } ?>
                        </div>
                        <div class='clearb'></div>
                    </div>
                <?php } ?>

                <!-- Profil du compte -->
                <div class="card card-border" style="padding-top: .5em;">
                    <div style='padding: 1.5em; padding-top: 0;'>
                        <h4 style='padding-bottom: 0.5em;'>Propriétés de votre compte</h4>
                        <!-- Profile image -->
                        <img id='profile_img' class='right image-changer' onclick='changeImg()' width="75" height='75' style='border-radius: 5px;' src="<?= HTTPS_URL.$rowUser['profile_img'] ?>">
                        <p id='textAccount'>Nom d'utilisat<?= ($rowUser['genre'] == 'm' ? 'eur' :
                                                            ($rowUser['genre'] == 'f' ? 'rice' : ($rowUser['genre'] == 'b' ? 'eur.rice' : 'eur'))) 
                                                            ?> : <?= $rowUser['usrname'] ?></p>
                        <p>Statut : <?php if($rowUser['moderator'] == 0){
                                echo "Utilisat".($rowUser['genre'] == 'm' ? 'eur' :
                                ($rowUser['genre'] == 'f' ? 'rice' : ($rowUser['genre'] == 'b' ? 'eur.rice' : 'eur')));
                            }
                            elseif($rowUser['moderator'] == 1){
                                echo "Modérat".($rowUser['genre'] == 'm' ? 'eur' :
                                ($rowUser['genre'] == 'f' ? 'rice' : ($rowUser['genre'] == 'b' ? 'eur.rice' : 'eur')));
                            }
                            elseif($rowUser['moderator'] == 2){
                                echo "Administrat".($rowUser['genre'] == 'm' ? 'eur' :
                                ($rowUser['genre'] == 'f' ? 'rice' : ($rowUser['genre'] == 'b' ? 'eur.rice' : 'eur')));
                            } ?>
                        </p>
                        <p>
                            <a href='#!' onclick='changeBannerImg()'>Changer la bannière du profil</a>
                        </p>
                        <div clas='spacer' style='padding-bottom:0.5em;'></div>

                        <!-- Frame for no redirection -->
                        <iframe name="submitFr" style="display:none;"></iframe>
                        <!-- JS form --> 
                        <form action="about:blank" method="get" target='submitFr'>
                            <div class="input-field">
                                <label for="usrname">Pseudo</label>
                                <input type='text' id='usrname' required maxlength='16' value='<?= htmlspecialchars($rowUser['usrname']) ?>'>
                            </div>
                            <div class="input-field">
                                <label for="realname">Nom d'usage</label>
                                <input type='text' id='realname' required maxlength='32' value='<?= htmlspecialchars($rowUser['realname'], ENT_QUOTES, 'UTF-8') ?>'>
                            </div>
                            <div class='input-field'>
                                <input type="text" id="mail" maxlength="128" pattern='^[a-zA-Z0-9._+-]+@[a-zA-Z0-9._-]{2,}\.[a-zA-Z]{2,6}$' value='<?= $rowUser['mail'] ?>'>
                                <label for="mail">Adresse email</label>
                            </div>
                            <div class='input_field' id='genre_type'>
                            <label for="genre">Pronom</label>
                                <select name='genre' id='gender'>
                                    <option value='male' <?= ($rowUser['genre'] == 'm' ? 'selected' : '') ?>>Il</option>
                                    <option value='female' <?= ($rowUser['genre'] == 'f' ? 'selected' : '') ?>>Elle</option>
                                    <option value='inclusive' <?= ($rowUser['genre'] == 'b' ? 'selected' : '') ?>>Iel</option>
                                    <option value='alternative' <?= ($rowUser['genre'] == 'a' ? 'selected' : '') ?>>Autre</option>
                                </select>
                            </div>
                            <p style='text-align: left;'>
                                Inscrit<?= ($rowUser['genre'] == 'f' ? 'e' : ($rowUser['genre'] == 'b' ? '.e' : '')) ?> le <?= formatDate($rowUser['registerdate'], 0) ?>
                                <?= ($rowUser['confirmedMail'] ? "<br>Adresse e-mail confirmée" : '') ?>
                            </p>
                            
                            <button type="submit" onclick='changeAttributs()' name='changeRN' class="btn right">Appliquer</button>
                            <div class='clearb'></div>
                        </form>
                    </div>
                </div>
                <!-- Mot de passe -->
                <div class="card card-border" style="padding-top: .5em;">
                    <div style='padding: 1.5em; padding-top: 0;'>
                        <h4 style='padding-bottom: 0.5em;'>Modifier le mot de passe</h4>
                        <form action='#' method='post'>
                            <div class="input-field">
                                <label for="psw">Mot de passe actuel</label>
                                <input type="password" id="psw" name="psw" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Doit contenir au moins 8 caractères, un chiffre, une lettre minuscule et majuscule." required>
                            </div>
                            <div class="input-field">
                                <label for="reppsw">Répétez votre mot de passe</label>
                                <input type="password" id="reppsw" name="reppsw" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Doit contenir au moins 8 caractères, un chiffre, une lettre minuscule et majuscule." required>
                            </div>
                            <div class="input-field">
                                <label for="newpsw">Nouveau mot de passe</label>
                                <input type="password" id="newpsw" name="newpsw" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Doit contenir au moins 8 caractères, un chiffre, une lettre minuscule et majuscule." required>
                            </div>
                            <button type="submit" name='changePSW' class="btn red lighten-2 right">Modifier le mot de passe</button>
                            <div class='clearb'></div>
                        </form>
                    </div>
                </div>
                
                <!-- Suppression de compte -->
                <div class="card card-border" style="padding-top: .5em;">
                    <div class='clearb' style='padding: 1.5em; padding-top: 0.8em;'>
                        <a href="#modalresacc" class="waves-effect waves-light modal-trigger btn col s12 red lighten-1">Réinitialiser mon compte</a>
                    </div>
                    <div class='clearb' style='padding: 1.5em; padding-top: 0.8em;'>
                        <a href="#modaldelacc" class="waves-effect waves-light modal-trigger btn col s12 red darken-2">Supprimer mon compte</a>
                    </div>
                    <div class='clearb' style='padding-bottom : 1.5em;'></div>
                </div>
            </div>
        </div>

        <div id='my_sub_tab'>
            <div class="col s12 xl8 offset-xl2">
                <div class="card card-border" style="padding-top: .5em;">
                    <div style='padding: 1.5em; padding-top: 0;'>
                        <a href='<?= HTTPS_URL ?>profil/<?= $_SESSION['username'] ?>/shitstorms'><button style='margin-top: 1em' type="submit" name='mySub' class="btn col s12 l5 but0 blue darken-1">Mes soumissions</button></a>
                        <a href='<?= HTTPS_URL ?>mycomments'><button style='margin-top: 1em' type="submit" name='myComs' class="btn col s12 l5 but1 orange lighten-2">Mes commentaires</button></a>
                    </div>
                    <div class='spacer' style='clear: both; padding-bottom: 1em;'></div>
                </div>
                <div class="card card-border" style="padding-top: .5em;">
                    <div style='padding: 1.5em; padding-top: 0;'>
                        <a href='<?= HTTPS_URL ?>mywaiting'><button style='margin-top: 1em' type="submit" name='mySub' class="btn col s12 l5 but0 blue darken-4">Shitstorms en attente</button></a>
                        <a href='<?= HTTPS_URL ?>mywaiting?answer=t'><button style='margin-top: 1em' type="submit" name='myComs' class="btn col s12 l5 but1 orange darken-1">Réponses en attente</button></a>
                    </div>
                    <div class='spacer' style='clear: both; padding-bottom: 1em;'></div>
                </div>
            </div>
        </div>

        <div id='settings'>
            <div class="col s12 l8 offset-l2">      
                <!-- Formulaire de réglage de la vue par like -->
                <div class='card card-border'>
                    <div style='padding: 1.5em; padding-top: 0.8em; padding-bottom: 0.5em;'>
                        <h4>Réglages de la vue du journal</h4>
                        <h5 id='viewMode' class='col s12'>Vue actuelle : Vue par <?= ($note_view ? 'note' : 'date') ?>.</h5>
                        <span class='col s12 black-text' style='margin-top: 1em;'>Nombre d'éléments par page/chargement sur le journal</span>
                        <div class='card-image' style='padding: 1.5em; padding-top: 0.7em;'>
                            
                                <select id='perpagecount' name='perpagecount' class='col s7'>
                                    <?php
                                        echo "<option value='1'".(1 == $per_page ? ' selected' : '').">1</option>";
                                        echo "<option value='2'".(2 == $per_page ? ' selected' : '').">2</option>";
                                        echo "<option value='3'".(3 == $per_page ? ' selected' : '').">3</option>";
                                        for($i = 5; $i <= 50; $i += 5){
                                            echo "<option value='$i'".($i == $per_page ? ' selected' : '').">$i</option>";
                                        }
                                    ?>
                                </select>
                                <span class='col s1'></span>

                                <div style='padding-top:0.8em;'>
                                    <button onclick='changePerPage()' type='button' id='goPerPage' class='btn col s4 teal lighten-1'>Modifier</button>
                                </div>
                                <div class='clearb' style='margin-bottom: 1em;'></div>
                            
                            <button id='changeVMod' name='changeMod' onclick='changeViewMode(<?= ($note_view ? '0' : '1') ?>)' class='center btn blue waves-effect waves-light next col s12'>
                                Changer pour la vue par <?= ($note_view ? 'date' : 'note') ?><i class='material-icons right'>autorenew</i>
                            </button>

                            <div class='clearb'></div>
                        </div>
                    </div>
                </div>

                <!-- Eclairs et neige -->
                <div class="card card-border" style="padding-top: .5em;">
                    <div style='padding: 1.5em; padding-top: 0.8em;'>
                        <button id='disable_auto_scroll_button' type='button' href="#!" data-value="0" class='waves-effect waves-light btn col s12 darken-2'></button>
                        <div class='spacer clearb' style='margin-bottom: 1.5em;'></div>

                        <?php if($rowUser['storm']){
                            echo "<button id='stormbtn' onclick='changeStormMode(0)' type='button' name='STORM' class='btn col s12 amber darken-3'>Désactiver les éclairs</button>";
                        }
                        else{
                            echo "<button id='stormbtn' onclick='changeStormMode(1)' type='button' name='STORM' class='btn col s12 indigo darken-2'>Activer les éclairs</button>";
                        } ?>
                        <div class='spacer clearb' style='margin-bottom: 1.5em;'></div>

                        <?php if($rowUser['snow']){
                            echo "<button id='snowbtn' onclick='changeSnowMode(0)' type='button' name='SNOW' class='btn col s12 purple lighten-3'>Désactiver la neige</button>";
                        }
                        else{
                            echo "<button id='snowbtn' onclick='changeSnowMode(1)' type='button' name='SNOW' class='btn col s12 blue lighten-3'>Activer la neige</button>";
                        } ?>
                        <div class='spacer clearb' style='margin-bottom: 1.5em;'></div>

                        <button id='followbtn' type='button' href="#modalmodifyfollow" class='waves-effect waves-light modal-trigger btn col s12 <?= ($rowUser['followable'] ? 'grey' : 'green') ?>'>
                            <?= ($rowUser['followable'] ? 'Empêcher les abonnements à mon profil' : 'Me rendre suivable') ?>
                        </button>
                        <div class='clearb'></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        
<div class='row'>
    <div class="col s12 l8 offset-l2">
        <!-- Modal de suppression -->
        <div id="modaldelacc" class="modal bottom-sheet">
            <div class="modal-content">
                <h4>Voulez-vous vraiment supprimer votre compte ?</h4>
            </div>
            <div class="modal-footer" style='padding-left: 1.5em;'>
                <button name='deleteAccount' style='text-align: right;'
                class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 red lighten-1 left white-text" onclick='closeAndOpenModal("modaldelacc", "modaldelaccsure")'>
                    <i class='material-icons left'>clear</i>Oui
                </button>
                <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 blue lighten-1 left white-text" style='margin-left : 2em;'>
                    <i class='material-icons left'>keyboard_capslock</i>Non
                </a>
                <div class='clearb'></div>
            </div>
        </div>

        <!-- Modal de confirmation -->
        <div id="modaldelaccsure" class="modal bottom-sheet">
            <form method='post' action='#'>
                <div class="modal-content">
                    <h4 class='error'>Êtes-vous sûr de vraiment vouloir supprimer votre compte ?</h4>
                    <p>
                        L'opération est irréversible.<br>
                        Toutes vos shitstorms seront anonymisées.<br>
                        Vos commentaires, réponses aux shitstorms, et soumissions encore non approuvées seront définitivement supprimés.<br>
                    </p>
                    <p>Tapez votre mot de passe pour confirmer votre action.</p>
                    <div class='input-field'>
                        <label for='confirmPsw'>Mot de passe</label>
                        <input type='password' id='confirmPsw' name='confirmPsw' required>
                    </div>
                </div>
                <div class="modal-footer" style='padding-left: 1.5em;'>
                    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 blue lighten-1 left white-text">
                        <i class='material-icons left'>keyboard_capslock</i>Non
                    </a>
                    <button type='submit' name='deleteAccount' style='text-align: right; margin-left : 2em;' class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 red lighten-1 left white-text">
                        <i class='material-icons left'>clear</i>Oui
                    </button>
                    <input type='hidden' name='sure' value='<?= (isset($_COOKIE['token']) ? md5($_COOKIE['token'] . $_SESSION['id']) : 0) ?>'>
                    <div class='clearb'></div>
                </div>
            </form>
        </div>

        <!-- Modal de réinitialisation -->
        <div id="modalresacc" class="modal bottom-sheet">
            <div class="modal-content">
                <h4>Voulez-vous vraiment réinitialiser votre compte ?</h4>
            </div>
            <div class="modal-footer" style='padding-left: 1.5em;'>
                <button name='resetAccount' style='text-align: right;'
                class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 red lighten-1 left white-text" onclick='closeAndOpenModal("modalresacc", "modalresaccsure")'>
                    <i class='material-icons left'>clear</i>Oui
                </button>
                <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 blue lighten-1 left white-text" style='margin-left : 2em;'>
                    <i class='material-icons left'>keyboard_capslock</i>Non
                </a>
                <div class='clearb'></div>
            </div>
        </div>

        <!-- Modal de confirmation -->
        <div id="modalresaccsure" class="modal bottom-sheet">
            <form method='post' action='#'>
                <div class="modal-content">
                    <h4 class='error'>Êtes-vous sûr de vraiment vouloir réinitialiser votre compte ?</h4>
                    <p>
                        L'opération est irréversible.<br>
                        Vos commentaires, réponses aux shitstorms, et soumissions encore non approuvées seront définitivement supprimés.<br>
                        Si vous n'anonymisez pas vos shitstorms, elles seront également supprimées.<br>
                        Vos abonnements seront réinitialisés et vos abonnés ne vous suivront plus.<br>
                        L'ensemble de vos notifications seront supprimées.<br>
                        Vous conserverez votre nom d'utilisateur, ainsi que les autres paramètres de votre compte.
                    </p>
                    <p>Tapez votre mot de passe pour confirmer votre action.</p>
                    <p>
                        <input type="checkbox" name="anonymyse" class="filled-in" id="anonymyse" checked="checked">
                        <label for="anonymyse">Anonymiser les shitstorms</label>
                    </p>
                    <div class='input-field'>
                        <label for='confirmPswRenew'>Mot de passe</label>
                        <input type='password' id='confirmPswRenew' name='confirmPsw' required>
                    </div>
                </div>
                <div class="modal-footer" style='padding-left: 1.5em;'>
                    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 blue lighten-1 left white-text">
                        <i class='material-icons left'>keyboard_capslock</i>Non
                    </a>
                    <button type='submit' name='resetAccount' style='text-align: right; margin-left : 2em;' class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 red lighten-1 left white-text">
                        <i class='material-icons left'>clear</i>Oui
                    </button>
                    <input type='hidden' name='sure' value='<?= (isset($_COOKIE['token']) ? md5($_COOKIE['token'] . $_SESSION['id']) : 0) ?>'>
                    <div class='clearb'></div>
                </div>
            </form>
        </div>

        <!-- Modal de modification de follow -->
        <div id="modalmodifyfollow" class="modal bottom-sheet">
            <div class="modal-content">
                <h4 id='follow_status_text'>Voulez-vous <?= ($rowUser['followable'] ? 'désactiver' : 'activer') ?> les abonnements à votre compte ?</h4>
                <p id='follow_status_par' <?= ($rowUser['followable'] ? '' : "style='display: none;'") ?>>Désactiver les abonnements à votre compte désabonnera immédiatement tous les abonnés à votre profil.</p>
            </div>
            <div class="modal-footer" style='padding-left: 1.5em;'>
                <button id='followbtn_confirm_modal' style='text-align: right;'
                class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 red lighten-1 left white-text" onclick='changeFollowVisiblity("<?= ($rowUser['followable'] ? 'hidden' : 'followable') ?>")'>
                    <i class='material-icons left'>clear</i>Oui
                </button>
                <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 blue lighten-1 left white-text" style='margin-left : 2em;'>
                    <i class='material-icons left'>keyboard_capslock</i>Non
                </a>
                <div class='clearb'></div>
            </div>
        </div>
    </div>
</div>

<script src="<?= HTTPS_URL ?>js/croppie.min.js"></script>
<link rel="stylesheet" href="<?= HTTPS_URL ?>css/croppie.css">
