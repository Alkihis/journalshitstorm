<?php

function loadOpenScript(){
    echo "<script>
            $(document).ready(function () { $('#new_answer').slideDown(200); });
        </script>";
}

// --------------------------------------
// PAGE DE DEMANDE D'UNE NOUVELLE REPONSE
// --------------------------------------

echo "<h2 style='padding-bottom: 0.5em;'>Proposer réponse / excuse de l'initiateur</h2>";

if(!$GLOBALS['settings']['allowAnswerPosting'] || !$GLOBALS['settings']['allowUserPosting']){ // On a pas le droit de poster
    if(!$GLOBALS['settings']['allowAnswerPosting']){ // Personne n'a le droit de poster
        echo "<h3 class='error'>Il est actuellement impossible de poster une réponse.</h3>";
        loadOpenScript();
        return;
    }
    else if(!$GLOBALS['settings']['allowUserPosting'] && !isModerator()) { // Les utilisateurs n'ont pas le droit de poster et 'utilisateur n'est pas modérateur
        echo "<h3 class='error'>Il est actuellement impossible de poster une réponse.</h3>";
        loadOpenScript();
        return;
    }
}

global $connexion;
$id = NULL;

// Récupération de l'ID
if(isset($_GET['idPubli'])){
    $id = $idSub = (int)mysqli_real_escape_string($connexion, $_GET['idPubli']);
}
else{
    $id = $idSub = (int)($GLOBALS['options'] ?? 0);
}

if($id <= 0) {
    echo '<h3 class="error">Impossible de valider l\'identifiant de la shitstorm.</h3>';
    return;
}

if(isset($_POST['linkA'], $_POST['dateA_real']) && isset($_POST['dscrA']) && isset($_POST['sendA'], $_SESSION['connected']) && $_SESSION['connected']){
    loadOpenScript();
    $link = mysqli_real_escape_string($connexion, $_POST['linkA']);
    $dscr = mysqli_real_escape_string($connexion, $_POST['dscrA']);
    $date = $_POST['dateA_real'];
    $pass = true;

    $dateApp = gmdate('Y-m-d H:i:s');
    
    if($link != '' && !preg_match(REGEX_TWITTER_LINK, $link) && !preg_match(REGEX_MASTODON_LINK, $link)){
        $pass = false;
        echo "<h5 class='error'>Le lien est invalide.</h5>";
    }
    if(!preg_match(REGEX_DATE, $date)){
        $pass = false;
        echo "<h5 class='error'>La date est invalide.</h5>";
    }
    if(mb_strlen($dscr) > MAX_LEN_ANSWER_DSCR){
        $pass = false;
        echo "<h5 class='error'>La description est trop longue.</h5>";
    }

    $resSub = mysqli_query($connexion, "SELECT idSub, dateShit FROM Shitstorms WHERE idSub='$idSub';");
    if(!$resSub || mysqli_num_rows($resSub) == 0){
        $pass = false;
        echo "<h5 class='error'>La shitstorm n'existe pas.</h5>";
    }
    elseif($pass){
        $rowSub = mysqli_fetch_assoc($resSub);
        $rowSub = $rowSub['dateShit'];
        try{
            $dateT = new DateTime($date);
            $actual = new DateTime($dateApp);
            $dateShitstorm = new DateTime($rowSub);

            if($dateT > $actual || $dateT < $dateShitstorm){
                echo "<h5 class='error'>La date est invalide : trop vieille.</h5>";
                $pass = false;
            }
        } catch (Exception $E){
            echo "<h5 class='error'>La date est invalide.</h5>";
            $pass = false;
        }
    }

    if($pass){
        // Vérification que la SS ne contient pas MAX_ANSWER_COUNT réponses
        $resSub = mysqli_query($connexion, "SELECT idSub FROM Answers WHERE idSub='$idSub';");

        if(isset($_SESSION['connected']) && $_SESSION['connected'] && mysqli_num_rows($resSub) < MAX_ANSWER_COUNT) {
            
            $nom = NULL;

            if($link == ''){
                // Traitement du possible fichier
                $nom = 1;
                if(isset($_FILES['img1']) && $pass && !empty($_FILES['img1']['name'])){
                    $nom = traitementFichier();
                }
                if(!$nom || $nom === 1){
                    echo "<h5 class='error'>Le fichier est invalide.</h5>";
                    $pass = false;
                }
                // Si aucun fichier, $nom vaut 1 (null si fichier pas valide)
            }
            
            if($pass){
                $psu = $_SESSION['username'];
                $idUsr = $_SESSION['id'];

                if($nom != NULL) $link = $nom;

                $res = mysqli_query($connexion, "INSERT INTO WaitAnsw (dscrA, linkA, idUsrA, idSub, dateAn) VALUES ('$dscr', '$link', $idUsr, '$idSub', '$date');");
                if($res){
                    $idWaitAnswer = mysqli_insert_id($connexion);
                    echo "<h5 class='pass'>La soumission a réussi et a été transmise pour modération.</h5>";
                    sendNotif('moderation_answer', $idUsr, 9, $idWaitAnswer);
                }
                else{
                    echo "<h5 class='error'>L'insertion a échoué.</h5>";
                }
            }
            else{
                echo "<h5 class='error'>Erreur inconnue.</h5>";
            }
            
        }
        else{
            echo "<h5 class='error'>Vous n'êtes pas connecté ou la shitstorm possède déjà trois réponses.</h5>";
        }
    }
}

// DEBUT DE L'AFFICHAGE DE LA PAGE
// -------------------------------

if(!isset($_SESSION['connected']) || !$_SESSION['connected']){
    echo "<h3 style='text-align: center; padding: 0.5em; margin-bottom: 2em;'><a href='".HTTPS_URL."login'>Connectez-vous</a> pour proposer une réponse à cette shitstorm.</h3>";
}
else{ ?>
<div class='card-user-new border-user' style='height: auto !important; margin-top: 8px; margin-bottom: 30px;'>
    <form action="#new_answer" method='post' enctype="multipart/form-data">
        <div class="input-field">
            <input type='url' name='linkA' maxlength='2000'>
            <label for="linkA">Lien tweet/toot (1 autorisé, pas d'image si lien)</label>
        </div>
        <div class="file-field input-field">
            <input type='hidden' name='MAX_FILE_SIZE' value='1048576'>
            <div class="btn">
                <span>Image</span>
                <input type="file" name='img1' accept="image/png, image/jpeg">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path validate" type="text" placeholder="1 Mo max, type PNG/JPG">
            </div>
        </div>
        <textarea name="dscrA" rows="5" cols="70" placeholder="Description rapide de la réponse (facultatif)" maxlength="<?= strval(MAX_LEN_ANSWER_DSCR) ?>" class="materialize-textarea"></textarea>

        <p>
            <label for="datepicker">Date de la réponse</label>
            <input class="datepicker" type="text" id="datepicker" name="dateA">
        </p>
        <button type="submit" name='sendA' class="btn btn-personal-mini right blue lighten-1">Proposer une réponse</button>
        <div class='spacer clearb' style='padding-bottom: 7px;'></div>
    </form>
</div>
<?php 
} 
