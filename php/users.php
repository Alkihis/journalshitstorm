<?php
// -------------------------------------------------------------
// PAGE DE GESTION DES UTILISATEURS (Administrateurs uniquement)
// -------------------------------------------------------------

if(!isset($_SESSION['mod']) || $_SESSION['mod'] <= 1){
    if(isset($GLOBALS['is_included'])) { include(PAGE403); return; }
    else {header("Location:403"); exit();}
}
global $connexion;

require_once(ROOT . '/inc/postDelete.php');
require_once(ROOT . '/inc/send_mail.php');

$GLOBALS['navbar']->addElement('usermgmt', 'Utilisateurs');

if($GLOBALS['options'] === 'last_registered') {
    require PAGES_DIR . 'admin/lastRegistered.php';
    return;
}

if($GLOBALS['options'] === 'mod') {
    require PAGES_DIR . 'admin/modify_users.php';
    return;
}

// sendMailShitstorm('alkihis@gmail.com', 41, 'JDG et fantabob974444 DE DROATE§!!!!!!!!!!', 'JDG C VREMENT UNE PUTE JESPZER QUIE C ABONNEESS VON PARTIR§ fanaTA E MORT VIVE LE FANATA', 'Wydrop', 3);
// sendMailReset('fdfefesffssfesfe', 'alkihis@gmail.com');
// sendMailNewComment('alkihis@gmail.com', 3, 'bite', 'bite','bite', 9);
// sendConfirmationMail('alkihis@gmail.com', 'dzdqdz');
// genericMail('alkihis@gmail.com', 'd','d', 'ddd', 'dddd', 'fff');

$token = NULL;

if((isset($_GET['grantM']) || isset($_GET['grantA']) || isset($_GET['degrade']) || isset($_GET['ban'])) && isset($_GET['idUsr']) && isset($_COOKIE['token']) && isset($_GET['sure'])){
    $newS = NULL;
    if(isset($_GET['grantM']))
        $newS = 1;
    elseif(isset($_GET['grantA']))
        $newS = 2;
    elseif(isset($_GET['degrade']))
        $newS = 0;
    $id = mysqli_real_escape_string($connexion, $_GET['idUsr']);

    if(md5($_COOKIE['token'].$id) == $_GET['sure']){
        $res = mysqli_query($connexion, "SELECT * FROM Users WHERE id='$id';");
        if(mysqli_num_rows($res) == 0){
            echo "<h5 class='error'>Une erreur est survenue.</h5>";
        }
        $row = mysqli_fetch_assoc($res);
        if($row['moderator'] > 1){
            echo "<h5 class='error'>Une erreur est survenue : Vous n'avez pas le droit de faire cela.</h5>";
        }
        else{
            if(isset($_GET['ban'])){
                $res = deleteUser($id);
                if($res){
                    echo "<h5 class='pass'>L'opération a réussi.</h5>";
                }
                else{
                    echo "<h5 class='error'>Une erreur est survenue lors du bannissement.</h5>";
                }
            }
            else{
                $res = mysqli_query($connexion, "UPDATE Users SET moderator=$newS WHERE id='$id';");
                if($res){
                    echo "<h5 class='pass'>L'opération a réussi.</h5>";
                }
                else{
                    echo "<h5 class='error'>Une erreur est survenue lors de l'enregistrement.</h5>";
                }
            }
            
        }
    }
}
?>

<div class='row'>
    <div class='col s12'>
        <div class='card-user-new border-user black-text center' 
            style='height: auto !important; margin-bottom: 15px;'>
            <span class='bold'>Vue d'ensemble</span> 
            &bull; <span class='underline'><a href='<?= HTTPS_URL ?>usermgmt/last_registered'>Derniers inscrits</a></span>
            &bull; <span class='underline'><a href='<?= HTTPS_URL ?>usermgmt/mod'>Gérer</a></span>
        </div>
    </div>
</div>

<table class='striped'>
    <thead>
        <tr>
            <th>Nom d'usage</th>
            <th>Pseudo</th>
            <th>Statut</th>
            <th>Mail confirmé</th>
            <th>Visible</th>
            <th>E-mail</th>
            <th>Genre</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    <?php
    $res = mysqli_query($connexion, "SELECT * FROM Users;");
    while($row = mysqli_fetch_assoc($res)){
        $s = ($row['genre'] == 'm' ? 'eur' : ($row['genre'] == 'f' ? 'rice' : ($row['genre'] == 'b' ? 'eur.ice' : 'eur')));
        echo "<tr>";
            echo "<td>{$row['realname']}</td>";
            echo "<td>{$row['usrname']}</td> ";
            echo "<td>";
            if($row['moderator'] == 0){
                echo "Utilisat$s";
            }
            elseif($row['moderator'] == 1){
                echo "Modérat$s";
            }
            elseif($row['moderator'] == 2){
                echo "Administrat$s";
            }
            echo "</td>";
        ?>
            <td><?= ($row['confirmedMail'] ? 'Oui' : ($row['mail'] ? 'Non' : 'Non défini')) ?></td>
            <td><?= ($row['findable'] ? 'Oui' : 'Non') ?></td>
            <td><?= ($row['mail'] ? $row['mail'] : 'Non défini') ?></td>
            <td><?= $row['genre'] ?></td>
            <td class="row">
            <?php 
                if(isset($_COOKIE['token']))
                    $token = md5($_COOKIE['token'] . $row['id']);
                if($row['moderator'] == 0){
                    echo '<button '.($token ? "onclick=\"createModalStatusUser('modalphbottom', {$row['id']}, '$token', 1)\" " : ' ').'
                            class="btn col s5 but0 blue darken-1" type="submit" name="grantM">Soulever</button>';
                    echo '<button '.($token ? "onclick=\"createModalStatusUser('modalphbottom', {$row['id']}, '$token', -1)\" " : ' ').'
                            class="btn col s5 red lighten-1 but1 offset-s1" type="submit" name="ban">Bannir</button>';
                }
                elseif($row['moderator'] == 1){
                    echo '<button '.($token ? "onclick=\"createModalStatusUser('modalphbottom', {$row['id']}, '$token', 2)\" " : ' ').'
                            class="btn col m6 l5 but0 blue darken-1" type="submit" name="grantA">Soulever</button> ';
                    echo '<button '.($token ? "onclick=\"createModalStatusUser('modalphbottom', {$row['id']}, '$token', 0)\" " : ' ').'
                            class="btn col m6 l5 but1 orange lighten-2" type="submit" name="degrade">Rétrograder</button>';
                    echo '<button '.($token ? "onclick=\"createModalStatusUser('modalphbottom', {$row['id']}, '$token', -1)\" " : ' ').'
                            class="btn col s12 red lighten-1" type="submit" name="ban">Bannir</button>';
                }
                
            ?>
            </td>
            
        </tr>
        <?php
    } ?>
    </tbody>
</table>
<?php
