<?php

// Page d'information / d'aide
// ---------------------------

$GLOBALS['navbar']->addElement("help", "Aide");

$GLOBALS['turn_off_container'] = true;

$menu = "<div class='divider'></div>
<p class='flow-text black-text col s12 center'>
    <a href='#post_shitstorm'>Poster une shitstorm</a> - <a href='#post_answer'>Poster une réponse</a> -
    <a href='#acc_profile'>Compte et profil</a> - <a href='#follow'>Abonnements</a>
</p>
<div class='clearb'></div>
<div class='divider'></div>";
?>

<div class='row section'>
    <div class='col s12 l10 offset-l1'>
        <p class='flow-text black-text'>
            Bienvenue sur le Journal des Shitstorms !<br>
            Un endroit où vous êtes libres à tout moment de partager les dramas et shitstorms de Twitter, commenter, poster des réponses et personnaliser
            votre profil à votre image.<br>
            Consultez comment réaliser les actions les plus basiques ici.
        </p>

        <?= $menu ?>
        
        <h3 id='post_shitstorm'>Poster une shitstorm</h3>
        <p class='flow-text black-text'>
            Pour soumettre une nouvelle publication, cliquez sur le bouton ajouter (<span><i class='material-icons'>add</i></span>) dans le menu en haut à droite
            sur PC et dans le menu déroulant sur mobile, ouvrable avec le bouton <span><i class='material-icons'>menu</i></span>.<br>
            Vous n'êtes pas obligé-e d'être connecté-e ou d'avoir un compte sur le Journal des Shitstorms pour partager votre publication.<br>
            Une shitstorm nécessite un titre, un description (peut être courte), la date où elle a éclatée,
            ainsi qu'un minimum d'un lien (Twitter ou Mastodon) ou une image.<br>
            Après soumission, celle-ci sera modérée au plus vite par l'équipe du Journal qui sera libre de réarranger les liens et images soumises,
            en ajouter possiblement de nouveaux-elles, et de modifier la date, le titre ou la description si ceux-ci comportent des fautes ou des imprécisions.
            <br><br>
            Lorsqu'une shitstorm est validée, elle est publiée dans le Journal et les tweets contenus sont sauvegardés dans le cas où l'auteur les supprimerait.
            Si vous avez un compte sur le site, vous recevrez une notification vous signalant la publication de votre shitstorm.
        </p>

        <?= $menu ?>

        <h3 id='post_answer'>Poster une réponse</h3>
        <p class='flow-text black-text'>
            Une shitstorm peut s'envenimer, mais l'initiateur de celle-ci peut également s'excuser, ou à l'inverse en remettre une couche.<br>
            Le Journal vous permet donc de soumettre des réponses à une shitstorm existante. Cependant, <span class='underline'>être connecté est 
            obligatoire pour effectuer cette action</span>. Libre à vous de créer un compte (la procédure est décrite dans la partie suivante) pour publier
            votre réponse.<br>
            Pour accéder au formulaire, rendez-vous sur la page de la shitstorm (cliquez sur son titre depuis le Journal) et cliquez sur le bouton "Proposer
            une réponse" (<span><i class='material-icons'>announcement</i></span>). Une shitstorm peut comporter jusqu'à trois réponses différentes.<br>
            De la même façon que pour créer une shitstorm, publier un réponse demande un seul lien, ou une seule image, additionné à la date de celle-ci.
            Vous pouvez ajouter une description pour préciser les conditions de la réponse, mais celle-ci est facultative.<br>
            Enfin, votre réponse soumise sera modérée selon les mêmes conditions que les shitstorms et sera publiée sous la shitstorm initiale,
            dans l'onglet "réponse". Vous recevrez une notification lorsque votre soumission sera approuvée.
        </p>

        <?= $menu ?>

        <h3 id='acc_profile'>Compte et profil</h3>
        <h5>Créer un compte</h5>
        <p class='flow-text black-text'>
            Interagissez avec le monde en créant votre compte rien qu'à vous sur le Journal ! Pour cela, rendez-vous sur la page Connexion, accessible
            tout en haut à droite de la page, ou depuis le menu déroulant.<br>
            Depuis la page Connexion, vous pouvez soit choisir de créer un nouveau compte simplement en cliquant sur le bouton "Créer un compte", ou
            créer un compte lié directement à votre compte Twitter en cliquant sur "Sign in with Twitter".
            Vous serez invité à rentrer votre nom d'utilisateur et un mot de passe (facultatif en venant de Twitter), puis sur les pages suivantes une
            personnalisation de votre profil sera possible.
        </p>
        <h5>Personnaliser son profil</h5>
        <p class='flow-text black-text'>
            Vous voulez changer votre bannière, changer votre image de profil ?<br>
            Rendez-vous dans les paramètres de votre compte (bouton engrenage <span><i class='material-icons'>settings</i></span> sur le profil),
            puis cliquez sur "Modification du profil" dans le menu de gauche. Pour changer votre bannière ou votre photo de profil, cliquez sur celles-ci,
            puis importez une image depuis votre ordinateur. Si votre compte est associé à votre compte Twitter, vous aurez la possiblité d'importer directement
            votre image de profil Twitter ou votre bannière depuis les liens correspondants.
        </p>
        <h5>Paramètres du compte</h5>
        <p class='flow-text black-text'>
            Accédez aux paramètres de votre compte (bouton engrenage <span><i class='material-icons'>settings</i></span> sur le profil) pour configurer
            votre profil, changer votre nom d'utilisateur, nom d'usage et configurer votre adresse e-mail.
            Depuis la rubrique autres, vous avez la possiblité de lier ou délier votre compte Twitter et supprimer votre compte du Journal des Shitstorms.
        </p>

        <?= $menu ?>

        <h3 id='follow'>Abonnements</h3>
        <p class='flow-text black-text'>
            Vous pouvez vous abonner à un utilisateur en passant la souris sur la cloche depuis son profil. Accédez au profil d'un utilisateur en
            cliquant sur son nom d'utilisateur sur un commentaire ou sous la description d'une shitstorm.
            Lorsque vous êtes abonné à un utilisateur, vous recevrez les notifications à chaque nouvelle shitstorm qu'il poste.<br>
            Vous pouvez également vous abonner à une shitstorm via le même bouton sur la page de shitstorm, à côté des boutons pour consulter le
            nombre de votes laissés. Vous recevrez des notifications à chaque nouvelle réponse ou nouveau commentaire posté sur cette shitstorm.<br>
            Vous pouvez choisir de recevoir ces notifications par e-mail.
        </p>
    </div>
</div>


