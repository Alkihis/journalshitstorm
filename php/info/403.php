<?php header($_SERVER['SERVER_PROTOCOL'] . " 403 Forbidden"); ?>

<h2>Impossible d'accéder à la ressource demandée</h2>
<h3 class='error'>Accès refusé.</h3>

<div class='row'>
    <div class='col s12 l10 offset-l1'>
        <p class='flow-text justify'>
            Votre statut ou votre compte ne vous permet pas d'accéder à la page que vous souhaitez.<br>
            Souhaitez-vous accéder à une page réservée aux modérateurs ou aux administrateurs ?<br>
            Si vous n'êtes pas connecté, et que vous tentez d'accéder à votre compte / réglages, essayez de vous connecter, puis réessayez.<br>
            Cette alerte peut également s'afficher si vous tentez de modifier une shitstorm ou une réponse qui ne vous appartient pas.<br>
        </p>
    </div>
</div>
