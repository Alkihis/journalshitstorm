------------------------
-- GESTION DES IMAGES --
------------------------

    Endpoint : image.json

)) 0 ) Exemple de résultat

    Exemple de requête :
        POST https://www.alkihis.fr/journal/api/image.json
        Body = {img : FILE}
    
    En cas de succès, des informations seront renvoyées.
    Voir le contenu dans la description des paramètres.


))) PARAMETRES )

))) I ) ENREGISTREMENT D'IMAGE )))
))))))))))))))))))))))))))))))))))

)) 1 ) Image
    Image à envoyer sur le serveur.
    Ratio largeur/hauteur > 0.4.
    Taille <= 1 Mo.

    ) => POST{img : FILE}

)) 2 ) Répertoire (facultatif)
    Sous-dossier qui recevra l'image.
    Pour fonctionner dans les shitstorms, le répertoire DOIT être 'uploaded' (ou laissé vide).

    ) => POST{directory : string}

)) => Réponse (si réussite)
    img_name(string) || Nom de l'image enregistrée (md5.extension)
    img_link(string) || Lien symbolique de l'image sur le serveur (img/[directory]/md5.extension)
    size(integer) || Taille de l'image en octets
    width(integer) || Largeur de l'image
    height(integer) || Hauteur de l'image


))) II ) SUPPRESSION D'IMAGE )))
))))))))))))))))))))))))))))))))

)) 1 ) Lien
    Lien de l'image à supprimer (sous la forme img/uploaded/name.ext).

    ) => POST{delete : string}

)) => Réponse (si réussite)
    status(string) || Pass si réussi

)) => Réponse (si échec avec erreur 48)
    error_code(integer) || 48
    error_type(string) || Client
    message(string) || Description de l'échec
    type(string) || answer si l'image à supprimer se trouve dans une réponse, shitstorm si elle se trouve dans une shitstorm
    matches_count(integer) || Nombre d'occurences trouvées
    matches(array){ || Occurences trouvées 
        [0...matches_count-1]{
            id_answer | id_link(integer) || ID de la réponse / ID du lien contenant l'image (id_answer si type = answer, id_link sinon)
            id_shitstorm(integer) || ID de la shitstorm associée au lien / réponse contenant l'image
        }
    } 


))) III ) RECUPERATION DES IMAGES PRESENTES SUR LE SERVEUR )))
))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))

)) 1 ) Demande
    Spécifie que l'on veut obtenir la liste des images.

    ) => POST{get_all_images : 1}

)) 2 ) Répertoire (facultatif)
    Sous-dossier à explorer.
    Pour rechercher les images des shitstorms, le répertoire DOIT être 'uploaded' (ou laissé vide).

    ) => POST{directory : string}

)) => Réponse (si réussite)
    count(integer) || Nombre d'images renvoyées
    images(array){ || Images présentes
        [0...count-1]{
            raw(string) || Nom brut de l'image (avec extension)
            link(string) || Lien symbolique de l'image (au format img/[directory]/raw.ext)
        }
    }

))) IV ) DEMANDE DE PRESENCE D'UNE IMAGE )))
))))))))))))))))))))))))))))))))))))))))))))

)) 1 ) Sur le serveur
    Spécifie que l'on veut savoir si une image est présente sur le serveur.

    Le lien de l'image doit être de la forme img/uploaded/nom.ext ou de la forme nom.ext.

    ) => POST{is_existant_in_server : string}

)) 2 ) Dans une shitstorm
    Spécifie que l'on veut savoir si une image est présente dans une shitstorm.

    Le lien de l'image doit être de la forme img/uploaded/nom.ext ou de la forme nom.ext.

    ) => POST{is_existant_in_shitstorm : string}

)) 3 ) Dans une réponse
    Spécifie que l'on veut savoir si une image est présente dans une réponse.

    Le lien de l'image doit être de la forme img/uploaded/nom.ext ou de la forme nom.ext.

    ) => POST{is_existant_in_answer : string}

)) => Réponse (si réussite)
    present(string) || 'true' si existante, 'false' sinon
    present_int(integer) || 1 si existante, 0 sinon

