------------------------
-- CREATION DE COMPTE --
------------------------

    Endpoint : createacc.json

)) 0 ) Exemple de résultat
    Exemple de requête :
        POST https://www.alkihis.fr/journal/api/createacc.json
            Body = {username : string, password : string, realname : string, mail : string}

    ) =>
        id_user(integer) || ID du nouvel utilisateur
        token(string) || API token du nouvel utilisateur
        status(string) || pass si réussi

))) PARAMETRES OBLIGATOIRES )

)) 1 ) Nom d'utilisateur
    Nom d'utilisateur souhaité.
    Ne doit contenir que des caractères alphanumériques et doit être inférieur ou égal à 16 caractères.

    ) => POST{username : string}

)) 2 ) Mot de passe
    Doit faire au moins 8 caractères, contenir une majuscule et une minuscule, ainsi qu'un chiffre.

    ) => POST{password : string}

)) 3 ) Nom d'usage
    Doit être inférieur ou égal à 32 caractères.

    ) => POST{realname : string}

)) 4 ) Adresse e-mail
    Doit être au format adresse@domaine.ext.

    ) => POST{mail : string}
