<?php

global $connexion;

function putTweetsInTweetSave() {
    global $connexion;

    $res = mysqli_query($connexion, "SELECT * FROM saveTweet");

    while($row = mysqli_fetch_assoc($res)) {
        $content = mysqli_real_escape_string($connexion, $row['content']);
        $infos = mysqli_real_escape_string($connexion, $row['infos']);

        mysqli_query($connexion, "INSERT INTO tweetSave (idTweet, content, infos) VALUES ('{$row['idTweet']}', '$content', '$infos');");

        $insert_id = mysqli_insert_id($connexion);

        if($insert_id) {
            $res2 = mysqli_query($connexion, "SELECT * FROM saveTweetImg WHERE idSave={$row['idSave']};");

            while($row2 = mysqli_fetch_assoc($res2)) {
                $link = mysqli_real_escape_string($connexion, $row2['link']);
                mysqli_query($connexion, "INSERT INTO tweetSaveImg (idSave, link) VALUES ('$insert_id', '$link');");
            }
        }
        else {
            echo "echec";
            exit();
        }
    }
}

function putAnswersInTweetSave() {
    global $connexion;

    // Récupération de tous les ID déjà enregistrés
    $ids = [];

    $res = mysqli_query($connexion, "SELECT DISTINCT idTweet FROM tweetSave");

    while($row = mysqli_fetch_assoc($res)) {
        $ids[] = (int)$row['idTweet'];
    }


    $res = mysqli_query($connexion, "SELECT * FROM saveAnswer");

    while($row = mysqli_fetch_assoc($res)) {
        $content = mysqli_real_escape_string($connexion, $row['content']);
        $infos = mysqli_real_escape_string($connexion, $row['infos']);

        if(!in_array((int)$row['idTweet'], $ids)) {
            mysqli_query($connexion, "INSERT INTO tweetSave (idTweet, content, infos) VALUES ('{$row['idTweet']}', '$content', '$infos');");

            $insert_id = mysqli_insert_id($connexion);
    
            if($insert_id) {
                $res2 = mysqli_query($connexion, "SELECT * FROM saveAnswerImg WHERE idSaveA={$row['idSaveA']};");
    
                while($row2 = mysqli_fetch_assoc($res2)) {
                    $link = mysqli_real_escape_string($connexion, $row2['link']);
                    mysqli_query($connexion, "INSERT INTO tweetSaveImg (idSave, link) VALUES ('$insert_id', '$link');");
                }
            }
            else {
                echo "echec";
                exit();
            }
        }
        else {
            echo "déjà présent";
        }
    }
}

function updateAllLinksForTweetID() {
    global $connexion;

    $res = mysqli_query($connexion, "SELECT * FROM Links;");

    while($row = mysqli_fetch_assoc($res)) {
        $l = (int)getTweetIdFromURL($row['link']);

        if($l) {
            mysqli_query($connexion, "UPDATE Links SET idTweet='$l' WHERE idLink={$row['idLink']};");
        }
    }
    $res = mysqli_query($connexion, "SELECT * FROM Answers;");

    while($row = mysqli_fetch_assoc($res)) {
        $l = (int)getTweetIdFromURL($row['linkA']);

        if($l) {
            mysqli_query($connexion, "UPDATE Answers SET idTweet='$l' WHERE idAn={$row['idAn']};");
        }
    }
}

// putTweetsInTweetSave();
// putAnswersInTweetSave();
// updateAllLinksForTweetID();

echo $_SESSION['visitor_id'] ?? null;
