<?php

try {
    $twitter_link = new OAuth(CONSUMER_KEY, CONSUMER_SECRET, OAUTH_SIG_METHOD_HMACSHA1, OAUTH_AUTH_TYPE_URI);

    $request_tokens = $twitter_link->getRequestToken('https://api.twitter.com/oauth/request_token', 'https://shitstorm.fr/callback_twitter', 'POST');
} catch (OAuthException $e) {
    echo $e->lastResponse;
    return;
}

// Saving tokens
$_SESSION['tokens']['request'] = $request_tokens['oauth_token'];
$_SESSION['tokens']['request_secret'] = $request_tokens['oauth_token_secret'];

$url_to_give_to_user = 'https://api.twitter.com/oauth/authorize?oauth_token=' . $request_tokens['oauth_token'];

echo "<a href='$url_to_give_to_user'>Sign in with Twitter</a>";

