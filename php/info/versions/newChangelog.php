<?php
$datach = 'changelog';

$_GET['version'] = $_GET['version'] ?? $GLOBALS['options'] ?? null;

if(isset($_GET['version'])){
    $v = htmlentities($_GET['version']);
    switch($v){
        case '150':
        case '140':
        case '130':
        case '120':
        case '110':
        case '100':
        case '080':
        case '070':
        case '060':
        case '050':
        case '040':
        case '040':
        case '030':
        case '020':
        case '010':
            $datach = ROOT.'/php/info/versions/'.$v;
            break;
    }
    $GLOBALS['navbar']->addElement('nchangelog', 'Changelog');
    $ver = str_split($_GET['version']);

    if(count($ver) > 1){
        $GLOBALS['navbar']->addElement("nchangelog/".$_GET['version'], "Version {$ver[0]}.{$ver[1]}");
    }
    else{
        $GLOBALS['navbar']->addElement("nchangelog/g", "Général");
    }
}
else{
    $GLOBALS['navbar']->addElement('nchangelog', 'Changelog du Journal des Shitstorms');
}

?>

<div class="row">
    <div class="col s12 m12">
        <div class="card" style="padding-bottom: 3em;">
            <div class="card-content">
                <?php
                if(!isset($v)){
                    ?>
                    <div class="collection">
                        <a href='nchangelog/g' class="collection-item doc active">Changelog général</a>
                        <a href='nchangelog/150' class="collection-item doc">Version 1.5</a>
                        <a href='nchangelog/140' class="collection-item doc">Version 1.4</a>
                        <a href='nchangelog/130' class="collection-item doc">Version 1.3</a>
                        <a href='nchangelog/120' class="collection-item doc">Version 1.2</a>
                        <a href='nchangelog/110' class="collection-item doc">Version 1.1</a>
                        <a href='nchangelog/100' class="collection-item doc">Version 1.0</a>
                        <a href='nchangelog/080' class="collection-item doc">Version 0.8</a>
                        <a href='nchangelog/070' class="collection-item doc">Version 0.7</a>
                        <a href='nchangelog/060' class="collection-item doc">Version 0.6</a>
                        <a href='nchangelog/050' class="collection-item doc">Version 0.5</a>
                        <a href='nchangelog/040' class="collection-item doc">Version 0.4</a>
                        <a href='nchangelog/030' class="collection-item doc">Version 0.3</a>
                        <a href='nchangelog/020' class="collection-item doc">Version 0.2</a>
                        <a href='nchangelog/010' class="collection-item doc">Version 0.1</a>
                    </div>
                    <?php
                }
                else
                    echo '<pre>' . file_get_contents($datach.'.log') . '</pre>'; ?>
            </div>
        </div>
    </div>
</div>