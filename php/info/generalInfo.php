<?php 

$GLOBALS['navbar']->addElement('data_protection', 'Informations sur les données');

if($GLOBALS['options'] === 'query_data'){
    include(PAGES_DIR . 'info/introduction_personal_data.php');
    return;
}

$GLOBALS['turn_off_container'] = true;

?>
<div class='section row'>
    <div class='col s12 l10 offset-l1'>
        <p class='flow-text black-text'>
            La protection de vos données est importante, et nous, équipe modératrice et administratice du Journal des Shitstorms, nous engageons à respecter la confidentialité de
            vos données.<br>
        </p>
        <div class='divider'></div>
        <h4>Widgets Twitter</h4>
        <p class='flow-text black-text'>
            Le Journal des Shitstorms utilise les widgets Twitter (<?= detectTransformLink("https://dev.twitter.com/web/embedded-tweets") ?>) pour afficher les tweets inclus dans les shitstorms.<br>
            Ceci implique l'utilisation des cookies Twitter, ainsi que des scripts automatiquement chargés qui ne sont pas contrôlés par le Journal des Shitstorms.
            Pour plus d'informations concernant ceux-ci, nous vous invitons à lire la politique de confidentialité concernant les cookies de Twitter depuis leur site web.
            Notez qu'une extension comme Privacy Badger (disponible pour Chrome et Firefox) permet de bloquer les cookies qui collectent potentiellement vos habitudes de navigation.
        </p>
        <div class='divider'></div>
        
        <h4>Sécurité des informations</h4>
        <p class='flow-text black-text'>
            Les identifiants (noms d'utilisateur, noms d'usage, adresses e-mail) sont stockées en clair (sans chiffrement), dans une base de données protégée par mot de passe.<br>
            Les mots de passe sont hashés selon l'algorithme BCRYPT avec un sel sûr généré aléatoirement à chaque nouveau mot de passe.
        </p>

        <div class='divider'></div>
        <h4>Collecte et utilisation des données</h4>
        <p class='flow-text black-text'>
            Lorsque vous êtes connecté, la date de votre dernière connexion (qui correspond plus ou moins à la dernière page que vous avez chargé sur le site)
            est enregistrée.<br><br>
            Le Journal se réserve également de stocker une date quand :<br>
            - Vous créez votre compte utilisateur<br>
            - Vous suivez quelqu'un ou une shitstorm<br>
            - Vous faites une action qui déclenche potientiellement une notification<br>
            - Vous notez une shitstorm ou un commentaire<br><br>

            Le Journal ne collecte à aucun moment vos habitudes de navigation, les pages que vous chargez sur le site,
            votre configuration matérielle ou l'appareil utilisé, les recherches que vous effectuez, les shitstorms que vous visionnez,
            le navigateur que vous utilisez, ainsi que toute autre action non décrite précédemment.<br>
            Aucune information n'est volontairement collectée par le Journal des Shitstorms, et aucune donnée, y compris celles entrées lors de la configuration de votre profil utilisateur,
            n'est redistribuée, vendue, ou mise à disposition des autres utilisateurs et de tiers.<br><br>
            Les administrateurs peuvent consulter l'ensemble des données configurées sur votre profil, à savoir votre dernière connexion, votre adresse e-mail, vos paramètres d'affichage de shitstorms,
            si vous êtes suivable ou non, ainsi que toutes les informations publiques. Ces administrateurs s'engagent à ne divulguer aucune information.
        </p>

        <div class='divider'></div>
        <h4>Accès et visibilité des informations</h4>
        <p class='flow-text black-text'>
            Les données suivantes sont accessibles publiquement :<br>
            - Votre date d'inscription<br>
            - La date à laquelle vous suivez un utilisateur<br>
            - Votre pseudo<br>
            - Votre nom d'usage<br>
            - Votre identifiant utilisateur<br>
            - Vos abonnements, ainsi que vos abonnés<br>
            - Votre genre configuré<br>
            - Les éclairs ou la neige<br>
            - Vos favoris laissés sur les commentaires<br>
            - Vos votes positifs ou négatifs laissés sur les shitstorms<br>
            - Votre statut (utilisateur/modérateur/administrateur)<br><br>

            Vous souhaitez télécharger vos données personnelles ?<br>
            <a href='<?= HTTPS_URL ?>data_protection/query_data'>Ce service</a> vous permettra d'obtenir une archive récapitulant
            toutes vos actions sur le Journal des Shitstorms.
        </p>
        <p class='flow-text black-text'>
            Si vous voulez effacer les possibles données personnelles que le Journal des Shitstorms a collecté sur vous,
            il vous suffit de supprimer votre compte, et la suppression sera effective dans la minute. <br>
            Nous vous rappelons que le Journal n'enregistre aucune donnée de ses visiteurs ou de ses membres
            de façon implicite, et que la très grande majorité des informations collectées sont des données entrées par l'utilisateur lui-même.
        </p>

        <div class='divider'></div>
        <h4>Cookies</h4>
        <p class='flow-text black-text'>
            Le Journal des Shitstorms utilise des cookies pour améliorer votre expérience de navigation.<br>
            Si vous êtes connecté, un cookie "token" vous sera alloué et servira à vous identifier sur le site du Journal si votre session expire.<br>
            Dans tous les cas, un cookie "PHPSESSID" sera présent pour conserver des informations relatives à votre session en cours sur le serveur du Journal.
            Si vous n'êtes pas connecté, ce cookie sera lié à une session PHP où aucune information n'est stockée (excepté les paramètres du mode nuit).<br>
            Un cookie relatif à l'utilisation du mode nuit sera également présent ("night_mode"),
            servant à enregister vos paramètres de mode nuit après même l'expiration de votre session, uniquement pour l'ordinateur courant.
        </p>

        <div class='divider'></div>
        <h4 id='mails'>E-mails</h4>
        <div>
            <p class='flow-text black-text'>
                Il se peut que vous receviez des e-mails de la part du Journal des Shitstorms, uniquement lorsque votre adresse est configurée dans un compte
                et confirmée (ou pour recevoir l'e-mail de confirmation).
                Les raisons amenant à recevoir un e-mail sont les suivantes :
            </p>
            
            <p class='flow-text black-text'>
                <span class='underline'>1. Vous avez souscrit un abonnement auprès d'un utilisateur et celui-ci a posté une shitstorm</span><br>
                Lorsque vous vous abonnez à un utilisateur depuis sa page de profil, un e-mail vous sera automatiquement envoyé quand celui-ci poste une shitstorm.<br>
                L'envoi se fait uniquement si votre adresse est confirmée.
            </p>

            <p class='flow-text black-text'>
                <span class='underline'>2. Vous avez souscrit un abonnement à une shitstorm et il y a une nouvelle réponse / un nouveau commentaire sur celle-ci</span><br>
                Lorsque vous vous abonnez à une shitstorm depuis sa page dédiée, un e-mail vous sera automatiquement envoyé quand un utilisateur y ajoute une réponse, 
                ou bien lorsqu'un nouveau commentaire est publié sous celle-ci.<br>
                L'envoi se fait uniquement si votre adresse est confirmée.
            </p>

            <p class='flow-text black-text'>
                <span class='underline'>3. Vous avez demandé à confirmer votre adresse e-mail</span><br>
                Depuis les paramètres de votre compte, il est possible de demander l'envoi d'un e-mail pour confirmer votre adresse.<br>
                La confirmation vous permettra de :
                <ul class='black-text flow-text'>
                    <li>&rsaquo; Réinitialiser votre mot de passe</li>
                    <li>&rsaquo; Recevoir des e-mails lorsqu'un utilisateur que vous suivez postera une shitstorm</li>
                </ul>
            </p>

            <p class='flow-text black-text'>
                <span class='underline'>4. Vous avez demandé une réinitialisation de votre mot de passe</span><br>
                Si vous n'arrivez pas à vous connecter, et que votre adresse e-mail est confirmée, il est possible d'envoyer une demande de réinitialisation de mot de passe.<br>
                La demande arrivera sur l'adresse e-mail configurée sur votre compte.
            </p>
        </div>

        <div class='divider'></div>
        <h4 id='twitter_data'>Connexion avec Twitter</h4>
        <div>
            <p class='flow-text black-text'>
                Le Journal des Shitstorms vous offre l'opportunité de lier votre compte local à votre compte Twitter, pour faciliter la connexion et éviter les
                oublis de mots de passe. Lorsque vous choisissez ce mode de connexion, vous acceptez de partager avec le Journal des Shitstorms votre identifiant utilisateur
                Twitter, votre @ [appelé screen_name], ainsi que des clés d'accès à votre compte spécifiques au Journal des Shitstorms.<br>
                Ces clés d'accès sont configurées, organisées et limitées par Twitter lui-même qui encadre les conditions d'utilisations des données de votre compte par ce site web.<br>
                <br>
                Telle que l'application est configurée, vous consentez à donner l'accès au Journal des Shitstorms une fois la liaison entre votre compte et votre compte Twitter réalisée,
                à n'importe quel moment, à ces informations :
            </p>

            <ul class='black-text flow-text'>
                <li>&rsaquo; Votre "objet utilisateur" contenant notamment votre identifiant, nom, nom d'utilisateur, et toutes les autres informations <span class='underline'>publiques</span>
                de votre profil Twitter (plus d'informations sur l'objet utilisateur <a href='https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/user-object.html' target='_blank'>ici</a>)</li>
                <li>&rsaquo; L'accès en <span class='underline'>lecture uniquement</span> à vos tweets, votre timeline (accueil Twitter) et vos listes</li>
            </ul>

            <p class='flow-text black-text'>
                L'application ne peut <span class='underline'>PAS</span> avoir accès à ces fonctionnalités :
            </p>
            <ul class='black-text flow-text'>
                <li>&rsaquo; Voir votre mot de passe ou votre adresse e-mail</li>
                <li>&rsaquo; Lire vos messages privés ou en envoyer</li>
                <li>&rsaquo; Poster des tweets en votre nom</li>
                <li>&rsaquo; Interagir avec d'autres personnes (favoris, retweets, ajouts dans des listes)</li>
                <li>&rsaquo; Mettre à jour votre profil</li>
            </ul>

            <p class='flow-text black-text'>
                Si votre compte Twitter est en mode "privé" (option "Protéger mon profil et mes tweets" activée), le Journal des Shitstorms sera capable de lire votre profil de la même façon que 
                l'un de vos abonnés.<br>
                À partir du moment où l'accès à votre compte Twitter est délié/révoqué depuis les paramètres du Journal, les clés d'accès et informations concernant votre compte Twitter
                sont <span class='underline'>immédiatement</span> effacées, et vous serez libre de relier un autre compte Twitter ultérieurement.
            </p>
            <p class='flow-text black-text'>
                Notez cependant que si vous avez créé votre compte depuis le bouton Sign in with Twitter, et qu'aucun mot de passe n'a été défini sur votre compte, vous serez incapable de révoquer
                l'accès à votre compte Twitter pour conserver un moyen de connexion à votre compte.
            </p>
        </div>
    </div>
</div>
