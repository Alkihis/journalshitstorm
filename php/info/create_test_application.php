<?php

$token = '';

if(isset($_GET['custom_token']) && is_string($_GET['custom_token'])) {
    $token = $_GET['custom_token'];
    $_SESSION['app_custom_token'] = $_GET['custom_token'];
}

$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => 'https://api.shitstorm.fr/v2/auth/request_token.json',
    CURLOPT_POST => 1,
    CURLOPT_POSTFIELDS => array(
        'app_token' => $token,
        'callback' => 'https://shitstorm.fr/finalize_test_app'
    )
));
// Send the request & save response to $resp
$resp = curl_exec($curl);
// Close request to clear up some resources
curl_close($curl);

if($resp) {
    $json = json_decode($resp, true);

    $request_url = $json['callback_url'];

    echo "<a class='flow-text' href='$request_url'>Cliquez ici pour vous identifier</a>";
}
else {
    echo 'Erreur. Recommencez ultériurement.';
}
