<?php header($_SERVER['SERVER_PROTOCOL'] . " 404 Not Found"); ?>

<h2>Impossible d'accéder à la ressource demandée</h2>
<h3 class='error'>Page non trouvée.</h3>

<div class='row'>
    <div class='col s12 l10 offset-l1'>
        <p class='flow-text justify'>
            La page à laquelle vous tentez d'accéder n'existe pas.<br>
            Si vous pensez que la page demandée devrait être accessible, vous pouvez adresser un e-mail à
            l'administrateur du site (<a href="mailto:journalshitstorm@gmail.com">journalshitstorm@gmail.com</a>).<br>
            Si vous tentez d'accéder à un profil utilisateur via son pseudo, vérifiez que cette personne n'a pas modifié son nom d'utilisateur.<br>
            Si vous souhaitez rechercher une shitstorm ou un utilisateur, utilisez le champ de recherche ci-dessous.
        </p>

        <div class='divider' style='margin: 30px 0;'></div>

        <form id="search_form" method="get" action="search">
            <div class="input-field">
                <a class="prefix" onclick="document.getElementById('search_form').submit();"><i class="material-icons card-pointer black-text" style="font-size: 2rem; padding-top: 0.3em;">search</i></a>
                <input placeholder="Recherche" type="text" id="search-input" name="search" value="">
                <div class="clearb"></div>
            </div>
        </form>
        <span class="flow-text" style="margin-right: 40px;"><a class="card-pointer" onclick="document.getElementById('search_form').submit();">Rechercher</a></span>
    </div>
</div>
