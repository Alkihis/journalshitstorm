<?php

// -------------------------------------------------
// PAGE DE REGLAGES DE DETAILS DU COMPTE UTILISATEUR
// -------------------------------------------------

$GLOBALS['navbar']->addElement('createaccount/configure', 'Configurer');
$GLOBALS['navbar']->addElement('createaccount/configure/complete', 'Terminer');

$dialog = '';

global $connexion;
$res = mysqli_query($connexion, "SELECT access_token_twitter FROM Users WHERE id='{$_SESSION['id']}'");
$tokens = null;
if($res && mysqli_num_rows($res)) {
    $tokens = mysqli_fetch_assoc($res);
}

if(isset($_SESSION['to_configure'], $_SESSION['id'], $_SESSION['username'], $_POST['complete']) && $_SESSION['to_configure']){
    $storm = (isset($_POST['storm']) ? 1 : 0);
    $snow = (isset($_POST['snow']) ? 1 : 0);

    $importPP = (isset($_POST['importPP']) ? 1 : 0);
    $import_banner = (isset($_POST['import_banner']) ? 1 : 0);

    $res = mysqli_query($connexion, "UPDATE Users SET snow='$snow', storm='$storm' WHERE id='{$_SESSION['id']}';");

    if($res){
        $res = mysqli_query($connexion, "SELECT mail, passw FROM Users WHERE id='{$_SESSION['id']}';");
        if($res && mysqli_num_rows($res)){
            $row = mysqli_fetch_assoc($res);

            if($tokens && $tokens['access_token_twitter']) {
                require_once(ROOT . '/inc/postDelete.php');
                require_once(ROOT . '/inc/image/img_manipulator.php');
                $_SESSION['error_msg_creation'] = '';

                if ($importPP) { // Importe la PP depuis Twitter
                    $img = getTwitterProfileImg($tokens['access_token_twitter']);
                    if($img) {
                        $nom = traitementImgBase64($img, 'profile', 1048576, 200, 200, true);
                        if(!$nom){
                            $_SESSION['error_msg_creation'] = "<h5>Impossible d'importer l'image de profil.</h5>";
                        }
                        else{
                            saveImgForUser($nom, $_SESSION['id']);
                        }
                    }
                }
                if ($import_banner) {
                    $img = getTwitterBannerImg($tokens['access_token_twitter']);
                    if($img) {
                        $nom = traitementImgBase64($img, 'banner');
                        if(!$nom){
                            $_SESSION['error_msg_creation'] .= "<h5>Impossible d'importer la bannière du profil.</h5>";
                        }
                        else{
                            saveImgBannerForUser($nom, $_SESSION['id']);
                        }
                    }
                }
            }

            // Envoi du mail de vérification si jamais l'utilisateur a défini un e-mail
            if(isset($_SESSION['send_mail']) && $_SESSION['send_mail'] === true && preg_match(REGEX_MAIL, $row['mail']) && $row['mail'] != ''){
                require_once(ROOT . '/inc/send_mail.php');

                $current_date = gmdate('Y-m-d H:i:s');
                $real_token = md5(substr($row['passw'], 9, 29-9) . $current_date); // Token généré
    
                $se = sendConfirmationMail($row['mail'], $real_token);
                if($se){
                    mysqli_query($connexion, "UPDATE Users SET lastCMailSent='$current_date' WHERE id='{$_SESSION['id']}';");
                    $_SESSION['send_mail'] = -1;
                }
                else{
                    $_SESSION['send_mail'] = false;
                }
            }
        }

        header('Location: '.HTTPS_URL.'profil/'.$_SESSION['username']);
        return;
    }
    else{
        $dialog .= "<h3 class='error'>Une erreur de la base de données est survenue : ".mysqli_error($connexion)."</h3>";
    }
}

if(isset($_SESSION['to_configure']) && $_SESSION['to_configure']){
    $GLOBALS['turn_off_container'] = true;
    ?>
    <div class='background-full complete-image'>
    </div>
    
    <div class="row">
        <div class='col s12 l10 offset-l1' style='margin-top: 30px;'>
            <?= ($dialog ? '<div>' . $dialog . '</div>' : '') ?>
            <div class='col s12 l6'>
                <div class='card-user-new border-user' style='height: auto !important; margin-top: 30px;'>
                    <form action="#" method='post'>
                        
                        <p style='margin-top: 2em;'>
                            <input type="checkbox" name='storm' class="filled-in" id="storm">
                            <label for="storm">Activer les éclairs</label>
                        </p>

                        <div class='divider' style='margin-top: 1em;'></div>

                        <p style='margin-top: 1.2em;'>
                            <input type="checkbox" name='snow' class="filled-in" id="snow">
                            <label for="snow">Activer la neige</label>
                        </p>

                        <div class='divider' style='margin-top: 1em;'></div>

                        <?php if($tokens && $tokens['access_token_twitter']) { ?>
                            <p style='margin-top: 1.2em;'>
                                <input type="checkbox" name='importPP' class="filled-in" id="importPP">
                                <label for="importPP">Importer l'image de profil Twitter</label>
                            </p>
                            <p style='margin-top: 1.2em;'>
                                <input type="checkbox" name='import_banner' class="filled-in" id="import_banner">
                                <label for="import_banner">Importer la bannière Twitter</label>
                            </p>

                            <div class='divider' style='margin-top: 1em;'></div>
                        <?php } ?>

                        <p style='margin-top: 1.2em;'>
                            <input type="checkbox" onclick='toggleDarkMode(true, "night")' name='night' class="filled-in" id="night" <?= $_SESSION['night_mode'] ? 'checked' : '' ?>>
                            <label for="night">Mode nuit</label>
                        </p>
                    
                        <div class='clearb' style='margin-top: 1em;'></div>

                        <button name='complete' class='btn card-user-new border-user attenuation right btn-personal'>
                            Terminer
                        </button>
                    </form>
                    <div class='clearb'></div>
                </div>
            </div>
            <div class='col s12 l6'>
                <p class='flow-text black-text' style='text-align: justify; text-justify: inter-word; padding: 0 25px;'>
                    Dernière petite touche de personnalisation !<br>
                    Envie d'éclairs, envie de neige ? Faites le savoir.<br>
                    Les éclairs seront aux côtés des shitstorms, la neige en arrière plan du site.<br>
                    Le mode nuit est une façon de reposer ses yeux devant les flammes de Twitter. (et qui utilise un thème clair, après tout ?!)<br><br>
                    Tout ce que vous avez configuré depuis le début est de toute manière entièrement modifiable depuis les réglages de votre compte.<br>
                    Vous pourrez y accéder depuis l'engrenage, sur votre profil !
                </p>
            </div>
        </div>
    </div>
    <?php    
}
else{
    ?>
    <h3 class='pass'>Ce compte est déjà configuré.</h3>
    <h4>Utilisez les paramètres du profil pour modifier des options.</h4>
    <?php
}
