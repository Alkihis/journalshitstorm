<?php

$GLOBALS['navbar']->addElement('about', 'À propos');

$_SESSION['pageTitle'] = 'À propos du Journal des Shitstorms';
$GLOBALS['add_meta_description'] = 'Qu\'est-ce que le Journal des Shitstorms ? Comment fonctionne t\'il ? Le Journal est un projet communautaire qui grandit à chacune de vos soumissions.';

$GLOBALS['addMeta'] = '<meta name="twitter:title" content="À propos du Journal des Shitstorms"/>
            <meta name="twitter:description" content="Référence les shitstorms de Twitter depuis janvier 2018."/>
            <meta name="twitter:image" content="' . DEFAULT_CARD_IMG . '"/>';
?>

<h3>Pourquoi le Journal des Shitstorms ?</h3>
<p class='flow-text black-text'>
    L'idée du Journal est tout à fait innocente et fait suite à de nombreux dramas successifs sur Twitter dans la période de début janvier 2018 (voir
    <a target='_blank' href='https://twitter.com/MayakoLyyn/status/949198928179875840'>ici</a>). Immédiatement conquis par l'idée,
    <a target='_blank' href='https://twitter.com/alkihis'>me</a> voilà lancé pour créer un site collaboratif où chacun peut poster (avec ou sans compte)
    la shitstorm du moment qu'il ou elle voit passer dans sa timeline. La processus de modération est imaginé dès le départ pour éviter les fautes d'orthographe,
    ou autres soumissions invalides.<br><br>
    Depuis le lendemain de ce tweet, le 6 janvier 2018, la première version est en ligne, elle permet de poster les premiers dramas et de se créer un compte !<br>
</p>

<div class='divider'></div>
<h3>L'histoire du Journal</h3>
<p class='flow-text black-text'>
    Une phase de développement relativement intense de quelques semaines est lancée après sa création, dans l'optique de coder les fonctionnalités principales :<br><br>
    Le profil utilisateur décrivant les shitstorms publiées ainsi que les commentaires (0.11, 7 janvier), indispensables pour donner son avis,
    suivis du support des réponses aux shitstorms plus les liens multiples dans les shitstorms (0.2, 8 janvier).<br>
    Viennent ensuite les cookies pour la connexion permanente et la page de compte utilisateur (0.21, 9 janvier), puis l'édition de shitstorm avec 
    la gestion du téléversement d'images dans les shitstorms (0.3, 10 janvier).<br>
    Plus tard, la réinitialisation de mot de passe (0.31, 22 janvier), les réponses multiples dans les shitstorms et des améliorations de route (0.32, 29 janvier),
    suivies de la notation des shitstorms et des commentaires (0.34, 4 février) complètent solidement la base du site.<br><br>
    Progressivement, d'autres fonctionnalités non prévues initialement arrivent :<br>
    Journal affiché par note (0.4, 5 février), une API entière (0.42, 12 février), modification des réponses, suppression de compte
    et titre pour les shitstorms (0.5, 1er mars), images de profil, recherche d'utilisateur et site dynamique (0.6, 23 mars), et enfin le mode nuit (0.65, 29 mars).<br>
    <br>
    Enfin, le Journal a connu de nombreuses modifications esthétiques successives (0.6, 0.8 puis 1.1/1.2) pour connaître le design actuel.<br>
    Est arrivé finalement la sauvegarde des tweets dans les shitstorms (0.7, 2 avril) et dans les réponses (0.71), les notifications, l'abonnement aux utilisateurs
    et la modification de shitstorm pré-modération (0.8, 20 avril), les notifications par e-mail (0.81/0.9, 26 avril) et des tas de fonctionnalités 
    plus diverses les unes que les autres dans les versions finales.
</p>

<div class='divider'></div>
<h3>Comment est fait le Journal ?</h3>
<p class='flow-text black-text'>
    Le Journal des Shitstorms est quasi-intégralement développé en PHP côté serveur (sans framework), la partie route est opérée par un serveur Apache.
    Il est aidé par le framework CSS Materialize (version 0.100), possède de nombreuses parties dynamiques en JavaScript reliées à son API,
    et exploite des librairies telles que Hammer.js, fancyBox, Croppie, particles.js, et surtout jQuery.<br>
    La partie stockage est gérée par une simple base MariaDB, le tout faisant son bonhomme de chemin sur un VPS Debian 9.<br>
</p>
<div style='padding-bottom: 30px;'></div>
