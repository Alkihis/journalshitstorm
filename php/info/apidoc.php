<div class="row">
    <h2>Documentation du journal des shitstorms</h2>
    <div class="col s12 m12">
        <div class="card" style="padding-bottom: 3em;">
            <div class="card-content">
                <p>Ce site est développé en PHPCore, JavaScript+jQuery, à l'aide du framework CSS <a target='_blank' href='http://materializecss.com'>Materialize</a>.</p><br>
                <div class="collection with-header">
                    <div class="collection-header titleBox">API du journal des shitstorms</div>
                    <div class="collection-item active">Recherche</div>
                    <a href='searchapi' class="collection-item doc">Recherche de shitstorm</a>
                    <a href='searchcom' class="collection-item doc">Recherche de commentaire</a>
                    <a href='searchansw' class="collection-item doc">Recherche de réponse</a>

                    <div class="collection-item active">Soumission</div>
                    <a href='docposts' class="collection-item doc">Soumission de shitstorm</a>
                    <a href='docposta' class="collection-item doc">Soumission/suppression de réponse</a>
                    <a href='docpostc' class="collection-item doc">Soumission/suppression de commentaire</a>

                    <div class="collection-item active">Utilisateur</div>
                    <a href='docprofil' class="collection-item doc">Profil d'un utilisateur</a>
                    <a href='docacc' class="collection-item doc">Gestion du compte utilisateur</a>

                    <div class="collection-item active">Interaction et modification de shitstorm</div>
                    <a href='docnote' class="collection-item doc">Notation des shitstorms et mise en favori des commentaires</a>
                    <a href='docmod' class="collection-item doc">Modification/suppression de shitstorm</a>
                    <a href='docmodansw' class="collection-item doc">Modification/suppression de réponse</a>
                    <a href='docmoderation' class="collection-item doc">Modération de shitstorms et réponses en attente</a>

                    <div class="collection-item active">Autres</div>
                    <a href='docauth' class="collection-item doc">Authentification et connexion</a>
                    <a href='docimg' class="collection-item doc">Gestion des images sur le serveur</a>
                    <a href='docerror' class="collection-item doc">Gestion des erreurs et catalogue des erreurs</a>
                </div>

                <div class="collection with-header">
                    <div class="collection-header titleBox">Historique des versions du journal</div>
                    <a href='nchangelog' class="collection-item doc">Accès changelog</a>
                </div>
            </div>
        </div>
    </div>
</div>
