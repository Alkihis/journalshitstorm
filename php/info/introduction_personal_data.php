<?php

$GLOBALS['navbar']->addElement('data_protection/query_data', 'Données personnelles');

if($GLOBALS['additionnals'] === 'get'){
    include(PAGES_DIR . 'info/get_personal_data.php');
    return;
}

$GLOBALS['turn_off_container'] = true;
?>

<div class='row'>
    <div class='col s12 l10 offset-l1'>
        <p class='flow-text justify' style='margin: 40px 0;'>
            Vous tenez à vos données, et nous le comprenons.<br>
            Nous avons mis à votre disposition un service qui permet de rassembler toutes les données qui vous concernent sur le Journal des Shitstorms,
            puis vous permet de les télécharger.<br>
            Dans cette archive, où il suffira d'ouvrir l'index dans votre navigateur web favori, vous trouvez les shitstorms que vous avez posté,
            les réponses, commentaires, favoris, notes distribuées sur le site au fur et à mesure de votre navigation.<br>
            Vous aurez également un récapitulatif complet de votre profil, incluant des données cachées (ID divers, dernière connexion, dernier mail de confirmation
            envoyé, si vous êtes recherchable...).<br><br>

            Un résumé des notifications non supprimées sera également mis à disposition (les notifications que vous avez masqué ont déjà été définitivement supprimées du serveur).<br>
            Enfin, un récapitulatif de vos abonnés ainsi que vos abonnements sera présent.<br><br>

            Pour rappel, un récapitulatif de comment, pourquoi, et où les données sont sauvegardées et utilisées sur le Journal des Shitstorms
            est disponible à la <a href='<?= HTTPS_URL ?>data_protection'>page précédente</a>.<br><br>

            <?php if(isLogged()) { ?>
                Le lien de téléchargement est disponible à <a href='<?= HTTPS_URL ?>data_protection/query_data/get'>cette adresse</a>.<br>
                Vous ne pouvez demander qu'un seul lien par période de 24 heures, et vous avez deux jours pour télécharger votre fichier à compter de la génération du lien.
                Vous ne pouvez pas effectuer plus d'un téléchargement toutes les deux heures.
            <?php } else { ?>
                <span class='bold'>Vous n'êtes pas connecté.</span><br>
                Pour télécharger les données de votre compte, connectez-vous.
            <?php } ?>
        </p>
    </div>
</div>
