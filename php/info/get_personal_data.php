<?php

function constructProfile(int $idUsr, $ext_banner, $ext_profile){
    global $connexion;

    $res = mysqli_query($connexion, "SELECT * FROM Users WHERE id=$idUsr;");

    $s = '<h1>Profil</h1>';
    if($res && mysqli_num_rows($res)){
        $rowUser = mysqli_fetch_assoc($res);

        $s .= "<h3>Profil de {$rowUser['usrname']}</h3>";

        $s .= "<h4>Bannière</h4>";
        if($ext_banner){
            $s .= "<img style='float: left; width: 60%; height: auto !important;' alt='Bannière du profil' src='img/banner_img$ext_banner'>";
        }
        else{
            $s .= "<h6>Aucune.</h6>";
        }

        $s .= "<div style='clear: both;'></div>";
        $s .= "<h4>Photo de profil</h4>";
        if($ext_profile){
            $s .= "<img width=100 height=100 style='float: left;' alt='Image de profil' src='img/profile_img$ext_profile'>";
        }
        else{
            $s .= "<h6>Aucune.</h6>";
        }

        $token_str = '';
        if($rowUser['access_token_twitter']) {
            $t = json_decode($rowUser['access_token_twitter']);
            $token_str = "<br>Ce compte est capable de s'identifier en utilisant les informations suivantes :<br>
            Nom d'utilisateur Twitter : @{$t->screen_name}<br>
            ID utilisateur Twitter : {$t->user_id}<br>
            -- ATTENTION : Les données suivantes ne sont à communiquer à personne : --<br>
            Token d'accès : {$t->oauth_token}<br>
            Token d'accès (clé privée) : {$t->oauth_token_secret}<br>
            -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --";
        }

        $s .= "<div style='clear: both;'></div>";
        $s .= "<h4>Informations</h4><p>
            Nom d'utilisateur : {$rowUser['usrname']}<br>
            Nom d'usage : {$rowUser['realname']}<br>
            Adresse e-mail : {$rowUser['mail']}<br>
            Statut : {$rowUser['moderator']} (0 : utilisateur / 1 : modérateur / 2 : administrateur)<br>
            ID utilisateur : {$rowUser['id']}<br>
            Genre : {$rowUser['genre']} (m : masculin / f : féminin / b : inclusif / a : autre)<br>
            Dernière connexion : {$rowUser['lastlogin']} UTC<br>
            Date d'inscription : {$rowUser['registerdate']} UTC<br>
            Dernier e-mail de confirmation d'adresse envoyé : {$rowUser['lastCMailSent']} UTC<br>
            Eclairs activés : {$rowUser['storm']} (0 : non / 1 : oui)<br>
            Neige activée : {$rowUser['snow']} (0 : non / 1 : oui)<br>
            Recherchable : {$rowUser['findable']} (0 : non / 1 : oui)<br>
            Suivable : {$rowUser['followable']} (0 : non / 1 : oui)<br>
            Adresse e-mail confirmée : {$rowUser['confirmedMail']} (0 : non / 1 : oui)<br>
            Dernière génération de données utilisateur : {$rowUser['lastDataGeneration']} UTC<br>
            $token_str
        </p>";

        $s .= "<h4>Notifications non supprimées</h4>";

        $resNotif = mysqli_query($connexion, "SELECT * FROM Notifications WHERE idDest=$idUsr;");
        if($resNotif && mysqli_num_rows($resNotif)){
            $s .= "<p>";

            while($rowNotif = mysqli_fetch_assoc($resNotif)){
                $s .= "Notification {$rowNotif['idNotif']}<br>";
                $s .= "Emetteur (ID) : {$rowNotif['idUsr']}<br>
                Elément en relation avec la notification (ID) : {$rowNotif['idRelated']}<br>
                Type de notification : {$rowNotif['type']}<br>
                Date : {$rowNotif['date']} UTC<br>
                Lue : {$rowNotif['viewed']} (0 : non / 1 : oui)<br><br>";
            }

            $s .= "</p>";
        }
        else{
            $s .= "<p>Aucune notification.</p>";
        }

        $s .= "<h4>Abonnés</h4>";

        $resNotif = mysqli_query($connexion, "SELECT f.*, u.usrname, u.realname FROM Followings f JOIN Users u ON f.idUsr=u.id WHERE idFollowed=$idUsr;");
        if($resNotif && mysqli_num_rows($resNotif)){
            $s .= "<p>";

            while($rowNotif = mysqli_fetch_assoc($resNotif)){
                $s .= "Follow {$rowNotif['idFollow']}<br>";
                $s .= "Abonné (ID) : {$rowNotif['idUsr']}<br>
                Abonné (nom d'utilisateur) : {$rowNotif['usrname']}<br>
                Abonné (nom d'usage) : {$rowNotif['realname']}<br>
                Depuis le : {$rowNotif['followDate']} UTC<br><br>";
            }

            $s .= "</p>";
        }
        else{
            $s .= "<p>Aucun abonné.</p>";
        }

        $s .= "<h4>Abonnements</h4>";

        $resNotif = mysqli_query($connexion, "SELECT f.*, u.usrname, u.realname FROM Followings f JOIN Users u ON f.idFollowed=u.id WHERE idUsr=$idUsr;");
        if($resNotif && mysqli_num_rows($resNotif)){
            $s .= "<p>";

            while($rowNotif = mysqli_fetch_assoc($resNotif)){
                $s .= "Follow {$rowNotif['idFollow']}<br>";
                $s .= "Abonnement (ID) : {$rowNotif['idFollowed']}<br>
                Abonnement (nom d'utilisateur) : {$rowNotif['usrname']}<br>
                Abonnement (nom d'usage) : {$rowNotif['realname']}<br>
                Abonnement e-mail : {$rowNotif['acceptMail']} (0 : non / 1 : oui)<br>
                Depuis le : {$rowNotif['followDate']} UTC<br><br>";
            }

            $s .= "</p>";
        }
        else{
            $s .= "<p>Aucun abonnement.</p>";
        }

        $s .= "<h4>Shitstorms auquelles vous êtes abonné</h4>";

        $resNotif = mysqli_query($connexion, "SELECT f.*, u.title FROM ShitFollowings f JOIN Shitstorms u ON f.idFollowed=u.idSub WHERE idFollower=$idUsr;");
        if($resNotif && mysqli_num_rows($resNotif)){
            $s .= "<p>";

            while($rowNotif = mysqli_fetch_assoc($resNotif)){
                $s .= "Follow {$rowNotif['idSFollow']}<br>";
                $s .= "ID Shitstorm : {$rowNotif['idFollowed']}<br>
                Titre de la shitstorm : <a target='_blank' href='".HTTPS_URL."shitstorm/{$rowNotif['idFollowed']}'>{$rowNotif['title']}</a><br>
                Abonnement e-mail : {$rowNotif['withMail']} (0 : non / 1 : oui)<br>
                Date : {$rowNotif['followDate']} UTC<br><br>";
            }

            $s .= "</p>";
        }
        else{
            $s .= "<p>Aucun abonnement.</p>";
        }   
    }

    return $s;
}

function constructShitstorms(int $idUsr){
    global $connexion;

    $res = mysqli_query($connexion, "SELECT * FROM Shitstorms WHERE idUsr=$idUsr;");

    $s = '';

    $s .= "<h1>Shitstorms</h1>";
    if($res && mysqli_num_rows($res)){
        $begin = true;
        while($row = mysqli_fetch_assoc($res)){
            if(!$begin){
                $s .= '<hr>';
            }
            else{
                $begin = false;
            }

            // Infos de la shitstorm
            $s .= "<div class='shitstorm-title'>".htmlspecialchars($row['title'])."</div><br>";

            $s .= "<h4>Informations</h4><p>
                Titre de la shitstorm : ".htmlspecialchars($row['title'])."<br>
                Description : ".preg_replace('/\n/', '<br>', htmlspecialchars($row['dscr']))."<br>
                Note : {$row['nbLikes']}<br>
                Date de la shitstorm : {$row['dateShit']} UTC<br>
                Date de post : {$row['approvalDate']} UTC<br>
                ID : {$row['idSub']}<br>
                ID modérateur l'ayant approuvé : {$row['idApprovator']}<br>
                Cachée : {$row['hidden']} (0 : non / 1 : oui)<br>";

            // Notes de la shitstorm
            $rowLink = mysqli_query($connexion, "SELECT s.*, u.usrname FROM ShitLikes s LEFT JOIN Users u ON s.idUsr=u.id WHERE idSub={$row['idSub']} ORDER BY isLike DESC;");

            if($rowLink && mysqli_num_rows($rowLink)){
                $s .= "Utilisateurs ayant noté la shitstorm [ID | username | like (0 : dislike / 1 : like)] : <br>=> ";
                while($link = mysqli_fetch_assoc($rowLink)){
                    $s .= "[{$link['idUsr']} | " . ($link['usrname'] ? $link['usrname'] : 'null') . " | {$link['isLike']}] ";
                }
            }
            $s .= "</p>";

            // Liens de la shitstorm
            $rowLink = mysqli_query($connexion, "SELECT * FROM Links WHERE idSub={$row['idSub']};");

            $s .= "<h4>Liens</h4><div>";
            if($rowLink && mysqli_num_rows($rowLink)){
                while($link = mysqli_fetch_assoc($rowLink)){
                    if(preg_match(REGEX_IMG_LINK_UPLOADED, $link['link'])) {
                        $s .= "<div class='image-card'><img class='shitstorm-image' alt='Image de shitstorm' src='{$link['link']}'></div>";
                    }
                    else { // Normalement c'est un lien de tweet
                        $s .= "<div class='shit-link'><p><a target='_blank' href='{$link['link']}'>{$link['link']}</a></p>";
                        $id = getTweetIdByLink($link['link']);

                        if($id) {
                            $tweet = getTweetDetailsFromSave($id, false, true, true);

                            if($tweet) {
                                $s .= "<p>Tweet posté par {$tweet['name']}
                                (<a href='https://twitter.com/{$tweet['screen_name']}'>@{$tweet['screen_name']}</a>)</p>";

                                $s .= "<p>{$tweet['full_text']}</p>";

                                if($tweet['media_count']) {
                                    $s .= "<h6>Médias</h6>";
                                    $with_centered_img = (bool)($tweet['media_count'] % 2);
                                    $remaining = $tweet['media_count'];
                                    foreach($tweet['medias'] as $m) {
                                        if($remaining > 1) {
                                            $s .= "<div class='media-block'><img alt='Image de tweet' src='{$m['link']}' class='tweet-media'></div>";
                                        }
                                        else {
                                            $s .= "<div class='media-block ".($with_centered_img ? 'unique-media' : '')."'>
                                                    <img alt='Image de tweet' src='{$m['link']}' class='tweet-media'>
                                                </div>";
                                        }
                                        $remaining--;
                                    }
                                    $s .= '<div class="clearb"></div>';
                                }
                            }
                            else {
                                $s .= "<p>Ce tweet n'a aucune sauvegarde enregistrée.</p>";
                            }
                        }
                        $s .= '</div>';
                    }
                }
            }
            $s .= "</div>";
        }
    }
    else{
        $s .= "<h3>Vous n'avez publié aucune shitstorm.</h3>";
    }

    return $s;
}

function constructShitLikes(int $idUsr){
    global $connexion;

    $res = mysqli_query($connexion, "SELECT c.*, s.title, s.idUsr as posteur FROM ShitLikes c LEFT JOIN Shitstorms s ON c.idSub=s.idSub WHERE c.idUsr=$idUsr;");

    $s = '';

    $s .= "<h1>Notes attribuées aux shitstorms</h1>";
    if($res && mysqli_num_rows($res)){
        $begin = true;
        while($row = mysqli_fetch_assoc($res)){
            // Infos des favs
            if(!$begin){
                $s .= "<hr>";
            }
            else{
                $begin = false;
            }
            $s .= "<p>
                Shitstorm concernée : <a target='_blank' href='".HTTPS_URL."shitstorm/{$row['idSub']}'>".htmlspecialchars($row['title'])."</a><br>
                ID shitstorm : {$row['idSub']}<br>
                Postée par (ID) : {$row['posteur']}<br>
                Note laissée : ".($row['isLike'] ? 'positive' : 'négative')."<br>
                ID note : {$row['idSLike']}
                </p>";
        }
    }
    else{
        $s .= "<h3>Vous n'avez noté aucune shitstorm.</h3>";
    }

    return $s;
}

function constructComments(int $idUsr){
    global $connexion;

    $res = mysqli_query($connexion, "SELECT c.*, s.title FROM Comment c LEFT JOIN Shitstorms s ON c.idSub=s.idSub WHERE c.idUsr=$idUsr ORDER BY c.idSub DESC;");

    $s = '';

    $s .= "<h1>Commentaires</h1>";
    if($res && mysqli_num_rows($res)){
        $begin = true;
        while($row = mysqli_fetch_assoc($res)){
            // Infos des commentaires
            if(!$begin){
                $s .= "<hr>";
            }
            else{
                $begin = false;
            }
            $s .= "<p>
                Commentaire (ID) : {$row['idCom']}<br>
                Shitstorm du commentaire (ID) : <a target='_blank' href='".HTTPS_URL."shitstorm/{$row['idSub']}'>{$row['idSub']}</a><br>
                Titre de la shitstorm du commentaire : <a target='_blank' href='".HTTPS_URL."shitstorm/{$row['idSub']}'>".htmlspecialchars($row['title'])."</a><br>
                Date du commentaire : {$row['contentTime']} UTC<br>
                Utilisateur ayant posté le commentaire (ID) : {$row['idUsr']}<br>
                En réponse à (ID) : ".($row['inReplyToId'] ? $row['inReplyToId'] : 'aucun')."<br>
                En réponse dans la conversation (ID) : ".($row['inReplyToIdMain'] ? $row['inReplyToIdMain'] : 'aucune')."<br> 
                Contenu : ".preg_replace('/\n/', '<br>', htmlspecialchars($row['content']))."
                </p>";
        }
    }
    else{
        $s .= "<h3>Vous n'avez publié aucun commentaire.</h3>";
    }

    return $s;
}

function constructComLikes(int $idUsr){
    global $connexion;

    $res = mysqli_query($connexion, "SELECT c.*, s.contentTime, s.idSub, s.idUsr as posteur FROM ComLikes c LEFT JOIN Comment s ON c.idCom=s.idCom WHERE c.idUsr=$idUsr;");

    $s = '';

    $s .= "<h1>Favoris attribués aux commentaires</h1>";
    if($res && mysqli_num_rows($res)){
        $begin = true;
        while($row = mysqli_fetch_assoc($res)){
            if(!$begin){
                $s .= "<hr>";
            }
            else{
                $begin = false;
            }
            // Infos des favs
            $s .= "<p>
                Commentaire concerné (ID) : {$row['idCom']}<br>
                Shitstorm du commentaire (ID) : <a target='_blank' href='".HTTPS_URL."shitstorm/{$row['idSub']}'>{$row['idSub']}</a><br>
                Date du commentaire : {$row['contentTime']} UTC<br>
                Utilisateur ayant posté le commentaire (ID) : {$row['posteur']}<br>
                ID note : {$row['idCLike']}
                </p>";
        }
    }
    else{
        $s .= "<h3>Vous n'avez mis en favori aucun commentaire.</h3>";
    }

    return $s;
}

function constructAnswers(int $idUsr){
    global $connexion;

    $res = mysqli_query($connexion, "SELECT a.*, s.title, s.idUsr as posteur FROM Answers a JOIN Shitstorms s ON a.idSub=s.idSub WHERE s.idUsr=$idUsr OR a.idUsrA=$idUsr;");

    $s = '';

    $s .= "<h1>Réponses</h1>";
    if($res && mysqli_num_rows($res)){
        $begin = true;
        while($row = mysqli_fetch_assoc($res)){
            // Infos des réponses
            if(!$begin){
                $s .= "<hr>";
            }
            else{
                $begin = false;
            }

            $s .= "<p>
                Réponse (ID) : {$row['idAn']}<br>
                Shitstorm associée à la réponse (ID) : <a target='_blank' href='".HTTPS_URL."shitstorm/{$row['idSub']}'>{$row['idSub']}</a><br>
                Titre de la shitstorm associée : <a target='_blank' href='".HTTPS_URL."shitstorm/{$row['idSub']}'>".htmlspecialchars($row['title'])."</a><br>
                Date de la réponse : {$row['dateAn']} UTC<br>
                Date de post de la réponse : {$row['approvalDateA']} UTC<br>
                Utilisateur ayant posté la réponse (ID) : {$row['idUsrA']}".($row['idUsrA'] == $_SESSION['id'] ? ' (Vous-même)' : '')."<br>
                Utilisateur ayant posté la shitstorm associée (ID) : {$row['posteur']}".($row['posteur'] == $_SESSION['id'] ? ' (Vous-même)' : '')."<br>
                Description : ".($row['dscrA'] ? preg_replace('/\n/', '<br>', htmlspecialchars($row['dscrA'])) : 'Aucune')."<br><br>
                </p>";
            
            if(preg_match(REGEX_IMG_LINK_UPLOADED, $row['linkA'])){
                $s .= "<div class='image-card'><img class='shitstorm-image' alt='Image de réponse' src='{$row['linkA']}'></div>";
            }
            else { // Normalement c'est un lien de tweet
                $s .= "<div class='shit-link'><p><a target='_blank' href='{$row['linkA']}'>{$row['linkA']}</a></p>";
                $id = getTweetIdByLink($row['linkA']);

                if($id) {
                    $tweet = getTweetDetailsFromSave($id, false, true, true);

                    if($tweet) {
                        $s .= "<p>Tweet posté par {$tweet['name']}
                        (<a href='https://twitter.com/{$tweet['screen_name']}'>@{$tweet['screen_name']}</a>)</p>";

                        $s .= "<p>{$tweet['full_text']}</p>";

                        if($tweet['media_count']) {
                            $s .= "<h6>Médias</h6>";
                            $with_centered_img = (bool)($tweet['media_count'] % 2);
                            $remaining = $tweet['media_count'];
                            foreach($tweet['medias'] as $m) {
                                if($remaining > 1) {
                                    $s .= "<div class='media-block'><img alt='Image de tweet' src='{$m['link']}' class='tweet-media'></div>";
                                }
                                else {
                                    $s .= "<div class='media-block' ".($with_centered_img ? 'unique-media' : '').">
                                            <img alt='Image de tweet' src='{$m['link']}' class='tweet-media'>
                                        </div>";
                                }
                                $remaining--;
                            }
                            $s .= '<div class="clearb"></div>';
                        }
                    }
                    else {
                        $s .= "<p>Ce tweet n'a aucune sauvegarde enregistrée.</p>";
                    }
                }
                $s .= '</div>';
            }
        }
    }
    else{
        $s .= "<h3>Vous n'avez publié aucune réponse ou aucune de vos shitstorms n'a reçu de réponse.</h3>";
    }

    return $s;
}

function constructUserData(int $idUsr){
    global $connexion;

    $zip = new ZipArchive(); 

    $res = mysqli_query($connexion, "SELECT * FROM Users WHERE id=$idUsr;");

    if($res && mysqli_num_rows($res)){
        $rowUser = mysqli_fetch_assoc($res);

        $hash = md5(uniqid());

        $link = $idUsr . '-' . $rowUser['usrname'] . '-' . $hash . '.zip';
        $path = '/var/www/zipFiles/' . $link;
        $root_dir = ROOT . '/';

        if($zip->open($path, ZipArchive::CREATE) == TRUE) {
            $zip->addEmptyDir('img');
            $zip->addEmptyDir('img/uploaded');
            $zip->addEmptyDir('img/tweet_save');
            $zip->addEmptyDir('img/answer_save');
            $zip->addEmptyDir('css');

            $zip->addFile('/var/www/zipFiles/base.css', 'css/base.css');

            $ext_profile = null;
            $ext_banner = null;
            
            // Ajout bannière, PP utilisateur
            if(file_exists($root_dir . $rowUser['profile_img'])){
                // Récupération de l'extension du fichier
                $ext = pathinfo($root_dir . $rowUser['profile_img'], PATHINFO_EXTENSION);
                // Ajout du fichier dans l'archive
                $zip->addFile($root_dir . $rowUser['profile_img'], 'img/profile_img.'.$ext); 

                $ext_profile = '.' . $ext;
            }
            
            if($rowUser['banner_img'] && file_exists($root_dir . $rowUser['banner_img'])){
                $ext = pathinfo($root_dir . $rowUser['banner_img'], PATHINFO_EXTENSION);
                $zip->addFile($root_dir . $rowUser['banner_img'], 'img/banner_img.'.$ext);

                $ext_banner = '.' . $ext;
            }

            // Ajout images des shitstorms de l'utilisateur / des réponses dans le zip
            $resShit = mysqli_query($connexion, "SELECT l.link, l.idLink, l.idTweet FROM Links l JOIN Shitstorms s ON l.idSub=s.idSub WHERE s.idUsr=$idUsr;");
            if($resShit && mysqli_num_rows($resShit)){
                while($rowShit = mysqli_fetch_assoc($resShit)){
                    // Ajout des images contenues dans les tweets des shitstorms
                    $resimgTweet = mysqli_query($connexion, "SELECT * FROM tweetSave s JOIN tweetSaveImg i 
                                                              ON s.idSave=i.idSave WHERE s.idTweet='{$rowShit['idTweet']}';");

                    if($resimgTweet && mysqli_num_rows($resimgTweet)) {
                        while($rowImgA = mysqli_fetch_assoc($resimgTweet)) {
                            $ext = pathinfo($root_dir . $rowImgA['link'], PATHINFO_BASENAME);
                            $zip->addFile($root_dir . $rowImgA['link'], 'img/tweet_save/'.$ext);
                        }
                    }

                    if(preg_match(REGEX_IMG_LINK_UPLOADED, $rowShit['link']) && file_exists($root_dir . $rowShit['link'])){
                        $ext = pathinfo($root_dir . $rowShit['link'], PATHINFO_BASENAME);
                        $zip->addFile($root_dir . $rowShit['link'], 'img/uploaded/'.$ext);
                    }
                }
            }

            $resAnsw = mysqli_query($connexion, "SELECT a.linkA, a.idAn, a.idTweet FROM Answers a JOIN Shitstorms s ON a.idSub=s.idSub WHERE s.idUsr=$idUsr OR a.idUsrA=$idUsr;");
            if($resAnsw && mysqli_num_rows($resAnsw)){
                while($rowAnsw = mysqli_fetch_assoc($resAnsw)){
                    // Ajout des images contenues dans les tweets des réponses
                    $resimgAnswer = mysqli_query($connexion, "SELECT * FROM tweetSave s JOIN tweetSaveImg i 
                                                              ON s.idSaveA=i.idSaveA WHERE idTweet='{$rowAnsw['idTweet']}';");

                    if($resimgAnswer && mysqli_num_rows($resimgAnswer)) {
                        while($rowImgA = mysqli_fetch_assoc($resimgAnswer)) {
                            $ext = pathinfo($root_dir . $rowImgA['link'], PATHINFO_BASENAME);
                            $folder = explode('/', $rowImgA['link'])[1];

                            $zip->addFile($root_dir . $rowImgA['link'], "img/$folder/$ext");
                        }
                    }

                    if(preg_match(REGEX_IMG_LINK_UPLOADED, $rowAnsw['linkA']) && file_exists($root_dir . $rowAnsw['linkA'])){
                        $ext = pathinfo($root_dir . $rowAnsw['linkA'], PATHINFO_BASENAME);
                        $zip->addFile($root_dir . $rowAnsw['linkA'], 'img/uploaded/'.$ext);
                    }
                }
            }

            $menu = "<div class='nav-bar'>
                Accès rapide &#12539; <a href='#profile'>Profil</a> &#12539; <a href='#shitstorms'>Shitstorms</a> &#12539; <a href='#shitrate'>Notes</a> &#12539;
                <a href='#comments'>Commentaires</a> &#12539; <a href='#commentrate'>Favoris</a> &#12539; <a href='#answers'>Réponses</a>
            </div>";

            // Récupération des fichiers terminées, construction de l'HTML
            ob_start();
            ?>
            <!DOCTYPE html>
            <html>

            <head>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="ie=edge">

                <title>Archive de <?= $rowUser['usrname'] ?></title>
                <link href="css/base.css" rel="stylesheet" media="all" type="text/css">
            </head>

            <body>
                <main id='main-block'>
                    <div class='clearb' style='margin-bottom: 0px;'></div>
                    <div class="container">
                        <?= $menu ?>
                        <div id='profile'><?= constructProfile($idUsr, $ext_banner, $ext_profile) ?></div>
                        <?= $menu ?>
                        <div id='shitstorms'><?=  constructShitstorms($idUsr) ?></div>
                        <?= $menu ?>
                        <div id='shitrate'><?= constructShitLikes($idUsr) ?></div>
                        <?= $menu ?>
                        <div id='comments'><?=  constructComments($idUsr) ?></div>
                        <?= $menu ?>
                        <div id='commentrate'><?=  constructComLikes($idUsr)  ?></div>
                        <?= $menu ?>
                        <div id='answers'><?=  constructAnswers($idUsr)  ?></div>
                        <?= $menu ?>
                    </div>
                </main>
            </body>
            </html> <?php

            $str_html = ob_get_clean();

            $zip->addFromString('index.html', $str_html);

            $zip->close();

            return [$path, $hash];
        }
    }
   
    return false;
}

// Génère un fichier HTML qui contient les données personnelles

global $connexion;

if(!isLogged() || !isset($connexion)){
    echo "<h3 class='error'>Vous n'êtes pas connecté.</h3>";
    echo "<h6 class='error'>Connectez-vous à votre compte pour obtenir votre archive.</h6>";
    echo "<p class='flow-text'>
        Si vous n'avez jamais créé de compte sur le Journal des Shitstorms, aucune donnée n'est à récupérer.<br>
        Le Journal ne regarde pas l'activité de ses utilisateurs : Un utilisateur sans profil n'a donc aucune donnée enregistrée sur le site !
    </p>";
    return; exit();
}

$res = mysqli_query($connexion, "SELECT lastDataGeneration, moderator FROM Users WHERE id='{$_SESSION['id']}';");

if($res && mysqli_num_rows($res)){
    // Vérification que la date est valide
    $row = mysqli_fetch_assoc($res);

    $too_recent = false;
    $current_date = gmdate('Y-m-d H:i:s');
    $dateLast = new DateTime($row['lastDataGeneration'] . ' UTC');
    $dateLast = $dateLast->add(new DateInterval('P1D')); // Ajoute un jour à la date
    $dateActual = new DateTime(gmdate('Y-m-d H:i:s e')); // e est la timezone (UTC)
    $forceComplilation = (isModerator() && $GLOBALS['url_params'] === 'force');

    if($dateLast < $dateActual || $forceComplilation){ // Si ça fait plus de 1 jour depuis la dernière archive
        $se = constructUserData($_SESSION['id']); // se est le nom de l'archive zip à faire télécharger
        if($se){
            mysqli_query($connexion, "UPDATE Users SET lastDataGeneration='$current_date', lastDownloadArchive='1970-01-01' WHERE id='{$_SESSION['id']}';");
            
            $ha = $se[1];
            $file_size = (float)filesize($se[0]) / 1024;
            $unit = 'Ko';
            if(($file_size / 1024) > 1){ // Si il y a plus d'1 Mo
                $file_size /= 1024;
                $unit = 'Mo';
            }

            echo "<h3>Archive à télécharger</h3>";
            echo "<div class='row' style='margin-top: 40px;'>
                <div class='col s12 l10 offset-l1'>
                    <div class='card-user-new border-user' style='height: auto !important;'>";
                echo "  <div class='col s12 link-download'>
                            <p class='flow-text'>
                                <a target='_blank' href='".HTTPS_URL."static/get_last_save.php?hash=$ha'>Sauvegarde du ".strftime('%d/%m/%Y %H:%M', filemtime($se[0]))."</a><br>
                                (". round(($file_size), 1)." $unit)
                            </p>
                        </div>";
                echo "<div class='clearb'></div>
                    </div>
                </div>
            </div>";
        }
    }
    else{
        $res = mysqli_query($connexion, "SELECT lastDownloadArchive, moderator FROM Users WHERE id='{$_SESSION['id']}';");

        $row = mysqli_fetch_assoc($res);

        $too_recent = false;
        $dateDown = new DateTime($row['lastDownloadArchive'] . ' UTC');
        $dateDown = $dateDown->add(new DateInterval('PT120M')); // Ajoute 120 minutes

        if($dateDown < $dateActual || $row['moderator']){ // Si ça fait plus de 2h depuis le dernier téléchargement
            // Recherche si un fichier .zip est encore disponible pour téléchargement
            $files = [];

            $files = glob("/var/www/zipFiles/{$_SESSION['id']}-*.zip");
            usort($files, function($a, $b) { // Trie en fonction du plus récent d'abord
                return filemtime($a) < filemtime($b);
            });

            if(!empty($files)){ // Si un fichier a été trouvé
                // On liste les fichiers
                echo "<h3>Archive à télécharger</h3>";
                echo "<div class='row' style='margin-top: 40px;'>
                    <div class='col s12 l10 offset-l1'>
                        <div class='card-user-new border-user' style='height: auto !important;'>";
                
                foreach($files as $f){
                    // Extraction du hash
                    $file_name = pathinfo($f, PATHINFO_FILENAME);
                    $file_name = explode('-', $file_name);
                    $file_name = end($file_name);

                    $file_size = (float)filesize($f) / 1024;
                    $unit = 'Ko';
                    if(($file_size / 1024) > 1){ // Si il y a plus d'1 Mo
                        $file_size /= 1024;
                        $unit = 'Mo';
                    }

                    echo "<div class='col s12 link-download'>
                        <p class='flow-text'>
                            <a target='_blank' href='".HTTPS_URL."static/get_last_save.php?hash=$file_name'>Sauvegarde du ".strftime('%d/%m/%Y %H:%M', filemtime($f))."</a><br>
                            (". round($file_size, 1) ." $unit)
                        </p>
                    </div>";
                }
                
                echo "  <div class='clearb'></div></div>
                    </div>
                </div>";
            }
            else{
                $renewalDate = $dateActual->diff($dateLast)->format('%hh et %imin');

                echo "<h4 class='error'>Impossible d'obtenir les données : Aucun téléchargement n'est disponible.</h4>
                      <h5 class='error'>Une archive a déjà été demandée durant les dernières 24 heures.</h5>
                      <h5>Vous pourrez demander une nouvelle génération d'archive dans $renewalDate.</h5>";
            }
        }
        else{
            $newDownDate = $dateActual->diff($dateDown)->format('%hh et %imin');
            echo "<h4 class='error'>Impossible d'obtenir les données : Un téléchargement a déjà été demandé récemment.</h4>
                  <h5 class='error'>Une archive a déjà été téléchargée durant les dernières 24 heures.</h5>
                  <h5>Vous pourrez effectuer un nouveau téléchargement dans $newDownDate.</h5>";
        }
    }
}
else{
    echo "<h4 class='error'>Impossible d'obtenir les données : Une erreur est survenue./h4>
          <h5 class='error'>Merci de réessayer plus tard.</h5>";
}
