<div class="row">
    <h2>Erreurs</h2>
    <div class="col s12 m12">
        <div class="card" style="padding-bottom: 3em;">
            <div class="card-content">
                <pre><?= htmlentities(file_get_contents('php/info/api/error.txt')) ?></pre>
            </div>
        </div>
    </div>
</div>