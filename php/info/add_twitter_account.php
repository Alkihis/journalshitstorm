<?php
// PAGE CENSEE ETRE INCLUSE DANS L'URL DU COMPTE
// >> compte/others/add_twitter_account <<

// Vérification si c'est un renouvellement
$renew = $GLOBALS['url_params'] && $GLOBALS['url_params'] === 'renew';
$user_id_renew = null;

// Vérification que l'utilisateur connecté n'a pas de compte Twitter déjà lié
global $connexion;
$q = mysqli_query($connexion, "SELECT twitter_id FROM Users WHERE twitter_id IS NOT NULL AND id='{$_SESSION['id']}';");

if(!$q) {
    echo '<h3 class="error">Impossible d\'établir une relation avec notre base de données.</h3>';
    return;
}
if(mysqli_num_rows($q)) {
    if(!$renew){
        echo '<h3 class="error">Un compte Twitter est déjà associé au compte actuellement connecté.</h3>';
        return;
    }
    else { // On veut renouveller : on sauvegarde l'ID utilisateur du compte Twitter associé
        $qrow = mysqli_fetch_assoc($q);
        $user_id_renew = $qrow['twitter_id'];
    }
}

// Gestion du callback : Récupération de l'access token
$request_token = [];
$request_token['oauth_token'] = $_SESSION['tokens']['request'] ?? null;
$request_token['oauth_token_secret'] = $_SESSION['tokens']['request_secret'] ?? null;

if (isset($_REQUEST['denied'])) {
    echo "<h5>Vous avez annulé la demande de liaison.</h5>";
    return;
}

// Vérifications que les tokens reçus sont valides et que la sauvegarde de session existe encore
if (isset($_REQUEST['oauth_token']) && $request_token['oauth_token'] !== $_REQUEST['oauth_token']) {
    echo "<h5 class='error'>Impossible de vérifier les jetons.</h5>";
    return;
}
else if (!isset($request_token['oauth_token'], $request_token['oauth_token_secret'])) {
    echo "<h5 class='error'>Votre session a expiré ou les cookies sont désactivés. Impossible de continuer.</h5>";
    return;
}
else if (!isset($_REQUEST['oauth_verifier'])) {
    echo "<h5 class='error'>Impossible de vérifier que vous provenez de Twitter.</h5>";
    return;
}

try {
    $twitter_link = new OAuth(CONSUMER_KEY, CONSUMER_SECRET, OAUTH_SIG_METHOD_HMACSHA1, OAUTH_AUTH_TYPE_AUTHORIZATION);
    $twitter_link->setToken($request_token['oauth_token'], $request_token['oauth_token_secret']);

    $access_tokens = $twitter_link->getAccessToken('https://api.twitter.com/oauth/access_token', '', $_REQUEST['oauth_verifier'], 'POST');
} catch (OAuthException $e) {
    throw new Exception('Impossible d\'obtenir l\'access token Twitter', 402);
}

// à partir de cet endroit =>
// Enregistrement du Twitter ID dans le compte en cours
$json_encoded_token = json_encode($access_tokens);

// Vérification que l'UserID obtenu correspond avec l'UserID enregistré si c'est un renouvellement
if($renew) {
    if($access_tokens['user_id'] !== $user_id_renew) {
        echo "<h5 class='error'>Le compte Twitter authentifié n'est pas le compte avec lequel votre profil est lié.</h5>";
        echo "<h6>Pour lier ce compte, révoquez l'accès du compte actuel, puis recommencez.</h6>";
        return;
    }
}
// Sinon vérification que l'UserID n'est pas déjà utilisé pour un autre profil
else {
    $res = mysqli_query($connexion, "SELECT twitter_id FROM Users WHERE twitter_id={$access_tokens['user_id']};");
    if(!$res || mysqli_num_rows($res)) {
        echo "<h5 class='error'>Ce compte Twitter est déjà associé à un autre profil.</h5>";
        return;
    }
}

$res = mysqli_query($connexion, "UPDATE Users SET twitter_id='{$access_tokens['user_id']}', access_token_twitter='$json_encoded_token' WHERE id='{$_SESSION['id']}';");

if($res) {
    echo '<h3 class="pass">Votre compte Twitter a été lié correctement.</h3>';
    global $rowUser;
    $rowUser['twitter_id'] = $access_tokens['user_id'];
    $rowUser['access_token_twitter'] = $json_encoded_token;
}
else {
    echo '<h3 class="error">Une erreur interne est survenue. Merci de réessayer ultérieurement.</h3>';
}
