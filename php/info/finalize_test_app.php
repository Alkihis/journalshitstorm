<?php

$token = '';

if(isset($_SESSION['app_custom_token'])) {
    $token = $_SESSION['app_custom_token'];
}

echo "<p class='flow-text' style='word-wrap: break-word;'>";

if(isset($_REQUEST['verifier'])) {
    $verifier = $_REQUEST['verifier'];

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => 'https://api.shitstorm.fr/v2/auth/access_token.json',
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => array(
            'app_token' => $token,
            'verifier' => $verifier
        )
    ));
    // Send the request & save response to $resp
    $resp = curl_exec($curl);
    // Close request to clear up some resources
    curl_close($curl);

    if($resp) {
        $json = json_decode($resp, true);

        if(isset($json['access_token'])) {
            echo "Félicitations ! Voici votre token : <br>", $json['access_token'];
        }
        else {
            echo "Erreur :<br>", $json['message'];
            echo '<br>';
            var_dump($json);
        }
    }
    else {
        echo "Quelque chose s'est mal passé :(";
    }
}
else {
    echo "Quelque chose s'est mal passé :(";
}

echo "</p>";
