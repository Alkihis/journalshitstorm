<?php
// -----------------------------------------------------
// VUE UNIQUE D'UNE SHITSTORM RECUPEREE PAR GET[idPubli]
// -----------------------------------------------------

if(isset($_GET['generated'])){
    $gen = addslashes($_GET['generated']);
    if(file_exists(ROOT . "/img/preview/$gen.html")){
        $GLOBALS['navbar']->addElement("journal", "Journal");
        $GLOBALS['navbar']->addElement("soumission", "Ajouter une shitstorm");
        $GLOBALS['navbar']->addElement("unique?generated={$gen}", "Prévisualisation");
        include(ROOT . "/img/preview/$gen.html");
        return;
    }
    else{
        echo "<h2 class='error'>La page générée demandée n'est plus disponible.</h2>";
        include(ROOT . '/php/info/404.php'); 
        return;
    }
}

if(!isset($_GET['idPubli'])){
    if(isset($GLOBALS['current_page']) && isset($GLOBALS['current_page'][2]) && $GLOBALS['current_page'][2] != ''){
        $_GET['idPubli'] = urldecode($GLOBALS['current_page'][2]);
    }
    elseif(isset($GLOBALS['is_included'])) { include('php/info/404.php'); return; }
    else {header("Location:/404"); exit();}
}

global $connexion;
require_once(ROOT . '/inc/postDelete.php');

$idUsrP = 0;
$idSub = mysqli_real_escape_string($connexion, $_GET['idPubli']);
$pubCom = NULL;
$row = NULL;
$done = false;

// Vérifie l'existance de la shitstorm
if(is_numeric($idSub) && $idSub > 0){
    $res = mysqli_query($connexion, "SELECT idUsr, idSub, title FROM Shitstorms WHERE idSub='$idSub';");
}
else{
    $idSub = substr(trim($idSub), 0, strlen($idSub)-1);
    // Supprime le - terminal obligatoire qui sert à le différencier d'un nombre

    $res = mysqli_query($connexion, "SELECT idUsr, idSub, title FROM Shitstorms WHERE title='$idSub';");
}

if($res && mysqli_num_rows($res) > 0){
    $row = mysqli_fetch_assoc($res);
    $idSub = $row['idSub'];
    // echo "<a href='". getShitstormLink($row['title']) ."'>Lien lien lien</a>";

    // Gestion de la publication / suppression
    if(isset($_POST['delCom']) && isset($_SESSION['id']) && isset($_COOKIE['token']) && isset($_POST['del'])){
        supprimerCommentaire();
    }

    if(isset($_POST['pubCom'], $_POST['in_reply_to_id']) && isset($_SESSION['id']) && isset($_SESSION['connected']) && $_SESSION['connected']){
        publierCommentaire($idSub, $pubCom, $row['idUsr']); 
    }

    if(isset($_POST['delAnsw']) && ((isset($_SESSION['mod']) && $_SESSION['mod'] > 0) && isset($_POST['idAnswer']) || isset($_SESSION['id']))){
        $idAn = mysqli_real_escape_string($connexion, $_POST['idAnswer']);
        supprimerReponse($idSub, $_SESSION['id'], $idAn);
    }

    $res = mysqli_query($connexion, "SELECT *, (SELECT COUNT(*) FROM Comment WHERE idSub='$idSub') c FROM Shitstorms WHERE idSub='$idSub';");

    if($res && mysqli_num_rows($res) > 0){
        $done = true;
        $row = mysqli_fetch_assoc($res);
    }
}

// Formation du breadcrumb
$GLOBALS['navbar']->addElement('journal', 'Journal');

if($done){
    $year = explode("-", $row['dateShit']);
    $month = getTextualMonth($year[1]);

    $GLOBALS['navbar']->addElement("journal?date={$row['dateShit']}&goBack=1#pub$idSub", "$month {$year[0]}");
    $GLOBALS['navbar']->addElement("shitstorm/{$row['idSub']}", htmlspecialchars($row['title']));
}
else{
    $GLOBALS['navbar']->addElement('404', 'Erreur 404');
}

if(!$res || @mysqli_num_rows($res) == 0){
    echo "<h4>Aucune shitstorm correspondant à l'identifiant n'a été trouvée.</h4>";
}
else{
    // Définition du titre de la page
    $_SESSION['pageTitle'] = htmlspecialchars($row['title']) . ' - Le journal des shitstorms';

    $img_shitstorm_link = getImageLinkRelatedToShitstorm($idSub);

    $GLOBALS['addMeta'] = '<meta name="twitter:title" content="'.htmlspecialchars($row['title']).'"/>
    <meta name="twitter:description" content="Shitstorm du '.formatDate($row['dateShit'], false).'"/>
    <meta name="twitter:image" content="'.$img_shitstorm_link.'"/>
    <meta name="Description" content="Title: '.htmlspecialchars($row['title']).'">';
    
    // Affichage des boutons shitstorm précédente / suivante
    $idPrevious = getIDFromPreviousSS($idSub, $row['dateShit']);
    $idNext = getIDFromNextSS($idSub, $row['dateShit']);
    
    ?>

    <!-- Boutons suivant/précédent --> 
    <div class='row'>
        <ul class="pagination center hide-on-small-only">
            <li class="<?= ($idPrevious ? 'waves-effect' : 'disabled') ?>"><a href="<?= HTTPS_URL . ($idPrevious ? "shitstorm/$idPrevious" : '#!' ) ?>"><i class="material-icons">chevron_left</i></a></li>
            <li style='vertical-align: sub; font-size: 1.3em; color: #7392ea;'><?= htmlentities($row['title']) ?></li>
            <li class="<?= ($idNext ? 'waves-effect' : 'disabled') ?>"><a href="<?= HTTPS_URL . ($idNext ? "shitstorm/$idNext" : '#!' ) ?>"><i class="material-icons">chevron_right</i></a></li>
        </ul>
        <h5 class='center hide-on-med-and-up'><?= htmlentities($row['title']) ?></h5>
        <div class='col s12'>
            <ul class="pagination hide-on-med-and-up">
                <li class="left <?= ($idPrevious ? 'waves-effect' : 'disabled') ?>"><a href="<?= HTTPS_URL . ($idPrevious ? "shitstorm/$idPrevious" : '#!' ) ?>"><i class="material-icons">chevron_left</i></a></li>
                <li class="right <?= ($idNext ? 'waves-effect' : 'disabled') ?>"><a href="<?= HTTPS_URL . ($idNext ? "shitstorm/$idNext" : '#!' ) ?>"><i class="material-icons">chevron_right</i></a></li>
            </ul>
        </div>
    </div>
    <div class='clearb'></div>

    <?php
    // Récup du premier lien de la shitstorm (les possibles suivants ne sont pas chargés dans le journal)
    $linksres = mysqli_query($connexion, "SELECT * FROM Links WHERE idSub='$idSub' ORDER BY idLink ASC;");
    if(!$linksres || @mysqli_num_rows($linksres) == 0){
        echo "<p class='empty'>Impossible de charger les liens correspondants à la shitstorm. Veuillez recharger la page.</p>";
        exit();
    }
    $nbLink = mysqli_num_rows($linksres);

    // Récup de(s) possible(s) réponse(s)
    $linkAnswer = mysqli_query($connexion, "SELECT * FROM Answers WHERE idSub='$idSub';");
    $hasAnswer = NULL;
    $answer = NULL;
    if(!$linkAnswer || mysqli_num_rows($linkAnswer) == 0){
        $hasAnswer = false;
    }
    else{
        $hasAnswer = mysqli_num_rows($linkAnswer);
    }

    // Récupération du nombre de likes
    $nbLikes = recupereNbLikes($idSub);
    if(!$nbLikes){
        $nbLikes = '0';
    }

    // Récupération si l'utilisateur courant a like ou dislike
    $hasLiked = false;
    $hasDisliked = false;

    if(isset($_SESSION['id']) && isset($_SESSION['connected']) && $_SESSION['connected']){
        $resPresence = mysqli_query($connexion, "SELECT * FROM ShitLikes WHERE idUsr='{$_SESSION['id']}' AND idSub='$idSub';");
        if($resPresence && mysqli_num_rows($resPresence) > 0){
            $rowPres = mysqli_fetch_assoc($resPresence);
            if($rowPres['isLike']){
                $hasLiked = true;
            }
            else{
                $hasDisliked = true;
            }
        }
    }

    $s_num = ($hasAnswer ? 4 : 6);
    $not_classic = !(isset($_GET['classic']) && isActive($_GET['classic']));

    if($not_classic){
        $onglets = "<li class='tab col s$s_num'><a class='active' href='#shitstorm'>Shitstorm</a></li>" . 
            ($hasAnswer ? "<li class='tab col s$s_num'><a href='#answer_b'>Réponse".($hasAnswer > 1 ? 's' : '')."</a></li>" : '') . 
            "<li class='tab col s$s_num'><a href='#comments_b'>Commentaires</a></li>";
    }

    // Bouton de suivi de la shitstorm
    if(isset($_SESSION['id'], $_SESSION['connected']) && $_SESSION['connected']){ 
        // Vérification si elle est suivi par l'utilisateur connectée
        $resFol = mysqli_query($connexion, "SELECT withMail FROM ShitFollowings WHERE idFollowed='$idSub' AND idFollower='{$_SESSION['id']}';");

        $with_mail = false;
        $is_followed = false;

        if($resFol && mysqli_num_rows($resFol)){
            $rowFol = mysqli_fetch_assoc($resFol);
            $with_mail = $rowFol['withMail'];
            $is_followed = true;
        }
        ?>
        <!-- Dropdown Structure -->
        <ul id='follow_shit_drop<?= $idSub ?>' class='dropdown-content' data-title='<?= htmlspecialchars($row['title'], ENT_QUOTES) ?>'>
            <li>
                <span>
                    <input class='follow-activate follow-shitstorm-checkbox' data-idsub='<?= $idSub ?>' type='checkbox' id='followed_shit<?= $idSub ?>' <?= ($is_followed ? 'checked' : '') ?>>
                    <label class='black-text' for="followed_shit<?= $idSub ?>"><?= $is_followed ? 'Suivie' : 'Suivre' ?></label>
                </span>
            </li>
            <li>
                <span>
                    <input class='follow-activate email-shitstorm-checkbox' data-idsub='<?= $idSub ?>' type='checkbox' id='followed_shit_with_mail<?= $idSub ?>' <?= ($with_mail ? 'checked' : '') ?>>
                    <label class='black-text' for="followed_shit_with_mail<?= $idSub ?>">E-mails</label>
                </span>
            </li>
        </ul> <?php 
    }

    // ------------
    // PUBLICATION
    // ------------
    ?>
    <!-- Onglets -->
    <?php if($not_classic) $GLOBALS['nav_tab'] = '<div class="row"><ul class="tabs"><div class="col s12 l8 offset-l2">' . $onglets . '</div></ul></div>'; ?>

    <script src="<?= HTTPS_URL ?>js/unique.js"></script>
    <script src="<?= HTTPS_URL ?>js/comment.js"></script>
    <div class="row">
        <div class="col s12 <?= ($not_classic && $nbLink > 1 ? "l10 offset-l1" : "m10 l8 offset-m1 offset-l2") ?>">
            <div id='shitstorm'>
                <!-- SHITSTORM -->
                <div class="card card-border" style="padding-top: .5em;">
                    <div class="card-image">
                        <div class='link center'><?php 
                            generateUniqueShitTweetsFromMysqliLinksRes($linksres, !$not_classic);
                        ?>
                            <div class='clearb'></div>
                        </div>
                    </div>
                    <div class="card-content">
                        <!-- Description -->
                        <p style="margin-bottom: 1em;"><?php echo detectTransformLink(htmlentities($row['dscr'])); ?>

                        <!-- Boutons like/dislike -->
                        <div style="margin-bottom: 1em;">
                            <span class='likeButtons'>
                                
                                <?php
                                if(isset($_SESSION['id']) && isset($_SESSION['connected'])): ?>
                                    <form>
                                        <button type='button' class='btn-floating blue lighten-2 tooltipped' data-position="bottom" data-delay="50" data-tooltip="+1" style='float: left; margin-right: 10px;' name='favSub' id='like<?= $idSub ?>'>
                                            <i class='material-icons<?= ($hasLiked ? ' liked' : '') ?>' id='blike<?= $idSub ?>'>keyboard_arrow_up</i>
                                        </button> 
                                    </form>
                                    <form>
                                        <button type='button' class='btn-floating red lighten-3 tooltipped' data-position="bottom" data-delay="50" data-tooltip="-1" style='float: left; margin-right: 10px;' name='unfavSub' id='dlike<?= $idSub ?>'>
                                            <i class='material-icons<?= ($hasDisliked ? ' disliked' : '') ?>' id='dislike<?= $idSub ?>'>keyboard_arrow_down</i>
                                        </button> 
                                    </form><?php
                                endif;
                                ?>
                                <script>
                                    $("#like<?= $idSub ?>").click(function(){
                                        likeDislike(<?= $idSub ?>, true);
                                    });
                                    $("#dlike<?= $idSub ?>").click(function(){
                                        likeDislike(<?= $idSub ?>, false);
                                    });
                                </script>
                                <button class="btn-floating orange darken-5 tooltipped" data-position="bottom" data-delay="50" data-tooltip="Note" 
                                    style="float: left; margin-bottom: 1em;" id='likecount<?= $idSub ?>' onclick="generateViewRateModal('modalplaceholder', <?= $idSub ?>);"><?= $nbLikes ?>
                                </button> 
                                <?php if(isset($_SESSION['id'], $_SESSION['connected']) && $_SESSION['connected']) { ?>
                                    <button class='left btn-floating <?= $is_followed ? 'green' : 'grey' ?> dropdown-button' style='margin-left: 10px;' 
                                        id='follow_shit_button<?= $idSub ?>' data-activates='follow_shit_drop<?= $idSub ?>'>
                                        <i id='follow_shit_bell<?= $idSub ?>' class='material-icons'><?= $is_followed ? 'notifications' : 'notifications_off' ?></i>
                                    </button>
                                <?php } ?>
                            </span> 
                        </div>

                        <!-- Boutons éditions / proposer réponse -->
                        <?php if (!$hasAnswer || $hasAnswer < MAX_ANSWER_COUNT){ ?>
                            <div> <?php 
                                if((isset($_SESSION['id']) && $_SESSION['id'] == $row['idUsr']) || (isset($_SESSION['mod']) && $_SESSION['mod'] > 0)){
                                    echo "<a href='".HTTPS_URL."modify/$idSub'>
                                    <button style='float: left; margin-bottom: 1em; margin-left: 10px;' name='edit' class='btn-floating waves-effect waves-light teal lighten-1 tooltipped' data-position='bottom' data-delay='50' data-tooltip='Éditer'>
                                        <i class='material-icons left'>mode_edit</i>éditer
                                    </button>
                                    </a>";
                                } ?>
                                <a href='<?= HTTPS_URL ?>reponse/<?= $idSub ?>'>
                                    <button class='btn col s12 m12 l5 waves-effect waves-light teal lighten-1' style='float: right; margin-bottom: 1em;'><i class="material-icons right">announcement</i>Proposer une réponse</button>
                                </a>
                            </div>
                            <?php } 
                            else { ?>
                                <div>
                                    <?php 
                                    if((isset($_SESSION['id']) && $_SESSION['id'] == $row['idUsr']) || (isset($_SESSION['mod']) && $_SESSION['mod'] > 0)){
                                        echo "<a href='".HTTPS_URL."modify/$idSub'>
                                        <button style='float: left; margin-bottom: 1em; margin-left: 10px;' name='edit' class='btn-floating  waves-effect waves-light teal lighten-1 tooltipped' data-position='bottom' data-delay='50' data-tooltip='Éditer'>
                                            <i class='material-icons left'>mode_edit</i>éditer
                                        </button>
                                        </a>";
                                    } ?>
                                </div>
                            <?php } ?>
                        </p>

                        <!-- Informations de la shitstorm -->
                        <div class="row card-action" style='clear: both;'>
                            <p style="float: left;">Shitstorm du <?php echo formatDate($row['dateShit'], 0); ?></p>
                            <span style='margin-left: .8em;'><a class="twitter-share-button"
                                href="https://twitter.com/intent/tweet?text=<?= urlencode($row['title']) ?>&url=<?= urlencode('https://shitstorm.fr/shitstorm/'.$row['idSub']) ?>">
                            Tweet</a></span>
                            <p data-position='bottom' data-delay='10' data-tooltip='<?= formatDate($row['approvalDate'], 1) ?>' class='tooltipped right' style="font-style: italic;">Ajoutée par <?php 
                                if($row['idUsr'] > 0){
                                    echo "<a class='notCard' href='".HTTPS_URL."profil?userid={$row['idUsr']}'>{$row['pseudo']}</a>";
                                }
                                else{
                                    echo "{$row['pseudo']}";
                                } 
                            ?> </p>
                        </div>
                    </div>
                </div>
            </div>

            <div id='answer_b'>
                <?php if($hasAnswer){
                    // REPONSE DU CREATEUR 
                    if(!genereReponses($linkAnswer)){ // genereReponses s'occupe de l'affichage des réponses en HTML
                        echo "<h4>Une erreur est survenue lors de la génération des réponses.</h4>";
                    }
                } ?>
            </div>

            <!-- Commentaires -->
            <div id='comments_b'>
                <div id='sorting_comment_block' class='card card-border' style="padding-top: .8em; display: none;">
                    <div class='card-image' style='padding: 1.5em; padding-top: 0.7em;'>
                        <h6 class='center'>Tri des commentaires</h6>
                        <form>
                            <select id='selecttype' data-sub='<?= $row['idSub'] ?>' name='selecttype' class='col s5'>
                                <option value='recent' selected>Date</option>
                                <option value='rating'>Note</option>
                            </select>
                            <select id='selectsort' data-sub='<?= $row['idSub'] ?>' name='selectsort' class='col s5 offset-s2'>
                                <option value='asc'>Croissante</option>
                                <option value='desc' selected>Décroissante</option>
                            </select>
                            <div class='clearb'></div>
                        </form>
                    </div>
                </div>

                <div id='comment_parent_block'>
                </div>
                <script async>
                    $(document).ready(function () {
                        initComment(<?= $row['idSub'] ?>);
                    });
                </script>
                <div class='clearb'></div>

                <?php
                if(isset($_SESSION['connected']) && $_SESSION['connected']){?>
                    <div class="card card-border" style="padding-top: .5em;">
                        <div class="card-content">
                            
                                <textarea style="margin-bottom: 0.5em;" id='contentCom' name="contentCom" rows="5" cols="50" placeholder="Ecrivez un nouveau commentaire..."
                                    data-idsub='' maxlength="<?= strval(MAX_LEN_COMMENT) ?>" class="materialize-textarea comment-textaera" required><?php 
                                    if($pubCom)
                                        echo htmlentities($_POST['contentCom']);
                                ?></textarea>
                                <input type='hidden' id='reply_to' name='in_reply_to_id' value='0'>
                                <div class="row card-action">
                                    <button class="btn blue lighten-1" onclick='postComment(<?= $row['idSub'] ?>)' style="float: right;" id='pubCom' name="pubCom">Publier</button>
                                </div>

                        </div>
                    </div> <?php
                }
                else{
                    echo "<p style='margin-bottom: 0.9em; font-style: italic;'>Connectez-vous pour pouvoir commenter cette shitstorm</p>";
                }
                ?>
            </div>
        </div>  
    </div>
    <?php
}
