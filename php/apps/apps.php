<?php

// Page de redirection pour les apps

$to_load = 'manage.php';

$GLOBALS['navbar']->addElement('apps', 'Applications');

if($GLOBALS['options'] === 'create') {
    $to_load = 'create.php';
}

if(!isLogged()) {
    return _403();
}
else if(!$_SESSION['email']) {
    echo '<h4 class="error">Vous devez avoir défini et confirmé votre adresse e-mail pour accéder à cette page.</h4>';
    echo '<h5>Vous pouvez gérer votre adresse e-mail dans les <a href="'.HTTPS_URL.'compte">paramètres</a>.</h5>';
    return;
}

require(PAGES_DIR . 'apps/' . $to_load);
