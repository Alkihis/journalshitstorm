<?php

// Manage existing apps

$GLOBALS['navbar']->addElement('apps/manage', 'Gérer');

// Récupération des applications existantes

global $connexion;

if(isset($_POST['id_application'])) { // Si l'utilisateur tente de supprimer une application
    $id_app = (int)$_POST['id_application'];
    
    if($id_app > 0) {
        // Vérification que l'app appartient bien à l'utilisateur connecté
        $resOwner = mysqli_query($connexion, "SELECT * FROM TokensApps WHERE idApp=$id_app AND idOwner={$_SESSION['id']};");

        if($resOwner && mysqli_num_rows($resOwner)) {
            $res = mysqli_query($connexion, "DELETE FROM TokensApps WHERE idApp=$id_app AND idOwner={$_SESSION['id']};");

            if($res) {
                $res = mysqli_query($connexion, "DELETE FROM Tokens WHERE idApp=$id_app;");
                echo '<h4 class="pass">L\'application a été supprimée.</h4>';
            }
            else {
                echo '<h4 class="error">Une erreur est survenue.</h4>';
            }
        }
        else {
            echo '<h4 class="error">Cette application ne vous appartient pas.</h4>';
        }
    }
    else {
        echo '<h4 class="error">L\'ID est invalide.</h4>';
    }
}

$res = mysqli_query($connexion, "SELECT * FROM TokensApps WHERE idOwner={$_SESSION['id']} ORDER BY idApp DESC;");

echo "<div class='row'>";
echo "<div class='col s12 l10 offset-l1'>";

echo "
<div class='card-user-new border-user black-text center' 
    style='height: auto !important; margin-bottom: 15px;'>
    <span class='underline'><a href='".HTTPS_URL."apps/create'>Créer une application</a></span> &bull; <span class='bold'>Gérer</span>
</div>";

if($res && mysqli_num_rows($res)) {
    while($row = mysqli_fetch_assoc($res)) { ?>
        <div class='application-owning card-user-new border-user' 
            style='height: auto !important; margin-bottom: 15px;'>
            <h5><?= htmlspecialchars($row['name']) ?></h5>
            <p>
                Cette application a des droits de lecture<?= ($row['allowWrite'] ? ' et d\'écriture' : '') ?>.
            </p>

            <a href="#modalinfosapplication" class='app-infos-show modal-trigger clearb card-pointer' 
                data-app-name="<?= htmlspecialchars($row['name'], ENT_QUOTES) ?>"
                data-app-dscr="<?= htmlspecialchars($row['dscr'], ENT_QUOTES) ?>"
                data-app-token="<?= htmlspecialchars($row['token'], ENT_QUOTES) ?>"
                data-app-rights="<?= (int)($row['allowWrite']) ?>"
                data-app-website="<?= htmlspecialchars($row['website'], ENT_QUOTES) ?>"
                data-app-id="<?= $row['idApp'] ?>">
                <div class='btn btn-personal card-user-new border-user attenuation' style='width: 100% !important; margin-top: 7px;'>
                    Gérer l'application
                </div>
            </a>

            <a href="#modaldelapplication" class='app-deleter modal-trigger clearb card-pointer' 
                data-app-id="<?= $row['idApp'] ?>">
                <div class='btn btn-personal card-user-new border-user attenuation' style='width: 100% !important; margin-top: 7px;'>
                    Supprimer l'application
                </div>
            </a>
        </div>
    <?php }
}
else {
    echo '<h4 class="empty">Vous n\'avez aucune application.</h4><h5>En <a href="'.HTTPS_URL.'apps/create">créer une</a> ?</h5>';
}

echo '</div>';
echo '</div>';
?>
<script>
    $(document).ready(function () {
        $('.app-deleter').on('click', function() {
            document.getElementById('id_application').value = this.dataset.appId;
        });
        $('.app-infos-show').on('click', function() {
            document.getElementById('app-id').innerHTML = this.dataset.appId;
            document.getElementById('app-name').innerHTML = this.dataset.appName;
            document.getElementById('app-website-href').href = this.dataset.appWebsite;
            document.getElementById('app-website').innerHTML = this.dataset.appWebsite;
            document.getElementById('app-dscr').innerHTML = this.dataset.appDscr;
            document.getElementById('app-token').innerHTML = this.dataset.appToken;
            document.getElementById('app-auth-text').href = https_url + "?page=info/create_test_application.php&custom_token=" + this.dataset.appToken;
            document.getElementById('app-rights').innerHTML = 'Lecture' + (Number(this.dataset.appRights) === 1 ? ' et écriture' : '');
        });
    });
</script>

<?php

// Modal de suppression
?>
<div class='row'>
    <div class="col s12 l8 offset-l2">
        <div id="modaldelapplication" class="modal bottom-sheet">
            <div class="modal-content">
                <h4>Voulez-vous vraiment supprimer cette application ?</h4>
                <p>
                    La suppression est irréversible.
                </p>
            </div>
            <div class="modal-footer" style='padding-left: 1.5em;'>
                <form method='post' action='#'>
                    <button name='deleteApp' style='text-align: right;'
                    class="modal-action waves-effect waves-green btn-flat col s3 l1 red lighten-1 left white-text">
                        <i class='material-icons left'>clear</i>Oui
                    </button>
                    <input id='id_application' name='id_application' type='hidden' value=''>
                    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 blue lighten-1 left white-text" style='margin-left : 2em;'>
                        <i class='material-icons left'>keyboard_capslock</i>Non
                    </a>
                    <div class='clearb'></div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php

// Modal d'informations
?>
<div class='row' style='padding-bottom: 0'>
    <div class="col s12 l8 offset-l2">
        <div id="modalinfosapplication" class="modal modal-fixed-footer">
            <div class="modal-content">
                <h4 id='app-name'></h4>
                <p class='flow-text'>
                    <span class='grey-text text-lighten-1'>Identifiant de l'application:</span> <span id='app-id'></span><br>
                    <span class='grey-text text-lighten-1'>Site web:</span> <a id='app-website-href' class='underline-hover' href=''><span id='app-website'></span></a><br>
                    <span class='grey-text text-lighten-1'>Description:</span> <span id='app-dscr'></span><br>
                    <span class='grey-text text-lighten-1'>Droits:</span> <span id='app-rights'></span><br><br>
                    <span class='underline grey-text text-lighten-1'>Application token:</span><br><span style='word-wrap: break-word;' id='app-token'></span><br>
                    <a id='app-auth-text' class='underline-hover' href=''>Tester l'authentification de cette app</a>
                </p>
            </div>
            <div class="modal-footer" style='padding-left: 1.5em;'>
                <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fermer</a>
            </div>
        </div>
    </div>
</div>
<?php
