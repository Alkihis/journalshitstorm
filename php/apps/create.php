<?php

// Create applications

$GLOBALS['navbar']->addElement('apps/create', 'Créer');

// Récupération des applications existantes

global $connexion;

if(isset($_POST['rights'], $_POST['name'], $_POST['website'], $_POST['dscr'])) {
    $rights = (int)(bool)$_POST['rights'];
    $name = trim($_POST['name']);
    $website = trim($_POST['website']);

    if(!preg_match('/^https?:/', $website)) {
        $website = "https://" . $website;
    } 

    $dscr = mysqli_real_escape_string($connexion, trim($_POST['dscr']));

    if(!preg_match(REGEX_REALNAME, $name)) {
        echo '<h4 class="error">Le nom est invalide.</h4>';
    }
    else if(strlen($dscr) > 150 || !$dscr) {
        echo '<h4 class="error">La description est trop longue ou inexistante.</h4>';
    }
    else if(strlen($website) > 150 || !preg_match(REGEX_WEB_LINK, $website)) {
        echo '<h4 class="error">L\'URL est trop longue ou invalide.</h4>';
    }
    else {
        $name = mysqli_real_escape_string($connexion, $name);
        $website = mysqli_real_escape_string($connexion, $website);

        // Vérification que le nom est pas déjà pris
        $res = mysqli_query($connexion, "SELECT idApp FROM TokensApps WHERE name='$name' AND idOwner={$_SESSION['id']};");

        if($res && mysqli_num_rows($res) === 0) {
            // Enregistrement de l'application
            $tokenApp = '';
            $tries = 0;

            // Vérification que ce token n'existe pas déjà
            do {
                $tokenApp = bin2hex(random_bytes(8));

                $tokenApp = mysqli_real_escape_string($connexion, $tokenApp);
                $res = mysqli_query($connexion, "SELECT * FROM TokensApps WHERE token='$tokenApp';");

                if($res && mysqli_num_rows($res) === 0) {
                    break;
                }
                $tries++;

                if($tries > 5) {
                    echo '<h4 class="error">Impossible de créer un token.</h4>';
                    $tokenApp = '';
                    break;
                }
            } while(true);

            if($tokenApp) {
                $res = mysqli_query($connexion, "INSERT INTO TokensApps (token, allowWrite, idOwner, name, website, dscr) 
                                                VALUES ('$tokenApp', $rights, {$_SESSION['id']}, '$name', '$website', '$dscr');");

                if($res) {
                    echo '<h4 class="pass">Votre application a été enregistrée.</h4>'; 
                    echo '<h5>Consultez ses détails <a href="'.HTTPS_URL.'apps">ici</a>.</h5>'; 
                    $_POST['name'] = $_POST['website'] = $_POST['dscr'] = '';
                    $_POST['rights'] = '0';
                }
                else {
                    echo '<h4 class="error">Impossible d\'enregistrer l\'application.</h4>';
                }
            }
        }
        else {
            echo '<h4 class="error">Vous avez déjà une application avec ce nom.</h4>';
        }
    }
}

?>

<div class='row'>
    <div class='col s12 l10 offset-l1'>

        <div class='card-user-new border-user black-text center' 
            style='height: auto !important; margin-bottom: 15px;'>
            <span class='bold'>Créer une application</span> &bull; <span class='underline'><a href='<?= HTTPS_URL ?>apps/manage'>Gérer</a></span>
        </div>

        <div class='application-owning card-user-new border-user' 
            style='height: auto !important; margin-bottom: 15px;'>
            <form method='post' action='#'>
                <div class="input-field col s12">
                    <input value="<?= (isset($_POST['name']) ? htmlspecialchars($_POST['name'], ENT_QUOTES) : '') ?>" id='name'
                            name='name' type="text" maxlength=32 required>
                    <label for="name">Nom de l'application</label>
                </div>

                <div class="input-field col s12">
                    <input value="<?= (isset($_POST['website']) ? htmlspecialchars($_POST['website'], ENT_QUOTES) : '') ?>" id='website'
                            name='website' type="text" maxlength=150 required>
                    <label for="website">Site web lié à l'application</label>
                </div>

                <div class="input-field col s12">
                    <input value="<?= (isset($_POST['dscr']) ? htmlspecialchars($_POST['dscr'], ENT_QUOTES) : '') ?>" id='dscr'
                            name='dscr' type="text" maxlength=150 required>
                    <label for="dscr">Courte description</label>
                </div>
                <div class='clearb'></div>

                <div class="input-field col s12">
                    <select class='material-select' id='rights' name='rights'>
                        <option value='0' <?= (isset($_POST['rights']) && $_POST['rights'] === '0' ? 'selected' : '') ?>>Lecture</option>
                        <option value='1' <?= (isset($_POST['rights']) && $_POST['rights'] === '1' ? 'selected' : '') ?>>Lecture et écriture</option>
                    </select>
                    <label for="rights">Droits de l'application</label>
                </div>

                <div class='clearb'></div>

                <button name='confirm' class='btn card-user-new border-user attenuation right btn-personal'>
                    Créer
                </button>
                <div class='clearb'></div>
            </form>
        </div>
    </div>
</div>
<?php
