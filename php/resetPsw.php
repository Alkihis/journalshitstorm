<?php
// ----------------------------
// RECUPERATION DE MOT DE PASSE
// ----------------------------

global $connexion;

$GLOBALS['navbar']->addElement('login', 'Connexion');
$GLOBALS['navbar']->addElement('passwordreset', 'Réinitialisation du mot de passe');

if(isset($_SESSION['connected']) && $_SESSION['connected']){
    echo "<h3>Vous êtes déjà connecté.</h3>";
    return;
}

if(isset($_POST['token']) && isset($_POST['newPsw']) && isset($_POST['confirmPw'])){
    // Si on vient du formulaire où on rentre le nouveau mot de passe
    $token = mysqli_real_escape_string($connexion, $_GET['token']);

    $res = mysqli_query($connexion, "SELECT * FROM Users WHERE resetToken='$token';");

    if($res && mysqli_num_rows($res) > 0 && $token != NULL){
        $user = mysqli_fetch_assoc($res);

        if(!preg_match(REGEX_PASSWORD, $_POST['newPsw'])){
            echo "<h5 class='error'>Le mot de passe ne contient pas les prérequis demandés.</h5>";
        }
        elseif($_POST['newPsw'] != $_POST['psw2'] || empty($_POST['psw2'])){
			echo "<h5 class='error'>Les mots de passe ne correspondent pas.</h5>";
        }
        else{
            $psw = password_hash($_POST['newPsw'], PASSWORD_BCRYPT);
            $res = mysqli_query($connexion, "UPDATE Users SET passw='$psw', resetToken=NULL WHERE id='{$user['id']}';");
            if($res){
                echo "<h4>La réinitialisation a réussi.</h4>";
            }
        }
    }
    else{
        echo "<h3>Impossible de valider le token.</h3><h4>Vérifiez si vous n'avez pas demandé une nouvelle réinitialisation de mot de passe.</h4>";
    }
}
// Si on vient du lien de l'email
elseif(isset($_GET['token'])){
    $token = mysqli_real_escape_string($connexion, $_GET['token']);

    $res = mysqli_query($connexion, "SELECT * FROM Users WHERE resetToken='$token';");
    if($res && mysqli_num_rows($res) > 0){
        $user = mysqli_fetch_assoc($res);

        $GLOBALS['navbar']->addElement("passwordreset?token={$_GET['token']}", 'Nouveau mot de passe de ' . $user['usrname'], 1);
        ?>
        <div class="row login">
            <div class="col s12 l8 offset-l2">
                <div class="card-new-flow" style="padding-top: .5em;">
                    <form action="#" method='post'>
                        <div class="input-field">
                            <label for="newPsw">Nouveau mot de passe</label>
                            <input type="password" id="newPsw" name="newPsw" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Doit contenir au moins 8 caract&egrave;res, un chiffre, une lettre minuscule et majuscule." required>
                        </div>
                        <div class="input-field">
                            <label for="psw2">Répétez le mot de passe</label>
                            <input type="password" id="psw2" name="psw2" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Doit contenir au moins 8 caract&egrave;res, un chiffre, une lettre minuscule et majuscule." required>
                        </div>
                        <input type='hidden' name='token' value='<?= $token ?>'>
                        <button name='confirmPw' class='btn btn-personal card-user-new border-user fit-content auto-height attenuation right' style='padding: 0.8em !important;'>
                            <a class='card-pointer' style='font-size: 1.2rem;'>Définir nouveau mot de passe</a>
                        </button>
                        <div class='clearb' style='padding-bottom: 2em;'></div>
                    </form>
                </div>
            </div>
        </div>
        <?php
    }
    else{
        echo "<h3>Impossible de valider le token.</h3><h4>Vérifiez si vous n'avez pas demandé une nouvelle réinitialisation de mot de passe.</h4>";
    }
}
elseif(isset($_SESSION['connected']) && $_SESSION['connected'] > 0){
    echo "<h2>Vous êtes déjà connecté</h2>";
}
else{ // Reste de la page
    if(isset($_POST['reset']) && isset($_POST['mail'])) {
        if(!preg_match(REGEX_MAIL, $_POST['mail']) || strlen($_POST['mail']) > MAX_LEN_EMAIL){
            echo "<h5 class='error'>L'adresse email est invalide.</h5>";
        }
        else{
            $mail = mysqli_real_escape_string($connexion, $_POST['mail']);

            $res = mysqli_query($connexion, "SELECT usrname, confirmedMail FROM Users WHERE mail='$mail';");
            if(mysqli_num_rows($res) > 0){
                $rowMail = mysqli_fetch_assoc($res);

                if($rowMail['confirmedMail']){
                    $pass = false;
                    $token = mt_rand(0, 32768);
                    do{
                        $token = bin2hex(random_bytes(16));
                        $res = mysqli_query($connexion, "SELECT usrname FROM Users WHERE resetToken='$token';");
                        if(mysqli_num_rows($res) == 0){
                            $pass = true;
                            mysqli_query($connexion, "UPDATE Users SET resetToken='$token' WHERE mail='$mail';");
                        }
                    }while(!$pass);

                    require($_SERVER['DOCUMENT_ROOT'].'/inc/send_mail.php');
        
                    // Token généré et mail valide : envoi du mail
                    if(sendMailReset($token, html_entity_decode($mail))){
                        echo "<h3>Le mail a été envoyé.</h3>";
                        echo "<h4>Pensez à vérifier les courriers indésirables si vous ne trouvez pas l'email.</h4>";
                    }
                    else{
                        echo "<h3 class='error'>Une erreur est survenue lors de l'envoi du mail.</h3>";
                    }
                }
                else{
                    echo "<h3 class='error'>Votre adresse e-mail n'est pas validée.</h3>
                          <h5>Vous ne pouvez pas demander de réinitialisation de mot de passe.</h5>
                          <h6>Une fois connecté, la validation de l'adresse e-mail peut se faire via les paramètres du compte.</h6>";
                }
            }
            else{
                echo "<h4 class='error'>Aucun compte n'est lié à l'adresse e-mail que vous avez précisé.</h4>";
            }
        }
    }
    ?>
    <h4 class='error'>Attention</h4>
    <h5>Vous devez avoir défini une adresse e-mail sur votre compte pour demander la réinitialisation.</h5>
    <div class='clearb' style='padding-bottom: 1em;'></div>
    <div class="row">
        <div class="col s12 l10 offset-l1">
            <div class='clearb'></div>
            <div class="card-new-flow" style="padding-top: .5em;">
                <form action="#" method='post'>
                    <div class="input-field">
                        <label for="mail">Adresse email</label>
                        <input type="text" id="mail" name="mail" maxlength="128" value='<?php if(isset($_POST['mail'])) echo $_POST['mail']; ?>' required>
                    </div>

                    <a href='<?= HTTPS_URL ?>login' class='card-pointer'>
                        <div class='btn btn-personal card-user-new border-user attenuation left'>
                        <i class='material-icons left'>arrow_left</i>Retour</div>
                    </a>
                    <button name='reset' class='btn btn-personal card-user-new border-user attenuation right'>
                        Demander la réinitialisation
                    </button>
                    <div class='clearb'></div>
                </form>
            </div>
        </div>
    </div>
    <?php
}