<?php

if(!(isset($_SESSION['id'], $_SESSION['connected']) && $_SESSION['connected'])){
    if(isset($GLOBALS['is_included'])) { include('php/info/403.php'); return; }
    else {header("Location:403"); exit();}
}

// Navbar
$GLOBALS['navbar']->addElement('journal', 'Journal');
$GLOBALS['navbar']->addElement('notifications', 'Notifications');

?>
<div class='row'>
    <div class='col s12 l10 offset-l1'>
        <div id="notifblock">
            <div id="notbcurrent">Chargement des notifications...</div>
        </div>
    </div>
</div>

<script>loadNotifications(true);</script>
