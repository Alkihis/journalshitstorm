<?php

// Derniers inscrits sur le site web

$GLOBALS['navbar']->addElement('last_people', 'Dernières personnes enregistrées');

if(!isModerator()) {
    include(PAGE403);
    return;
}
$GLOBALS['turn_off_container'] = true;

?>
<div class='container' style='margin-top: 20px;'>
    <div class='row'>
        <div class='col s12'>
            <div class='card-user-new border-user black-text center' 
                style='height: auto !important; margin-bottom: 15px;'>
                <span class='underline'><a href='<?= HTTPS_URL ?>usermgmt'>Vue d'ensemble</a></span>
                &bull; <span class='bold'>Derniers inscrits</span>
                &bull; <span class='underline'><a href='<?= HTTPS_URL ?>usermgmt/mod'>Gérer</a></span>
            </div>
        </div>
    </div>
</div>

<?php

echo "<div class='row container'>";
echo "<p class='flow-text'>Personnes inscrites durant les deux derniers mois</p>";
echo "</div>";

echo "<div class='row container' style='margin-bottom: 60px;'>";

global $connexion;
$dateT = new DateTime();
$dateT->sub(new DateInterval('P2M'));

$res = mysqli_query($connexion, "SELECT u.profile_img, u.realname, u.banner_img, u.usrname, u.registerdate, u.id, u.genre, u.snow, u.storm, u.moderator, u.followable
                                 FROM Users u WHERE findable=1 AND registerDate > '" . $dateT->format('Y-m-d H:i:s') . "' ORDER BY registerDate DESC LIMIT 0,20;");

while($row = mysqli_fetch_assoc($res)) {
    generateUserFlow($row, true, true);
}

echo "</div>";
