<?php

// Modifie les utilisateurs existants sur le site

$GLOBALS['navbar']->addElement('usermgmt/mod', 'Modification');

?>

<div class='row'>
    <div class='col s12'>
        <div class='card-user-new border-user black-text center' 
            style='height: auto !important; margin-bottom: 15px;'>
            <span class='underline'><a href='<?= HTTPS_URL ?>usermgmt'>Vue d'ensemble</a></span>
            &bull; <span class='underline'><a href='<?= HTTPS_URL ?>usermgmt/last_registered'>Derniers inscrits</a></span>
            &bull; <span class='bold'>Gérer</span>
        </div>
    </div>
</div>

<div class='row'>
    <div class='col s12 l10 offset-l1'>
        <div class="input-field col s12">
            <i class="material-icons prefix black-text">filter_list</i>
            <input id="filter_users" type="text">
            <label for="filter_users">Filter par nom d'utilisateur</label>
        </div>

        <div class='clearb'></div>
        <div class='divider divider-user'></div>

        <div id='user-admin-placeholder'>
            <p class='flow-text show-if-only-child'>
                Aucun utilisateur ne correspond à vos critères.
            </p>
        </div>
    </div>

    <!-- Modal utilisateur -->
    <div id="modal-user-modification" class="modal modal-fixed-footer">
        <div class='modal-content' id='container-user'>
            <h3 class='modal-username'></h3>
            <form id='modal-form' data-id=''>
                <div class='left' style='margin-top: 8px;'>
                    <input type="checkbox" class="filled-in" id="modal-confirmed-mail" checked>
                    <label for="modal-confirmed-mail">E-mail confirmé</label>
                </div>
                <button type='button' class='btn btn-personal-mini right' id='modal-confirmed-mail-button'>Modifier</button>

                <div class='clearb'></div>
                <div class='divider-user divider'></div>

                <div class='input-field col s12'>
                    <i class="material-icons prefix black-text">account_circle</i>
                    <input id="modal-realname" type="text">
                    <label for="modal-realname">Nom d'usage</label>
                </div>
                <div class='clearb'></div>

                 <div class="input-field col s12">
                    <select id='modal-gender'>
                        <option value="agender">Agenre</option>
                        <option value="genderqueer">Non binaire</option>
                        <option value="male">Masculin</option>
                        <option value="female">Féminin</option>
                    </select>
                    <label>Genre</label>
                </div>

                <div class='clearb'></div>
                
                <div class='input-field col s12'>
                    <i class="material-icons prefix black-text">contact_mail</i>
                    <input id="modal-mail" type="text">
                    <label for="modal-mail">E-mail</label>
                </div>
                <button type='button' class='btn btn-personal-mini right' id='modal-realname-button'>Modifier</button>

                <div class='clearb'></div>

                <div class='divider-user divider'></div>

                <div class='input-field col s12'>
                    <select id='modal-status'>
                        <option value="0">User</option>
                        <option value="1">Moderator</option>
                        <option value="2">Admin</option>
                    </select>
                    <label for="modal-status">Status</label>
                </div>
                <button type='button' class='btn btn-personal-mini right' id='modal-status-button'>Modifier</button>

                <div class='clearb'></div>

                <div class='divider-user divider'></div>

                <div class='input-field col s12'>
                    <i class="material-icons prefix black-text">vpn_key</i>
                    <input id="modal-password" type="password">
                    <label for="modal-password">Mot de passe</label>
                </div>
                <button type='button' class='btn btn-personal-mini right' id='modal-password-button'>Modifier</button>

                <div class='clearb'></div>
            </form>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fermer</a>
        </div>
    </div>
</div>

<style>
    .divider-user {
        margin-top: 10px;
        margin-bottom: 10px;
    }
</style>

<script src='<?= HTTPS_URL ?>js/admin/users.js'></script>
