<?php
// -------
// ARCHIVE
// -------


if(!isLogged()){
    if(isset($GLOBALS['is_included'])) { include(PAGES_DIR . 'info/403.php'); return; }
    else {header("Location: /403"); exit();}
}
global $connexion;

?>

<style>
    header, main, footer {
      padding-left: 300px;
    }

    @media only screen and (max-width : 992px) {
      header, main, footer {
        padding-left: 0;
      }
    }
</style>

<div class='clearb'></div>
<script src='<?= HTTPS_URL ?>js/messages.js'></script>

<ul id="slide-out" class="side-nav fixed">
    <li>
        <div class="user-view">
            <div class="background default-color"></div>
            <a href="<?= HTTPS_URL ?>profil/<?= $_SESSION['username'] ?>"><img class="circle side-nav-pp" src="<?= $_SESSION['profile_img'] ?>"></a>
            <a href="<?= HTTPS_URL ?>profil/<?= $_SESSION['username'] ?>"><span class="name-side-nav name"><?= htmlspecialchars($_SESSION['realname']) ?></span></a>
        </div>
    </li> 
    <li><a class='grey-text disabled nav-header' href='#!'>Messages</a></li>
    <li <?= ($acc_page ===  3 ? 'class="active"' : '') ?>><a href="<?= HTTPS_URL ?>compte/profile">Modification du profil</a></li>
    <li <?= ($acc_page ===  4 ? 'class="active"' : '') ?>><a href="<?= HTTPS_URL ?>compte/mysubs">Mes soumissions</a></li>
    <li><div class='divider' style='margin-bottom: .5em'></div></li>
    <li <?= ($acc_page ===  0 ? 'class="active"' : '') ?>><a href="<?= HTTPS_URL ?>compte/content">Propriétés du compte</a></li>
    <li <?= ($acc_page ===  1 ? 'class="active"' : '') ?>><a href="<?= HTTPS_URL ?>compte/password">Mot de passe</a></li>
    <li <?= ($acc_page ===  2 ? 'class="active"' : '') ?>><a href="<?= HTTPS_URL ?>compte/visual">Options visuelles</a></li>
    <li><div class='divider' style='margin-bottom: .5em'></div></li>
    <li <?= ($acc_page ===  5 ? 'class="active"' : '') ?>><a href="<?= HTTPS_URL ?>compte/others">Autres</a></li>
</ul>