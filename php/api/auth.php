<h3>Authentification</h3>

<p class='flow-text'>
    La création de token fonctionne de la même manière que celle de Twitter.<br>
    Deux endpoints et deux redirections au total sont nécessaires pour obtenir un access token final.<br>
    La réponse est faite au format JSON.
</p>

<div class='divider divider-margin'></div>

<p class='flow-text'>
    POST https://api.shitstorm.fr/v2/auth/request_token.json
</p>

<h6>Paramètres nécessaires</h6>
<p>
    app_token: &lt;&lt; Votre token d'application disponible sur <a href='https://shitstorm.fr/apps'>shitstorm.fr/apps</a> &gt;&gt;,<br>
    callback: &lt;&lt; L'URL où l'utilisateur sera renvoyé quand il acceptera les autorisations demandées par votre application &gt;&gt;
</p>

<h6>Réponse</h6>
<p>
    request_token: &lt;&lt; Token généré &gt;&gt;,<br>
    callback_url: &lt;&lt; L'URL à intégrer dans un &lt;a&gt; sur votre site web &gt;&gt;,<br>
    expires: &lt;&lt; Timestamp d'expiration du token &gt;&gt;
</p>

<div class='divider divider-margin'></div>

<p class='flow-text'>
    POST https://api.shitstorm.fr/v2/auth/access_token.json
</p>

<h6>Paramètres nécessaires</h6>
<p>
    app_token: &lt;&lt; Votre token d'application disponible sur <a href='https://shitstorm.fr/apps'>shitstorm.fr/apps</a> &gt;&gt;,<br>
    verifier: &lt;&lt; Le token verifier renvoyé en paramètre GET à votre callback, lorsque l'utilisateur a accepté les autorisations &gt;&gt;<br>
</p>

<h6>Réponse</h6>
<p>
    access_token: &lt;&lt; Token généré, à utiliser dans vos requêtes à l'API en tant que Bearer Token &gt;&gt;,<br>
    user_id: &lt;&lt; L'ID utilisateur de l'utilisateur lié au token &gt;&gt;
</p>

<div class='divider divider-margin'></div>

<h4 style='margin-bottom: 30px;'>Schéma récapitulatif</h4>
<a class='fancy-boxed' href='<?= HTTPS_URL ?>php/api/auth/auth_flow.png' style='
    margin: 0 1vw;
'>
    <img class='responsive-img' style='border-radius: 8px;' src='<?= HTTPS_URL ?>php/api/auth/auth_flow.png'>
</a>
