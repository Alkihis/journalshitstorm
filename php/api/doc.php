<?php

function nullFalseText($value) : string {
    if($value === null) {
        return 'null';
    }
    else if($value === false) {
        return 'false';
    }
    else if($value === true) {
        return 'true';
    }
    else {
        return $value;
    }
}

// Récupération du root

$folder = PAGES_DIR . 'api/';

$root = json_decode(file_get_contents($folder . 'root.json'), true);

$GLOBALS['navbar']->addElement('api_documentation', "Documentation");

if($GLOBALS['options'] === 'auth') {
    $GLOBALS['navbar']->addElement('api_documentation/auth', "Authentification");

    include(PAGES_DIR . 'api/auth.php');
}
else if(isset($_GET['doc'])) {
    $element = $_GET['doc'];

    $GLOBALS['navbar']->addElement('api_documentation?doc=' . htmlspecialchars($_GET['doc'], ENT_QUOTES), $_GET['doc']);

    if(isset($root[$_GET['doc']])) {
        $endpoints_dir = $folder . '/' . $_GET['doc'];

        $files = glob($endpoints_dir . '/*.json');

        foreach($files as $f) {
            $current = json_decode(file_get_contents($f), true);
            $filen = basename($f);
            echo "<h3><a href='".HTTPS_URL."api_documentation?source={$_GET['doc']}/$filen'>{$current['name']}</a></h3>";
            echo "<h5>{$current['description']}</h5>";
            echo "<div class='divider' style='margin: 30px 0;'></div>";
        }
    }
}
else if(isset($_GET['source']) && is_string($_GET['source']) && strpos($_GET['source'], '..') === false && file_exists(PAGES_DIR . 'api/'. $_GET['source'])) {
    $current = json_decode(file_get_contents(PAGES_DIR . 'api/'. $_GET['source']), true);

    $first = explode('/', $_GET['source']);

    $GLOBALS['navbar']->addElement('api_documentation?doc=' . htmlspecialchars($first[0], ENT_QUOTES), $first[0]);
    $GLOBALS['navbar']->addElement('api_documentation?source=' . htmlspecialchars($first[1], ENT_QUOTES), $first[1]);
    ?>

    <h3><?= $current['real_name'] ?></h3>
    <h5><?= $current['name'] ?></h5>

    <p class='flow-text'>
        <?= $current['description'] ?>
    </p>

    <?= (isset($current['additionnals']) ? "<p class='flow-text'>
            {$current['additionnals']}
        </p>" : '') ?>

    <?= (!isset($current['not_endpoint']) ? "<div class='divider divider-margin'></div>" : '') ?>

    <p>
        <?php 
        if(!isset($current['not_endpoint'])) {
            $v = function() { 
                global $current;
    
                switch($current['auth_level']) {
                    case 0:
                        return "Aucune authentification requise";
                    case 1:
                    case 2:
                        return "Connexion requise";
                    case 3:
                        return "Connexion et permissions modérateur requises";
                    case 4:
                        return "Connexion et permissions administrateur requises";
                }
            };
    
            echo $v();
    
            echo "<br>Nombre de requêtes maximum par utilisateur (fenêtre de " . ($current['expiration'] ?? 15) . " minutes) : " . $current['rate_limit'] ?? "Illimité";
            echo "<br>";
            echo "Droits en lecture" . ($current['rights'] > 0 ? ' et écriture' : '') . ' requis'; 
        }
        ?>
    </p>
    
    <?php if (isset($current['url'])) { ?>

    <div class='divider divider-margin'></div>
    <h6><?= $current['method'] ?> <?= HTTPS_URL . $current['url'] ?></h6>

    <?php } ?>

    <div class='divider divider-margin'></div>

    <p class='flow-text'>
        <?= (!isset($current['not_endpoint']) ? "Arguments disponibles" : 'Représentation') ?>
    </p>

    <?php if(count($current['arguments']) === 0) {
        echo "<p>Aucun</p>";
    }
    else { ?>
        <table>
            <thead>
                <tr>
                    <th>Nom</th>
                    <th>Description</th>
                    <?php if(!isset($current['not_endpoint'])) { echo "<th>Valeur par défaut</th>"; } ?>
                    <th>Type</th>
                </tr>
            </thead>

            <tbody>
                <?php 
                foreach($current['arguments'] as $k => $arg) {
                    echo '<tr>';

                        echo "<td>$k</td>";
                        echo "<td>{$arg['text']}</td>";
                        if(!isset($current['not_endpoint'])) { echo "<td>". nullFalseText($arg['default']) ."</td>"; }
                        echo "<td>{$arg['type']}</td>";

                    echo '</tr>';
                }
                ?>
            </tbody>
        </table>
    <?php } ?>

    <div class='divider' style='margin: 30px 0; clear: both;'></div>

    <p class='flow-text'>
        Exemple de réponse
    </p>

    <pre class='example-request' id='json'><?= $current['response'] ?? '{}' ?></pre>
    <?php
}
else {
    echo '<ul class="flow-text">';

    foreach($root as $key => $element) {
        echo "<li><a href='?doc=$key'>{$key} ({$element['description']})</a></li>";
    }
    echo '</ul>'; ?>

    <div class='flow-text center' style='margin: 50px 0;'>
        <a href='<?= HTTPS_URL ?>api_documentation/auth'>Comment s'identifier pour utiliser l'API ?</a>
    </div> <?php
}

?>

<style>
    .example-request {
        margin: 0 1vw;
        margin-bottom: 50px;
        height: fit-content;
        border: 1px #010 solid;
        border-radius: 7px;
        padding: 10px;
    }
</style>

<script>
    $(document).ready(function() {
        let j = document.getElementById('json');
        if(j) {
            j.innerText = JSON.stringify(JSON.parse(j.innerText), null, 2);
        }
    });
</script>
