<?php 
// ----------------------
// JOURNAL DES SHITSTORMS
// ----------------------

require_once(ROOT . '/inc/journal_func.php');

$GLOBALS['add_meta_description'] = 'Journal des Shitstorms';

global $connexion;

// Initialisation des paramètres
$isPerNote = false;
$isPerYear = false;
$dateBase = date('Y-m');
$dateNote = null;
$dateMax = $dateBase;
$should_load_all = false;
$month = null;
$year = null;
$hidden_nav = false;

$choosenDate = $GLOBALS['options']; // La date est contenue dans la première partie des arguments

$GLOBALS['navbar']->addElement('journal', 'Journal');

if($choosenDate === 'all') {
    $should_load_all = true;
    $hidden_nav = true;

    $params = [];
    $params['prec_month'] = null;
    $params['prec_year'] = null;
    $params['next_month'] = null;
    $params['next_year'] = null;

    $month = getTextualMonth(date('m'));
    $year = date('Y');
    $real_month = date('m');
    generateNextPreviousByMonth($params, $month, $year, $isPerYear, $hidden_nav);

    $withJS = true;
    $finalDate = 'all';
} 
else {
    // Vérification que la date est valide
    $expl = explode('-', $choosenDate);
    if(count($expl) > 1 && $GLOBALS['additionnals'] !== 'force_year'){
        if(is_numeric($expl[0]) && is_numeric($expl[1])){ // Date valide
            $choosenDate = $expl[0] . '-' . $expl[1];

            try {
                $dateT = new DateTime($choosenDate);
        
                $actual = new DateTime($dateBase);
                if($dateT > $actual){
                    echo "<h5 class='error'>La date choisie est supérieure à la date actuelle.</h5>";
                    $dateBase = $actual->format('Y-m');
                }
                else{
                    $dateBase = $dateT->format('Y-m');
                }
            } catch (Exception $E) {
                echo "<h5 class='error'>La date choisie est invalide.</h5>";
                $choosenDate = $dateBase;
            }
        }
        else{
            // Date non valide
            $choosenDate = date('Y-m');
        }
    }
    else if (is_numeric($expl[0])) {
        $choosenDate = $expl[0];
        $isPerYear = true;

        $act = date('Y');
        if($choosenDate > $act){
            $choosenDate = $act;
        }
    }
    else {
        // Date non valide
        $choosenDate = date('Y-m');
    }

    $currentYear = explode("-", $dateBase);;
    $currentMonth = intval($currentYear[1]);
    $currentYear = intval($currentYear[0]);
    $withJS = false;

    // --------------------------
    // Affichage normal, par mois
    // --------------------------
    $dateBase = $choosenDate;

    $params = getPastLastLegitMonths($dateBase, $dateMax, $isPerYear);

    // Récupération mois et année choisie
    $explodedDate = explode("-", $dateBase);
    $month = 'Janvier';
    if(!$isPerYear){
        if(count($explodedDate) > 1) {
            $month = getTextualMonth($explodedDate[1]);
        }
        else {
            $isPerYear = true;
        }
    }
    $year = $explodedDate[0];
    $real_month = ($explodedDate[1] ?? 1);

    // Boutons précédent / Suivant
    generateNextPreviousByMonth($params, $month, $year, $isPerYear);

    $dateNext = $dateBase;
    $finalDate = $dateBase;

    if($isPerYear){
        $dateBase = $year.'-01';
        $dateNext = $year.'-12';
        $finalDate = $year;
    }

    $withJS = !(isset($_GET['force_php']) && isActive($_GET['force_php']));
}


// Récupération des shitstorms du mois choisi
// ------------------------------------------
$countShit = 0;
if($should_load_all) {
    $res = mysqli_query($connexion, "SELECT COUNT(*) c FROM Shitstorms;");

    if($res && mysqli_num_rows($res) > 0){
        $coS = mysqli_fetch_assoc($res);
        $countShit = $coS['c'];
    }
}
else if($withJS){
    $res = mysqli_query($connexion, "SELECT COUNT(*) c FROM Shitstorms WHERE dateShit BETWEEN '$dateBase-01' AND '$dateNext-31';");

    if($res && mysqli_num_rows($res) > 0){
        $coS = mysqli_fetch_assoc($res);
        $countShit = $coS['c'];
    }
}
else{
    $res = mysqli_query($connexion, "SELECT * FROM Shitstorms WHERE dateShit BETWEEN '$dateBase-01' AND '$dateNext-31' ORDER BY dateShit DESC;");
    $countShit = mysqli_num_rows($res);
}

if($countShit == 0){
    echo "<div class='row'>";
    echo "<h4 id='shit_number'>Aucune shitstorm ". ($should_load_all ? 'au total' : ($isPerYear ? "cette année" : "ce mois-ci")) .".</h4>";
    echo "</div><div class='row'>";

    $GLOBALS['add_meta_description'] = 'Aucune shitstorm n\'est disponible pour ' . ($isPerYear ? $year : "$real_month/$year").
                                        ". Retrouvez de nombreuses shitstorms dans le Journal des Shitstorms";

    // Sélécteur de mois
    generateMonthSelector($withJS, $isPerNote, $real_month, $year, $isPerYear, $should_load_all);

    echo "<div id='shitmainblock'></div></div>";

    generateNextPreviousByMonth($params, $month, $year, $isPerYear, $hidden_nav);
}
else{
    echo "<div class='row'>";
    if($countShit <= 1) echo "<h4 id='shit_number'>Une shitstorm ". ($should_load_all ? 'au total' : ($isPerYear ? "cette année" : "ce mois-ci")) .".</h4>";
    else echo "<h4 id='shit_number'>". $countShit . " shitstorms ". ($should_load_all ? 'au total' : ($isPerYear ? "cette année" : "ce mois-ci")) .".</h4>";
    echo "</div>";

    $GLOBALS['add_meta_description'] = "$countShit shitstorm". ($countShit > 1 ? 's' : '') 
                                        ." disponible". ($countShit > 1 ? 's' : '') ." pour " . ($isPerYear ? $year : "$real_month/$year") .
                                        ". Retrouvez de nombreuses shitstorms dans le Journal des Shitstorms";

    // Sélécteur de mois
    generateMonthSelector($withJS, $isPerNote, $real_month, $year, $isPerYear, $should_load_all);

    if($withJS){
        ?> 
        <div class='row'><div id='shitmainblock'></div></div>
        <script>
            var load_shitstorm_until_id = <?= (isset($_GET['from']) && is_numeric($_GET['from']) ? $_GET['from'] : 'null') ?>;
            $(document).ready(() => {
                generateJournalShits('shitmainblock', '<?= $finalDate ?>', <?= (int)$isPerYear ?>, 'date', 'desc', '<?= ($isPerYear ? 'year' : 'month') ?>');
            });
        </script>
    <?php }

    else{
        $pastWeek = NULL;
        while($row = mysqli_fetch_assoc($res)){
            $dateActuelle = $row['dateShit'];
            $week = (int)getWeekNumber($dateActuelle);

            if($week != $pastWeek && $week != NULL){ // Il faut afficher la semaine
                $pastWeek = $week;
                echo "<div class='row'><div class='col s12'>";
                echo "<h4 id='week$week' class='col offset-l1 numWeek'>Semaine $week</h4>";
                echo "</div></div>";
            }

            if(!generateShitstormCardFromMysqliRow($row))
                break;

        }
    }

    // Boutons précédent / Suivant
    generateNextPreviousByMonth($params, $month, $year, $isPerYear, $hidden_nav);
    echo '<div style="padding-bottom: 15px;"></div>';
}
    
?>

<!-- Bouton d'ajout de shitstorm -->
<div class="fixed-action-btn hide-on-med-and-up">
    <a id='add_shitstorm_btn' class="btn-floating btn-large red waves-effect waves-light hoverable" href="<?= HTTPS_URL ?>soumission">
        <span style="font-size: 2em;">+</span>
    </a>
</div>

<script src="<?= HTTPS_URL ?>js/journal.js"></script>
