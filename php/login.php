<?php
// -----------------
// PAGE DE CONNEXION
// -----------------

$GLOBALS['navbar']->addElement('login', 'Connexion');

$GLOBALS['add_meta_description'] = 'Connectez-vous au Journal des Shitstorms et accédez à votre compte.';

if(!$GLOBALS['settings']['allowLogin'] || !$GLOBALS['settings']['allowStayLogged']){
    echo "<h3 class='error'>Il est actuellement impossible de se connecter.</h3>";
    return;
}

global $connect;

$login_text = '';

if($connect > 0){
    if($connect == 1){
        if(isset($_REQUEST['redirect']) && is_string($_REQUEST['redirect'])) {
            $redirect = HTTPS_URL . $_REQUEST['redirect'];

            header("Location: ".$redirect);
            return;
        }
        header("Location: /");
        return;
    }
    elseif($connect == 2){
        $login_text .= "<h3 class='error'>Mot de passe incorrect.</h3>";
    }
    elseif($connect == 3){
        $login_text .= "<h3 class='error'>Identifiant incorrect.</h3>";
    }
    elseif($connect == 4){
        $login_text .= "<h3 class='error'>Impossible de se connecter : La connexion est désactivée pour les utilisateurs.</h3>";
    }
    elseif($connect == 5){
        $login_text .= "<h3 class='error'>Impossible de se connecter : La connexion est désactivée.</h3>";
    }
}

if(isset($_SESSION['connected']) && $_SESSION['connected']){
    $login_text .= "<h3>Vous êtes déjà connecté.</h3>";
    return;
}

$url_to_give_to_user = null;
$request_tokens = null;
// Bouton connexion via Twitter
if(class_exists('OAuth')) {
    try {
        $twitter_link = new OAuth(CONSUMER_KEY, CONSUMER_SECRET, OAUTH_SIG_METHOD_HMACSHA1, OAUTH_AUTH_TYPE_URI);
    
        $request_tokens = $twitter_link->getRequestToken('https://api.twitter.com/oauth/request_token', 'https://shitstorm.fr/callback_twitter', 'POST');
    } catch (OAuthException $e) { }
    
    if($request_tokens) {
        // Save tokens
        $_SESSION['tokens']['request'] = $request_tokens['oauth_token'];
        $_SESSION['tokens']['request_secret'] = $request_tokens['oauth_token_secret'];
    
        // Utilisation d'authentificate plutôt qu'authorize pour éviter d'avoir à réapprouver l'appli à chaque connexion
        $url_to_give_to_user = 'https://api.twitter.com/oauth/authenticate?oauth_token=' . $request_tokens['oauth_token'];
    }
}

$GLOBALS['turn_off_container'] = true;
?>
<div class='background-full login-image'>
</div>
<div class="row">
    <div class='col s12 l10 offset-l1'>
        <div class="col s12 l6">
            <?= ($login_text ? $login_text : '') ?>
            <div class="card-user-new border-user" style='height: auto !important; margin-top: 30px;'>
                <form action="#" method='post' id='login_form'>
                    <div class="input-field">
                        <label for="usrname">Nom d'utilisateur</label>
                        <input type="text" placeholder="Nom d'utilisateur" id="usrname" name="usrname" <?= (isset($_POST['usrname']) ? "value='". htmlspecialchars($_POST['usrname'], ENT_QUOTES) ."'" : '') ?> required>
                    </div>
                    <div class="input-field">
                        <label for="psw">Mot de passe</label>
                        <input type="password" placeholder="Mot de passe" id="psw" name="psw" title="Doit contenir au moins 8 caractères, un chiffre, une lettre minuscule et majuscule." required>
                    </div>
                
                    <a href='<?= HTTPS_URL ?>passwordreset'>Mot de passe oublié</a>
                    <div class='right'>
                        <input name="remember_me" value="stay_logged" type="checkbox" class="filled-in" id="remember_me" checked>
                        <label for="remember_me">Rester connecté</label>
                    </div>

                    <div class='clearb' style='padding-bottom : 1em;'></div>

                    <a class='card-pointer' style='display: block; font-size: 1.2rem;' href='<?= HTTPS_URL ?>createaccount'>
                        <div class='btn card-user-new border-user attenuation left btn-personal'>
                            Créer un compte 
                        </div>
                    </a>
                    <button name='confirm' class='btn card-user-new border-user attenuation right btn-personal'>
                        Connexion
                    </button>

                    <?php if(isset($_REQUEST['redirect']) && is_string($_REQUEST['redirect'])) { ?>
                        <input name='redirect' value='<?= htmlspecialchars($_REQUEST['redirect'], ENT_QUOTES) ?>' type='hidden'>
                    <?php } ?>

                    <div class='clearb' style='padding-bottom : .5em;'></div>
                </form>
            </div>

            <?php if($url_to_give_to_user) { ?>
                <div class='col s12 hide-on-med-and-down' style='padding: 0 !important'>
                    <a title='Se connecter avec Twitter' href='<?= $url_to_give_to_user ?>' class='btn left card-user-new border-user attenuation-twitter btn-personal' style='width: 100% !important;'>
                        <img alt='Twitter login button' src='<?= HTTPS_URL ?>img/sign-in-with-twitter.png' style='height: auto; vertical-align: sub;'>
                    </a>
                </div>
            <?php } ?>
        </div>

        <?php if($url_to_give_to_user) { ?>
            <div class='col s12 hide-on-large-only'>
                <a title='Se connecter avec Twitter' href='<?= $url_to_give_to_user ?>' class='btn left card-user-new border-user attenuation-twitter btn-personal' style='width: 100% !important;'>
                    <img alt='Twitter login button' src='<?= HTTPS_URL ?>img/sign-in-with-twitter.png' style='height: auto; vertical-align: sub;'>
                </a>
            </div>
        <?php } ?>

        <div class="col s12 l6">
            <p class='flow-text black-text' style='text-align: justify; text-justify: inter-word; padding: 0 25px;'>
                Notez, aimez, commentez, gérez vos publications !<br>
                Avec votre compte, vous pourrez interagir avec les autres utilisateurs, et donner votre avis en laissant des commentaires,
                tout en notant les dramas qui parcourent la twittosphère.<br>
                En partageant vos propres trouvailles, vous pourrez à loisir y ajouter des liens si la shitstorm s'envenime,
                ou proposer des réponses des initiateurs de la polémique sous des publications.<br>
            </p>
        </div>
    </div>
</div>
