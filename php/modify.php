<?php
// ---------------------------
// PAGE D'EDITION DE SHITSTORM
// ---------------------------

if(!isset($_SESSION['id']) || !isset($_SESSION['mod'])){
    if(isset($GLOBALS['is_included'])) { include(PAGE403); return; }
    else {header("Location: /403"); exit();}
}

if(!$GLOBALS['settings']['allowUserModification'] || !$GLOBALS['settings']['allowAllModification']){ // On a pas le droit de modifier
    if(!$GLOBALS['settings']['allowAllModification']){ // Personne n'a le droit de modifier
        echo "<h3 class='error'>Il est actuellement impossible de modifier une shitstorm.</h3>";
        return;
    }
    else if(!$GLOBALS['settings']['allowUserModification'] && !isModerator()) { // Les utilisateurs n'ont pas le droit de modifier et l'utilisateur n'est pas modérateur
        echo "<h3 class='error'>Il est actuellement impossible de modifier une shitstorm.</h3><h5>Vous pouvez demander de l'aide à un modérateur pour plus d'informations.</h5>";
        return;
    }
}

global $connexion;

require('inc/postDelete.php');

$idSub = null;
// Récupération de l'ID
if(isset($_GET['idPubli'])){
    $idSub = (int)mysqli_real_escape_string($connexion, $_GET['idPubli']);
}
else{
    $idSub = (int)($GLOBALS['options'] ?? 0);
}

if($idSub <= 0){
    echo "<h3 class='error'>L'ID est incorrect.</h3>";
    return;
}

$isWait = false;
$toCheck = false;
$res = null;
// Est-ce une réponse en attente ?
if(isset($_GET['waiting']) && ($_GET['waiting'] === '1' || $_GET['waiting'] === 't' || $_GET['waiting'] === 'true')){
    $isWait = true;

    if(!(isset($_SESSION['mod']) && $_SESSION['mod'] > 0)){
        $toCheck = true;
    }
}
$ShitstormTable = ($isWait ? "Waiting" : "Shitstorms");
$LinksTable = ($isWait ? "WaitingLinks" : "Links");

$res = mysqli_query($connexion, "SELECT * FROM $ShitstormTable WHERE idSub='$idSub' ".($toCheck ? ' AND locked=0' : '').";");
$row = NULL;
if($res && mysqli_num_rows($res) > 0)
    $row = mysqli_fetch_assoc($res);

if (!$res || @mysqli_num_rows($res) == 0) {
    echo "<h4>Aucune shitstorm correspondant à l'identifiant n'a été trouvée.</h4>";
} 
elseif (isset($_GET['delete']) && ($_SESSION['mod'] > 0 || $_SESSION['id'] == $row['idUsr']) && isset($_COOKIE['token'])) {
    if(md5($_COOKIE['token'] . $idSub) == $_GET['delete']){
        if(!$isWait){
            $res2 = deleteShitstorm($idSub);
        }
        else{
            deleteWaitingShitstorm($idSub);
            // Supprime les possibles réponses en attente associées à la shitstorm
        }

        echo "<h4 class='error'>La shitstorm a été supprimée.</h4>";
        return;
        exit();
    }
} 
else {
    if($_SESSION['mod'] == 0 && $_SESSION['id'] != $row['idUsr']){
        include(PAGE403); return;
    }
    else{ // Droits autorisés
        $resLink = mysqli_query($connexion, "SELECT * FROM $LinksTable WHERE idSub='$idSub';");

        if(!$resLink || @mysqli_num_rows($resLink) == 0){
            echo "<h4>Aucune lien correspondant à l'identifiant n'a été trouvée. Rechargez la page ou supprimez la shitstorm.</h4>";
            exit();
        }

        $tabLink = array();
        while($rowLink = mysqli_fetch_assoc($resLink)){
            $tabLink[] = $rowLink;
        }

        $nbLink = count($tabLink);
        $hasImage = false;
        
        // TRAITEMENT DES POSTS
        // --------------------
        $respost = NULL;

        // Définir comme lien principal (échanger lien 1 et autre)
        if(isset($_GET['makeSup'])){
            $idLink = mysqli_real_escape_string($connexion, htmlentities($_GET['makeSup']));
            $primaryId = $tabLink[0]['idLink']; // ID du premier lien de la SS
            $linkPrimaryActual = $tabLink[0]['link']; // Lien du premier lien de la SS

            $keyNewPrimary = 0;
            $currentIdLink = $primaryId;
            // recherche de la position
            foreach($tabLink as $key => $l){
                if($l['idLink'] == $idLink){ // Si on trouve idLink dans le tableau
                    $keyNewPrimary = $key;
                    $currentIdLink = $idLink;
                    break;
                }
            }
            $futurePrincLink = $tabLink[$keyNewPrimary]['link'];

            invertTwoSavedLinks($primaryId, $currentIdLink);

            mysqli_query($connexion, "UPDATE $LinksTable SET link='$futurePrincLink' WHERE idLink='$primaryId';");
            $respost = mysqli_query($connexion, "UPDATE $LinksTable SET link='$linkPrimaryActual' WHERE idLink='$currentIdLink';");

            if($LinksTable === 'Links') { // Si on travaille avec des liens non "waiting", on actualise le ID Tweet
                autoDetectLinkTweetId($primaryId);
                autoDetectLinkTweetId($currentIdLink);
            }
        }

        // Description et date
        if(isset($_POST['savedscr'], $_POST['title'], $_POST['dateShit'], $_POST['dscr']) && ($_SESSION['mod'] > 0 || AUTHORIZE_CONTENT_MODIFICATION)){
            $pass = false;
            $title = mysqli_real_escape_string($connexion, trim($_POST['title']));
            $date = mysqli_real_escape_string($connexion, $_POST['dateShit']);
            $dscr = trim(mysqli_real_escape_string($connexion, $_POST['dscr']));

            $dateApp = date('Y-m-d H:i:s');
            try{
                $dateT = new DateTime($date);
                $actual = new DateTime($dateApp);
    
                if($dateT <= $actual){
                    $pass = true;
                }
                else{
                    echo "<h5 class='error'>La date est invalide.</h5>";
                }
            } catch (Exception $E){
                echo "<h5 class='error'>La date est invalide.</h5>";
            }

            if(mb_strlen($dscr) > MAX_LEN_DSCR){
                echo "<h5 class='error'>La description est trop longue.</h5>";
                $pass = false;
            }
            if(mb_strlen($title) > MAX_LEN_TITLE){
                echo "<h5 class='error'>Le titre est trop longue.</h5>";
                $pass = false;
            }

            if($title != mysqli_real_escape_string($connexion, $row['title'])){
                // Si le titre rentré est différent du titre enregistré
                // Vérif si le title est pris
                $resT = mysqli_query($connexion, "SELECT idSub FROM Shitstorms WHERE title='$title';");

                if($resT && mysqli_num_rows($resT)){
                    $pass = false;
                    echo "<h5 class='error'>Le titre est déjà utilisé pour une autre shitstorm.</h5>";
                }
            }
            

            if($pass){
                // Mise à jour de la BDD
                if($row['idUsr'] == $_SESSION['id'] || $_SESSION['mod'] > 0){ // Si la ss choisie correspond bien à une ss postée par l'user connecté ou alors modérateur
                    $respost = mysqli_query($connexion, "UPDATE $ShitstormTable SET title='$title', dateShit='$date', dscr='$dscr' WHERE idSub='$idSub';");
                }
            }
        }

        // Ajout d'un lien
        if(isset($_POST['savelink'])){
            $newlink = trim(mysqli_real_escape_string($connexion, $_POST['newlink']));
            $pass = true;

            if(!preg_match(REGEX_TWITTER_LINK, $newlink) && !preg_match(REGEX_MASTODON_LINK, $newlink)){
                if(preg_match(REGEX_IMG_LINK_UPLOADED, $newlink) && file_exists($_SERVER['DOCUMENT_ROOT'].'/img/uploaded/'.$newlink)){
                    // Ok
                }
                else{
                    echo "<h5 class='error'>Le lien est invalide.</h5>";
                    $pass = false;
                }
            }
            
            
            if($pass){ // Si il y a bien moins de MAX_LINK_COUNT liens
                if($nbLink < MAX_LINK_COUNT){
                    if($row['idUsr'] == $_SESSION['id'] || $_SESSION['mod'] > 0){ // Si la ss choisie correspond bien à une ss postée par l'user connecté ou alors modérateur
                        if(!$isWait){
                           $respost = postLink($idSub, $newlink); 
                        }
                        else{
                            $respost = mysqli_query($connexion, "INSERT INTO WaitingLinks (link, idSub) VALUES ('$newlink', '$idSub');");
                        }
                    }
                }
                else{
                    echo "<h5 class='error'>Il y a déjà $nbLink liens.</h5>";
                }
            }
        }

        // Ajout d'une image
        elseif(isset($_POST['saveimg'])){
            if($nbLink < MAX_LINK_COUNT){
                $nom = 1;
                if(isset($_FILES['img1']) && !empty($_FILES['img1']['name'])){
                    $nom = traitementFichier();
                }
                if(!$nom || $nom == 1){
                    echo "<h5 class='error'>Le fichier est invalide.</h5>";
                }
                else{
                    if(!$isWait){
                        $respost = postLink($idSub, $nom); 
                    }
                    else{
                        $respost = mysqli_query($connexion, "INSERT INTO WaitingLinks (link, idSub) VALUES ('$nom', '$idSub');");
                    }
                }
            }
            else{
                echo "<h5 class='error'>Il y a déjà $nbLink liens.</h5>";
            }
        }

        if($respost){ // Tout le traitement est à refaire
            $res = mysqli_query($connexion, "SELECT * FROM $ShitstormTable WHERE idSub='$idSub';");
            $row = mysqli_fetch_assoc($res);
            $resLink = mysqli_query($connexion, "SELECT * FROM $LinksTable WHERE idSub='$idSub';");

            $tabLink = array();
            while($rowLink = mysqli_fetch_assoc($resLink)){
                $tabLink[] = $rowLink;
            }

            $nbLink = count($tabLink);
        }
        elseif($respost === false){
            echo "<h4 class='error'>Une erreur de traitement est survenue.</h4>";
        }

        // Obtention de la date de la shitstorm
        $year = explode("-", $row['dateShit']);
        $month = getTextualMonth($year[1]);
        $day = $year[2];
        $year = $year[0];
        
        // Navbar
        $GLOBALS['navbar']->addElement('journal?date='.$row['dateShit'], $month." $year");
        $GLOBALS['navbar']->addElement(($isWait ? '#!' : 'shitstorm/'.$row['idSub']), htmlspecialchars($row['title']));
        $GLOBALS['navbar']->addElement('modify/'.$row['idSub'].($isWait ? '?waiting=t' : ''), 'Modification');

        // echo "<h2>Éditer une shitstorm</h2>";
        ?>
        <script src='<?= HTTPS_URL ?>js/modify.js'></script>
        <div class="row">
            <div class="col s12 l10 offset-l1">
                <!-- Métadonnées (date, titre et description) -->
                <ul class="collapsible" data-collapsible="accordion">
                    <li>
                        <div class="collapsible-header active hoverable"><i class="material-icons">format_quote</i>Propriétés de la shitstorm</div>
                        <div class="collapsible-body collap-body-custom">
                        <?php if($_SESSION['mod'] > 0 || AUTHORIZE_CONTENT_MODIFICATION){ // Description et lien ?>
                            <form method='post' action='#'>
                                <div class="input-field">
                                    <input type='text' value='<?= htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8')?>' name='title' id='title' required maxlength='<?= MAX_LEN_TITLE ?>'>
                                    <label for="title">Titre de la shitstorm</label>
                                </div>
                                <div class="input-field">
                                    <textarea name="dscr" rows="7" cols="70" placeholder="Description de la shitstorm"
                                            maxlength="<?= strval(MAX_LEN_DSCR) ?>" class="materialize-textarea" required><?php echo htmlentities($row['dscr']); ?></textarea>
                                    <label for="dscr">Description de la shitstorm</label>
                                </div>
                                <label for="date">Date de la shitstorm</label>
                                <input id="date" type="date" name="dateShit" max=<?php echo date('Y-m-d'); ?> value='<?= htmlentities($row['dateShit']); ?>'>
                                <button type="submit" name='savedscr' class="btn purple lighten-1 btn-personal-mini right" style="margin-bottom: 1em;">Modifier</button>
                            </form>
                            <div class='clearb'></div>
                        <?php } else { ?>
                            <p style='margin-bottom: 0em;'><?= htmlspecialchars($row['dscr'], ENT_QUOTES, 'UTF-8') ?></p>
                        <?php } ?>
                        </div>
                    </li>
                </ul>

                <!-- Liens -->
                <ul class="collapsible" id='link_collapsible' data-collapsible="expandable">
                    <?php
                    foreach($tabLink as $key => $l){
                        echo "
                        <li id='linkID{$l['idLink']}'>";
                            echo "<div class='collapsible-header hoverable'><i class='material-icons'>insert_link</i>Lien ".($key+1)."</div>"; ?>
                            <div class="collapsible-body collap-body-custom">
                                <?php viewLink($l['link'], $key, ($isWait ? null : $l['idLink'])); ?>
                                <div class='clearb' style='margin-bottom: 2em;'></div>
                                <?php if($key != 0){
                                    echo "<a class='notMargin' href='".HTTPS_URL."modify/$idSub?makeSup={$l['idLink']}".($isWait ? '&waiting=t' : '')."'>
                                    <button class='btn col s4 waves-effect waves-light teal darken-1 btn-personal-mini' style='margin-bottom: 0.7em; clear: both;' type='submit' name='makeSup'>
                                    Définir lien principal</button></a>";
                                }
                                echo "<a href='#!' onclick='createDeleteLinkModal(\"modalphbottom\", $idSub, \"{$l['idLink']}\", ".(int)$isWait.")' style='margin-right: 2em;' class='btn-floating red right'>
                                    <i class='material-icons'>delete_forever</i>
                                </a>";
                                echo "<div class='clearb'></div>
                            </div>
                        </li>";
                    }
                    ?>
                </ul>

                <!-- Ajout de lien ou d'image -->
                <ul class="collapsible" data-collapsible="accordion" id='add_link' style='display: <?= ($nbLink < MAX_LINK_COUNT ? 'block' : 'none') ?>;'>
                    <li>
                        <div class="collapsible-header hoverable"><i class="material-icons">add_circle</i>Ajouter un lien</div>
                        <div class="collapsible-body collap-body-custom">
                            <form method='post' action='#'>
                                <div class="input-field">
                                    <input type='url' name='newlink' maxlength='1000'>
                                    <label for="newlink">Nouveau lien</label>
                                </div>
                                <button type="submit" name='savelink' class="btn blue lighten-1 btn-personal-mini" style="margin-bottom: 1em; float: right;">Ajouter</button>
                            </form>
                            <div class='clearb'></div>
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header hoverable"><i class="material-icons">image</i>Ajouter une image</div>
                        <div class="collapsible-body collap-body-custom">
                            <form method='post' action='#' enctype="multipart/form-data">
                                <div class="file-field input-field">
                                    <input type='hidden' name='MAX_FILE_SIZE' value='1048576'>
                                    <div class="btn">
                                        <span>Image</span>
                                        <input type="file" name='img1' accept="image/png, image/jpeg">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text" placeholder="1 Mo max, type PNG/JPG">
                                    </div>
                                </div>
                                <button type="submit" name='saveimg' class="btn green lighten-1 btn-personal-mini" style="margin-bottom: 1em; float: right;">Ajouter</button>
                            </form>
                            <div class='clearb'></div>
                        </div>
                    </li>
                </ul>

                <div class="card card-border" style="padding-top: .5em;">
                    <div class='clearb' style='padding: 1.5em; padding-top: 0.8em;'>
                        <a href="#modaldelsh" class="waves-effect waves-light modal-trigger btn col s12 red lighten-1 btn-personal-mini">Supprimer la shitstorm</a>
                    </div>
                    <div class='clearb' style='padding-bottom : 1.5em;'></div>
                </div>

                <!-- Modal -->
                <div id="modaldelsh" class="modal bottom-sheet">
                    <div class="modal-content">
                        <h4>Voulez-vous vraiment supprimer la shitstorm ?</h4>
                    </div>
                    <div class="modal-footer" style='padding-left: 1.5em;'>
                        <a href="<?= HTTPS_URL ?>modify/<?= $idSub ?>?delete=<?= (isset($_COOKIE['token']) ? md5($_COOKIE['token'] . $idSub) : 0) . ($isWait ? '&waiting=t' : '') ?>" 
                            class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 red lighten-1 left white-text">
                            <i class='material-icons left'>clear</i>Oui
                        </a>
                        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 blue lighten-1 left white-text" style='margin-left : 2em;'>
                            <i class='material-icons left'>keyboard_capslock</i>Non
                        </a>
                        <div class='clearb'></div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}