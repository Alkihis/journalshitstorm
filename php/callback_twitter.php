<?php

// Gestion du callback : Récupération de l'access token
$request_token = [];
$request_token['oauth_token'] = $_SESSION['tokens']['request'] ?? null;
$request_token['oauth_token_secret'] = $_SESSION['tokens']['request_secret'] ?? null;

$return_to_login_url = HTTPS_URL . 'login';
$rtl = "<h6><a href='$return_to_login_url'>Revenir à la connexion</a></h6>";

if (isset($_REQUEST['denied'])) {
    echo "<h5>Vous avez annulé la demande de liaison.</h5>$rtl";
    return;
}

if (isset($_REQUEST['oauth_token']) && $request_token['oauth_token'] !== $_REQUEST['oauth_token']) {
    echo "<h5 class='error'>Impossible de vérifier les jetons. Abandon. Recommencez.</h5>$rtl";
    return;
}
else if (!isset($request_token['oauth_token'], $request_token['oauth_token_secret'])) {
    echo "<h5 class='error'>Votre session a expiré ou les cookies sont désactivés. Impossible de continuer.</h5>$rtl";
    return;
}
else if (!isset($_REQUEST['oauth_verifier'])) {
    echo "<h5 class='error'>Impossible de vérifier que vous provenez de Twitter.</h5>$rtl";
    return;
}

try {
    $twitter_link = new OAuth(CONSUMER_KEY, CONSUMER_SECRET, OAUTH_SIG_METHOD_HMACSHA1, OAUTH_AUTH_TYPE_URI);
    $twitter_link->setToken($request_token['oauth_token'], $request_token['oauth_token_secret']);

    $access_tokens = $twitter_link->getAccessToken('https://api.twitter.com/oauth/access_token', '', $_REQUEST['oauth_verifier'], 'POST');
} catch (OAuthException $e) {
    throw new Exception('Impossible d\'obtenir l\'access token Twitter', 402);
}

// var_dump($access_tokens);
// à partir de cet endroit =>
// Si UserID connu : Connexion automatique, redirection vers l'accueil
// Si UserID inconnu : Amène vers la page de création de compte spécial Twitter, avec champ username prérempli avec le screen_name

global $connexion;

$res = mysqli_query($connexion, "SELECT * FROM Users WHERE twitter_id IS NOT NULL AND twitter_id='{$access_tokens['user_id']}';");
if ($res && mysqli_num_rows($res)) { // Il y a un utilisateur qui correspond
    $connection_code = checkConnect($res);
    if($connection_code === 1) {
        header('Location: ' . HTTPS_URL . 'journal');
        unset($_SESSION['tokens']);
    }
    else {
        echo "<h5 class='error'>Impossible de vous connecter. (code $connection_code)</h5>";
    }
}
else {
    $_SESSION['create_tokens']['twitter_id'] = $access_tokens['user_id'];
    $_SESSION['create_tokens']['screen_name'] = $access_tokens['screen_name'];
    $_SESSION['create_tokens']['json'] = json_encode($access_tokens);
    // Redirection vers la création de compte
    header('Location: ' . HTTPS_URL . 'createaccount_twitter');
    unset($_SESSION['tokens']);
}

