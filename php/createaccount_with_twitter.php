<?php 
// --------------------------------------
// PAGE DE CREATION DE COMPTE UTILISATEUR
// --------------------------------------

$GLOBALS['navbar']->addElement('login', 'Connexion');
$GLOBALS['navbar']->addElement('createaccount_twitter', 'Créer un compte');

if(!$GLOBALS['settings']['allowUserCreation'] || isLogged()){
    echo "<h3 class='error'>Il est actuellement impossible de créer un compte.</h3><h6>Réessayez plus tard.</h6>";
    return;
}

$create_text = '';
$make_form = true;

if(!isset($_SESSION['create_tokens'], $_SESSION['create_tokens']['twitter_id'], $_SESSION['create_tokens']['screen_name'])) {
    echo "<h3 class='error'>Vos identifiants ont expiré.</h3><h6>Réessayez de créer un compte.</h6>";
    return;
}

if(isset($_POST['usrname'])){
    global $connexion;

    $forbidden = ['administrateur', 'administrator', 'moderator', 'moderateur', 'alki'];

    $psw = $_POST['psw'] ?? null;

    if(empty($_POST['usrname'])){
        $create_text .= "<h3 class='error'>Le nom d'utilisateur ne peut pas être vide.</h3>";
    }
    
    elseif(in_array(strtolower($_POST['usrname']), array_map('strtolower', array_values($forbidden))) || in_array(strtolower($_POST['usrname']), array_map('strtolower', array_keys(PAGES_REFERENCES)))){
        $create_text .= "<h3 class='error'>Ce nom d'utilisateur est réservé.</h3>";
    }
    else{
        $usrn = mysqli_real_escape_string($connexion, $_POST['usrname']);
        $res = mysqli_query($connexion, "SELECT usrname FROM Users WHERE usrname='$usrn';");

        if(mysqli_num_rows($res) != 0){
            $create_text .= "<h3 class='error'>Cet utilisateur est déjà enregistré.</h3>";
        }
        else{
            if($psw && !preg_match(REGEX_PASSWORD, $psw)){
                $create_text .= "<h3 class='error'>Le mot de passe ne contient pas les prérequis demandés.</h3>";
            }
            elseif(!preg_match(REGEX_USERNAME, $usrn)){
                $create_text .= "<h3 class='error'>Le nom d'utilisateur est trop long, ou ne contient pas les prérequis demandés.</h3>";
            }
            else{
                $psw = ($psw ? password_hash($_POST['psw'], PASSWORD_BCRYPT) : '');
                // Si le mot de passe est vide, password_verify retournera toujours faux
                $res = mysqli_query($connexion, "INSERT INTO Users (usrname, realname, passw, twitter_id, access_token_twitter) 
                                                 VALUES ('$usrn', '$usrn', '$psw', '{$_SESSION['create_tokens']['twitter_id']}', '{$_SESSION['create_tokens']['json']}');");
                if($res){
                    $user_id = mysqli_insert_id($connexion);

                    $resUser = mysqli_query($connexion, "SELECT * FROM Users WHERE id='$user_id';");

                    $create_text .= "<h3 class='pass'>Votre compte a bien été enregistré.</h3>";
                    unset($_SESSION['create_tokens']);
                    $_POST['confirm'] = true;
                    $_POST['usrname'] = $usrn;

                    // Tentative de connexion
                    $connection_code = checkConnect($resUser);

                    if($connection_code === 1){
                        $_SESSION['to_configure'] = true;
                        header('Location: '.HTTPS_URL.'createaccount/configure'); return;
                    }
                    else{
                        $create_text .= "<h3 class='error'>Une erreur est survenue lors de la connexion automatique (code $connection_code). Connectez-vous avez la page connexion.</h3>";
                    }

                    $make_form = false;
                }
                else{
                    $create_text .= "<h3 class='error'>Une erreur de la base de données est survenue : ".mysqli_error($connexion)."</h3>";
                }
            }
        }
    }
}

if($make_form && isset($_SESSION['create_tokens']['screen_name'])) {
    // Recherche d'un screen_name validant un nom d'utilisateur non existant
    $tries = 0;
    $found = false;
    $screen_name = $_SESSION['create_tokens']['screen_name'];
    do {
        $res = mysqli_query($connexion, "SELECT id FROM Users WHERE usrname='$screen_name';");
        if($res && mysqli_num_rows($res)) {
            $screen_name .= (string)rand(1, 9);
        }
        else {
            $found = true;
        }

        $tries++;
    } while(!$found && $tries < 5);
}

$GLOBALS['turn_off_container'] = true;
?>

<div class='background-full creation-image'>
</div>

<div class="row">
    <div class='col s12 l10 offset-l1'>
        <?= ($create_text ? '<div style="margin-top: 30px;">' . $create_text . '</div>' : '') ?>
        <?php if($make_form) { ?>
            <div class='col s12 l6'>
                <div class="card-user-new border-user" style='height: auto !important; margin-top: 30px;'>
                    <form action="#" method='post'>
                        <div class='input-field'>
                            <label for="usrname">Nom d'utilisateur</label>
                            <input type="text" id="usrname" name="usrname" maxlength="16" pattern='^[a-zA-Z0-9_-]{3,16}$'
                                    value='<?= htmlspecialchars($screen_name, ENT_QUOTES); ?>' required>
                        </div>
                        <div class='input-field'>
                            <label for="psw">Mot de passe (facultatif)</label>
                            <input type="password" id="psw" name="psw" 
                                    placeholder="Doit contenir au moins 8 caractères, un chiffre, une lettre minuscule et majuscule.">
                        </div>

                        <button name='submit' class='btn card-user-new border-user attenuation right btn-personal'>
                            Créer un compte
                        </button>
                    </form>

                    <div class='clearb'></div>
                    <p class='black-text' style='text-align: justify; text-justify: inter-word; padding: 0 15px;'>
                        Vous avez déjà un compte sur le Journal des Shitstorms ? <a href='<?= HTTPS_URL ?>login?redirect=compte/others'>Cliquez ici</a> pour vous connecter normalement
                        et accéder automatiquement à vos paramètres pour lier votre profil Twitter.
                    </p>
                    <div class='clearb'></div>
                </div>
            </div>
            <div class='col s12 l6'>
                <p class='flow-text black-text' style='text-align: justify; text-justify: inter-word; padding: 0 25px;'>
                    Votre nom d'utilisateur est un prérequis obligatoire pour commencer.
                    En revanche, comme vous autorisez la connexion via Twitter, la création d'un mot de passe est facultative.<br>
                    Si vous souhaitez un jour en définir un, vous pourrez laisser libre cours à votre imagination dans les paramètres de votre compte.<br>
                    Aucune information supplémentaire n'est nécessaire pour personnaliser votre expérience.<br>
                    Informez-vous un peu plus sur notre <a target='_blank' href='<?= HTTPS_URL ?>data_protection'>politique de confidentialité</a>.
                </p>
            </div>
        <?php } ?>

        <div class='clearb'></div>
        <p class='black-text center' style='font-style: italic;'>
            En créant un compte associé à Twitter, vous autorisez le stockage de données permettant d'obtenir des informations sur votre profil. 
            Vous pouvez en savoir plus <a href='<?= HTTPS_URL ?>data_protection#twitter_data' target='_blank'>ici</a>.
        </p>
    </div>
</div>
