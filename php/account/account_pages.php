<?php 
function loadAccountProprieties(&$rowUser) { // Propriétés du compte 
    // Affichage card de validation de mot de passe si non validé
    if(!$rowUser['confirmedMail']){ ?>
        <div class='row'>
            <div class='card-user-new border-user hoverable col s12 l10 offset-l1' style='height: auto !important; margin-top: 8px; margin-bottom: 8px; padding: 18px !important;'>
                <h6>Vous n'avez pas <?= ($rowUser['mail'] != '' ? 'confirmé' : 'configuré') ?> votre adresse e-mail.</h6>
                <?php if($rowUser['mail'] != '') { ?>
                    <p>Une confirmation est nécessaire si vous souhaitez pouvoir réinitialiser votre mot de passe, ainsi que recevoir un e-mail lorsqu'un de vos abonnements poste une shitstorm.</p>
                    <p><a href='#!' onclick='confirmEmail()'>(R)envoyer l'e-mail de confirmation</a></p>
                <?php }
                else { ?>
                    <p>Vous pouvez configurer votre adresse e-mail ci-dessous.</p>
                <?php } ?>
            </div>
            <div class='clearb'></div>
        </div>
    <?php } ?>
    <div class="row">
        <div class='col s12 l10 offset-l1'> 
            <!-- Profil du compte -->
            <div class='card-user-new border-user' style='height: auto !important; margin-top: 8px; margin-bottom: 8px;'>
                <!-- Profile image -->
                <img id='profile_img' class='right' width="75" height='75' style='border-radius: 5px; margin-top: 20px; margin-right: 15px;' src="<?= HTTPS_URL.$rowUser['profile_img'] ?>">
                <p class='flow-text' id='textAccount'><?= $rowUser['usrname'] . ' (' . htmlspecialchars($rowUser['realname']) . ')' ?></p>
                <p class='flow-text'>
                    <?php if($rowUser['moderator'] == 0){
                        echo "Utilisat".($rowUser['genre'] == 'm' ? 'eur' :
                        ($rowUser['genre'] == 'f' ? 'rice' : ($rowUser['genre'] == 'b' ? 'eur.rice' : 'eur')));
                    }
                    elseif($rowUser['moderator'] == 1){
                        echo "Modérat".($rowUser['genre'] == 'm' ? 'eur' :
                        ($rowUser['genre'] == 'f' ? 'rice' : ($rowUser['genre'] == 'b' ? 'eur.rice' : 'eur')));
                    }
                    elseif($rowUser['moderator'] == 2){
                        echo "Administrat".($rowUser['genre'] == 'm' ? 'eur' :
                        ($rowUser['genre'] == 'f' ? 'rice' : ($rowUser['genre'] == 'b' ? 'eur.rice' : 'eur')));
                    } ?>
                </p>
                <p class='flow-text' style='text-align: left;'>
                    Inscrit<?= ($rowUser['genre'] == 'f' ? 'e' : ($rowUser['genre'] == 'b' ? '.e' : '')) ?> le <?= formatDate($rowUser['registerdate'], 0) ?>
                    <?= ($rowUser['confirmedMail'] ? "<br>Adresse e-mail confirmée" : '') ?>
                </p>
                <div class='divider' style='margin-bottom: 2rem;'></div>

                <!-- Frame for no redirection -->
                <iframe name="submitFr" style="display:none;"></iframe>
                <!-- JS form --> 
                <form action="about:blank" method="get" target='submitFr'>
                    <div class="input-field">
                        <label for="usrname">Pseudo</label>
                        <input type='text' id='usrname' required maxlength='16' value='<?= htmlspecialchars($rowUser['usrname']) ?>'>
                    </div>
                    <div class="input-field">
                        <label for="realname">Nom d'usage</label>
                        <input type='text' id='realname' required maxlength='32' value='<?= htmlspecialchars($rowUser['realname'], ENT_QUOTES, 'UTF-8') ?>'>
                    </div>
                    <div class='input-field'>
                        <input type="text" id="mail" maxlength="128" pattern='^[a-zA-Z0-9._+-]+@[a-zA-Z0-9._-]{2,}\.[a-zA-Z]{2,6}$' value='<?= $rowUser['mail'] ?>'>
                        <label for="mail">Adresse email</label>
                    </div>
                    <div class='input_field' id='genre_type'>
                    <label for="genre">Genre (pronom)</label>
                        <select name='genre' id='gender'>
                            <option value='male' <?= ($rowUser['genre'] == 'm' ? 'selected' : '') ?>>Masculin (il)</option>
                            <option value='female' <?= ($rowUser['genre'] == 'f' ? 'selected' : '') ?>>Féminin (elle)</option>
                            <option value='genderqueer' <?= ($rowUser['genre'] == 'b' ? 'selected' : '') ?>>Non binaire (iel)</option>
                            <option value='agender' <?= ($rowUser['genre'] == 'a' ? 'selected' : '') ?>>Sans (iel)</option>
                        </select>
                    </div>
                    
                    <button name='changeRN' onclick='changeAttributs()' class='btn card-user-new border-user attenuation right btn-personal'>
                        Enregistrer
                    </button>
                    <div class='clearb'></div>
                </form>
            </div>

            <div class='spacer clearb' style='margin-bottom: 1.5em;'></div>

            <div class='card-user-new border-user' style='height: auto !important; margin-top: 8px; margin-bottom: 30px;'>
                <button id='followbtn' type='button' href="#modalmodifyfollow" class='waves-effect waves-light modal-trigger btn col s12 btn-personal-mini <?= ($rowUser['followable'] ? 'grey' : 'green') ?>'>
                    <?= ($rowUser['followable'] ? 'Empêcher les abonnements à mon profil' : 'Me rendre suivable') ?>
                </button>
            <div class='clearb'></div>
            </div>
        </div>
    </div>
<?php } 
function loadAccountPassword(&$rowUser) { // Mot de passe ?>
    <div class='row'>
        <div class='col s12 l10 offset-l1'>  
            <div class='card-user-new border-user' style='height: auto !important; margin-top: 8px; margin-bottom: 30px;'>
                <form action='#' method='post'>
                    <div class="input-field">
                        <label for="psw">Mot de passe actuel</label>
                        <input type="password" id="psw" name="psw" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                        title="Doit contenir au moins 8 caractères, un chiffre, une lettre minuscule et majuscule." <?= ($rowUser['passw'] ? 'required' : '') ?>>
                    </div>
                    
                    <div class="input-field">
                        <label for="newpsw">Nouveau mot de passe</label>
                        <input type="password" id="newpsw" name="newpsw" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Doit contenir au moins 8 caractères, un chiffre, une lettre minuscule et majuscule." required>
                    </div>
                    <div class="input-field">
                        <label for="reppsw">Répétez votre mot de passe</label>
                        <input type="password" id="reppsw" name="reppsw" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Doit contenir au moins 8 caractères, un chiffre, une lettre minuscule et majuscule." required>
                    </div>

                    <button name='changePSW' class='btn card-user-new border-user attenuation right btn-personal'>
                        Modifier mot de passe
                    </button>
                    <div class='clearb'></div>
                </form>
            </div>
        </div>
    </div>
<?php }
function loadAccountVisual(&$rowUser) { // Options visuelles ?>
    <div class='row'>
        <div class='col s12 l10 offset-l1'>  
            <div class='card-user-new border-user' style='height: auto !important; margin-top: 8px; margin-bottom: 30px;'>
                <div>
                    <span class='col s12 black-text' style='margin-top: 1em;'>Nombre d'éléments par chargement sur le journal</span>
                    <select id='perpagecount' name='perpagecount' class='col s7'>
                        <?php
                            echo "<option value='1'>1</option>";
                            echo "<option value='2'>2</option>";
                            echo "<option value='3'>3</option>";
                            for($i = 5; $i <= 25; $i += 5){
                                echo "<option value='$i'>$i</option>";
                            }
                        ?>
                    </select>
                    <span class='col s1'></span>

                    <div style='padding-top:0.8em;'>
                        <button onclick='changePerPage()' type='button' id='goPerPage' class='btn card-user-new border-user attenuation right btn-personal-classic col s4'>
                            Modifier
                        </button>
                    </div>
                    <div class='clearb' style='margin-bottom: 1em;'></div>
                </div>
                <div>
                    <div class='col s12 black-text' style='margin-top: 1em;'>Mode d'affichage préféré du Journal</div>
                    <select id='view_mode' name='view_mode' class='col s12'>
                        <option value='date'>Date</option>
                        <option value='rating'>Note</option>
                        <option value='add_date'>Date d'ajout</option>
                    </select>
                    <div class='clearb'></div>
                </div>
            </div>
        

            <!-- Eclairs et neige -->
            <div class='card-user-new border-user' style='height: auto !important; margin-top: 8px; margin-bottom: 30px;'>
                <button id='disable_auto_scroll_button' type='button' href="#!" data-value="0" class='waves-effect waves-light btn col s12 darken-2 btn-personal-mini'></button>
                <div class='spacer clearb' style='margin-bottom: 1.5em;'></div>

                <?php if($rowUser['storm']){
                    echo "<button id='stormbtn' onclick='changeStormMode(0)' type='button' name='STORM' class='btn col s12 amber darken-3 btn-personal-mini'>Désactiver les éclairs</button>";
                }
                else{
                    echo "<button id='stormbtn' onclick='changeStormMode(1)' type='button' name='STORM' class='btn col s12 indigo darken-2 btn-personal-mini'>Activer les éclairs</button>";
                } ?>
                <div class='spacer clearb' style='margin-bottom: 1.5em;'></div>

                <?php if($rowUser['snow']){
                    echo "<button id='snowbtn' onclick='changeSnowMode(0)' type='button' name='SNOW' class='btn col s12 purple lighten-3 btn-personal-mini'>Désactiver la neige</button>";
                }
                else{
                    echo "<button id='snowbtn' onclick='changeSnowMode(1)' type='button' name='SNOW' class='btn col s12 blue lighten-3 btn-personal-mini'>Activer la neige</button>";
                } ?>
                <div class='clearb'></div>
            </div>
        </div>
    </div>
<?php }
function loadAccountModificationProfile(&$rowUser, $menu_bar) { // Modification du profil ?>
    <div class='background-full banner-background default-color image-changer' style='height: 450px !important;' onclick='changeBannerImg()'>
    </div>
    <div style='padding: 0; position: relative'>
        <span class='image-profile-container-parallax' style='font-size: 4.2rem;
                     position: absolute;
                     right: 7%;
                     top: -165px;
                     margin-top: .5em;
                     z-index: 3;'>
            <a class='card-pointer' onclick='changeImg()'>
                <img id='profile_img' class='image-profile-parallax image-changer' src="<?= HTTPS_URL.$rowUser['profile_img'] ?>">
            </a>
        </span>
    </div>
    <div class='section'>
        <div class='row'>
            <div class='col s12 l10 offset-l1'>
                <p class='flow-text' style='margin-left: .3em; margin-top: 40px; word-wrap: break-word;'>
                    Cliquez sur un élément pour le modifier
                    <?php if($rowUser['access_token_twitter']) { // Si l'utilisateur a défini son compte Twitter ?>
                        <br><br>
                        <a href='#!' onclick='setTwitterProfileImg()'>Importer votre image de profil Twitter</a><br>
                        <a href='#!' onclick='setTwitterBannerImg()'>Importer votre bannière Twitter</a><br>
                        <span class='italic small-info'>Définir une image depuis Twitter écrasera votre image de profil / bannière actuelle</span>
                    <?php } ?>
                </p>
            </div>
        </div>
    </div>
    
    <div class='container row'>
        <?= $menu_bar ?>
    </div>
<?php }
function loadMySubs(&$rowUser) { // Mes commenaires 
    require(PAGES_DIR . 'mycomments.php');
}

function loadAccountOthers(&$rowUser) { // Autres ?>
    <div class='row'>
        <div class='col s12 l10 offset-l1'>  
            <!-- Suppression de compte -->
            <div class='card-user-new border-user' style='height: auto !important; margin-top: 8px; margin-bottom: 15px;'>
                <a href="#modalresacc" class='modal-trigger clearb card-pointer'>
                    <div class='btn btn-personal card-user-new border-user attenuation' style='width: 100% !important; margin-top: 7px;'>
                        Réinitialiser mon compte
                    </div>
                </a>
                <a href="#modaldelacc" class='modal-trigger clearb card-pointer'>
                    <div class='btn btn-personal card-user-new border-user attenuation' style='width: 100% !important;'>
                        Supprimer mon compte
                    </div>
                </a>
                <?php if ($rowUser['passw'] === '') { ?>
                    <div class='clearb'></div>
                    <p class='flow-text black-text' style='text-align: justify; margin-bottom: 5px;'>
                        Vous ne pouvez pas supprimer ou réinitialiser votre compte sans avoir défini de mot de passe.
                    </p>
                <?php } ?>
                <div class='clearb'></div>
            </div>
        </div>
    </div>

    <div class='row'>
        <div class='col s12 l10 offset-l1'>  
            <!-- Suppression de profil / bannière -->
            <div class='card-user-new border-user' style='height: auto !important; margin-bottom: 15px;'>
                <a href="#modaldelpp" class='modal-trigger clearb card-pointer'>
                    <div class='btn btn-personal card-user-new border-user attenuation' style='width: 100% !important; margin-top: 7px;'>
                        Réinitialiser la photo de profil
                    </div>
                </a>
                <a href="#modaldelbanner" class='modal-trigger clearb card-pointer'>
                    <div class='btn btn-personal card-user-new border-user attenuation' style='width: 100% !important;'>
                        Réinitialiser la bannière
                    </div>
                </a>
                <div class='clearb'></div>
            </div>
        </div>
    </div>

    <div class='row'>
        <div class='col s12 l10 offset-l1'>  
            <!-- Archive et fonctions expérimentales -->
            <div class='card-user-new border-user' style='height: auto !important; margin-bottom: 15px;'>
                <a href="<?= HTTPS_URL ?>data_protection/query_data" class='modal-trigger clearb card-pointer'>
                    <div class='btn btn-personal card-user-new border-user attenuation' style='width: 100% !important; margin-top: 7px;'>
                        Télécharger une archive de mes données personnelles
                    </div>
                </a>
                <div class='clearb'></div>
                <button onclick='enableDisableLab()' class='btn card-user-new border-user attenuation btn-personal-classic' style='width: 100% !important;'>
                    Activer/désactiver fonctionnalités expérimentales
                </button>
                <div class='clearb'></div>
            </div>
        </div>
    </div>
<?php }

function loadTwitterAccount(&$rowUser) { // Compte Twitter 
    global $connexion;

    // Initialisation des boutons et du lien OAuth
    $twitter_text = 'Révoquer l\'accès à mon compte Twitter';
    $twitter_link_value = HTTPS_URL . 'compte/twitter/unlink_twitter_account';
    $twitter_color = 'red';
    $twitter_link = new OAuth(CONSUMER_KEY, CONSUMER_SECRET, OAUTH_SIG_METHOD_HMACSHA1, OAUTH_AUTH_TYPE_AUTHORIZATION);

    // Si on cherche à ajouter un compte twitter au profil / délier
    if ($GLOBALS['additionnals'] === 'add_twitter_account') {
        require(PAGES_DIR . 'info/add_twitter_account.php');
    }
    else if ($GLOBALS['additionnals'] === 'unlink_twitter_account') {
        if($rowUser['passw'] == '') {
            echo '<h3 class="error">Vous devez avoir défini un mot de passe pour supprimer l\'association avec votre compte Twitter.</h3>';
        }
        else {
            $q = mysqli_query($connexion, "UPDATE Users SET twitter_id=NULL, access_token_twitter=NULL WHERE id={$rowUser['id']};");
            $rowUser['twitter_id'] = null;
            $rowUser['access_token_twitter'] = null;
    
            echo '<h3 class="pass">Votre compte Twitter n\'est plus lié à votre profil.</h3>';
        }
    }
    
    if(!$rowUser['twitter_id']) { // Si aucun compte n'est lié
        try {
            $request_tokens = $twitter_link->getRequestToken('https://api.twitter.com/oauth/request_token', 
                                                             'https://shitstorm.fr/compte/twitter/add_twitter_account', 'POST');
            // Saving tokens
            $_SESSION['tokens']['request'] = $request_tokens['oauth_token'];
            $_SESSION['tokens']['request_secret'] = $request_tokens['oauth_token_secret'];
            
            $twitter_text = 'Lier ce compte à mon compte Twitter';
            $twitter_link_value = 'https://api.twitter.com/oauth/authorize?oauth_token=' . $request_tokens['oauth_token'];
            $twitter_color = 'teal';
        } catch (OAuthException $e) {
            $twitter_text = 'Impossible de lier un compte Twitter actuellement';
            echo '<span>'.$e->lastResponse.'</span>';
        }
    } ?>

    <div class='row'>
        <div class='col s12 l10 offset-l1'>  
            <!-- Compte Twitter lié -->
            <div class='card-user-new border-user' style='height: auto !important;'>
                <a href="<?= $twitter_link_value ?>" class='modal-trigger clearb card-pointer' style='margin-bottom: 30px;'>
                    <div class='waves-effect waves-light modal-trigger btn col s12 btn-personal-mini <?= $twitter_color ?>'>
                        <?= $twitter_text ?>
                    </div>
                </a>
                <div class='clearb'></div>
                <p class='flow-text black-text' style='text-align: justify; margin-bottom: 5px;'>
                    En autorisant l'accès à Twitter, vous approuvez le stockage de certaines informations de votre compte Twitter
                    dans le Journal des Shitstorms.<br>
                    <a target='_blank' href='<?= HTTPS_URL ?>data_protection#twitter_data'>En savoir plus</a>
                </p>
                <div class='clearb'></div>
            </div>

            <?php if($rowUser['twitter_id']) { 
                // Si la personne a un compte configuré, on va chercher à obtenir ses credentials
                // Pour afficher les détails de son profil
                $tokens = json_decode($rowUser['access_token_twitter'], true);
                if($tokens) {
                    $twitter_link->setToken($tokens['oauth_token'], $tokens['oauth_token_secret']);

                    $credentials = getTwitterUserObjectFromUser($twitter_link);

                    if(is_object($credentials)) {
                        $credentials->profile_image_url_https = preg_replace('/_normal/', '', $credentials->profile_image_url_https);
                        ?>
                        <!-- Détails du compte identifié -->
                        <h3>Compte lié</h3>
                        <div class='card-user-new border-user' style='height: auto !important;'>
                            <div style='width: 100%'>
                                <img class='left' style='border-radius: 7px;' src='<?= $credentials->profile_image_url_https ?>' height='75' width='75'>
                                <div class='flow-text left' style='margin-left: 15px;'>
                                    <a class='underline-hover' href='https://twitter.com/<?= $credentials->screen_name ?>' target='_blank'>
                                        <?= $credentials->name ?>
                                    </a><br>
                                    <a class='underline-hover' href='https://twitter.com/<?= $credentials->screen_name ?>' target='_blank'>
                                        @<?= $credentials->screen_name ?>
                                    </a>
                                </div>
                                <div class='clearb'></div>
                            </div>
                        </div>
                    <?php }
                    else { // Les tokens ne sont plus valides ?>
                        <h3 class="error">Impossible de vérifier votre compte Twitter.</h3>
                        <h5>Vos identifiants sont invalides. Tentez de dissocier votre compte Twitter puis l'associer à nouveau.</h5>
                        <h5>Vous ne pourrez pas vous connecter avec votre compte Twitter tant que cette erreur subsiste.</h5>
                        <div class='card-user-new border-user' style='height: auto !important;'>
                            <div style='width: 100%'>
                                <pre><?= $credentials ?></pre>
                            </div>
                        </div> <?php
                        // Obtention d'un request_token pour les renouveler
                        try {
                            $request_tokens = $twitter_link->getRequestToken('https://api.twitter.com/oauth/request_token', 
                                                                             'https://shitstorm.fr/compte/twitter/add_twitter_account/renew', 'POST');
                            // Saving tokens
                            $_SESSION['tokens']['request'] = $request_tokens['oauth_token'];
                            $_SESSION['tokens']['request_secret'] = $request_tokens['oauth_token_secret'];
                            
                            $twitter_text = 'Renouveller l\'accès';
                            $twitter_link_value = 'https://api.twitter.com/oauth/authorize?oauth_token=' . $request_tokens['oauth_token'];
                            $twitter_color = 'teal';
                        } catch (OAuthException $e) {
                            $twitter_text = 'Impossible de renouveller l\'accès actuellement';
                            $twitter_color = 'red';
                            echo "<span>{$e->lastResponse}</span>";
                        } ?>
                        <div class='card-user-new border-user' style='height: auto !important;'>
                            <a href="<?= $twitter_link_value ?>" class='modal-trigger clearb card-pointer' style='margin-bottom: 30px;'>
                                <div class='waves-effect waves-light modal-trigger btn col s12 btn-personal-mini <?= $twitter_color ?>'>
                                    <?= $twitter_text ?>
                                </div>
                            </a>
                            <div class='clearb'></div>
                        </div> <?php
                    }
                }
            } ?>
        </div>
    </div>
<?php } 

function loadAccountLinkedApps(&$rowUser) { // Applications liées au profil 
    // Récupération de la liste d'applications liées
    global $connexion;

    $res = mysqli_query($connexion, "SELECT DISTINCT t.idApp, a.name, a.allowWrite FROM Tokens t JOIN TokensApps a ON t.idApp=a.idApp 
                                     WHERE tokenType='API' AND t.idUsr={$rowUser['id']}");
                                    
    $hasApp = ($res && mysqli_num_rows($res)); // L'utilisateur a au moins une app liée

    ?>

    <div class='row'>
        <div class='col s12 l10 offset-l1'> <?php
            if($hasApp) {
                while($row = mysqli_fetch_assoc($res)) { ?>
                    <div class='application-description card-user-new border-user' data-app-id="<?= $row['idApp'] ?>" 
                        style='height: auto !important; margin-bottom: 15px;'>
                        <h5><?= htmlspecialchars($row['name']) ?></h5>
                        <p>
                            Cette application peut lire <?= ($row['allowWrite'] ? 'et écrire sur' : '') ?> votre profil.
                        </p>

                        <a onclick='deleteApplication(this.parentElement)' class='clearb card-pointer'>
                            <div class='btn btn-personal card-user-new border-user attenuation' style='width: 100% !important; margin-top: 7px;'>
                                Révoquer l'application
                            </div>
                        </a>
                    </div>
                <?php }
            } ?>
            <h4 class='no-application-linked'>Vous n'avez aucune application configurée à votre compte.</h4>
        </div>
        <?php if($rowUser['mail'] && $rowUser['confirmedMail']) { ?>
            <div class='col s12 l10 offset-l1'>
                <p class='flow-text'>Envie de créer sa propre appli ? <a href='<?= HTTPS_URL ?>apps'>Cliquez ici</a>.</p>
            </div>
        <?php } ?>
    </div>
<?php }
