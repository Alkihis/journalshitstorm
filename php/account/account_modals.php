<?php

function generateAccountModals($followable) { ?>
    <div class='row'>
        <div class="col s12 l8 offset-l2">
            <!-- Modal de suppression de PP -->
            <div id="modaldelpp" class="modal bottom-sheet">
                <div class="modal-content">
                    <h4>Voulez-vous vraiment supprimer votre photo de profil ?</h4>
                </div>
                <div class="modal-footer" style='padding-left: 1.5em;'>
                    <button name='deleteAccount' style='text-align: right;'
                    class="modal-action waves-effect waves-green btn-flat col s3 l1 red lighten-1 left white-text" onclick='setDefaultImg()'>
                        <i class='material-icons left'>clear</i>Oui
                    </button>
                    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 blue lighten-1 left white-text" style='margin-left : 2em;'>
                        <i class='material-icons left'>keyboard_capslock</i>Non
                    </a>
                    <div class='clearb'></div>
                </div>
            </div>

            <!-- Modal de suppression de bannière -->
            <div id="modaldelbanner" class="modal bottom-sheet">
                <div class="modal-content">
                    <h4>Voulez-vous vraiment supprimer votre bannière ?</h4>
                </div>
                <div class="modal-footer" style='padding-left: 1.5em;'>
                    <button name='deleteAccount' style='text-align: right;'
                    class="modal-action waves-effect waves-green btn-flat col s3 l1 red lighten-1 left white-text" onclick='setDefaultBanner()'>
                        <i class='material-icons left'>clear</i>Oui
                    </button>
                    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 blue lighten-1 left white-text" style='margin-left : 2em;'>
                        <i class='material-icons left'>keyboard_capslock</i>Non
                    </a>
                    <div class='clearb'></div>
                </div>
            </div>

            <!-- Modal de suppression -->
            <div id="modaldelacc" class="modal bottom-sheet">
                <div class="modal-content">
                    <h4>Voulez-vous vraiment supprimer votre compte ?</h4>
                </div>
                <div class="modal-footer" style='padding-left: 1.5em;'>
                    <button name='deleteAccount' style='text-align: right;'
                    class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 red lighten-1 left white-text" onclick='closeAndOpenModal("modaldelacc", "modaldelaccsure")'>
                        <i class='material-icons left'>clear</i>Oui
                    </button>
                    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 blue lighten-1 left white-text" style='margin-left : 2em;'>
                        <i class='material-icons left'>keyboard_capslock</i>Non
                    </a>
                    <div class='clearb'></div>
                </div>
            </div>

            <!-- Modal de confirmation -->
            <div id="modaldelaccsure" class="modal bottom-sheet">
                <form method='post' action='#'>
                    <div class="modal-content">
                        <h4 class='error'>Êtes-vous sûr de vraiment vouloir supprimer votre compte ?</h4>
                        <p>
                            L'opération est irréversible.<br>
                            Toutes vos shitstorms seront anonymisées.<br>
                            Vos commentaires, réponses aux shitstorms, et soumissions encore non approuvées seront définitivement supprimés.<br>
                        </p>
                        <p>Tapez votre mot de passe pour confirmer votre action.</p>
                        <div class='input-field'>
                            <label for='confirmPsw'>Mot de passe</label>
                            <input type='password' id='confirmPsw' name='confirmPsw' required>
                        </div>
                    </div>
                    <div class="modal-footer" style='padding-left: 1.5em;'>
                        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 blue lighten-1 left white-text">
                            <i class='material-icons left'>keyboard_capslock</i>Non
                        </a>
                        <button type='submit' name='deleteAccount' style='text-align: right; margin-left : 2em;' class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 red lighten-1 left white-text">
                            <i class='material-icons left'>clear</i>Oui
                        </button>
                        <div class='clearb'></div>
                    </div>
                </form>
            </div>

            <!-- Modal de réinitialisation -->
            <div id="modalresacc" class="modal bottom-sheet">
                <div class="modal-content">
                    <h4>Voulez-vous vraiment réinitialiser votre compte ?</h4>
                </div>
                <div class="modal-footer" style='padding-left: 1.5em;'>
                    <button name='resetAccount' style='text-align: right;'
                    class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 red lighten-1 left white-text" onclick='closeAndOpenModal("modalresacc", "modalresaccsure")'>
                        <i class='material-icons left'>clear</i>Oui
                    </button>
                    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 blue lighten-1 left white-text" style='margin-left : 2em;'>
                        <i class='material-icons left'>keyboard_capslock</i>Non
                    </a>
                    <div class='clearb'></div>
                </div>
            </div>

            <!-- Modal de confirmation -->
            <div id="modalresaccsure" class="modal bottom-sheet">
                <form method='post' action='#'>
                    <div class="modal-content">
                        <h4 class='error'>Êtes-vous sûr de vraiment vouloir réinitialiser votre compte ?</h4>
                        <p>
                            L'opération est irréversible.<br>
                            Vos commentaires, réponses aux shitstorms, et soumissions encore non approuvées seront définitivement supprimés.<br>
                            Si vous n'anonymisez pas vos shitstorms, elles seront également supprimées.<br>
                            Vos abonnements seront réinitialisés et vos abonnés ne vous suivront plus.<br>
                            L'ensemble de vos notifications seront supprimées.<br>
                            Vous conserverez votre nom d'utilisateur, ainsi que les autres paramètres de votre compte.
                        </p>
                        <p>Tapez votre mot de passe pour confirmer votre action.</p>
                        <p>
                            <input type="checkbox" name="anonymyse" class="filled-in" id="anonymyse" checked="checked">
                            <label for="anonymyse">Anonymiser les shitstorms</label>
                        </p>
                        <div class='input-field'>
                            <label for='confirmPswRenew'>Mot de passe</label>
                            <input type='password' id='confirmPswRenew' name='confirmPsw' required>
                        </div>
                    </div>
                    <div class="modal-footer" style='padding-left: 1.5em;'>
                        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 blue lighten-1 left white-text">
                            <i class='material-icons left'>keyboard_capslock</i>Non
                        </a>
                        <button type='submit' name='resetAccount' style='text-align: right; margin-left : 2em;' class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 red lighten-1 left white-text">
                            <i class='material-icons left'>clear</i>Oui
                        </button>
                        <div class='clearb'></div>
                    </div>
                </form>
            </div>

            <!-- Modal de modification de follow -->
            <div id="modalmodifyfollow" class="modal bottom-sheet">
                <div class="modal-content">
                    <h4 id='follow_status_text'>Voulez-vous <?= ($followable ? 'désactiver' : 'activer') ?> les abonnements à votre compte ?</h4>
                    <p id='follow_status_par' <?= ($followable ? '' : "style='display: none;'") ?>>Désactiver les abonnements à votre compte désabonnera immédiatement tous les abonnés à votre profil.</p>
                </div>
                <div class="modal-footer" style='padding-left: 1.5em;'>
                    <button id='followbtn_confirm_modal' style='text-align: right;'
                    class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 red lighten-1 left white-text" onclick='changeFollowVisiblity("<?= ($followable ? 'hidden' : 'followable') ?>")'>
                        <i class='material-icons left'>clear</i>Oui
                    </button>
                    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 blue lighten-1 left white-text" style='margin-left : 2em;'>
                        <i class='material-icons left'>keyboard_capslock</i>Non
                    </a>
                    <div class='clearb'></div>
                </div>
            </div>
        </div>
    </div><?php
}
