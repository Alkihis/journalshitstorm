<?php 
// --------------------------------------
// PAGE DE CREATION DE COMPTE UTILISATEUR
// --------------------------------------

$GLOBALS['navbar']->addElement('login', 'Connexion');
$GLOBALS['navbar']->addElement('createaccount', 'Créer un compte');

$configure = false;
$finish = false;

$create_text = '';

if(isset($GLOBALS['current_page']) && isset($GLOBALS['current_page'][2], $GLOBALS['current_page'][3]) && $GLOBALS['current_page'][2] == 'configure' && $GLOBALS['current_page'][3] == 'complete'){
    $finish = true;
    require(ROOT. '/php/info/finish_creation.php');
    return;
}
elseif(isset($GLOBALS['current_page']) && isset($GLOBALS['current_page'][2]) && $GLOBALS['current_page'][2] == 'configure'){
    $configure = true;
    require(ROOT. '/php/configure_account.php');
    return;
}


if(!$GLOBALS['settings']['allowUserCreation'] || isLogged()){
    echo "<h3 class='error'>Il est actuellement impossible de créer un compte.</h3><h6>Réessayez plus tard.</h6>";
    return;
}

if(isset($_POST['usrname'], $_POST['psw'], $_POST['repeatpsw'])){
    global $connexion;
    
    $forbidden = ['administrateur', 'administrator', 'moderator', 'moderateur', 'alki'];

    if($_POST['psw'] != $_POST['repeatpsw'] || empty($_POST['psw'])){
        $create_text .= "<h3 class='error'>Les mots de passe ne correspondent pas.</h3>";
    }
    elseif(empty($_POST['usrname'])){
        $create_text .= "<h3 class='error'>Le nom d'utilisateur ne peut pas être vide.</h3>";
    }
    
    elseif(in_array(strtolower($_POST['usrname']), array_map('strtolower', array_values($forbidden))) || in_array(strtolower($_POST['usrname']), array_map('strtolower', array_keys(PAGES_REFERENCES)))){
        $create_text .= "<h3 class='error'>Ce nom d'utilisateur est réservé.</h3>";
    }
    else{
        $usrn = mysqli_real_escape_string($connexion, $_POST['usrname']);
        $res = mysqli_query($connexion, "SELECT usrname FROM Users WHERE usrname='$usrn';");

        if(mysqli_num_rows($res) != 0){
            $create_text .= "<h3 class='error'>Cet utilisateur est déjà enregistré.</h3>";
        }
        else{
            if(!preg_match(REGEX_PASSWORD, $_POST['psw'])){
                $create_text .= "<h3 class='error'>Le mot de passe ne contient pas les prérequis demandés.</h3>";
            }
            elseif(!preg_match(REGEX_USERNAME, $usrn)){
                $create_text .= "<h3 class='error'>Le nom d'utilisateur est trop long, ou ne contient pas les prérequis demandés.</h3>";
            }
            else{
                $psw = password_hash($_POST['psw'], PASSWORD_BCRYPT);
                $res = mysqli_query($connexion, "INSERT INTO Users (usrname, realname, passw) VALUES ('$usrn', '$usrn', '$psw');");
                if($res){
                    $create_text .= "<h3 class='pass'>Votre compte a bien été enregistré.</h3>";
                    $_POST['confirm'] = true;
                    $_POST['usrname'] = $usrn;
                    if(checkConnect() == 1) {
                        $_SESSION['to_configure'] = true;
                        header('Location: '.HTTPS_URL.'createaccount/configure'); return;
                    }
                    else {
                        $create_text .= "<h3 class='error'>Une erreur est survenue lors de la connexion automatique. Connectez-vous avez la page connexion.</h3>";
                    }
                }
                else{
                    $create_text .= "<h3 class='error'>Une erreur de la base de données est survenue : ".mysqli_error($connexion)."</h3>";
                }
            }
        }
    }
}

$GLOBALS['turn_off_container'] = true;
?>

<div class='background-full creation-image'>
</div>

<div class="row">
    <div class='col s12 l10 offset-l1'>
        <?= ($create_text ? '<div style="margin-top: 30px;">' . $create_text . '</div>' : '') ?>
        <div class='col s12 l6'>
            <div class="card-user-new border-user" style='height: auto !important; margin-top: 30px;'>
                <form action="#" method='post'>
                    <div class='input-field'>
                        <label for="usrname">Nouveau nom d'utilisateur</label>
                        <input type="text" id="usrname" name="usrname" maxlength="16" pattern='^[a-zA-Z0-9_-]{3,16}$'
                                value='<?php if(isset($_POST['usrname'])) echo htmlspecialchars($_POST['usrname'], ENT_QUOTES); ?>' required>
                    </div>
                    <div class='input-field'>
                        <label for="psw">Mot de passe</label>
                        <input type="password" id="psw" name="psw" 
                                placeholder="Doit contenir au moins 8 caractères, un chiffre, une lettre minuscule et majuscule." required>
                    </div>
                    <div class='input-field'>
                        <label for="repeatpsw">Retapez le mot de passe</label>
                        <input type="password" id="repeatpsw" name="repeatpsw"
                                placeholder="Doit contenir au moins 8 caractères, un chiffre, une lettre minuscule et majuscule." required>
                    </div>

                    <input name="remember_me" value="stay_logged" type="hidden">

                    <button name='submit' class='btn card-user-new border-user attenuation right btn-personal'>
                        Créer un compte
                    </button>
                </form>
                <div class='clearb'></div>
            </div>
        </div>
        <div class='col s12 l6'>
            <p class='flow-text black-text' style='text-align: justify; text-justify: inter-word; padding: 0 25px;'>
                C'est tout.<br>
                Nous n'avons pas besoin de plus d'informations pour vous créer un compte, pas d'adresse e-mail, pas de prénom, pas de genre.<br>
                Qui est la personne ayant décrété que ces données seraient obligatoires ?<br>
                Sur la page suivante, vous serez libre de personnaliser les informations complémentaires que vous souhaitez, mais le nécessaire, lui, est ici.<br>
                De plus, si vous souhaitez savoir comment le Journal des Shitstorms stocke les informations que nous vous transmettez, nous vous invitons
                à lire notre <a target='_blank' href='<?= HTTPS_URL ?>data_protection'>politique d'informations sur les données</a>.
            </p>
        </div>
    </div>
</div>
