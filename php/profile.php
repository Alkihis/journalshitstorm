<?php
// --------------------------------------------------------
// PAGE DE PROFIL D'UN UTILISATEUR RECUPERE PAR GET[userid]
// --------------------------------------------------------

$mine = false;

if(!isset($_GET['userid'])){
    if(isset($GLOBALS['current_page']) && isset($GLOBALS['current_page'][2]) && $GLOBALS['current_page'][2] != ''){
        $_GET['userid'] = $GLOBALS['current_page'][2];
        $id = intval($_GET['userid']);
    }
    elseif(isset($_SESSION['id'])){
        $id = $_SESSION['id'];
        $mine = true;
    }
    else if(isset($GLOBALS['is_included'])) { 
        echo "<h2>Aucun utilisateur n'est spécifié.</h2>"; return; 
    }
    else {
        return _403();
    }
}
else {
    $id = intval($_GET['userid']); // Renvoie 0 si c'est une chaine
}

$finish_account_dialog = '';

if(isset($_SESSION['to_configure']) && $_SESSION['to_configure']){
    $finish_account_dialog .= '<div class="section"><div class="row"><div class="col s12 l10 offset-l1">';

    $finish_account_dialog .= "<h3 class='pass'>C'est terminé !</h3>";
    $finish_account_dialog .= "<h4 class='pass' style='padding-bottom: 1em;'>Voici votre profil.</h4>";
    if(isset($_SESSION['error_msg_creation'])) {
        $finish_account_dialog .= $_SESSION['error_msg_creation'];
        unset($_SESSION['error_msg_creation']);
    }
    
    if(isset($_SESSION['send_mail'])){
        if($_SESSION['send_mail'] === -1){ // Mail défini et confim envoyée
            $finish_account_dialog .= "<h5>Votre adresse e-mail doit être validée et une confirmation a été envoyée à celle-ci. Vérifiez vos e-mails.</h5>";
        }
        elseif($_SESSION['send_mail'] === false){ // Mail défini et confim non envoyée
            $finish_account_dialog .= "<h5 class='error'>Votre adresse e-mail doit être validée mais l'envoi de la confirmation a échoué.</h5>";
            $finish_account_dialog .= "<h6>Vérifiez la validité de votre adresse et tentez l'envoi d'une nouvelle confirmation plus tard depuis vos paramètres de compte.</h6>";
        }
    }
    $_SESSION['to_configure'] = false;
    $_SESSION['new_user'] = true;

    $finish_account_dialog .= '</div></div>';
}

global $connexion;
$resUser = NULL;
$res = NULL;

// Possiblité d'accéder au profil par le nom (id == 0) ou par l'id (id != 0)
if($id == 0){
    $id = mysqli_real_escape_string($connexion, $_GET['userid']);
    $resUser = mysqli_query($connexion, "SELECT id, profile_img, banner_img, usrname, realname, moderator, registerdate, storm, snow, genre, followable, mail, confirmedMail FROM Users WHERE usrname='$id';");
    if($resUser && mysqli_num_rows($resUser) > 0){
        $rowuser = mysqli_fetch_assoc($resUser);
        $id = $rowuser['id'];
        $storm = (bool)$rowuser['storm'];
    }
    else{
        $resUser = NULL;
    }
}
else{
    $id = mysqli_real_escape_string($connexion, $id);
    $resUser = mysqli_query($connexion, "SELECT id, usrname, banner_img, profile_img, realname, moderator, registerdate, storm, snow, genre, followable, mail, confirmedMail FROM Users WHERE id='$id';");

    if($resUser && mysqli_num_rows($resUser) > 0)
        $rowuser = mysqli_fetch_assoc($resUser);
    else
        $resUser = NULL;
}

if(isset($_SESSION['id']) && $_SESSION['id'] == $id){
    $mine = true;
}

if($resUser && $id != 0){
    if($mine){
        $GLOBALS['navbar']->addElement('profil/'.$_SESSION['username'], 'Mon profil');
        $GLOBALS['page'] = 'compte'; // Permet de set le slider du menu sur "Compte" si c'est le profil de l'utilisateur courant
        $GLOBALS['pageTitle'] = 'Mon profil - Journal des Shitstorms';
    }
    else{
        $GLOBALS['navbar']->addElement('profil/'.$rowuser['id'], 'Profil de '. htmlspecialchars($rowuser['realname']));
        $GLOBALS['pageTitle'] = "Profil de " . htmlspecialchars($rowuser['realname']) . " - Journal des Shitstorms";
    }
    
    $rowS = null;
    // Récupération des métadonnées
    $nbShit = 0;
    $res = mysqli_query($connexion, "SELECT COUNT(*) c FROM Shitstorms WHERE idUsr='$id';");
    if($res && mysqli_num_rows($res) > 0){
        $rowS = mysqli_fetch_assoc($res);
        $nbShit = $rowS['c'];
    }
    
    $storm = (bool)$rowuser['storm'];
    $snow = (bool)$rowuser['snow'];
    $status = (int)$rowuser['moderator'];
    $followed_by_you = false;
    $follows_you = false;
    $with_mail = false;
    $follow_date = null;
    $waiting_shit = null;
    $waiting_answ = null;

    // Vérification si l'utilisateur follow cette personne / si il nous suit
    if(!$mine && isLogged()){
        if($rowuser['followable'] == 1) {
            // Empêche de pouvoir suivre les non-followable, soi-même et vérifie que l'utilisateur qui consulte le profil est connecté
            $resF = mysqli_query($connexion, "SELECT idFollow, acceptMail FROM Followings WHERE idUsr='{$_SESSION['id']}' AND idFollowed='$id' LIMIT 0,1;");

            $followed_by_you = (int)($resF && mysqli_num_rows($resF) > 0);
            if($followed_by_you){
                $rowF = mysqli_fetch_assoc($resF);
                $with_mail = (bool)$rowF['acceptMail'];
            }
        }
        
        // Vérifie si l'utilisateur nous suit
        $resF = mysqli_query($connexion, "SELECT idUsr, followDate FROM Followings WHERE idFollowed='{$_SESSION['id']}' AND idUsr='$id' LIMIT 0,1;");

        $follows_you = (int)($resF && mysqli_num_rows($resF) > 0);
        if($follows_you){
            $rowF = mysqli_fetch_assoc($resF);
            $follow_date = $rowF['followDate'];
        }
    }

    // Vérification si l'utilisateur a des shitstorms / réponses en attente
    if($mine) {
        $resWait = mysqli_query($connexion, "SELECT COUNT(*) c FROM Waiting WHERE idUsr={$rowuser['id']} ".($rowuser['moderator'] ? '' : 'AND locked=0'));
        if($resWait && mysqli_num_rows($resWait)){
            $rowWait = mysqli_fetch_assoc($resWait);
            $waiting_shit = $rowWait['c'];
        }

        $resWait = mysqli_query($connexion, "SELECT COUNT(*) c FROM WaitAnsw WHERE idUsrA={$rowuser['id']} ".($rowuser['moderator'] ? '' : 'AND locked=0'));
        if($resWait && mysqli_num_rows($resWait)){
            $rowWait = mysqli_fetch_assoc($resWait);
            $waiting_answ = $rowWait['c'];
        }
    }
    
    // Vérification si l'utilisateur a déjà like
    $has_liked = false;
    $has_disliked = false;
    
    /* La feature actuelle est désactivée.
    $resl = mysqli_query($connexion, "SELECT idUsr FROM ShitLikes WHERE idUsr='$id' AND isLike=1 LIMIT 0,1;");
    
    if($resl && mysqli_num_rows($resl) > 0){
        $has_liked = true;
    }
    else{
        $has_liked = false;
    }
    
    // Vérification si l'utilisateur a déjà dislike
    $resl = mysqli_query($connexion, "SELECT idUsr FROM ShitLikes WHERE idUsr='$id' AND isLike=0 LIMIT 0,1;");
    
    if($resl && mysqli_num_rows($resl) > 0){
        $has_disliked = true;
    }
    else{
        $has_disliked = false;
    } */

    // Nombre d'abonnés et d'abonnements de l'utilisateur
    $resAbonnes = mysqli_query($connexion, "SELECT COUNT(*) c FROM Followings WHERE idFollowed='$id';");
    $nbAbonnes = 0;
    if($resAbonnes && mysqli_num_rows($resAbonnes)){
        $rowAbonnes = mysqli_fetch_assoc($resAbonnes);
        $nbAbonnes = $rowAbonnes['c'];
    }
    $resAbonnement = mysqli_query($connexion, "SELECT COUNT(*) c FROM Followings WHERE idUsr='$id';");
    $nbAbonnement = 0;
    if($resAbonnement && mysqli_num_rows($resAbonnement)){
        $rowAbonnement = mysqli_fetch_assoc($resAbonnement);
        $nbAbonnement = $rowAbonnement['c'];
    }

    $GLOBALS['add_meta_description'] = "Profil de " . htmlspecialchars($rowuser['realname']) . 
                                     " ({$rowuser['usrname']}). $nbShit shitstorm(s) publiée(s), $nbAbonnes abonné(s) et $nbAbonnement abonnement(s).";

    // Suffixe de genre
    $s = getUserSuffixTextByGender($rowuser['genre']);

    $GLOBALS['turn_off_container'] = true;

    // Génère l'activité de l'utilisateur
    // (Trois dernières actions)
    $res = mysqli_query($connexion, "SELECT * FROM Shitstorms WHERE idUsr='$id' ORDER BY dateShit DESC LIMIT 0,2;");
    $resComment = mysqli_query($connexion, "SELECT c.*, s.title FROM Comment c JOIN Shitstorms s ON c.idSub=s.idSub WHERE c.idUsr='$id' ORDER BY contentTime DESC LIMIT 0,2;");
    $resFollow = mysqli_query($connexion, "SELECT f.*, u.usrname, u.realname FROM Followings f JOIN Users u ON idFollowed=id WHERE idUsr='$id' ORDER BY followDate DESC LIMIT 0,2;");

    $actions = [];
    $i = 0;
    if($res && mysqli_num_rows($res)){
        while($row = mysqli_fetch_assoc($res)){
            $actions[$i]['row'] = $row;
            $actions[$i]['date'] = $row['approvalDate'];
            $actions[$i]['type'] = 'shitstorm';
            $i++;
        }
    }
    if($resComment && mysqli_num_rows($resComment)){
        while($row = mysqli_fetch_assoc($resComment)){
            
            $actions[$i]['row'] = $row;
            $actions[$i]['date'] = $row['contentTime'];
            $actions[$i]['type'] = 'comment';
            $i++;
        }
    }
    if($resFollow && mysqli_num_rows($resFollow)){
        while($row = mysqli_fetch_assoc($resFollow)){
            $actions[$i]['row'] = $row;
            $actions[$i]['date'] = $row['followDate'];
            $actions[$i]['type'] = 'follow';
            $i++;
        }
    }

    // Tri par date
    sortByDate($actions);

    ?>
    <script src='<?= HTTPS_URL ?>js/profile.js'></script>

    <div class='container' style='padding: 0; position: relative'>
        <span class="image-profile-container-parallax">
            <a href='<?= HTTPS_URL ?>profil/<?= $rowuser['usrname'] ?>'>
                <img id='profile_img_par' class='image-profile-parallax' src="<?= HTTPS_URL.$rowuser['profile_img'] ?>">
            </a>
        </span>
    </div>
    <div id='particle_banner' class="parallax-container parallax-container-profile default-color">
        <?php if($rowuser['banner_img']) { ?>
            <style>
                .parallax-container-profile {
                    background-image: url("<?= HTTPS_URL . $rowuser['banner_img'] ?>");
                }
            </style>
            <div class="parallax not-on-mobile no-z-index default-color">
                <img src='<?= HTTPS_URL . $rowuser['banner_img'] ?>'>
            </div>
        <?php } ?>
    </div>
    <?php

    // Affichage card de validation d'e-mail si non validé / non configuré 
    if(!$rowuser['confirmedMail'] && $mine){ ?>
       <div class="row" id='pop_out_mail' style="
            z-index: 4;
            position: absolute;
            top: 125px;
            width: 90%;
            margin-left: 5%;
        ">
            <div class="col s12">
                <div class='card card-border hoverable' style='padding: 12px !important; padding-top: 18px !important;'>
                    <div class='col s12'>
                        <h6>Vous n'avez pas <?= ($rowuser['mail'] != '' ? 'confirmé' : 'configuré') ?> votre adresse e-mail.</h6>
                        <p>
                            La <?= ($rowuser['mail'] != '' ? 'confirmation' : 'configuration de celle-ci') ?> est nécessaire si vous souhaitez pouvoir réinitialiser votre mot de passe,
                            ainsi que recevoir un e-mail lorsqu'un de vos abonnements poste une shitstorm.<br>
                            <?= ($rowuser['mail'] != '' ? "L'envoi du mail de confirmation" : 'La configuration') ?>  est faisable dans les <a href='<?= HTTPS_URL ?>compte'>paramètres de votre compte</a>.
                        </p>
                        <div style='position: absolute; top: 15px; right: 15px;' onclick='$("#pop_out_mail").hide(180)'><i class='material-icons black-text card-pointer'>close</i></div>
                    </div>
                    <div class='clearb'></div>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php if(!$rowuser['banner_img']) { ?>
        <script src="<?= HTTPS_URL ?>js/particles.min.js"></script>

        <script async>
            particlesJS.load('particle_banner', '<?= HTTPS_URL ?>static/particlesjs-black.json', function() { });
        </script>
    <?php } ?>

    <div class='master-section'>
        <?= $finish_account_dialog ?>
        <div class="section" id='infos'>
            <div class='row'>
                <div class='col s12 l10 offset-l1'>
                    <h1 style='margin-left: .3em; word-wrap: break-word;'>
                        <a style='color: inherit;' class='underline-hover' href='<?= HTTPS_URL."profil/{$rowuser['usrname']}" ?>'><?= htmlspecialchars($rowuser['realname']) ?></a>
                    </h1>
                </div>
            </div>
            
            <div class="row container">
                <div class='black-text flow-text'>
                    <a class='black-text underline-hover' href='<?= HTTPS_URL."profil/{$rowuser['usrname']}" ?>'>@<?= $rowuser['usrname'] ?></a>
                </div>

                <div class='black-text flow-text'>
                    <a href='<?= HTTPS_URL."profil/{$rowuser['usrname']}/followings#abo" ?>'><?= $nbAbonnement ?> abonnement<?= ($nbAbonnement > 1 ? 's' : '') ?></a>,
                    <a href='<?= HTTPS_URL."profil/{$rowuser['usrname']}/followers#abo" ?>'><?= $nbAbonnes ?> abonné<?= ($nbAbonnes > 1 ? 's' : '') ?></a>
                </div>

                <div class='black-text flow-text'>                    
                    <a href='<?= HTTPS_URL."profil/{$rowuser['usrname']}/shitstorms#shits" ?>'><?= ($nbShit ? $nbShit : 'Aucune') ?> shitstorm<?= ($nbShit > 1 ? 's' : '') ?> publiée<?= ($nbShit > 1 ? 's' : '') ?></a>
                    <?php if($mine) { 
                        if($waiting_shit) { ?>
                            <br><a href='<?= HTTPS_URL."profil/{$rowuser['usrname']}/waiting#shits" ?>'><?= ($waiting_shit > 1 ? $waiting_shit : 'Une') ?> shitstorm<?= ($waiting_shit > 1 ? 's' : '') ?> en attente</a>
                        <?php }
                        if($waiting_answ) { ?>
                            <br><a href='<?= HTTPS_URL."profil/{$rowuser['usrname']}/waiting/answers#shits" ?>'><?= ($waiting_answ > 1 ? $waiting_answ : 'Une') ?> réponse<?= ($waiting_answ > 1 ? 's' : '') ?> en attente</a>
                        <?php }
                    } ?>
                </div>

                <div class='black-text flow-text'>Inscrit<?= getClassicSuffixTextByGender($rowuser['genre']) ?> le <?= formatDate($rowuser['registerdate'], 0) ?></div>
                
                <?php if($follows_you === 1){ ?>
                    <div class='notification-bold-text flow-text'>Vous suit depuis le <?= formatDate($follow_date, 0) ?></div>
                <?php } ?>
                <div class='clearb'></div>

                <!-- Boutons -->
                <span style='margin-left: .5em;' id='buttons'>
                    <?= ($mine ? "<a href='".HTTPS_URL."compte' class='btn-floating tooltipped orange lighten-1 profile_button'
                        data-position='bottom' data-delay='10' data-tooltip='Mon compte'>
                            <i class='material-icons'>settings</i>
                        </a>" : '') ?>

                    <?= ($storm ? "<button class='btn-floating tooltipped purple darken-2 profile_button'
                        data-position='bottom' data-delay='10' data-tooltip='Des ÉCLAIRS'>
                            <i class='material-icons'>flash_on</i>
                        </button>" : '') ?>

                    <?= ($snow ? "<button class='btn-floating tooltipped blue lighten-3 profile_button'
                        data-position='bottom' data-delay='10' data-tooltip='Neige !'>
                            <i class='material-icons'>ac_unit</i>
                        </button>" : '') ?>

                    <button class='btn-floating tooltipped cyan white-text darken-<?= ($status ? ($status == 1 ? '2' : '3') : '1') ?> profile_button'
                    data-position='bottom' data-delay='10' data-tooltip='<?= ($status ? ($status == 1 ? "Modérat$s" : "Administrat$s") : "Utilisat$s") ?>'>
                        <i class='material-icons'><?= ($status ? ($status == 1 ? 'group' : 'supervisor_account') : 'person') ?></i>
                    </button>

                    <?php // Les fonctions suivantes sont désactivés et en attente de remplacement. ?>
                    <?= ($has_disliked ? "<button class='btn-floating tooltipped red lighten-2 profile_button'
                        data-position='bottom' data-delay='10' data-tooltip='Shitstorms notées négativement' onclick=\"generateLikedDislikedShitstorms('modalphfixed', {$rowuser['id']}, 0)\">
                            <i class='material-icons'>mood_bad</i>
                        </button>" : '') ?>

                    <?= ($has_liked ? "<button class='btn-floating tooltipped blue darken-1 profile_button'
                        data-position='bottom' data-delay='10' data-tooltip='Shitstorms notées positivement' onclick=\"generateLikedDislikedShitstorms('modalphfixed', {$rowuser['id']}, 1)\">
                            <i class='material-icons'>mood</i>
                        </button>" : '') ?>

                    <?php if($followed_by_you !== false){ ?>
                        <!-- Dropdown Structure -->
                        <ul id='follow_user_drop<?= $rowuser['id'] ?>' class='dropdown-content' data-userid='<?= $rowuser['id'] ?>' data-realname='<?= htmlspecialchars($rowuser['realname'], ENT_QUOTES) ?>'>
                            <li>
                                <span>
                                    <input class='follow-activate follow-checkbox' data-userid='<?= $rowuser['id'] ?>' type='checkbox' id='followed<?= $rowuser['id'] ?>' <?= ($followed_by_you ? 'checked' : '') ?>>
                                    <label class='black-text' for="followed<?= $rowuser['id'] ?>"><?= $followed_by_you ? 'Suivi' : 'Suivre' ?></label>
                                </span>
                            </li>
                            <li>
                                <span>
                                    <input class='follow-activate email-checkbox' data-userid='<?= $rowuser['id'] ?>' type='checkbox' id='followed_with_mail<?= $rowuser['id'] ?>' <?= ($with_mail ? 'checked' : '') ?>>
                                    <label class='black-text' for="followed_with_mail<?= $rowuser['id'] ?>">E-mails</label>
                                </span>
                            </li>
                        </ul>

                        <button id='follow_user_button<?= $rowuser['id'] ?>' class='btn-floating <?= $followed_by_you ? 'green' : 'grey' ?> profile_button dropdown-button'
                            data-activates='follow_user_drop<?= $rowuser['id'] ?>'>
                            <i id='follow_user_bell<?= $rowuser['id'] ?>' class='material-icons'><?= $followed_by_you ? 'notifications' : 'notifications_off' ?></i>
                        </button>
                    <?php } ?>
                    
                    <div class='clearb'></div>
                </span>
            </div>

            <?php if(isset($GLOBALS['additionnals']) && $GLOBALS['additionnals']){
                $realn = htmlspecialchars($rowuser['realname']);
                $usrn = htmlspecialchars($rowuser['usrname']);
                $suffix = '';
                if($mine){
                    $realn = 'Vous';
                    $suffix = 'vez';
                }
                
                if($GLOBALS['additionnals'] === 'followers'){ ?>
                    <div id='abo' class='row'>
                        <div class='col s12 l10 offset-l1'>
                            <h2 style='margin-left: 1em;'>Abonnés</h2>
                        </div>
                    </div>
    
                    <?php if(!generateFollowFromUser($rowuser['id'], 0)){ ?>
                        <div class='row'>
                            <div class='col s12 l10 offset-l1'>
                                <h3><?= $realn ?> n'a<?= $suffix ?> aucun abonné.</h3>
                            </div>
                        </div>
                    <?php }

                    $GLOBALS['navbar']->addElement("profil/$usrn/followers", 'Abonnés');
                    return;
                }
                else if($GLOBALS['additionnals'] === 'followings'){ ?>
                    <div id='abo' class='row'>
                        <div class='col s12 l10 offset-l1'>
                            <h2 style='margin-left: 1em;'>Abonnements</h2>
                        </div>
                    </div>
    
                    <?php 
                    if(!generateFollowFromUser($rowuser['id'], 1)){ ?>
                        <div class='row'>
                            <div class='col s12 l10 offset-l1'>
                                <h3><?= $realn ?> n'a<?= $suffix ?> aucun abonnement.</h3>
                            </div>
                        </div>
                    <?php }

                    $GLOBALS['navbar']->addElement("profil/$usrn/followings", 'Abonnements');
                    return;
                }
                else if($GLOBALS['additionnals'] === 'shitstorms'){ ?>
                    <div id='shits' class='row'>
                        <div class='col s12 l10 offset-l1'>
                            <h2 style='margin-left: 1em;'>Shitstorms</h2>
                        </div>
                    </div>
    
                    <?php 
                    if(!generateShitstormsFromUser($rowuser['id'], $_GET['count'] ?? 0, $_GET['sort'] ?? 'asc', $_GET['content_like'] ?? false, $_GET['order'] ?? false)){ ?>
                        <div class='row'>
                            <div class='col s12 l10 offset-l1'>
                                <h3><?= $realn ?> n'a<?= $suffix ?> publié aucune shitstorm<?= (isset($_GET['content_like']) && !empty($_GET['content_like']) ? ' correspondant à ces termes' : '' ) ?>.</h3>
                            </div>
                        </div>
                    <?php }

                    $GLOBALS['navbar']->addElement("profil/$usrn/shitstorms", 'Shitstorms');
                    return;
                    
                }
                else if($GLOBALS['additionnals'] === 'waiting' && $mine){ ?>
                    <div id='shits' class='row'>
                        <div class='col s12 l10 offset-l1'>
                            <?php if($GLOBALS['url_params'] === 'answers'){
                                $GLOBALS['navbar']->addElement("profil/$usrn/waiting/answers", 'Réponses en attente');
                                echo "<h2 style='margin-left: 1em;'>Réponses en attente</h2>";
                            }
                            else{
                                $GLOBALS['navbar']->addElement("profil/$usrn/waiting", 'Shitstorms en attente');
                                echo "<h2 style='margin-left: 1em;'>Shitstorms en attente</h2>";
                            } ?>
                            <?php require_once(PAGES_DIR . 'mywaiting.php'); ?>
                        </div>
                    </div>
                    <?php return;
                }
            } ?>
        </div>

        <div class="row container" style='margin-bottom: 0;'>
            <div class='divider'></div>
        </div>
        
        <div class='section'>
            <div class="row container">
                <h2 class="header">Dernières activités</h2>
                <?php
                if($actions){
                    echo "";
                }
                // Affichage des trois dernières actions
                for($i = 0; $i < 3 && $i < count($actions); $i++){
                    echo "<div class='col s12'><h5 class='flow-text'>";
                    echo formatDate($actions[$i]['date'], 0, 1) . ' ';
                    if($actions[$i]['type'] == 'shitstorm'){
                        echo htmlspecialchars($rowuser['realname']) . " a publié une shitstorm (<a href='".HTTPS_URL."shitstorm/{$actions[$i]['row']['idSub']}'>{$actions[$i]['row']['title']}</a>).";
                    }
                    elseif($actions[$i]['type'] == 'follow'){
                        echo htmlspecialchars($rowuser['realname']) . " a suivi <a href='".HTTPS_URL."profil/{$actions[$i]['row']['usrname']}'>{$actions[$i]['row']['realname']}</a>.";
                    }
                    elseif($actions[$i]['type'] == 'comment'){
                        echo htmlspecialchars($rowuser['realname']) . " a commenté la shitstorm <a href='".HTTPS_URL."shitstorm/{$actions[$i]['row']['idSub']}'>{$actions[$i]['row']['title']}</a>.";
                    }
                    echo "</h5></div>";
                }
                if($actions){
                    echo "<div class='clearb'></div>";
                }
                else{ ?>
                    <h5>Cet<?= ($rowuser['genre'] == 'f' ? 'te' : ($rowuser['genre'] == 'b' ? '.te' : '')) ?> utilisat<?= $s ?> n'a aucune activité récente.</h5>
                <?php } ?>
            </div>
        </div>

        <div class="row container" style='margin-bottom: 0;'>
            <div class='divider'></div>
        </div>

        <div class='section'>
            <div class="row container">
                <h2 class="header">Dernières shitstorms publiées</h2>
                <p class='flow-text' style='font-style: italic;'>
                    <?php $old_storm = (isset($_SESSION['storm']) ? $_SESSION['storm'] : 0);
                    $_SESSION['storm'] = $storm; // Applique le profil de l'utilisateur regardé au profil

                    $begin = true;
                
                    if($res && $nbShit > 0 && $id != 0){
                        for($i = 0; $i < count($actions); $i++){
                            if($actions[$i]['type'] == 'shitstorm'){
                                if(!$begin){
                                    echo "<div class='divider'></div>";
                                }
                                else{
                                    $begin = false;
                                }

                                if(!generateShitstormFlowFromMysqliRow($actions[$i]['row']))
                                    break;
                            }
                        }
                    }
                    else{
                        echo "<h5>Cet".($rowuser['genre'] == 'f' ? 'te' : ($rowuser['genre'] == 'b' ? '.te' : ''))." utilisat$s n'a publié aucune shitstorm.</h5>";
                    }
                
                    $_SESSION['storm'] = $old_storm; ?>
                </p>
            </div>
        </div>
    </div> <?php
}
else{
    return _404();
}
