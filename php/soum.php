<?php
// -----------------------------------
// SOUMISSION D'UNE NOUVELLE SHITSTORM
// -----------------------------------

// Navbar
$GLOBALS['navbar']->addElement('soumission', 'Ajouter une shitstorm');

if($GLOBALS['options'] && $GLOBALS['options'] === 'explore_tweets') {
    require PAGES_DIR . 'tweet_saver/view_tweets.php'; return;
}
else if($GLOBALS['options'] && $GLOBALS['options'] === 'tweet_saver') {
    require PAGES_DIR . 'tweet_saver/save_tweet.php'; return;
}

global $connexion;

if(!$GLOBALS['settings']['allowPosting'] || !$GLOBALS['settings']['allowUserPosting'] || !$GLOBALS['settings']['allowUnloggedPosting']){ // On a pas le droit de poster
    if(!$GLOBALS['settings']['allowPosting']){ // Personne n'a le droit de poster
        echo "<h3 class='error'>Il est actuellement impossible de poster une shitstorm.</h3>";
        return;
    }
    else if(!$GLOBALS['settings']['allowUserPosting'] && !isModerator()) { // Les utilisateurs n'ont pas le droit de poster et 'utilisateur n'est pas modérateur
        echo "<h3 class='error'>Il est actuellement impossible de poster une shitstorm.</h3>";
        return;
    }
    else if(!isLogged() && !$GLOBALS['settings']['allowUnloggedPosting']){
        echo "<h3 class='error'>Il est actuellement impossible de poster une shitstorm.</h3>";
        echo "<h5 class='error'>Les utilisateurs connectés peuvent toutefois en soumettre.</h5>";
        echo "<h6><a href='".HTTPS_URL."login'>Connectez-vous</a> pour accéder au formulaire.</h6>";
        return;
    }
}

require(ROOT . '/inc/postDelete.php');

if(isset($_POST['link'], $_POST['dscr'], $_POST['title'], $_POST['link2'], $_POST['link3'], $_POST['link4'], $_POST['date_real'])){
    $link[0] = trim(mysqli_real_escape_string($connexion, $_POST['link']));
    $link[1] = trim(mysqli_real_escape_string($connexion, $_POST['link2']));
    $link[2] = trim(mysqli_real_escape_string($connexion, $_POST['link3']));
    $link[3] = trim(mysqli_real_escape_string($connexion, $_POST['link4']));
    $dscr = trim(mysqli_real_escape_string($connexion, html_entity_decode($_POST['dscr'])));
    $title = trim(mysqli_real_escape_string($connexion, html_entity_decode($_POST['title'])));
    $psu = "";
    $idUsr = 0;
    $date = $_POST['date_real'];
    $pass = false;

    $dateApp = gmdate('Y-m-d H:i:s');
    try{
        $dateT = new DateTime($date);
        $actual = new DateTime($dateApp);

        if($dateT <= $actual && $date){
            $pass = true;
        }
        else{
            echo "<h5 class='error'>La date est invalide.</h5>";
        }
    } catch (Exception $E) {
        echo "<h5 class='error'>La date est invalide.</h5>";
    }
    
    if(mb_strlen($title) > MAX_LEN_TITLE){
        $pass = false;
        echo "<h5 class='error'>Le titre est trop long.</h5>";
    }
    if(mb_strlen($dscr) > MAX_LEN_DSCR){
        $pass = false;
        echo "<h5 class='error'>La description est trop longue.</h5>";
    }

    // Vérif si le title est pris
    $resT = mysqli_query($connexion, "SELECT idSub FROM Shitstorms WHERE title='$title';");

    if($resT && mysqli_num_rows($resT)){
        $pass = false;
        echo "<h5 class='error'>Le titre est déjà utilisé pour une autre shitstorm.</h5>";
    }

    foreach($link as $key => $l){
        if($l != ''){
            if(!preg_match(REGEX_TWITTER_LINK, $l) && !preg_match(REGEX_MASTODON_LINK, $l)){
                echo "<h5 class='error'>Le lien ". ($key+1) ." est invalide.</h5>";
                $pass = false;
            }
        }
        else unset($link[$key]);
    }
    // Les liens valides ne valent désormais pas NULL, ceux vides oui

    // Traitement du possible fichier
    $nom = 1;
    if(isset($_FILES['images'], $_FILES['images']['name'][0]) && $pass && !empty($_FILES['images']['name'][0])){
        for($i = 0; $i < count($_FILES['images']['name']); $i++){
            $nom = traitementFichier('', 'uploaded', false, 
                                    ['name' => $_FILES['images']['name'][$i], 'type' => $_FILES['images']['type'][$i],
                                     'tmp_name' => $_FILES['images']['tmp_name'][$i], 'size' => $_FILES['images']['size'][$i],
                                     'error' => $_FILES['images']['error'][$i]]);
            if($nom){
                $link[] = $nom;
            }
            else{
                echo "<h5 class='error'>Un des fichiers est invalide.</h5>";
                $pass = false;
                break;
            }
        }
        
    }
    // Si aucun fichier, $nom vaut 1 (null si fichier pas valide)
    $answers = [];
    $valid_answers = [];

    if($pass){ // Si tout est déjà bon
        // On vérifie qu'il y a au moins un lien ($link contient une case)
        $pass = (!empty($link) && count($link) < (MAX_LINK_COUNT+1));
        if(!$pass){
            echo "<h5 class='error'>Aucun lien valide n'a été précisé, ou le nombre d'images + liens cumulés dépasse 5.</h5><h6 class='error'>5 liens maximum sont autorisés.</h6>";
        }
        elseif(isset($_POST['pseudo'])){
            $psu = mysqli_real_escape_string($connexion, htmlspecialchars($_POST['pseudo']));

            if(!preg_match(REGEX_USERNAME, $psu)){
                echo "<h5 class='error'>Le nom d'utilisateur choisi contient des caractères invalides.</h5>";
                $pass = false;
            }
        }

        if(isset($_SESSION['connected'], $_SESSION['id']) && $_SESSION['connected']){
            // Ajout des réponses
            for($i = 1; $i <= MAX_ANSWER_COUNT; $i++){
                if(isset($_POST["linkA$i"], $_POST["dateA{$i}_real"], $_POST["dscrA$i"])){
                    addAnswer($answers, $_POST["dateA{$i}_real"], $_POST["dscrA$i"], $_POST["linkA$i"], "imgA$i", $_SESSION['id']);
                }
            }
        }

        foreach($answers as $key => $an){
            $check = verifyAnswerValidy($date, $an['date'], $an['dscr'], $an['link'], $an['filename'], $key);

            if($check !== 0){
                $pass = false;
                break;
            }
            else{ // Réponse valide, on remplit valid answers
                $nomA = $an['link'];
                if($an['link'] == ''){
                    // Traitement du possible fichier
                    $nomA = 1;
                    if(isset($_FILES[$an['filename']]) && $pass && !empty($_FILES[$an['filename']]['name'])){
                        $nomA = traitementFichier($an['filename']);
                    }
                    if(!$nomA || $nomA === 1){
                        echo "<h5 class='error'>Le fichier est invalide pour la réponse $key.</h5>";
                        $pass = false;
                    }
                    // Si aucun fichier, $nomA vaut 1 (null si fichier pas valide)
                }
                // nomA vaut désormais soit le lien, soit le lien vers l'image

                $valid_answers[] = ['link' => $nomA, 'dscr' => $an['dscr'], 'date' => $an['date'], 'idUsr' => $an['idUsr']];
            }
        }
    }

    if($pass && $nom){
        if(isset($_SESSION['connected']) && $_SESSION['connected']){
            $psu = $_SESSION['username'];
            $idUsr = $_SESSION['id'];
        }
        $res = mysqli_query($connexion, "INSERT INTO Waiting (title, dscr, pseudo, idUsr, dateShit) VALUES ('$title', '$dscr', '$psu', '$idUsr', '$date') ;");
        if($res){
            $idSub = mysqli_insert_id($connexion);
            sendNotif('moderation_shitstorm', $idUsr, 9, $idSub);

            foreach($link as $l){
                if($l != NULL){
                    mysqli_query($connexion, "INSERT INTO WaitingLinks (link, idSub) VALUES ('$l', '$idSub');");
                }
            }
            // Insère les réponses à faire attendre
            foreach($valid_answers as $an){
                if($an['link'] != NULL){
                    mysqli_query($connexion, "INSERT INTO WaitAnsw (dscrA, linkA, idUsrA, idSub, dateAn, fromWaitingShitstorm) 
                                              VALUES ('{$an['dscr']}', '{$an['link']}', '{$an['idUsr']}', '$idSub', '{$an['date']}', 1);");
                }
            }

            echo "<h5 class='pass'>La soumission a réussi et a été transmise pour modération.</h5>";
        }
        else{
            echo "<h5 class='error'>L'insertion a échoué.</h5>";
            if($nom !== 1){ // L'insertion a échoué mais un fichier était uploadé
                foreach($link as $nom){
                    deleteFile($nom);
                }
            }
        }
    }
    else {
        // Supprime les fichiers possiblement ajoutés
        foreach($link as $nom){
            deleteFile($nom);
        }

        foreach($valid_answers as $an){
            deleteFile($an['link']);
        }
    }
}

// Regarde si il y a un tweet sauvegardé orphelin disponible
$resTExists = mysqli_query($connexion, "SELECT idSave FROM tweetSave WHERE notLinked=1 LIMIT 0,1;");
$save_exists = $resTExists && mysqli_num_rows($resTExists);

if(!isLogged()){
    echo "<h5>Connectez-vous pour profiter d'options supplémentaires pour cette shitstorm !</h5>
          <h6>Vous pourrez gérer votre publication après soumission, ainsi que lui ajouter directement des réponses</h6>" .
          ($save_exists ? "<h6><a href='".HTTPS_URL."soumission/explore_tweets'>Explorer les tweets sauvegardés</a></h6>" : '');
}
else {
    echo "<h6>Vous avez un tweet provocateur sous la main, mais il n'a pas encore déclenché de shitstorm ?</h6>";
    echo "<h6><a href='".HTTPS_URL."soumission/tweet_saver'>Sauvegarder un tweet de façon préventive</a>".
    ($save_exists ? " &bull; <a href='".HTTPS_URL."soumission/explore_tweets'>Explorer les tweets sauvegardés</a></h6>" : '');
}

echo "<div class='divider' style='margin: 15px 0;'></div>";
$dateDuJour = date('Y-m-d');

?>
<div class="row">
    <div class="col s12">
        <div class='black-text' id='msg'></div>
        <form name='composeShitstorm' id='composeShitstorm' action="#" method='post' enctype="multipart/form-data">
            <ul class="collapsible popout" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header hoverable"><i class="material-icons">timeline</i>Résumé de la shitstorm</div>
                    <div class="collapsible-body collap-body-custom">
                        <h5 id='shit_title'>Titre de la shitstorm</h5>
                        <p id='shit_content'>Description de la shitstorm</p>
                        <div class='divider' style='margin-bottom: .7em;'></div>
                        <span>Shitstorm du </span><span id='shit_date'>1er janvier 1970</span><br/>
                        <div class='divider' style='margin: .7em 0;'></div>
                        <h6>Liens de la shitstorm</h6>
                        <div id='shit_links'>
                            <div class='col s12' id='shit_link_1'>Aucun lien n'est défini.</div>
                            <div class='col s12' id='shit_link_2'></div>
                            <div class='col s12' id='shit_link_3'></div>
                            <div class='col s12' id='shit_link_4'></div>
                        </div>
                        <div id='shit_images' class='col s12'></div>
                        <div class='clearb'></div>
                    </div>
                </li>
            </ul>

            <ul class="collapsible popout" data-collapsible="expandable">
                <li>
                    <div class="collapsible-header active hoverable"><i class="material-icons">format_quote</i>Propriétés de la shitstorm</div>
                    <div class="collapsible-body collap-body-custom">
                        <?php 
                        if(!isset($_SESSION['connected']) || !$_SESSION['connected']){ ?>
                            <div class="input-field">
                                <input type='text' name='pseudo' required maxlength='16'>
                                <label for="pseudo">Pseudo &agrave; utiliser</label>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="input-field">
                            <input maxlength='40' type='text' id='title' name='title' required <?= (isset($_POST['title']) ? "value='".htmlspecialchars($_POST['title'], ENT_QUOTES)."'" : '') ?>>
                            <label for="title">Titre de la shitstorm</label>
                        </div>
                        <textarea name="dscr" id='dscr' rows="7" cols="70" placeholder="Description de la shitstorm" maxlength="<?= strval(MAX_LEN_DSCR) ?>"
                            class="materialize-textarea txa" required><?= (isset($_POST['dscr']) ? htmlspecialchars($_POST['dscr']) : '') ?></textarea>
                        <p>
                            <label for="datepicker">Date de la shitstorm</label>
                            <input class="datepicker" type="text" id="datepicker" name="date">
                        </p>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header hoverable"><i class="material-icons">assignment</i>Liens de la shitstorm</div>
                    <div class="collapsible-body collap-body-custom">
                        <div class="card-content" style='padding-top: 0;'>
                            <div class="input-field">
                                <input type='url' id='link' class='validate link-soumission' name='link' maxlength='500' <?= (isset($_POST['link']) ? "value='".htmlspecialchars($_POST['link'])."'" : '') ?>>
                                <label for="link">Lien vers tweet/toot 1</label>
                            </div>
                            <div class="input-field">
                                <input type='url' id='link2' class='validate link-soumission' name='link2' maxlength='500' <?= (isset($_POST['link2']) ? "value='".htmlspecialchars($_POST['link2'])."'" : '') ?>>
                                <label for="link2">Lien vers tweet/toot 2</label>
                            </div>
                            <div class="input-field">
                                <input type='url' id='link3' class='validate link-soumission' name='link3' maxlength='500' <?= (isset($_POST['link3']) ? "value='".htmlspecialchars($_POST['link3'])."'" : '') ?>>
                                <label for="link3">Lien vers tweet/toot 3</label>
                            </div>
                            <div class="input-field">
                                <input type='url' id='link4' class='validate link-soumission' name='link4' maxlength='500' <?= (isset($_POST['link4']) ? "value='".htmlspecialchars($_POST['link4'])."'" : '') ?>>
                                <label for="link4">Lien vers tweet/toot 4</label>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header hoverable"><i class="material-icons">filter</i>Images de la shitstorm</div>
                    <div class="collapsible-body collap-body-custom">
                        <div class="file-field input-field">
                            <input type='hidden' name='MAX_FILE_SIZE' value='1048576'>
                            <div class="btn">
                                <span>Images</span>
                                <input id='imagesInput' type="file" multiple name='images[]' accept="image/png, image/jpeg">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text" placeholder="Cinq images maximum, 1 Mo max, PNG/JPG">
                            </div>
                        </div>
                    </div>
                </li>
            </ul>

            <?php if(isset($_SESSION['connected'], $_SESSION['id']) && $_SESSION['connected']){ ?>
                <!-- Réponses -->
                <ul class="collapsible popout" data-collapsible="expandable">
                <?php 
                for($i = 1; $i <= MAX_ANSWER_COUNT; $i++){ ?>
                    <li>
                        <div class="collapsible-header hoverable"><i class="material-icons">reply</i>Réponse <?= $i ?></div>
                        <div class="collapsible-body collap-body-custom">
                            <div class="input-field">
                                <input type='url' name='linkA<?= $i ?>'
                                    <?= (isset($_POST["linkA$i"]) ? "value='".htmlspecialchars($_POST["linkA$i"], ENT_QUOTES)."'" : '') ?> maxlength='2000'>
                                <label for="linkA<?= $i ?>">Lien vers tweet/toot (1 autorisé, pas d'image si lien)</label>
                            </div>
                            <div class="file-field input-field">
                                <input type='hidden' name='MAX_FILE_SIZE' value='1048576'>
                                <div class="btn">
                                    <span>Image</span>
                                    <input type="file" name='imgA<?= $i ?>' accept="image/png, image/jpeg">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="1 Mo max, type PNG/JPG">
                                </div>
                            </div>
                            <textarea name="dscrA<?= $i ?>" rows="5" cols="70" placeholder="Description rapide de la réponse (facultatif)"
                            maxlength="<?= MAX_LEN_ANSWER_DSCR ?>" class="materialize-textarea"><?= (isset($_POST["dscrA$i"]) ? htmlspecialchars($_POST["dscrA$i"]) : '') ?></textarea>

                            <p>
                                <label for="datepicker<?= $i ?>">Date de la réponse</label>
                                <input class="datepicker datepicker_answer" type="text" id="datepicker<?= $i ?>" name="dateA<?= $i ?>">
                            </p>
                            <div class='clearb'></div>
                        </div>
                    </li>
                <?php } ?>
                </ul>
            <?php } ?>
            
            <div class='clearb'></div>
            <button type="submit" name='send' class="btn col s12 l10 offset-l1 purple lighten-1 btn-personal-mini" style='margin-top: 1em; margin-bottom: .5em;'><?php 
                if(isset($_SESSION['storm']) && $_SESSION['storm']) 
                    echo "Déclencher la tempête" ;
                else
                    echo "Envoyer la shitstorm";
                ?>
            </button>
        </form>
        <div class='clearb'></div>
        <div class="col s12" id='cardContainer' style="padding-top: 1.5em;"></div>
    </div>
</div>
<script src='<?= HTTPS_URL ?>js/date.js'></script>
<script src='<?= HTTPS_URL ?>js/soum.js'></script>
