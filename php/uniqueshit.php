<?php
// -----------------------------------------------------
// VUE UNIQUE D'UNE SHITSTORM RECUPEREE PAR GET[idPubli]
// -----------------------------------------------------

if(!isset($_GET['idPubli'])){
    if(isset($GLOBALS['current_page']) && isset($GLOBALS['current_page'][2]) && $GLOBALS['current_page'][2] != ''){
        $_GET['idPubli'] = urldecode($GLOBALS['current_page'][2]);
    }
    elseif(isset($GLOBALS['is_included'])) { include(PAGES_DIR . 'info/404.php'); return; }
    else {header("Location: /404"); exit();}
}

global $connexion;
require_once(ROOT . '/inc/postDelete.php');

$idUsrP = 0;
$idSub = mysqli_real_escape_string($connexion, $_GET['idPubli']);
$pubCom = NULL;
$row = NULL;
$done = false;
$queryAnswer = false;

// Vérifie l'existance de la shitstorm
if((int)$idSub > 0){
    $idSub = (int)$idSub;
    $res = mysqli_query($connexion, "SELECT idUsr, idSub, title FROM Shitstorms WHERE idSub='$idSub';");
}
else {
    $idSub = substr(trim($idSub), 0, strlen($idSub)-1);
    // Supprime le - terminal obligatoire qui sert à le différencier d'un nombre

    $res = mysqli_query($connexion, "SELECT idUsr, idSub, title FROM Shitstorms WHERE title='$idSub';");
}

if($res && mysqli_num_rows($res) > 0){
    $row = mysqli_fetch_assoc($res);
    $idSub = $row['idSub'];
    // echo "<a href='". getShitstormLink($row['title']) ."'>Lien lien lien</a>";

    if(isset($_POST['pubCom'], $_POST['in_reply_to_id']) && isset($_SESSION['id']) && isset($_SESSION['connected']) && $_SESSION['connected']){
        // publierCommentaire($idSub, $pubCom, $row['idUsr']); 
    }

    $res = mysqli_query($connexion, "SELECT *, (SELECT COUNT(*) FROM Comment WHERE idSub='$idSub') c FROM Shitstorms WHERE idSub='$idSub';");

    if($res && mysqli_num_rows($res) > 0){
        $done = true;
        $row = mysqli_fetch_assoc($res);
    }
}

// Formation du breadcrumb
$GLOBALS['navbar']->addElement('journal', 'Journal');

if($done){
    $year = explode("-", $row['dateShit']);
    $month = getTextualMonth($year[1]);

    $GLOBALS['navbar']->addElement("journal/{$row['dateShit']}?from=$idSub#pub$idSub", "$month {$year[0]}");
    $GLOBALS['navbar']->addElement("shitstorm/{$row['idSub']}", htmlspecialchars($row['title']));
}
else{
    $GLOBALS['navbar']->addElement('404', 'Erreur 404');
}

if(!$res || @mysqli_num_rows($res) == 0){
    echo "<h4>Aucune shitstorm correspondant à l'identifiant n'a été trouvée.</h4>";
}
else {
    require(PAGES_DIR . 'shitstorm/unique.php');

    generateUniqueShitstorm($idSub, (isset($_GET['force_trimmed']) && isActive($_GET['force_trimmed']) ? false : true));
}
