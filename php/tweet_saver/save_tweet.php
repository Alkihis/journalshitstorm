<?php

// Save a tweet :
// Enter a tweet ID (or a tweet link), save it if necessary, and view it

$GLOBALS['navbar']->addElement('soumission/tweet_saver', 'Sauvegarder un tweet');
$GLOBALS['pageTitle'] = "Sauvegarder un tweet - Journal des Shitstorms";

if(!isLogged()) {
    include(PAGE403); return; exit();
}

?>

<div class='row'>
    <div class='col s12 l10 offset-l1'>
        <p class='flow-text' style='margin-top: 0'>
            Sauvegardez ici un tweet qui pourrait servir dans une shitstorm plus tard.
            Si jamais le tweet venait à être supprimé entre temps, une sauvegarde restera dans le Journal des Shitstorms.<br>
            Si le tweet n'est pas intégré à une shitstorm (ou une réponse à une shitstorm) dans une période de deux semaines, il sera supprimé.
        </p>

        <div class='card-user-new border-user' style='height: auto !important; margin-top: 8px; margin-bottom: 8px;'>
            <form id='saveATweet'>
                <div class='input-field col s12'>
                    <input id='tweetID' type='text'>
                    <label for='tweetID'>Lien ou identifiant du tweet</label>
                </div>

                <button type='button' class='btn card-user-new border-user attenuation right btn-personal btn-save-tweet'>
                    Sauvegarder
                </button>
                <div class='clearb'></div>
            </form>
        </div>

        <div style='margin: 30px 0;'>
            <div id='placeholdertweet'></div>
        </div>
        <div class='clearb'></div>
    </div>
</div>

<script src='<?= HTTPS_URL ?>js/tweet_saver/save_tweet.js' async></script>
