<?php

// View saved unused tweet
$GLOBALS['navbar']->addElement('soumission/explore_tweets', 'Sauvegardes de tweets');
$GLOBALS['pageTitle'] = "Tweets sauvegardés - Journal des Shitstorms";

?>

<div class='row'>
    <div class='col s12 l10 offset-l1'>
        <p class='flow-text' style='margin-top: 0'>
            Vous pouvez explorer ici les tweets sauvegardés par les utilisateurs qui n'ont pas encore été
            intégré à une shitstorm. Servez-vous, c'est gratuit !
        </p>

        <div style='margin-top: 30px; margin-bottom: 40px;' id='main_tweet_ph'>
            <div class='flow-text black-text center show-if-only-child'>Aucun tweet sauvegardé non associé n'est disponible.</div>
        </div>
    </div>
</div>

<script src='<?= HTTPS_URL ?>js/tweet_saver/view_tweets.js' async></script>
