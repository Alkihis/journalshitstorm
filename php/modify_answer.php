<?php

// -----------------------
// MODIFICATION DE REPONSE
// -----------------------

require(ROOT . '/inc/postDelete.php');

if(!isset($_SESSION['id']) || !isset($_SESSION['mod'])){
    if(isset($GLOBALS['is_included'])) { include(PAGE403); return; }
    else {header("Location:403"); exit();}
}

if(!$GLOBALS['settings']['allowUserModification'] || !$GLOBALS['settings']['allowAllModification']){ // On a pas le droit de modifier
    if(!$GLOBALS['settings']['allowAllModification']){ // Personne n'a le droit de modifier
        echo "<h3 class='error'>Il est actuellement impossible de modifier une shitstorm.</h3>";
        return;
    }
    else if(!$GLOBALS['settings']['allowUserModification'] && !isModerator()) { // Les utilisateurs n'ont pas le droit de modifier et l'utilisateur n'est pas modérateur
        echo "<h3 class='error'>Il est actuellement impossible de modifier une shitstorm.</h3><h5>Vous pouvez demander de l'aide à un modérateur pour plus d'informations.</h5>";
        return;
    }
}

global $connexion;

$idAn = null;
// Récupération de l'ID
if(isset($_GET['idAn'])){
    $idAn = (int)mysqli_real_escape_string($connexion, $_GET['idAn']);
}
else{
    $idAn = (int)($GLOBALS['options'] ?? 0);
}

if($idAn <= 0){
    echo "<h3 class='error'>L'ID est incorrect.</h3>";
}
else {
    $isWait = false;
    $toCheck = false;
    $table = 'Shitstorms';
    $isWaitShit = false;
    $res = null;
    // Est-ce une réponse en attente ?
    if(isset($_GET['waiting']) && ($_GET['waiting'] === '1' || $_GET['waiting'] === 't' || $_GET['waiting'] === 'true')){
        $isWait = true;

        if(!(isset($_SESSION['mod']) && $_SESSION['mod'] > 0)){
            $toCheck = true;
        }
        if(isset($_GET['waiting_shit']) && isActive($_GET['waiting_shit'])){
            $isWaitShit = true;
            $table = 'Waiting';
        }
    }

    if($isWait){
        $res = mysqli_query($connexion, "SELECT a.*, s.dateShit, s.title FROM WaitAnsw a JOIN $table s ON a.idSub=s.idSub WHERE idSubA='$idAn' ".($toCheck ? ' AND a.locked=0' : '').";");
    }
    else{
        $res = mysqli_query($connexion, "SELECT a.*, s.dateShit, s.title FROM Answers a JOIN Shitstorms s ON a.idSub=s.idSub WHERE idAn='$idAn';");
    }
    
    $row = NULL;

    if($res && mysqli_num_rows($res) > 0) {
        $row = mysqli_fetch_assoc($res);
        if($_SESSION['mod'] == 0 && $_SESSION['id'] != $row['idUsrA']){
            include(PAGE403); return;
        }
        else {
            // L'utilisateur a les droits
            // Traitement suppression
            if(isset($_GET['delete']) && isset($_COOKIE['token'])){
                if(md5($_COOKIE['token'] . $idAn) == $_GET['delete']){
                    deleteOneAnswer($isWait, $idAn);

                    echo "<h5 class='error'>La réponse a été supprimée.</h5>";
                    return;
                    exit();
                }
            }

            // Traitement du POST
            // Changement lien / image
            if(isset($_POST['newlink']) || isset($_FILES['img1'])){
                $pass = false;
                $nl = $row['linkA'];

                // Si lien
                if(isset($_POST['newlink'])){
                    $newlink = trim(mysqli_real_escape_string($connexion, $_POST['newlink']));
                    $pass = true;
    
                    if(!preg_match(REGEX_TWITTER_LINK, $newlink) && !preg_match(REGEX_MASTODON_LINK, $newlink)){
                        echo "<h5 class='error'>Le lien est invalide.</h5>";
                        $pass = false;
                    }
                    
                    if($pass){
                        // Modification du lien
                        $nl = $newlink;
                    }
                }
                // Si c'est une image
                elseif(isset($_FILES['img1']) && !empty($_FILES['img1']['name'])){
                    $nom = 1;
                    $nom = traitementFichier();
                    if(!$nom || $nom === 1){
                        echo "<h5 class='error'>Le fichier est invalide.</h5>";
                    }
                    else{
                        $nl = $nom;
                        $pass = true;
                    }
                }

                if($nl != $row['linkA']){ // Mise à jour de la réponse si le lien a changé
                    if(!$isWait){
                        updateAnswer($idAn, $nl);
                    }
                    else{
                        $respost = mysqli_query($connexion, "UPDATE WaitAnsw SET linkA='$nl' WHERE idSubA='$idAn';");
                    }
                }

                if($pass){
                    // Suppression de l'image contenue dans l'ancien lien si ça en est une
                    deleteFile($row['linkA']);

                    // Sauvegarde du nouveau lien dans les variables actuelles
                    $row['linkA'] = $nl;

                    echo "<h5 class='pass'>Le lien a été modifié.</h5>";
                }
            }

            // Changement description
            elseif(isset($_POST['dscr']) && isset($_POST['dateShit'])){
                $rawdscr = trim(html_entity_decode($_POST['dscr']));
                $dscr = mysqli_real_escape_string($connexion, $rawdscr);
                $date = mysqli_real_escape_string($connexion, $_POST['dateShit']);

                // Vérif date par rapport à la shitstorm
                $res = mysqli_query($connexion, "SELECT dateShit FROM $table WHERE idSub='{$row['idSub']}';");

                if(mb_strlen($dscr) > MAX_LEN_ANSWER_DSCR){
                    echo "<h5 class='error'>La description est trop longue.</h5>";
                }
                elseif($res && mysqli_num_rows($res) > 0){
                    $row2 = mysqli_fetch_assoc($res);
                    $dateApp = date('Y-m-d H:i:s');
                    $dateShit = $row2['dateShit'];
                    $pass = false;
                    
                    try{
                        $dateT = new DateTime($date);
                        $actual = new DateTime($dateApp);
                        $ds = new DateTime($dateShit);
            
                        if($dateT <= $actual && $dateT >= $ds){
                            $pass = true;
                        }
                    } catch (Exception $E) { }

                    if($pass){
                        // Sauvegarde des données
                        if(!$isWait){
                            $respost = mysqli_query($connexion, "UPDATE Answers SET dateAn='$date', dscrA='$dscr' WHERE idAn='$idAn';");
                        }
                        else{
                            $respost = mysqli_query($connexion, "UPDATE WaitAnsw SET dateAn='$date', dscrA='$dscr' WHERE idSubA='$idAn';");
                        }
                        $row['dscrA'] = $rawdscr;
                        $row['dateAn'] = $date;
                        echo "<h5 class='pass'>Les métadonnées ont bien été mises à jour.</h5>";
                    }
                    else {
                        echo "<h5 class='error'>La date est invalide.</h5>";
                    }
    
                }
                else {
                    echo "<h5 class='error'>La shitstorm associée à la réponse n'existe pas.</h5>";
                }
            }

            $date = explode('-', $row['dateShit']);
            $month = $date[1];
            $year = $date[0];

            // Navbar
            $GLOBALS['navbar']->addElement('journal?date='.$row['dateShit'], getTextualMonth($month)." $year");
            $GLOBALS['navbar']->addElement(($isWait ? '#!' : 'shitstorm/'.$row['idSub']), htmlspecialchars($row['title']));
            $GLOBALS['navbar']->addElement('modify_an/'.($isWait ? $row['idSubA'] : $row['idAn']).($isWait ? '?waiting=t' : '').($isWaitShit ? '&waiting_shit=t' : ''), 'Modification de réponse');

            // Affichage de la réponse ?>

            <div class="row">
                <div class="col s12 l10 offset-l1">
                    <!-- Lien -->
                    <ul class="collapsible" data-collapsible="expandable">
                        <li>
                            <div class='collapsible-header active hoverable'><i class='material-icons'>insert_link</i>Lien de la réponse</div>
                            <div class="collapsible-body collap-body-custom">
                                <?php viewLink($row['linkA'], 1, ($isWait ? null : $row['idAn']), true) ?>
                                <div class='clearb'></div>
                            </div>
                        </li>
                    </ul>
                    <!-- Métadonnées (date, titre et description) -->
                    <ul class="collapsible" data-collapsible="accordion">
                        <li>
                            <div class="collapsible-header active hoverable"><i class="material-icons">format_quote</i>Propriétés de la réponse</div>
                            <div class="collapsible-body collap-body-custom">
                                <form method='post' action='#'>
                                    <div class='input-field'>
                                        <label for='dscr'>Description</label>
                                        <textarea id='dscr' name="dscr" rows="7" cols="70" placeholder="Description de la réponse (max 1000 caract&egrave;res)" maxlength="1000" class="materialize-textarea"><?= htmlspecialchars($row['dscrA'], ENT_QUOTES, 'UTF-8') ?></textarea>
                                    </div>   
                                    <label for="date">Date de la réponse</label>
                                    <input id="date" type="date" name="dateShit" max="<?= date('Y-m-d'); ?>" value='<?= htmlentities($row['dateAn']); ?>'>
                                    <button type="submit" name='savedscr' class="btn col s12 l4 purple lighten-1 btn-personal-mini right" style="margin-bottom: 1em;">Modifier</button>
                                    <div class='clearb'></div>
                                </form>
                            </div>
                        </li>
                    </ul>

                    <!-- Changement de lien ou d'image -->
                    <ul class="collapsible" data-collapsible="accordion">
                        <li>
                            <div class="collapsible-header hoverable"><i class="material-icons">add_circle</i>Changer le lien</div>
                            <div class="collapsible-body collap-body-custom">
                                <form method='post' action='#'>
                                    <div class="input-field">
                                        <input type='url' name='newlink' maxlength='1000'>
                                        <label for="newlink">Nouveau lien</label>
                                    </div>
                                    <button type="submit" name='savelink' class="btn col s12 l4 blue lighten-1 btn-personal-mini right" style="margin-bottom: 1em;">Modifier</button>
                                </form>
                                <div class='clearb'></div>
                            </div>
                        </li>
                        <li>
                            <div class="collapsible-header hoverable"><i class="material-icons">image</i>Remplacer par une image</div>
                            <div class="collapsible-body collap-body-custom">
                                <form method='post' action='#' enctype="multipart/form-data">
                                    <div class="file-field input-field">
                                        <input type='hidden' name='MAX_FILE_SIZE' value='1048576'>
                                        <div class="btn">
                                            <span>Image</span>
                                            <input type="file" name='img1' accept="image/png, image/jpeg">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" placeholder="1 Mo max, type PNG/JPG">
                                        </div>
                                    </div>
                                    <button type="submit" name='saveimg' class="btn col s12 l4 green lighten-1 btn-personal-mini right" style="margin-bottom: 1em;">Modifier</button>
                                </form>
                                <div class='clearb'></div>
                            </div>
                        </li>
                    </ul>

                    <div class="card card-border" style="padding-top: .5em;">
                        <div class='clearb' style='padding: 1.5em; padding-top: 0.8em;'>
                            <a href="#modaldelan" class="waves-effect waves-light modal-trigger btn col s12 red lighten-1 btn-personal-mini">Supprimer la réponse</a>
                        </div>
                        <div class='clearb' style='padding-bottom : 1.5em;'></div>
                    </div>

                    <!-- Modal -->
                    <div id="modaldelan" class="modal bottom-sheet">
                        <div class="modal-content">
                            <h4>Voulez-vous vraiment supprimer la réponse ?</h4>
                        </div>
                        <div class="modal-footer" style='padding-left: 1.5em;'>
                            <a href="<?= HTTPS_URL ?>modify_an/<?= ($isWait ? $row['idSubA'].'?waiting=t'.($isWaitShit ? '&waiting_shit=t' : '') : $row['idAn']) ?>&delete=<?= (isset($_COOKIE['token']) ? md5($_COOKIE['token'] . $idAn) : 0) ?>" 
                                class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 red lighten-1 left white-text">
                                <i class='material-icons left'>clear</i>Oui
                            </a>
                            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat col s3 l1 blue lighten-1 left white-text" style='margin-left : 2em;'>
                                <i class='material-icons left'>keyboard_capslock</i>Non
                            </a>
                            <div class='clearb'></div>
                        </div>
                    </div>
                </div>
            </div>

            <?php
        }
    }
    else {
        echo "<h4>Aucune réponse correspondant à l'identifiant n'a été trouvée.</h4>";
    }
}
