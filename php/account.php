<?php
// ------------------------------------------------------
// PAGE DE GESTION DU COMPTE DE L'UTILISATEUR AUTHENTIFIE
// ------------------------------------------------------

if(!isset($_SESSION['connected'], $_SESSION['id'], $_SESSION['username']) || !$_SESSION['connected']){
    if(isset($GLOBALS['is_included'])) { include(PAGE403); return; }
    else {header("Location: /403"); exit();}
}
global $connexion;

$res = mysqli_query($connexion, "SELECT * FROM Users WHERE id='{$_SESSION['id']}';");
if(!$res){
    if(isset($GLOBALS['is_included'])) { include(PAGE403); return; }
    else {header("Location: /403"); exit();}
}
$rowUser = mysqli_fetch_assoc($res);

$dialog = '';

// TRAITEMENT DES FORMULAIRES
// --------------------------

if(isset($_GET['token']) && $_GET['token']){
    if(!$rowUser['confirmedMail']){
        // Calcul si le token est encore valide (24H)
        $dateLast = new DateTime($rowUser['lastCMailSent'] . ' UTC');
        $dateLast = $dateLast->add(new DateInterval('P1D')); // Ajoute 1 jour (Période de 1 Day)
        $dateActual = new DateTime(gmdate('Y-m-d H:i:s e')); // e est la timezone (UTC)

        if($dateLast < $dateActual){ // Si ça fait plus de 1 jour depuis le dernier mail
            $dialog .= "<h5 class='error'>Le lien n'est plus valide.</h5><h6>Utilisez le module ci-dessous pour renvoyer un e-mail de confirmation.</h6>";
        }
        else{
            $token_mail = $_GET['token'];
            // Le token constitue la chaîne entre le 10è et le 30è caractères du mot de passe (60 chars)
        
            $real_token = md5(substr($rowUser['passw'], 9, 29-9) . $rowUser['lastCMailSent']);
            if($real_token != $token_mail){
                $dialog .= "<h5 class='error'>Impossible de vérifier votre compte.</h5><h6>Avez-vous changé de mot de passe depuis l'envoi de l'e-mail de validation ?</h6>";
            }
            else{
                mysqli_query($connexion, "UPDATE Users SET confirmedMail=1 WHERE id='{$rowUser['id']}';");
                $rowUser['confirmedMail'] = true;
                $dialog .= "<h5 class='pass'>L'adresse e-mail a été confirmée.</h5>";
            }
        }
        
    }
    else{
        $dialog .= "<h5 class='pass'>Votre adresse e-mail a déjà été confirmée.</h5>";
    }
}

if(isset($_POST['changePSW'])){
    $psw = mysqli_real_escape_string($connexion, $_POST['psw']);
    if($_POST['newpsw'] == $_POST['reppsw'] && $psw != $_POST['newpsw']){
        if($rowUser['passw'] === '' || password_verify($psw, $rowUser['passw'])){ // Si le mot de passe d'origine est vide, on ne vérifie pas
            $newpsw = mysqli_real_escape_string($connexion, $_POST['newpsw']);
            if(!preg_match(REGEX_PASSWORD, $newpsw)){
                $dialog .= "<h5>Le mot de passe ne contient pas les prérequis demandés.</h5>";
            }
            else{
                $hashnew = password_hash($newpsw, PASSWORD_BCRYPT);
                $res3 = mysqli_query($connexion, "UPDATE Users SET passw='$hashnew' WHERE id='{$rowUser['id']}';");
                if($res3){
                    $dialog .= "<h5>Vos changements ont été pris en compte.</h5>";
                }
                else{
                    $dialog .= "<h5 class='error'>Impossible de valider vos changements.</h5>";
                }
            }
        }
        else{
            $dialog .= "<h5 class='error'>Votre ancien mot de passe est invalide.</h5>";
        }
    }
    else{
        $dialog .= "<h5 class='error'>Les mots de passe ne correspondent pas ou le nouveau mot de passe est identique au précédent.</h5>";
    }
}
elseif(isset($_POST['deleteAccount'], $_POST['confirmPsw'])){
    require_once(INC_DIR . 'postDelete.php');

    $idUsr = $_SESSION['id'];
    if(password_verify($_POST['confirmPsw'], $rowUser['passw'])){
        if(deleteUser($idUsr)){
            echo "<h4 class='error'>Votre compte a été supprimé.</h4>";
            disconnect();
            return; exit();
        }
    }
    else{
        $dialog .= "<h4 class='error'>Impossible de supprimer votre compte. Le mot de passe est incorrect.</h4>";
    }
}
elseif(isset($_POST['resetAccount'], $_POST['confirmPsw'])){
    require_once(INC_DIR . 'postDelete.php');

    $idUsr = $_SESSION['id'];
    if(password_verify($_POST['confirmPsw'], $rowUser['passw'])){
        $delete = false;
        if(!isset($_POST['anonymyse'])){
            $delete = true;
        }

        if(resetUser($idUsr, $delete)){
            $dialog .= "<h4 class='error'>Votre compte a été réinitialisé. Vous pouvez recharger cette page.</h4>";
            return; exit();
        }
    }
    else{
        $dialog .= "<h4 class='error'>Impossible de réinitialiser votre compte. Le mot de passe est incorrect.</h4>";
    }
}

// Navbar
// $GLOBALS['navbar']->addElement('profil/'.$_SESSION['username'], 'Mon profil');
$GLOBALS['navbar']->addElement('compte', 'Paramètres');

// Récupération de la page désirée
$acc_page = 0; // Propriétés du compte
if(!isset($GLOBALS['options']) || !$GLOBALS['options'] || $GLOBALS['options'] === 'content'){
    // pass
}
else if($GLOBALS['options'] === 'password'){
    $acc_page = 1; // Mot de passe
}
else if($GLOBALS['options'] === 'visual'){
    $acc_page = 2; // Options visuelles
}
else if($GLOBALS['options'] === 'profile'){
    $acc_page = 3; // Modification du profil
}
else if($GLOBALS['options'] === 'mysubs'){
    $acc_page = 4; // Mes commentaires
}
else if($GLOBALS['options'] === 'others'){
    $acc_page = 5; // Autres
}
else if($GLOBALS['options'] === 'twitter'){
    $acc_page = 6; // Twitter
}
else if($GLOBALS['options'] === 'apps'){
    $acc_page = 7; // Applications liées
}

// DEBUT AFFICHAGE
// ---------------

?>
<style>
    header, main, footer {
      padding-left: 300px;
    }

    @media only screen and (max-width : 992px) {
      header, main, footer {
        padding-left: 0;
      }
    }
</style>

<div class='clearb'></div>
<script src='<?= HTTPS_URL ?>js/account.js'></script>

<ul id="slide-out" class="side-nav fixed side-nav-account">
    <li>
        <div class="user-view">
            <div class="background default-color"></div>
            <a href="<?= HTTPS_URL ?>profil/<?= $_SESSION['username'] ?>"><img class="circle side-nav-pp" src="<?= $_SESSION['profile_img'] ?>"></a>
            <a href="<?= HTTPS_URL ?>profil/<?= $_SESSION['username'] ?>"><span class="name-side-nav name"><?= htmlspecialchars($_SESSION['realname']) ?></span></a>
        </div>
    </li> 
    <li><a class='grey-text disabled nav-header' href='#!'>Paramètres</a></li>
    <li data-ele="3" class="account-menu-element <?= ($acc_page ===  3 ? 'active' : '') ?>"><a><span class='card-pointer'>Modification du profil</span></a></li>
    <li data-ele="4" class="account-menu-element <?= ($acc_page ===  4 ? 'active' : '') ?>"><a><span class='card-pointer'>Mes commentaires</span></a></li>
    <li><div class='divider' style='margin-bottom: .5em'></div></li>
    <li data-ele="0" class="account-menu-element <?= ($acc_page ===  0 ? 'active' : '') ?>"><a><span class='card-pointer'>Propriétés du compte</span></a></li>
    <li data-ele="1" class="account-menu-element <?= ($acc_page ===  1 ? 'active' : '') ?>"><a><span class='card-pointer'>Mot de passe</span></a></li>
    <li data-ele="2" class="account-menu-element <?= ($acc_page ===  2 ? 'active' : '') ?>"><a><span class='card-pointer'>Options visuelles</span></a></li>
    <li><div class='divider' style='margin-bottom: .5em'></div></li>
    <li data-ele="6" class="account-menu-element <?= ($acc_page ===  6 ? 'active' : '') ?>"><a><span class='card-pointer'>Compte Twitter</span></a></li>
    <li data-ele="7" class="account-menu-element <?= ($acc_page ===  7 ? 'active' : '') ?>"><a><span class='card-pointer'>Applications liées</span></a></li>
    <li data-ele="5" class="account-menu-element <?= ($acc_page ===  5 ? 'active' : '') ?>"><a><span class='card-pointer'>Autres</span></a></li>
</ul>

<?php
require(PAGES_DIR . 'account/account_pages.php');

echo '<div id="profile-modification-container">';
if($acc_page === 3) { // Modification du profil
    $GLOBALS['turn_off_container'] = true;
    $GLOBALS['navbar']->addElement('compte/profile', 'Modification profil'); 

    loadAccountModificationProfile($rowUser, '');
}
echo '</div>';

echo "<div id='menu-bar-button' class='row hide-on-large-only'>
        <a href='#' data-activates='slide-out' class='hide-on-large-only button-collapse'>
            <div class='btn btn-personal card-user-new border-user attenuation' style='width: 100% !important'>
                <i class='material-icons left'>menu</i>Menu
            </div>
        </a>
    </div>";
echo $dialog;

echo '<div id="account-container">';
if($acc_page === 0){ // Propriétés du compte 
    $GLOBALS['navbar']->addElement('compte/content', 'Propriétés');
    loadAccountProprieties($rowUser);
}
else if($acc_page === 1){ // Mot de passe 
    $GLOBALS['navbar']->addElement('compte/password', 'Mot de passe');
    loadAccountPassword($rowUser);
}
else if($acc_page === 2) { // Options visuelles 
    $GLOBALS['navbar']->addElement('compte/visual', 'Visuel');
    loadAccountVisual($rowUser);
}
else if($acc_page === 4) { // Mes soumissions 
    $GLOBALS['navbar']->addElement('compte/mysubs', 'Commentaires'); 
    loadMySubs($rowUser);
}
else if($acc_page === 5) { // Autres 
    $GLOBALS['navbar']->addElement('compte/others', 'Autres');
    loadAccountOthers($rowUser);
}
else if($acc_page === 6) { // Compte Twitter 
    $GLOBALS['navbar']->addElement('compte/twitter', 'Compte Twitter'); 
    loadTwitterAccount($rowUser);
}
else if($acc_page === 7) { // Applications liées
    $GLOBALS['navbar']->addElement('compte/apps', 'Applications liées'); 
    loadAccountLinkedApps($rowUser);
}
echo '</div>';

require(PAGES_DIR . 'account/account_modals.php');
generateAccountModals($rowUser['followable']); ?>

<script src="<?= HTTPS_URL ?>js/croppie.min.js"></script>
<link rel="stylesheet" href="<?= HTTPS_URL ?>css/croppie.css">
