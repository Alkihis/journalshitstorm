<?php

function getID($username) : int { // Renvoie une chaîne avec l'ID de l'utilisateur si réussi, 0 sinon
    if(is_numeric($username)){
        return (int)$username;
    }
    global $connexion;

    $res = mysqli_query($connexion, "SELECT id FROM Users WHERE usrname='$username';");
    if($res && mysqli_num_rows($res) > 0){
        $row = mysqli_fetch_assoc($res);
        return $row['id'];
    }
    return 0;
}

function getUsersIdByStatus(int $status) : array { // Renvoie un tableau avec tous les utilisateurs qui ont le statut voulu
    global $connexion;
    $users = [];

    $res = mysqli_query($connexion, "SELECT id FROM Users ". ($status === -1 ? '' : "WHERE moderator='$status'") .";");
    if($res && mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_assoc($res)){
            $users[] = $row['id'];
        }
    }
    return $users;
}

if(!isset($_SESSION['mod']) || $_SESSION['mod'] == 0 || !isset($_SESSION['id'])){
    if(isset($GLOBALS['is_included'])) { include('php/info/403.php'); return; }
    else {header("Location:403"); exit();}
}

$GLOBALS['navbar']->addElement('send_group_mail', 'Envoi d\'un mail groupé');

if(isset($_POST['person'], $_POST['sujet'], $_POST['title'], $_POST['content'], $_POST['dest'])){
    // Traitement du mail à envoyer
    global $connexion;

    if(!(empty($_POST['dest']) || empty($_POST['title']) || empty($_POST['content']) || empty($_POST['sujet']))){
        $has_to_check_person = false;
        $mode = 0;

        switch($_POST['dest']){
            case 'me':
                $mode = 1;
                break;
            case 'administrators_users':
                $mode = 2;
                break;
            case 'moderators_users':
                $mode = 3;
                break;
            case 'supervisors_users':
                $mode = 4;
                break;
            case 'basic_users':
                $mode = 5;
                break;
            case 'all_users':
                $mode = 6;
                break;
            case 'simple_user':
                $mode = 7;
                $has_to_check_person = true;
                break;
            case 'followers_from_user':
                $mode = 8;
                $has_to_check_person = true;
                break;
            case 'followings_from_user':
                $mode = 9;
                $has_to_check_person = true;
                break;
            default:
                echo "<h5 class='error'>Impossible de vérifier le destinataire.</h5>";
                break;
        }

        if($mode > 0){
            $users = [];
            if($has_to_check_person){
                if(empty($_POST['person'])){
                    echo "<h5 class='error'>Destinataire vide.</h5>";
                }
                else{
                    $ex = explode(',', $_POST['person']);
                    foreach($ex as $perso){
                        $perso = trim($perso);
                        if(is_numeric($perso)){
                            $users[] = $perso;
                        }
                        else{
                            $i = getID($perso);
                            if($i){
                                $users[] = $i;
                            }
                        }
                    }

                    if($mode === 8){ // Get followers
                        $targeted = [];
                        foreach($users as $u){
                            $res = mysqli_query($connexion, "SELECT * FROM Followings WHERE idFollowed='$u';");

                            if($res && mysqli_num_rows($res)){
                                while($row = mysqli_fetch_assoc($res)){
                                    $targeted[] = $row['idUsr']; // Récupère de l'idUsr qui follow la personne voulue
                                }
                            }
                        }
                        $users = $targeted;
                    }
                    elseif($mode === 9){ // Get followings
                        $targeted = [];
                        foreach($users as $u){
                            $res = mysqli_query($connexion, "SELECT * FROM Followings WHERE idUsr='$u';");

                            if($res && mysqli_num_rows($res)){
                                while($row = mysqli_fetch_assoc($res)){
                                    $targeted[] = $row['idFollowed']; // Récupère de l'idFollowed qui est suivie par la personne voulue
                                }
                            }
                        }
                        $users = $targeted;
                    }
                }
            }
            else{
                // Vérification du destinataire avec le mode
                switch($mode){
                    case 1:
                        $users[] = $_SESSION['id'];
                        break;
                    case 2: // Admin
                        $users = getUsersIdByStatus(2);
                        break;
                    case 3: // Mod
                        $users = getUsersIdByStatus(1);
                        break;
                    case 4: // Admin + mod
                        $users = array_merge(getUsersIdByStatus(2), getUsersIdByStatus(1));
                        break;
                    case 5: // Basic
                        $users = getUsersIdByStatus(0);
                        break;
                    case 6: // Tous
                        $users = getUsersIdByStatus(-1);
                        break;
                }
            }

            // Tous les utilisateurs sont désormais trouvés, envoi mail
            if(!empty($users)){
                require_once(ROOT . '/inc/send_mail.php');

                // Génération du texte à afficher dans l'e-mail
                $cause = "un superviseur vous a directement adressé ce message";

                echo '<p>';
                foreach($users as $u){ // Envoi du mail
                    $res = mysqli_query($connexion, "SELECT id, usrname, realname, mail, lastlogin, registerdate, lastCMailSent, confirmedMail FROM Users WHERE id=$u;");

                    if($res && mysqli_num_rows($res)){
                        $row = mysqli_fetch_assoc($res);

                        if($row['mail']){
                            if($row['confirmedMail']){
                                $content = $_POST['content'];
                                $title = $_POST['title'];
                                $subject = $_POST['sujet'];

                                // Remplacement des symboles spéciaux
                                $content = preg_replace('/%username%/', $row['usrname'], $content);
                                $title = preg_replace('/%username%/', $row['usrname'], $title);
                                $subject = preg_replace('/%username%/', $row['usrname'], $subject);

                                $content = preg_replace('/%realname%/', $row['realname'], $content);
                                $title = preg_replace('/%realname%/', $row['realname'], $title);
                                $subject = preg_replace('/%realname%/', $row['realname'], $subject);

                                $content = preg_replace('/%mail%/', $row['mail'], $content);
                                $title = preg_replace('/%mail%/', $row['mail'], $title);
                                $subject = preg_replace('/%mail%/', $row['mail'], $subject);

                                $content = preg_replace('/%register_date%/', formatDate($row['registerdate'], 0), $content);
                                $title = preg_replace('/%register_date%/', formatDate($row['registerdate'], 0), $title);
                                $subject = preg_replace('/%register_date%/', formatDate($row['registerdate'], 0), $subject);

                                $text_content = $content;

                                // Mise en HTML Entities
                                $content = htmlspecialchars($content);
                                $title = htmlspecialchars($title);

                                $content = preg_replace('/\[a:(.+):(.+)\]/', "<a href='$1'>$2</a>", $content);
                                $content = preg_replace('/\n/', "<br>", $content);
                                $title = preg_replace('/\[a:(.+):(.+)\]/', "<a href='$1'>$2</a>", $title);
                                $text_content = preg_replace('/\[a:(.+):(.+)\]/', "$1", $text_content);

                                echo 'Mail : ', $content, '<br>';

                                // Envoi du mail
                                genericMail($row['mail'], $title, $subject, $text_content, $content, $cause);
                            }
                            else{
                                echo "<span class='error'>Notice : {$row['usrname']} n'a pas confirmé son adresse e-mail : Mail non envoyé.<br></span>";
                            }
                        }
                        else{
                            echo "<span class='error'>Notice : {$row['usrname']} n'a pas défini d'adresse e-mail : Mail non envoyé.<br></span>";
                        }
                    }
                }
                echo "</p><h5>Les mails ont été envoyés.</h5>";
            }

            else{
                echo "<h5 class='error'>Aucun utilisateur n'est sélectionné.</h5>";
            }
        }
        else{
            echo "<h5 class='error'>Un élément requis est vide.</h5>";
        }
    }
    else{
        echo "<h5 class='error'>Un élément requis est vide.</h5>";
    }
}

?>
<div class='row'>
    <div class='col s12 l10 offset-l1'>
        <form method='post' action='#'>
            <div class="input-field">
                <input maxlength='40' type='text' id='sujet' name='sujet' required <?= (isset($_POST['sujet']) ? "value='".htmlspecialchars($_POST['sujet'], ENT_QUOTES)."'" : '') ?>>
                <label for="sujet">Sujet de l'e-mail</label>
            </div>
            <div class="input-field">
                <input maxlength='40' type='text' id='title' name='title' required <?= (isset($_POST['title']) ? "value='".htmlspecialchars($_POST['title'], ENT_QUOTES)."'" : '') ?>>
                <label for="title">Titre de l'e-mail</label>
            </div>

            <textarea name="content" id='content' rows="20" cols="70" placeholder="Contenu de l'e-mail"
                class="materialize-textarea txa" required><?= (isset($_POST['content']) ? htmlspecialchars($_POST['content']) : '') ?></textarea>

            <div class="input-field col s12">
                <select name="dest">
                    <option value="" disabled selected>Choisissez le(s) destinataire(s)</option>
                    <optgroup label="Sélecteurs simples">
                        <option value="me">Vous</option>
                        <option value="administrators_users">Administrateurs seulement</option>
                        <option value="moderators_users">Modérateurs seulement</option>
                        <option value="supervisors_users">Modérateurs et administrateurs</option>
                        <option value="basic_users">Utilisateurs standard</option>
                        <option value="all_users">Tous les utilisateurs</option>
                    </optgroup>
                    
                    <optgroup label="Sélecteurs spécifiques">
                        <option value="simple_user">Un/plusieurs utilisateur(s)</option>
                        <option value="followers_from_user">Abonnés d'un/plusieurs utilisateur(s)</option>
                        <option value="followings_from_user">Abonnements d'un/plusieurs utilisateur(s)</option>
                    </optgroup>
                </select>
            </div>
            <div class='clearb'></div>

            <div class="input-field">
                <input maxlength='40' type='text' id='person' name='person' placeholder='Séparez les utilisateurs par des virgules (,)' 
                    <?= (isset($_POST['person']) ? "value='".htmlspecialchars($_POST['person'], ENT_QUOTES)."'" : '') ?>>
                <label for="person">Destinataire(s) (si spécifique)</label>
            </div>

            <button type='submit' class='btn col s12 l3 offset-l9'>Envoyer</button>
        </form>
    </div>
</div>
