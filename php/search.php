<?php

global $connexion;
$cur_search = NULL;
$loadedThings = false;
$GLOBALS['turn_off_container'] = true;
$onglets = null;

require_once('inc/search_traitement.php');

echo "<div style='padding-bottom: 20px;'></div>";

if(isset($_GET['since_real'], $_GET['before_real'], $_GET['users'], $_GET['terms'], $_GET['search_type'], $_GET['other_terms'])){
    $_GET['search'] = getStringQuery($_GET['terms'], $_GET['since_real'], $_GET['before_real'], $_GET['users'], $_GET['search_type'], $_GET['other_terms']);
}

if(isset($_GET['search']) && is_string($_GET['search']) && $_GET['search'] != '' && strlen($_GET['search']) < 100){
    // Recherche de mots clés
    $matches = [];
    $cur_search = trim($_GET['search']);
    $skip_shitstorm = preg_match('/^user:(.*)/', $_GET['search'], $matches);
    $skip_user = false;

    if(isset($matches[1])){
        $cur_search = ltrim($matches[1]);
    }
    else{
        $matches = [];
        $skip_user = preg_match('/^shitstorm:(.*)/', $_GET['search'], $matches);
        if(isset($matches[1])){
            $cur_search = ltrim($matches[1]);
        }
    }

    $len = mb_strlen($cur_search);
    $count = 0;

    if($len >= 2 && $len <= 100){ // 50 caractères + 49 caractères pour tous les paramètres entrés
        $cur_search = mysqli_real_escape_string($connexion, trim($cur_search)); // Escape 1
        $cur_search = addcslashes($cur_search, '%_'); // Escape additionnel
        $noshit = true;

        if(!$skip_shitstorm){
            // Recherche de mot clés
            $query = '';
            $curSearch = $cur_search;
            $def_req = '';
            $trimmed = '';

            $query = searchParam($curSearch, $def_req, $trimmed);
            $trimmed = trim($trimmed);
            $len = mb_strlen($trimmed);
            $cur_search = $trimmed;

            if($len > 50){
                echo "<h4 class='noShitstorm error'>Les termes de recherche doivent faire entre trois et cinquante caractères.</h4>";
                $skip_user = true;
            }
            elseif(!empty($query)) {
                // Recherche safe désormais
                if(isset($_GET['count'])){
                    if(is_numeric($_GET['count']) && $_GET['count'] > 0 && $_GET['count'] % 10 == 0){
                        $count = (int)$_GET['count'];
                    }
                }
                $res = mysqli_query($connexion, "SELECT * FROM Shitstorms WHERE $query ORDER BY dateShit DESC LIMIT $count,11");

                $next = NULL;
                $previous = NULL;
                $is_next = NULL;
                $is_previous = NULL;

                if(mysqli_num_rows($res) > 10){
                    $next = $count+10;
                    $is_next = true;
                }
                if($count >= 10){
                    $previous = $count-10;
                    $is_previous = true;
                }

                if($res && mysqli_num_rows($res) > 0){
                    $noshit = false;
                    $loadedThings = true;

                    $res_numb = mysqli_num_rows($res);
                    if($res_numb > 10) $res_numb = 10;
                    $page = "&search=" . htmlspecialchars($_GET['search'], ENT_QUOTES);
                    $actual_page = $count / 10 + 1;
                    
                    echo "<div id='shitstorms'>";
                    echo "
                    <div class='row'>
                        <div class='col s12 l10 offset-l1'>";
                    
                    echo "<h2>Shitstorms $def_req</h2>";
                    echo "<h4>$res_numb résultat".($res_numb > 1 ? 's' : '')." sur cette page.</h4>";

                    echo "</div>
                    </div>";

                    if($is_next || $is_previous){ // Génération boutons prec/suiv
                        generateNewNextPrevious('search', $is_next, $is_previous, $previous, $next, $page, $actual_page);
                    }

                    // Affiche cards
                    $begin = true;
                    while($row = mysqli_fetch_assoc($res)){
                        if(!$begin){
                            echo "<div class='divider'></div><div class='clearb'></div>";
                        }
                        else{
                            $begin = false;
                        }

                        echo "<div class='section'><div class='row container'>";

                        if(!generateShitstormFlowFromMysqliRow($row, null, false, false, true))
                            break;

                        echo "</div></div>";
                    }
                    echo "</div>";

                    if($is_next || $is_previous){ // Affiche boutons 2
                        generateNewNextPrevious('search', $is_next, $is_previous, $previous, $next, $page, $actual_page);
                    }
                }
                else if ($count > 0){
                    echo "<h4 class='noShitstorm'>Vous avez atteint la fin de la recherche.</h4>";
                    generateNewNextPrevious('search', false, true, $count-10, 0, "&search=". htmlspecialchars($_GET['search'], ENT_QUOTES), $actual_page);
                }
            }
        }

        if(!$skip_user && $len >= 2 && $count == 0 && strpos($cur_search, ' ') === false){ // Si il reste une chaîne à chercher + si on est sur la page root (count = 0) + il n'y a pas d'espace dans la chaîne
            // Recherche si le nom d'utilisateur correspond
            $res = mysqli_query($connexion, "SELECT id, usrname, realname, banner_img, profile_img, registerdate, storm, snow, moderator, genre
                                             FROM Users 
                                             WHERE (usrname LIKE '%$cur_search%' 
                                             OR realname LIKE '%$cur_search%'
                                             OR SOUNDEX(usrname) = SOUNDEX('$cur_search')) 
                                             AND findable=1 
                                             LIMIT 0,5;");

            if($res && mysqli_num_rows($res) > 0){
                $res_numb = mysqli_num_rows($res);
                if($loadedThings){ // Il y a déjà des shitstorms > On met des onglets
                    echo "<div class='clearb'></div>";
                    $onglets = "<li class='tab col s6'><a class='active' href='#shitstorms'>Shitstorms</a></li>
                        <li class='tab col s6'><a href='#users'>Utilisateurs</a></li>";
                }
                
                echo "<div id='users'>";
                $loadedThings = true;
                echo "
                    <div class='row'>
                        <div class='col s12 l10 offset-l1'>";
                    
                    echo "<h4>$res_numb utilisateur".($res_numb > 1 ? 's' : '')." trouvé".($res_numb > 1 ? 's' : '').".</h4>";

                    echo "</div>
                </div>";

                $begin = true;
                echo "<div class='row container' style='margin-bottom: 60px;'>";

                while($row = mysqli_fetch_assoc($res)){
                    generateUserFlow($row, true, true);
                }
                echo "</div></div>";
                $noshit = false;
            }
        }
        
        if($noshit){
            echo "<h2 class='center'>Aucun résultat.</h2>";
            echo "<h4 class='center'>Aucun utilisateur ou shitstorm ne correspond à votre recherche.</h4>";
        }
    }
    else {
        echo "<h4 class='noShitstorm error'>Les termes de recherche doivent faire entre deux et cinquante caractères.</h4>";
    }
}

$_GET['search'] = isset($_GET['search']) ? htmlspecialchars($_GET['search'], ENT_QUOTES) : '';

// Navbar
$GLOBALS['navbar']->addElement('search?search='.(isset($_GET['search']) ? $_GET['search'] : ''), 'Recherche');

if(isset($_GET['advanced'])){
    $GLOBALS['navbar']->addElement('search?advanced', 'Recherche avancée');
}

if(isset($_GET['advanced'])){
    ?>
    <!-- Formulaire de recherche avancée -->
    <div class='row'>
        <div class='col s12 l10 offset-l1'>
            <div class='col s12 l10 offset-l1'>
                <div class='card-user-new border-user' style='height: auto !important;'>
                    <form method='get' action='search' id='adv_search'>
                        <h5 style='padding : 0 !important'>Date</h5>
                        <div class="input-field">
                            <label for="datepickersince">Shitstorms depuis le</label>
                            <input class="datepicker" type="text" id="datepickersince" name="since">
                        </div>
                        <div class="input-field">
                            <label for="datepickerbefore">Shitstorms avant le</label>
                            <input class="datepicker" type="text" id="datepickerbefore" name="before">
                        </div>

                        <div class='divider' style='margin: 15px 0 25px 0;'></div>

                        <h5 style='padding : 0 !important'>Posteur(s)</h5>
                        <div class="input-field">
                            <input type='text' id='users' name='users' maxlength='50'>
                            <label for="users">Shitstorms postées par cet/ces utilisateur(s)</label>
                        </div>

                        <div class='divider' style='margin: 15px 0 25px 0;'></div>

                        <h5 style='padding : 0 !important'>Termes</h5>
                        <div class="input-field">
                            <input type='text' id='terms' name='terms' maxlength='50'>
                            <label for="terms">Nom(s) d'utilisateur ou shitstorm(s) contenant ces termes</label>
                        </div>
                        <div class="input-field">
                            <input type='text' id='oterms' name='other_terms' maxlength='50'>
                            <label for="oterms">ou shitstorm(s) contenant ces termes</label>
                        </div>

                        <div class='divider' style='margin: 15px 0 25px 0;'></div>
                        
                        <h5 class='col s12' style='padding : 0 !important'>Type</h5>
                        <select name='search_type'>
                            <option value='both' selected>Shitstorms et utilisateurs</option>
                            <option value='shitstorm'>Shitstorms uniquement</option>
                            <option value='user'>Utilisateurs uniquement</option>
                        </select>

                        <div class='divider' style='margin: 15px 0 25px 0;'></div>

                        <div class='card-user-new border-user fit-content auto-height attenuation' style='height: auto !important;'>
                            <a style='display: block;' onclick='document.getElementById("adv_search").submit()' class='card-pointer flow-text'>Rechercher</a>
                            <div class='clearb'></div>
                        </div>
                    </form>
                    <div class='clearb'></div>
                </div>
            </div>
        </div>
    </div>

    <!-- Aide -->
    <div class='row'>
        <div class='col s12 l10 offset-l1'>
            <h4 style='margin-bottom: 20px;'>Recherche avancée manuelle</h4>
            <div class='col s12 l10 offset-l1'>
                <div class='card-user-new border-user' style='height: auto !important;'>
                    <p>La recherche trouve les shitstorms contenant l'ensemble des termes que vous rentrez séparés par un espace.</p>
                    <p>Pour effectuer une recherche ciblant l'un ou l'autre terme, séparez les avec le mot-clé <strong>or</strong>.</p>
                    <p>Vous pouvez également faire une recherche avancée avec certains paramètres.</p>
                    <ul class="collection with-header">
                        <li class="collection-header"><h6>Paramètres</h6></li>
                        <li class="collection-item">
                            <div>
                                <strong>since:YYYY-MM-DD</strong> <small>(YYYY désignant une année, MM le numéro du mois, DD le jour)</small>
                            </div>
                            <div>
                                <small>Recherche les shitstorms postées depuis la date donnée (incluse)</small>
                            </div>
                        </li>
                        <li class="collection-item">
                            <div>
                                <strong>before:YYYY-MM-DD</strong> <small>(YYYY désignant une année, MM le numéro du mois, DD le jour)</small>
                            </div>
                            <div>
                                <small>Recherche les shitstorms postées avant la date donnée (exclue)</small>
                            </div>
                        </li>
                        <li class="collection-item">
                            <div>
                                <strong>id:x,y,z,...</strong> <small>(où x, y et z désignent des identifiants numériques d'utilisateurs)</small>
                            </div>
                            <div>
                                <small>Recherche les shitstorms postées par le ou les utilisateur(s) donné(s)</small>
                            </div>
                            <div>
                                <small>Si plusieurs utilisateurs sont spécifiés, ils doivent être séparés par une virgule</small>
                            </div>
                        </li>
                        <li class="collection-item">
                            <div>
                                <strong>from:username</strong> <small>(où username désigne un nom d'utilisateur)</small>
                            </div>
                            <div>
                                <small>Recherche les shitstorms postées par l'utilisateur donné</small>
                            </div>
                        </li>
                        <li class="collection-item">
                            <div>
                                <strong>shitstorm:{recherche}</strong> <small>(où {recherche} désigne des termes de recherche)</small>
                            </div>
                            <div>
                                <small>Recherche les shitstorms et omet la recherche d'utilisateur</small>
                            </div>
                        </li>
                        <li class="collection-item">
                            <div>
                                <strong>user:{name}</strong> <small>(où {name} désigne un nom d'utilisateur ou un nom d'usage)</small>
                            </div>
                            <div>
                                <small>Recherche les utilisateurs uniquement et omet la recherche de shitstorms</small>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class='clearb'></div>
            </div>
        </div>
    </div>
    <?php
}
else {
    if($loadedThings){
        echo "<div class='divider'></div><div class='clearb'></div>";
    }
    if($onglets){
        $GLOBALS['nav_tab'] = '<div class="row"><ul class="tabs"><div class="col s12 l8 offset-l2">' . $onglets . '</div></ul></div>';
    }
    ?>
    <div class='section container'>
        <div class='row'>
            <form id='search_f' method='get' action='search'>
                <div class="input-field">
                    <a class='prefix' onclick="document.getElementById('search_f').submit();"><i class="material-icons card-pointer black-text" style='font-size: 2rem; padding-top: 0.3em;'>search</i></a>
                    <input class='' placeholder='Recherche' type="text" id="search-input" name='search'<?= (isset($_GET['search']) ? " value='". $_GET['search'] ."'" : '') ?>>
                    <div class='clearb'></div>
                </div>
            </form>
        </div>

        <span class='flow-text' style='margin-right: 40px;'><a class='card-pointer' onclick="document.getElementById('search_f').submit();">Recherche</a></span>
        <span class='flow-text'><a href='<?= HTTPS_URL ?>search?advanced'>Recherche avancée</a></span>
        <div class='clearb' style='margin-bottom: 50px;'></div>
    </div>
    <?php
}
?>
<script src='<?= HTTPS_URL ?>js/date.js'></script>
<script src='<?= HTTPS_URL ?>js/profile.js'></script>
