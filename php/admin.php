<?php 
// -----------------------------------------------------
// PAGE DE MODERATION DES SHITSTORMS PAR LES MODERATEURS
// -----------------------------------------------------

require(ROOT . '/inc/postDelete.php');

if(!isModerator()){
    return _403();
}

global $connexion;

// creationToutesSaves();
// creationToutesSavesAnswers();
// Vérification si il faut effacer des tweets de la base de sauvegarde
if(isset($GLOBALS['settings']['nextTweetVerification']) && time() > strtotime($GLOBALS['settings']['nextTweetVerification'])) {
    deleteOldUnlinkedTweets();

    $next = date('Y-m-d H:i:s', time() + (60 * 60 * 24 * 5));
    $res = mysqli_query($connexion, "UPDATE sharedSettings SET nextTweetVerification='$next' WHERE idSet={$GLOBALS['settings']['idSet']};");
}

// PUBLICATION SHITSTORM
if(isset($_POST['pub'], $_POST['idSub']) && $_POST['idSub'] > 0){
    $id = acceptShitstorm((int)$_POST['idSub']);

    if($id > 0) {
        echo "<h4>La publication a réussi</h4>
        <h5 class='pass'>Voir la publication réalisée <a target='_blank' href='".HTTPS_URL."shitstorm/$id'>ici</a></h5>";
    }
    else {
        echo "<h4 class='error'>";
        
        switch($id) {
            case -1:
                echo "Shitstorm introuvable"; break;
            case -2:
                echo "Shitstorm ne contient aucun lien"; break;
            case -3:
                echo "Titre déjà utilisé"; break;
            case -5:
                echo "Un des liens est invalide"; break;
            case -4:
                echo "Le lien 1 est invalide"; break;
            case -6:
                echo "Erreur d'insertion"; break;
            default:
                echo "Erreur inconnue";
        }
        
        echo "</h4>";
    }
}
elseif(isset($_POST['delete'], $_POST['idSub'])){
    $idSub = mysqli_real_escape_string($connexion, $_POST['idSub']);
    if(isset($_POST['delete'])){
        $res = deleteWaitingShitstorm($idSub);
    }

    if($res){
        echo "<h5 class='pass'>La suppression s'est bien déroulée.</h5>";
    }
    else{
        echo "<h5 class='error'>Erreur : Impossible de supprimer la soumission de la table d'attente.</h5>";
    }
}

// Réponses
else if (isset($_POST['pubA'], $_POST['idSubA'])) {
    $id = acceptAnswer((int)$_POST['idSubA']);

    if($id > 0) {
        $res = $connexion->query("SELECT idSub FROM Answers WHERE idAn=$id;");

        echo "<h4>La publication a réussi</h4>";
        if($res && $res->num_rows) {
            $row = $res->fetch_assoc();

            echo "<h5 class='pass'>Voir la publication réalisée <a target='_blank' href='".HTTPS_URL."shitstorm/{$row['idSub']}'>ici</a></h5>";
        }
    }
    else {
        echo "<h4 class='error'>";
        
        switch($id) {
            case -1:
                echo "Réponse introuvable"; break;
            case -2:
                echo "Cette réponse est liée à une shitstorm en attente"; break;
            case -3:
                echo "Le lien est invalide"; break;
            case -4:
                echo "Erreur d'insertion"; break;
            default:
                echo "Erreur inconnue";
        }
        
        echo "</h4>";
    }
}

elseif(isset($_POST['deleteAn'], $_POST['idSubA'])){
    $idSubA = mysqli_real_escape_string($connexion, $_POST['idSubA']);
    if(isset($_POST['deleteAn']))
        $res = deleteWaitingAnswer($idSubA);

    if($res){
        echo "<h5 class='pass'>La suppression s'est bien déroulée.</h5>";
    }
    else{
        echo "<h5 class='error'>Erreur : Impossible de supprimer la soumission de la table d'attente.</h5>";
    }
}

$res = mysqli_query($connexion, "SELECT * FROM Waiting ORDER BY dateShit DESC;");
$resAnswers = mysqli_query($connexion, "SELECT * FROM WaitAnsw ORDER BY dateAn ASC;");

// Navbar
$GLOBALS['navbar']->addElement('soumission', 'Ajouter une shitstorm');
$GLOBALS['navbar']->addElement('moderation', 'Modération');

if($res && $resAnswers && $res->num_rows === 0 && $resAnswers->num_rows === 0) {
    echo '<h4 class="empty">Aucune soumission n\'est disponible pour modération.</h4>';
}
else {
    ?>
    <script src='<?= HTTPS_URL ?>js/admin.js'></script>
    
    <div class='moderation'>
        <!-- <h2>Modération</h2> -->
        <h3>Shitstorms non approuvées</h3>
    
        <?php 
            // SHITSTORMS A APPROUVER
            if(mysqli_num_rows($res) == 0){
                echo "<h5 class='empty'>Aucune shitstorm n'est actuellement disponible pour modération.</h5>";
            }
    
            while($row = mysqli_fetch_assoc($res)){
                $waitID = $row['idSub'];
                // RECUPERATION DES LIENS DE LA SS EN COURS
                $resLink = mysqli_query($connexion, "SELECT * FROM WaitingLinks WHERE idSub='{$row['idSub']}';");
                if(mysqli_num_rows($resLink) == 0){
                    echo "<h5 class='error'>Erreur : aucun lien n'est associé avec cette shitstorm. Vérifiez le formulaire de soumission</h5>";
                }
                $linkRow = mysqli_fetch_assoc($resLink);
                ?>
                <div class="row">
                    <div class="col s12 l10 offset-l1">
                        <div class="card" style="padding: 1.5em; padding-bottom: 0 !important;">
                            <form action="#" method='post'>
                                <div class="card-image">
                                    <h5 class='detailsS'><?php echo "{$row['pseudo']} a proposé une shitstorm du {$row['dateShit']} ({$row['idSub']})"; ?></h5>
                                    <div class='divider'></div>
                                    <div id='infos<?= $waitID ?>' style='padding: 1em; padding-bottom: 0 !important'>
                                        <h6 class='col s12' id='title<?= $waitID ?>'>
                                            <?= htmlentities($row['title']) ?>
                                        </h6>
                                        <p class='col s12 black-text' id='dscr<?= $waitID ?>'><?= htmlentities($row['dscr']); ?></p>
                                        <div id='link<?= $waitID ?>' class='link card-tweet-deleted col s12 with-padding'><?php 
                                            do{ ?>
                                                <div class='black-text'>
                                                    <i class="material-icons left">open_in_new</i><a href='<?= htmlentities($linkRow['link']) ?>' target='_blank'><?= htmlentities($linkRow['link']) ?></a>
                                                </div>
                                                <div class='clearb'></div>
                                                <?php
                                            }while($linkRow = mysqli_fetch_assoc($resLink)); ?>
                                        </div>
                                        <div id='date<?= $waitID ?>' class='col s12 black-text'>Shitstorm du <?= formatDate($row['dateShit'], 0) ?></div>
                                        <input type='hidden' value='<?= $row['idSub'] ?>' name='idSub'>
                                        <div class='clearb'></div>
                                    </div>
                                    <p>
                                        <a href='#!' id='setActive<?= $row['idSub'] ?>' onclick='changeVisibility(<?= $row['idSub'] ?>, false, <?= ($row['locked'] ? '0' : '1') ?>)'>
                                            <?= 'Définir comme '.($row['locked'] ? 'éditable' : 'non-éditable') ?>
                                        </a>
                                    </p>
                                </div>
    
                                <div class="card-content">
                                    <div class="row card-action" style='clear: both; margin-bottom: 0 !important; padding-bottom: 0 !important'>
                                        <a class="btn col s12 l5 teal darken-1" style='margin-bottom: .2em !important' href='#!' onclick='actualiseSS(0, <?= $waitID ?>)'>Actualiser</a>
                                        <a class="btn col s12 l5 purple lighten-1 offset-l2" href='<?= HTTPS_URL ?>modify?idPubli=<?= $row['idSub'] ?>&waiting=t' target='_blank'>éditer</a>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="row card-action" style='clear: both;'>
                                        <button class="btn col s12 l5 but0 blue darken-1" style='margin-bottom: .2em !important' type="submit" name="pub">Insérer</button>
                                        <button class="btn col s12 l5 red lighten-1 but1 offset-l2" type="submit" name="delete">Supprimer</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <?php
            }
    
            // GESTION DES REPONSES A APPROUVER
            echo "<h3>Réponses non approuvées</h3>";
    
            if(mysqli_num_rows($resAnswers) == 0){
                echo "<h5 class='empty'>Aucune réponse n'est actuellement disponible pour modération.</h5>";
            }
    
            while($row = mysqli_fetch_assoc($resAnswers)){
                ?>
                <div class="row">
                    <div class="col s12 l10 offset-l1">
                        <div class="card" style="padding: 1.5em; padding-bottom: 0 !important;">
                            <form action="#" method='post'>
                                <div class="card-image">
                                    <h5 class='detailsS'><?= "<a class='notCardComment' target='_blank' href='".HTTPS_URL."profil/{$row['idUsrA']}'>{$row['idUsrA']}</a>
                                        a proposé une réponse pour une shitstorm du {$row['dateAn']} (<a target='_blank 'href='".HTTPS_URL."shitstorm/{$row['idSub']}'>Voir</a>)"; ?>
                                    </h5>
                                    <div class='divider'></div>
                                    <div id='infosAPlus<?= $row['idSubA'] ?>' style='padding: 1em; padding-bottom: 0 !important'>
                                        <?= ($row['fromWaitingShitstorm'] ? "<p>Cette publication est liée à la shitstorm en attente {$row['idSub']}</p>" : '') ?>
                                    </div>
                                    <div id='infosA<?= $row['idSubA'] ?>' style='padding: 1em; padding-bottom: 0 !important'>
                                        <p class='col s12 black-text' id='dscrA<?= $row['idSubA'] ?>'><?= htmlentities($row['dscrA']); ?></p>
                                        <div id='linkA<?= $row['idSubA'] ?>' class='link card-tweet-deleted col s12 with-padding'>
                                            <div class='black-text'><a href='<?= htmlentities($row['linkA']) ?>' target='_blank'><?= htmlentities($row['linkA']) ?></a></div>
                                        </div>
                                        <div id='dateA<?= $row['idSubA'] ?>' class='col s12 black-text'>Réponse du <?= formatDate($row['dateAn'], 0) ?></div>
                                        <input type='hidden' value='<?= $row['idSubA'] ?>' name='idSubA'>
                                        <div class='clearb'></div>
                                    </div>
    
                                    <p>
                                        <a href='#!' id='setActive<?= $row['idSubA'] ?>A' onclick='changeVisibility(<?= $row['idSubA'] ?>, true, <?= ($row['locked'] ? '0' : '1') ?>)'>
                                            <?= 'Définir comme '.($row['locked'] ? 'éditable' : 'non-éditable') ?>
                                        </a>
                                    </p>
                                </div>
    
                                <div class="card-content">
                                    <div class="row card-action" style='clear: both; <?= $row['fromWaitingShitstorm'] ? '' : 'margin-bottom: 0 !important; padding-bottom: 0 !important' ?>'>
                                        <a class="btn col s12 l5 teal darken-1" style='margin-bottom: .2em !important' href='#!' onclick='actualiseSS(1, <?= $row['idSubA']; ?>)'>Actualiser</a>
                                        <a class="btn col s12 l5 purple lighten-1 offset-l2" 
                                            href='<?= HTTPS_URL ?>modify_an?idAn=<?= $row['idSubA'] ?>&waiting=t<?= ($row['fromWaitingShitstorm'] ? '&waiting_shit=t' : '') ?>' target='_blank'>
                                            éditer
                                        </a>
                                    </div>
                                </div>
    
                                <div class='clearb'></div>
    
                                <?php if(!$row['fromWaitingShitstorm']) { ?>
                                <div class="card-content">
                                    <div class="row card-action">
                                        <button class="btn col s12 l5 but0 blue darken-1" name="pubA">Insérer réponse</button>
                                        <button class="btn col s12 l5 red lighten-1 but1 offset-l2" type="submit" name="deleteAn">Supprimer</button>
                                    </div>
                                </div>
                                <?php } ?>
                                <div class='clearb'></div>
                            </form>
                        </div>
                    </div>
                </div>
                <?php
            }
        ?>
    </div>
    <?php
}
