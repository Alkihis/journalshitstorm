<?php

// -------------------------------------------
// PAGE DE CONFIGURATION DE COMPTE UTILISATEUR
// -------------------------------------------

$GLOBALS['navbar']->addElement('createaccount/configure', 'Configurer le compte');

$dialog = '';

if(isset($_POST['submit'], $_POST['realname'], $_POST['genre'], $_POST['mail'], $_SESSION['to_configure'], $_SESSION['id']) && $_SESSION['to_configure']){
    $pass = true;

    $mail = mysqli_real_escape_string($connexion, $_POST['mail']);
    if(!empty($mail)){
        $res = mysqli_query($connexion, "SELECT mail FROM Users WHERE mail='$mail';");

        if($res && mysqli_num_rows($res) != 0){
            $pass = false;
            $dialog .= "<h3 class='error'>Cette adresse e-mail est déjà utilisée.</h3>";
        }
        elseif(!preg_match(REGEX_MAIL, $_POST['mail']) || strlen($_POST['mail']) > 128){
            $dialog .= "<h3 class='error'>L'adresse email est invalide.</h3>";
            $pass = false;
        }
    }
    else{
        $mail = NULL;
    }

    $realn = mysqli_real_escape_string($connexion, htmlspecialchars($_POST['realname']));
    if(!empty($realn)){
        if(mb_strlen($realn) > 32){
            $dialog .= "<h3 class='error'>Le nom d'usage est trop long, ou ne contient pas les prérequis demandés.</h3>";
            $pass = false;
        }
    }
    else{
        $realn = NULL;
    }

    $genre = 'ukn';
    $genre = mysqli_real_escape_string($connexion, htmlspecialchars($_POST['genre']));
    switch($genre){
        case 'm':
        case 'f':
            break;
        case 'both':
            $genre = 'b';
            break;
        case 'alt':
            $genre = 'a';
            break;
        default:
            $genre = 'b';
            break;
    }

    if($pass){
        $res = mysqli_query($connexion, "UPDATE Users SET ".($realn ? "realname='$realn'," : '').($mail ? "mail='$mail'," : '')." genre='$genre' WHERE id='{$_SESSION['id']}';");
        if($res){
            $_SESSION['send_mail'] = true;
            header('Location: '.HTTPS_URL.'createaccount/configure/complete');
            return;
        }
        else{
            $dialog .= "<h3 class='error'>Une erreur de la base de données est survenue : ".mysqli_error($connexion)."</h3>";
        }
    }
}

if(isset($_SESSION['to_configure']) && $_SESSION['to_configure']){
    $GLOBALS['turn_off_container'] = true;
    ?>
    <div class='background-full configure-image'>
    </div>

    <div class="row">
        <div class='col s12 l10 offset-l1' style='margin-top: 30px;'>
            <?= ($dialog ? '<div>' . $dialog . '</div>' : '') ?>
            <div class='col s12 l6'>
                <div class='card-user-new border-user' style='height: auto !important; margin-top: 30px;'>
                    <form action="#" method='post'>
                        <label for="mail">Adresse e-mail</label>
                        <input type="text" id="mail" name="mail" maxlength="128" pattern='^[a-zA-Z0-9._+-]+@[a-zA-Z0-9._-]{2,}\.[a-zA-Z]{2,6}$'
                        value='<?php if(isset($_POST['mail'])) echo htmlspecialchars($_POST['mail'], ENT_QUOTES); ?>' placeholder='Permet de réinitialiser votre mot de passe'>

                        <label for="realname">Nom d'usage</label>
                        <input type="text" id="realname" name="realname" maxlength="32" 
                        value='<?php if(isset($_POST['realname'])) echo htmlspecialchars($_POST['realname'], ENT_QUOTES, 'UTF-8')?>'>

                        <div class='input_field' id='genre_type'>
                            <label for="genre">Précisez votre pronom préféré</label>
                            <select name='genre'>
                                <option value='m'>Il</option>
                                <option value='f'>Elle</option>
                                <option value='both' selected>Iel</option>
                                <option value='alt'>Autre</option>
                            </select>
                        </div>

                        <a href='<?= HTTPS_URL ?>profil/<?= $_SESSION['username'] ?>' class='left'>
                            Ne pas enregistrer
                        </a>

                        <button name='submit' class='btn card-user-new border-user attenuation right btn-personal'>
                            Enregistrer
                        </button>
                    </form>
                    <div class='clearb'></div>
                </div>
            </div>
            <div class='col s12 l6'>
                <p class='flow-text black-text' style='text-align: justify; text-justify: inter-word; padding: 0 25px;'>
                    Une adresse e-mail finalement ?! Pas d'inquiétude, vous n'êtes pas obligés de remplir ces champs, tout est facultatif !<br>
                    Alors, pourquoi demandons nous ces informations ?<br>
                    Une adresse e-mail servira à vous informer lorsque vos abonnements posteront de nouvelles shitstorms,
                    ou quand les vôtres reçoivent des nouveaux commentaires !<br>
                    Le nom d'usage servira simplement à faire plus joli sur votre profil, sans compter qu'il n'est pas unique, contrairement au pseudo.
                    Vous pouvez donc le partager avec un autre utilisateur.<br>
                    Enfin, le pronom préféré servira à personnaliser votre page de profil. Ni plus, ni moins.
                </p>
            </div>
        </div>
    </div>
    <?php
}
else{
    ?>
    <h3 class='pass'>Ce compte est déjà configuré.</h3>
    <h4>Utilisez les paramètres du profil pour modifier des options.</h4>
    <?php
}
