<?php 
// ------------------------------------------------------------------------------
// PAGE DE GESTION DE TOUS LES COMMENTAIRES PUBLIES PAR L'UTILISATEUR AUTHENTIFIE
// ------------------------------------------------------------------------------

if(!isset($_SESSION['connected']) || !$_SESSION['connected']){
    if(isset($GLOBALS['is_included'])) { include(PAGE403); return; }
    else {header("Location:403"); exit();}
}

$GLOBALS['page'] = 'compte'; // Permet de set le slider du menu sur "Compte"

global $connexion;

require_once(INC_DIR . 'postDelete.php');

if(isset($_GET['del']) && isset($_GET['id']) && isset($_COOKIE['token'])){
    $idCom = (int)mysqli_real_escape_string($connexion, $_GET['id']);
    if(md5($_COOKIE['token'] . $idCom) == $_GET['del']){
        
        $res = mysqli_query($connexion, "SELECT * FROM Comment WHERE idUsr='{$_SESSION['id']}' AND idCom='$idCom';");
        if(mysqli_num_rows($res) == 0){
            echo "<h5 class='error'>Ce commentaire n'existe pas.</h5>";
        } 
        else{
            if(deleteCom($idCom)){
                echo "<h5 class='pass'>Le commentaire a bien été supprimé.</h5>";
            }
            else{
                echo "<h5 class='error'>Une erreur de suppression est survenue.</h5>";
            }
        }
    }
}

$count = 0;
$is_previous = false;
if(isset($_GET['count']) && is_string($_GET['count'])){
    $count = intval(mysqli_real_escape_string($connexion, $_GET['count']));
    $count = ($count < COUNT_LIMIT_PER_PAGE ? 0 : $count);
    $is_previous = $count >= COUNT_LIMIT_PER_PAGE;
}

$current_page_number = ($count / COUNT_LIMIT_PER_PAGE) + 1;
$GLOBALS['turn_off_container'] = true;

echo "<div class='mysub' style='margin: 20px 0;'>";

$res = mysqli_query($connexion, "SELECT c.*, title, dateShit FROM Comment c JOIN Shitstorms s ON c.idSub=s.idSub 
                                 WHERE c.idUsr='{$_SESSION['id']}' LIMIT $count,".(COUNT_LIMIT_PER_PAGE+1).";");
if(mysqli_num_rows($res) == 0){
    echo "<h4 class='center'>Il n'y a aucun commentaire.</h4>";
    if($is_previous){
        generateNewNextPrevious('compte/mysubs', false, true, $count-COUNT_LIMIT_PER_PAGE, 0, null, $current_page_number);
    }
}
else{
    $is_next = mysqli_num_rows($res) > COUNT_LIMIT_PER_PAGE;

    if($is_previous || $is_next){
        generateNewNextPrevious('compte/mysubs', $is_next, $is_previous, $count-COUNT_LIMIT_PER_PAGE,
                                $count+COUNT_LIMIT_PER_PAGE, null, $current_page_number);
    }
    
    $i = 0;
    while(($row = mysqli_fetch_assoc($res)) && $i != COUNT_LIMIT_PER_PAGE){
        $resNbCom = mysqli_query($connexion, "SELECT COUNT(*) c FROM ComLikes WHERE idCom='{$row['idCom']}';");

        $nbLikes = 0;

        if($resNbCom && mysqli_num_rows($resNbCom) > 0){
            $rowNb = mysqli_fetch_assoc($resNbCom);
            $nbLikes = $rowNb['c'];
        }

        $idSub = $row['idSub'];
        ?>
        <div class='row container'>
            <div class='col s12 l10 offset-l1'>
                <div class="card card-border commentBack" style="padding-top: .5em;">
                    <div class="card-content white-text">
                        <p style="margin-bottom: .5em;">
                            <a class='notCardComment' target='_blank' href='<?= HTTPS_URL ?>shitstorm/<?= $idSub ?>'>
                                Lié à la shitstorm <?= htmlspecialchars($row['title']) ?>, du <?= formatDate($row['dateShit'], false) ?>
                            </a>
                        </p>
                        <p style="margin-bottom: 1.5em;">
                            <?= detectTransformLink(htmlentities($row['content'])); ?>
                        </p>
                        <div class="row card-action">
                            <button class="btn-floating orange darken-5 tooltipped" data-position="bottom" data-delay="50" data-tooltip="Nombre de favs"
                            style="float: left;" id='nb<?= $row['idCom'] ?>' onclick='generateViewFavModal("modalplaceholder", <?= $row['idCom'] ?>)'><?= $nbLikes ?></button>
                            <p style="font-style: italic; float: right; padding-top: 0.7em !important;">Publié le <?= formatDate($row['contentTime'], 1) ?> </p>
                            
                            <button class="btn-floating red left lighten-1 tooltipped" data-position="bottom" data-delay="50" data-tooltip="Supprimer" type="submit"
                            style="margin-left: 20px;" name="del"
                            onclick='createDeleteCommentModal("modalphbottom", <?= $row['idCom']; ?>, "<?= (isset($_COOKIE['token']) ? md5($_COOKIE['token'] . (int)$row['idCom']) : 0) ?>", <?= $count ?>)'>
                                <i class='material-icons'>delete_forever</i>
                            </button>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
        <?php
        $i++;
    }
    if($is_previous || $is_next){
        generateNewNextPrevious('compte/mysubs', $is_next, $is_previous, $count-COUNT_LIMIT_PER_PAGE, 
                                $count+COUNT_LIMIT_PER_PAGE, null, $current_page_number);
    }
}

echo "</div>";
