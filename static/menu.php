<?php
$pages = 0;

// Page sélectionnée
switch($GLOBALS['page']){
    case 'journal':
        $pages = 1;
        break;
    case 'soumission':
        $pages = 2;
        break;
    case 'moderation':
        $pages = 3;
        break;
    case 'usermgmt':
        $pages = 4;
        break;
    case 'compte':
        $pages = 5;
        break;
    case 'login':
        $pages = 6;
        break;
    case 'notifications':
        $pages = 7;
        break;
}

$notifText = '';
$countN = null;

if(isset($_SESSION['id'], $_SESSION['connected']) && $_SESSION['connected']){
    $countN = hasNotifications($_SESSION['id']);
    if($countN > 20){
        $countN = 20;
    }
    $notifText = ($countN == 0 ? 'Aucune' : "$countN").' nouvelle'.($countN > 1 ? 's' : '').' notification'.($countN > 1 ? 's' : '');
}

?>
<nav class='nav-extended'>
    <div class="nav-wrapper nav-main">
        <a href="<?= HTTPS_URL ?>journal" class="brand-logo"><i class="material-icons" style="font-size: 1.3em; margin-left: 0.3em;">forum</i></a>
        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>

        <ul class="hide-on-med-and-down"> 
            <form method='get' action='<?= HTTPS_URL ?>search'>
                <li>
                    <a href='#!' style='margin-left: 65px; padding: 0px !important'>
                        <button type='submit'><i class="material-icons prefix white-text no-text-transform search-link">search</i></button>
                    </a>
                </li>              
                <li>
                    <div class="row" style='margin-bottom: 0px !important;'>
                        <div class="input-field col s12 white-text">
                            <input autocomplete="off" style='margin-bottom:0 !important;' type="text" placeholder="Recherche" id="search-input" name='search'>
                        </div>
                    </div>
                </li>
            </form>
        </ul>

        <ul class="right hide-on-med-and-down">
            <li <?= ($pages==1 ? 'class="active"' : '') ?>>
                <a class="tooltipped" data-position="bottom" data-delay="10" data-tooltip="Journal des shitstorms" href="<?= HTTPS_URL ?>journal">
                    <i class="material-icons white-text left no-text-transform">library_books</i>Journal
                </a>
            </li>
            <li <?= ($pages==2 ? 'class="active"' : '') ?>>
                <a class="tooltipped" data-position="bottom" data-delay="10" data-tooltip="Soumettre une shitstorm" href="<?= HTTPS_URL ?>soumission">
                    <i class="material-icons white-text left no-text-transform">add</i>Ajouter
                </a>
            </li>
            <?php 
            if(isset($_SESSION['connected']) && $_SESSION['connected']){ ?>
                <li class='show-on-xtra-large-only <?= ($pages == 5 ? 'active' : '') ?>'>
                    <a class="tooltipped" data-position="bottom" data-delay="10" data-tooltip="Mon profil" href="<?= HTTPS_URL ?>profil/<?= $_SESSION['username'] ?>">
                        <i class='material-icons white-text left no-text-transform'>person_pin</i>Profil
                    </a>
                </li>
                
                <?php if($pages != 7) { ?>
                <li><a id='notifMenuButton' class="tooltipped" data-position="bottom" data-delay="10" data-tooltip="<?= $notifText ?>" href="#!" onclick='initNotificationModal()'>
                    <i id='notifMenuButtonContent' class='material-icons <?= ($countN ? 'red' : 'white') ?>-text no-text-transform'><?= ($countN ? 'notifications_active' : 'notifications_none') ?></i></a>
                </li>
                <?php } ?>

                <!-- Dropdown Structure -->
                <ul id='menu_drop' class='dropdown-content'>
                    <li class='hide-on-xtra-large-only'>
                        <a href="<?= HTTPS_URL ?>profil/<?= $_SESSION['username'] ?>">
                            <i class='material-icons black-text'>person_pin</i>Profil
                        </a>
                    </li>
                    <li>
                        <a href="<?= HTTPS_URL ?>compte">
                            <i class='material-icons black-text'>settings</i>Paramètres
                        </a>
                    </li>
                    <li>
                        <a href='#!' onclick='toggleDarkMode()'>
                            <i class='material-icons black-text'>brightness_2</i>Mode nuit
                        </a>
                    </li>
                    
                    <?php if(isset($_SESSION['mod']) && $_SESSION['mod'] > 0){
                            echo "<li><a href='".HTTPS_URL."moderation'>
                                <i class='material-icons black-text'>assignment_turned_in</i>Modération</a></li>";
                        if($_SESSION['mod'] > 1){
                            echo "<li><a href='".HTTPS_URL."usermgmt'>
                                <i class='material-icons black-text'>supervisor_account</i>Administration</a></li>";
                            echo "<li>
                                    <a href='".HTTPS_URL."console_admin'>
                                        <i class='material-icons black-text'>dashboard</i>Console de gestion
                                    </a>
                                </li>";
                        }
                    } ?>
                    <li>
                        <a href="<?= HTTPS_URL ?>help">
                            <i class='material-icons black-text'>help_outline</i>Aide
                        </a>
                    </li>

                    <li>
                        <a href="<?= HTTPS_URL ?>/?page=misc/disconnect.php&logout=1">
                            <i class='material-icons black-text'>exit_to_app</i>Déconnexion
                        </a>
                    </li>
                </ul>

                <!-- Dropdown trigger -->
                <li>
                    <a class='dropdown-button-menu' href='#!' data-activates='menu_drop'><i class='material-icons white-text no-text-transform'>more_vert</i></a>
                </li>
                <?php
            }
            else{
                ?>
                <li><a href='#!' onclick='toggleDarkMode()' class="tooltipped" data-position="bottom" data-delay="10" data-tooltip="Mode nuit">
                    <i class='material-icons white-text no-text-transform'>brightness_2</i>
                </a></li>
                <li><a href='<?= HTTPS_URL ?>help' class="tooltipped" data-position="bottom" data-delay="10" data-tooltip="Aide">
                    <i class='material-icons left white-text no-text-transform'>help_outline</i>Aide
                </a></li>
                <?php
                echo '<li '.($pages==6 ? 'class="active"' : '').'><a class="tooltipped" data-position="bottom" data-delay="10" data-tooltip="Connexion" href="'.HTTPS_URL.'login">
                    <i class="material-icons left white-text no-text-transform">person</i>Connexion</a>
                    </li>';
            }
            ?>
        </ul>
        <ul class="side-nav" id="mobile-demo">
            <?php  if(isset($_SESSION['connected'], $_SESSION['realname'], $_SESSION['banner_img'], $_SESSION['profile_img']) && $_SESSION['connected']){ ?>
                <li>
                    <div class="user-view">
                        <div class="background default-color">
                            <?php if($_SESSION['banner_img']) { ?>
                                <style id='banner_profile_user_style'>
                                    .side-nav .user-view .background, .side-nav .userView .background, .banner-background{
                                        background-image: url("<?= $_SESSION['banner_img'] ?>");
                                    }
                                </style>
                            <?php }
                            else { ?>
                                <style id='banner_profile_user_style'></style>
                            <?php } ?>
                        </div>
                        <a href="<?= HTTPS_URL ?>profil/<?= $_SESSION['username'] ?>"><img class="circle side-nav-pp" src="<?= $_SESSION['profile_img'] ?>"></a>
                        <a href="<?= HTTPS_URL ?>profil/<?= $_SESSION['username'] ?>"><span class="name-side-nav name"><?= htmlspecialchars($_SESSION['realname']) ?></span></a>
                    </div>
                </li> 
            <?php } ?>

            <li><a href="<?= HTTPS_URL ?>journal"><i class="material-icons black-text left">library_books</i>Journal</a></li>
            <li><a href="<?= HTTPS_URL ?>soumission"><i class="material-icons black-text left">add</i>Soumettre une shitstorm</a></li>
            <li><a href="<?= HTTPS_URL ?>search"><i class="material-icons left black-text">search</i>Recherche</a></li>

            <?php if(isLogged()){
                if(isModerator()){
                    echo "<li><div class='divider' style='margin-bottom: .5em'></div></li>";
                    echo "<li><a href='".HTTPS_URL."moderation'><i class='material-icons black-text'>assignment_turned_in</i>Modération</a></li>";
                    if(isAdmin()){
                        echo "<li><a href='".HTTPS_URL."usermgmt'><i class='material-icons black-text'>supervisor_account</i>Administration</a></li>";
                        echo "<li><a href='".HTTPS_URL."console_admin'><i class='material-icons black-text'>dashboard</i>Console de gestion</a></li>";
                    }
                } ?>

                <li><div class='divider' style='margin-bottom: .5em'></div></li>

                <li><a href="<?= HTTPS_URL ?>profil/<?= $_SESSION['username'] ?>"><i class="material-icons black-text left">person_pin</i>Profil</a></li>

                <li><a href="<?= HTTPS_URL ?>notifications" id='notifMenuButtonMobile'>
                    <i class='material-icons black-text'><?= ($countN ? 'notifications_active' : 'notifications_none') ?></i>Notifications (<?= $countN ?>)</a>
                </li>

                <li><a href="<?= HTTPS_URL ?>/?page=misc/disconnect.php&logout=1"><i class='material-icons black-text left'>exit_to_app</i>Déconnexion</a></li>
            <?php }
            else{
                echo "<li><div class='divider' style='margin-bottom: .5em'></div></li>";
                echo '<li><a href="'.HTTPS_URL.'login"><i class="material-icons black-text left">person</i>Connexion</a></li>';
            } ?>

            <li><div class='divider' style='margin-bottom: .5em'></div></li>
            <li><a href='#!' onclick='toggleDarkMode()'><i class='material-icons black-text left'>brightness_2</i>Mode nuit</a></li>
            <li>
                <a href='<?= HTTPS_URL ?>help'>
                    <i class='material-icons black-text left'>help_outline</i>Aide
                </a>
            </li>
        </ul>
    </div>
    <?php 
    if(isset($GLOBALS['navbar']) && $GLOBALS['navbar']->isValid()){
        ?>
        <div class="nav-wrapper second-nav row">
            <div class="col s12 l8 offset-l2">
                <?php
                $i = 1;
                foreach($GLOBALS['navbar'] as $element){
                    echo "<a id='navbar_$i' href='{$element['href']}' class='breadcrumb'>{$element['data']}</a>";
                    $i++;
                }
                ?>
            </div>
        </div>
        <?php
    }
    ?>
</nav>
<div class="side-shitstorm" id="shitstorm-side-container"> 
</div>
<?php
