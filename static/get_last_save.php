<?php

session_start();

require($_SERVER['DOCUMENT_ROOT'] . '/inc/constantes.php');
require($_SERVER['DOCUMENT_ROOT'] . '/inc/fonctions.php');

global $connexion;

connectBD();

if(!isLogged()){
    header('HTTP/1.1 403 Forbidden');
}
elseif(isset($_GET['hash'])){   
    $res = mysqli_query($connexion, "SELECT lastDownloadArchive, moderator FROM Users WHERE id='{$_SESSION['id']}';");
    
    if($res && mysqli_num_rows($res)){
        $row = mysqli_fetch_assoc($res);

        $too_recent = false;
        $current_date = gmdate('Y-m-d H:i:s');
        $dateLast = new DateTime($row['lastDownloadArchive'] . ' UTC');
        $dateLast = $dateLast->add(new DateInterval('PT120M')); // Ajoute 120 minutes à la date
        $dateActual = new DateTime(gmdate('Y-m-d H:i:s e')); // e est la timezone (UTC)

        if($dateLast < $dateActual || $row['moderator']){ // Si ça fait plus de 120 minutes avant le dernier téléchargement
            // Vérification de l'existance du fichier
            $path = str_replace('/', '_', $_GET['hash']);
            if(file_exists('/var/www/zipFiles/' . $_SESSION['id'] . '-' . $_SESSION['username'] . '-' . $path . '.zip')){
                $path = '/var/www/zipFiles/' . $_SESSION['id'] . '-' . $_SESSION['username'] . '-' . $path . '.zip';
                header('Content-Type: application/octet-stream');
                header("Content-Transfer-Encoding: Binary"); 
                header("Content-disposition: attachment; filename=\"" . basename($path) . "\""); 
                readfile($path);

                mysqli_query($connexion, "UPDATE Users SET lastDownloadArchive='$current_date' WHERE id='{$_SESSION['id']}';");
            }
        }
        else{
            $too_recent = true;
            header('HTTP/1.1 403 Forbidden');
        }
    }
    else{
        header('HTTP/1.1 404 Not Found');
    }
}
else{
    header('HTTP/1.1 400 Bad Request');
}

deconnectBD();
