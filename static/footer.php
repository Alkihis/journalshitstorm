<div class="container" style="padding-top: 0;">
    <div class="row">
        <div class="col l6 s12">
            <p class="grey-text text-lighten-4">
                Le <a href='<?= HTTPS_URL ?>about' class='grey-text text-lighten-3 underline-hover'>Journal des Shitstorms</a> est un projet
                communautaire qui grandit à chacune de vos soumissions. Participez 
                <a class="grey-text text-lighten-3 underline" href='<?= HTTPS_URL ?>soumission'>ici</a> !
            </p>
        </div>
        <div class="col l4 offset-l2 s12">
            <ul>
                <?= (isLogged() ? '' : '<li>&rsaquo; <a class="grey-text text-lighten-3 underline-hover" href="' .HTTPS_URL. 'data_protection">Politique de confidentialité</a></li>') ?>
                <li>&rsaquo; <a class="grey-text text-lighten-3 underline-hover" href="<?= HTTPS_URL ?>nchangelog/g">Derniers changements</a></li>
                <?= (isLogged() ? '<li>&rsaquo; <a class="grey-text text-lighten-3 underline-hover" href="' .HTTPS_URL. 'soumission/tweet_saver">Sauvegarder un tweet</a></li>' : '') ?>
            </ul>
        </div>
    </div>
</div>
<div class="footer-copyright">
    <div class="container" style="padding-top: 0;">
        <span class='hide-on-large-only'>Version </span><span class='hide-on-med-and-down'>Journal des Shitstorms v</span>1.6
        <div class='right'>
            <?= (isLogged() ? '<a class="grey-text text-lighten-4 underline-hover" href="'. HTTPS_URL .'data_protection">Confidentialité</a> &#183;' : '') ?>
            <a target='_blank' class="grey-text text-lighten-4 underline-hover" href="https://twitter.com/j_shitstorm">Twitter</a>
        </div>
        
    </div>
</div>
