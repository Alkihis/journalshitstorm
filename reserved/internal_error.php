<?php 
$file = null;
$error_text = 'Une erreur est survenue.<br>
C\'est notre faute. 😞';
$modo_plus = isset($_SESSION['mod']) && $_SESSION['mod'];

if(isset($GLOBALS['exception_catched'])) {
    $file = explode('/', $GLOBALS['exception_catched']['file']);
    $file = end($file);

    if($GLOBALS['exception_catched']['code'] >= 400 && $GLOBALS['exception_catched']['code'] < 500) { // Code d'erreur utilisateur
        $error_text = 'Une erreur est survenue.<br>
        Les informations que vous avez entrées sont probablement invalides. 😞';
    }
}
?>

<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="/img/favicon.png" />

    <title>Erreur interne</title>
</head>

<body>
    <style>
        body {
            background-color: #222222;
            font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; 
            font-weight: 280;
        }

        .before-error {
            font-size: 2vh;
            color: #ffffff;
        }
        .error-code {
            font-size: 9vh;
            color: #ffffff;
        }
        .mini-divider {
            margin: 0 12.5%;
            padding-bottom: 1px;
            background-color: #4e4e4e;
        }
        img {
            height: auto; 
            width: 25vh;  
            margin-top: 3vh;
        }
        .exception-text {
            border: 1px solid;
            border-color: #333333;
            border-radius: 5px;
            padding: 17px;
            color: #e9e9e9;
            width: fit-content;
        }

        p {
            font-size: 3.1vh;
            color: #e9e9e9;
        }
        .tiny-text {
            font-size: 1.8vh;
        }
        a {
            font-size: 1.9vh;
            color: #0896ff;
            text-decoration: none;
        }
        a:hover {
            text-decoration: underline;
        }

        @media screen and (min-width: 996px) {
            center {
                margin: 0 20%;
            }

            .mini-divider {
                margin: 0 7.5%;
            }

            p {
                font-size: 2.1rem;
            }
            a {
                font-size: 1.4rem;
            }

            .tiny-text {
                font-size: 1.5rem;
            }
        }
    </style>

    <center>
        <img src='/img/js_logo_300.png'>
        <div style='margin-top: 5vh;'>
            <div class='error-code'>500</div>
            <div class='before-error'>Erreur interne du serveur</div>
        </div>
        
        <div class='mini-divider' style='margin-top: 5vh;'></div>
        <p style='margin-top: 5vh;'>
            <?= $error_text ?>
        </p>
        <p class='tiny-text'>
            OOPSIE WOOPSIE!!!!!!! Uwu we made a fucky wucky!!!! a wittle fucko boingo!<br>
            The code monkeys at our headquarters are working VEWY HAWD to fix this!!!!
        </p>
        <?php if(isset($GLOBALS['exception_catched'])) {
            echo "
            <div style='font-size: 1rem;'>
                <div class='exception-text'>
                    \"{$GLOBALS['exception_catched']['text']}\"<br>
                    Exception code {$GLOBALS['exception_catched']['code']}" .
                    ($modo_plus ? " in file {$file} at line {$GLOBALS['exception_catched']['line']}" : '') . "
                </div>
            </div>";
        } ?>
        <p>
            <a href='/'>Revenir à l'accueil</a>
        </p>
    </center>
</body>

</html>
