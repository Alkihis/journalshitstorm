-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  lun. 02 juil. 2018 à 12:21
-- Version du serveur :  10.1.26-MariaDB-0+deb9u1
-- Version de PHP :  7.2.7-1+0~20180622080745.23+stretch~1.gbpfd8e2e

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `journal`
--

-- --------------------------------------------------------

--
-- Structure de la table `Answers`
--

CREATE TABLE `Answers` (
  `idAn` int(11) NOT NULL,
  `idSub` int(11) NOT NULL COMMENT 'ID de la shitstorm liée à la réponse',
  `idUsrA` int(11) NOT NULL COMMENT 'ID utilisateur postant la réponse',
  `dscrA` mediumtext COLLATE utf8mb4_unicode_ci COMMENT 'Description',
  `linkA` text CHARACTER SET utf8mb4 NOT NULL COMMENT 'Lien twitter/lien logique img',
  `dateAn` date DEFAULT NULL COMMENT 'Date de la réponse',
  `approvalDateA` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'Date d''approbation',
  `idApprovatorA` int(11) NOT NULL COMMENT 'ID utilisateur de l''approbateur'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ComLikes`
--

CREATE TABLE `ComLikes` (
  `idCLike` int(11) NOT NULL,
  `idCom` int(11) NOT NULL,
  `idUsr` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Comment`
--

CREATE TABLE `Comment` (
  `content` mediumtext COLLATE utf8mb4_unicode_ci,
  `contentTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `inReplyToId` int(11) DEFAULT NULL,
  `inReplyToIdMain` int(11) DEFAULT NULL,
  `idUsr` int(11) NOT NULL,
  `idCom` int(11) NOT NULL,
  `idSub` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `Conversation`
--

CREATE TABLE `Conversation` (
  `idConv` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `idUsr1` int(11) NOT NULL,
  `idUsr2` int(11) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ConversationMessage`
--

CREATE TABLE `ConversationMessage` (
  `idMsg` int(11) NOT NULL,
  `idConv` int(11) NOT NULL,
  `idUsr` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `Followings`
--

CREATE TABLE `Followings` (
  `idFollow` int(11) NOT NULL,
  `idUsr` int(11) NOT NULL COMMENT 'User qui a émis la demande de follow',
  `idFollowed` int(11) NOT NULL COMMENT 'User qui se fait follow',
  `acceptMail` tinyint(1) NOT NULL DEFAULT '1',
  `followDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `Links`
--

CREATE TABLE `Links` (
  `idLink` int(11) NOT NULL,
  `link` tinytext CHARACTER SET utf8mb4 NOT NULL,
  `position` smallint(6) NOT NULL,
  `idSub` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Notifications`
--

CREATE TABLE `Notifications` (
  `idNotif` bigint(20) NOT NULL,
  `idDest` int(11) NOT NULL,
  `idUsr` int(11) NOT NULL COMMENT 'Émetteur de la notif',
  `idRelated` int(11) NOT NULL,
  `type` enum('LIKE_COMMENT','SHITSTORM','LIKE_SHITSTORM','DISLIKE_SHITSTORM','MENTION_COMMENT','POST_COMMENT','FOLLOW','CONFIRM_SHITSTORM','CONFIRM_ANSWER','POST_ANSWER','MODERATION_SHITSTORM','MODERATION_ANSWER') NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `viewed` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `RateLimits`
--

CREATE TABLE `RateLimits` (
  `idRate` int(11) NOT NULL,
  `type` varchar(80) NOT NULL,
  `count` int(11) NOT NULL,
  `expiration` bigint(20) NOT NULL,
  `idUsr` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `saveAnswer`
--

CREATE TABLE `saveAnswer` (
  `idSaveA` int(11) NOT NULL,
  `idSub` int(11) NOT NULL,
  `idAn` int(11) NOT NULL,
  `idTweet` bigint(20) NOT NULL,
  `content` text CHARACTER SET utf8mb4 NOT NULL,
  `infos` text CHARACTER SET utf8mb4 NOT NULL,
  `linkType` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `saveAnswerImg`
--

CREATE TABLE `saveAnswerImg` (
  `idSImgA` int(11) NOT NULL,
  `idSaveA` int(11) NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `saveTweet`
--

CREATE TABLE `saveTweet` (
  `idSave` int(11) NOT NULL,
  `idSub` int(11) NOT NULL,
  `idLink` int(11) NOT NULL,
  `idTweet` bigint(20) NOT NULL,
  `content` text CHARACTER SET utf8mb4 NOT NULL,
  `infos` text CHARACTER SET utf8mb4 NOT NULL,
  `linkType` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `saveTweetImg`
--

CREATE TABLE `saveTweetImg` (
  `idSImg` int(11) NOT NULL,
  `idSave` int(11) NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sharedSettings`
--

CREATE TABLE `sharedSettings` (
  `allowPosting` tinyint(1) NOT NULL DEFAULT '1',
  `allowUserPosting` tinyint(1) NOT NULL DEFAULT '1',
  `allowUnloggedPosting` tinyint(1) NOT NULL DEFAULT '1',
  `allowUserModification` tinyint(1) NOT NULL DEFAULT '1',
  `allowAllModification` tinyint(1) NOT NULL DEFAULT '1',
  `allowAnswerPosting` tinyint(1) NOT NULL DEFAULT '1',
  `allowUserCreation` tinyint(1) NOT NULL DEFAULT '1',
  `allowUserLogin` tinyint(1) NOT NULL DEFAULT '1',
  `allowLogin` tinyint(1) NOT NULL DEFAULT '1',
  `allowStayLogged` tinyint(1) NOT NULL DEFAULT '1',
  `allowWebsiteAccess` tinyint(1) NOT NULL DEFAULT '1',
  `idSet` int(11) NOT NULL,
  `lastUpdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `sharedSettings`
--

INSERT INTO `sharedSettings` (`allowPosting`, `allowUserPosting`, `allowUnloggedPosting`, `allowUserModification`, `allowAllModification`, `allowAnswerPosting`, `allowUserCreation`, `allowUserLogin`, `allowLogin`, `allowStayLogged`, `allowWebsiteAccess`, `idSet`, `lastUpdate`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 11, '2018-06-20 17:40:57');

-- --------------------------------------------------------

--
-- Structure de la table `ShitFollowings`
--

CREATE TABLE `ShitFollowings` (
  `idSFollow` int(11) NOT NULL,
  `idFollower` int(11) NOT NULL,
  `idFollowed` int(11) NOT NULL,
  `withMail` tinyint(1) NOT NULL DEFAULT '0',
  `followDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ShitLikes`
--

CREATE TABLE `ShitLikes` (
  `idSLike` int(11) NOT NULL,
  `idSub` int(11) NOT NULL,
  `idUsr` int(11) NOT NULL,
  `isLike` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Shitstorms`
--

CREATE TABLE `Shitstorms` (
  `title` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dscr` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pseudo` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idUsr` int(11) DEFAULT NULL,
  `idSub` int(11) NOT NULL,
  `dateShit` date DEFAULT NULL,
  `approvalDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `idApprovator` int(11) NOT NULL,
  `nbLikes` int(11) NOT NULL DEFAULT '0',
  `hidden` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `Tokens`
--

CREATE TABLE `Tokens` (
  `idToken` int(11) NOT NULL,
  `idUsr` int(11) NOT NULL,
  `token` text NOT NULL,
  `tokenType` enum('API','COOKIE','CONFIRM_MAIL','RESET_PASSWORD','APPLICATION','') NOT NULL,
  `lastUse` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `countUses` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `Users`
--

CREATE TABLE `Users` (
  `usrname` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `realname` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `passw` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `mail` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id` int(11) NOT NULL,
  `twitter_id` bigint(20) DEFAULT NULL,
  `moderator` smallint(2) NOT NULL DEFAULT '0',
  `profile_img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'img/default.png',
  `banner_img` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `genre` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'm',
  `token` varchar(130) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastlogin` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `registerdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastCMailSent` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT 'Dernier mail de confirm envoyé',
  `storm` tinyint(4) NOT NULL DEFAULT '0',
  `snow` tinyint(4) NOT NULL DEFAULT '0',
  `resetToken` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `authtoken` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `findable` tinyint(1) NOT NULL DEFAULT '1',
  `followable` tinyint(1) NOT NULL DEFAULT '1',
  `confirmedMail` tinyint(1) NOT NULL DEFAULT '0',
  `lastDataGeneration` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `lastDownloadArchive` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `access_token_twitter` text COLLATE utf8mb4_unicode_ci COMMENT 'JSON du access_token'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `WaitAnsw`
--

CREATE TABLE `WaitAnsw` (
  `dscrA` mediumtext COLLATE utf8mb4_unicode_ci,
  `linkA` mediumtext COLLATE utf8mb4_unicode_ci,
  `idUsrA` int(11) DEFAULT NULL,
  `idSubA` int(11) NOT NULL,
  `idSub` int(11) NOT NULL,
  `dateAn` date DEFAULT NULL,
  `fromWaitingShitstorm` tinyint(1) NOT NULL DEFAULT '0',
  `locked` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `Waiting`
--

CREATE TABLE `Waiting` (
  `title` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dscr` mediumtext COLLATE utf8mb4_unicode_ci,
  `pseudo` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idUsr` int(11) DEFAULT NULL,
  `idSub` int(11) NOT NULL,
  `dateShit` date NOT NULL,
  `locked` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `WaitingLinks`
--

CREATE TABLE `WaitingLinks` (
  `idLink` int(11) NOT NULL,
  `link` text,
  `idSub` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `Answers`
--
ALTER TABLE `Answers`
  ADD PRIMARY KEY (`idAn`,`idSub`);

--
-- Index pour la table `ComLikes`
--
ALTER TABLE `ComLikes`
  ADD PRIMARY KEY (`idCLike`,`idCom`,`idUsr`);

--
-- Index pour la table `Comment`
--
ALTER TABLE `Comment`
  ADD PRIMARY KEY (`idCom`,`idUsr`,`idSub`);

--
-- Index pour la table `Conversation`
--
ALTER TABLE `Conversation`
  ADD PRIMARY KEY (`idConv`);

--
-- Index pour la table `ConversationMessage`
--
ALTER TABLE `ConversationMessage`
  ADD PRIMARY KEY (`idMsg`);

--
-- Index pour la table `Followings`
--
ALTER TABLE `Followings`
  ADD PRIMARY KEY (`idFollow`);

--
-- Index pour la table `Links`
--
ALTER TABLE `Links`
  ADD PRIMARY KEY (`idLink`,`idSub`);

--
-- Index pour la table `Notifications`
--
ALTER TABLE `Notifications`
  ADD PRIMARY KEY (`idNotif`);

--
-- Index pour la table `RateLimits`
--
ALTER TABLE `RateLimits`
  ADD PRIMARY KEY (`idRate`);

--
-- Index pour la table `saveAnswer`
--
ALTER TABLE `saveAnswer`
  ADD PRIMARY KEY (`idSaveA`);

--
-- Index pour la table `saveAnswerImg`
--
ALTER TABLE `saveAnswerImg`
  ADD PRIMARY KEY (`idSImgA`);

--
-- Index pour la table `saveTweet`
--
ALTER TABLE `saveTweet`
  ADD PRIMARY KEY (`idSave`);

--
-- Index pour la table `saveTweetImg`
--
ALTER TABLE `saveTweetImg`
  ADD PRIMARY KEY (`idSImg`);

--
-- Index pour la table `sharedSettings`
--
ALTER TABLE `sharedSettings`
  ADD PRIMARY KEY (`idSet`);

--
-- Index pour la table `ShitFollowings`
--
ALTER TABLE `ShitFollowings`
  ADD PRIMARY KEY (`idSFollow`);

--
-- Index pour la table `ShitLikes`
--
ALTER TABLE `ShitLikes`
  ADD PRIMARY KEY (`idSLike`,`idSub`,`idUsr`);

--
-- Index pour la table `Shitstorms`
--
ALTER TABLE `Shitstorms`
  ADD PRIMARY KEY (`idSub`),
  ADD UNIQUE KEY `ind_title` (`title`);
ALTER TABLE `Shitstorms` ADD FULLTEXT KEY `ind_title_text` (`title`);
ALTER TABLE `Shitstorms` ADD FULLTEXT KEY `ind_dscr` (`dscr`);

--
-- Index pour la table `Tokens`
--
ALTER TABLE `Tokens`
  ADD PRIMARY KEY (`idToken`);

--
-- Index pour la table `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ind_usrname` (`usrname`);

--
-- Index pour la table `WaitAnsw`
--
ALTER TABLE `WaitAnsw`
  ADD PRIMARY KEY (`idSubA`);

--
-- Index pour la table `Waiting`
--
ALTER TABLE `Waiting`
  ADD PRIMARY KEY (`idSub`);

--
-- Index pour la table `WaitingLinks`
--
ALTER TABLE `WaitingLinks`
  ADD PRIMARY KEY (`idLink`,`idSub`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `Answers`
--
ALTER TABLE `Answers`
  MODIFY `idAn` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `ComLikes`
--
ALTER TABLE `ComLikes`
  MODIFY `idCLike` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `Comment`
--
ALTER TABLE `Comment`
  MODIFY `idCom` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `Conversation`
--
ALTER TABLE `Conversation`
  MODIFY `idConv` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `ConversationMessage`
--
ALTER TABLE `ConversationMessage`
  MODIFY `idMsg` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `Followings`
--
ALTER TABLE `Followings`
  MODIFY `idFollow` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `Links`
--
ALTER TABLE `Links`
  MODIFY `idLink` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `Notifications`
--
ALTER TABLE `Notifications`
  MODIFY `idNotif` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `RateLimits`
--
ALTER TABLE `RateLimits`
  MODIFY `idRate` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `saveAnswer`
--
ALTER TABLE `saveAnswer`
  MODIFY `idSaveA` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `saveAnswerImg`
--
ALTER TABLE `saveAnswerImg`
  MODIFY `idSImgA` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `saveTweet`
--
ALTER TABLE `saveTweet`
  MODIFY `idSave` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `saveTweetImg`
--
ALTER TABLE `saveTweetImg`
  MODIFY `idSImg` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `sharedSettings`
--
ALTER TABLE `sharedSettings`
  MODIFY `idSet` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `ShitFollowings`
--
ALTER TABLE `ShitFollowings`
  MODIFY `idSFollow` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `ShitLikes`
--
ALTER TABLE `ShitLikes`
  MODIFY `idSLike` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `Shitstorms`
--
ALTER TABLE `Shitstorms`
  MODIFY `idSub` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `Tokens`
--
ALTER TABLE `Tokens`
  MODIFY `idToken` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `Users`
--
ALTER TABLE `Users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `WaitAnsw`
--
ALTER TABLE `WaitAnsw`
  MODIFY `idSubA` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `Waiting`
--
ALTER TABLE `Waiting`
  MODIFY `idSub` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `WaitingLinks`
--
ALTER TABLE `WaitingLinks`
  MODIFY `idLink` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
