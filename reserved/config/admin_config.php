<?php

function generateAdminConfig() {
    global $connexion;

    // Formulaire d'entrée de la base de données
    if(isset($_POST['usr'], $_POST['psw'])) {
        $usrname = is_string($_POST['usr']) ? $_POST['usr'] : '';
        $psw = is_string($_POST['psw']) ? $_POST['psw'] : '';

        if($usrname && $psw) {
            connectBD();
            if (!$connexion) {
                echo '<h2>Impossible d\'utiliser la base de données (connexion impossible). Vérifiez les champs.</h2>';
            }
            else {
                $psw = password_hash($psw, PASSWORD_DEFAULT);
                $usrname = mysqli_real_escape_string($connexion, $usrname);

                $q = mysqli_query($connexion, "INSERT INTO Users (usrname, realname, passw, moderator) VALUES ('$usrname', '$usrname', '$psw', 2);");
                if($q) {
                    file_put_contents('.op', CONFIGURE_SITE);

                    header('Location: .');
                }
                else {
                    echo '<h2>Impossible d\'utiliser la base de données. Vérifiez les champs.</h2>';
                }
            }
            deconnectBD();
        }
        else {
            echo '<h2>Au moins un des champs est invalide.</h2>';
        }
    }

    ?>
    <center>
        <h1>Configuration de l'administrateur</h1>
        <p>
            Créeons maintenant l'utilisateur qui sera chargé d'administrer le site.
        </p>
        <hr>
        <form method='post' action='#'>
            <div class='inputs'>
                <div class='input-field'>
                    <label for='usr'>Nom d'utilisateur</label>
                    <input type='text' name='usr' id='usr'>  
                </div>
                
                <div class='input-field'>
                    <label for='psw'>Mot de passe</label>
                    <input type='text' name='psw' id='psw'>
                </div>
                <div class='clearb'></div>
            </div>


            <button type='submit'>Suivant</button>
        </form>
    </center>
    <?php
}
