<?php

function generateSiteConfig() {
    if(isset($_POST['complete'])) {
        file_put_contents('.op', COMPLETE);
        header('Location: ../../index.php');
    }

    ?>
    <center>
        <h1>Configuration du site</h1>
        <div class='container'>
            <p>
                C'est presque terminé.<br>
                Pour vous assurer que le Journal des Shitstorms fonctionnera, vous DEVEZ activer les .htaccess de votre serveur Apache.<br>
                <span class='underline'>Fichier httpd.conf / apache2.conf</span> : AllowOverride All<br><br>

                Vous devez impérativement configurer l'URL active du site pour qu'il fonctionne, dans les fichiers :
            </p>
            <ul>
                <li>&rsaquo; inc/const_string.php (constante HTTPS_URL minimum)</li>
                <li>&rsaquo; js/auto-init/const.js (constante https_url)</li>
            </ul>
            <br>
            <p>
                Le Journal des Shitstorms doit être à la racine d'un site activé sur Apache pour fonctionner correctement (utilisation de la constante
                DOCUMENT_ROOT).<br>
                Cependant vous pouvez émuler son fonctionnement dans un sous-répertoire en ajoutant la chaîne menant au dossier, après la constante
                $_SERVER['DOCUMENT_ROOT'], dans les fichiers :<br>
            </p>
            <ul>
                <li>&rsaquo; index.php</li>
                <li>&rsaquo; inc/constantes.php</li>
                <li>&rsaquo; api/apiconst.php</li>
                <li>&rsaquo; api/main.php</li>
                <li>&rsaquo; api/generation/shitstorm.php</li>
                <li>&rsaquo; Rajouter la chaîne à la définition de la constante ROOT dans inc/const_string.php</li>
            </ul>
            <br>
            <p>
                Si vous ne possédez pas de serveur Apache et utilisez une méthode de configuration différente, renseignez-vous sur l'équivalent des .htaccess et
                de la constante DOCUMENT_ROOT. Notez que la constante REDIRECT_URL est également utilisée.
            </p>
        </div>
        <hr>
        <form method='post' action='#'>
            <button type='submit' name='complete'>Terminer</button>
        </form>
    </center>
    <?php
}
