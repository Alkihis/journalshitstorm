<?php

function generateBDDConfig() {
    global $connexion;

    // Formulaire d'entrée de la base de données
    if(isset($_POST['usr'], $_POST['name'], $_POST['psw'], $_POST['srv'])) {
        $usrname = is_string($_POST['usr']) ? $_POST['usr'] : '';
        $psw = is_string($_POST['psw']) ? $_POST['psw'] : '';
        $db_name = is_string($_POST['name']) ? $_POST['name'] : '';
        $srv_name = is_string($_POST['srv']) ? $_POST['srv'] : '';

        if($usrname && $psw && $db_name) {
            $connexion = mysqli_connect($srv_name, $usrname, $psw);
            if (mysqli_connect_errno()) {
                echo '<h2>Impossible d\'utiliser la base de données. Vérifiez les champs.</h2>';
            }
            else {
                mysqli_query($connexion, 'SET NAMES UTF8mb4'); // requete pour avoir les noms en UTF8mb4

                $db_name = mysqli_real_escape_string($connexion, $db_name);
                mysqli_query($connexion, "CREATE DATABASE IF NOT EXISTS $db_name CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;");

                $q = mysqli_query($connexion, "USE $db_name;");
                if($q) {
                    $q = mysqli_multi_query($connexion, file_get_contents('journal_base.sql'));

                    // Base de données crée
                    $consumer = is_string($_POST['cst']) ? $_POST['cst'] : '';
                    $c_secret = is_string($_POST['css']) ? $_POST['css'] : '';
                    $access = is_string($_POST['ast']) ? $_POST['ast'] : '';
                    $a_secret = is_string($_POST['ass']) ? $_POST['ass'] : '';

                    // Sauvegarde de la configuration

                    $const = file_get_contents('../../inc/constantes.sample.php');

                    $const = sprintf($const, $srv_name, $usrname, $psw, $db_name, $consumer, $c_secret, $access, $a_secret);

                    file_put_contents('../../inc/constantes.php', $const);

                    file_put_contents('.op', CONFIGURE_ADMIN);

                    header('Location: .');
                }
                else {
                    echo '<h2>Impossible d\'utiliser la base de données. Vérifiez les champs.</h2>';
                }
                deconnectBD();
            }
        }
        else {
            echo '<h2>Au moins un des champs est invalide.</h2>';
        }
    }

    ?>
    <center>
        <h1>Configuration de la base de données</h1>
        <p>
            Le Journal des Shitstorms demande une base de données compatible MySQL (MariaDB ou MySQL) et est administable grâce à PHPMyAdmin.<br>
            Pour pouvoir utiliser le Journal des Shitstorms, merci d'entrer le nom de la base de données dans laquelle paramétrer le Journal.<br>
            Si elle existe, elle DOIT être vide et comporter le jeu de caractère utf8mb4.<br>
        </p>
        <hr>
        <form method='post' action='#'>
            <div class='inputs'>
                <div class='input-field'>
                    <label for='srv'>Serveur (adresse IP ou localhost)</label>
                    <input type='text' name='srv' id='srv' value='localhost'>
                </div>
                <div class='clearb'></div>

                <div class='input-field'>
                    <label for='name'>Nom de la base de données</label>
                    <input type='text' name='name' id='name' value='journal'>
                </div>
                <div class='clearb'></div>
            
                <div class='input-field'>
                    <label for='usr'>Nom d'utilisateur MySQL</label>
                    <input type='text' name='usr' id='usr' value='root'>  
                </div>
                <div class='clearb'></div>
                
                <div class='input-field'>
                    <label for='psw'>Mot de passe MySQL</label>
                    <input type='text' name='psw' id='psw' value='secret'>
                </div>
                <div class='clearb'></div>

                <div class='clearb'></div>
                <p>
                    Les champs suivants sont optionnels.<br>Ils deviennent utilisés lorsqu'un tweet veut être sauvegardé ou lorsqu'un
                    utilisateur lie son compte avec Twitter.<br>Créez votre propre application sur <a href='https://apps.twitter.com'>https://apps.twitter.com</a> et spécifiez vos tokens ici.
                </p>
                <div class='input-field'>
                    <label for='cst'>Consumer token</label>
                    <input type='text' name='cst' id='cst'>
                </div>
                <div class='clearb'></div>

                <div class='input-field'>
                    <label for='css'>Consumer secret token</label>
                    <input type='text' name='css' id='css'>
                </div>
                <div class='clearb'></div>

                <div class='input-field'>
                    <label for='ast'>Access token</label>
                    <input type='text' name='ast' id='ast'>
                </div>
                <div class='clearb'></div>

                <div class='input-field'>
                    <label for='ass'>Access secret token</label>
                    <input type='text' name='ass' id='ass'>
                </div>
                <div class='clearb'></div>
            </div>


            <button type='submit'>Suivant</button>
        </form>
    </center>
    <?php
}
