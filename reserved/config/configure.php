<?php
    ob_start();
    ini_set('display_errors', 'on');
?><html>
    <head>
        <title>Configuration du Journal des Shitstorms</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link rel="stylesheet" href="styles_config.css">
    </head>

    <body>
        <?php
        $connexion = null;

        if(file_exists('../../inc/constantes.php'))
            require('../../inc/constantes.php');

        // connexion à la BD
        function connectBD() : void {
            global $connexion;
            $connexion = mysqli_connect(SERVEUR, UTILISATRICE, MOTDEPASSE, BDD);
            if (mysqli_connect_errno()) {
                printf("Échec de la connexion : %s\n", mysqli_connect_error());
            }
            else 
                mysqli_query($connexion, 'SET NAMES UTF8mb4'); // requete pour avoir les noms en UTF8mb4
        }

        function deconnectBD() {
            global $connexion;
            if($connexion)
                mysqli_close($connexion);
        }

        const COMPLETE = '4';
        const CONFIGURE_SITE = '3';
        const CONFIGURE_ADMIN = '2';
        const CONFIGURE_BDD = '1';

        // ------------------------------------------------
        // FICHIER DE DEPLOYEMENT DU JOURNAL DES SHITSTORMS
        // ------------------------------------------------

        // Fichier censé être exécuté seul (hors du index.php, par son adresse directement)

        if(file_exists('.op')) {
            $op = file_get_contents('.op');

            if(!$op || $op == COMPLETE) {
                // La configuration est terminée ou a échoué
                // Supprimez .op et réessayez
                exit();
            }
            else if ($op === CONFIGURE_BDD) {
                require('bdd_config.php');
                generateBDDConfig();
            }
            else if ($op === CONFIGURE_ADMIN) {
                require('admin_config.php');
                generateAdminConfig();
            }
            else if ($op === CONFIGURE_SITE) {
                require('site_config.php');
                generateSiteConfig();
            }
        }
        else if(isset($_GET['go_config']) && $_GET['go_config'] === 'true') {
            file_put_contents('.op', CONFIGURE_BDD);

            require('bdd_config.php');
            generateBDDConfig();
        }
        else { // Bienvenue ?>
            <center>
                <h1>Bienvenue dans la configuration du Journal des Shitstorms</h1>

                <p>
                    Cet utilitaire va vous aider à configurer dans les grandes lignes le Journal des Shitstorms.<br>
                    Pour des raisons de sécurité, après la configuration, vous devez supprimer cet utilitaire.<br>
                    
                </p>
                <hr/>
                <a href='?go_config=true'><button>C'est parti</button></a>
            </center>
        <?php } ?>
    </body>
</html><?php
    ob_flush();
