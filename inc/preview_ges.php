<?php

// Fichiers ZIP à supprimer (expirés)
$files = [];

$files = glob('/var/www/zipFiles/*.zip');
usort($files, function($a, $b) {
    return filemtime($a) > filemtime($b);
});

$max_time = time() - (3600 * 48);
// Fichiers à ne pas supprimer : Fichier qui ont un timestamp compris entre now() et now() - 48h
foreach($files as $f){
    if(filemtime($f) < $max_time){
        unlink($f);
    }
}
