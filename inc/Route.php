<?php

class Route {
    protected $full_url;
    protected $url_parameters;
    protected $page;
    protected $complete_par;

    // Base dir needs to be formatted like "journal/" if site is in domain.com/journal/, empty if dir is document root
    public function __construct($base_dir = '', $default_page = '', $request = null) {
        if(!$request) {
            $request = $_SERVER['REDIRECT_URL'];
        }
        // Where to look after explode. Request start with a /, first value will be after the /, [1] position.
        $offset = 1;

        // Calc offset value
        $str_offset = explode('/', $base_dir);
        $offset = count($str_offset);

        $this->full_url = $request;

        $par = explode('/', $request);

        if(count($par) < $offset) {
            throw new RuntimeException('Unable to fetch parameters. Less parameters than offset.');
        }

        if($par[$offset] === '') { // If query is for root
            $par[$offset] = $default_page;
        }

        // $par[0] will always be empty
        for($i = $offset; $i < count($par); $i++) {
            $this->url_parameters[] = $par[$i];
        }

        // $par[offset] will always be base page to be fetched
        $this->page = $par[$offset];
        $this->complete_par = $par;
    }

    // Give page name affiliated to current query
    public function getPage() : string {
        return $this->page;
    }

    public function getMainUrlOption() : ?string {
        return (isset($this->url_parameters[1]) ? $this->url_parameters[1] : null);
    }

    public function getAdditionalUrlOption() : ?string {
        return (isset($this->url_parameters[2]) ? $this->url_parameters[2] : null);
    }

    public function getDeepUrlOption() : ?string {
        return (isset($this->url_parameters[3]) ? $this->url_parameters[3] : null);
    }

    public function manualGetUrlOption(int $offset) : ?string {
        return (isset($this->url_parameters[$offset]) ? $this->url_parameters[$offset] : null);
    }

    public function getCompleteParameters() : array {
        return $this->complete_par;
    }
}
