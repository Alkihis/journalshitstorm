<?php

function genericMail($mailDest, $title, $subject, $text_content, $content, $cause){
    $mail = $mailDest; // Déclaration de l'adresse de destination.
    if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) // On filtre les serveurs qui présentent des bogues.
    {
        $passage_ligne = "\r\n";
    }
    else
    {
        $passage_ligne = "\n";
    }
    //=====Déclaration des messages au format texte et au format HTML.
    $message_txt = $text_content;
    $message_html = getHTMLMailCore(
                    $title,
                    $content,
                    $cause);
    //==========
    
    //=====Création de la boundary.
    $boundary = "-----=".md5(rand());
    $boundary_alt = "-----=".md5(rand());
    //==========
    
    //=====Définition du sujet.
    $sujet = $subject;
    //=========
    
    //=====Création du header de l'e-mail.
    $header = "From: \"Journal des shitstorms\"<post@shitstorm.fr>".$passage_ligne;
    
    $header.= "MIME-Version: 1.0".$passage_ligne;
    $header.= "Content-Type: multipart/mixed;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
    //==========
    
    //=====Création du message.
    $message = $passage_ligne."--".$boundary.$passage_ligne;
    $message.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary_alt\"".$passage_ligne;
    $message.= $passage_ligne."--".$boundary_alt.$passage_ligne;
    //=====Ajout du message au format texte.
    $message.= "Content-Type: text/plain; charset=\"utf-8\"".$passage_ligne;
    $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
    $message.= $passage_ligne.$message_txt.$passage_ligne;
    //==========
    
    $message.= $passage_ligne."--".$boundary_alt.$passage_ligne;
    
    //=====Ajout du message au format HTML.
    $message.= "Content-Type: text/html; charset=\"utf-8\"".$passage_ligne;
    $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
    $message.= $passage_ligne.$message_html.$passage_ligne;
    //==========
    
    //=====On ferme la boundary alternative.
    $message.= $passage_ligne."--".$boundary_alt."--".$passage_ligne;
    //==========

    //=====Envoi de l'e-mail.
    return mail($mail, $sujet, $message, $header);
}

function sendMailReset($token, $mailDest){
    $mail = $mailDest; // Déclaration de l'adresse de destination.
    if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) // On filtre les serveurs qui présentent des bogues.
    {
        $passage_ligne = "\r\n";
    }
    else
    {
        $passage_ligne = "\n";
    }
    //=====Déclaration des messages au format texte et au format HTML.
    $message_txt = "Vous avez demandé une réinitialisation du mot de passe. Voici le lien de réinitialisation : https://shitstorm.fr/passwordreset?token=$token";
    $message_html = getHTMLMailCore(
                    "Vous avez demandé une réinitialisation du mot de passe.",
                    "Si ce n'est pas vous qui avez demandé cette réinitialisation, déconnectez-vous et reconnectez-vous.<br><br>
                        Sinon, voici le <a href='https://shitstorm.fr/passwordreset?token=$token'>lien de réinitialisation</a>",
                    "vous avez demandé une réinitialisation du mot de passe depuis les paramètres de votre compte");
    //==========
    
    //=====Création de la boundary.
    $boundary = "-----=".md5(rand());
    $boundary_alt = "-----=".md5(rand());
    //==========
    
    //=====Définition du sujet.
    $sujet = "Réinitialisation du mot de passe";
    //=========
    
    //=====Création du header de l'e-mail.
    $header = "From: \"Journal des shitstorms\"<recuperation@shitstorm.fr>".$passage_ligne;
    $header.= "Reply-to: \"Journal des shitstorms\" <recuperation@shitstorm.fr>".$passage_ligne;
    $header.= "MIME-Version: 1.0".$passage_ligne;
    $header.= "Content-Type: multipart/mixed;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
    //==========
    
    //=====Création du message.
    $message = $passage_ligne."--".$boundary.$passage_ligne;
    $message.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary_alt\"".$passage_ligne;
    $message.= $passage_ligne."--".$boundary_alt.$passage_ligne;
    //=====Ajout du message au format texte.
    $message.= "Content-Type: text/plain; charset=\"utf-8\"".$passage_ligne;
    $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
    $message.= $passage_ligne.$message_txt.$passage_ligne;
    //==========
    
    $message.= $passage_ligne."--".$boundary_alt.$passage_ligne;
    
    //=====Ajout du message au format HTML.
    $message.= "Content-Type: text/html; charset=\"utf-8\"".$passage_ligne;
    $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
    $message.= $passage_ligne.$message_html.$passage_ligne;
    //==========
    
    //=====On ferme la boundary alternative.
    $message.= $passage_ligne."--".$boundary_alt."--".$passage_ligne;
    //==========

    //=====Envoi de l'e-mail.
    return mail($mail, $sujet, $message, $header);
}

function sendMailShitstorm($mailDest, $idSub, $title, $dscr, $usrnameEmitter, $idEmitter){
    $mail = $mailDest; // Déclaration de l'adresse de destination.
    if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)){
        $passage_ligne = "\r\n";
    }
    else{
        $passage_ligne = "\n";
    }

    $title = htmlentities($title);
    //=====Déclaration des messages au format texte et au format HTML.
    $message_txt = "$usrnameEmitter a posté une nouvelle shitstorm ($title) !\nRetrouvez la sur https://shitstorm.fr/unique?idPubli=$idSub !";
    $message_html = getHTMLMailCore(
        "<a href='https://shitstorm.fr/profil/$usrnameEmitter'>$usrnameEmitter</a> a posté une nouvelle shitstorm !",
        "<h4><a href='https://shitstorm.fr/unique?idPubli=$idSub'>$title</a></h4>
        <div class='description'>
            <p>".htmlentities($dscr)."</p>
        </div>
        <span style='font-style: italic;'>Vous pouvez retrouver cette shitstorm sur le <a href='https://shitstorm.fr/unique?idPubli=$idSub'>site du Journal des Shitstorms</a></span>",
        "vous êtes abonné à <a href='https://shitstorm.fr/profil/$usrnameEmitter'>$usrnameEmitter</a>. Rendez-vous sur son profil pour vous désabonner");
    //==========
    
    //=====Création de la boundary.
    $boundary = "-----=".md5(rand());
    $boundary_alt = "-----=".md5(rand());
    //==========
    
    //=====Définition du sujet.
    $sujet = "Nouvelle shitstorm de $usrnameEmitter";
    //=========
    
    //=====Création du header de l'e-mail.
    $header = "From: \"Journal des shitstorms\"<post@shitstorm.fr>".$passage_ligne;
    $header.= "MIME-Version: 1.0".$passage_ligne;
    $header.= "Content-Type: multipart/mixed;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
    //==========
    
    //=====Création du message.
    $message = $passage_ligne."--".$boundary.$passage_ligne;
    $message.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary_alt\"".$passage_ligne;
    $message.= $passage_ligne."--".$boundary_alt.$passage_ligne;
    //=====Ajout du message au format texte.
    $message.= "Content-Type: text/plain; charset=\"utf-8\"".$passage_ligne;
    $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
    $message.= $passage_ligne.$message_txt.$passage_ligne;
    //==========
    
    $message.= $passage_ligne."--".$boundary_alt.$passage_ligne;
    
    //=====Ajout du message au format HTML.
    $message.= "Content-Type: text/html; charset=\"utf-8\"".$passage_ligne;
    $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
    $message.= $passage_ligne.$message_html.$passage_ligne;
    //==========
    
    //=====On ferme la boundary alternative.
    $message.= $passage_ligne."--".$boundary_alt."--".$passage_ligne;
    //==========

    //=====Envoi de l'e-mail.
    return mail($mail, $sujet, $message, $header);
}

function sendMailNewComment($mailDest, $idSub, $title, $content, $realnameEmitter, $idEmitter){
    $mail = $mailDest; // Déclaration de l'adresse de destination.
    if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)){
        $passage_ligne = "\r\n";
    }
    else{
        $passage_ligne = "\n";
    }

    $title = htmlentities($title);
    //=====Déclaration des messages au format texte et au format HTML.
    $message_txt = "$realnameEmitter a commenté la shitstorm $title !\nRetrouvez son commentaire sur https://shitstorm.fr/unique?idPubli=$idSub !";
    $message_html = getHTMLMailCore(
        "<a href='https://shitstorm.fr/profil/$idEmitter'>$realnameEmitter</a> a commenté la shitstorm $title !",
        "<div class='description'>
            <p>".htmlentities($content)."</p>
        </div>
        <span style='font-style: italic;'>Vous pouvez retrouver cette shitstorm sur le <a href='https://shitstorm.fr/unique?idPubli=$idSub#comments_b'>site du Journal des Shitstorms</a></span>",
        "vous êtes abonné à la shitstorm <a href='https://shitstorm.fr/unique?idPubli=$idSub'>$title</a>. Rendez-vous sur sa page pour vous désabonner");
    //==========
    
    //=====Création de la boundary.
    $boundary = "-----=".md5(rand());
    $boundary_alt = "-----=".md5(rand());
    //==========
    
    //=====Définition du sujet.
    $sujet = "Nouveau commentaire de $realnameEmitter sous une shitstorm que vous suivez";
    //=========
    
    //=====Création du header de l'e-mail.
    $header = "From: \"Journal des shitstorms\"<post@shitstorm.fr>".$passage_ligne;
    $header.= "MIME-Version: 1.0".$passage_ligne;
    $header.= "Content-Type: multipart/mixed;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
    //==========
    
    //=====Création du message.
    $message = $passage_ligne."--".$boundary.$passage_ligne;
    $message.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary_alt\"".$passage_ligne;
    $message.= $passage_ligne."--".$boundary_alt.$passage_ligne;
    //=====Ajout du message au format texte.
    $message.= "Content-Type: text/plain; charset=\"utf-8\"".$passage_ligne;
    $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
    $message.= $passage_ligne.$message_txt.$passage_ligne;
    //==========
    
    $message.= $passage_ligne."--".$boundary_alt.$passage_ligne;
    
    //=====Ajout du message au format HTML.
    $message.= "Content-Type: text/html; charset=\"utf-8\"".$passage_ligne;
    $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
    $message.= $passage_ligne.$message_html.$passage_ligne;
    //==========
    
    //=====On ferme la boundary alternative.
    $message.= $passage_ligne."--".$boundary_alt."--".$passage_ligne;
    //==========

    //=====Envoi de l'e-mail.
    return mail($mail, $sujet, $message, $header);
}

function sendMailNewAnswer($mailDest, $idSub, $title, $content, $realnameEmitter, $idEmitter){
    $mail = $mailDest; // Déclaration de l'adresse de destination.
    if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)){
        $passage_ligne = "\r\n";
    }
    else{
        $passage_ligne = "\n";
    }

    $title = htmlentities($title);
    //=====Déclaration des messages au format texte et au format HTML.
    $message_txt = "$realname a posté une réponse pour la shitstorm $title !\nRetrouvez là sur https://shitstorm.fr/unique?idPubli=$idSub#answer_b !";
    $message_html = getHTMLMailCore(
        "<a href='https://shitstorm.fr/profil/$idEmitter'>$realnameEmitter</a> a posté une réponse pour la shitstorm $title !",
        "<div class='description'>
            <p>".htmlentities($content)."</p>
        </div>
        <span style='font-style: italic;'>Vous pouvez retrouver cette shitstorm sur le <a href='https://shitstorm.fr/unique?idPubli=$idSub#answer_b'>site du Journal des Shitstorms</a></span>",
        "vous êtes abonné à la shitstorm <a href='https://shitstorm.fr/unique?idPubli=$idSub'>$title</a>. Rendez-vous sur sa page pour vous désabonner");
    //==========
    
    //=====Création de la boundary.
    $boundary = "-----=".md5(rand());
    $boundary_alt = "-----=".md5(rand());
    //==========
    
    //=====Définition du sujet.
    $sujet = "Nouvelle réponse pour une shitstorm que vous suivez";
    //=========
    
    //=====Création du header de l'e-mail.
    $header = "From: \"Journal des shitstorms\"<post@shitstorm.fr>".$passage_ligne;
    $header.= "MIME-Version: 1.0".$passage_ligne;
    $header.= "Content-Type: multipart/mixed;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
    //==========
    
    //=====Création du message.
    $message = $passage_ligne."--".$boundary.$passage_ligne;
    $message.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary_alt\"".$passage_ligne;
    $message.= $passage_ligne."--".$boundary_alt.$passage_ligne;
    //=====Ajout du message au format texte.
    $message.= "Content-Type: text/plain; charset=\"utf-8\"".$passage_ligne;
    $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
    $message.= $passage_ligne.$message_txt.$passage_ligne;
    //==========
    
    $message.= $passage_ligne."--".$boundary_alt.$passage_ligne;
    
    //=====Ajout du message au format HTML.
    $message.= "Content-Type: text/html; charset=\"utf-8\"".$passage_ligne;
    $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
    $message.= $passage_ligne.$message_html.$passage_ligne;
    //==========
    
    //=====On ferme la boundary alternative.
    $message.= $passage_ligne."--".$boundary_alt."--".$passage_ligne;
    //==========

    //=====Envoi de l'e-mail.
    return mail($mail, $sujet, $message, $header);
}

function sendConfirmationMail($mailDest, $token){
    $mail = $mailDest; // Déclaration de l'adresse de destination.
    if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)){
        $passage_ligne = "\r\n";
    }
    else{
        $passage_ligne = "\n";
    }

    //=====Déclaration des messages au format texte et au format HTML.
    $message_txt = "Confirmez votre adresse mail en cliquant sur ce lien : https://shitstorm.fr/compte?token=$token";
    $message_html = getHTMLMailCore(
        "Vous avez configuré une nouvelle adresse e-mail pour votre compte",
        "Cliquez sur le lien ci-dessous pour confirmer l'adresse e-mail.<br><br>
            <a href='https://shitstorm.fr/compte?token=$token'>Confirmer mon adresse</a>",
        "vous avez inscrit cette adresse e-mail sur votre compte du <a href='https://shitstorm.fr/compte'>Journal des shitstorms</a>");
    
    //=====Création de la boundary.
    $boundary = "-----=".md5(rand());
    $boundary_alt = "-----=".md5(rand());
    //==========
    
    //=====Définition du sujet.
    $sujet = "Confirmez votre adresse e-mail";
    //=========
    
    //=====Création du header de l'e-mail.
    $header = "From: \"Journal des shitstorms\"<post@shitstorm.fr>".$passage_ligne;
    $header.= "MIME-Version: 1.0".$passage_ligne;
    $header.= "Content-Type: multipart/mixed;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
    //==========
    
    //=====Création du message.
    $message = $passage_ligne."--".$boundary.$passage_ligne;
    $message.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary_alt\"".$passage_ligne;
    $message.= $passage_ligne."--".$boundary_alt.$passage_ligne;
    //=====Ajout du message au format texte.
    $message.= "Content-Type: text/plain; charset=\"utf-8\"".$passage_ligne;
    $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
    $message.= $passage_ligne.$message_txt.$passage_ligne;
    //==========
    
    $message.= $passage_ligne."--".$boundary_alt.$passage_ligne;
    
    //=====Ajout du message au format HTML.
    $message.= "Content-Type: text/html; charset=\"utf-8\"".$passage_ligne;
    $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
    $message.= $passage_ligne.$message_html.$passage_ligne;
    //==========
    
    //=====On ferme la boundary alternative.
    $message.= $passage_ligne."--".$boundary_alt."--".$passage_ligne;
    //==========

    //=====Envoi de l'e-mail.
    return mail($mail, $sujet, $message, $header);
}

function getHTMLMailCore(string $title, string $content, string $cause) : string {
    ob_start();
    ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html>
        <head>
            <meta charset="utf-8">
        </head>
        <body style="
            margin:0;
            padding-top:0;
            padding-bottom:0;
            padding-right:0;
            padding-left:0;
            min-width:100%;">

            <style type="text/css">
                * {
                    -webkit-font-smoothing: antialiased;
                }
                body {
                    margin: 0;
                    padding: 0;
                    min-width: 100%;
                    font-family: ArialMT, Helvetica, Arial, sans-serif;
                    -webkit-font-smoothing: antialiased;
                }
                table {
                    border-spacing: 0;
                    font-family: ArialMT, Helvetica, Arial, sans-serif;
                }
                a {
                    color: #00AFF5;
                    text-decoration: none !important;
                }
                a:hover {
                    text-decoration: underline !important;
                }
                img {
                    border: 0;
                }
                .wrapper {
                    width: 100%;
                    table-layout: fixed;
                    -webkit-text-size-adjust: 100%;
                    -ms-text-size-adjust: 100%;
                }
                .webkit {
                    max-width: 500px;
                }
                .outer {
                    margin: 0 auto;
                    padding: 24px;
                    width: 100%;
                    max-width: 500px;
                }
            </style>

            <center class="wrapper" style="
                    width:100%;
                    table-layout:fixed;
                    -webkit-text-size-adjust:100%;
                    -ms-text-size-adjust:100%;">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td width="100%">
                            <div class="webkit" style="max-width: 600px; margin: 0 auto;">
                                <table class="outer" align="center" cellpadding="0" cellspacing="0" border="0" style="
                                        border-spacing: 0;
                                        font-family: ArialMT, Helvetica, Arial, sans-serif;
                                        margin: 0 auto;
                                        padding: 24px;
                                        width: 100%;
                                        max-width: 500px;">
                                    <tr>
                                        <td>
                                            <table style="margin-bottom: 40px; width: 100%" width="100%">
                                                <tr>
                                                    <td>
                                                        <a href="" style="font-family:Helvetica, Arial, sans-serif; color:#0086BF;">
                                                            <img src="https://shitstorm.fr/img/js_logo_300_text.png" style="display:block;"
                                                                alt="Journal des Shitstorms" width="170" height="30" />
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;">
                                            <table style="margin-bottom: 20px; width: 100%" width="100%">
                                                <tr>
                                                    <td>
                                                        <table style="width: 100%; margin-bottom: 20px" width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td>
                                                                    <h1 style="font-size: 26px; line-height: 30px; color:#054752;">
                                                                        <?= $title ?>
                                                                    </h1>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <table style="width: 100%; margin-bottom: 20px" width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td>
                                                                    <p style="color: #708C91; font-size: 16px; line-height: 22px;">
                                                                        <?= $content ?>
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <tr>
                                                <td>
                                                    <table width="100%" style="margin-bottom: 20px; width: 100%;">
                                                        <tr>
                                                            <td width="100%">
                                                                <div style="width:100%; height: 1px; background-color: #DDD;" color="#DDD" width="100%"></div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center;">
                                            <img src="https://shitstorm.fr/img/js_logo_300.png" alt="" style="display: block; width:29px; height: auto; margin-left: auto; margin-right: auto;  margin-bottom:10px;"
                                                width="29px" height="auto">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; font-size: 13px;">
                                            <a href="https://shitstorm.fr" style="color: #00AFF5;">
                                                Journal des shitstorms
                                            </a>
                                            <span style="color: #00AFF5;">|</span>
                                            <a href="https://shitstorm.fr/data_protection#mails" style="color: #00AFF5;">
                                                Informations sur cet e-mail
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center;">
                                            <table style="max-width: 100%; width: 100%; text-align: center; font-family: ArialMT, Arial, sans-serif;" cellspacing="0"
                                                cellpadding="0">
                                                <tr>
                                                    <td style="text-align: center;">
                                                        <p style="font-size: 8px; color: #708C91; text-align: center; padding: 0; margin-top: 10px; margin-bottom: 2px;">
                                                            Vous recevez cet e-mail car <?= $cause ?>.
                                                        </p>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </center>
        </body>
    </html>
    <?php
    return ob_get_clean();
}
