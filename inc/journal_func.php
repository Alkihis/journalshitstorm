<?php

// Fonctions du journal

// ------------------------------------
// RECUPERATION DE MOIS POUR LE JOURNAL
// ------------------------------------

function getPastLastLegitMonths($dateBase, $dateMax, bool $by_year = false) : array { // Retourne un tableau contenant precMonth, precYear, nextMonth, nextYear
    global $connexion;
    
    // Recherche shitstorm dans mois précédent
    $return = [];
    $precMonth = &$return['prec_month'];
    $precYear = &$return['prec_year'];
    $nextMonth = &$return['next_month'];
    $nextYear = &$return['next_year'];

    if($by_year){
        $dateBase = explode('-', $dateBase)[0];
    }

    if($by_year){ // precMonth => precYear
        $precMonth = ($dateBase - 1) . '-12';
    }
    else{
        $precMonth = getPastNextMonth(true, $dateBase);
    }

    $resPMonth = mysqli_query($connexion, "SELECT * FROM Shitstorms WHERE dateShit BETWEEN '0000-01-01' AND '$precMonth-31' ORDER BY dateShit DESC;");
    if($resPMonth && mysqli_num_rows($resPMonth) > 0){
        $rowPMonth = mysqli_fetch_assoc($resPMonth);
        $precMonth = $rowPMonth['dateShit'];
        $precMonth = explode('-', $precMonth);
        $precYear = $precMonth[0];
        $precMonth = $precMonth[1];
    }
    else{
        $precMonth = null;
    }
    // Recherche shitstorm mois suivant
    
    if($by_year){ // precMonth => precYear
        $nextMonth = ($dateBase + 1) . '-01';
    }
    else{
        $nextMonth = getPastNextMonth(false, $dateBase);
    }

    if($nextMonth){ // Si le mois suivant est possible
        $resNMonth = mysqli_query($connexion, "SELECT * FROM Shitstorms WHERE dateShit BETWEEN '$nextMonth-01' AND '9999-12-31' ORDER BY dateShit ASC;");
        if($resNMonth && mysqli_num_rows($resNMonth) > 0){
            $rowNMonth = mysqli_fetch_assoc($resNMonth);
            $nextMonth = $rowNMonth['dateShit'];
            $nextMonth = explode('-', $nextMonth);
            $nextYear = $nextMonth[0];
            $nextMonth = $nextMonth[1];
        }
        elseif($by_year && ($dateBase + 1) > date('Y')){ // Si l'année choisie est l'année est en cours
            $nextMonth = null;
        }
        else{ // Si il n'y a pas de shitstorm suivante dans la BDD, mais que le mois suivant est un mois possible, alors le mois suivant accessible est le mois en cours (même si il est vide)
            $nextMonth = explode('-', $dateMax);
            $nextYear = $nextMonth[0];
            $nextMonth = $nextMonth[1];
        }
    }
    
    return $return;
}

function getPastNextMonth($isPast = true, $date){
    $dateBase = date('Y-m');
    try{
        if(!$isPast){
            $date = new DateTime($date);
            $date->add(new DateInterval('P1M')); //Où 'P1M' indique 'Période de 1 Mois'

            $actual = new DateTime($dateBase);
            if($date > $actual){
                return NULL;
            }
            else{
                return $date->format('Y-m');
            }
        }
        $date = new DateTime($date);
        $date->sub(new DateInterval('P1M')); //Où 'P12M' indique 'Période de 12 Mois'
        
        return $date->format('Y-m');
        
    } catch (Exception $E) {
        echo "<h5 class='error'>Erreur lors de l'obtention d'une date.</h5>";
    }
}

function generatePageButtons(&$p, $add_param = NULL, $params = NULL){
    // Initialisation des paramètres
    $per_page = $p['per_page'];
    $page_actuelle = $p['actual'];
    $affichage_fleche_first = $p['ffirst'];
    $affichage_fleche_last = $p['flast'];
    $p_start = $p['pstart'];
    $p_end = $p['pend'];
    $ancient_view = $p['previous'];
    $next_view = $p['next'];
    $bornes = &$p['bornes'];
    $inside = &$p['inside'];

    $nextYear = $nextMonth = $precYear = $precMonth = NULL;

    if($params){
        $nextYear = $params['next_year'];
        $nextMonth = $params['next_month'];
        $precYear = $params['prec_year'];
        $precMonth = $params['prec_month'];
    }

    ?>
    <div class='row'>
        <div class='col s12'>
            <ul class="pagination center">
                <?php if($add_param && $params){ ?>
                    <!-- Mois précédent -->
                    <li style='/*vertical-align: middle;*/' class="<?= ($precMonth ? 'waves-effect tooltipped" data-position="bottom" data-delay="10" data-tooltip="'.
                                                                    getTextualMonth($precMonth)." $precYear" : 'disabled') ?>">
                    <a href="<?= ($precMonth ? HTTPS_URL."journal?perlikeview=0&selyear=$precYear&selmonth=$precMonth" : '#!' ) ?>"><i class="material-icons">arrow_back</i></a></li>
                <?php }

                if ($bornes[0]['number'] > 0) : // Si il y a au moins une shitstorm {soit une page} ?>
                    <!-- Bouton précédent -->
                    <li class='<?= ($affichage_fleche_first ? 'waves-effect' : 'disabled') ?>'>
                        <a href="<?= ($affichage_fleche_first ? HTTPS_URL."journal?perlikeview=$ancient_view".($per_page ? "&perlikepage=$per_page" : '').($add_param ? $add_param : '') : '#!') ?>">
                            <i class="material-icons">chevron_left</i>
                        </a>
                    </li>

                    <!-- Borne inférieure -->
                    <li class='<?= ($page_actuelle == $bornes[0]['number'] ? 'active' : 'waves-effect') ?>'>
                        <a href="<?= HTTPS_URL."journal?perlikeview=0".($per_page ? "&perlikepage=$per_page" : '').($add_param ? $add_param : '')?>"><?= $bornes[0]['number'] ?></a>
                    </li>
                    
                    <!-- Possibles ... -->
                    <?php if($p_start){ ?>
                        <li class="disabled">
                            <span><i class="material-icons">more_horiz</i></span>
                        </li>
                    <?php } ?>
                    
                    <!-- Elements standards -->
                    <?php foreach($inside as $in) { ?>
                        <li class='<?= ($page_actuelle == $in['number'] ? 'active' : 'waves-effect') ?>'>
                            <a href="<?= HTTPS_URL."journal?perlikeview={$in['count']}".($per_page ? "&perlikepage=$per_page" : '').($add_param ? $add_param : '')?>"><?= $in['number'] ?></a>
                        </li>
                    <?php } ?>

                    <!-- Possibles ... -->
                    <?php if($p_end){ ?>
                        <li class="disabled">
                            <span><i class="material-icons">more_horiz</i></span>
                        </li>
                    <?php } ?>

                    <?php if($bornes[1]['number'] != $bornes[0]['number']){ ?>
                        <!-- Borne supérieure -->
                        <li class='<?= ($page_actuelle == $bornes[1]['number'] ? 'active' : 'waves-effect') ?>'>
                            <a href="<?= HTTPS_URL."journal?perlikeview={$bornes[1]['count']}".($per_page ? "&perlikepage=$per_page" : '').($add_param ? $add_param : '')?>"><?= $bornes[1]['number'] ?></a>
                        </li>
                    <?php } ?>
                    

                    <!-- Bouton suivant -->
                    <li class='<?= ($affichage_fleche_last ? 'waves-effect' : 'disabled') ?>'>
                        <a href="<?= ($affichage_fleche_last ? HTTPS_URL."journal?perlikeview=$next_view".($per_page ? "&perlikepage=$per_page" : '').($add_param ? $add_param : '') : '#!') ?>">
                            <i class="material-icons">chevron_right</i>
                        </a>
                    </li>
                    <?php 
                endif; // Fin du if ($bornes[0]['number'] > 0)

                if($add_param && $params){ ?>
                    <!-- Mois suivant -->
                    <li style='/*vertical-align: middle;*/' class="<?= ($nextMonth ? 'waves-effect tooltipped" data-position="bottom" data-delay="10" data-tooltip="'.
                                                                    getTextualMonth($nextMonth)." $nextYear" : 'disabled') ?>">
                    <a href="<?= ($nextMonth ? HTTPS_URL."journal?perlikeview=0&selyear=$nextYear&selmonth=$nextMonth" : '#!' ) ?>"><i class="material-icons">arrow_forward</i></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <?php
}

function generateMonthSelector($withJS, $isPerNote, $currentMonth, $currentYear, $isPerYear, $should_load_all) : void {
    ?>
    <!-- Formulaire de sélection de mois -->
    <div class='selectMonth row' style='margin-bottom: 0 !important;'>
        <div class='col s12 <?= ($withJS || $isPerNote ? "l10 offset-l1" : "l8 offset-l2") ?>'>
            <div class='card-user-new border-user' style='height: auto !important; margin-top: 8px; margin-bottom: 30px;'>
                <form id='journal_form' method='get' action='#!'>
                    <select id='selmonth' name='selmonth' class='col s5' <?= ($should_load_all || $isPerYear ? 'disabled' : '') ?>>
                        <?php
                            for($i = 1; $i <= 12; $i++){
                                echo "<option value='$i'".($i == $currentMonth ? ' selected' : '').">".getTextualMonth($i)."</option>";
                            }
                        ?>
                    </select>

                    <select id='selyear' name='selyear' class='col s5 offset-s2' <?= ($should_load_all ? 'disabled' : '') ?>>
                        <?php
                            $year = date('Y');
                            for($i = 2010; $i <= $year; $i++){
                                echo "<option value='$i' ".($i == $currentYear ? 'selected' : '').">$i</option>";
                            }
                        ?>
                    </select>

                    <div class='clearb divider' style='margin-top: 20px; margin-bottom: 20px;'></div>

                    <select id='selmode' name='selmode' class='col s2 l4'>
                        <option id='date_select' value='date' <?= (!$isPerNote ? 'selected' : '') ?>>Date</option>
                        <option id='rate_select' value='rating' <?= ($isPerNote == 1 ? 'selected' : '') ?>>Note</option>
                        <option id='add_select' value='add_date' <?= ($isPerNote > 1 ? 'selected' : '') ?>>Date d'ajout</option>
                    </select>

                    <select id='selorder' name='selorder' class='col s4 offset-s1'>
                        <option value='asc'>Croissante</option>
                        <option value='desc' selected>Décroissante</option>
                    </select>

                    <div class='col s4 l2 offset-l1 offset-s1' style='margin-top: -7px;'>
                        <div id="radio_all_block">
                            <input name="radio_year" value="all" type="radio" id="radio_all" <?= $should_load_all ? 'checked' : '' ?>>
                            <label for="radio_all">Tout</label>
                        </div>
                        <div>
                            <input name="radio_year" value="year" type="radio" id="radio_year" <?= $isPerYear ? 'checked' : '' ?>>
                            <label for="radio_year">Année</label>
                        </div>
                        <div>
                            <input name="radio_year" value="month" type="radio" id="radio_month" <?= !$isPerYear && !$should_load_all ? 'checked' : '' ?>>
                            <label for="radio_month">Mois</label>
                        </div>
                    </div>
                    <div class='clearb'></div>
                </form>
            </div>
        </div>
    </div>
    <?php
}
