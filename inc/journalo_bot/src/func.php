<?php

function findAskedMode(string $tweet) : string {
    if(preg_match('/^Sauvegarde/i', $tweet)) {
        return 'save';
    }
    if(preg_match('/^Poste/i', $tweet)) {
        return 'post';
    }

    return 'none';
}
