<?php

// BASE DE DONNEES
// ---------------

// connexion à la BD
function connectBD() {
	global $connexion;
	$connexion = mysqli_connect(SERVEUR, USERE, MDPASSE, BDD);
	if (mysqli_connect_errno()) {
		printf("Échec de la connexion : %s\n", mysqli_connect_error());
		exit();
	}
	mysqli_query($connexion, 'SET NAMES UTF8'); // requete pour avoir les noms en UTF8
}

function deconnectBD() {
	global $connexion;
	mysqli_close($connexion);
}

// Initialisation des constantes : La personne est aléatoirement bourrée
$prctage = mt_rand(15, 75);
// Chance qu'elle soit sobre, 8%
if(mt_rand(1, 100) <= 8){
    $prctage = 0;
}

$dels = (160 * $prctage) . '';
$ranletters = (160 * $prctage) . '';
$ranchar = ((92 * $prctage) + 750) . '';    
$dots = (34 * $prctage) . '';
$modver = (70 * $prctage) . '';
$ponct = (520 * $prctage) . '';
$rajchars = (158 * $prctage) . '';
$insultes = (118 * $prctage) . '';
$squarep = mt_rand(1800, 4800) . '';
$addingl = mt_rand(2100, 4800) . '';

define('PERCENTAGE_DRUNK', $prctage);
define('IS_DRUNK', ($prctage != 0));
define('RAND_LIMIT_S', $dels);
define('RAND_LIMIT_LETTER', $ranletters);
define('RAND_LIMIT_RANCHAR', $ranchar);
define('RAND_LIMIT_DOTS', $dots);
define('RAND_LIMIT_MODVERBES', $modver);
define('RAND_LIMIT_PONCTUATION', $ponct);
define('RAND_LIMIT_ADDCHARS', $rajchars);
define('RAND_LIMIT_INSULTE', $insultes);
define('RAND_MAKE_SQUAREP', $squarep);
define('RAND_MULTIPLE_TWEETS', 7500);
define('RAND_MULTIPLE_MORE', 900);
define('RAND_LIMIT_ADDING', $addingl);
define('BOT_USER_DIR', "/home/pi/bot/");
define('BOT_SCREEN_NAME', "chuipasbourre");


$replacement_letters_array = [
    'a' => ['a', 'z', 'q', 's'],
    'z' => ['z', 'b', 'j', 'h'],
    'e' => ['e', 'r', 's', 'd'],
    'r' => ['r', 'e', 'd', 'g'],
    't' => ['t', 'g', 'y', 'f'],
    'y' => ['y', 'u', 'j', 'g'],
    'u' => ['u', 'i', 'o', 'k'],
    'i' => ['i', 'u', 'o', 'l'],
    's' => ['s', 'd', 'q', 'x'],
    'h' => ['h', 'g', 'b', 'j'],
    'k' => ['k', 'l', 'i', '\''],
    'b' => ['b', 'n', 'g', 'h'],
    'n' => ['n', 'b', 'j', 'h'],
];

$authorized = ['alkihis', 'mayakolyyn', 'wydrops', 'erykyucc', 'zabreix', 'saw_dah', 'halquihisse', 'iarwainPi', 'angerydrop'];

// -------------------------------------------------------
// -- Fonctions de combinaisation / traitement du tweet --
// -------------------------------------------------------

function buildCombinedTweet(string $first_tweet, string $second_tweet, array &$pos, int $offset = 0) : string {
    // Explosion des tweets par les espaces (on récupère des words)
    $f_array = explode(' ', $first_tweet);
    $s_array = explode(' ', $second_tweet);

    // Repérage du word clé pour premier tweet
    $pos1 = findKeyword($f_array, $offset);
    // Repérage du word clé pour second
    $pos2 = secondTweetCombinaison($s_array);

    // Assemblage en chaîne
    return combineTweet($f_array, $s_array, $pos1, $pos2, $pos);
}

function combineTweet(array &$first_tweet, array &$second_tweet, int $pos1, int $pos2, array &$final_pos) : string { 
    // Réunit dans une chaîne unique first jusqu'à l'indice pos1 (pos1 est inclus) puis second à partir de pos2 (pos2 est inclus)
    $new_tweet = [];
    $GLOBALS['log']->write("Position limite 1er tweet : $pos1, position de début 2ème tweet : $pos2");
    $final_pos = [$pos1, $pos2];
    
    $i = 0;
    for(; $i <= $pos1; $i++){
        $new_tweet[$i] = $first_tweet[$i];
    }
    
    $taille = count($second_tweet);
    
    for($j = 0; ($pos2+$j) < $taille; $j++){
        $new_tweet[$i+$j] = $second_tweet[$pos2+$j];
    }

    // Suppression des liens multiples dans le tweet
    removeMultiplesLinks($new_tweet);
    
    return implode(' ', $new_tweet);
}

function findKeyword(array &$tabTweet, int $offset = 0) : int { // Renvoie la position d'un word clé si trouvé (ceci peut être une virgule en fin de word !) ou une position quelconque
    $key_words = [];
    foreach($tabTweet as $key => $word){
        if ($key < $offset) {
            continue;
        }

        if(preg_match("/(.*)(\.|,|!|\?|:)$/", $word)){ // Si il trouve une virgule ou un point en bout de word
            $key_words[] = $key;
        }
        if(preg_match("/(^et$)|(^de$)|(^des$)|(^du$)|(^car$)|(^donc$)|(^alors$)|(^mais$)|(^plus$)/i", $word)){ // Si il trouve un word clé
            $key_words[] = $key;
        }
    }

    if(count($key_words) == 0 || mt_rand(1, 8192) < 2500){ // Si aucun word clé / chance quelconque : On retourne une position au hasard
        $begin = intval(count($tabTweet)/3);
        if ($begin < $offset && count($tabTweet) > $offset) {
            $begin = $offset;
        }
        return mt_rand($begin, (count($tabTweet)-1));
    }
    $ranword = mt_rand(0, (count($key_words)-1));
    return $key_words[$ranword];
}

function removeMultiplesLinks(array &$tweet) : void { // Supprime les liens présents dans le tweet, excepté la première occurence.
    $occ = 0;
    foreach($tweet as $key => $word){
        if(preg_match('/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/', $word)){
            $occ++;
            if($occ > 1){
                unset($tweet[$key]);
            }
        }
    }
}

function secondTweetCombinaison(array $second_tweet) : ?int { // Renvoie l'emplacement ou il faut combiner dans le deuxième tableau de tweet
    foreach($second_tweet as $key => $word){
        if(preg_match("/(.*)(\.|,|!|\?|:)$/", $word)){ // Si il trouve une virgule ou un point en bout de word
            if(array_key_exists($key+1, $second_tweet)){
                return $key+1;
            }
        }
        if(preg_match("/(^et$)|(^de$)|(^des$)|(^du$)|(^car$)|(^donc$)|(^alors$)|(^mais$)|(^plus$)/i", $word)){ // Si il trouve un word clé
            if(array_key_exists($key+1, $second_tweet)){ // Si il y a quelque chose après (sinon aucun délimiteur n'existe)
                return $key+1;
            }
        }
    }

    $se_count = count($second_tweet);
    if($se_count < 4 && $se_count > 0){
        return mt_rand(0, intval(($se_count - 1) / 2));
    }
    elseif($se_count == 0) {
        return null;
    }
    else{
        return mt_rand(0, $se_count-1);
    }
}

function deleteMentions(string $tweet) : ?string { 
    // Supprime les @ trouvés en début du tweet précisé. Si le tweet devient vide, retourne null.
    $new = $tweet;
    if(preg_match('/^(@\w+ ){1,}/', $tweet)){
        $GLOBALS['log']->write("Tweet détecté comme mention : \n$tweet");
        $new = preg_replace('/^(@\w+ ){1,}(.)/', '$2', $tweet);
        if($new === ""){ // Si le tweet devient vide si on lui enlève les @ qu'il contient au début
            return null;
        }
    }
    return $new;
}

// ---------------------------------
// -- Fonctions de bourrification --
// ---------------------------------

function tweetBourrification(string $tweet) : string {
    $square_pants = mt_rand(0, 32768) < RAND_MAKE_SQUAREP;

    $tweet = preg_replace_callback('/(\b)?([\w_@:\/\.,\'\?&=-]+)(\b)?/u', function ($m) use ($square_pants) { 
        // Capture words, liens (les ?, . et ! en bout de word (non collés) ne sont pas capturés), et @username
        return $m[1] . wordCompute($m[2], $square_pants) . ($m[3] ?? '');
    }, $tweet);

    $tweet = preg_replace_callback("/(^|\n|\h)(!|\?)($|\n|\h)/", function ($m) {
        // Multiplication des ! ou ? seuls
        return ($m[1] ?? '') . punctuation($m[2]) . ($m[3] ?? '');
    }, $tweet);

    $tweet = preg_replace_callback("/(?<!\.)(\.{1})([\n\h]|$)/", function ($m) {
        // Recherche les . seuls
        return addDots($m[1]) . $m[2];
    }, $tweet);
    
    return $tweet;
}

function punctuation(string $dots) : string {
    // Multiplication des ! ou ? seuls
    if(mt_rand(1, 32768) >= RAND_LIMIT_PONCTUATION) return $dots;
    
    if($dots === '!'){
        $nb = mt_rand(1, 5);
        while($nb > 1){
            $dots .= '!';
            $nb--;
        }
    }

    elseif($dots === '?'){
        $nb = mt_rand(1, 5);
        while($nb > 1){
            $dots .= '?';
            $nb--;
        }
    }
    return $dots;
}

function addDots(string $word) : string {
    if(mt_rand(1, 32768) < RAND_LIMIT_DOTS){
        $nb = mt_rand(1, 5);
        $ch = '.';
        while($nb > 1){
            $ch .= '.';
            $nb--;
        }
        return $ch;
    }
    return $word;
}

function wordCompute(string $word, bool $sqp = false) : string {
    // Vérification si le word est un lien ou un arobase : aucun traitement à effectuer dans ce cas
    if(isALinkOrAtSign($word)){
        return $word;
    }

    // Recherche et suppression des s en bout de word
    if(preg_match("/^\w+(s)$/ui", $word)){
        $word = deleteS($word);
    }
    
    // Dé-conjuguer certains verbes du premier grp (>fais des erreurs en trouvant des verbes pour des words qui n'est sont pas)
    //, pr le moment : indicatif présent, imparfait, passé composé simple (non quantifié ni genré)
    $word = firstGroupVerb($word);
    
    // Ajoute possiblement des insultes avant un point ou une virgule précédée d'un word.
    $word = addInsult($word);
    
    // Gestion du fait que certains caractères soient aléatoirement déplacés dans la phrase à 2-3 caractères près
    $word = randomizeChars($word);
    
    // Rajouter aléatoirement des caractères dans les words ou à la fin des caractères proches sur le clavier du carac précédent
    $word = addRandomChars($word);
    
    // Remplacement aléatoire de certains caractères. Pour le moment e, i, r, a et n.
    $word = replaceLetters($word);

    // Ecriture façon mocking spongebob si l'aléatoire l'a décidé
    if($sqp){
        $word = squarePants($word);
    }
    
    return $word;
}

function deleteS(string $word) : string {
    // La fonction se contente de supprimer les s en bout de word (avec une chance de RAND_LIMIT_S/32768) de le faire
    if(mt_rand(1, 32768) < RAND_LIMIT_S){
        $word = preg_replace("/s$/i", '', $word);
    }
    return $word;
}

function isInFirstGroup(string $word, string &$verbe) : bool { // Le verbe passé doit être une chaîne vide, et restera vide si la fonction renvoie faux. Le mot doit être en majuscule !
    $length = strlen($word);
    if($length > 3){
        // Test des conjuguaisons
        // Nombreux passés
        if(preg_match("/É$/i", $word)){
            $verbe = preg_replace("/É$/i", '', $word);
            return true;
        }
        
        // 1ere et 3eme pers indicatif
        if(preg_match("/E$/i", $word)){
            $verbe = preg_replace("/E$/i", '', $word);
            return true;
        }
        // 2eme personne indicatif
        elseif(preg_match("/(ES)$/i", $word)){
            $verbe = preg_replace("/(ES)$/i", '', $word);
            return true;
        }
        // 2eme pers pluriel imparfait
        elseif(preg_match("/(IEZ)$/i", $word)){
            $verbe = substr($word, 0, $length-3);
            return true;
        }
        if($length > 3){
            // 1ere perso pluriel imparfait
            if(preg_match("/(IONS)$/i", $word)){
                $verbe = substr($word, 0, $length-4);
                return true;
            }
            if($length > 4){
                // 3eme personne pluriel imparfait
                if(preg_match("/(AIENT)$/i", $word)){
                    $verbe = substr($word, 0, $length-5);
                    return true;
                }
            }
        }
        // 1ere et 2eme perso imparfait
        if(preg_match("/(AIS)$/i", $word)){
            $verbe = substr($word, 0, $length-3);
            return true;
        }
        // 3eme perso imparfait
        elseif(preg_match("/(AIT)$/i", $word)){
            $verbe = substr($word, 0, $length-3);
            return true;
        }
        
        // 1eme personne plu ind
        elseif(preg_match("/(ONS)$/i", $word)){
            $verbe = substr($word, 0, $length-3);
            return true;
        }
        // 2eme personne plu indicatif
        elseif(preg_match("/(EZ)$/i", $word)){
            $verbe = substr($word, 0, $length-2);
            return true;
        }
        // 3eme personne plu ind
        elseif(preg_match("/(ENT)$/i", $word)){
            $verbe = substr($word, 0, $length-3);
            return true;
        }
    }
    
    return false;
}

function firstGroupVerb(string $word) : string {
    // Aléatoire : définit si il va chercher ou non à savoir si c'est un verbe et le remplacer
    if(mt_rand(1, 32768) >= RAND_LIMIT_MODVERBES)
        return $word;
    
    // 1- Vérifier si le verbe est tout en majuscule ou tout en minuscule (ne rien faire sinon)
    $verb = '';
    $is_up = ctype_upper($word);
    
    // 2- Mémoriser si il est tout en maj, et passer en maj avant d'appeler isInFirstGroup
    // 3- Si c'est un verbe et que la chaîne n'est pas vide, rechercher le verbe en WHERE = $verbe."ER" (sql)
    if(isInFirstGroup($word,  $verb) && $verb != ''){
        $verb .= ($is_up ? "ER" : 'er'); // les verbes ont leur terminaisons ER dans la BDD
        global $connexion;
        $verb = mysqli_real_escape_string($connexion, $verb);
        $requete = 'SELECT verbe FROM firstg WHERE verbe = \''.$verb.'\';'; // Les verbes sont en maj dans la BDD : D'où la conversion en maj de partout...
        // envoi de la requete au SGBD
        $resultat = mysqli_query($connexion, $requete);

        if(mysqli_num_rows($resultat)) {
            $verb = mysqli_fetch_row($resultat);
            $word = $verb[0];
            // 5- Insérer le nouveau verbe à la place du word
        }
    }
    return $word;
}

function addInsult(string $word) : string {
    $insult_array = ["putain", "fdp", "sa mère", "connard", "wala", "ntm", "wesh", "hein", "hihi", "haha"];

    // Traitement des virgules et points en bout de mot
    if(preg_match("/(.+)(,|\.)(.*)/u", $word)){
        $word = preg_replace_callback("/(.+)(,|\.)(.*)/u",
            function ($mat) use ($insult_array){
                if(mt_rand(1, 32768) < RAND_LIMIT_INSULTE){
                    $pos_insult = mt_rand(0, count($insult_array)-1);
                    return $mat[1] . " " . (ctype_upper($mat[1]) ? mb_strtoupper($insult_array[$pos_insult]) : $insult_array[$pos_insult]) . $mat[2] . $mat[3];
                }
                return $mat[0];
            }
        , $word);
    }
    return $word;
}

function randomizeChars(string $word) : string { // La fonction se charge d'inverser la place de deux ou plusieurs caractères dans le mot. Le nombre de chances est égal à (partie entière)taille_du_mot/4.
    $taille = strlen($word);
    
    // Nombre de caractères à inverser : dépendant de la taille du mot ; On considère deux chances si le mot est supérieur ou égal à 8 caractères, trois si >= 12...
    $chances = $taille / 4;
    $chances = intval($chances);
    if($chances < 1) $chances = 1;
    
    while($chances > 0){
        if(mt_rand(1, 32768) < RAND_LIMIT_RANCHAR && $taille > 1){
            $letter1 = mt_rand(0, $taille-1);
            if(preg_match("/[a-z]/i", $word[$letter1])){
                
                // Ce bloc de 4 lignes permet de savoir quelles sont les bornes d'échanges de lettre. Par défaut, lettre-2 : lettre+2. Cependant, si on touche le min ou le max du mot, on les fixe.
                if($letter1-2 < 0) $min = 0;
                else $min = $letter1-2;

                if($letter1+2 > $taille-1) $max = $taille-1;
                else $max = $letter1+2;
                
                $letter2 = $letter1;
                while($letter2 == $letter1){
                    $letter2 = mt_rand($min, $max);
                } // On tire jusqu'à ce que les deux lettres tirées soient différentes. C'est forcément possible car on a vérifié au début que $taille > 1.

                if(preg_match("/[a-z]/i", $word[$letter2])){ // Si la lettre tirée est bien une lettre (non unicode, certes), on continue
                    $tmp2 = $word[$letter2];
                    $tmp1 = $word[$letter1];
                    $word[$letter2] = (ctype_upper($word[$letter2]) ? strtoupper($word[$letter1]) : strtolower($word[$letter1]));
                    $word[$letter1] = (ctype_upper($tmp1) ? strtoupper($tmp2) : strtolower($tmp2));
                }
            }
        }
        $chances--;
    }
    return $word;
}

function addRandomChars(string $word) : string { // Rajoute devant, dans ou après un mot, des caractères en fonction d'une lettre dans le mot choisi aléatoirement
    $word_length = strlen($word);
    
    if($word_length > 2 && mt_rand(1, 32768) < RAND_LIMIT_ADDCHARS){
        $mixedres = str_split($word, subWordLength($word_length));
        $splitted_word_length = count($mixedres);
        if($splitted_word_length == 1){ // Le tableau ne contient qu'un seul élément : le mot n'a pas été scindé
            $letter = mt_rand(0, $word_length-1);
            $back_front = mt_rand(0, 1); // Tire si la(s) nouvelle(s) lettre(s) est/sont placé(s) à gauche ou à droite de la lettre tirée
            $generated = generateChars($word[$letter]);

            if($back_front == 0){
                $word = insertString($word, $generated, $letter);
            }
            else{
                $word = insertString($word, $generated, $letter+1);
            }
        }
        else{ // Le tableau contient plusieurs parties (classes) : Choix de la classe
            // Tirage de la classe
            $class = mt_rand(0, $splitted_word_length - 1);
            if($class < $splitted_word_length / 2){ // Nouveau tirage pour biaiser le choix de la classe vers les classes les plus élevées
                $class = mt_rand(0, $splitted_word_length - 1);
            }
            // Classe choisie : Choix du caractère à l'intérieur de la classe
            $tailleclasse = strlen($mixedres[$class]);
            $letter = mt_rand(0, $tailleclasse - 1);
            $back_front = mt_rand(0, 1);
            $generated = generateChars($mixedres[$class][$letter]);
            
            if($back_front == 0){
                $mixedres[$class] = insertString($mixedres[$class], $generated, $letter);
            }
            else{
                $mixedres[$class] = insertString($mixedres[$class], $generated, $letter + 1);
            }
            $word = implode('', $mixedres);
        }
    }
    elseif($word != ''){ // Si le word est petit et non vide
        $rand = mt_rand(1, 32768);
        if($rand < RAND_LIMIT_ADDCHARS){
            if($rand < RAND_LIMIT_ADDCHARS/2){ // Rajout d'un ou plusieurs caractères aléatoires au début
                $word = generateChars($word[0]) . $word;
            }
            else{ // Rajout d'un ou plusieurs caractères aléatoires à la fin
                $word .= generateChars($word[$word_length-1]);
            }
        }
    }
    
    return $word;
}

function generateChars(string $letter) : string { // Génère un chaîne de 1, 2, 3 ou 4 caractères (différents ou non) ou vide si la génération a échouée dépendant de la lettre passée en paramètre
    // Mémorisation si la lettre est en majuscule
    $is_up = false;
    if(ctype_upper($letter)) {
        $letter = strtolower($letter);
        $is_up = true;
    }

    global $replacement_letters_array;
    
    if(!array_key_exists($letter, $replacement_letters_array)){
        return '';
    }
    // Nombre de remplacements possibles pour la lettre en cours
    $possible_replacements = count($replacement_letters_array[$letter]);

    $numlettre = mt_rand(0, $possible_replacements - 1);
    // Sélection aléatoire de la lettre à ajouter depuis le tableau
    $res = $replacement_letters_array[$letter][$numlettre];
    
    $i = 0;
    // Chances de rajout supplémentaire : Limitaion à trois caractères supplémentaires (i < 3) soit 4 caractères maximum pour la chaîne retournée
    while(mt_rand(1, 32768) < RAND_LIMIT_ADDCHARS && $i < 3){
        $numlettre = mt_rand(0, $possible_replacements - 1);
        $res .= $replacement_letters_array[$letter][$numlettre];
        $i++;
    }
    
    if($is_up){
        $res = strtoupper($res);
    }
    return $res;
}

function subWordLength(int $taille) : int { // Définit la taille des sous-mots où l'on insère des caractères
    $div = intval($taille/4);
    if($div < 2){
        return (int)($taille + 1);
    }
    return (int)$div;
}

function insertString(string $parent, string $child, int $pos) : string { // Insère la chaîne child dans la chaîne parent à la position pos
    if($pos == strlen($parent)){
        return $parent . $child;
    }
    return substr($parent, 0, $pos) . $child . substr($parent, $pos);
}

function replaceLetters(string $word) : string { // Demande un mot non ponctué (rien ne se passera sinon) : Ceci n'est PAS la fonction qui déplace des lettres !
    // Remplacement aléatoire de certains caractères : i par u, p par o... Chaque caractère a 4 possibilités de remplacement (dont lui-même)
    $tabLettre = [
        'e' => ['e', 'r', 's', 'd'],
        'i' => ['i', 'u', 'o', 'l'],
        'r' => ['r', 'e', 'd', 'g'],
        'a' => ['a', 'z', 'q', 's'],
        'n' => ['n', 'b', 'j', 'h'],
    ];

    foreach($tabLettre as $let => $replacements){
        $word = preg_replace_callback("/$let/i",
            function ($mat) use ($replacements){ // Fonction anonyme appelée pour remplacer
                if(mt_rand(1, 32768) < RAND_LIMIT_LETTER){
                    $letter = mt_rand(0, count($replacements) - 1);

                    if(ctype_lower($mat[0])){ // mat[0] correspond à l'entière chaîne capturée. L'expression rationnelle ne précisant qu'un caractère, mat[0] n'est qu'un simple caractère égal à $let.
                        return $replacements[$letter];
                    }
                    else{
                        return strtoupper($replacements[$letter]);
                    }
                }
                return $mat[0];
            }
        , $word);
    }

    return $word;
}

function squarePants(string $word) : string { // Rend un mot dE cEtTe fAçOn
    $up = (bool)mt_rand(0, 1);
    $word = preg_replace_callback('/([a-z])/i', function ($mat) use (&$up) {
        if($up){ // majuscule
            $up = !(mt_rand(0,9) <= 8);
            return mb_strtoupper($mat[1]);
        }
        else{
            $up = mt_rand(0,9) <= 7;
            return mb_strtolower($mat[1]);
        }
    }, $word);
    return $word;
}

function isALinkOrAtSign(string $word) : bool {
    // Si le mot passé est un @username ou un lien, la fonction renvoie vrai.
    if(preg_match('/^(@\w)+/', $word)){
        return true;
    }
    if(preg_match('/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/', $word)){
        return true;
    }
    return false;
}

function interestingWordIn(string $tweet) : ?string {
    $tPasI = ['de', 'un', 'une', 'des', 'ça', 'ce', 'le', 'la', 'les', 'du', 'se', 'ses', 'ces', '',
            'je', 'suis', 'est', 'es', 'ai', 'ait', 'tu', 'il', 'elle',
            'iel', 'ils', 'elles', 'ont', 'sont', 'et'];

    $mots = explode(' ', $tweet);

    $nb = count($mots);
    $alea = 0;
    $try = 0;

    do{
        $alea = mt_rand(0, $nb-1);
        $try++;
    } while (preg_grep("/{$mots[$alea]}/i", $tPasI) && $try < 20);
    if($try >= 20){
        return NULL;
    }
    else {
        return $mots[$alea];
    }
}

function isTweetToB(string $tweet) : bool {
    return (bool)preg_match('/^B:(.+)$/', $tweet);
}

function isTweetToSourced(string $tweet) : bool {
    return (bool)preg_match('/^source ?\?*$/i', $tweet);
}

function isTweetToDab(string $tweet) : bool {
    return (bool)preg_match('/^\*.*dab.*\* ?\?*$/i', $tweet);
}

function isTweetToAcab(string $tweet) : bool {
    return (bool)preg_match('/^acab$/i', $tweet);
}

function isTweetToDelete(string $tw, string $username, ?string $id_to_delete = null, ?string $id_user = null) : bool {
    global $authorized;
    if(preg_match('/^supprime\.*$/i', trim($tw))){
        if(preg_grep("/^$username$/i", $authorized)) {
            return true;
        }

        if($id_to_delete && $id_user) {
            if(inUsedTweets($id_user, $id_to_delete)) {
                return true;
            }
        }
    }
    return false;
}

function addWordOnBeginning(string $tweet) : string {
    $adding = '';
    // Ajoute possiblement un mot au début du tweet
    if(mt_rand(0, 32768) < RAND_LIMIT_ADDING){
        $adding = interestingWordIn($tweet); // Recherche un mot à mettre
        $punc = '';
        if(mt_rand(0, 1) == 1){
            $punc = '!';
        }
        else {
            $punc = '?';
        }
        $adding .= ' ' . punctuation($punc); // Ajoute une ponctuation avec le mot intéressant
    }

    return $adding;
}

function tweetToFetchCount(int $tweets_count) : int {
    $to_fetch = 1;
    if($tweets_count > 1 && mt_rand(0, 8192) < RAND_MULTIPLE_TWEETS) { // Plusieurs tweets à fetch
        $to_fetch = 2;

        while(mt_rand(0, 8192) < RAND_MULTIPLE_MORE && $to_fetch < 5 && $tweets_count > $to_fetch) {
            $to_fetch++;
        }
    }
    
    return $to_fetch;
}

// Sauvegarde de tweet
function logTweets(array $tweets) {
    $i = 1;
    foreach($tweets as $t) {
        $GLOBALS['log']->write("Tweet $i : {$t->full_text}\nID : {$t->id_str} (envoyé par @{$t->user->screen_name} / UserID {$t->user->id_str})");
        $i++;
    }
}

function saveTweetsDetails($final_tweet, array $tweet_array, ?array $details = null) { // Final tweet is tweet object
    if(!$final_tweet) {
        return;
    }

    $str = [];
    $str['save_type'] = "drunked";
    $str['used_tweets'] = [];
    $u = &$str['used_tweets'];
    $i = 0;

    foreach($tweet_array as $t) {
        $u[$i] = $t;
        $i++;
    }
    $str['count'] = count($tweet_array);
    $str['tweet'] = $final_tweet;

    if($details) {
        $str['misc'] = [];
        $mi = &$str['misc'];

        $mi['drunk_percentage'] = $details['drunk_percentage'];
        $mi['fusion_positions'] = $details['fusion'];
    }

    $id_str_tweet = $final_tweet->id_str;

    file_put_contents(BOT_USER_DIR . "$id_str_tweet.json", json_encode($str));
}

function inUsedTweets(string $id_str_user, string $id_str) : bool {
    if(file_exists(BOT_USER_DIR . "$id_str.json")) {
        $tweets = json_decode(file_get_contents(BOT_USER_DIR . "$id_str.json"));
        foreach($tweets->used_tweets as $t) {
            if($t->user->id_str === $id_str_user) {
                return true;
            }
        }
    }
    
    return false;
}

function getSourcesFromTweet(?string $id_str, ?string $user_screen_name = null) : ?string {
    if(!$id_str) {
        return null;
    }

    if(file_exists(BOT_USER_DIR . "$id_str.json")) {
        $tweets = json_decode(file_get_contents(BOT_USER_DIR . "$id_str.json"));

        $str = "Il y a $tweets->count tweet" . ($tweets->count > 1 ? 's' : '') . " utilisé" . ($tweets->count > 1 ? 's' : '') . ".\n";

        if($tweets->count > 1) {
            $i = 1;
            foreach($tweets->used_tweets as $t) {
                if(isset($t->original_retweet_information)) { // le tweet choisi était un RT
                    $screen_name_rter = ($t->original_retweet_information->user->screen_name === $user_screen_name ? 
                                     'vous' : '@.' . $t->original_retweet_information->user->screen_name);
                    $str .= "[$i] RT @.{$t->user->screen_name} par $screen_name_rter :\n";
                }
                else {
                    $str .= "[$i] Tweet @.{$t->user->screen_name} :\n";
                }
                
                $str .= "https://twitter.com/" . $t->user->screen_name . "/status/" . $t->id_str . "\n";
                $i++;
            }
        }
        else {
            if(isset($tweets->used_tweets[0]->original_retweet_information)) { // le tweet choisi était un RT
                $screen_name_rter = ($tweets->used_tweets[0]->original_retweet_information->user->screen_name === $user_screen_name ? 
                                     'vous' : '@.' . $tweets->used_tweets[0]->original_retweet_information->user->screen_name);
                $str .= "RT @.{$tweets->used_tweets[0]->user->screen_name} effectué par $screen_name_rter :\n";
            }
            else {
                $str .= "Tweet @.{$tweets->used_tweets[0]->user->screen_name} :\n";
            }
            
            $str .= "https://twitter.com/" . $tweets->used_tweets[0]->user->screen_name . "/status/" . $tweets->used_tweets[0]->id_str . "\n";
        }

        $str .= "Log: https://www.alkihis.fr/get_sourced_log.php?tweet_id=$id_str\n";

        return $str;
    }

    return null;
}

function selectRandomDab() : ?string {
    $files = glob(BOT_USER_DIR . "dab/*.{jpg,jpeg,png}", GLOB_BRACE);

    if($files) {
        $rand = rand(0, count($files) - 1);

        return $files[$rand];
    }
    else {
        return null;
    }
}

function autoCleanSources() {
    $files = [];

    $files = glob(BOT_USER_DIR . '*.json');
    usort($files, function($a, $b) {
        return filemtime($a) > filemtime($b);
    });

    $max_time = time() - (3600 * 144);
    // Fichiers à ne pas supprimer : Fichier qui ont un timestamp compris entre now() et now() - 144 (une semaine)
    foreach($files as $f){
        if(filemtime($f) < $max_time){
            unlink($f);
        }
        else {
            // break;
        }
    }
}

/* Gestion des erreurs */
function error($message, $level = E_USER_NOTICE) { 
    $bck_t = debug_backtrace();
    $caller = next($bck_t); 
    trigger_error($message . ' in ' . $caller['function'] . '() called from ' . $caller['file'] . 
    ' on line ' . $caller['line'] . ", error handler", $level); 
} 
