<?php

class Tweet {
    public function __construct($tweet) {
        foreach($tweet as $key => $t) {
            $this->$key = $t;
        }
    }

    public function isARetweet() : bool {
        return isset($this->original_retweet_information);
    }

    // Retourne le tweet initialement formaté par twitter
    public function getOriginalRetweet() : ?stdClass {
        if($this->isARetweet()) {
            $orig = $this->original_retweet_information;

            $actual = clone $this;
            unset($actual->original_retweet_information);

            $orig->retweeted_status = $actual;

            return $orig;
        }

        return null;
    }

    public function getScreenName() : string {
        if(isset($this->user->screen_name)) {
            return $this->user->screen_name;
        }
        return "";
    }

    public function getName() : string {
        if(isset($this->user->name)) {
            return $this->user->name;
        }
        return "";
    }

    public function getText() : string {
        if(isset($this->full_text)) {
            return $this->full_text;
        }

        if(isset($this->text)) {
            return $this->text;
        }

        return "";
    }

    public function getTrimmedText() : string {
        if(isset($this->full_text) && isset($this->display_text_range)) {
            return substr($this->full_text, $this->display_text_range[0]);
        }

        return "";
    }

    public function getID() : string {
        if(isset($this->id_str)) {
            return $this->id_str;
        }
        
        return "";
    }

    public function getMentionnedPeople(bool $ids = false) : array {
        $return = [];

        if(isset($this->entities->user_mentions)) {
            foreach($this->entities->user_mentions as $user) {
                if($ids) {
                    $return[] = $user->id_str;
                }
                else {
                    $return[] = $user->screen_name;
                }
            }
        }

        return $return;
    }
}
