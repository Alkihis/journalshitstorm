<?php

// Classe Twitter : Interactions, post, get list...
class Twitter {
    protected $url_update = 'https://api.twitter.com/1.1/statuses/update.json';
    protected $url_verify = 'https://api.twitter.com/1.1/account/verify_credentials.json';
    protected $url_lists = 'https://api.twitter.com/1.1/lists/list.json';
    protected $url_lists_statuses = 'https://api.twitter.com/1.1/lists/statuses.json';
    protected $url_delete = 'https://api.twitter.com/1.1/statuses/destroy/';
    protected $url_user_timeline = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
    protected $url_mentions_timeline = 'https://api.twitter.com/1.1/statuses/mentions_timeline.json';
    protected $url_media_unique = 'https://upload.twitter.com/1.1/media/upload.json';

    protected $since_id_file = '/var/www/html/inc/journalo_bot/last_id';
    protected $since_id_mention = '/var/www/html/inc/journalo_bot/last_id';

    protected $screen_name = '';
    protected $id_str = '';
    protected $name = '';
    
    protected $oauth;

    /**
     * Constructor of Twitter class
     *
     * @param string $key
     * Consumer key of your OAuth app
     * 
     * @param string $secret
     * Consumer secret of your OAuth app
     */
    public function __construct(string $key, string $secret) { 
        $this->oauth = new OAuth($key, $secret, OAUTH_SIG_METHOD_HMACSHA1, OAUTH_AUTH_TYPE_URI);
        // $this->oauth->disableSSLChecks(); 
    } 

    /**
     * Set the access token of the OAuth module
     * 
     * @param string $token
     * Access Token
     * 
     * @param string $secret
     * Access Secret Token
     * @return void
     */
    public function setToken(string $token, string $secret) : void {
        $this->oauth->setToken($token, $secret); 
    } 

    /**
     * Send a media (img, gif, video) to Twitter using the chunked upload method
     * This function modify script time limit to prevent unexpected script ending.
     *
     * @param string $path
     * Path of the media being uploaded. Existence of file is checked.
     * You CAN'T use an URL because basename(), mime_content_type() and file_exists() are used.
     * 
     * @return string|null
     * Return null if invalid / inexistant file, error while sending file, if Twitter is unable to treat file
     * Return media_id_string if media have been uploaded successfully 
     * Use media_id_string while sending tweet, parameter 'media_ids'
     */
    public function sendChunkedMedia(string $path) : ?string {
        if(file_exists($path)) {
            $len_chunk = 1024 * 512; // 512 ko
            // La taille demandée est la taille normale, pas la taille encodée en base64
            $total_size = filesize($path);

            // Initialisation de l'envoi
            $array_args = ['command' => 'INIT', 'total_bytes' => $total_size, 'media_type' => mime_content_type($path)];

            // Initialise le media category si c'est une vidéo pour permettre l'envoi asynchrone
            if(explode('/', $array_args['media_type'])[0] === 'video') {
                $array_args['media_category'] = 'tweet_video';
            }
            
            $GLOBALS['log']->write('Début de l\'envoi du fichier ' . basename($path));
            // Définition d'une haute time limit
            $max_time = ($total_size / 1024) * 10; // 10 secondes par Mo
            set_time_limit($max_time);

            try {
                $this->oauth->fetch($this->url_media_unique, $array_args, OAUTH_HTTP_METHOD_POST, 
                                    ['Content-Type' => 'application/x-www-form-urlencoded']);
            } catch(OAuthException $ex) {
                $GLOBALS['log']->write('Impossible d\'uploader un média : ' . $ex->lastResponse);
                return null;
            }

            $json = json_decode($this->oauth->getLastResponse());
            $id_media = $json->media_id_string;
            $i = 0;
            // Il y a taille_totale/longueur_chunk parties au total, +1 si jamais une partie fait moins de longueur_chunk (ce qui arrivera quasi tout le temps)
            $nb_part = ceil($total_size / $len_chunk);

            $handle = fopen($path, 'rb');

            // Tant que le buffer contient quelque chose
            while (!feof($handle)) {
                $buffer = base64_encode(fread($handle, $len_chunk));

                if(!empty($buffer)) { // Les parties peuvent être vides
                    $GLOBALS['log']->write("Envoi... (" . ($i + 1) . "/$nb_part)");

                    $array_args = ['command' => 'APPEND', 'media_id' => $id_media, 'segment_index' => $i, 'media_data' => $buffer];
                    try {
                        $this->oauth->fetch($this->url_media_unique, $array_args, OAUTH_HTTP_METHOD_POST, 
                                            ['Content-Type' => 'application/x-www-form-urlencoded']);
                    } catch (OAuthException $ex) {
                        $GLOBALS['log']->write('Impossible d\'uploader (ajouts de chunk) un média : ' . $ex->lastResponse);
                        return null;
                    }
                    $i++;
                }
            }

            // Termine l'envoi
            $array_args = ['command' => 'FINALIZE', 'media_id' => $id_media];
            try {
                $this->oauth->fetch($this->url_media_unique, $array_args, OAUTH_HTTP_METHOD_POST, 
                                    ['Content-Type' => 'application/x-www-form-urlencoded']);
            } catch (OAuthException $ex) {
                $GLOBALS['log']->write('Impossible d\'uploader un média (finalisation) : ' . $ex->lastResponse);
                return null;
            }

            $json = json_decode($this->oauth->getLastResponse());

            // Remise à zéro de la time limit par une limite normale
            set_time_limit(30);

            if(isset($json->processing_info)) { // Le media est toujours en cours de traitement
                $GLOBALS['log']->write("Média $id_media en cours de traitement.");
                $complete = false;
                $array_args = ['command' => 'STATUS', 'media_id' => $id_media];
                do {
                    if(!isset($json->processing_info->check_after_secs)) {
                        return null;
                    }
                    sleep($json->processing_info->check_after_secs);
                    // On demande au GET STATUS où ça en est

                    try {
                        $this->oauth->fetch($this->url_media_unique, $array_args);
                    } catch (OAuthException $ex) {
                        return null;
                    }
    
                    $json = json_decode($this->oauth->getLastResponse());
                    if(!isset($json->processing_info->state)) {
                        return null;
                    }

                    $GLOBALS['log']->write("Traité à {$json->processing_info->progress_percent}%");

                    if($json->processing_info->state === 'succeeded') {
                        $complete = true;
                    }
                    else if($json->processing_info->state === 'failed') {
                        $GLOBALS['log']->write("Media $id_media invalide.");
                        return null;
                    }
                } while (!$complete);
            }

            return $id_media;
        }

        return null;   
    }

    /**
     * Send an image (png/jpg only) to Twitter using the classic upload method
     *
     * @param string $path
     * Path of the media being uploaded. Existence of file is checked.
     * As this function use file_get_contents() to get file, you can use an URL.
     * 
     * @return string|null
     * Return null if invalid / inexistant file or error while sending file
     * Return media_id_string if media have been uploaded successfully 
     * Use media_id_string while sending tweet, parameter 'media_ids'
     */
    public function sendMedia(string $path) : ?string {
        if(file_exists($path) || filter_var($path, FILTER_VALIDATE_URL)) {
            $array = ['media_data' => base64_encode(file_get_contents($path))];
            try {
                $this->oauth->fetch($this->url_media_unique, $array, OAUTH_HTTP_METHOD_POST, ['Content-Type' => 'application/x-www-form-urlencoded']);
            } catch(OAuthException $ex) {
                $GLOBALS['log']->write('Impossible d\'uploader un media : ' . $ex->lastResponse);
                return null;
            }
    
            $json = json_decode($this->oauth->getLastResponse());
            $id = $json->media_id_string ?? null;
    
            return $id;
        }

        return null;        
    }

    /**
     * Post a new status/tweet from the account configured tokens
     *
     * @param string $tweet
     * Text of the status
     * 
     * @param array $args
     * Optional arguments associated to status (see Twitter doc for details)
     * 
     * @param string|null $attach_one_picture
     * Path to a picture meant to be attached to the tweet
     * Macro for sendMedia() function implemented directly in postStatus()
     * 
     * @return stdClass|null
     * Return JSON status object if tweet is successfully posted to Twitter, null if not
     */
    public function postStatus(string $tweet, ?array $args = [], ?string $attach_one_picture = null) : ?stdClass {
        if(is_null($args)) {
            $args = [];
        }
        $final_args = ['status' => $tweet, 'tweet_mode' => 'extended'];

        if(!isset($args['auto_populate_reply_metadata']) && isset($args['in_reply_to_status_id'])) {
            $args['auto_populate_reply_metadata'] = true;
        }

        if($attach_one_picture) {
            $id = $this->sendMedia($attach_one_picture);

            if($id) {
                $args['media_ids'] = $id;
            }
        }

        $final_args = array_merge($final_args, $args);

        try {
            $this->oauth->fetch($this->url_update, $final_args, OAUTH_HTTP_METHOD_POST);
        } catch(OAuthException $ex) {
            $GLOBALS['log']->write('Impossible d\'envoyer le tweet : ' . $ex->lastResponse);
            return null;
        }

        $json = json_decode($this->oauth->getLastResponse());
        $tweet = $json ?? null;

        return $tweet;
    }

    /**
     * Deprecated function for sending tweets.
     * Use postStatus() instead.
     *
     * @param string $tweet
     * @param string|null $media_path
     * @param string|null $attach_url
     * @param string|null $reply_id_str
     * @param string|null $media_ids
     * @return stdClass|null
     */
    public function sendTweet(string $tweet, ?string $media_path = null, ?string $attach_url = null, 
                              ?string $reply_id_str = null, ?string $media_ids = null) : ?stdClass { // Return ID of tweet or null if fail
        $array = [];
        if ($media_ids) {
            $array['media_ids'] = $media_ids;
        }
        if($attach_url && !$media_path) {
            $array['attachment_url'] = $attach_url;
        }
        if($reply_id_str) {
            $array['in_reply_to_status_id'] = $reply_id_str;
        }

        return $this->postStatus($tweet, $array, $media_path);
    }
    
    /**
     * Macro for postStatus to make replies to a status easier.
     * You shoud better use postStatus with arg ['in_reply_to_status_id' => $id_str] instead.
     *
     * @param string $tweet
     * @param string $id_str
     * ID of tweet you want to reply to
     * 
     * @param string|null $media_path
     * Path to a picture that should be attached to a tweet.
     * 
     * @return stdClass|null
     * Return tweet if success, null otherwise
     */
    public function replyTo(string $tweet, string $id_str, ?string $media_path = null) : ?stdClass {
        return $this->postStatus($tweet, ['in_reply_to_status_id' => $id_str], $media_path);
    }
    
    /**
     * Check Twitter credentials and save some currenty logged user informations in object
     *
     * @return boolean
     * true if credentials are valid, false otherwise
     */
    protected function verificationCredentials() : bool {
        try {
            $this->oauth->fetch($this->url_verify, ['skip_status' => true], OAUTH_HTTP_METHOD_GET);
            
        } catch(OAuthException $ex) {
            $GLOBALS['log']->write('Impossible de valider les credentials Twitter : ' . $ex->lastResponse);
            return false;
        }
        $json = json_decode($this->oauth->getLastResponse());
        // Enregistrement des propriétés de l'utilisateur connecté
        $this->id_str = $json->id_str;
        $this->screen_name = $json->screen_name;
        $this->name = $json->name;

        return true;
    }
    
    public function getSinceId() : string {
        $since_id = @file_get_contents($this->since_id_file);
        if(!$since_id){
            $since_id = '1';
        }
        return $since_id;
    }

    public function setSinceId($max_id = null) : void {
        file_put_contents($this->since_id_file, $max_id);
    }

    public function getSinceIdMention() : string {
        $since_id = @file_get_contents($this->since_id_mention);
        if(!$since_id){
            $since_id = '1';
        }
        return $since_id;
    }

    public function setSinceIdMention(?string $max_id = null) : void {
        file_put_contents($this->since_id_mention, $max_id);
    }

    /**
     * Return mentions of logged user since set since_id
     *
     * @param boolean $setMaxID
     * true if since_id should be set using setSinceIdMention() function at the end
     * 
     * @return Timeline|null
     */
    public function getMentions(bool $setMaxID = true) : ?Timeline { // Récupère les dernières mentions adressées au bot, renvoie une timeline de mentions
        $since_id = $this->getSinceIdMention(); // récupération de l'ancien id max stocké dans un fichier
        $max_id = $since_id;

        $tabTweets = [];
        
        if($this->verificationCredentials()){
            // Récupération des listes de l'utilisateur authentifié pour obtenir le @ et le nom de la liste
            $request = $this->url_mentions_timeline . '?include_rts=false&tweet_mode=extended&include_entities=true&count=100&since_id=%s';
            
            try{
                $this->oauth->fetch(sprintf($request, $since_id));
            }catch(OAuthException $ex){
                $GLOBALS['log']->write('Erreur OAuth à la requête d\'obtention des mentions : ' . $ex->lastResponse);
                return null;
            }
            $resp = json_decode($this->oauth->getLastResponse());
            
            $nbReponses = count($resp);
            $GLOBALS['log']->write("Nombre de mentions renvoyées : $nbReponses");

            foreach($resp as $single_tweet){ 
                $single_tweet->full_text = htmlspecialchars_decode($single_tweet->full_text);                 
                $tabTweets[] = $single_tweet;
            }
            
            // Actualisation de l'ID min pour la prochaine requête : 
            // on prend le premier tweet renvoyé car la timeline est renvoyée chronologiquement (normalement)
            if($nbReponses > 0){
                $max_id = $resp[0]->id_str;
            }
        }
        else{
            $GLOBALS['log']->write("Erreur : Impossible de se connecter au compte et de valider les identifiants. :/ (Le problème pourrait venir de Twitter ou d'une réinitialisation des tokens)");
        }
        
        if($setMaxID){
            $this->setSinceIdMention($max_id);
        } // Comme prévu : Si le booléen vaut vrai, alors on actualise le since_id enregistré
            
        
        if(count($tabTweets) > 0)
            return new Timeline($tabTweets);
        else
            return null;
    }

    public function getCurrentUser() : string {
        if(empty($this->screen_name)) {
            if($this->verificationCredentials()) {
                return $this->screen_name;
            }
            else {
                return "";
            }
        }
        else {
            return $this->screen_name;
        }
    }

    /**
     * Delete a tweet matching $id_str ID
     *
     * @param string $id_str
     * ID of tweet to be deleted
     * 
     * @return boolean
     * true if deletion works fine, false otherwise
     */
    public function deleteTweet(string $id_str) : bool {
        $endp = $this->url_delete . "$id_str.json";
        if($this->verificationCredentials()){
            try {
                $this->oauth->fetch($endp, [], OAUTH_HTTP_METHOD_POST);
                return true;
            } catch(OAuthException $ex) {
                $GLOBALS['log']->write('Impossible de supprimer le tweet : ' . $ex->lastResponse);
            }
        }
        return false;
    }
}
