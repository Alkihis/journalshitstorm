<?php

class Log {
    private $emplacement = '/var/www/html/inc/journalo_bot/bot_log.log';
    private $log_time;
    private $log_type;
    protected $verbose = 0;
    protected $current_storage = '';
    // Verbose mode : 0 -> inactive / 1 -> active (console) / 2 -> active (HTML mode)
    
    public function __construct(bool $answer_mode = false, int $verbose = 0) {
        if($answer_mode) {
            $this->emplacement = '/var/www/html/inc/journalo_bot/bot_log.log';
        }

        $this->log_time = date('Y-m-d H:i:s');
        $this->log_type = $answer_mode;
        $this->verbose = $verbose;

        $this->write('------------- ' . $this->log_time . ' -------------');
    }

    public function __destruct() {
        $this->write('------------- End of ' . $this->log_time . ' -------------');
        $this->flush();
    }

    public function setVerbose(bool $setter) : bool { // Renvoie l'ancien mode
        $ancien = $this->verbose;
        $this->verbose = $setter;
        return $ancien;
    }

    public function write(string $message) : void {
        $this->current_storage .= $message . "\n";

        if($this->verbose) {
            echo $message . ($this->verbose === 2 ? '<br>' : '') . "\n";
        }
    }

    public function flush() : void {
        @file_put_contents($this->emplacement, $this->current_storage, FILE_APPEND);
        $this->current_storage = '';
    }
}
