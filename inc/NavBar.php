<?php

class NavBar implements Iterator {
    protected $elements = [];
    protected $length = 0;

    public function __construct(){ 
    }

    public function addElement($href, $data, $position = NULL){
        if($position !== NULL){
            if(is_numeric($position) && ($position < $this->length || $position == 0)){
                if(!isset($this->elements[$position])){
                    $this->length++;
                }
                $this->elements[$position]['href'] = HTTPS_URL.$href;
                $this->elements[$position]['data'] = $data;
            }
            else{
                trigger_error("Illegal position ($position) used in NavBar::addElement($href, $data).", E_USER_WARNING);
                return;
            }
        }
        else{
            $this->elements[] = ['href' => HTTPS_URL.$href, 'data' => $data];
            $this->length++;
        }
    }

    public function isValid(){
        return $this->length > 0;
    }

    public function deleteElement($position){
        if($position < $this->length){
            unset($this->elements[$position]);
            $this->length--;
        }
    }

    public function rewind(){
        reset($this->elements);
    }

    public function current(){
        return current($this->elements);
    }

    public function key(){
        return key($this->elements);
    }

    public function next(){
        return next($this->elements);
    }

    public function valid(){
        $key = key($this->elements);
        return ($key !== NULL && $key !== FALSE);
    }
}