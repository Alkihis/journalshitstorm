<?php

// ----------------------------------------------
// - VIEILLES FONCTIONS : UTILISATION DEPRÉCIÉE -
// ----------------------------------------------

function generateNextPrevious($remaining_to_view, $countAffiche, $ancient_view, $next_view, $per_page = NULL, $hidden = false){
    echo "<div class='row ".($hidden ? 'hide-on-med-and-up' : '')."'>";
        if($remaining_to_view === '-1' || $countAffiche > 0){
            if($hidden)
                echo "<a href='".HTTPS_URL."journal?perlikeview=$ancient_view".($per_page ? "&perlikepage=$per_page" : '')."'>
                    <button style='margin-bottom: .3em;' class='center btn blue waves-effect waves-light previous col s2 offset-s1'>
                    <i class='material-icons'>arrow_back</i></button>
                </a>";
            else
             echo "<a href='".HTTPS_URL."journal?perlikeview=$ancient_view".($per_page ? "&perlikepage=$per_page" : '')."'>
                    <button style='margin-bottom: .3em;' class='center btn blue waves-effect waves-light previous col m5 s12 l3 offset-l2'>
                    <i class='material-icons left'>arrow_back</i>Page précédente</button>
                </a>";
        }
        else{
            echo "<span class='col ".($hidden ? 's3' : 'm5 s12 l3 offset-l2')."'></span>";
        }
        if($remaining_to_view > 0){
            echo "<a href='".HTTPS_URL."journal?perlikeview=$next_view".($per_page ? "&perlikepage=$per_page" : '')."'>
                    <button class='center btn blue waves-effect waves-light next col ".($hidden ? 's2 push-s6' : 'm5 s12 l3 offset-l2')."'>
                    ".($hidden ? '' : 'Page suivante')."<i class='material-icons ".($hidden ? '' : 'right')."'>arrow_forward</i></button>
                </a>";
        }
        else{
            echo "<span class='col ".($hidden ? 's7' : 'm5 s12 l3 offset-l2')."'></span>";
        }
    echo '</div>';
}

function calculeBoutons($per_page, $countc, $count_affiche, $per_page_button) : array { // Calcule les boutons de pages accessibles sur la vue par note et retourne les paramètres dans un tableau
    // Boutons de page
    // ---------------
    $bornes = array();
    $inside = array();
    $fleche_first = false;
    $fleche_last = false;
    $p_start = false;
    $p_end = false;

    $nbpages = $countc / $per_page;
    if($nbpages > (int)$nbpages){
        $nbpages = (int)$nbpages+1;
    }

    $page_actuelle = ($count_affiche / $per_page)+1;

    $bornes[0] = ['number' => 1, 'count' => 0];

    if($page_actuelle == 1){ // Cas de la borne inférieure
        for($i = 2; $i < 5 && $i < $nbpages; $i++){
            $count_to_page = ($i - 1) * $per_page;
            $inside[] = ['number' => $i, 'count' => $count_to_page];
        }
    }
    elseif($page_actuelle == $nbpages){ // Cas de la borne supérieure
        for($i = $page_actuelle-3; $i < $nbpages; $i++){
            if($i <= 1){
                continue;
            }
            $count_to_page = ($i - 1) * $per_page;
            $inside[] = ['number' => $i, 'count' => $count_to_page];
        }
    }
    else { // Cas général
        for($i = $page_actuelle-1; $i <= $page_actuelle+1; $i++){
            if($i < 2 || $i >= $nbpages) // Si jamais la page en cours est négative / 0 ou 1 > On ignore | Ou alors si on a atteint le nombre de pages (borne)
                continue;
            $count_to_page = ($i - 1) * $per_page; // On calcule le nombre de shitstorm à avancer pour la page de destination
            $inside[] = ['number' => $i, 'count' => $count_to_page];
        }
        if(count($inside) < 3 && $nbpages > 4){ // Remplissage des trous si ils existent (pour éviter d'afficher [1 <  4 (5) > 6] si on est sur la page 5
            for($i = $page_actuelle-2; $i <= $page_actuelle+2; $i++){ // On étend la plage pour masquer les trous
                if($i < 2 || $i >= $nbpages)
                    continue;
                if(count($inside) > 2)
                    break;
                $count_to_page = ($i - 1) * $per_page;
                if($i == $page_actuelle-2)
                    array_unshift($inside, ['number' => $i, 'count' => $count_to_page]);
                elseif($i == $page_actuelle+2)
                    $inside[] = ['number' => $i, 'count' => $count_to_page];
            }
        }
    }
    
    $bornes[1] = ['number' => $nbpages, 'count' => ($nbpages-1)*$per_page];

    $in_co = count($inside);

    if($in_co > 0 || $bornes[1]['number'] != $bornes[0]['number']){
        if($page_actuelle != 1){
            $fleche_first = true;
        }
        if($page_actuelle != $nbpages){
            $fleche_last = true;
        }
        if($in_co > 0){
            if($inside[0]['number']-1 != $bornes[0]['number']){
                if($in_co == 3 && $inside[0]['number']-2 == $bornes[0]['number']){
                    array_unshift($inside, ['number' => 2, 'count' => $per_page]);
                }
                else{
                    $p_start = true;
                }
            }
            if(end($inside)['number']+1 != $bornes[1]['number']){
                if($in_co == 3 && end($inside)['number']+2 == $bornes[1]['number']){
                    $inside[] = ['number' => $nbpages-1, 'count' => ($nbpages-2)*$per_page];
                }
                else{
                    $p_end = true;
                }
            }
        }
    }

    $ancient_view = 0;
    $next_view = $count_affiche + $per_page;
    if($count_affiche-$per_page >= 0)
        $ancient_view = $count_affiche - $per_page;

    return [
        'per_page' => $per_page_button, 
        'actual' => $page_actuelle,
        'ffirst' => $fleche_first,
        'flast' => $fleche_last,
        'pstart' => $p_start,
        'pend' => $p_end,
        'previous' => $ancient_view,
        'next' => $next_view,
        'bornes' => $bornes,
        'inside' => $inside
    ];
}
