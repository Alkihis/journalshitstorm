<?php

// User functions

function userExists(int $id) : bool {
    global $connexion;

    $res = mysqli_query($connexion, "SELECT id FROM Users WHERE id=$id;");

    return ($res && mysqli_num_rows($res));
}

function getIDFromUsername($username) : int { // Renvoie une chaîne avec l'ID de l'utilisateur si réussi, 0 sinon
    if(is_numeric($username)){
        return (int)$username;
    }
    global $connexion;

    $username = mysqli_real_escape_string($connexion, $username);

    $res = mysqli_query($connexion, "SELECT id FROM Users WHERE usrname='$username';");
    if($res && mysqli_num_rows($res) > 0){
        $row = mysqli_fetch_assoc($res);
        return $row['id'];
    }
    return 0;
}

function getClassicSuffixTextByGender(string $gender) : string {
    if($gender === 'm' || $gender === 'male') {
        return '';
    }
    else if($gender === 'f' || $gender === 'female') {
        return "e";
    }
    else {
        return ".e";
    }
}

function getUserSuffixTextByGender(string $gender) : string {
    if($gender === 'm' || $gender === 'male') {
        return "eur";
    }
    else if($gender === 'f' || $gender === 'female') {
        return "rice";
    }
    else {
        return "eur-rice";
    }
}
