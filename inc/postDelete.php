<?php

// Suppression ou post de shitstorm / Réponse

// ------------------
// SOUMISSION D'IMAGE
// ------------------

function traitementFichier($filePOSTname = 'img1', $sub_directory = 'uploaded', $muted = false, ?array $custom_file_entity = NULL) : ?string {
    $filelink = NULL;
    if($custom_file_entity){
        $filelink = &$custom_file_entity;
    }
    else{
        $filelink = &$_FILES[$filePOSTname];
    }

    if($sub_directory == NULL){
        if(!$muted) echo "<h5 class='error'>Erreur d'exécution de la fonction : Répertoire invalide</h5>";
    }
    elseif($filelink['error'] == 0){
        $image_sizes = getimagesize($filelink['tmp_name']);
        if($image_sizes[1] != 0){ // avoid / by 0 exception
            if(($image_sizes[0] / (float)$image_sizes[1]) > 0.4){
                $extensions_valides = array('jpg','jpeg','png');
                $extension_upload = strtolower(substr(strrchr($filelink['name'], '.'), 1)); // Search last ., catch the part after it, then lower-ise it
        
                if(in_array($extension_upload, $extensions_valides)){
                    if($filelink['size'] <= 1048576){
                        do{
                            $nomInit = md5(uniqid(rand(), true));
                            $nom = "/var/www/html/img/$sub_directory/$nomInit.$extension_upload";
                        }while(file_exists($nom));
                        $resultat = move_uploaded_file($filelink['tmp_name'], $nom);
                        if($resultat){
                            $nom = "img/$sub_directory/$nomInit.$extension_upload";
                            return $nom;
                        }
                    }
                }
            }
            else{
                if(!$muted) echo "<h5 class='error'>Dimensions incorrectes : ratio largeur/hauteur trop faible</h5>";
            }
        } 
        else {
            if(!$muted) echo "<h5 class='error'>Dimensions invalides : Une opérande vaut 0.</h5>";
        }
    }
}

function saveImgForUser($img_name, $idUsr){
    global $connexion;

    // Vérification de l'existance d'une image de profil
    $res = mysqli_query($connexion, "SELECT profile_img FROM Users WHERE id='$idUsr';");
    
    if($res && mysqli_num_rows($res) > 0){
        $row = mysqli_fetch_assoc($res);

        $res2 = mysqli_query($connexion, "UPDATE Users SET profile_img='$img_name' WHERE id='$idUsr';");

        if($res2){
            if(preg_match(REGEX_IMG_PROFILE, $row['profile_img'])){
                deleteFile($row['profile_img'], 'profile');
            }

            return ['new_profile_img' => $img_name]; 
        }
        else { 
            deleteFile($img_name, 'profile');
        }
    }
    else{
        deleteFile($img_name, 'profile');
    }
}

function setDefaultImgForUser($idUsr){
    global $connexion;

    // Vérification de l'existance d'une image de profil
    $res = mysqli_query($connexion, "SELECT profile_img FROM Users WHERE id='$idUsr';");
    
    if($res && mysqli_num_rows($res) > 0){
        $row = mysqli_fetch_assoc($res);

        $res2 = mysqli_query($connexion, "UPDATE Users SET profile_img='img/default.png' WHERE id='$idUsr';");

        if($res2){
            if(preg_match(REGEX_IMG_PROFILE, $row['profile_img'])){
                deleteFile($row['profile_img'], 'profile');
            }

            return ['new_profile_img' => 'img/default.png']; 
        }
    }
}

function saveImgBannerForUser($img_name, $idUsr){
    global $connexion;

    // Vérification de l'existance d'une image de profil
    $res = mysqli_query($connexion, "SELECT banner_img FROM Users WHERE id='$idUsr';");
    
    if($res && mysqli_num_rows($res) > 0){
        $row = mysqli_fetch_assoc($res);

        $res2 = mysqli_query($connexion, "UPDATE Users SET banner_img='$img_name' WHERE id='$idUsr';");

        if($res2){
            if(preg_match(REGEX_IMG_BANNER, $row['banner_img'])){
                deleteFile($row['banner_img'], 'banner');
            }

            return ['new_banner_img' => $img_name]; 
        }
        else { 
            deleteFile($img_name, 'banner');
        }
    }
    else{
        deleteFile($img_name, 'banner');
    }
}

function setDefaultBannerForUser($idUsr){
    global $connexion;

    // Vérification de l'existance d'une image de profil
    $res = mysqli_query($connexion, "SELECT banner_img FROM Users WHERE id='$idUsr';");
    
    if($res && mysqli_num_rows($res) > 0){
        $row = mysqli_fetch_assoc($res);

        $res2 = mysqli_query($connexion, "UPDATE Users SET banner_img=NULL WHERE id='$idUsr';");

        if($res2){
            if(preg_match(REGEX_IMG_BANNER, $row['banner_img'])){
                deleteFile($row['banner_img'], 'banner');
            }

            return ['new_banner_img' => false]; 
        }
    }
}

// --------------------------
// ANONYMISATION DE SHITSTORM
// --------------------------

function anonymiseShitstorm($idSub){
    global $connexion;

    mysqli_query($connexion, "UPDATE Shitstorms SET pseudo='profil supprimé', idUsr=0 WHERE idSub='$idSub';");
}

// Autre

function addAnswer(&$answers, $date, $dscr, $link, $fileName, $idUsr){
    global $connexion;

    if(verifyAnswerSetted($link, $fileName, $date)){
        $answers[] = ['date' => $date, 'link' => mysqli_real_escape_string($connexion, $link), 'filename' => $fileName, 'dscr' => mysqli_real_escape_string($connexion, $dscr), 'idUsr' => $idUsr];
    }
}

// Post de commentaire
// -------------------

function postComment(int $idSub, string $content, int $idUsr, ?int $inReplyTo = null) : int { // Return 0 if success
    global $connexion;

    // VERIFICATION DE L'EXISTENCE DE LA PUBLICATION
    if($idSub <= 0){
        return -1; // Shitstorm does not exists
    }
    $res = mysqli_query($connexion, "SELECT idUsr FROM Shitstorms WHERE idSub='$idSub'");
    if(!($res && mysqli_num_rows($res) > 0)){
        return -1;
    }

    $row = mysqli_fetch_assoc($res);

    // VERIFICATION DE LA LONGUEUR
    $content = trim(mysqli_real_escape_string($connexion, $content));
    if(mb_strlen($content) > MAX_LEN_COMMENT){
        return -2; // Comment is too long
    }

    $inReply = 'NULL';
    $inReplyMain = 'NULL';

    if($inReplyTo > 0) {
        $inReply = $inReplyTo;
        $inReplyMain = $inReplyTo;

        $test = mysqli_query($connexion, "SELECT idCom, inReplyToIdMain FROM Comment WHERE idCom=$inReply;");
        // Si le commentaire où la réponse est demandée n'existe pas
        if(!($test && mysqli_num_rows($test))){
            return -3; // Replies goes to inexistant comment
        }
        if(!verifyInReplyCommentValidity($inReply, $content)){
            $inReply = 'NULL';
            $inReplyMain = 'NULL';
        }
        $rowReply = mysqli_fetch_assoc($test);
        if($rowReply['inReplyToIdMain'] != null){ // Si le commentaire auquel on répond est une réponse
            // On stocke l'ID du commentaire initial qui a débuté la conversation
            $inReplyMain = (int)$rowReply['inReplyToIdMain'];
        }
    }

    // INSERTION
    $res = mysqli_query($connexion, "INSERT INTO Comment (content, idUsr, idSub, inReplyToId, inReplyToIdMain) VALUES ('$content', '$idUsr', '$idSub', $inReply, $inReplyMain);");
    if(!$res){
        return -4; // Unable to fetch database
    } else {
        $idCom = mysqli_insert_id($connexion);

        // Envoi des possibles notifications
        $IDs = containMention($content);

        foreach($IDs as $id){
            if($id != $idUsr) 
                sendNotif('mention_comment', $idUsr, $id, $idCom);
        }

        // Envoi de la notif aux abonnés de la SS
        sendNotif('post_comment', $idUsr, $idSub, $idCom);

        return (int)$idCom; // Success
    }
}

// --------------------------------
// ACCEPTATION DE SHITSTORM/REPONSE
// --------------------------------

function acceptShitstorm(int $idSub) : int {
    global $connexion;

    // Début du traitement de la publication de la shitstorm dans la table originale
    $res = mysqli_query($connexion, "SELECT * FROM Waiting WHERE idSub='$idSub';");

    if(mysqli_num_rows($res) === 0){
        return -1; // Shitstorm introuvable
    }
    else {
        $row = mysqli_fetch_assoc($res);
        $idUsr = (int)$row['idUsr'];
    
        $psu = mysqli_real_escape_string($connexion, $row['pseudo']);
        $dateShit = $row['dateShit'];
        $link = [];
        $resL = mysqli_query($connexion, "SELECT * FROM WaitingLinks WHERE idSub='$idSub';");
        if($resL && mysqli_num_rows($resL) > 0){
            while($rowL = mysqli_fetch_assoc($resL)){
                $link[] = mysqli_real_escape_string($connexion, $rowL['link']);
            }
        }
        else{
            return -2; // Aucun lien dans la shitstorm
        }

        $title = mysqli_real_escape_string($connexion, $row['title']);

        // Verif si le title existe déjà
        $resTi = mysqli_query($connexion, "SELECT idSub FROM Shitstorms WHERE title='$title';");

        if($resTi && mysqli_num_rows($resTi)){
            return -3; // Titre déjà pris
        }

        $dscr = mysqli_real_escape_string($connexion, $row['dscr']);
        $appro = $_SESSION['id'];
    
        $res = NULL;
    
        if(isset($_POST['pub'])){
            if($link[0] == ''){
                return -4; // Lien 1 invalide, nécessaire
            }
    
            foreach($link as $key => $l){
                if($l != ''){
                    if(!preg_match(REGEX_TWITTER_LINK, $l) && !preg_match(REGEX_MASTODON_LINK, $l)){
                        if(!preg_match(REGEX_IMG_LINK_UPLOADED, $l)){
                            return -5; // Un des liens est invalide
                        }
                    }
                }
                else $link[$key] = NULL;
            }
    
            $res = mysqli_query($connexion, "INSERT INTO Shitstorms (title, dscr, pseudo, idUsr, dateShit, idApprovator) 
                                             VALUES ('$title', '$dscr', '$psu', '$idUsr', '$dateShit', '$appro');");
            $idSubSended = mysqli_insert_id($connexion);

            if(!$res){
                return -6; // Insertion a échoué
            }

            foreach($link as $key => $l) {
                if($l != NULL){
                    postLink($idSubSended, $l); // Poste les liens et les enregistre
                }
            }

            // Envoi des réponses en attente associées à la shitstorm en attente
            $resA = mysqli_query($connexion, "SELECT * FROM WaitAnsw WHERE fromWaitingShitstorm=1 AND idSub='$idSub';");
            if($resA && mysqli_num_rows($resA)){
                while($rowA = mysqli_fetch_assoc($resA)){
                    $dscrA = mysqli_real_escape_string($connexion, $rowA['dscrA']);
                    $dateA = mysqli_real_escape_string($connexion, $rowA['dateAn']);
                    $linkA = mysqli_real_escape_string($connexion, $rowA['linkA']);

                    $resM = postAnswer($idSubSended, $dscrA, $linkA, $rowA['idUsrA'], $dateA, $appro);

                    if($res){
                        $idNewSub = $resM;
                    }

                    $resM = deleteOneAnswer(true, $rowA['idSubA'], false);
                }
            }

            if($res){
                if($idUsr != 0){
                    sendNotif('shitstorm', $idUsr, 0, $idSubSended);
                    sendNotif('confirm_shitstorm', $appro, $idUsr, $idSubSended); // Notif au posteur
                    mysqli_query($connexion, "INSERT INTO ShitFollowings (idFollower, idFollowed) VALUES ('$idUsr', '$idSubSended');");
                    // Inscrit le posteur aux abonnés de sa shitstorm
                }    
                $res = mysqli_query($connexion, "DELETE FROM Waiting WHERE idSub='$idSub'");
                deleteLinks(true, $idSub, false); // Waiting, et ne pas supprimer le fichier
            }
        }
        if($res && $idSubSended){
            return (int)$idSubSended;
        }
        else{
            return -6;
        }
    }
}

function acceptAnswer(int $idSubA) : int {
    // Début du traitement de la publication de la shitstorm dans la table originale
    $res = mysqli_query($connexion, "SELECT * FROM WaitAnsw WHERE idSubA='$idSubA';");

    if(mysqli_num_rows($res) == 0){
        return -1; // réponse supprimée
    }
    else{
        $row = mysqli_fetch_assoc($res);

        if($row['fromWaitingShitstorm']){
            return -2; // Liée à une shitstorm en attente
        }
        else{
            $idUsr = (int)$row['idUsrA'];
        
            $idSub = (int)$row['idSub'];
            $date = $row['dateAn'];
            $link = mysqli_real_escape_string($connexion, $row['linkA']);
            $dscr = mysqli_real_escape_string($connexion, $row['dscrA']);
            $appro = $_SESSION['id']; 
    
            $resI = mysqli_query($connexion, "SELECT * FROM Answers WHERE idSub='$idSub';");
            if(!preg_match(REGEX_TWITTER_LINK, $link) && !preg_match(REGEX_IMG_LINK_UPLOADED, $link) && !preg_match(REGEX_MASTODON_LINK, $link)){
                return -3; // Lien invalide
            }
            else if(!$resI || @mysqli_num_rows($resI) >= MAX_ANSWER_COUNT) {
                return -4; // Trois réponses déjà présentes
            }
            else{
                if(!preg_match("/^img\/uploaded\/.+$/", $link)){
                    if($link != ''){
                        if(!preg_match(REGEX_TWITTER_LINK, $link) && !preg_match(REGEX_MASTODON_LINK, $link)){
                            return -3; // Lien invalide
                        }
                    }
                    else return -3; // Lien invalide
                }
                if($link){
                    $res = postAnswer($idSub, $dscr, $link, $idUsr, $date, $appro);
    
                    if($res){
                        $idNewSub = $res;
                        $res = deleteOneAnswer(true, $idSubA, false);
    
                        sendNotif('confirm_answer', $appro, $idUsr, $idNewSub); // Notif au posteur
                        // Envoi de la notif aux abonnés de la SS
                        sendNotif('post_answer', $idUsr, $idSub, $idNewSub);
    
                        return (int)$idNewSub;
                    }
                    else{
                        return -4; // Erreur d'insertion
                    }
                }
                else {
                    return -3; // Lien invalide
                }
            }
        }
    }
}

// ---------------------------------------------------------
// POST DE LIEN (avec sauvegarde) [Shitstorms puis réponses]
// ---------------------------------------------------------

function getBlockquote($link, $omit_script = true) : ?string {
    $curl = curl_init();

    if(!preg_match(REGEX_WEB_LINK, $link)) { // On lui a passé un ID
        $link = "https://twitter.com/username/status/".$link;
    }
    
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL =>  "https://publish.twitter.com/oembed?url=".$link."&align=center".($omit_script ? "&omit_script=1" : ''),
    ));
    // Send the request & save response to $resp
    $resp = curl_exec($curl);
    curl_close($curl);

    $embedJson = json_decode($resp);
    if(isset($embedJson->author_name)){
        return $embedJson->html;
    }

    return null;
}

function postWaitingShitstorm(string $title, string $dscr, string $psu, int $idUsr, string $date, array $link, array $valid_answers = []) : int {
    global $connexion;

    $res = mysqli_query($connexion, "INSERT INTO Waiting (title, dscr, pseudo, idUsr, dateShit) VALUES ('$title', '$dscr', '$psu', '$idUsr', '$date') ;");
    if($res){
        $idSub = mysqli_insert_id($connexion);
        sendNotif('moderation_shitstorm', $idUsr, 9, $idSub);

        foreach($link as $l){
            if($l != NULL){
                mysqli_query($connexion, "INSERT INTO WaitingLinks (link, idSub) VALUES ('$l', '$idSub');");
            }
        }
        // Insère les réponses à faire attendre
        foreach($valid_answers as $an){
            if($an['link'] != NULL){
                mysqli_query($connexion, "INSERT INTO WaitAnsw (dscrA, linkA, idUsrA, idSub, dateAn, fromWaitingShitstorm) 
                                            VALUES ('{$an['dscr']}', '{$an['link']}', '{$an['idUsr']}', '$idSub', '{$an['date']}', 1);");
            }
        }

       return $idSub;
    }
    else {
        return -1;
    }
}

function postLink(int $idSub, string $link, bool $is_for_shitstorm = true, bool $muted = false) : ?bool {
    global $connexion;

    if(!preg_match(REGEX_TWITTER_LINK, $link)){
        if(preg_match(REGEX_MASTODON_LINK, $link)){
            // Sauvegarde du lien
            $res = mysqli_query($connexion, "INSERT INTO Links (link, position, idSub) VALUES ('$link', '0', '$idSub');");
        }
        else if(!preg_match(REGEX_IMG_LINK_UPLOADED, $link)){
            if(!$muted) echo "<h5 class='error'>Le lien \"$link\" est invalide.</h5>";
            else return null;
        }
        else { // C'est une image
            if($is_for_shitstorm){
                // Sauvegarde du lien
                $res = mysqli_query($connexion, "INSERT INTO Links (link, position, idSub) VALUES ('$link', '0', '$idSub');");
            }
        }
    }
    else{
        // Lien twitter
        if($is_for_shitstorm){
            // Sauvegarde du lien
            $idTweet = (int)getTweetIdFromURL($link);
            if($idTweet === 0) {
                $idTweet = 'NULL';
            }
            $res = mysqli_query($connexion, "INSERT INTO Links (link, position, idSub, idTweet) VALUES ('$link', '0', '$idSub', $idTweet);");

            // Si l'insertion a réussi, on sauvegarde le tweet
            if($res) {
                $idLink = mysqli_insert_id($connexion);

                if($idTweet !== 'NULL'){
                    createSaveFromTweet((int)$idTweet);
                }
            }
            else{
                return null;
            }
        }
    }
    return true;
}

// Sauvegarde et post de réponse
function postAnswer(int $idSub, string $dscr, string $link, int $idUsr, string $date, int $appro, bool $muted = true) : ?int { // Retourne l'ID de la réponse nouvellement crée
    global $connexion;

    if(!preg_match(REGEX_TWITTER_LINK, $link)){
        if(!preg_match(REGEX_IMG_LINK_UPLOADED, $link) && !preg_match(REGEX_MASTODON_LINK, $link)){
            if(!$muted) echo "<h5 class='error'>Le lien \"$link\" est invalide.</h5>";
            else return NULL;
        }
        // Image ou lien masto
        // Sauvegarde
        $res = mysqli_query($connexion, "INSERT INTO Answers (idSub, dscrA, linkA, idUsrA, dateAn, idApprovatorA) VALUES 
        ('$idSub', '$dscr', '$link', '$idUsr', '$date', '$appro');");

        $idAn = mysqli_insert_id($connexion);
        return $idAn;
    }
    else {
        // Lien twitter
        // Sauvegarde
        $idTweet = (int)getTweetIdFromURL($link);
        if($idTweet === 0) {
            $idTweet = 'NULL';
        }

        $res = mysqli_query($connexion, "INSERT INTO Answers (idSub, dscrA, linkA, idUsrA, dateAn, idApprovatorA, idTweet) VALUES 
        ('$idSub', '$dscr', '$link', '$idUsr', '$date', '$appro', $idTweet);");

        // L'insertion a réussi, on sauvegarde le tweet si présent
        if($res) {
            $idAn = mysqli_insert_id($connexion);

            if($idTweet !== 'NULL'){
                createSaveFromTweet((int)$idTweet);
            }

            return $idAn;
        }
        else{
            return null;
        }
    }
}

function updateAnswer(int $idAn, string $link) : bool {
    global $connexion;

    $idTweet = (int)getTweetIdFromURL($link);
    if($idTweet === 0) {
        $idTweet = 'NULL';
    }

    $resprev = mysqli_query($connexion, "SELECT idTweet FROM Links WHERE idLink=$idLink;");
    $idPreviousTweet = null;

    if($resprev && mysqli_num_rows($resprev)) {
        $row = mysqli_fetch_assoc($resprev);

        $idPreviousTweet = $row['idTweet'];
    }

    $respost = mysqli_query($connexion, "UPDATE Answers SET linkA='$link', idTweet=$idTweet WHERE idAn='$idAn';");

    // Supprime la sauvegarde de l'ancien tweet
    if($idPreviousTweet) {
        deleteTweetSave($idPreviousTweet);
    }    
    // Sauvegarde le nouveau
    if($idTweet) {
        createSaveFromTweet($idTweet);
    }

    return true;
}

// TWEET SAVE 4 FUNCTIONS
// id_str is tweet ID
function createImgSaveFromTwitterLink(int $id_save, string $id_str) {
    global $connexion;

    if($id_str){ // Si il y a un id de tweet
        require_once(ROOT . '/inc/TwitterBot.php');
        $dir = 'tweet_save';

        $tw = new TwitterBot(CONSUMER_KEY, CONSUMER_SECRET);
        $tw->setToken(OAUTH_KEY, OAUTH_SECRET);

        $medias = $tw->getMedias($id_str); // Return a json object

        if($medias){
            $path = ROOT . "/img/$dir/";
            foreach($medias as $media){
                $url = $media->media_url;

                $fileName = '';
                do{
                    $fileName = md5(random_bytes(8));
                } while(file_exists($path.$fileName.'.jpg'));

                $fullPath = $path.$fileName.'.jpg';
                $smallPath = "img/$dir/$fileName.jpg";
                // Engistrement de l'image
                file_put_contents($fullPath, fopen($url, 'r'));

                // Sauvegarde dans la BDD
                mysqli_query($connexion, "INSERT INTO tweetSaveImg (idSave, link) VALUES ('$id_save', '$smallPath');");
            }
        }
    }
}

/**
 * Create a save for a tweet
 *
 * @param integer $id_tweet
 * ID of tweet that will be saved
 * @param boolean $set_as_oprhan
 * true if tweet isn't currently linked to a shitstorm or an answer
 * If it isn't, the tweet have 2 weeks to be linked to a shitstorm or an answer, afterwards it will be deleted
 * @return void
 */
function createSaveFromTweet(int $id_tweet, bool $set_as_oprhan = false) : void {
    global $connexion;

    $html = getBlockquote((string)$id_tweet);
    $set_orphan = (int)$set_as_oprhan;

    if($html && $id_tweet) {
        $matches = [];
        preg_match(REGEX_BLOCKQUOTE, $html, $matches);

        $content = mysqli_real_escape_string($connexion, $matches[1]);
        $infos = mysqli_real_escape_string($connexion, $matches[2]);

        // Vérification que le tweet n'a pas déjà été sauvegardé
        $res = mysqli_query($connexion, "SELECT idTweet FROM tweetSave WHERE idTweet=$id_tweet;");

        if($res && mysqli_num_rows($res) === 0) {
            $res = mysqli_query($connexion, "INSERT INTO tweetSave (idTweet, content, infos, notLinked) VALUES
                                             ($id_tweet, '$content', '$infos', $set_orphan);");

            if($res && preg_match('/pic\.twitter\.com/', $html)){ // Si il y a un lien vers media
                $insert_id = mysqli_insert_id($connexion);

                createImgSaveFromTwitterLink($insert_id, $id_tweet);
            }
        }
        else if (!$set_as_oprhan) {
            // Le tweet est sauvegardé, mais là on demande de le set "lié à une shitstorm" (non orphelin,
            // donc on supprime son possible notLinked=1

            $res = mysqli_query($connexion, "UPDATE tweetSave SET notLinked=0 WHERE idTweet=$id_tweet;");
        }
    }
    else if ($id_tweet) {
        // Vérification que le tweet n'a pas déjà été sauvegardé
        $res = mysqli_query($connexion, "SELECT idTweet FROM tweetSave WHERE idTweet=$id_tweet;");

        if ($res && mysqli_num_rows($res)) { // Si le tweet est déjà save
            if (!$set_as_oprhan) {
                // Le tweet est sauvegardé, mais là on demande de le set "lié à une shitstorm" (non orphelin,
                // donc on supprime son possible notLinked=1

                $res = mysqli_query($connexion, "UPDATE tweetSave SET notLinked=0 WHERE idTweet=$id_tweet;");
            }
        }
    }
}

function deleteTweetSave(?int $id_tweet, bool $force_deletion_on_oprhan = false) : bool {
    global $connexion;

    if($id_tweet) {
        $registred_id = [];

        // On recheche les ID déjà enregistrés
        $res = mysqli_query($connexion, "SELECT idTweet, idLink FROM Links WHERE idTweet IS NOT NULL;");

        while($row = mysqli_fetch_assoc($res)) {
            $registred_id[(int)$row['idTweet']] = true;
        }

        $res = mysqli_query($connexion, "SELECT idTweet, idAn FROM Answers WHERE idTweet IS NOT NULL;");
        while($row = mysqli_fetch_assoc($res)) {
            $registred_id[(int)$row['idTweet']] = true;
        }

        if(!array_key_exists($id_tweet, $registred_id)) { // La clé n'existe pas => On peut supprimer la sauvegarde
            deleteSavedTweetByID($id_tweet, $force_deletion_on_oprhan);
            return true;
        }
        else { // On définit le tweet comme "notLinked" au cas où
            mysqli_query($connexion, "UPDATE tweetSave SET notLinked=0 WHERE idTweet=$id_tweet;");
            return false;
        }
    }
    return false;
}

// NOT MEANT TO BE CALLED
function deleteSavedTweetByID(int $id_tweet, bool $force_deletion_on_oprhan = false) {
    global $connexion;

    $res = mysqli_query($connexion, "SELECT * FROM tweetSave WHERE idTweet='$id_tweet';");

    if($res && mysqli_num_rows($res) > 0){
        $row = mysqli_fetch_assoc($res);

        if($row['notLinked'] == 1 && (strtotime($row['addDate']) + (60 * 60 * 24 * 15)) > time() && !$force_deletion_on_oprhan) {
            // Si le tweet est un tweet 'non-lié' et sa date d'enregistrement a moins de 15 jours, et on ne demande pas la suppression absolue
            // On ne supprime pas dans ce cas

        }
        else {
            $res = mysqli_query($connexion, "SELECT * FROM tweetSaveImg WHERE idSave='{$row['idSave']}';");

            if($res && mysqli_num_rows($res) > 0){
                while($rowImg = mysqli_fetch_assoc($res)){
                    $current_link = ROOT . '/' . $rowImg['link'];
                    if(file_exists($current_link)){
                        unlink($current_link);
                    }
                }
    
                $res = mysqli_query($connexion, "DELETE FROM tweetSaveImg WHERE idSave='{$row['idSave']}';");
            }
    
            mysqli_query($connexion, "DELETE FROM tweetSave WHERE idTweet='$id_tweet';");
        }
    }
}
// END TWEET SAVER FUNCTIONS

// -------------------------------------------------------
// SUPPRESSION SAUVEGARDE AVEC IMAGE POSSIBLEMENT CONTENUE
// -------------------------------------------------------

function deleteOneSave($idLink){
    global $connexion;

    $res = mysqli_query($connexion, "SELECT idTweet FROM Links WHERE idLink=$idLink;");

    if($res && mysqli_num_rows($res)) {
        $row = mysqli_fetch_assoc($res);

        if(isset($row['idTweet'])) {
            deleteTweetSave($row['idTweet']);
        }
    }
}

function deleteSaves($idSub){
    global $connexion;

    $res = mysqli_query($connexion, "SELECT * FROM Links WHERE idSub='$idSub';");

    if($res && mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_assoc($res)){
            deleteOneSave($row['idLink']);
        }
    }
}

function autoDetectLinkTweetId(int $id_link) : void {
    global $connexion;

    $res = mysqli_query($connexion, "SELECT * FROM Links WHERE idLink=$id_link;");

    if($res && mysqli_num_rows($res)) {
        $row = mysqli_fetch_assoc($res);

        $id_link = (int)getTweetIdFromURL($row['link']);
        if($id_link === 0) {
            $id_link = 'NULL';
        }

        mysqli_query($connexion, "UPDATE Links SET idTweet='$id' WHERE idLink=$id_link;");
    }
}

// ---------------------------------------------------------
// SUPPRESSION LIEN/REPONSE AVEC IMAGE POSSIBLEMENT CONTENUE
// ---------------------------------------------------------

function deleteLinks($waiting, $idSub, $hasToDeleteFile = true){ // Delete all links affiliated with shitstormID (waiting or not) and files attached to
    global $connexion;

    if($waiting){
        $res = mysqli_query($connexion, "SELECT * FROM WaitingLinks WHERE idSub='$idSub';");
        if(!$res || @mysqli_num_rows($res) == 0){
            return NULL;
        }
    }
    else{
        $res = mysqli_query($connexion, "SELECT * FROM Links WHERE idSub='$idSub';");
        if(!$res || @mysqli_num_rows($res) == 0){
            return NULL;
        }
    }

    while($row = mysqli_fetch_assoc($res)){
        deleteOneLink($waiting, $row['idLink'], $hasToDeleteFile);
    }

    return true;
}

function deleteOneLink($waiting, $idLink, $hasToDeleteFile = true){ // Delete one link and file attached to
    global $connexion;

    $res = NULL;
    if($hasToDeleteFile){
        if($waiting){
            $res = mysqli_query($connexion, "SELECT * FROM WaitingLinks WHERE idLink='$idLink';");
            if(!$res || @mysqli_num_rows($res) == 0){
                return false;
            }
        }
        else{
            $res = mysqli_query($connexion, "SELECT * FROM Links WHERE idLink='$idLink';");
            if(!$res || @mysqli_num_rows($res) == 0){
                return false;
            }
        }
        while($row = mysqli_fetch_assoc($res)){
            $link = $row['link'];
    
            deleteFile($link);
        }
    }
    
    if($waiting){
        $res = mysqli_query($connexion, "DELETE FROM WaitingLinks WHERE idLink='$idLink'");
    }
    else{
        // Récupère des infos pour supprimer la sauvegarde
        $res2 = mysqli_query($connexion, "SELECT idTweet FROM Links WHERE idLink=$idLink;");

        $res = mysqli_query($connexion, "DELETE FROM Links WHERE idLink='$idLink'");

        if($res2 && mysqli_num_rows($res2)) {
            $row = mysqli_fetch_assoc($res2);

            if(isset($row['idTweet'])) {
                deleteTweetSave($row['idTweet']);
            }
        }
    }
    return $res;
}

function deleteAnswers($waiting, $idSub, $hasToDeleteFile = true, $fromWaiting = false){ // Delete all answers affiliated with shitstormID (waiting or not) and files attached to
    global $connexion;

    $res = NULL;
    if($hasToDeleteFile){
        if($waiting){
            $res = mysqli_query($connexion, "SELECT * FROM WaitAnsw WHERE idSub='$idSub' AND fromWaitingShitstorm=" . ($fromWaiting ? '1;' : '0;'));
            if(!$res){
                return NULL;
            }
        }
        else{
            $res = mysqli_query($connexion, "SELECT * FROM Answers WHERE idSub='$idSub';");
            if(!$res){
                return NULL;
            }
        }
        while($row = mysqli_fetch_assoc($res)){
            $link = $row['linkA'];
    
            deleteFile($link);
        }
    }
    
    if($waiting){
        $res = mysqli_query($connexion, "DELETE FROM WaitAnsw WHERE idSub='$idSub' AND fromWaitingShitstorm=" . ($fromWaiting ? '1;' : '0;'));
    }
    else{
        $res = mysqli_query($connexion, "DELETE FROM Answers WHERE idSub='$idSub'");
    }

    if(!$fromWaiting && !$waiting){
        // Supprime les sauvegardes des réponses uniquement si elles ne sont pas en attente
        deleteTweetSave($row['idTweet']); // Suppression du tweet sauvegardé
    }
    return $res;
}

function deleteOneAnswer($waiting, $idAn, $hasToDeleteFile = true) { // Delete one answer and file attached to
    global $connexion;

    $res = NULL;
    if($hasToDeleteFile){
        if($waiting){
            $res = mysqli_query($connexion, "SELECT * FROM WaitAnsw WHERE idSubA='$idAn';");
            if(!$res){
                return false;
            }
        }
        else{
            $res = mysqli_query($connexion, "SELECT * FROM Answers WHERE idAn='$idAn';");
            if(!$res){
                return false;
            }
        }
        while($row = mysqli_fetch_assoc($res)){
            $link = $row['linkA'];
    
            deleteFile($link);
        }
    }
    
    if($waiting){
        $res = mysqli_query($connexion, "DELETE FROM WaitAnsw WHERE idSubA='$idAn'");
    }
    else{
        $res = mysqli_query($connexion, "DELETE FROM Answers WHERE idAn='$idAn'");
    }

    if(!$waiting){
        deleteTweetSave($row['idTweet']); // Suppression du tweet sauvegardé
    }
    return (bool)$res;
}

function deleteWaitingAnswer(int $idAn) : bool {
    // Supprime uniquement la réponse refusée. 
    // Les autres associés à la même SS ne le sont pas

    return (bool)deleteOneAnswer(true, $idAn, true); 
}

function deleteCom($idCom) : bool {
    global $connexion;

    mysqli_query($connexion, "UPDATE Comment SET inReplyToIdMain=NULL WHERE inReplyToIdMain='$idCom';");
    mysqli_query($connexion, "UPDATE Comment SET inReplyToId=NULL WHERE inReplyToId='$idCom';");

    $res = mysqli_query($connexion, "DELETE FROM Comment WHERE idCom='$idCom';");
    $res2 = mysqli_query($connexion, "DELETE FROM ComLikes WHERE idCom='$idCom';");
    if($res && $res2){
        return true;
    }
    else{
        return false;
    }
}

function deleteWaitingShitstorm(int $idSub) : bool {
    global $connexion;

    $res = mysqli_query($connexion, "DELETE FROM Waiting WHERE idSub='$idSub';");
    if($res) $res = deleteLinks(true, $idSub);
    deleteAnswers(true, $idSub, true, true);
    // Supprime les possibles réponses en attente associées à la shitstorm

    return (bool)$res;
}

function deleteShitstorm($idSub) : bool { // Supprime une shitstorm, liens, commentaires et réponses associées
    global $connexion;

    $res = mysqli_query($connexion, "DELETE FROM Shitstorms WHERE idSub='$idSub';");
    $res2 = mysqli_query($connexion, "DELETE FROM ShitLikes WHERE idSub='$idSub';");
    $res3 = deleteLinks(false, $idSub);
    $res4 = deleteAnswers(false, $idSub);
    $res5 = deleteAnswers(true, $idSub);
    $res6 = mysqli_query($connexion, "DELETE FROM ShitFollowings WHERE idFollowed='$idSub';");

    $reqComment = mysqli_query($connexion, "SELECT * FROM Comment WHERE idSub='$idSub';");
    while($rowComment = mysqli_fetch_assoc($reqComment)){
        deleteCom($rowComment['idSub']);
    }

    if($res && $res2 && $res3 && $res4){
        return true;
    }
    else{
        return false;
    }
}

function deleteFile($path, $sub_dir = 'uploaded', $has_to_verif = false){ // Delete file by theorical path (w/o /var/...).
    if(preg_match("/^img\/$sub_dir\/.+$/", $path)){ // Le lien est une image
        if($has_to_verif){ // Vérifie si le lien donné est présent qu'une seule fois. Si ce n'est pas le cas, le fichier ne sera pas supprimé.
            global $connexion;
    
            $res1 = mysqli_query($connexion, "SELECT * FROM Links WHERE link='$path';");
            $count = 0;
            if($res1){
                $count += mysqli_num_rows($res1);
            }
            if($count < 2){ // Si il n'apparait pas ou une fois
                $res2 = mysqli_query($connexion, "SELECT * FROM Answers WHERE linkA='$path';");
                if($res2){
                    $count += mysqli_num_rows($res2);
                }
            }

            if($count < 2){ // On a le droit de supprimer
                $path = "/var/www/html/" . $path;
                if(file_exists($path)){
                    unlink($path);
                }
            }
        }
        else {
            $path = "/var/www/html/" . $path;
            if(file_exists($path)){
                unlink($path);
            }
        }
    }
}

function deleteProfileImg(int $idUsr, bool $replace_with_default = false) : void {
    global $connexion;

    $res = mysqli_query($connexion, "SELECT profile_img FROM Users WHERE id='$idUsr';");
    if($res && mysqli_num_rows($res) > 0){
        $row = mysqli_fetch_assoc($res);
        if(preg_match(REGEX_IMG_PROFILE, $row['profile_img'])){
            deleteFile($row['profile_img'], 'profile'); // Supprime l'image de profil
        }

        if($replace_with_default){
            mysqli_query($connexion, "UPDATE Users SET profile_img='".DEFAULT_PROFILE_IMG."' WHERE id='$idUsr';");
        }
    }
}

function deleteBannerImg(int $idUsr, bool $replace_with_null = false) : void {
    global $connexion;

    $res = mysqli_query($connexion, "SELECT banner_img FROM Users WHERE id='$idUsr';");
    if($res && mysqli_num_rows($res) > 0){
        $row = mysqli_fetch_assoc($res);
        if($row['banner_img'] && preg_match(REGEX_IMG_BANNER, $row['banner_img'])){
            deleteFile($row['banner_img'], 'banner'); // Supprime l'image de profil
        }

        if($replace_with_null){
            mysqli_query($connexion, "UPDATE Users SET banner_img=NULL WHERE id='$idUsr';");
        }
    }
}

function deleteUser(int $idUsr, $has_to_delete_shitstorms = false, bool $must_delete_user = true) : bool { // Supprime un utilisateur, ses shitstorms, ses réponses, ses commentaires. Définitif !
    global $connexion;
    if($idUsr > 0){
        $res = mysqli_query($connexion, "SELECT idSub FROM Shitstorms WHERE idUsr='$idUsr';");
        if($res){
            $i = 0;
            while($row = mysqli_fetch_assoc($res)){
                $i++;
                if($has_to_delete_shitstorms){
                    deleteShitstorm($row['idSub']); // Supprime chaque SS de l'utilisateur
                }
                else{
                    anonymiseShitstorm($row['idSub']); // Supprime le pseudo de l'utilisateur et son id de la shitstorm
                }
            }
        }

        $res = mysqli_query($connexion, "SELECT idSub FROM Waiting WHERE idUsr='$idUsr';");
        while($row = mysqli_fetch_assoc($res)){
            deleteLinks(true, $row['idSub']); // Supprime chaque SS en attente de l'utilisateur
        }
        $tmp = mysqli_query($connexion, "DELETE FROM Waiting WHERE idUsr='$idUsr';");

        $res = mysqli_query($connexion, "SELECT idSubA FROM WaitAnsw WHERE idUsrA='$idUsr';");
        while($row = mysqli_fetch_assoc($res)){
            deleteOneAnswer(true, $row['idSubA']); // Supprime chaque réponse en attente de l'utilisateur
        }

        $res2 = mysqli_query($connexion, "SELECT idCom FROM Comment WHERE idUsr='$idUsr';");
        if($res2){
            while($row = mysqli_fetch_assoc($res2)){
                deleteCom($row['idCom']); // Supprime chaque commentaire de l'utilisateur
            }
        }
        $res9 = mysqli_query($connexion, "DELETE FROM ComLikes WHERE idUsr='$idUsr';"); // Suppression des possibles likes laissés sur les commentaires

        $res3 = mysqli_query($connexion, "SELECT idAn FROM Answers WHERE idUsrA='$idUsr';");
        if($res3){
            while($row = mysqli_fetch_assoc($res3)){
                deleteOneAnswer(false, $row['idAn']); // Supprime chaque réponse de l'utilisateur
            }
        }
        $res4 = mysqli_query($connexion, "DELETE FROM ShitLikes WHERE idUsr='$idUsr';"); // Suppression des possibles votes laissés sur les shitstorms
        generalLikesActualisation(); // Actualisation des likes

        $res7 = mysqli_query($connexion, "DELETE FROM Followings WHERE idFollowed='$idUsr' OR idUsr='$idUsr';");
        // Supprime tous les follows des gens qui ont suivi cet utilisateur (idFollowed) et tous les follows de cet utilisateur (idUsr)

        $res8 = mysqli_query($connexion, "DELETE FROM Notifications WHERE idDest='$idUsr' OR idUsr='$idUsr';");
        // Supprime toutes les notifications émises (idUsr) et reçues de cet utilisateur (idDest)

        $res8 = mysqli_query($connexion, "DELETE FROM ShitFollowings WHERE idFollower='$idUsr';");
        // Supprime tous les follows de shitstorm de l'utilisateur en question

        deleteProfileImg($idUsr, !$must_delete_user);
        // Supprime l'image de profil de l'utilisateur si il en avait une

        deleteBannerImg($idUsr, !$must_delete_user);
        // Supprime l'image de bannière de l'utilisateur si il en avait une

        // Suppression de l'utilisateur
        if($must_delete_user){
            $res5 = mysqli_query($connexion, "DELETE FROM Users WHERE id='$idUsr';");
        }
        return true;        
    }
    return false;
}

function resetUser(int $idUsr, bool $deleteShitstorms = false) : bool {
    return deleteUser($idUsr, $deleteShitstorms, false);
}
