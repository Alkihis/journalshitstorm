<?php

const COUNT_LIMIT_PER_PAGE = 5;

const MAX_LEN_DSCR = 1500;

const MAX_LEN_ANSWER_DSCR = 700;

const MAX_LEN_USERNAME = 16;

const MAX_LEN_REALNAME = 32;

const MAX_LEN_EMAIL = 128;

const MAX_LEN_TITLE = 50;

const MAX_LEN_COMMENT = 700;

const MAX_ANSWER_COUNT = 3; // 3 réponses max par shitstorm

const MAX_LINK_COUNT = 5; // 5 liens max par shitstorm

const AUTHORIZE_CONTENT_MODIFICATION = true; // Autorise le posteur de la shitstorm (non modérateur à modifier description et date

const DEFAULT_CARD_IMG = 'https://shitstorm.fr/img/twiimg.png'; // Image par défaut pour la card Twitter

const DEFAULT_PROFILE_IMG = 'img/default.png'; // Image de profil par défaut

const REGEX_TWITTER_LINK = '/^https?:\/\/twitter.com\b([-a-zA-Z0-9@:%_\+~#?&\/=]*)\/status\/([0-9]+)(\?.+)?$/';

const REGEX_MASTODON_LINK = '/^https?:\/\/(.+)\.(.+)\/@[a-zA-Z_-]+\/([0-9]+)(\?.+)?$/';
// Instance récupérée avec $1.$2, ID du toot avec $3

const REGEX_WEB_LINK = '/^https?:\/\/(.+)\.(.+)(\?.+)?$/';

const REGEX_IMG_LINK = '/^img\/(uploaded)|(preview)\/.+$/';

const REGEX_IMG_LINK_UPLOADED = '/^img\/uploaded\/.+$/';

const REGEX_IMG_PROFILE = "/^img\/profile\/.+$/";

const REGEX_IMG_BANNER = "/^img\/banner\/.+$/";

const REGEX_MAIL = "/^[a-z0-9._+-]+@[a-z0-9._-]{2,}\.[a-z]{2,6}$/i";

const REGEX_DATE = "/^[0-9]{4}-[0-9]{2}-[0-9]{2}/";

const REGEX_PASSWORD = "/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/";

const REGEX_USERNAME = '/^[a-z][a-z0-9_-]{2,15}$/i';

const REGEX_REALNAME = '/^[^:\\\'"<>]{1,32}$/i';

const REGEX_CITATION_USERNAME = '/@([a-z0-9_-]{3,16})\b/i';

const REGEX_BLOCKQUOTE = '/<p.+ dir=\\"..."\\>(.+)<\/p>(.+)<\/blockquote>/';

// Directories

define('ROOT', $_SERVER['DOCUMENT_ROOT']);

define('STATIC_DIR', ROOT . '/static/');

define('INC_DIR', ROOT . '/inc/');

define('API_DIR', ROOT . '/api/');

define('API_ENDPOINTS_DIR', ROOT . '/api/v2/endpoints/');

define('API_RESERVED_DIR', ROOT . '/api/v2/reserved/');

define('API_JWT_VENDOR', ROOT . '/api/v2/endpoints/auth/vendor/');

define('PAGES_DIR', ROOT . '/php/');

define('IMG_DIR', ROOT . '/img/');

define('PAGE403', PAGES_DIR . 'info/403.php');

define('PAGE404', PAGES_DIR . 'info/404.php');

// End of directories

const HTTP_URL = 'http://shitstorm.fr/';

const HTTPS_URL = 'https://shitstorm.fr/';

const DOT_DOMAIN = '.shitstorm.fr';

// Timezone
date_default_timezone_set('Europe/Paris');

// Log errors
const ENABLE_LOG = false;
