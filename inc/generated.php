<?php
// ----------------------
// GENERATIONS GENERIQUES
// ----------------------

function viewLink(string $html, $id, $linkID = null, bool $is_answer = false) : void { // Affiche le lien sous la bonne forme : soit fetch Twitter pour obtenir le blockquote, soit echo le blockquote, soit affiche l'image / Return void
    $matches = array();
    if(preg_match(REGEX_TWITTER_LINK, $html, $matches)){
        $id_str = $matches[2];
        echo "<span class='twitter-link' id='pub{$id}link' data-link-id='$id_str' data-theme=".(isset($GLOBALS['black']) && $GLOBALS['black'] ? '"dark"' : '"light"').
              ($linkID ? " data-check='$linkID'" : '')." data-pub-id='pub$id'>";
        echo "</span>";
    }
    else if(preg_match(REGEX_MASTODON_LINK, $html, $matches)){
        $instance = $matches[1] . '.' . $matches[2];
        $id_str = $matches[3];
        echo "<span class='mastodon-link' id='pub{$id}link' data-link-id='$id_str' data-instance='$instance'".
        ($linkID ? " data-check='$linkID'" : '')." data-pub-id='pub$id'>";
        echo "</span>";
    }
    else{
        transformLinkToAOrImg($html, $id);
    }
}

function transformLinkToAOrImg(string $link, $id = null) : void { // Transforme le lien soit en balise img, soit en lien standard / Return void
    if(preg_match(REGEX_IMG_LINK, $link)){
        echo "<a class='fancy-boxed' href='".HTTPS_URL."$link' data-fancybox>
                <img class='responsive-img tweet-image' src='".HTTPS_URL."$link'>
            </a>";
        echo "<div class='hide-on-small-only col m2'></div>";
        echo "<div class='clearb'></div>";
    }
    else{
        echo "<a target='_blank' href='$link'>Lien ou profil en relation avec la shitstorm</a>";
    }
}

// ------------------------------------------------
// GENERATION DE CARD DE SHITSTORM POUR QUERYANSWER
// ------------------------------------------------

function generateCard($row){ // Génère une card pour queryAnswer
    global $connexion;

    $linksres = mysqli_query($connexion, "SELECT * FROM Links WHERE idSub='{$row['idSub']}' ORDER BY idLink ASC LIMIT 0,1;");
    if(!$linksres || mysqli_num_rows($linksres) == 0){
        echo "<h5 class='empty'>Impossible de charger les liens correspondants à la shitstorm. Veuillez recharger la page.</h5>";
        return NULL;
    }
    $linkrow = mysqli_fetch_assoc($linksres);
    
    ?>
    <div class="card card-border" style="padding-top: .5em;">
        <div class="card-image">
            <div class='link center'><?php 
                viewLink($linkrow['link'], $row['idSub']."l", $linkrow['idLink']);
            ?></div>
        </div>
        <div class="card-content">
            <?php 
                echo "<p style='margin-bottom: 3em;'> " . detectTransformLink(htmlentities($row['dscr'])) . "</p>";
            ?>
            <div class="row card-action">
                <p style="clear: both; float: left;"><a class='notMargin' href='<?= HTTPS_URL ?>shitstorm/<?= $row['idSub'] ?>'><?= formatDate($row['dateShit'], 0)."</a>";
                echo "</p>";
                if($row['idUsr'] > 0){
                    echo "<p style='font-style: italic; float: right;'>Ajout&eacute;e par <a class='notCard' href='".HTTPS_URL."profil/{$row['idUsr']}'>{$row['pseudo']}</a> le ". formatDate($row['approvalDate'], 1) . '.</p>';
                }
                else{
                    echo "<p style='font-style: italic; float: right;'>Ajout&eacute;e par {$row['pseudo']} le ". formatDate($row['approvalDate'], 1) . '.</p>';
                }?>
            </div>
        </div>
    </div> <?php
}

// -----------------------------
// GENERATION DE CARD DE REPONSE
// -----------------------------

function genereReponses($linkAnswer, $row = null){ // Joindre le résultat du query valide OU un tableau de rows
    $i = 1;
    $j = 0;

    $nbAnswer = ($row ? count($row) : mysqli_num_rows($linkAnswer));
    $answer = null;

    if(!$row){
        $answer = mysqli_fetch_assoc($linkAnswer);
    }
    else{
        $answer = &$row[0];
    }

    while($answer){ ?>
        <div class="card card-border" style="padding-top: .5em;">
            <div class="card-image">
                <p><h5 style="text-align: center;"><?php echo ($nbAnswer > 1 ? transformeIeme($i).' r' : 'R') ?>éponse de l'initiateur de la shitstorm</h5></p>
                <div class='link center'><?php 
                    viewLink($answer['linkA'], "answ$i", $answer['idAn'], true);
                ?></div>
            </div>
            <div class="card-content">
                <p style="margin-bottom: 1.2em;"><?php echo detectTransformLink(htmlentities($answer['dscrA'])); ?></p>
                <div class="row card-action">
                    <p style="float: left;">Réponse du <?php echo formatDate($answer['dateAn'], 0); ?></p>

                    <?php 
                        if((isset($_SESSION['mod']) && $_SESSION['mod'] > 0) || (isset($_SESSION['id']) && $_SESSION['id'] == $answer['idUsrA'])){
                            ?>
                            <a href='<?= HTTPS_URL ?>modify_an?idAn=<?= $answer['idAn'] ?>'>
                                <button type='button' class='btn-floating waves-effect waves-light teal lighten-1 tooltipped' data-position="bottom" data-delay="50" data-tooltip="Modifier" style='float: right;'>
                                    <i class='material-icons left'>mode_edit</i>
                                </button>
                            </a>
                            <?php
                        }
                    ?>
                </div>
            </div>
        </div><?php
        $i++;
        $j++;

        if(!$row){
            $answer = mysqli_fetch_assoc($linkAnswer);
        }
        else{
            $answer = &$row[$j];
        }
    }
    return true;
}

function generateAnswersForUniqueShit($linkAnswer, $row = null){ // Joindre le résultat du query valide OU un tableau de rows
    $i = 1;
    $j = 0;

    $begin = true;

    $nbAnswer = ($row ? count($row) : mysqli_num_rows($linkAnswer));
    $answer = null;

    if(!$row){
        $answer = mysqli_fetch_assoc($linkAnswer);
    }
    else{
        $answer = &$row[0];
    }

    while($answer){ 
        if(!$begin){
            echo "<div class='divider'></div>";
        }
        else{
            $begin = false;
        }
        
        ?>
        <h4>
            <?= ($nbAnswer > 1 ? transformeIeme($i).' réponse (' : '') ?><?= formatDate($answer['dateAn'], 0) ?><?= ($nbAnswer > 1 ? ')' : '') ?>
            <?php 
            if((isset($_SESSION['mod']) && $_SESSION['mod'] > 0) || (isset($_SESSION['id']) && $_SESSION['id'] == $answer['idUsrA'])){ ?>
                <a href='<?= HTTPS_URL ?>modify_an/<?= $answer['idAn'] ?>'>
                    <button type='button' class='btn-floating waves-effect waves-light teal tooltipped' data-position="bottom" data-delay="50" data-tooltip="Modifier">
                        <i class='material-icons left'>mode_edit</i>
                    </button>
                </a>
            <?php } ?>
        </h4>

        <p class="flow-text black-text" style='margin-bottom: 0'>
            <?= detectTransformLink(preg_replace('/\n/', '<br>', htmlspecialchars($answer['dscrA']))) ?>
        </p>
        <p class='black-text' style='font-size: small; font-style: italic;'>
            Postée par <a href='<?= HTTPS_URL ?>profil/<?= htmlspecialchars($answer['usrname']) ?>'><?= htmlspecialchars($answer['usrname']) ?></a>
        </p>

        <div class='link center'>
            <?php viewLink($answer['linkA'], "answ$i", $answer['idAn'], true); ?>
        </div>
        
        <div class='clearb' style='padding-bottom: 2em;'></div>

        <?php
        $i++;
        $j++;

        if(!$row){
            $answer = mysqli_fetch_assoc($linkAnswer);
        }
        else{
            $answer = &$row[$j];
        }
    }
    return true;
}

// -----------------------------------------------
// GENERATION DE CARD DE SHITSTORM POUR LE JOURNAL
// -----------------------------------------------

function generateShitstormCardFromMysqliRow(&$row, $unique_id = null, $preview = false) : bool { // Génére les cartes pour le journal et l'userview || unique_id est pour préciser un ID supplémentaire à coller sur les ID de boutons
    global $connexion;

    // Initialisation
    $hasLiked = false;
    $hasDisliked = false;
    $nbLikes = '0';
    $hasAnswer = false;
    $answer = NULL;

    if(!$preview){
        // Récup du nombre de commentaires
        $cres = mysqli_query($connexion, "SELECT COUNT(*) c FROM Comment WHERE idSub='{$row['idSub']}';");
        if(!$cres){
            echo "<h5 class='empty'>Erreur : Impossible de charger la shitstorm. Veuillez recharger la page. Num: 0x01</h5>";
            return false;
        }
        $comrow = mysqli_fetch_assoc($cres);
        $countres = $comrow['c'];

        // Récupération du métadata des commentaires si il en existe
        if($countres > 0){
            $cres = mysqli_query($connexion, "SELECT content, contentTime, usrname, idUsr, idCom, profile_img, c.idSub FROM Comment c JOIN Users ON c.idUsr=id WHERE idSub='{$row['idSub']}' ORDER BY idCom DESC LIMIT 0,2;");
            if(!$cres){
                echo "<h5 class='empty'>Erreur : Impossible de charger la shitstorm. Veuillez recharger la page. Num: 0x02</h5>";
                return false;
            }
            $comrow[0] = mysqli_fetch_assoc($cres);
            if($countres > 1)
                $comrow[1] = mysqli_fetch_assoc($cres);
        }

        // Récupération du nombre de likes
        $nbLikes = recupereNbLikes($row['idSub']);
        if(!$nbLikes){
            $nbLikes = '0';
        }

        // Récupération si l'utilisateur courant a like ou dislike
        if(isLogged()){
            $resPresence = mysqli_query($connexion, "SELECT * FROM ShitLikes WHERE idUsr='{$_SESSION['id']}' AND idSub='{$row['idSub']}';");
            if($resPresence && mysqli_num_rows($resPresence) > 0){
                $rowPres = mysqli_fetch_assoc($resPresence);
                $hasLiked = (bool)$rowPres['isLike'];
                $hasDisliked = !$rowPres['isLike'];
            }
        }

        // Récup du premier lien de la shitstorm (les possibles suivants ne sont pas chargés dans le journal)
        $linksres = mysqli_query($connexion, "SELECT *, COUNT(*) c FROM Links WHERE idSub='{$row['idSub']}' ORDER BY idLink ASC LIMIT 0,1;");
        if(!$linksres || mysqli_num_rows($linksres) == 0){
            echo "<h5 class='empty'>Erreur : Impossible de charger la shitstorm. Veuillez recharger la page. Num: 0x03</h5>";
            return false;
        }
        $linkrow = mysqli_fetch_assoc($linksres); // Chargement d'un seul lien

        $nbLinks = $linkrow['c'];

        // Récup de la possible réponse
        $linkAnswer = mysqli_query($connexion, "SELECT * FROM Answers WHERE idSub='{$row['idSub']}' LIMIT 0,1;");
        if($linkAnswer && mysqli_num_rows($linkAnswer) > 0){
            $hasAnswer = true;
        }
    } // end !$preview
    else{ // Paramètres de base d'une card
        $countres = 0;
        $nbLinks = count($row['links']);
        $linkrow['link'] = reset($row['links']);
    }
    
    $shitdetails = "<a class='card-action-refresh tooltipped' data-position='bottom' data-delay='10' data-tooltip='Voir les détails' 
                    href='".HTTPS_URL."shitstorm/{$row['idSub']}'>" . formatDate($row['dateShit'], 0) . '</a><div class="hide-on-med-and-up clearb"></div>';

    $lis = '';
    $detailsOpened = '';
    if($countres > 0){
        $lis .= "
        <li class='activator'>
            <a class='card-action-refresh' style='padding: 0; padding-right: 0.5em' href='#comments{$row['idSub']}'>
                <i class='material-icons tooltipped' data-position='bottom' data-delay='10' data-tooltip='Cette shitstorm a $countres commentaire".
                ($countres > 1 ? 's' : '')."'>question_answer</i>
            </a>
        </li>";
    }
    if($hasAnswer){
        $lis .= "
        <li>
            <a class='card-action-refresh' style='padding: 0; padding-right: 0.5em' href='".HTTPS_URL."shitstorm/{$row['idSub']}'>
                <i class='material-icons tooltipped' data-position='bottom' data-delay='10' data-tooltip='L&apos;initiateur de la shitstorm a répondu'>announcement</i>
            </a>
        </li>";
        $detailsOpened .= "L'initiateur de la shitstorm a répondu.<br>";
    }
    if($nbLinks > 1){
        $lis .= "
        <li>
            <a class='card-action-refresh' style='padding: 0; padding-right: 0.5em' href='".HTTPS_URL."shitstorm/{$row['idSub']}'>
                <i class='material-icons tooltipped' data-position='bottom' data-delay='10' data-tooltip='Il y a $nbLinks liens dans cette shitstorm'>".($nbLinks == 2 ? 'looks_two' : "looks_$nbLinks")."</i>
            </a>
        </li>";
        $detailsOpened .= "Il y a $nbLinks liens dans cette shitstorm.<br>";
    }
    $lis .= "</ul>";

     // AFFICHAGE DES CARDS
     // -------------------
    ?>
    <div class="row">
        <div class="col s12">
            <div class="card sticky-action hoverable card-border <?= (isset($_SESSION['storm']) && $_SESSION['storm'] ? 'storm-card' : '') ?>"
                 style="padding-top: .5em;"> 
                <div class="card-image activator" id="pub<?= $row['idSub'].$unique_id ?>">
                    <div class='link center activator'>
                    <?php 
                        viewLink($linkrow['link'], $row['idSub'].$unique_id, $linkrow['idLink']);
                    ?>
                    </div>
                    
                </div>
                <div class='card-content' style='padding-bottom: 2.2em !important;'>
                    <!-- Informations visibles -->
                    <span class="card-title activator grey-text text-darken-4 "><?= htmlentities($row['title']) ?><i class="material-icons right">more_vert</i></span>
                    <div class='clearb'></div>
                    <?php
                    if($detailsOpened){
                        echo '<div class="left black-text" style="font-style: italic; font-size: small;">'.$detailsOpened.'</div>';
                        echo "<div class='right'><a class='notMargin' href='".HTTPS_URL."shitstorm/{$row['idSub']}'>Afficher les détails</a></div>";
                        echo '<div class="clearb"></div>';
                    } ?>
                </div>

                <div class="card-action card-border">
                    <!-- Informations du footer -->
                    <ul class='pagination' style='margin-top :0 !important;'>
                        <li class='left'>
                            <?= $shitdetails ?>
                        </li>
                        <div class='right'>
                            <ul class='pagination'>
                                <?= '<ul class="left">' . $lis ?>
                                <?php if(isset($_SESSION['id']) && isset($_SESSION['connected']) && !$preview): ?>
                                    <li class='blue lighten-2 waves-effect white-text tooltipped' data-position="bottom" data-delay="50" data-tooltip="+1" name='favSub' id='like<?= $row['idSub'] ?>' style='border-radius: 5px;'>
                                        <a class='card-action-refresh' name='unfavSub' href="#!">
                                            <i style='color: white' class='material-icons<?= ($hasLiked ? ' liked' : '') ?> like-button' 
                                                data-shitstorm-id='<?= $row['idSub'] ?>' >keyboard_arrow_up</i>
                                        </a>
                                    </li>

                                    <li class='red lighten-3 waves-effect white-text tooltipped' data-position="bottom" data-delay="50" data-tooltip="-1" style='border-radius: 5px;'>
                                        <a class='card-action-refresh' name='unfavSub' href="#!">
                                            <i style='color: white' class='material-icons<?= ($hasDisliked ? ' disliked' : '') ?> dislike-button'
                                                data-shitstorm-id='<?= $row['idSub'] ?>'>keyboard_arrow_down</i>    
                                        </a>
                                    </li>
                                    <?php
                                endif; ?>
                                
                                <li class='waves-effect orange darken-5 tooltipped' data-position="bottom" data-delay="50" data-tooltip="Note" style='border-radius: 12px;'>
                                    <a style='margin-right : 0 !important' class='white-text no-text-transform shitstorm-rates-button' data-shitstorm-id='<?= $row['idSub'] ?>'
                                    id='likecount<?= $row['idSub'].$unique_id ?>' href="#!"><?= $nbLikes ?></a>
                                </li>
                            </ul>
                        </div>
                    </ul>
                    <div class='clearb'></div>
                </div>

                <div class="card-reveal" id='reveal<?= $row['idSub'].$unique_id ?>'>
                    <!-- Informations cachées -->
                    <span class="card-title grey-text text-darken-4"><?= htmlentities($row['title']) ?><i class="material-icons right">close</i></span>

                    <?php 
                    // Description
                    echo "<div style='margin-bottom: 1.2em;'><p>" . detectTransformLink(htmlentities($row['dscr'])) . '</p></div>';
                    if($detailsOpened){
                        $detailsOpened = '<p style="font-style: italic; font-size: small;">'.$detailsOpened."</p>";
                    }
                    echo $detailsOpened;
                    echo "<div class='clearb'></div>"
                    ?>
                    <div class="row card-action">
                        <p style="clear: both; float: left;"><a class='notMargin' href='<?= HTTPS_URL ?>shitstorm/<?= $row['idSub'] ?>'>Afficher plus de détails</a><?php
                        echo "</p>";
                        if($row['idUsr'] > 0){
                            echo "<p style='font-style: italic; float: right;'>Ajout&eacute;e par <a class='notCard' href='".HTTPS_URL."profil/{$row['idUsr']}'>{$row['pseudo']}</a> le ". formatDate($row['approvalDate'], 1) . '.</p>';
                        }
                        else{
                            echo "<p style='font-style: italic; float: right;'>Ajout&eacute;e par {$row['pseudo']} le ". formatDate($row['approvalDate'], 1) . '.</p>';
                        }

                        // ------------
                        // Commentaires
                        // ------------
                        ?>
                        <div id='comments<?= $row['idSub'].$unique_id ?>' style="padding-top: .5em; clear:both;">
                        <?php 
                            // Affichage des deux premiers commentaires de la publication
                            // ----------------------------------------------------------
                            for($i = 0; $i < $countres && $i < 2; $i++){
                                generateCommentCardFromMysqliRow($comrow[$i], false, 0, $unique_id);
                            }
                            if($countres > 2){
                                ?>
                                <div class='col s12'>
                                    <a href='<?= HTTPS_URL ?>shitstorm/<?= $row['idSub'] ?>#comments_b' class="btn col s12 blue lighten-1" name="plusCom">Plus</a>
                                </div>
                                <?php  
                            }
                            // Champ d'ajout de commentaire
                            // ----------------------------
                            if(isset($_SESSION['connected']) && $_SESSION['connected'] && !$preview){ ?>
                                <div class="input-field col s12 ">
                                    <form method='post' action='shitstorm/<?= $row['idSub'] ?>#comments_b'>
                                        <textarea style="margin-bottom: 0.5em;" id='contentCom<?= $row['idSub'] ?>' name="contentCom" rows="2" cols="10" placeholder="Ecrivez un nouveau commentaire..."
                                            data-idsub='<?= $row['idSub'] ?>' maxlength="<?= strval(MAX_LEN_COMMENT) ?>" class="materialize-textarea comment-textaera" required></textarea>
                                        <div style="padding-bottom: 3em;">
                                            <button class="btn blue lighten-1" type="submit" style="float: right;" name="pubCom">Publier</button>
                                        </div>
                                        <input type='hidden' id='reply_to<?= $row['idSub'] ?>' name='in_reply_to_id' value='0'>
                                    </form>
                                </div>
                            <?php 
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php

    return true;
}

function generateShitstormFlowFromMysqliRow(&$row, $unique_id = null, $preview = false, bool $ignore_description = false, bool $big_title = false) : bool { // Génére le texte pour le journal et l'userview || unique_id est pour préciser un ID supplémentaire à coller sur les ID de boutons
    global $connexion;

    // Initialisation
    $hasLiked = false;
    $hasDisliked = false;
    $nbLikes = '0';
    $hasAnswer = false;
    $answer = NULL;

    if(!$preview){
        // Récup du nombre de commentaires
        $cres = mysqli_query($connexion, "SELECT COUNT(*) c FROM Comment WHERE idSub='{$row['idSub']}';");
        if(!$cres){
            echo "<h5 class='empty'>Erreur : Impossible de charger la shitstorm. Veuillez recharger la page. Num: 0x01</h5>";
            return false;
        }
        $comrow = mysqli_fetch_assoc($cres);
        $countres = $comrow['c'];

        // Récupération du nombre de likes
        $nbLikes = recupereNbLikes($row['idSub']);
        if(!$nbLikes){
            $nbLikes = '0';
        }

        // Récupération si l'utilisateur courant a like ou dislike
        if(isset($_SESSION['id']) && isset($_SESSION['connected']) && $_SESSION['connected']){
            $resPresence = mysqli_query($connexion, "SELECT * FROM ShitLikes WHERE idUsr='{$_SESSION['id']}' AND idSub='{$row['idSub']}';");
            if($resPresence && mysqli_num_rows($resPresence) > 0){
                $rowPres = mysqli_fetch_assoc($resPresence);
                $hasLiked = (bool)$rowPres['isLike'];
                $hasDisliked = !$rowPres['isLike'];
            }
        }

        // Récup du premier lien de la shitstorm (les possibles suivants ne sont pas chargés dans le journal)
        $linksres = mysqli_query($connexion, "SELECT *, COUNT(*) c FROM Links WHERE idSub='{$row['idSub']}' ORDER BY idLink ASC LIMIT 0,1;");
        if(!$linksres || mysqli_num_rows($linksres) == 0){
            echo "<h5 class='empty'>Erreur : Impossible de charger la shitstorm. Veuillez recharger la page. Num: 0x03</h5>";
            return false;
        }
        $linkrow = mysqli_fetch_assoc($linksres); // Chargement d'un seul lien

        $nbLinks = $linkrow['c'];

        // Récup de la possible réponse
        $linkAnswer = mysqli_query($connexion, "SELECT * FROM Answers WHERE idSub='{$row['idSub']}' LIMIT 0,1;");
        if($linkAnswer && mysqli_num_rows($linkAnswer) > 0){
            $hasAnswer = true;
        }
    } // end !$preview
    else{ // Paramètres de base d'une card
        $countres = 0;
        $nbLinks = count($row['links']);
        $linkrow['link'] = reset($row['links']);
    }
    
    $shitdetails = "<a class='card-action-refresh tooltipped' data-position='bottom' data-delay='10' data-tooltip='Voir les détails' 
                    href='".HTTPS_URL."shitstorm/{$row['idSub']}'>" . formatDate($row['dateShit'], 0) . '</a><div class="hide-on-med-and-up clearb"></div>';

    $lis = '';
    $detailsOpened = '';
    if($hasAnswer){
        $lis .= "
        <li>
            <a class='card-action-refresh' style='padding: 0; padding-right: 0.5em' href='".HTTPS_URL."shitstorm/{$row['idSub']}'>
                <i class='material-icons tooltipped' data-position='bottom' data-delay='10' data-tooltip='L&apos;initiateur de la shitstorm a répondu'>announcement</i>
            </a>
        </li>";
        $detailsOpened .= "L'initiateur de la shitstorm a répondu.<br>";
    }

    $lis .= "
        <li>
            <a class='card-action-refresh' style='padding: 0; padding-right: 0.5em' href='".HTTPS_URL."shitstorm/{$row['idSub']}'>
                <i class='material-icons tooltipped' data-position='bottom' data-delay='10' data-tooltip='Il y a $nbLinks lien".($nbLinks > 1 ? 's' : '')." dans cette shitstorm'>".
                    ($nbLinks <= 2 ? ($nbLinks == 1 ? 'looks_one' : 'looks_two') : "looks_$nbLinks")."</i>
            </a>
        </li>";
    $detailsOpened .= "Il y a $nbLinks lien".($nbLinks > 1 ? 's' : '')." dans cette shitstorm.<br>";
    
    $lis .= "</ul>";

     // AFFICHAGE DU TEXTE : dans une section + row container
     // -----------------------------------------------------

    $uniqIdSub = (string)time() . $row['idSub'];
    ?>
    <div id="pub<?= $row['idSub'].$unique_id ?>" class='<?= (isset($_SESSION['storm']) && $_SESSION['storm'] ? 'storm-card' : '') ?>'>
        <h<?= ($big_title ? '2' : '3') ?> class="header">
            <a class='underline-hover shitstorm-title card-pointer' data-shitstorm-id='<?= $row['idSub'] ?>' 
            onclick='loadShitstormInContainer(this.dataset.shitstormId);'><?= htmlentities($row['title']) ?></a>
        </h<?= ($big_title ? '2' : '3') ?>>

        <?php if(!$ignore_description) { ?>
            <p class='flow-text black-text card-pointer' title='Afficher/masquer lien' onclick='foldShitstorm("<?= $uniqIdSub ?>")'>
                <?= detectTransformLink(htmlentities($row['dscr'])) ?>
            </p>
        <?php } ?>

        <p class='flow-text center card-pointer' title='Afficher/masquer lien' onclick='foldShitstorm("<?= $uniqIdSub ?>")'>
            <i data-shitstorm-id="<?= $uniqIdSub ?>" class='link-arrow material-icons'>keyboard_arrow_down</i>
        </p>

        <div style='display: none;' data-shitstorm-id='<?= $uniqIdSub ?>' class='link center hidden-link'>
            <?php viewLink($linkrow['link'], $row['idSub'].$unique_id, $linkrow['idLink']); ?>
        </div>

        <div class='black-text' style='font-size: small;'>
            <?= $detailsOpened ?>
        </div>
        <div><a class='notMargin' href='<?= HTTPS_URL ?>shitstorm/<?= $row['idSub'] ?>'>Afficher les détails</a></div>

        <p class='black-text' style='font-size: small; font-style: italic;'>
            Postée par
            <?php if($row['idUsr'] > 0) { ?>
                <a href='<?= HTTPS_URL ?>profil/<?= $row['idUsr'] ?>'><?= htmlspecialchars($row['pseudo']) ?></a>
            <?php }
            else {
                echo htmlspecialchars($row['pseudo']);
            } ?>
            le <?= formatDate($row['approvalDate'], 0) ?>
        </p>

        <!-- Informations du footer -->
        <ul class='pagination' style='margin-top :0 !important;'>
            <li class='left'>
                <?= $shitdetails ?>
            </li>
            <div class='right'>
                <ul class='pagination'>
                    <?= '<ul class="left">' . $lis ?>
                    <?php if(isset($_SESSION['id']) && isset($_SESSION['connected']) && !$preview): ?>
                        <li class='blue lighten-2 waves-effect white-text tooltipped' data-position="bottom" data-delay="50" data-tooltip="+1" name='favSub' id='like<?= $row['idSub'] ?>' style='border-radius: 5px;'>
                            <a class='card-action-refresh' name='unfavSub' id='like<?= $row['idSub'].$unique_id ?>' href="#!">
                                <i style='color: white' class='material-icons<?= ($hasLiked ? ' liked' : '') ?> like-button' data-shitstorm-id='<?= $row['idSub'] ?>'
                                    id='blike<?= $row['idSub'].$unique_id ?>'>keyboard_arrow_up</i>
                            </a>
                        </li>

                        <li class='red lighten-3 waves-effect white-text tooltipped' data-position="bottom" data-delay="50" data-tooltip="-1" style='border-radius: 5px;'>
                            <a class='card-action-refresh' name='unfavSub' id='dlike<?= $row['idSub'].$unique_id ?>' href="#!">
                                <i style='color: white' class='material-icons<?= ($hasDisliked ? ' disliked' : '') ?> dislike-button' data-shitstorm-id='<?= $row['idSub'] ?>'
                                    id='dislike<?= $row['idSub'].$unique_id ?>'>keyboard_arrow_down</i>    
                            </a>
                        </li>
                        <?php
                    endif; ?>
                    
                    <li class='waves-effect orange darken-5 tooltipped' data-position="bottom" data-delay="50" data-tooltip="Note" style='border-radius: 12px;'>
                        <a style='margin-right : 0 !important' class='white-text no-text-transform shitstorm-rates-button' data-shitstorm-id='<?= $row['idSub'] ?>'
                        id='likecount<?= $row['idSub'].$unique_id ?>' href="#!"><?= $nbLikes ?></a>
                    </li>
                </ul>
            </div>
        </ul>
    </div>

    <div class='clearb' style='padding-bottom: 20px;'></div> 

    <?php 
    return true;
}

// --------------------------------------------------------
// GENERATION DE BOUTONS PREC/SUIV POUR LE JOURNAL PAR MOIS
// --------------------------------------------------------

function generateNextPreviousByMonth(&$p, $month, $year, bool $by_year = false, bool $hidden_nav = false){
    $prec_month_text = ($by_year ? '' : getTextualMonth($p['prec_month']));
    $next_month_text = ($by_year ? '' : getTextualMonth($p['next_month']));

    $month_year_text = ($by_year ? $year : "$month $year");
    ?>
    <!-- Boutons suivant/précédent --> 
    <div class='navigation-journal' <?= ($hidden_nav ? 'style="display: none;"' : '') ?> class='row'>
        <ul class="pagination center">
            <li style='vertical-align: middle;' class="prec-journal-button waves-effect <?= ($p['prec_month'] ? 'tooltipped' : 'disabled') ?>" 
                data-position="bottom" data-delay="10" data-tooltip="<?= $prec_month_text." {$p['prec_year']}" ?>">
                <a class="prec-journal-link" href="<?= ($p['prec_month'] ? HTTPS_URL."journal/{$p['prec_year']}-{$p['prec_month']}".($by_year ? '/force_year' : '') : '#!' ) ?>">
                    <i class="material-icons">chevron_left</i>
                </a>
            </li>
            <li class='current-journal-text pagePresentation'><?= "$month_year_text" ?></li>
            
            <li style='vertical-align: middle;' class="next-journal-button waves-effect <?= ($p['next_month'] ? 'tooltipped' : 'disabled') ?>" 
                data-position="bottom" data-delay="10" data-tooltip="<?= $next_month_text." {$p['next_year']}" ?>">
                <a class="next-journal-link" href="<?= ($p['next_month'] ? HTTPS_URL."journal/{$p['next_year']}-{$p['next_month']}".($by_year ? '/force_year' : '') : '#!' ) ?>">
                    <i class="material-icons">chevron_right</i>
                </a>
            </li>
        </ul>
    </div>
    <div class='clearb'></div> <?php
}

// --------------------------------
// GENERATION DE CARD D'UTILISATEUR
// --------------------------------

function generateUserCard(&$rowuser, $followButton = false, $suf, $suf2, $is_followed, $mine, $follows_you){
    ?>
    <div class="row">
        <div class="col s12 m8 offset-m2">
            <div class="card card-border hoverable card-pointer">
                <div class="card-content" onclick='location.href="<?= HTTPS_URL . 'profil/' . $rowuser['usrname'] ?>"'>
                    <img id='profile_img' class='right' width="75" height='75' style='border-radius: 5px;' src="<?= HTTPS_URL.$rowuser['profile_img'] ?>">
                    <h5><a href='<?= HTTPS_URL ?>profil/<?= $rowuser['id'] ?>'><?= htmlentities($rowuser['realname']) ?></a></h5>
                    <div class='black-text' style='margin-left: 1em;'><?= $rowuser['usrname'] ?></div>
                    <div class='black-text' style='margin-left: 1em;'>Inscrit<?= $suf2 ?> le <?= formatDate($rowuser['registerdate'], 0) ?></div>
                    <?= ($follows_you ? "<div class='notification-bold-text' style='margin-left: 1em;'>Vous suit</div>" : '') ?>
                    <div class='clearb'></div>

                    <?= ($mine ? "<a href='".HTTPS_URL."compte' class='btn-floating tooltipped orange lighten-1 right profile_button'
                        data-position='bottom' data-delay='10' data-tooltip='Mon compte'>
                            <i class='material-icons'>settings</i>
                        </a>" : '') ?>

                    <?= ($rowuser['storm'] ? "<button class='btn-floating tooltipped purple darken-2 right profile_button'
                        data-position='bottom' data-delay='10' data-tooltip='Des ÉCLAIRS'>
                            <i class='material-icons'>flash_on</i>
                        </button>" : '') ?>

                    <?= ($rowuser['snow'] ? "<button class='btn-floating tooltipped blue lighten-3 right profile_button'
                        data-position='bottom' data-delay='10' data-tooltip='Neige !'>
                            <i class='material-icons'>ac_unit</i>
                        </button>" : '') ?>

                    <button class='btn-floating tooltipped cyan white-text darken-<?= ($rowuser['moderator'] ? ($rowuser['moderator'] == 1 ? '2' : '3') : '1') ?> right profile_button'
                    data-position='bottom' data-delay='10' data-tooltip='<?= ($rowuser['moderator'] ? ($rowuser['moderator'] == 1 ? "Modérat$suf" : "Administrat$suf") : "Utilisat$suf") ?>'>
                        <i class='material-icons'><?= ($rowuser['moderator'] ? ($rowuser['moderator'] == 1 ? 'group' : 'supervisor_account') : 'person') ?></i>
                    </button>
                    <?php if($followButton && !$mine){ ?>
                        <button id='follow_user_button<?= $rowuser['id'] ?>' class='btn-floating <?= $is_followed ? 'green' : 'grey' ?> right profile_button dropdown-button'
                         data-activates='follow_user_drop<?= $rowuser['id'] ?>'>
                            <i id='follow_user_bell<?= $rowuser['id'] ?>' class='material-icons'><?= $is_followed ? 'notifications' : 'notifications_off' ?></i>
                        </button>
                    <?php } ?>
                    <div class='clearb' style='padding-bottom: 30px;'></div>
                </div>
            </div>
        </div>
    </div> 
    <?php
}

function generateUserFlow(&$rowuser, $followButton = false, $big = false, $card = true){
    global $connexion;

    $current_id = (isset($_SESSION['id']) ? $_SESSION['id'] : 0);
    $mine = null;

    $is_followed = false;
    $with_mail = false;
    $follows_you = false;

    ($current_id == $rowuser['id'] ? $mine = true : $mine = false);

    if((isset($rowuser['followable']) && !$rowuser['followable']) || !isset($_SESSION['id'])){
        // Si l'utilisateur n'est pas suivable ou si l'utilisateur n'est pas connecté
        $followButton = false;
    }

    // Si on est autorisé à suivre l'utilisateur, que ce n'est pas nous, et que l'on est connecté (current_id)
    if($followButton && $current_id && !$mine){
        // Recherche si on suit l'utilisateur
        $resF = mysqli_query($connexion, "SELECT idFollow, acceptMail FROM Followings WHERE idUsr='$current_id' AND idFollowed='{$rowuser['id']}' LIMIT 0,1;");

        $is_followed = (int)($resF && mysqli_num_rows($resF) > 0);
        if($is_followed){
            $row = mysqli_fetch_assoc($resF);
            $with_mail = (bool)$row['acceptMail'];
        }
    }

    // Si on est connecté et que ce n'est pas nous, on regarde si il nous suit
    if($current_id && !$mine) {
        $resF = mysqli_query($connexion, "SELECT idUsr FROM Followings WHERE idFollowed='$current_id' AND idUsr='{$rowuser['id']}' LIMIT 0,1;");

        $follows_you = (int)($resF && mysqli_num_rows($resF) > 0);
    }

    $suf = ($rowuser['genre'] == 'm' ? 'eur' : ($rowuser['genre'] == 'f' ? 'rice' : ($rowuser['genre'] == 'b' ? 'eur.ice' : 'eur')));
    $suf2 = ($rowuser['genre'] == 'm' ? '' : ($rowuser['genre'] == 'f' ? 'e' : ($rowuser['genre'] == 'b' ? '.e' : '')));

    if($followButton && !$mine){ ?>
        <ul id='follow_user_drop<?= $rowuser['id'] ?>' class='dropdown-content' data-realname='<?= htmlspecialchars($rowuser['realname'], ENT_QUOTES) ?>'>
            <li>
                <span>
                    <input class='follow-activate follow-checkbox' data-userid='<?= $rowuser['id'] ?>' type='checkbox' id='followed<?= $rowuser['id'] ?>' <?= ($is_followed ? 'checked' : '') ?>>
                    <label class='black-text' for="followed<?= $rowuser['id'] ?>"><?= $is_followed ? 'Suivi' : 'Suivre' ?></label>
                </span>
            </li>
            <li>
                <span>
                    <input class='follow-activate email-checkbox' data-userid='<?= $rowuser['id'] ?>' type='checkbox' id='followed_with_mail<?= $rowuser['id'] ?>' <?= ($with_mail ? 'checked' : '') ?>>
                    <label class='black-text' for="followed_with_mail<?= $rowuser['id'] ?>">E-mails</label>
                </span>
            </li>
        </ul>
    <?php } 

    if($big){
        generateBigUserFlow($rowuser, $followButton, $suf, $suf2, $is_followed, $mine, $follows_you);
    }
    else {
        if($card){
            generateUserCard($rowuser, $followButton, $suf, $suf2, $is_followed, $mine, $follows_you);
        }
        else{
            generateStandardUserFlow($rowuser, $followButton, $suf, $suf2, $is_followed, $mine, $follows_you);
        }
    }
}

function generateStandardUserFlow(&$rowuser, $followButton, $suf, $suf2, $is_followed, $mine, $follows_you){
    ?>
    <div>
        <img id='profile_img' class='right' width="75" height='75' style='border-radius: 5px;' src="<?= HTTPS_URL.$rowuser['profile_img'] ?>">
        <h5><a href='<?= HTTPS_URL ?>profil/<?= $rowuser['id'] ?>'><?= htmlentities($rowuser['realname']) ?></a></h5>
        <div class='black-text' style='margin-left: 1em;'><?= $rowuser['usrname'] ?></div>
        <div class='black-text' style='margin-left: 1em;'>Inscrit<?= $suf2 ?> le <?= formatDate($rowuser['registerdate'], 0) ?></div>
        <?= ($follows_you ? "<div class='notification-bold-text' style='margin-left: 1em;'>Vous suit</div>" : '') ?>

        <div class='clearb'></div>
    </div> 

    <div class='buttons-user'>
        <?= ($mine ? "<a href='".HTTPS_URL."compte' class='btn-floating tooltipped orange lighten-1 right profile_button'
            data-position='bottom' data-delay='10' data-tooltip='Mon compte'>
                <i class='material-icons'>settings</i>
            </a>" : '') ?>

        <?= ($rowuser['storm'] ? "<button class='btn-floating tooltipped purple darken-2 right profile_button'
            data-position='bottom' data-delay='10' data-tooltip='Des ÉCLAIRS'>
                <i class='material-icons'>flash_on</i>
            </button>" : '') ?>

        <?= ($rowuser['snow'] ? "<button class='btn-floating tooltipped blue lighten-3 right profile_button'
            data-position='bottom' data-delay='10' data-tooltip='Neige !'>
                <i class='material-icons'>ac_unit</i>
            </button>" : '') ?>

        <button class='btn-floating tooltipped cyan white-text darken-<?= ($rowuser['moderator'] ? ($rowuser['moderator'] == 1 ? '2' : '3') : '1') ?> right profile_button'
        data-position='bottom' data-delay='10' data-tooltip='<?= ($rowuser['moderator'] ? ($rowuser['moderator'] == 1 ? "Modérat$suf" : "Administrat$suf") : "Utilisat$suf") ?>'>
            <i class='material-icons'><?= ($rowuser['moderator'] ? ($rowuser['moderator'] == 1 ? 'group' : 'supervisor_account') : 'person') ?></i>
        </button>
        <?php if($followButton && !$mine){ ?>
            <button id='follow_user_button<?= $rowuser['id'] ?>' class='btn-floating <?= $is_followed ? 'green' : 'grey' ?> right profile_button dropdown-button'
                data-activates='follow_user_drop<?= $rowuser['id'] ?>'>
                <i id='follow_user_bell<?= $rowuser['id'] ?>' class='material-icons'><?= $is_followed ? 'notifications' : 'notifications_off' ?></i>
            </button>
        <?php } ?>
        <div class='clearb'></div>
    </div>
    <?php
}

function generateBigUserFlow(&$rowuser, $followButton, $suf, $suf2, $is_followed, $mine, $follows_you){
    ?>
    <div class='col s12 l5-mini card-user-new border-user user-card-borders'>
    
        <a href='<?= HTTPS_URL . 'profil/' . $rowuser['usrname'] ?>' class='background-full default-color background-user-card block' style="
            <?= ($rowuser['banner_img'] ? "background-image: url('".HTTPS_URL."{$rowuser['banner_img']}');" : '') ?>
        "></a>
        
        <div class='user-card-padding'>
            <a href='<?= HTTPS_URL . 'profil/' . $rowuser['usrname'] ?>'>
                <img id='profile_img' class='user-card-image' src="<?= HTTPS_URL.$rowuser['profile_img'] ?>">
            </a>

            <h5><a class='underline-hover no-text-color' href='<?= HTTPS_URL ?>profil/<?= $rowuser['id'] ?>'><?= htmlentities($rowuser['realname']) ?></a></h5>

            <div class='black-text' style='margin-left: 1em;'>
                <a class='underline-hover no-text-color' href='<?= HTTPS_URL . 'profil/' . $rowuser['usrname'] ?>'>@<?= $rowuser['usrname'] ?></a>
            </div>
            <div class='black-text' style='margin-left: 1em;'>
                Inscrit<?= $suf2 ?> le <?= formatDate($rowuser['registerdate'], 0) ?>
            </div>
            <?= ($follows_you ? "<div class='notification-bold-text' style='margin-left: 1em;'>Vous suit</div>" : '') ?>

            <div class='clearb'></div>
        </div> 

        <div class='buttons-user'>
            <?= ($mine ? "<a href='".HTTPS_URL."compte' class='btn-floating tooltipped orange lighten-1 left profile_button'
                data-position='bottom' data-delay='10' data-tooltip='Mon compte'>
                    <i class='material-icons'>settings</i>
                </a>" : '') ?>

            <?= ($rowuser['storm'] ? "<button class='btn-floating tooltipped purple darken-2 left profile_button'
                data-position='bottom' data-delay='10' data-tooltip='Des ÉCLAIRS'>
                    <i class='material-icons'>flash_on</i>
                </button>" : '') ?>

            <?= ($rowuser['snow'] ? "<button class='btn-floating tooltipped blue lighten-3 left profile_button'
                data-position='bottom' data-delay='10' data-tooltip='Neige !'>
                    <i class='material-icons'>ac_unit</i>
                </button>" : '') ?>

            <button class='btn-floating tooltipped cyan white-text darken-<?= ($rowuser['moderator'] ? ($rowuser['moderator'] == 1 ? '2' : '3') : '1') ?> left profile_button'
            data-position='bottom' data-delay='10' data-tooltip='<?= ($rowuser['moderator'] ? ($rowuser['moderator'] == 1 ? "Modérat$suf" : "Administrat$suf") : "Utilisat$suf") ?>'>
                <i class='material-icons'><?= ($rowuser['moderator'] ? ($rowuser['moderator'] == 1 ? 'group' : 'supervisor_account') : 'person') ?></i>
            </button>
            <?php if($followButton && !$mine){ ?>
                <button id='follow_user_button<?= $rowuser['id'] ?>' class='btn-floating <?= $is_followed ? 'green' : 'grey' ?> left profile_button dropdown-button'
                    data-activates='follow_user_drop<?= $rowuser['id'] ?>'>
                    <i id='follow_user_bell<?= $rowuser['id'] ?>' class='material-icons'><?= $is_followed ? 'notifications' : 'notifications_off' ?></i>
                </button>
            <?php } ?>
            <div class='clearb'></div>
        </div>
    </div>
    <?php
}

// -----------------------------------------------------
// GENERATION DES FOLLOWS/FOLLOWINGS/SHITSTORM DU PROFIL
// -----------------------------------------------------

function generateFollowFromUser(int $idUsr, int $type = 0) : bool { // Renvoie vrai si l'utilisateur a des abonnements/abonnés, faux sinon aucun [type : 0 = abonnés / 1 = abonnements]
    global $connexion;

    $res = null;

    if($type)
        $res = mysqli_query($connexion, "SELECT *
                                            FROM Followings f
                                            WHERE idUsr='$idUsr';");
    else
        $res = mysqli_query($connexion, "SELECT *
                                            FROM Followings
                                            WHERE idFollowed='$idUsr';");
    
    if($res && mysqli_num_rows($res)){
        $begin = true;
        echo "<div class='row container' style='margin-bottom: 60px;'>";

        echo "<div id='fol-users'></div>
        <script>
            $(document).ready(function () {
                let s = document.getElementById('fol-users');
                " . ($type ? "loadFollowings" : "loadFollowers") . "(s, $idUsr);
            });  
        </script>";

        echo "</div>";

        return true;
    }
    else{
        return false;
    }
}

function generateShitstormsFromUser(int $idUsr, int $count = 0, string $order = 'asc', $by_content = false, $by_rate = false) : bool { // Renvoie vrai si l'utilisateur a des shitstorms, faux sinon aucun
    global $connexion;

    $is_previous = false;
    $res = null;
    $usrn = '';
    $gender = null;
    $realn = '';
    $save_content = '';

    $arg_string = '';

    $count = intval(mysqli_real_escape_string($connexion, $count));
    $count = ($count < COUNT_LIMIT_PER_PAGE ? 0 : $count);
    $is_previous = $count >= COUNT_LIMIT_PER_PAGE;

    $arg_string .= "&sort={$order}";
    if($order == 'asc'){
        $order = 'ASC';
    }
    else{
        $order = 'DESC';
    }

    $arg_string .= "&order={$by_rate}";
    if($by_rate == 'rating'){
        $by_rate = true;
    }
    else{
        $by_rate = false;
    }

    if($by_content != ''){
        $arg_string .= "&content_like=".urlencode($by_content);
        $save_content = $by_content;
        $co = mysqli_real_escape_string($connexion, $by_content);
        $co = addcslashes($co, '%_'); 

        $len = mb_strlen($co);
        if($len > 1 && $len <= 50){
            $by_content = $co;
        }
        else{
            $by_content = false;
        }
    }

    $resUser = mysqli_query($connexion, "SELECT id, usrname, realname, genre FROM Users WHERE id='$idUsr';");
    if($resUser && mysqli_num_rows($resUser) > 0){
        $rowuser = mysqli_fetch_assoc($resUser);
        $idUsr = $rowuser['id'];
        $usrn = $rowuser['usrname'];
        $gender = $rowuser['genre'];
        $realn = $rowuser['realname'];
        $res = mysqli_query($connexion, "SELECT * FROM Shitstorms WHERE idUsr={$rowuser['id']}".
        ($by_content ? " AND (dscr LIKE '%$by_content%' OR title LIKE '%$by_content%')" : '').
        " ORDER BY ".($by_rate ? 'nbLikes' : 'dateShit').
        " $order LIMIT $count,".(COUNT_LIMIT_PER_PAGE+1).";");
    }
    else{
        $resUser = NULL;
    }

    // On connait idUsr quoiqu'il arrive
    if(isset($_SESSION['id']) && $_SESSION['id'] == $idUsr){
        // L'utilisateur regarde ses propres shitstorms
        $mine = true;
    }
    
    ?>
    <div class='row'>
        <div class='col s12 l8 offset-l2'>
            <div class='card-new-flow' style="padding-top: .8em;">
                <form method='get' action='#shits'>
                    <select name='order' class='col s5'>
                        <option value='recent'>Date</option>
                        <option value='rating' <?= ($by_rate == 'rating' ? "selected" : '') ?>>Note</option>
                    </select>
                    <select name='sort' class='col s5 offset-s2'>
                        <option value='asc'>Croissante</option>
                        <option value='desc' <?= ($order == 'DESC' ? "selected" : '') ?>>Décroissante</option>
                    </select>
                    <div class='clearb'></div>
                    <div class='col s12'>
                        <div class="input-field">
                            <input type='text' class='col s12 l8' id='co' name='content_like' maxlength='50' <?= ($save_content ? "value='".htmlspecialchars($save_content, ENT_QUOTES)."'" : '') ?>>
                            <label for="co">Contenu ou titre de la shitstorm</label>
                        </div>
                        <button type='submit' name='action' class='btn col s12 l3 offset-l1'><i class="material-icons right">sort</i>Trier</button>
                    </div>
                    <div class='clearb'></div>
                </form>
            </div>
        </div>
    </div>
    
    <?php

    if($res && mysqli_num_rows($res) > 0){
        echo "<div class='subs'>";
        
        if($res){
            $page = 'profil/'.$usrn.'/shitstorms';
            $arg_string .= '#shits';
            $actual_page = $count / COUNT_LIMIT_PER_PAGE + 1;
        
            if(mysqli_num_rows($res) == 0){
                echo "<h4 class='center'>Il n'y a aucune shitstorm.</h4>";
                if($is_previous){
                    generateNewNextPrevious($page, false, true, $count-COUNT_LIMIT_PER_PAGE, 0, $arg_string, $actual_page);
                }
            }
            else{
                $is_next = mysqli_num_rows($res) > COUNT_LIMIT_PER_PAGE;
        
                if($is_previous || $is_next){
                    generateNewNextPrevious($page, $is_next, $is_previous, $count-COUNT_LIMIT_PER_PAGE, $count+COUNT_LIMIT_PER_PAGE, $arg_string, $actual_page);
                }
        
                $j = 0;
                $begin = true;
        
                while(($row = mysqli_fetch_assoc($res)) && $j != COUNT_LIMIT_PER_PAGE){
                    if(!$begin){
                        echo "<div class='divider'></div><div class='clearb'></div>";
                    }
                    else{
                        $begin = false;
                    }
        
                    echo "<div class='section'><div class='row container'>";
        
                    if(!generateShitstormFlowFromMysqliRow($row, null, false, false, true)){
                        echo '<h3 class="error">Une erreur est survenue en chargeant les shitstorms.</h3>';
                    }
        
                    $j++;
                    echo "</div></div>";
                }
                if($is_previous || $is_next){
                    generateNewNextPrevious($page, $is_next, $is_previous, $count-COUNT_LIMIT_PER_PAGE, $count+COUNT_LIMIT_PER_PAGE, $arg_string, $actual_page);
                }
            }
        }
        
        echo "<div class='clearb' style='margin-bottom: 60px;'></div>";
        echo "</div>";

        return true;
    }
    else{
        return false;
    }
}

// ---------------------------------
// GENERATION DE CARD DE COMMENTAIRE
// ---------------------------------

function generateCommentCardFromMysqliRow(&$row, $isForUnique = true, $idSub = 0, $unique_id = NULL){ 
    global $connexion;
    $res = NULL;
    if(isset($_SESSION['id']) && isset($_SESSION['connected']) && $_SESSION['connected']){
        $res = mysqli_query($connexion, "SELECT * FROM ComLikes WHERE idCom='{$row['idCom']}' AND idUsr='{$_SESSION['id']}';");
    }
    
    $resNbCom = mysqli_query($connexion, "SELECT COUNT(*) c FROM ComLikes WHERE idCom='{$row['idCom']}';");

    $hasLiked = false;
    $nbLikes = 0;

    if($res && mysqli_num_rows($res) > 0){
        $hasLiked = true;
    }
    if($resNbCom && mysqli_num_rows($resNbCom) > 0){
        $rowNb = mysqli_fetch_assoc($resNbCom);
        $nbLikes = $rowNb['c'];
    }

    ?>
    <div class='spacer commentaire' style='padding-top: .5em;'>
        <div class="card card-border commentBack">
            <div class="card-content white-text">
                <p style="margin-bottom: 1.5em;" id='commentText<?= $row['idCom'] ?>'><?php echo detectTransformLink(htmlentities($row['content']), true); ?></p>
                <div class="row card-action<?= (!$isForUnique ? ' commentaireJournal' : ' commentaireUnique') ?>">
                    <span class='likeButtons'>
                        <?php
                        if(isset($_SESSION['id']) && isset($_SESSION['connected'])): ?>
                            <button type='button' class='btn-floating blue darken-3 tooltipped' data-position="bottom" data-delay="10" data-tooltip="Répondre"
                                style='float: left; margin-right: 10px;' onclick='replyToCom("<?= $row['usrname'] ?>", <?= $row['idSub'] ?>, <?= $row['idCom'] ?>);' id='reply<?= $row['idCom'].$unique_id ?>'>
                                <i class='material-icons'>reply</i>
                            </button> 
                            <form>
                                <button type='button' class='btn-floating blue lighten-1 tooltipped' data-position="bottom" data-delay="10" data-tooltip="Favori"
                                    style='float: left; margin-right: 10px;' name='favCom' id='fav<?= $row['idCom'].$unique_id ?>'>
                                    <i class='material-icons' id='inner<?= $row['idCom'].$unique_id ?>'><?= ($hasLiked ? 'star' : 'star_border') ?></i>
                                </button> 
                            </form>
                            <script>
                                $("#fav<?= $row['idCom'].$unique_id ?>").click(function(){
                                    like(<?= $row['idCom'] ?>, "<?= $unique_id ?>");
                                });
                            </script>
                            <?php
                        endif;
                        ?>
                    </span>
                    <button class="btn-floating orange darken-5 tooltipped" data-position="bottom" data-delay="50" data-tooltip="Nombre de favs"
                        style="float: left; text-align: center;" id='nb<?= $row['idCom'].$unique_id ?>'
                        onclick='generateViewFavModal("modalphfixed", <?= $row['idCom'] ?>, "<?= $unique_id ?>")'><?= $nbLikes ?>
                    </button>
                    <?php
                    if($isForUnique && ((isset($_SESSION['id']) && $row['idUsr'] == $_SESSION['id']) || (isset($_SESSION['mod']) && $_SESSION['mod'] > 0))){ ?>
                        <button class="btn-floating red lighten-1 tooltipped" data-position="bottom" data-delay="50" data-tooltip="Supprimer" 
                        type="submit" style="float: left !important; margin-left: 10px;" name="delCom"
                        onclick='createDeleteCommentModalUniqueS("modalphbottom", <?= $row['idCom']; ?>, "<?= (isset($_COOKIE['token']) ? md5($_COOKIE['token'] . (int)$row['idCom']) : 0) ?>", <?= $row['idSub'] ?>)'>
                            <i class='material-icons'>delete_forever</i>
                        </button>
                     <?php 
                    } ?>
                    <div style="font-style: italic; text-align: right; padding-top: 0.3em !important;">
                        <div class="chip tooltipped" data-position="bottom" data-delay="10" data-tooltip="Posté le <?= formatDate($row['contentTime'], 1) ?>">
                            <img class='circle' src="<?= HTTPS_URL.$row['profile_img'] ?>"  width='32' height='32'>
                            <?= "<a class='notCardCommentLink' id='linkToProfile{$row['idCom']}' href='".HTTPS_URL."profil/{$row['idUsr']}'>{$row['usrname']}</a>" ?>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>
    <?php
}

// -----------------------------------
// GENERATION DE TWEET POUR UNIQUESHIT
// -----------------------------------

function generateUniqueShitTweetsFromMysqliLinksRes(object &$linksres, bool $classic = false) : void {
    $i = 0;
    $linksTab = [];

    while($linkrow = mysqli_fetch_assoc($linksres)){
        if(!$classic){
            $linkrow['isImg'] = preg_match(REGEX_IMG_LINK_UPLOADED, $linkrow['link']);
            // Vérifie si le lien est une image
            $linksTab[] = $linkrow;
        }
        else{
            viewLink($linkrow['link'], $i, $linkrow['idLink']);
        }

        $i++;
    }

    if(!$classic){
        $j = $i; // On initialise le compteur de tweets restants à afficher
        while($j > 0){
            if($j-2 >= 0 && !$linksTab[$i-$j]['isImg'] && !$linksTab[($i-$j)+1]['isImg']){ // Si il y a au moins deux tweets que ce n'est pas une image ?>
                <div class='col s12 xl6'>
                    <?php viewLink($linksTab[$i-$j]['link'], $i-$j, $linksTab[$i-$j]['idLink']); ?>
                </div>
                <div class='col s12 xl6'>
                    <?php viewLink($linksTab[($i-$j)+1]['link'], ($i-$j)+1, $linksTab[($i-$j)+1]['idLink']); ?>
                </div>
                <div class='clearb'></div>
                <?php
                $j -= 2;
            }
            else{ ?>
                <div class='col s12'>
                    <?php viewLink($linksTab[$i-$j]['link'], $i-$j, $linksTab[$i-$j]['idLink']); ?>
                </div>
                <div class='clearb'></div>
                <?php
                $j--;
            }
        }
    }
}

// ----------------------------------------
// GENERATION DE TWEET DEPUIS LA SAUVEGARDE
// ----------------------------------------

function generateSavedTweet(&$row, $is_answer = false){ // Y ajouter le row d'un saveTweet result
    // Wait for disable
    throw new BadFunctionCallException('generateSavedTweet() is deprecated. Contact @j_shitstorm on twitter.');
}

function generateHTMLTweet(int $id_tweet, bool $context = true, bool $aut_delete = false) { 
    global $connexion;

    $res = mysqli_query($connexion, "SELECT * FROM tweetSave WHERE idTweet='$id_tweet';");
    $idSave = 0;

    if($res && mysqli_num_rows($res)) {
        $row = mysqli_fetch_assoc($res);
        $idSave = $row['idSave'];
    }
    
    if(!$row) {
        return;
    }

    $res = mysqli_query($connexion, "SELECT * FROM tweetSaveImg WHERE idSave='$idSave';");

    $hasImage = false;
    if($res && mysqli_num_rows($res) > 0){
        $hasImage = true;
    }

    $link = getTweetLinkFromInfos($row['infos']);

    ?>
    <div class='card-tweet-deleted'>
        <p class='context-delete-color'>
            <?php if ($context) { ?> 
                Tweet ou compte du posteur probablement supprimé.<br>
                Il se peut que votre bloqueur de publicité empêche l'affichage des tweets.
            <?php } else { ?>
                Tweet sauvegardé le <?= formatDate($row['addDate'], 0) ?>.<br>
                Lien vers le tweet original : <?= detectTransformLink($link) ?>
                <?php if ($aut_delete) { ?>
                    (<a class='delete-save-button card-pointer' data-id='<?= $id_tweet ?>'>Supprimer</a>)
                <?php }
            } ?>
        </p>
        <div class='col s12 divider divider-delete-tweet-top'></div>
        <div class='tweet-content black-text'>
            <?= $row['content'] ?>
        </div>
        <div class='col s12 divider divider-delete-tweet-bottom'></div>
        <?php 
            if($hasImage){
                $nbImages = mysqli_num_rows($res);
                $images = [];
                while($rowImg = mysqli_fetch_assoc($res)){
                    $images[] = $rowImg;
                }

                require_once(ROOT .'/inc/image/img_saved_gen.php');
                afficheImg($images, $nbImages);
            }
        ?>
        <div class='tweet-author black-text'>
            <div class='context-delete-color'>Posté par</div>
            <?= $row['infos'] ?>
        </div>
    </div>
    <div class='clearb'></div>
    <?php
}

// ---------------------------------------------------
// GENERATION DE BOUTON SUIV/PREC POUR LA VUE PAR LIKE
// ---------------------------------------------------

function generateNextPreviousPersonal($page, $is_next, $is_previous, $ancient_view, $next_view, $add_param = NULL){
    echo "<div class='row'>";
        if($is_previous){
            echo "<a href='".HTTPS_URL."$page?count=$ancient_view".($add_param ? $add_param : '')."'>
                    <button type='submit' class='center btn blue waves-effect waves-light previous col m5 s12 l3 offset-l2'><i class='material-icons left'>arrow_back</i>Page précédente</button>
                </a>";
        }
        else{
            echo "<span class='col m5 s12 l3 offset-l2'></span>";
        }
        if($is_next){
            echo "<a href='".HTTPS_URL."$page?count=$next_view".($add_param ? $add_param : '')."'>
                    <button type='submit' class='center btn blue waves-effect waves-light next col m5 s12 l3 offset-m2 offset-l2'>Page suivante<i class='material-icons right'>arrow_forward</i></button>
                </a>";
        }
        else{
            echo "<span class='col m7 s12 l3 offset-l2'></span>";
        }
    echo '</div>';
}

function generateNewNextPrevious($page, $is_next, $is_previous, $ancient_view, $next_view, $add_param = null, int $actual_page = null){
    echo "<div class='row container'>";
        if($is_previous){
            echo "<a class='no-decoration col s2 left white-text no-text-transform' href='".HTTPS_URL."$page?count=$ancient_view".($add_param ? $add_param : '')."'>
                    <i class='material-icons left arrow-button'>keyboard_arrow_left</i>
                </a>";
        }
        else{
            echo "<a class='no-decoration col s2 left mouse-cursor white-text no-text-transform' href='#!'>
                <i class='material-icons left arrow-button disabled'>keyboard_arrow_left</i>
            </a>";
        }

        echo "<span class='col s8 center black-text' style='font-size: 2.8rem;'>" . ($actual_page ? 'Page ' . $actual_page : '') . "</span>";

        if($is_next){
            echo "<a class='no-decoration col s2 right white-text no-text-transform' href='".HTTPS_URL."$page?count=$next_view".($add_param ? $add_param : '')."'>
                    <i class='material-icons right arrow-button'>keyboard_arrow_right</i>
                </a>";
        }
        else{
            echo "<a class='no-decoration col s2 mouse-cursor right white-text no-text-transform' href='#!'>
                <i class='material-icons right arrow-button disabled'>keyboard_arrow_right</i>
            </a>";
        }
    echo '</div>';
}

// ----------------------------
// GENERATION DE PAGES D'ERREUR
// ----------------------------

function _403() : bool {
    include(PAGE403);
    return true;
}

function _404() : bool {
    include(PAGE404);
    return true;
}
