<?php

// ----------------------------------------------
// FONCTIONS GENERALES : BDD, CONNEXION ET COOKIE
// ----------------------------------------------

require INC_DIR . 'user/user_functions.php';
require INC_DIR . 'shitstorm/shitstorm_functions.php';

$_SESSION['connexion'] = NULL;
$connexion = &$_SESSION['connexion'];

// connexion à la BD
function connectBD() : void {
    global $connexion;
    $connexion = mysqli_connect(SERVEUR, UTILISATRICE, MOTDEPASSE, BDD);
    if (mysqli_connect_errno()) {
        printf("Échec de la connexion : %s\n", mysqli_connect_error());
    }
    else 
        mysqli_query($connexion, 'SET NAMES UTF8mb4'); // requete pour avoir les noms en UTF8mb4
}

/**
 * Disconnect mysql database
 * @return void
 */
function deconnectBD() : void {
    global $connexion;
    if($connexion)
        mysqli_close($connexion);
}

function activeDesactiveNightMode() : void {
    if(isset($_SESSION['night_mode'])) {
        setcookie('night_mode', (int)$_SESSION['night_mode'], time() + 60*24*3600, '/', null, false, true);
    }
    else if(isset($_COOKIE['night_mode'])){
        $_SESSION['night_mode'] = $GLOBALS['black'] = (bool)$_COOKIE['night_mode'];
    }

    if(isset($_GET['night_mode']) && $_GET['night_mode'] == 'on'){
        $_SESSION['night_mode'] = true;
        $GLOBALS['black'] = true;

        setcookie('night_mode', 1, time() + 60*24*3600, '/', null, false, true);
    }
    else if(isset($_GET['night_mode']) && $_GET['night_mode'] == 'off'){
        $_SESSION['night_mode'] = false;
        $GLOBALS['black'] = false;

        setcookie('night_mode', 0, time() + 60*24*3600, '/', null, false, true);
    }
}

function loadSharedSettings() : void { // charge les paramètres généraux de la BDD
    global $connexion;

    $res = mysqli_query($connexion, "SELECT * FROM sharedSettings ORDER BY idSet DESC LIMIT 0,1;");

    if($res && mysqli_num_rows($res)){
        $GLOBALS['settings'] = mysqli_fetch_assoc($res);
    }
    else{
        $GLOBALS['settings'] = ['allowPosting' => 0, 'allowUserPosting' => 0, 'allowUnloggedPosting' => 0, 'allowUserModification' => 0, 'allowAllModification' => 0,
                                'allowLogin' => 0, 'allowStayLogged' => 1, 'allowAnswerPosting' => 0, 'allowUserCreation' => 0, 'allowUserLogin' => 0, 'allowWebsiteAccess' => 0];
    }
}

function isModerator() : bool { // Renvoie vrai si l'utilisateur connecté est modérateur
    if(isset($_SESSION['connected'], $_SESSION['mod']) && $_SESSION['connected']){
        return (bool)$_SESSION['mod'];
    }
    return false;
}

function isAdmin() : bool { // Renvoie vrai si l'utilisateur connecté est administrateur
    if(isset($_SESSION['connected'], $_SESSION['mod']) && $_SESSION['connected']){
        return $_SESSION['mod'] > 1;
    }
    return false;
}

function isLogged() : bool { // Renvoie vrai si l'utilisateur courant est connecté
    return isset($_SESSION['connected'], $_SESSION['id']) && $_SESSION['connected'];
}

function isContainerActive() : bool {
    return !(isset($GLOBALS['turn_off_container']) && $GLOBALS['turn_off_container']);
}

function isActive(?string $element) : bool {
    return is_string($element) && ($element === '1' || $element == 't' || $element == 'true');
}

function getMeaningOfStatus(int $status, string $gender = 'm') : string {
    $suf = getUserSuffixTextByGender($gender);
    if($status === 2) {
        return "Administrat$suf";
    }
    else if ($status === 1) {
        return "Modérat$suf";
    }
    else {
        return "Utilisat$suf";
    }
}

// row doit contenir : usrname, realname, moderator, storm, snow, token, id, profile_img, banner_img, confirmedMail, twitter_id, access_token_twitter
function setConnectedSession(&$row) : void {
    $_SESSION['connected'] = true;
    $_SESSION['visitor'] = true;
    $_SESSION['username'] = $row['usrname'];
    $_SESSION['realname'] = $row['realname'];
    // On enregistre l'email uniquement si il est confirmé (considéré invalide sinon)
    $_SESSION['email'] = ($row['mail'] && $row['confirmedMail'] ? $row['mail'] : null);
    $_SESSION['mod'] = $row['moderator'];
    $_SESSION['storm'] = $row['storm'];
    $_SESSION['token'] = $row['token'];
    $_SESSION['snow'] = $row['snow'];
    $_SESSION['id'] = $row['id'];
    $_SESSION['profile_img'] = HTTPS_URL . $row['profile_img'];
    $_SESSION['banner_img'] = ($row['banner_img'] ? (HTTPS_URL . $row['banner_img']) : false);
    $_SESSION['confirmed_mail'] = $row['confirmedMail'];
    $_SESSION['twitter_data'] = [];
    $infos = ($row['access_token_twitter'] ? json_decode($row['access_token_twitter'], true) : null);
    $_SESSION['twitter_data']['screen_name'] = ($infos['screen_name'] ?? null);
    $_SESSION['twitter_data']['id'] = $row['twitter_id'];
    $_SESSION['twitter_data']['access_token'] = ($infos['oauth_token'] ?? null);
    $_SESSION['twitter_data']['access_token_secret'] = ($infos['oauth_token_secret'] ?? null);
}

function setVisitorId() : void { // Set random visitor ID for UNCONNECTED user
    $_SESSION['visitor_id'] = -(mt_rand(1, mt_getrandmax()));
}

// Vérifie si l'utilisateur se connecte / est encore autorisé à se connecter
// Si un résultat mysqli lui est passé, il connectera cet utilisateur SANS poser de questions
function checkConnect($mysqli_result_twitter_connect = null, bool $set_visitor_id = true) : int { 
    global $connexion;

    if(!$GLOBALS['settings']['allowStayLogged']){ // On a pas le droit de rester connecté
        // disconnect();
        return 6;
    }

    // set la variable visiteur automatiquement
    $_SESSION['visitor'] = true;

    if(isset($_POST['confirm']) || $mysqli_result_twitter_connect){ // Au cas où l'utilisateur a demandé à se connecter
        if(!$GLOBALS['settings']['allowLogin']){ // On a pas le droit de se connecter et l'utilisateur n'est pas modérateur
            return 5;
        }

        $psw = $_POST['psw'] ?? null;
        $usrname = $_POST['usrname'] ?? null;
        $remember_me = (bool)($_POST['remember_me'] ?? false);
        $usrname = ($usrname ? mysqli_real_escape_string($connexion, $usrname) : null);
        $res = null;
        $dont_check_password = false;
        if ($mysqli_result_twitter_connect) {
            $res = $mysqli_result_twitter_connect;
            $dont_check_password = true;
            $remember_me = true;
        }
        else {
            $res = mysqli_query($connexion, "SELECT * FROM Users WHERE usrname='$usrname';");
        }
        
        if($res && mysqli_num_rows($res) > 0){
            $res = mysqli_fetch_assoc($res);

            if(!$GLOBALS['settings']['allowUserLogin'] && !$res['moderator']){ // On a pas le droit de se connecter et l'utilisateur n'est pas modérateur
                return 4;
            }

            if($dont_check_password || password_verify($psw, $res['passw'])){
                setConnectedSession($res);

                $_SESSION['night_mode'] = false;
                setcookie('night_mode', (int)$_SESSION['night_mode'], time() + 60*24*3600, '/', null, false, true);

                $pass = true;
                if($remember_me) {
                    do{
                        $token = $res['token'];
                        if($res['token'] == NULL){ // Si jamais l'utilisateur n'a aucun token enregisté, sinon on reprend l'ancien
                            $token = bin2hex(random_bytes(32));
                            // Vérif si il existe dans la BDD
                            $res = mysqli_query($connexion, "SELECT usrname FROM Users WHERE token='$token';");
                            if(mysqli_num_rows($res) > 0){
                                $pass = false;
                            }
                        }
                        
                        if($pass){
                            // Stockage du token dans la BDD
                            $date = gmdate("Y-m-d H:i:s");
                            $res = mysqli_query($connexion, "UPDATE Users SET token='$token', lastlogin='$date', resetToken=NULL WHERE usrname='$usrname';");
    
                            // Enregistrement du cookie
                            setcookie('token', $token, time() + 1600*24*3600, '/', DOT_DOMAIN, true, true);
                        }
                    } while(!$pass);
                }
                else {
                    $_SESSION['dont_remember_me'] = true;
                }
                
                return 1;
            }
            else{
                return 2;
            }
        }
        else{
            return 3;
        }
    }
    elseif((!isset($_SESSION['connected']) || !$_SESSION['connected']) && isset($_COOKIE['token']) && $_COOKIE['token'] != ''){
        // Au cas où un cookie existe, que l'utilisateur n'est pas connecté, et un token est à l'intérieur du cookie
        $token = mysqli_real_escape_string($connexion, $_COOKIE['token']);
        $res = mysqli_query($connexion, "SELECT * FROM Users WHERE token='$token';");

        if($res && mysqli_num_rows($res) > 0){
            // Le token existe
            $row = mysqli_fetch_assoc($res);
            setConnectedSession($row);

            if(isset($_COOKIE['night_mode'])){
                if(isset($_SESSION['night_mode']) && $_COOKIE['night_mode'] != $_SESSION['night_mode']){
                    // Les infos de session sont différentes de celles du cookie, on écrase les infos du cookie
                    $_COOKIE['night_mode'] = (int)$_SESSION['night_mode'];
                }
                else{
                    $_SESSION['night_mode'] = (bool)$_COOKIE['night_mode'];
                }
            }
            else{
                $_SESSION['night_mode'] = false;
            }
            setcookie('night_mode', (int)$_SESSION['night_mode'], time() + 1600*24*3600, '/', null, false, true);

            // Actualisation du lastlogin
            $date = gmdate("Y-m-d H:i:s");
            $res = mysqli_query($connexion, "UPDATE Users SET lastlogin='$date' WHERE usrname='{$row['usrname']}';");

            // Actualisation du cookie pour qu'il dure plus longtemps
            setcookie('token', $token, time() + 1600*24*3600, '/', DOT_DOMAIN, true, true);
        }
    }
    elseif(isset($_SESSION['id']) && isset($_SESSION['connected']) && $_SESSION['connected']){
        // Si l'utilisateur est connecté : vérification des droits
        $res = mysqli_query($connexion, "SELECT * FROM Users WHERE id='{$_SESSION['id']}';");
        if($res && mysqli_num_rows($res) > 0){
            // L'ID et l'utilisateur existe : actualisation des droits
            $row = mysqli_fetch_assoc($res);
            setConnectedSession($row);

            if(isset($_COOKIE['night_mode'])){
                if(isset($_SESSION['night_mode']) && $_COOKIE['night_mode'] != $_SESSION['night_mode']){
                    // Les infos de session sont différentes de celles du cookie, on écrase les infos du cookie
                    $_COOKIE['night_mode'] = (int)$_SESSION['night_mode'];
                }
                else{
                    $_SESSION['night_mode'] = (bool)$_COOKIE['night_mode'];
                }
            }
            else{
                $_SESSION['night_mode'] = false;
            }
            setcookie('night_mode', (int)$_SESSION['night_mode'], time() + 1600*24*3600, '/', null, false, true);

            $token = '';
            $date = gmdate("Y-m-d H:i:s");

            // Actualisation du lastlogin et token si besoin
            if(isset($_COOKIE['token'])){
                $token = mysqli_real_escape_string($connexion, $_COOKIE['token']);
                $res = mysqli_query($connexion, "UPDATE Users SET lastlogin='$date' WHERE usrname='{$row['usrname']}';");
            }
            else if (isset($_SESSION['dont_remember_me']) && $_SESSION['dont_remember_me']) {
                // N'actualise pas le token en cookie
            }
            else {
                if($row['token'] != NULL){
                    $token = $row['token'];
                    $res = mysqli_query($connexion, "UPDATE Users SET lastlogin='$date' WHERE usrname='{$row['usrname']}';");
                }
                else {
                    // Création d'un token
                    $pass = true;
                    do {
                        $token = bin2hex(random_bytes(32));
                        // Vérif si il existe dans la BDD
                        $res = mysqli_query($connexion, "SELECT usrname FROM Users WHERE token='$token';");
                        if(mysqli_num_rows($res) > 0){
                            $pass = false;
                        }
                        
                        if($pass){
                            // Stockage du token dans la BDD
                            $res = mysqli_query($connexion, "UPDATE Users SET token='$token', lastlogin='$date' WHERE id='{$_SESSION['id']}';");
                        }
                    } while(!$pass);
                }
            }

            if($token) {
                // Actualisation du cookie pour qu'il dure plus longtemps
                setcookie('token', $token, time() + 1600*24*3600, '/', DOT_DOMAIN, true, true);
            }   
        }
        else {
            disconnect();
        }
    }


    if(!isLogged() && $set_visitor_id) { // Si la personne est pas log et qu'elle n'a pas d'ID, on lui set un ID
        if(!isset($_SESSION['visitor_id'])) {
            setVisitorId($set_visitor_id);
        }
    }

    return 0;
}

function disconnect(bool $set_visitor_id = true) : void {
    $usrname = '';
    if(isset($_SESSION['username']) && $_SESSION['username']){
        $usrname = $_SESSION['username'];
    }
    $_SESSION = array();

    // Si vous voulez détruire complètement la session, effacez également
    // le cookie de session.
    // Note : cela détruira la session et pas seulement les données de session !
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000,
            $params["path"], $params["domain"],
            $params["secure"], $params["httponly"]
        );
    }
    if(isset($_COOKIE['token'])){ // Suppression du cookie
        global $connexion;
        // Stockage du token dans la BDD
        $date = gmdate("Y-m-d H:i:s");
        $res = mysqli_query($connexion, "UPDATE Users SET lastlogin='$date' WHERE usrname='$usrname';");

        setcookie('token', '', time() - 3600, '/', DOT_DOMAIN);
        setcookie('night_mode', 0, time() - 3600, '/');
        
        // Suppression de la valeur du cookie dans la variable $_COOKIE
        unset($_COOKIE['token']);
        if(isset($_COOKIE['night_mode'])) unset($_COOKIE['night_mode']);
    }
    unset($_SESSION);
    $GLOBALS['black'] = false;

    if($set_visitor_id) {
        $_SESSION['visitor'] = true;
        setVisitorId();
    }
}

function usernameExists($usrn) : bool {
    global $connexion;

    $usrn = mysqli_real_escape_string($connexion, $usrn);

    $res = mysqli_query($connexion, 'SELECT id FROM Users WHERE '.(!is_numeric($usrn) ? "usrname='$usrn';" : "id='$usrn';"));

    if($res && mysqli_num_rows($res) > 0){
        return true;
    }
    return false;
}

// ------------------
// FORMATAGE DU TEXTE
// ------------------

function formatDate($date, $withTime = true, $tiny = false) : string {
    if(!$tiny){
        if($withTime)
            return date('d/m/Y \à H\hi', strtotime($date . ' UTC'));
        else
            return date('d/m/Y', strtotime($date . ' UTC'));
    }
    else{
        if($withTime)
            return date('[d/m/y, H\hi]', strtotime($date . ' UTC'));
        else
            return date('[d/m/y]', strtotime($date . ' UTC'));
    }
}

function getWeekNumber($date){
    try{
        $dateTime = new DateTime($date);
        return $dateTime->format("W");
    } catch (Exception $E) {
        return NULL;
    }
}

function getTextualMonth($date) : string {
    $date = intval($date);
    switch($date){
        case 1:
            return "Janvier";
        case 2:
            return "Février";
        case 3:
            return "Mars";
        case 4:
            return "Avril";
        case 5:
            return "Mai";
        case 6:
            return "Juin";
        case 7:
            return "Juillet";
        case 8:
            return "Août";
        case 9:
            return "Septembre";
        case 10:
            return "Octobre";
        case 11:
            return "Novembre";
        case 12:
            return "Décembre";
        default:
            return "Unknown";
    }
}

function detectTransformLink($content, $isCom = false) : string {
    $content = preg_replace_callback("/https?:\/\/((www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6})\b([-a-zA-Z0-9@:%_\+.~#?&\/=]*)/",
            function ($mat) use ($isCom){ // Fonction anonyme appelée pour remplacer
                return "<a class='linkCard notMargin red-text text-lighten-".($isCom ? '4' : '1')."' target='_blank' href='{$mat[0]}'>{$mat[1]}/...</a>";
            }
        , $content);

    if($isCom){
        global $connexion;
        $content = preg_replace_callback(REGEX_CITATION_USERNAME,
            function ($mat) use ($connexion){ // Fonction anonyme appelée pour remplacer
                $usrname = mysqli_real_escape_string($connexion, $mat[1]);
                $res = mysqli_query($connexion, "SELECT id FROM Users WHERE usrname='$usrname'");

                if($res && mysqli_num_rows($res) > 0){
                    $row = mysqli_fetch_assoc($res);
                    return "<a class='linkCard notMargin arobase blue-text text-lighten-4' href='".HTTPS_URL."profil/{$row['id']}'>{$mat[0]}</a>";
                }

                return $mat[0];
            }
        , $content);
    }

    return $content;
}

function transformeIeme($i) : string {
    switch($i){
        case 1 :
            return "Première";
            break;
        case 2 :
            return "Deuxième";
            break;
        case 3 :
            return "Troisème";
            break;
        case 4 :
            return "Quatrième";
            break;
        case 5 :
            return "Cinquième";
            break;
        default:
            return "Unknown";
    }
}

// --------------------------------------------
// RECUPERATION DE MOIS OU D'ID POUR LE JOURNAL
// --------------------------------------------

function getIDFromPreviousSS(int $idSS, string $actualDateFromShitstorm) : ?int { // Return positive integer if found, NULL otherwise
    $next = NULL;
    global $connexion;

    $res = mysqli_query($connexion, "SELECT * FROM Shitstorms WHERE dateShit BETWEEN '0000-00-00' AND '$actualDateFromShitstorm' ORDER BY dateShit DESC, idSub ASC");

    if($res && mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_assoc($res)){
            if($next){
                return (int)$row['idSub'];
            }
            elseif($row['idSub'] == $idSS || $idSS == 0){
                $next = true;
            }
        }
        return null;
    }
    else return null;
}

function getIDFromNextSS(int $idSS, string $actualDateFromShitstorm) :?int { // Return positive integer if found, NULL otherwise
    $next = NULL;
    global $connexion;

    $res = mysqli_query($connexion, "SELECT * FROM Shitstorms WHERE dateShit BETWEEN '$actualDateFromShitstorm' AND '9999-12-31' ORDER BY dateShit ASC, idSub DESC");

    if($res && mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_assoc($res)){
            if($next){
                return (int)$row['idSub'];
            }
            elseif($row['idSub'] == $idSS || $idSS == 0){
                $next = true;
            }
        }
        return null;
    }
    else return null;
}

function recupereNbLikes(int $idSub) : ?string { // Retourne le nombre de likes enregistré d'une publication (sous forme de chaîne)
    global $connexion;

    $resNbL = mysqli_query($connexion, "SELECT nbLikes FROM Shitstorms WHERE idSub='$idSub';"); // Récupération du nombre de like enregistré
    if($resNbL){
        $row = mysqli_fetch_assoc($resNbL);
        return (string)$row['nbLikes'];
    }
    else return null;
}

function getRealLikes(int $idSub) : ?string { // Retourne le nombre de likes réellement calculé d'une publication (sous forme de chaîne)
    global $connexion;

    $nbDislike = 0;
    $nbLike = 0;

    $resNbDL = mysqli_query($connexion, "SELECT * FROM ShitLikes WHERE idSub='$idSub' AND isLike=0;"); // Récupération du nouveau nombre de disLike
    if($resNbDL){
        $nbDislike = mysqli_num_rows($resNbDL);

        $resNbL = mysqli_query($connexion, "SELECT * FROM ShitLikes WHERE idSub='$idSub' AND isLike=1;"); // Récupération du nouveau nombre de like
        if($resNbL){
            $nbLike = mysqli_num_rows($resNbL);

            return (string)($nbLike - $nbDislike);
        }
        else return null;
    }
    else return null;
}

function actualiseNbLikes(int $idSub) : void { // Actualise likes/dislikes d'une publication
    global $connexion;

    $nbDislike = 0;
    $nbLike = 0;

    $resNbDL = mysqli_query($connexion, "SELECT * FROM ShitLikes WHERE idSub='$idSub' AND isLike=0;"); // Récupération du nouveau nombre de disLike
    if($resNbDL){
        $nbDislike = mysqli_num_rows($resNbDL);

        $resNbL = mysqli_query($connexion, "SELECT * FROM ShitLikes WHERE idSub='$idSub' AND isLike=1;"); // Récupération du nouveau nombre de like
        if($resNbL){
            $nbLike = mysqli_num_rows($resNbL);

            $final = $nbLike - $nbDislike;

            $res = mysqli_query($connexion, "UPDATE Shitstorms SET nbLikes='$final' WHERE idSub='$idSub';");
        }
    }
}

function generalLikesActualisation() : void { // Actualise les likes de toutes les shitstorms
    global $connexion;

    $res = mysqli_query($connexion, "SELECT idSub FROM Shitstorms");

    if($res){
        while($row = mysqli_fetch_assoc($res)){
            actualiseNbLikes($row['idSub']);
        }
    }
}

// ------------------------
// Fonctions additionnelles
// ------------------------

// ---- TWITTER ----

/**
 * @param OAuth $oauth_object
 * @return object|string
 */
function getTwitterUserObjectFromUser(OAuth $oauth_object) {
    try {
        $resource = $oauth_object->fetch("https://api.twitter.com/1.1/account/verify_credentials.json", [], 'GET');
    } catch (OAuthException $e) {
        return $e->lastResponse;
    }

    return json_decode($oauth_object->getLastResponse());
}

function getTwitterProfileImg(?string $json_tokens) : ?string {
    if(!$json_tokens) {
        return null;
    }
    $tokens = json_decode($json_tokens);
    $access = $tokens->oauth_token;
    $secret = $tokens->oauth_token_secret;

    if($access && $secret) {
        $twitter_link = new OAuth(CONSUMER_KEY, CONSUMER_SECRET, OAUTH_SIG_METHOD_HMACSHA1, OAUTH_AUTH_TYPE_URI);
        $twitter_link->setToken($access, $secret);

        // Récupération de l'user object
        $obj = getTwitterUserObjectFromUser($twitter_link);
        if(is_object($obj)) {
            // Il n'utilise pas l'image par défaut
            if(!$obj->default_profile_image) {
                $link = $obj->profile_image_url_https;
                $link = preg_replace('/_normal/', '', $link); // Supprime le "normal" pour avoir accès à l'image de base
                $file = file_get_contents($link);
                if($file) {
                    return base64_encode($file);
                }
            }
        }
    }
    return null;
}

function getTwitterBannerImg(?string $json_tokens) : ?string {
    if(!$json_tokens) {
        return null;
    }
    $tokens = json_decode($json_tokens);
    $access = $tokens->oauth_token;
    $secret = $tokens->oauth_token_secret;

    if($access && $secret) {
        $twitter_link = new OAuth(CONSUMER_KEY, CONSUMER_SECRET, OAUTH_SIG_METHOD_HMACSHA1, OAUTH_AUTH_TYPE_URI);
        $twitter_link->setToken($access, $secret);

        // Récupération de l'user object
        $obj = getTwitterUserObjectFromUser($twitter_link);
        if(is_object($obj)) {
            // Il n'utilise pas l'image par défaut
            if($obj->profile_banner_url) {
                $link = $obj->profile_banner_url . '/1500x500';
                $file = file_get_contents($link);
                if($file) {
                    return base64_encode($file);
                }  
            }
        }
    }
    return null;
}

// ---- SAUVEGARDE DE TWEET ----

function tweetSaveExists(string $id_str) : bool {
    global $connexion;

    $res = mysqli_query($connexion, "SELECT idTweet FROM tweetSave WHERE idTweet='$id_str';");

    return ($res && mysqli_num_rows($res));
}

function getTweetFromSave(string $id_str) : ?array {
    if(is_numeric($id_str)) {
        return getTweetDetailsFromSave($id_str, false, false, true);
    }
    return null;
}

function getTweetDetailsFromSave(string $id_str, bool $is_answer = false, bool $as_html = false, bool $is_new = false) : ?array {
    $tweetTable = ($is_new ? 'tweetSave' : ($is_answer  ? 'saveAnswer' : 'saveTweet'));
    $imgTable = ($is_new ? 'tweetSaveImg' : ($is_answer ? 'saveAnswerImg' : 'saveTweetImg'));

    global $connexion;

    $q = mysqli_query($connexion, "SELECT * FROM $tweetTable WHERE idTweet='$id_str';");

    if($q && mysqli_num_rows($q)) {
        $tweet = [];

        $t = mysqli_fetch_assoc($q);

        // Trim du &mdash; au début de la chaîne
        $prefix = '&mdash; ';
        $owner = '';

        if(substr($t['infos'], 0, strlen($prefix)) == $prefix) {
            $owner = substr($t['infos'], strlen($prefix));
        } 

        $separation = explode('(', $owner);
        $tweet['name'] = trim($separation[0]);
        $tweet['screen_name'] = ltrim(explode(')', $separation[1])[0], '@');

        $t['content'] = preg_replace('/<br>/', "\n", $t['content']);

        $tweet['full_text'] = ($as_html ? $t['content'] : html_entity_decode(strip_tags($t['content']), ENT_QUOTES));

        $tweet['id_str'] = $t['idTweet'];

        if(!$is_new)
            $tweet['id_link'] = $is_answer ? $t['idAn'] : $t['idLink'];
        
        $tweet['id_save'] = $is_answer ? $t['idSaveA'] : $t['idSave'];

        // Récupération des images
        $img = mysqli_query($connexion, "SELECT * FROM $imgTable WHERE idSave" . ($is_answer ? 'A=' : '=') . $tweet['id_save']);
        if($img) {
            $tweet['media_count'] = mysqli_num_rows($img);
            $tweet['medias'] = [];
            $i = 0;

            while($rowimg = mysqli_fetch_assoc($img)) {
                $tweet['medias'][$i]['full_link'] = HTTPS_URL . $rowimg['link'];
                $tweet['medias'][$i]['link'] = $rowimg['link'];
                $tweet['medias'][$i]['id_save_img'] = $is_answer ? $rowimg['idSImgA'] : $rowimg['idSImg'];
                $i++;
            }
        }

        return $tweet;
    }

    return null;
}

/**
 * Delete saved tweets that are unlinked to a shitstorm/answer
 *
 * @return void
 */
function deleteOldUnlinkedTweets() : void {
    // Tweets à supprimer (expirés)
    // Expiration : 2 SEMAINES
    $expiration = time() - (60 * 60 * 24 * 7 * 2);
    $expiration_str = date('Y-m-d H:i:s', $expiration);

    global $connexion;

    $res = mysqli_query($connexion, "SELECT idTweet FROM tweetSave WHERE notLinked=1 AND addDate<'$expiration_str';");

    if($res && mysqli_num_rows($res)) {
        while($row = mysqli_fetch_assoc($res)) {
            deleteTweetSave($row['idTweet']);
        }
    }
}

/**
 * Get tweet link from infos (from save)
 *
 * @param string $infos
 * @return string|null
 */
function getTweetLinkFromInfos(string $infos) : ?string {
    $m = [];
    preg_match('/href="(.+)"/', $infos, $m);

    if(isset($m[1])) {
        return $m[1];
    }

    return null;
}

// ---- AUTRES ----

function sendDistressHeader(int $code, ?string $message = NULL, bool $is_JSON = false) : void { // Send error headers, close DB and kill execution of the PHP script
    global $connexion;

    switch($code){
        case 400:
            header('HTTP/1.1 400 Bad Request');
            break;
        case 401:
            header('HTTP/1.1 401 Unauthorized');
            break;
        case 403:
            header('HTTP/1.1 403 Forbidden');
            break;
        case 500:
            header('HTTP/1.1 500 Internal Server Error'); 
            break;
        case 502:
            header('HTTP/1.1 502 Bad Gateway');
            break;
    }
    if($message){
        if($is_JSON){
            header('Content-Type: application/json');
            echo $message;
        }
        else header('Last-Event-ID: '.$message);
    }
    deconnectBD();

    exit();
}

function getTweetIdFromURL(string $html) : ?string {
    $matches = array();
    if(preg_match(REGEX_TWITTER_LINK, $html, $matches)){
        // Le (\?.+)? permet d'éliminer la possible requête GET enregistrée avec
        return $matches[2];
    }

    return null;
}

function getTweetIdByLink(string $html) : ?string {
    $matches = array();
    if(preg_match(REGEX_TWITTER_LINK, $html, $matches)){
        // Le (\?.+)? permet d'éliminer la possible requête GET enregistrée avec
        return $matches[2];
    }
    elseif(preg_match(REGEX_MASTODON_LINK, $html, $matches)){
        return $matches[3];
    }
    return null;
}

function getImageLinkRelatedToShitstorm(int $idSub) : string {
    global $connexion;

    $res = mysqli_query($connexion, "SELECT * FROM Links WHERE idSub='$idSub';");
    if($res && mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_assoc($res)){
            if(preg_match(REGEX_IMG_LINK_UPLOADED, $row['link'])){
                return 'https://shitstorm.fr/' . $row['link']; // Renvoie la première image trouvable dans les liens
            }
        }

        // Si on a rien trouvé, on regarde la sauvegarde de chaque lien
        $res = mysqli_query($connexion, "SELECT ti.link 
                                         FROM Links l 
                                         JOIN tweetSave t
                                         ON l.idTweet=t.idTweet
                                         JOIN tweetSaveImg ti 
                                         ON ti.idSave=t.idSave 
                                         WHERE l.idSub='$idSub' LIMIT 0,1;");

        if($res && mysqli_num_rows($res) > 0){
            $row = mysqli_fetch_assoc($res); // Il n'y a qu'une seule sauvegarde possible associée à un lien
            return 'https://shitstorm.fr/' . $row['link'];
        }
    }

    return DEFAULT_CARD_IMG;
}

function getAllImageLinksRelatedToShitstorm(int $idSub) : array {
    global $connexion;

    $images = [];
    $res = mysqli_query($connexion, "SELECT * FROM Links WHERE idSub='$idSub';");
    if($res && mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_assoc($res)){
            if(preg_match(REGEX_IMG_LINK_UPLOADED, $row['link'])){
                $images[] = 'https://shitstorm.fr/' . $row['link']; // Renvoie la première image trouvable dans les liens
            }
        }

        $res = mysqli_query($connexion, "SELECT ti.link 
                                         FROM Links l 
                                         JOIN tweetSave t
                                         ON l.idTweet=t.idTweet
                                         JOIN tweetSaveImg ti 
                                         ON ti.idSave=t.idSave 
                                         WHERE l.idSub='$idSub';");

        if($res && mysqli_num_rows($res) > 0){
            while($row = mysqli_fetch_assoc($res)) {
                $images[] = 'https://shitstorm.fr/' . $row['link'];
            }
        }
    }

    return $images;
}

function getShitstormLink(string $title) : string {
    return HTTPS_URL . 'shitstorm/' . urlencode($title) . '-';
}

function saveAllIDFromLinksToSaveTweet(){
    global $connexion;

    $res = mysqli_query($connexion, "SELECT * FROM Links;");
    if($res && mysqli_num_rows($res)){
        while($row = mysqli_fetch_assoc($res)){
            $id = getTweetIdByLink($row['link']);

            if($id){
                mysqli_query($connexion, "UPDATE saveTweet SET idTweet='$id' WHERE idLink={$row['idLink']};");
            }
        }
    }
}

function saveAllIDFromAnswersToSaveTweet(){
    global $connexion;

    $res = mysqli_query($connexion, "SELECT * FROM Answers;");
    if($res && mysqli_num_rows($res)){
        while($row = mysqli_fetch_assoc($res)){
            $id = getTweetIdByLink($row['linkA']);

            if($id){
                mysqli_query($connexion, "UPDATE saveAnswer SET idTweet='$id' WHERE idAn={$row['idAn']};");
            }
        }
    }
}
