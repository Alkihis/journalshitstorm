<?php

// ---------------------------------
// MODULE D'INTERACTION AVEC TWITTER 
// ---------------------------------

class TwitterBot{
    protected $url_verify = 'https://api.twitter.com/1.1/account/verify_credentials.json';
    protected $url_embed = "https://publish.twitter.com/oembed";
    protected $url_single = "https://api.twitter.com/1.1/statuses/show.json%s";

    private $oauth;
    public function __construct($key, $secret){ 
        $this->oauth = new OAuth($key, $secret, OAUTH_SIG_METHOD_HMACSHA1, OAUTH_AUTH_TYPE_URI);
        $this->oauth->disableSSLChecks(); 
    } 
    public function setToken($token, $secret){
        $this->oauth->setToken($token, $secret); 
    }
    
    private function verificationCredentials() : bool{
        try{
            $this->oauth->fetch($this->url_verify, array(), OAUTH_HTTP_METHOD_GET);
            return true;
        }catch(Exception $ex){
            return false;
        }
    }
    
    public function getEmbedded($link){
        if($this->verificationCredentials()){
            try{
                $request = 'https://publish.twitter.com/oembed?url=%s&align=center';
                $this->oauth->fetch(sprintf($request, htmlentities($link)));
            }catch(OAuthException $ex){
                echo 'Erreur OAuth à la requête d\'obtention des propriétés de la liste : '.$ex->lastResponse;
            }
            $embedJson = json_decode($this->oauth->getLastResponse());
            if(!isset($embedJson->author_name))
                return false;
            return $embedJson->html;
        }
    }

    public function getSingleTweet($id_str){
        if($this->verificationCredentials()){
            try{
                $request = sprintf($this->url_single, "?id=$id_str&tweet_mode=extended");

                $this->oauth->fetch($request);
            } catch(OAuthException $ex){
                echo 'Erreur OAuth à la requête d\'obtention des propriétés de la liste : '.$ex->lastResponse;
            }
            $json = $this->oauth->getLastResponse();

            $embedJson = json_decode($json);
            if(!isset($embedJson->full_text))
                return false;
            return $json;
        }
    }

    public function getUserTime($user_id, $isScr, $count = 20){
        if($this->verificationCredentials()){
            try{
                $this->oauth->fetch("https://api.twitter.com/1.1/statuses/user_timeline.json?".($isScr ? "screen_name=$user_id" : "user_id=$user_id").
                "&tweet_mode=extended&include_rts=true&count=$count");
            } catch (Exception $e){

            }
            $json = $this->oauth->getLastResponse();

            $embedJson = json_decode($json);
            return $json;
        }
    }

    public function getMedias($id_str){
        if($this->verificationCredentials()){
            try{
                $request = sprintf($this->url_single, "?id=$id_str&tweet_mode=extended");

                $this->oauth->fetch($request);
            } catch (Exception $e){

            }
            $result = $this->oauth->getLastResponse();

            $result = json_decode($result);
            
            if(isset($result->extended_entities->media)) {
                return $result->extended_entities->media;
            }
            return false;
        }
        return NULL;
    }
    
}