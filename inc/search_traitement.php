<?php

// --------------------------------------------------------------------
// Gestion de la recherche dans le journal des shitstorms (version web)
// --------------------------------------------------------------------

function getStringQuery($terms, $since_date, $before_date, $users, $type, $other_terms) : string { // Construit la chaîne de recherche depuis le formulaire de recherche avancée
    $query = '';

    $terms = trim($terms);
    if($terms != ''){
        $query = $terms;
    }

    $other_terms = trim($other_terms);
    if($other_terms != ''){
        $query .= ($query != '' ? " or " : '').$other_terms;
    }

    if($users != ''){ // Utilisateurs spécifiés
        $all_users = explode(' ', $users);
        $id_str = 'id:';
        $found = false;
        foreach($all_users as $u){
            if($u == '') // Cas où il y a plusieurs espaces
                continue;

            $id = getIDFromUsername($u);
            if($id != 0){
                if(!$found){ // Premier utilisateur ajouté
                    $id_str .= $id;
                }
                else{
                    $id_str .= ','.$id;
                }
                $found = true;
            }
        }
        if($found){
            if(!empty($query)){
                $query .= ' ' . $id_str;
            }
            else{
                $query = $id_str;
            }
        }
    }

    if($since_date != ''){
        if(!empty($query)){
            $query .= " since:$since_date";
        }
        else{
            $query = "since:$since_date";
        }
    }
    if($before_date != ''){
        if(!empty($query)){
            $query .= " before:$before_date";
        }
        else{
            $query = "before:$before_date";
        }
    }

    if($type == 'shitstorm'){
        $query = 'shitstorm:'.$query;
    }
    elseif($type == 'user'){
        $query = "user:".$query;
    }

    return $query;
}

function getUsername($idUsr) : string { // Renvoie une chaîne avec le nom de l'utilisateur si réussi, le numéro brut sinon
    global $connexion;
    if(!is_numeric($idUsr)){
        return (string)$idUsr;
    }
    $idUsr = (int)$idUsr;

    $res = mysqli_query($connexion, "SELECT usrname FROM Users WHERE id='$idUsr';");
    if($res && mysqli_num_rows($res) > 0){
        $row = mysqli_fetch_assoc($res);
        return $row['usrname'];
    }
    return (string)$idUsr;
}

function consTerms($array_term, &$query, &$termes_str) : string { // Construit les termes de recherche dans $query et leur signification textuelle dans $termes_str / Retourne $query
    $query .= "(dscr LIKE '%".trim($array_term[0])."%'"; // Recherche, vaut quelque chose quoi qu'il arrive
    $termes_str .= " \"{$array_term[0]}\"";
    foreach($array_term as $key => $min_term){
        if($key == 0 || $min_term == '') 
            continue;

        $query .= " AND dscr LIKE '%".trim($min_term)."%'";
        $termes_str .= " et \"$min_term\"";
    }
    $query .= ')';
    // Même chose, mais pour le titre
    $query .= " OR (title LIKE '%".trim($array_term[0])."%'"; // Recherche, vaut quelque chose quoi qu'il arrive
    foreach($array_term as $key => $min_term){
        if($key == 0 || $min_term == '') 
            continue;

        $query .= " AND title LIKE '%".trim($min_term)."%'";
    }
    $query .= ')';

    return $query;
}

function searchParam($curSearch, &$def_req, &$trimmed) : string { // Retourne une chaîne qui sert de clause WHERE valide et stocke dans def_req un texte valide qui commente la recherche
    $have_sel = false;
    $query = '';
    // Recherche par date (après le)
    // \b est pour le word break (start / end of line OR space)
    $curSearch = preg_replace_callback('/\bsince:([0-9]{4}-[0-9]{2}-[0-9]{2})\b/', 
        function ($mat) use (&$query, &$have_sel, &$def_req){
            if(!strtotime($mat[1])) 
                return ''; // date invalide

            if($query == ''){
                $query .= " dateShit >= '{$mat[1]}'";
            }
            else 
                $query .= " AND dateShit >= '{$mat[1]}'";
            $have_sel = true;
            $def_req .= " postées depuis le " . formatDate($mat[1], 0);
            return '';
        }, 
        $curSearch);

    // Recherche par date (avant le)
    $curSearch = preg_replace_callback('/\bbefore:([0-9]{4}-[0-9]{2}-[0-9]{2})\b/', 
        function ($mat) use (&$query, &$have_sel, &$def_req){
            if(!strtotime($mat[1])) 
                return ''; // date invalide
            if($query == ''){
                $query .= " dateShit < '{$mat[1]}'";
            }
            else
                $query .= " AND dateShit < '{$mat[1]}'";
            $have_sel = true;

            if($def_req != ''){
                $def_req .= ' et postées avant le ' . formatDate($mat[1], 0);
            }
            else $def_req .= ' postées avant le ' . formatDate($mat[1], 0);
            return '';
        }, 
        $curSearch);

    // Recherche par id d'utilisateur
    $curSearch = preg_replace_callback('/\bid:([0-9]{1,}(,[0-9]{1,}){0,})\b/', 
        function ($mat) use (&$query, &$have_sel, &$def_req){
            // Récupération de tous les ID rentrés
            $tabID = explode(',', $mat[1]);

            // Vérification des ID avant de changer quoique ce soit
            $withreal = 0;
            foreach($tabID as $key => $id){
                if($id < 0){
                    unset($tabID[$key]);
                    continue; // ID invalide
                }
                $withreal++;
            }

            if($withreal == 0){ // Aucun ID n'est valide
                return '';
            }

            if($query != '')
                $query .= ' AND';

            $query .= ' (';
            $first = true;

            foreach($tabID as $id){
                if($first)
                    $query .= "idUsr='$id'";
                else
                    $query .= " OR idUsr='$id'";

                $have_sel = true; 
                if($id == 0){ // ID = 0 > anonyme
                    if($def_req == '')
                        $def_req .= (!$first ? ' et' : '') . ' postées anonymement';
                    
                    else 
                        $def_req .= (!$first ? ' et' : '') . ' par des anonymes';
                } 
                else {
                    if($def_req == '')
                        $def_req .= (!$first ? ' et' : '') . ' postées par ' . getUsername($id);

                    else 
                        $def_req .= (!$first ? ' et' : '') . ' par ' . getUsername($id);
                }
                $first = false;
            }

            $query .= ')';

            return '';
        }, 
        $curSearch);

    // Recherche par nom d'utilisateur
    $first = true;
    $tempQuery = '(';
    $curSearch = preg_replace_callback('/\bfrom:@?([a-z0-9_-]{3,16})\b/i', 
    function ($mat) use (&$tempQuery, &$have_sel, &$def_req, &$first){
        $username = $mat[1];
        $id = getIDFromUsername($username);

        if($id <= 0){
            return '';
        }

        if($first)
            $tempQuery .= "idUsr='$id'";
        else
            $tempQuery .= " OR idUsr='$id'";

        $have_sel = true; 
        if($def_req == '')
            $def_req .= (!$first ? ' et' : '') . ' postées par ' . getUsername($mat[1]);
        else 
            $def_req .= (!$first ? ' et' : '') . ' par ' . getUsername($mat[1]);

        $first = false;

        return '';
    }, 
    $curSearch);
    $tempQuery .= ')';

    if(!$first){
        if(empty($query)){
            $query = $tempQuery;
        }
        else{
            $query .= " AND $tempQuery";
        }
    }

    // Fin recherche
    
    $trimmed = trim($curSearch);
    $termes_str = '';

    if($trimmed != ''){ // Il n'y a autre chose que des sélecteurs
        $terms = explode(' or ', $trimmed); // Recherche de or
        $query .= ($have_sel ? " AND " : '') . '('; // Recherche, vaut quelque chose quoi qu'il arrive
        consTerms(explode(' ', trim($terms[0])), $query, $termes_str);

        // Formation des termes de recherches
        foreach($terms as $key => $term){
            if($key == 0) 
                continue;

            $query .= " OR ";
            $termes_str .= " ou ";
            consTerms(explode(' ', trim($term)), $query, $termes_str);
        }
        $query .= ')';
        $def_req .= " correspondant aux termes $termes_str";
    }

    return $query;
}
