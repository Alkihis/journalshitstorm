<?php

function shitstormExists(int $id) : bool {
    global $connexion;

    $res = mysqli_query($connexion, "SELECT idSub FROM Shitstorms WHERE idSub=$id;");

    return ($res && mysqli_num_rows($res));
}

function waitingShitstormExists(int $id) : bool {
    global $connexion;

    $res = mysqli_query($connexion, "SELECT idSub FROM Waiting WHERE idSub=$id;");

    return ($res && mysqli_num_rows($res));
}

function answerExists(int $id) : bool {
    global $connexion;

    $res = mysqli_query($connexion, "SELECT idAn FROM Answers WHERE idAn=$id;");

    return ($res && mysqli_num_rows($res));
}

function waitingAnswerExists(int $id) : bool {
    global $connexion;

    $res = mysqli_query($connexion, "SELECT idSubA FROM WaitAnsw WHERE idSubA=$id;");

    return ($res && mysqli_num_rows($res));
}

function commentExists(int $id) : bool {
    global $connexion;

    $res = mysqli_query($connexion, "SELECT idCom FROM Comment WHERE idCom=$id;");

    return ($res && mysqli_num_rows($res));
}
