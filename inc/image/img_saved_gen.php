<?php

function afficheImg(&$images, $nbImages){
    $rand = md5(random_bytes(10));
            
    if($nbImages == 1){
        ?>
        <div class='images-placeholder'>
            <a class='fancy-boxed' href='<?= HTTPS_URL.$images[0]['link'] ?>' data-fancybox='<?= $rand ?>'>
                <img class='col s12 l8 offset-l2 responsive-img deleted-img' src='<?= HTTPS_URL.$images[0]['link'] ?>'>
            </a>
            <div class='hide-on-small-only col m2'></div>
        </div>
        
        <?php
    }
    elseif($nbImages == 2){
        ?>
        <div class='images-placeholder'>
            <a class='fancy-boxed' href='<?= HTTPS_URL.$images[0]['link'] ?>' data-fancybox='<?= $rand ?>'>
                <img class='responsive-img deleted-img' style='width: 49%; float: left; height: 100% !important;' src='<?= HTTPS_URL.$images[0]['link'] ?>'>
            </a>
            <div style='width: 2%'></div>
            
            <a class='fancy-boxed' href='<?= HTTPS_URL.$images[1]['link'] ?>' data-fancybox='<?= $rand ?>'>
                <img class='responsive-img deleted-img' style='width: 49%; float: right; height: 100% !important;' src='<?= HTTPS_URL.$images[1]['link'] ?>'>
            </a>
        </div>
        <?php
    }
    elseif($nbImages == 3){
        ?>
        <div class='images-placeholder'>
            <a class='fancy-boxed' href='<?= HTTPS_URL.$images[0]['link'] ?>' data-fancybox='<?= $rand ?>'>
                <img class='responsive-img deleted-img' style='width: 49%; float: left; height: 49% !important;' src='<?= HTTPS_URL.$images[0]['link'] ?>'>
            </a>
            <a class='fancy-boxed' href='<?= HTTPS_URL.$images[1]['link'] ?>' data-fancybox='<?= $rand ?>'>
                <img class='responsive-img deleted-img' style='width: 49%; float: right; height: 49% !important;' src='<?= HTTPS_URL.$images[1]['link'] ?>'>
            </a>

            <div class='clearb' style='margin-bottom: 1em;'></div>

            <a class='fancy-boxed' href='<?= HTTPS_URL.$images[2]['link'] ?>' data-fancybox='<?= $rand ?>'>
                <img class='responsive-img deleted-img' style='width: 100%; height: 49% !important;' src='<?= HTTPS_URL.$images[2]['link'] ?>'>
            </a>
        </div>
        <?php
    }
    elseif($nbImages == 4){
        ?>
        <div class='images-placeholder'>
            <a class='fancy-boxed' href='<?= HTTPS_URL.$images[0]['link'] ?>' data-fancybox='<?= $rand ?>'>
                <img class='responsive-img deleted-img' style='width: 49%; float: left; height: 49% !important;' src='<?= HTTPS_URL.$images[0]['link'] ?>'>
            </a>
            <div style='width: 2%'></div>
            <a class='fancy-boxed' href='<?= HTTPS_URL.$images[1]['link'] ?>' data-fancybox='<?= $rand ?>'>
                <img class='responsive-img deleted-img' style='width: 49%; float: right; height: 49% !important;' src='<?= HTTPS_URL.$images[1]['link'] ?>'>
            </a>
            
            <div class='clearb' style='margin-bottom: 1em;'></div>
            
            <a class='fancy-boxed' href='<?= HTTPS_URL.$images[2]['link'] ?>' data-fancybox='<?= $rand ?>'>
                <img class='responsive-img deleted-img' style='width: 49%; float: left; height: 49% !important;' src='<?= HTTPS_URL.$images[2]['link'] ?>'>
            </a>
            <div style='width: 2%'></div>
            <a class='fancy-boxed' href='<?= HTTPS_URL.$images[3]['link'] ?>' data-fancybox='<?= $rand ?>'>
                <img class='responsive-img deleted-img' style='width: 49%; float: right; height: 49% !important;' src='<?= HTTPS_URL.$images[3]['link'] ?>'>
            </a>
        </div>
        <?php
    }
    else{
        foreach($images as $img){
            ?>
            <a class='fancy-boxed' href='<?= HTTPS_URL.$img['link'] ?>' data-fancybox='<?= $rand ?>'>
                <img class='col s12 l8 offset-l2 responsive-img deleted-img' src='<?= HTTPS_URL.$img['link'] ?>'>
            </a>
            <div class='hide-on-small-only col m2'></div>
            <div class='col s12 divider divider-delete-tweet-bottom'></div>
            <?php
        }
    }
    ?>
    <div class='clearb'></div>
    <div class='col s12 divider divider-delete-tweet-bottom'></div>
    <?php
}