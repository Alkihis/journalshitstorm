<?php

// include composer autoload
require 'vendor/autoload.php';
use Intervention\Image\ImageManagerStatic as Image;

// ini_set('display_errors', '1');

function traitementProfilImg($filePOSTname = 'imgpro', $sub_directory = 'profile', $muted = false){
    if($sub_directory == NULL && !$muted){
        echo "<h5 class='error'>Erreur d'exécution de la fonction : Répertoire invalide</h5>";
    }
    elseif($_FILES[$filePOSTname]['error'] == 0 && $_FILES[$filePOSTname]['size'] <= 1048576){
        // array of valid extensions
        $validExtensions = array('.jpg', '.jpeg', '.gif', '.png');
        // get extension of the uploaded file
        $fileExtension = strrchr($_FILES[$filePOSTname]['name'], ".");
        // check if file Extension is on the list of allowed ones
        if(in_array($fileExtension, $validExtensions)) {
            // read image from temporary file
            $img = Image::make($_FILES[$filePOSTname]['tmp_name']);
            
            // resize image
            $img->fit(200, 200);

            $newNamePrefix = '';
            $nom = '';
            do{
                $nomInit = md5(uniqid(rand(), true));
                $newNamePrefix = ROOT . "/img/$sub_directory/{$nomInit}{$fileExtension}";
                $nom = "img/$sub_directory/{$nomInit}{$fileExtension}";
            }while(file_exists($newNamePrefix));
            
            $img->save($newNamePrefix);
            return $nom;
        }
    }
    return NULL;
}

function traitementBannerImg($filePOSTname = 'imgban', $sub_directory = 'banner', $muted = false){
    if($sub_directory == NULL && !$muted){
        echo "<h5 class='error'>Erreur d'exécution de la fonction : Répertoire invalide</h5>";
    }
    elseif($_FILES[$filePOSTname]['error'] == 0 && $_FILES[$filePOSTname]['size'] <= (2097152)){
        // array of valid extensions
        $validExtensions = array('.jpg', '.jpeg', '.gif', '.png');
        // get extension of the uploaded file
        $fileExtension = strrchr($_FILES[$filePOSTname]['name'], ".");
        // check if file Extension is on the list of allowed ones
        if(in_array($fileExtension, $validExtensions)) {
            // read image from temporary file
            $img = Image::make($_FILES[$filePOSTname]['tmp_name']);
            
            // resize image
            $img->fit(2000, 700);

            $newNamePrefix = '';
            $nom = '';
            do{
                $nomInit = md5(uniqid(rand(), true));
                $newNamePrefix = ROOT . "/img/$sub_directory/{$nomInit}{$fileExtension}";
                $nom = "img/$sub_directory/{$nomInit}{$fileExtension}";
            }while(file_exists($newNamePrefix));
            
            $img->save($newNamePrefix);
            return $nom;
        }
    }
    return NULL;
}

function traitementImgBase64(string &$base64, string $sub_directory, int $max_size = 2097152, int $width = 2000, int $height = 700, $muted = false){
    if($sub_directory == NULL && !$muted){
        echo "<h5 class='error'>Erreur d'exécution de la fonction : Répertoire invalide</h5>";
    }
    elseif((int)(strlen(rtrim($base64, '=')) * 3 / 4) <= ($max_size)){
        // assume it's jpg
        // read image from temporary file
        $img = Image::make($base64);

        // resize image
        $img->fit($width, $height);
        $fileExtension = '.jpg';

        $newNamePrefix = '';
        $nom = '';
        do{
            $nomInit = md5(uniqid(rand(), true));
            $newNamePrefix = ROOT . "/img/$sub_directory/{$nomInit}{$fileExtension}";
            $nom = "img/$sub_directory/{$nomInit}{$fileExtension}";
        } while(file_exists($newNamePrefix));
        
        $img->save($newNamePrefix);
        // Retourne le chemin vers l'image (lien théorique depuis /var/www/html)
        return $nom;
    }
    return NULL;
}
