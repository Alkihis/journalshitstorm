<?php
	
const SERVEUR = '%s'; // serveur SGBD
	
const UTILISATRICE = '%s'; // utilisatrice
	
const MOTDEPASSE = '%s'; // mot de passe
	
const BDD = '%s'; // base de données

const CONSUMER_KEY = '%s';

const CONSUMER_SECRET = '%s';

const OAUTH_KEY = '%s';

const OAUTH_SECRET = '%s';

require($_SERVER['DOCUMENT_ROOT'] . '/inc/const_string.php');
