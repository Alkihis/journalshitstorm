<?php

// --------------------------
// TRAITEMENTS DE JOURNAL.PHP
// --------------------------

function getGestion(bool &$by_year = false) : string { // Renvoie la date associée aux possibles paramètres GET passés à journal.php
    global $connexion;

    $dateBase = date('Y');
    $date = '';

    if(isset($_GET['selyear']) && is_string($_GET['selyear'])){
        $date = $_GET['selyear'] . (isset($_GET['selmonth']) && is_string($_GET['selmonth']) ? '-' . $_GET['selmonth'] : '');
    }
    else{
        if(isset($_GET['date']) && is_string($_GET['date'])){
            $date = $_GET['date'];
        }
        else{
            $date = $dateBase;
        }
    }

    $dateTab = explode('-', $date);

    if(!$by_year && count($dateTab) > 1){ 
        try{
            $dateT = new DateTime($date);
    
            $actual = new DateTime($dateBase);
            if($dateT > $actual){
                echo "<h5 class='error'>La date choisie est supérieure à la date actuelle.</h5>";
                $dateBase = $actual->format('Y-m');
            }
            else{
                $dateBase = $dateT->format('Y-m');
            }
        } catch (Exception $E) {
            echo "<h5 class='error'>La date choisie est invalide.</h5>";
        }
    }
    else{ // Il n'y a qu'une année
        $by_year = true;
        $date = $dateTab[0];

        if(is_numeric($date) && $date > 0 && $date <= $dateBase){
            return $date;
        }
        else{
            echo "<h5 class='error'>La date choisie est supérieure à la date actuelle ou est invalide.</h5>";
            return date('Y');
        }
    }

    return $dateBase;
}

function perPageGestion($user_per_page) : int {
    global $connexion;

    $per_page_safe = intval(mysqli_real_escape_string($connexion, $user_per_page));

    if($per_page_safe > 1){
        if($per_page_safe % 5 == 0 && $per_page_safe <= 50){
            return $per_page_safe;
        }
    } 
    return 10;
}

function hasNotifications(int $idUsr) : int { // From 0 to 20; If 21 : Show 20+
    global $connexion;

    $res = mysqli_query($connexion, "SELECT idNotif FROM Notifications WHERE idDest='$idUsr' AND viewed=0 LIMIT 0,21;");

    if($res){
        return (int)mysqli_num_rows($res);
    }
    return 0;
}

function isFollowable(int $idUsr) : bool {
    global $connexion;

    $res = mysqli_query($connexion, "SELECT followable FROM Users WHERE id='$idUsr' AND followable=1;");

    return ($res && mysqli_num_rows($res));
}

// ----------------------
// ENVOI DE NOTIFICATIONS
// ----------------------

function verifyInReplyCommentValidity(int $idCom, string &$content) : bool {
    global $connexion;

    $res = mysqli_query($connexion, "SELECT u.usrname FROM Comment c JOIN Users u ON c.idUsr=u.id WHERE idCom=$idCom;");

    if($res && mysqli_num_rows($res)){
        $row = mysqli_fetch_assoc($res);
        $usrname = $row['usrname'];

        $content = ltrim($content);
        if(preg_match("/^@$usrname\b/i", $content, $mat)){ 
            // Si l'@ en début de commentaire correspond à l'username de l'utilisateur ayant posté le commentaire auquel il répond
            return true;
        }
    }
    return false;
}

function sendNotif(string $type_notif, int $idEmitter, int $idDest, int $idRelated, ?int $additionalVerif = null) : void {
    global $connexion;
    if($type_notif == 'shitstorm' && $idDest == 0){ // On doit envoyer la notif à tous les abonnés de l'utilisateur qui émet la shitstorm
        $res = mysqli_query($connexion, "SELECT f.idUsr, f.acceptMail, u.confirmedMail, u.mail FROM Followings f JOIN Users u ON f.idUsr=u.id WHERE idFollowed=$idEmitter;");
        // Récupère tous les utilisateurs (idUsr) qui suivent l'émetteur et récupère le fait d'avoir confirmé leur mail ou non (confirmedMail)

        if($res && mysqli_num_rows($res) > 0){ 
            // Récupération des infos de la shitstorm nécessaires
            $shit = mysqli_query($connexion, "SELECT s.title, s.dscr, s.pseudo as usrname FROM Shitstorms s WHERE idSub='$idRelated';");
            $shitrow = null;
            if($shit && mysqli_num_rows($shit)){
                $shitrow = mysqli_fetch_assoc($shit);
            }

            // Envoi des notifications + mails
            while($row = mysqli_fetch_assoc($res)){
                mysqli_query($connexion, "INSERT INTO Notifications (idDest, idUsr, idRelated, type) VALUES ({$row['idUsr']}, $idEmitter, $idRelated, 'SHITSTORM');");

                // Si l'utilisateur veut des mails pour les shitstorms de cet user, a confirmé son mail et si son mail n'est pas vide
                if($row['confirmedMail'] && $row['mail'] != '' && $row['acceptMail'] && $shitrow){
                    require_once(ROOT . '/inc/send_mail.php');
                    sendMailShitstorm($row['mail'], $idRelated, $shitrow['title'], $shitrow['dscr'], $shitrow['usrname'], $idEmitter);
                }
            }
        }
    }
    elseif($type_notif == 'like_comment' || $type_notif == 'like_shitstorm' || $type_notif == 'dislike_shitstorm'){
        if($idEmitter == $idDest){
            // Si une notif tente de s'envoyer à soi-même
            return;
        }

        $action_type = strtoupper($type_notif);
        mysqli_query($connexion, "INSERT INTO Notifications (idDest, idUsr, idRelated, type)
                                    SELECT $idDest as idDest, $idEmitter as idUsr, $idRelated as idRelated, '$action_type' as type
                                    FROM Notifications
                                    WHERE (idDest=$idDest AND idUsr=$idEmitter AND idRelated=$idRelated AND type='$action_type')
                                    HAVING COUNT(*) = 0;"); // Si la personne s'amuse à like/dislike, la notif n'arrive pas deux fois
        
    }
    elseif($type_notif == 'mention_comment'){
        if($idEmitter == $idDest){
            // Si une notif tente de s'envoyer à soi-même
            return;
        }

        mysqli_query($connexion, "INSERT INTO Notifications (idDest, idUsr, idRelated, type) VALUES ($idDest, $idEmitter, $idRelated, 'MENTION_COMMENT');");
    }
    elseif($type_notif == 'post_comment'){
        // Envoi d'une notif à tous les abonnés de la shitstorm
        // idEmitter : idUsr de l'émetteur du commentaire
        // idDest : idSub de la shitstorm qui reçoit le commentaire
        // idRelated : idCom du nouveau commentaire

        $res = mysqli_query($connexion, "SELECT f.idFollower, f.withMail, u.confirmedMail, u.mail, u.realname FROM ShitFollowings f JOIN Users u ON f.idFollower=u.id WHERE idFollowed=$idDest;");
        // Récupère tous les utilisateurs (idUsr) qui suivent l'émetteur et récupère le fait d'avoir confirmé leur mail ou non (confirmedMail)

        if($res && mysqli_num_rows($res) > 0){ 
            // Récupération des infos de la shitstorm nécessaires
            $shit = mysqli_query($connexion, "SELECT c.content, s.title, s.dscr, s.pseudo as usrname FROM Comment c JOIN Shitstorms s ON c.idSub=s.idSub WHERE idCom='$idRelated';");
            $shitrow = null;
            if($shit && mysqli_num_rows($shit)){
                $shitrow = mysqli_fetch_assoc($shit);
            }

            // Envoi des notifications + mails
            while($row = mysqli_fetch_assoc($res)){
                if($row['idFollower'] != $idEmitter){
                    // Envoi la notif uniquement si l'émetteur est différent du receveur

                    mysqli_query($connexion, "INSERT INTO Notifications (idDest, idUsr, idRelated, type) VALUES ({$row['idFollower']}, $idEmitter, $idRelated, 'POST_COMMENT');");

                    // Si l'utilisateur veut des mails pour les shitstorms de cet user, a confirmé son mail et si son mail n'est pas vide
                    if($row['confirmedMail'] && $row['mail'] != '' && $row['withMail'] && $shitrow){
                        require_once(ROOT . '/inc/send_mail.php');
                        sendMailNewComment($row['mail'], $idDest, $shitrow['title'], $shitrow['content'], $row['realname'], $idEmitter);
                    }
                }
            }
        }

    }
    elseif($type_notif == 'post_answer'){
        // Envoi d'une notif à tous les abonnés de la shitstorm
        // idEmitter : idUsr de l'émetteur de la réponse
        // idDest : idSub de la shitstorm qui reçoit la réponse
        // idRelated : idAn de la nouvelle réponse

        $res = mysqli_query($connexion, "SELECT f.idFollower, f.withMail, u.confirmedMail, u.mail, u.realname FROM ShitFollowings f JOIN Users u ON f.idFollower=u.id WHERE idFollowed=$idDest;");
        // Récupère tous les utilisateurs (idUsr) qui suivent l'émetteur et récupère le fait d'avoir confirmé leur mail ou non (confirmedMail)

        if($res && mysqli_num_rows($res) > 0){ 
            // Récupération des infos de la shitstorm nécessaires
            $shit = mysqli_query($connexion, "SELECT a.dscrA, s.title, s.dscr, s.pseudo as usrname FROM Answers a JOIN Shitstorms s ON c.idSub=s.idSub WHERE idAn='$idRelated';");
            $shitrow = null;
            if($shit && mysqli_num_rows($shit)){
                $shitrow = mysqli_fetch_assoc($shit);
            }

            // Envoi des notifications + mails
            while($row = mysqli_fetch_assoc($res)){
                if($row['idFollower'] != $idEmitter){
                    // Envoi la notif uniquement si l'émetteur est différent du receveur

                    mysqli_query($connexion, "INSERT INTO Notifications (idDest, idUsr, idRelated, type) VALUES ({$row['idFollower']}, $idEmitter, $idRelated, 'POST_ANSWER');");

                    // Si l'utilisateur veut des mails pour les shitstorms de cet user, a confirmé son mail et si son mail n'est pas vide
                    if($row['confirmedMail'] && $row['mail'] != '' && $row['withMail'] && $shitrow){
                        require_once(ROOT . '/inc/send_mail.php');
                        sendMailNewAnswer($row['mail'], $idDest, $shitrow['title'], $shitrow['dscrA'], $row['realname'], $idEmitter);
                    }
                }
            }
        }
    }
    elseif($type_notif == 'confirm_shitstorm'){
        if($idEmitter == $idDest){
            // Si une notif tente de s'envoyer à soi-même
            return;
        }

        mysqli_query($connexion, "INSERT INTO Notifications (idDest, idUsr, idRelated, type) VALUES ($idDest, $idEmitter, $idRelated, 'CONFIRM_SHITSTORM');");
    }
    elseif($type_notif == 'confirm_answer'){
        // Envoi d'une notif au posteur de la réponse
        // idEmitter : idUsr de l'approbateur de la réponse
        // idDest : idUsr du posteur de la réponse
        // idRelated : idAn de la nouvelle réponse

        if($idEmitter == $idDest){
            // Si une notif tente de s'envoyer à soi-même
            return;
        }

        mysqli_query($connexion, "INSERT INTO Notifications (idDest, idUsr, idRelated, type) VALUES ($idDest, $idEmitter, $idRelated, 'CONFIRM_ANSWER');");
    }
    elseif($type_notif == 'follow'){
        // Aucun ID related ici : Le destinataire est le suivi et l'émetteur est le suiveur.
        mysqli_query($connexion, "INSERT INTO Notifications (idDest, idUsr, idRelated, type)
                                    SELECT $idDest as idDest, $idEmitter as idUsr, 0 as idRelated, 'FOLLOW' as type
                                    FROM Notifications
                                    WHERE (idDest=$idDest AND idUsr=$idEmitter AND idRelated=0 AND type='FOLLOW')
                                    HAVING COUNT(*) = 0;"); // Si la personne s'amuse à suivre/unfollow puis resuivre, la notif n'arrive pas deux fois
    }
    // Modérateurs. Pour le moment, n'envoie qu'à moi :(
    elseif($type_notif == 'moderation_shitstorm'){
        mysqli_query($connexion, "INSERT INTO Notifications (idDest, idUsr, idRelated, type) VALUES (9, $idEmitter, $idRelated, 'MODERATION_SHITSTORM');");
    }
    elseif($type_notif == 'moderation_answer'){
        mysqli_query($connexion, "INSERT INTO Notifications (idDest, idUsr, idRelated, type) VALUES (9, $idEmitter, $idRelated, 'MODERATION_ANSWER');");
    }
}

function containMention(string $content, bool $usernames = false) : array { // Renvoie les (ID|usrname) utilisateurs liée à des mentions si il y en dans un tableau
    global $connexion;
    $return = [];
    $mat = [];
    preg_match_all('/([a-z0-9_-]{3,16})\b/i', $content, $mat);

    foreach($mat[1] as $username){
        $res = mysqli_query($connexion, "SELECT id, usrname FROM Users WHERE usrname='$username';");
        if($res && mysqli_num_rows($res) > 0){
            $row = mysqli_fetch_assoc($res);
            if($usernames){
                $return[] = $row['usrname'];
            }
            else{
                $return[] = $row['id'];
            }
        }
    }

    return $return;
}

// -----------------------------
// TRAITEMENTS DE UNIQUESHIT.PHP
// -----------------------------

function supprimerCommentaire() : void { // Supprime le commentaire en fonction du POST de uniqueshit.php. Return void
    global $connexion;

    $idCom = mysqli_real_escape_string($connexion, $_POST['delCom']);
    if(md5($_COOKIE['token'] . $idCom) == $_POST['del']){
        $res = NULL;
        if(isset($_SESSION['mod']) && $_SESSION['mod'] > 0){ // Autoriser les modos à supprimer n'importe quel commentaire
            $res = mysqli_query($connexion, "SELECT * FROM Comment WHERE idCom='$idCom';");
        }
        else{
            $res = mysqli_query($connexion, "SELECT * FROM Comment WHERE idCom='$idCom' AND idUsr='{$_SESSION['id']}';");
        }

        if($res && mysqli_num_rows($res) > 0){
            deleteCom($idCom);
            echo "<h5 class='pass'>Le commentaire a été supprimé.</h5>";
        }
        else{
            echo "<h5 class='error'>Impossible de trouver le commentaire voulu.</h5>";
        }
    }
}

function publierCommentaire($idSub, &$pubCom, $idPosteur = 0) : void {
    global $connexion;

    $content = mysqli_real_escape_string($connexion, $_POST['contentCom']);

    $in_reply = (int)$_POST['in_reply_to_id'];
    $in_reply_main = (int)$_POST['in_reply_to_id'];

    if($in_reply){
        $test = mysqli_query($connexion, "SELECT idCom, inReplyToId, inReplyToIdMain FROM Comment WHERE idCom=$in_reply;");
        // Si le commentaire où la réponse est demandée n'existe pas
        if(!($test && mysqli_num_rows($test))){
            echo "<h5 class='error'>Impossible d'ajouter le commentaire : Vous tentez de répondre à un commentaire qui n'existe pas.</h5>";
            $pubCom = $content;
            return;  
        }
        if(!verifyInReplyCommentValidity($in_reply, $content)){
            $in_reply = 'NULL';
            $in_reply_main = 'NULL';
        }
        $rowReply = mysqli_fetch_assoc($test);
        if($rowReply['inReplyToIdMain'] != null){ // Si le commentaire auquel on répond est une réponse
            // On stocke l'ID du commentaire initial qui a débuté la conversation
            $in_reply_main = $rowReply['inReplyToIdMain'];
        }
    }
    else{
        $in_reply = 'NULL';
        $in_reply_main = 'NULL';
    }

    $len = mb_strlen($content);

    if($len > MAX_LEN_COMMENT || $len < 1 || $in_reply === 0){
        if($in_reply === 0){
            echo "<h5 class='error'>Impossible d'ajouter le commentaire : Vous tentez de répondre à un commentaire qui n'existe pas.</h5>";
        }
        else{
            echo "<h5 class='error'>Impossible d'ajouter le commentaire : Commentaire vide ou dépassement de la limite de caractères. ".MAX_LEN_COMMENT." caractères maximum sont autorisés.</h5>";
        }
        $pubCom = $content;
    }
    else{
        $res = mysqli_query($connexion, "INSERT INTO Comment (content, idUsr, idSub, inReplyToId, inReplyToIdMain) VALUES ('$content', '{$_SESSION['id']}', '$idSub', $in_reply, $in_reply_main);");
        if($res){
            $idCom = mysqli_insert_id($connexion);

            $IDs = containMention($content);

            foreach($IDs as $id){
                if($id != $_SESSION['id']){
                    sendNotif('mention_comment', $_SESSION['id'], $id, $idCom);
                }
            }

            sendNotif('post_comment', $_SESSION['id'], $idSub, $idCom);
        }
        else{
            echo "<h5 class='error'>Impossible d'ajouter le commentaire.</h5>";
            $pubCom = $content;
        }
    }
}

function supprimerReponse($idSub, $idUsrA, $idAn) : void {
    global $connexion;

    $res = mysqli_query($connexion, "SELECT * FROM Answers WHERE idSub='$idSub' AND idAn='$idAn';");

    if($res && mysqli_num_rows($res) > 0){
        $row = mysqli_fetch_assoc($res);
        if($row['idUsrA'] == $idUsrA || ((isset($_SESSION['mod']) && $_SESSION['mod'] > 0))){
            $res = deleteOneAnswer(false, $idAn);
            if($res){
                echo "<h5 class='pass'>La réponse a été supprimée.</h5>";
            }
            else{
                echo "<h5 class='error'>Impossible de supprimer la réponse.</h5>";
            }
        }
        else{
            echo "<h5 class='error'>Impossible de supprimer la réponse.</h5>";
        }
    }
}

function sortByDate(array &$array) : void {
    for($i = 1; $i < count($array); $i++)
        inserer($array[$i], $array, $i);

}

function inserer($element_a_inserer, &$tab, $taille_gauche){
    for ($j = $taille_gauche; $j > 0 && new DateTime($tab[$j-1]['date']) < new DateTime($element_a_inserer['date']); $j--)
        $tab[$j] = $tab[$j-1];
    $tab[$j] = $element_a_inserer;
}

function verifyAnswerSetted($link, $filename, $date) : bool { // Vrai si elle est définie, faux sinon
    if($link != '' && $date != ''){
        return true;
    }
    elseif(isset($_FILES[$filename]) && !empty($_FILES[$filename]) && $date != ''){ // Si il y a un fichier
        return true;
    }

    return false;
}

function verifyAnswerValidy($dateShit, $dateAn, $content, $link, $filename, $answerNumber = 1, $muted = false) : int { // Retourne 0 si tout est valide, un chiffre d'erreur sinon
    if($link != '' && !preg_match(REGEX_TWITTER_LINK, $link) && !preg_match(REGEX_MASTODON_LINK, $link)){
        if(!$muted) echo "<h5 class='error'>Le lien est invalide pour la réponse $answerNumber.</h5>";
        return 1;
    }
    elseif(!(isset($_FILES[$filename]) && !empty($_FILES[$filename]))){ // Si il n'y a pas de fichier
        if(!$muted) echo "<h5 class='error'>Aucun fichier ni lien n'est défini pour la réponse $answerNumber.</h5>";
        return 4;
    }

    if(!preg_match(REGEX_DATE, $dateAn)){
        if(!$muted) echo "<h5 class='error'>La date est invalide pour la réponse $answerNumber.</h5>";
        return 2;
    }
    if(mb_strlen($content) > MAX_LEN_ANSWER_DSCR){
        if(!$muted) echo "<h5 class='error'>La description est trop longue pour la réponse $answerNumber.</h5>";
        return 3;
    }
    
    
    // Date plus récente que la date de la shitstorm
    $dateApp = date('Y-m-d');
    try{
        $dateT = new DateTime($dateAn);
        $actual = new DateTime($dateApp);
        $dateShitstorm = new DateTime($dateShit);

        if($dateT > $actual || $dateT < $dateShitstorm){
            if(!$muted) echo "<h5 class='error'>La date est invalide : trop vieille pour la réponse $answerNumber.</h5>";
            return 2;
        }
    } catch (Exception $E){
        if(!$muted) echo "<h5 class='error'>La date est invalide pour la réponse $answerNumber.</h5>";
        return 2;
    }

    return 0;
}

function isInConversation(&$row, int $idUsr) {
    return $row['idUsr1'] == $idUsr || $row['idUsr2'] == $idUsr;
}
