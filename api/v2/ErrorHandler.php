<?php

class ErrorHandler {
    protected $quit_at_error;

    protected $upcoming_errors = [];

    public const CLIENT = 'Client';
    public const SERVER = 'Server';
    public const UNKNOWN = 'Unknown';

    // CONSTANTES D'ERREUR
    protected const ERRORS = [1 => ['You must be logged to do that, or your grade is too low', 401],
                              2 => ['You are not able to do that', 403],
                              3 => ['Undefined handler', 404],
                              4 => ['Handler is unavailable', 404],
                              5 => ['Invalid or expired token', 403],
                              6 => ['Invalid or deleted user', 400],
                              7 => ['Date interval is invalid', 400],
                              8 => ['ID is needed', 400],
                              9 => ['Unable to fetch database', 500, self::SERVER],
                              10 => ['Shitstorm does not exists', 404],
                              11 => ['Application token is inexistant or invalid', 400],
                              12 => ['Unable to validate token. Please check your credentials or requested tokens', 400],
                              13 => ['Unable to find application', 400],
                              14 => ['Missing needed parameters', 400],
                              15 => ['This verifier token has already be used', 400],
                              16 => ['ID can not be equal or less to 0', 400],
                              17 => ['Target does not exists', 404],
                              18 => ['Username is invalid', 400],
                              19 => ['Email address is invalid', 400],
                              20 => ['Gender is invalid', 400],
                              21 => ['Realname is invalid', 400],
                              22 => ['Username is forbidden or already used', 400],
                              23 => ['Email is already used', 400],
                              24 => ['Application does not have rights to perform this action', 403],
                              25 => ['You email address has already been confirmed', 400],
                              26 => ['Confirmation mail has been sended recently', 400],
                              27 => ['Password is invalid', 400],
                              28 => ['Old password is invalid', 400],
                              29 => ['Old password can not be your new password', 400],
                              30 => ['Argument is invalid', 400],
                              31 => ['Accepted values are 0 or 1', 400],
                              32 => ['File is invalid', 400],
                              33 => ['Your twitter account is unreachable', 400],
                              34 => ['Endpoint is not selected', 400],
                              35 => ['You can not follow yourself', 400],
                              36 => ['This user is not followable', 400],
                              37 => ['Target request does not exists', 404],
                              38 => ['An unknown error occurred. Try again later', 500, self::SERVER],
                              39 => ['Content is too long', 400],
                              40 => ['Reply goes to inexistant comment', 400],
                              41 => ['You do not own this target', 400],
                              42 => ['Temporary limited action. Try again later', 400],
                              43 => ['Shitstorm can not have less than one link', 400],
                              44 => ['Link is not linked to this shitstorm', 400],
                              88 => ['Rate limit exceeded', 429]];

    public function __construct(bool $quit_when_error = true) {
        $this->quit_at_error = $quit_when_error;
    }

    public function changeQuitErrorMode(bool $new_mode) {
        $this->quit_at_error = $new_mode;
    }

    public function sendError(int $code, string $type = self::UNKNOWN, array $additionals_values = []) : array {
        // Enregistre les valeurs à afficher manuellement
        $returned = $additionals_values;

        if(!array_key_exists($code, self::ERRORS)) { // Si le code d'erreur n'existe pas
            throw new InvalidArgumentException('Error code is invalid');
        }
        $message = self::ERRORS[$code][0];
        $http_code = self::ERRORS[$code][1];
        
        if($type === self::UNKNOWN) {
            $type = self::ERRORS[$code][2] ?? self::CLIENT;
        }

        // Remplit le code d'erreur, le message et le type d'erreur
        $returned['error_code'] = $code;
        $returned['message'] = $message;
        $returned['error_type'] = $type;

        if($this->quit_at_error) { // Si on doit kill le script à cette erreur
            header('Content-Type: application/json');
            header($_SERVER['SERVER_PROTOCOL'] . ' ' . $this->getHeaderHTTPText($http_code));

            echo json_encode($returned);
            if(function_exists('deconnectBD')) {
                deconnectBD();
            }

            exit();
        }
        else {
            $this->upcoming_errors[] = $returned;
        }

        return $returned;
    }

    public function isUpcomingErrors() : bool {
        return (count($this->upcoming_errors) > 0);
    }

    /**
     * flushErrors
     * Flush encountered errors in JSON format and kill script
     *
     * @return void
     */
    public function flushErrors() : void {
        if(count($this->upcoming_errors) === 0) {
            return;
            exit();
        } 

        header('Content-Type: application/json');

        // Prend le dernier code d'erreur enregistré
        $code = $this->upcoming_errors[count($this->upcoming_errors) - 1]['error_code'];
        $http_code = self::ERRORS[$code][1];

        header($_SERVER['SERVER_PROTOCOL'] . ' ' . $this->getHeaderHTTPText($http_code));

        $returned = [];
        $returned['errors'] = $this->upcoming_errors;

        echo json_encode($returned);
        if(function_exists('deconnectBD')) {
            deconnectBD();
        }

        exit();
    }

    protected function getHeaderHTTPText(int $code) : string {
        switch($code) {
            case 400:
                return "400 Bad Request";
            case 401:
                return "401 Unauthorized";
            case 403:
                return "403 Forbidden";
            case 404:
                return "404 Not Found";
            case 429:
                return "429 Too Many Requests";
            case 500:
                return "500 Internal Server Error";
            default:
                throw new InvalidArgumentException("Code $code is unsupported.");
        }

        return "";
    }
}
