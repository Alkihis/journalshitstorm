<?php

function hasAccessToEndpoint(int $current, int $target) {
    return $current >= $target;
}

// -------------------------------------------------------------
// MAIN : Initialise les fonctions de l'API et charge le fichier
// -------------------------------------------------------------

require_once($_SERVER['DOCUMENT_ROOT'] . '/inc/constantes.php');

// Initialise le cookie de session pour tous les sous-domaines
$currentCookieParams = session_get_cookie_params(); 

session_set_cookie_params( 
    $currentCookieParams["lifetime"], 
    $currentCookieParams["path"], 
    DOT_DOMAIN, 
    $currentCookieParams["secure"], 
    $currentCookieParams["httponly"] 
); 

session_start();

// Accepte l'affichage des erreurs
ini_set('display_errors', 'on');

// Inclut les fichiers obligatoires
require_once(ROOT . '/inc/fonctions.php');

require_once(ROOT . '/api/v2/reserved/endpoints.php');
require_once(ROOT . '/api/v2/APIHandler.php');
require_once(ROOT . '/api/v2/Logger.php');
require_once(ROOT . '/api/v2/ErrorHandler.php');

// Charge les fichiers de "core json"
require_once(API_RESERVED_DIR . 'shitstorm_core_json.php');
require_once(API_RESERVED_DIR . 'comment_core_json.php');
require_once(API_RESERVED_DIR . 'user_core_json.php');

connectBD();

global $connexion;

loadSharedSettings();

if(!$GLOBALS['settings']['allowWebsiteAccess'] && !isModerator()) {
    header($_SERVER['SERVER_PROTOCOL'] . ' 503 Service Unavailable');
    exit();
}

$GLOBALS['error_handler'] = new ErrorHandler;

// L'utilisateur (si connecté) est dans $user_object
// On récupère le nom de la page à afficher

if(isset($_SERVER['REDIRECT_URL'])) {
    $GLOBALS['defined_path'] = $page = explode('/v2/', $_SERVER['REDIRECT_URL'])[1];

    // $page ressemble à shitstorm/get.json
    // On récupère la première partie (shitstorm ici)
    $GLOBALS['defined_page'] = $endp = explode('/', $page);

    $GLOBALS['defined_endpoint'] = null;
    $GLOBALS['defined_arg'] = null;
    if(isset($endp[1])) {
        $GLOBALS['defined_endpoint'] = $endp[1];
        if(isset($endp[2])) {
            $GLOBALS['defined_arg'] = $endp[2];
        }
    }

    $endp = $endp[0];

    if(empty(trim($endp))) {
        $GLOBALS['error_handler']->sendError(34);
    }
    else if(array_key_exists($endp, ENDPOINTS)) {
        $GLOBALS['endpoint_location'] = ROOT . '/api/v2/endpoints/' . ENDPOINTS[$endp]['path'];
        
        if(file_exists($GLOBALS['endpoint_location'] . 'available.php')) {
            $GLOBALS['endpoint_name'] = ENDPOINTS[$endp]['path'];
    
            // Connexion de l'utilisateur
            $user_object = null;
            $user_res = null;

            $force_login = isset($_REQUEST['force_cookie_connect']) && isActive($_REQUEST['force_cookie_connect']);
    
            $logger = new Logger($force_login);
    
            $user_res = $logger->getUserMysqliRes();
            $user_object = $logger->getUserObject();
            $user_status = $logger->getUserStatus();

            // Crée l'emplacement mémoire pour l'API Handler chargé par la fonction loadEndpointHandler()
            $api_handler = null;
    
            // On vérifie les droits d'accès à l'endpoint
            if(!hasAccessToEndpoint($user_status, ENDPOINTS[$endp]['usage'])) {
                $GLOBALS['error_handler']->sendError(2);
            }
    
            // Autorisation de charger le répertoire de l'endpoint
            require(ROOT . '/api/v2/handler.php');

            loadEndpointHandler($GLOBALS['endpoint_location'], $GLOBALS['endpoint_name']);
        }
        else {
            $GLOBALS['error_handler']->sendError(4);
        }
    }
    else {
        $GLOBALS['error_handler']->sendError(3);
    }
}

deconnectBD();
