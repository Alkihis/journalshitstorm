<?php

// Rate a shitstorm
// POST v2/rate/shitstorm/{like/dislike}.json
// ?id=:id

require_once(ROOT . '/inc/traitements.php');

function loadEndpoint() {
    global $api_handler;
    global $user_object;
    global $connexion;

    if(!$GLOBALS['allow_user_write']) {
        $GLOBALS['error_handler']->sendError(2);
    }

    $api_handler->addArg('id');

    // On cherche à fav ou unfav
    if($GLOBALS['defined_arg'] && $api_handler->getArg('id')) {
        $idSub = (int)$api_handler->getArg('id');
        $idUsr = (int)$user_object['id'];

        if($idSub > 0) {
            $resShitstormExists = mysqli_query($connexion, "SELECT idSub, idUsr, nbLikes FROM Shitstorms WHERE idSub='$idSub';");

            if($resShitstormExists && mysqli_num_rows($resShitstormExists) > 0) {
                $rowShitstorm = mysqli_fetch_assoc($resShitstormExists);

                $action = explode('.', $GLOBALS['defined_arg'])[0];

                $resPresence = mysqli_query($connexion, "SELECT * FROM ShitLikes WHERE idUsr='$idUsr' AND idSub='$idSub';");
                $isFavPresent = ($resPresence && mysqli_num_rows($resPresence) > 0);
                $is_liked = false;

                $current_like_count = $rowShitstorm['nbLikes'];

                if($isFavPresent) {
                    $rowPresence = mysqli_fetch_assoc($resPresence);

                    $is_liked = (bool)$rowPresence['isLike'];
                }

                if($action === 'like') {
                    $action = 'liked';
                    // On va fav la shitstorm en question
                    if(!$isFavPresent) { //  Si l'utilisateur n'a pas encore laissé de note
                        $resInser = mysqli_query($connexion, "INSERT INTO ShitLikes (idSub, isLike, idUsr) VALUES ('$idSub', 1, '$idUsr');");

                        if(!$resInser) {
                            $GLOBALS['error_handler']->sendError(9);
                        }
                        $current_like_count++;
                    }
                    else if (!$is_liked) { // Si l'utilisateur a laissé une note, mais elle est négative
                        $resInser = mysqli_query($connexion, "UPDATE ShitLikes SET isLike=1 WHERE idSub='$idSub' AND idUsr='$idUsr';");

                        if(!$resInser) {
                            $GLOBALS['error_handler']->sendError(9);
                        }
                        $current_like_count += 2;
                    }
                    else { // L'utilisateur a laissé une note positive, on a la retire
                        $resInser = mysqli_query($connexion, "DELETE FROM ShitLikes WHERE idSub='$idSub' AND idUsr='$idUsr';");

                        if(!$resInser) {
                            $GLOBALS['error_handler']->sendError(9);
                        }
                        $current_like_count--;
                        $action = 'unrated';
                    }

                    $actualise_like = mysqli_query($connexion, "UPDATE Shitstorms SET nbLikes='$current_like_count' WHERE idSub='$idSub';");

                    if($action === 'liked' && $rowShitstorm['idUsr'] != 0 && $user_object['id'] != $rowShitstorm['idUsr'])
                        sendNotif('like_shitstorm', $user_object['id'], $rowShitstorm['idUsr'], $idSub);

                    // Retourne la shitstorm concernée
                    return getJSONShitstorm(null, $idSub);
                }
                else if ($action === 'dislike') {
                    $action = 'disliked';
                    // On va unlike la shitstorm en question
                    if(!$isFavPresent) { //  Si l'utilisateur n'a pas encore laissé de note
                        $resInser = mysqli_query($connexion, "INSERT INTO ShitLikes (idSub, isLike, idUsr) VALUES ('$idSub', 0, '$idUsr');");

                        if(!$resInser) {
                            $GLOBALS['error_handler']->sendError(9);
                        }
                        $current_like_count--;
                    }
                    else if ($is_liked) { // Si l'utilisateur a laissé une note, mais elle est positive
                        $resInser = mysqli_query($connexion, "UPDATE ShitLikes SET isLike=0 WHERE idSub='$idSub' AND idUsr='$idUsr';");

                        if(!$resInser) {
                            $GLOBALS['error_handler']->sendError(9);
                        }
                        $current_like_count -= 2;
                    }
                    else { // L'utilisateur a laissé une note négative, on a la retire
                        $resInser = mysqli_query($connexion, "DELETE FROM ShitLikes WHERE idSub='$idSub' AND idUsr='$idUsr';");

                        if(!$resInser) {
                            $GLOBALS['error_handler']->sendError(9);
                        }
                        $current_like_count++;
                        $action = 'unrated';
                    }

                    $actualise_like = mysqli_query($connexion, "UPDATE Shitstorms SET nbLikes='$current_like_count' WHERE idSub='$idSub';");

                    if($action === 'disliked' && $rowShitstorm['idUsr'] != 0 && $user_object['id'] != $rowShitstorm['idUsr'])
                        sendNotif('dislike_shitstorm', $user_object['id'], $rowShitstorm['idUsr'], $idSub);

                    return getJSONShitstorm(null, $idSub);
                }
                else {
                    $GLOBALS['error_handler']->sendError(14);
                }
            }
            else {
                $GLOBALS['error_handler']->sendError(17);
            }
        }
        else {
            $GLOBALS['error_handler']->sendError(16);
        }
    }
    else {
        $GLOBALS['error_handler']->sendError(14);
    }
}
