<?php

// Describe available endpoints in this directory [RATE]

const AVAILABLE = ['shitstorm' => ['name' => 'rate_shitstorm.php', 'usage' => LOGGED, 'rate_limit' => 300, 'method' => 'POST', 'rights' => READ_WRITE],
                   'comment' => ['name' => 'rate_comment.php', 'usage' => LOGGED, 'rate_limit' => 300, 'method' => 'POST', 'rights' => READ_WRITE],
                   'fetch' => ['name' => 'get_rates.php', 'usage' => ALL, 'rate_limit' => 600, 'method' => 'GET', 'rights' => READ_ONLY]];

