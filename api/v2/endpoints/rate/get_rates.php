<?php

// Get rates from shitstorm or comment
// POST v2/rate/fetch/{shitstorm/comment}.json
// ?id=:id

require_once(API_RESERVED_DIR . 'user_core_json.php');

function getActualLikes(int $idSub){
    global $connexion;

    $resNbL = mysqli_query($connexion, "SELECT * FROM Shitstorms WHERE idSub='$idSub';"); // Récupération du nombre de like enregistré
    if($resNbL && mysqli_num_rows($resNbL)){
        $row = mysqli_fetch_assoc($resNbL);
        return $row['nbLikes'];
    }
    else return null;
}

function getActualFavs(int $idCom){
    global $connexion;

    $resNbL = mysqli_query($connexion, "SELECT COUNT(*) c FROM ComLikes WHERE idCom='$idCom';"); // Récupération du nombre de like enregistré
    if($resNbL && mysqli_num_rows($resNbL)){
        $row = mysqli_fetch_assoc($resNbL);
        return $row['c'];
    }
    else return null;
}

function loadEndpoint() {
    global $api_handler;
    global $user_object;
    global $connexion;

    $api_handler->addArg('id');
    $api_handler->addArg('mode');
    $api_handler->addArg('max_id');
    $api_handler->addArg('since_id');
    $api_handler->addArg('count', 20);

    // On cherche à obtenir les notes
    if($GLOBALS['defined_arg'] && $api_handler->getArg('id')) {
        $id = (int)$api_handler->getArg('id');
        $idUsr = (int)$user_object['id'];

        if($id > 0) {
            $action = explode('.', $GLOBALS['defined_arg'])[0];
            $extended_fetch_mode = ($api_handler->getArg('mode') === 'extended');
            $since_id = $max_id = null;
            $count = 20;
            if($extended_fetch_mode) {
                $max_id = (int)$api_handler->getArg('max_id');
                if($max_id <= 0) {
                    $max_id = null;
                } 

                $since_id = (int)$api_handler->getArg('since_id');
                if($since_id <= 0) {
                    $since_id = null;
                }

                $c = (int)$api_handler->getArg('count');
                if($c > 0 && $c <= 100) {
                    $count = $c;
                }
            }

            if($action === 'shitstorm') {
                $api_handler->addArg('type', 'both');
                $type = $api_handler->getArg('type');

                if($type !== 'both' && $type !== 'dislikes' && $type !== 'likes') {
                    $type = 'both';
                }

                if(!$extended_fetch_mode) { // Si on veut juste les notes associées et pas le détail
                    $rate = (int)getActualLikes($id);

                    if($rate === null) {
                        $GLOBALS['error_handler']->sendError(17);
                    }
                    else {
                        return ['rate' => $rate, 'id_shitstorm' => $id];
                    }
                }
                else { // Détail (utilisateurs, rate calculé correctement...)
                    // Vérification que la shitstorm existe
                    $resShitstormExists = mysqli_query($connexion, "SELECT idSub, nbLikes FROM Shitstorms WHERE idSub='$id';");
                    $like_count = $dislike_count = $real_count = null;

                    if($resShitstormExists && mysqli_num_rows($resShitstormExists) > 0) {
                        $rowShitstorm = mysqli_fetch_assoc($resShitstormExists);

                        if($type === 'both') {
                            // Calcul du nombre réel de likes
                            $resLikes = mysqli_query($connexion, "SELECT idSLike FROM ShitLikes WHERE isLike=1 AND idSub='$id';");
                            if(!$resLikes) {
                                $GLOBALS['error_handler']->sendError(9);
                            }

                            $like_count = mysqli_num_rows($resLikes);

                            // Calcul du nombre réel de dislikes
                            $resDislikes = mysqli_query($connexion, "SELECT idSLike FROM ShitLikes WHERE isLike=0 AND idSub='$id';");
                            if(!$resDislikes) {
                                $GLOBALS['error_handler']->sendError(9);
                            }

                            $dislike_count = mysqli_num_rows($resDislikes);

                            $real_count = $like_count - $dislike_count;
                            if($real_count != (int)$rowShitstorm['nbLikes']) { // On actualise la shitstorm si le nombre calculé est != de l'enregistré
                                mysqli_query($connexion, "UPDATE Shitstorms SET nbLikes=$real_count WHERE idSub='$id';");
                            }
                        }

                        $likes = [];
                        $dislikes = [];
                        $next_max_id = 0;

                        if($type !== 'both' || $like_count + $dislike_count > 0) { // Si il y a au moins une note
                            $resLikes = mysqli_query($connexion, "SELECT * FROM ShitLikes 
                                                                  WHERE idSub='$id' "
                                                                  .($max_id ? " AND idSLike < $max_id " : '')
                                                                  .($since_id ? " AND idSLike > $since_id " : '')
                                                                  .($type !== 'both' ? " AND isLike=" . ($type === 'likes' ? '1' : '0') : '').
                                                                  " ORDER BY idSLike DESC LIMIT 0,$count;");
                            
                            if($resLikes && mysqli_num_rows($resLikes)) {
                                // Première requête : aucun paramètre
                                // Deuxième requête : max_id = plus bas ID reçu
                                // Recommence la requête tant que likes reçus == count défini (20 par défaut)

                                $next_max_id = PHP_INT_MAX;
                                $i = 0;
                                while($rowLikes = mysqli_fetch_assoc($resLikes)) {
                                    if((int)$rowLikes['isLike']) {
                                        $likes[] = [
                                            'id_like' => (int)$rowLikes['idSLike'],
                                            'date' => $rowLikes['likeDate'],
                                            'id_shitstorm' => (int)$rowLikes['idSub'],
                                            'user' => getUserJSONCore(((int)$rowLikes['idUsr']), true, true)
                                        ];
                                    }
                                    else {
                                        $dislikes[] = [
                                            'id_like' => (int)$rowLikes['idSLike'],
                                            'date' => $rowLikes['likeDate'],
                                            'id_shitstorm' => (int)$rowLikes['idSub'],
                                            'user' => getUserJSONCore(((int)$rowLikes['idUsr']), true, true) // On obtient l'utilisateur, mais trimmé
                                        ];
                                    }

                                    if((int)$rowLikes['idSLike'] < $next_max_id) {
                                        $next_max_id = (int)$rowLikes['idSLike'];
                                    }
                                    $i++;
                                }

                                if($i !== $count) {
                                    $next_max_id = 0;
                                }
                            }
                        }

                        return ['id_shitstorm' => $id, 'rate' => $real_count, 'positive_rates' => $like_count, 'negative_rates' => $dislike_count, 
                                'likes' => $likes, 'dislikes' => $dislikes, 'next_max_id' => $next_max_id];
                    }
                    else {
                        $GLOBALS['error_handler']->sendError(17);
                    }
                }
            }
            else if ($action === 'comment') {
                if(!$extended_fetch_mode) {
                    $rate = (int)getActualFavs($id);

                    if($rate === null) {
                        $GLOBALS['error_handler']->sendError(17);
                    }
                    else {
                        return ['rate' => $rate, 'id_comment' => $id];
                    }
                }
                else {
                    // Vérification que la shitstorm existe
                    $commentExists = mysqli_query($connexion, "SELECT idCom FROM Comment WHERE idCom='$id';");

                    if($commentExists && mysqli_num_rows($commentExists)) {
                        $comlikes = (int)getActualFavs($id);

                        if($comlikes) {
                            $fav_count = $comlikes;

                            $favs = [];
                            $i = 0;
                            $next_max_id = 0;
                            // On recherche les likes de commentaires par ID, en tri avec max id et since id
                            $comments = mysqli_query($connexion, "SELECT * FROM ComLikes WHERE idCom='$id' ".
                                                    ($max_id ? " AND idCLike < $max_id " : '').
                                                    ($since_id ? " AND idCLike > $since_id " : '')." ORDER BY idCLike DESC LIMIT 0,$count;");

                            // Si des likes existent
                            if($comments && mysqli_num_rows($comments)) {
                                $next_max_id = PHP_INT_MAX;
                                while($rowComment = mysqli_fetch_assoc($comments)) {
                                    $favs[] = [
                                        'id_like' => (int)$rowComment['idCLike'],
                                        'id_comment' => (int)$rowComment['idCom'],
                                        'user' => getUserJSONCore((int)$rowComment['idUsr'], true, true)
                                    ];

                                    if((int)$rowComment['idCLike'] < $next_max_id) {
                                        $next_max_id = (int)$rowComment['idCLike'];
                                    }
                                    $i++;
                                }

                                if($i !== $count) {
                                    $next_max_id = 0;
                                }
                            }

                            return ['id_comment' => $id, 'rate' => $comlikes, 'favorites' => $favs, 'next_max_id' => $next_max_id];
                        }
                        else {
                            return ['id_comment' => $id, 'rate' => $comlikes, 'favorites' => [], 'next_max_id' => 0];
                        }
                    }
                    else {
                        $GLOBALS['error_handler']->sendError(17);
                    }
                }
            }
            else {
                $GLOBALS['error_handler']->sendError(14);
            }
        }
        else {
            $GLOBALS['error_handler']->sendError(16);
        }
    }
    else {
        $GLOBALS['error_handler']->sendError(14);
    }
}
