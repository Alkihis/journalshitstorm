<?php

// Rate a comment
// POST v2/rate/comment/{fav/unfav}.json
// ?id=:id

require_once(ROOT . '/inc/traitements.php');

function loadEndpoint() {
    global $api_handler;
    global $user_object;
    global $connexion;

    // Empêche d'écrire si jamais l'accès est refusé pour l'application
    if(!$GLOBALS['allow_user_write']) {
        $GLOBALS['error_handler']->sendError(2);
    }

    $api_handler->addArg('id');

    // On cherche à fav ou unfav
    if($GLOBALS['defined_arg'] && $api_handler->getArg('id')) {
        $idCom = (int)$api_handler->getArg('id');
        $idUsr = (int)$user_object['id'];

        if($idCom > 0) {
            $resCommentExist = mysqli_query($connexion, "SELECT idCom, idUsr FROM Comment WHERE idCom='$idCom';");

            if($resCommentExist && mysqli_num_rows($resCommentExist) > 0) {
                $rowComment = mysqli_fetch_assoc($resCommentExist);

                $action = explode('.', $GLOBALS['defined_arg'])[0];

                $resPresence = mysqli_query($connexion, "SELECT * FROM ComLikes WHERE idUsr='$idUsr' AND idCom='$idCom';");
                $isFavPresent = ($resPresence && mysqli_num_rows($resPresence) > 0);

                if($action === 'fav') {
                    // On va fav le commentaire en question
                    if(!$isFavPresent) {
                        $resInser = mysqli_query($connexion, "INSERT INTO ComLikes (idCom, idUsr) VALUES ('$idCom', '$idUsr');");

                        if(!$resInser) {
                            $GLOBALS['error_handler']->sendError(9);
                        }
                    }
                    // On regarde le nouveau compte de fav
                    $resNbL = mysqli_query($connexion, "SELECT COUNT(*) c FROM ComLikes WHERE idCom='$idCom';");
                    if(!$resNbL){
                        $GLOBALS['error_handler']->sendError(9);
                    }
                    $row = mysqli_fetch_assoc($resNbL);
                    $fav_count = (int)$row['c'];

                    if($rowComment['idUsr'] != 0 && $user_object['id'] != $rowComment['idUsr'])
                        sendNotif('like_comment', $user_object['id'], $rowComment['idUsr'], $idCom);

                    // On renvoie le commentaire associé au fav
                    return getJSONComment(null, $idCom);
                }
                else if ($action === 'unfav') {
                    // On va unfav le commentaire en question
                    if($isFavPresent) {
                        $resInser = mysqli_query($connexion, "DELETE FROM ComLikes WHERE idCom='$idCom' AND idUsr='$idUsr';");

                        if(!$resInser) {
                            $GLOBALS['error_handler']->sendError(9);
                        }
                    }
                    // On regarde le nouveau compte de fav
                    $resNbL = mysqli_query($connexion, "SELECT COUNT(*) c FROM ComLikes WHERE idCom='$idCom';");
                    if(!$resNbL){
                        $GLOBALS['error_handler']->sendError(9);
                    }
                    $row = mysqli_fetch_assoc($resNbL);
                    $fav_count = (int)$row['c'];

                    return getJSONComment(null, $idCom);
                }
                else {
                    $GLOBALS['error_handler']->sendError(14);
                }
            }
            else {
                $GLOBALS['error_handler']->sendError(17);
            }
        }
        else {
            $GLOBALS['error_handler']->sendError(16);
        }
    }
    else {
        $GLOBALS['error_handler']->sendError(14);
    }
}
