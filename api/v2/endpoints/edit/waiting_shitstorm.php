<?php

// Modify an existing shitstorm

require INC_DIR . 'traitements.php';
require INC_DIR . 'postDelete.php';

function loadEndpoint() {
    if(!$GLOBALS['settings']['allowAllModification']){ // On a pas le droit de modifier
        $GLOBALS['error_handler']->sendError(42);
    }

    global $connexion;
    global $api_handler;
    global $user_object;

    $api_handler->addArg('id');

    $id = (int)$api_handler->getArg('id');

    if($id > 0) {
        // Verif ownership of shitstorm
        if($user_object['moderator'] <= 0) {
            $res = mysqli_query($connexion, "SELECT idSub, idUsr FROM Waiting WHERE idUsr={$user_object['id']} AND idSub=$id;");

            if(!$res || mysqli_num_rows($res) === 0) {
                $GLOBALS['error_handler']->sendError(41);
            }
        }

        $shitstorm = getJSONWaitingShitstorm($id, false, false);
        // Will crash if SS does not exists

        // TO DO
        isActive($api_handler->addGetArg('delete')); // delete the shitstorm
        $api_handler->addGetArg('add_link'); // add a new twitter/masto link
        $api_handler->addGetArg('invert_two_links'); // invert two links
        $api_handler->addGetArg('add_media_data'); // base64 img
        // END TO DO

        $remove = (int)$api_handler->addGetArg('remove_link'); // delete a link by link ID

        if($remove > 0) {
            removeShitstormLink($remove, $shitstorm);
        }

        return getJSONWaitingShitstorm($id);
    }
    else {
        $GLOBALS['error_handler']->sendError(16);
    }
}

function removeShitstormLink(int $id, &$shitstorm) : void {
    // Vérification si le lien est dans la shitstorm ET si il y a plus d'un lien dans celle-ci
    if(count($shitstorm['links']) <= 1) {
        $GLOBALS['error_handler']->sendError(43);
    }

    $allow = false;
    foreach($shitstorm['links'] as $l) {
        if((int)$l['id_link'] === $id) {
            $allow = true;
            break;
        }
    }

    if(!$allow) {
        $GLOBALS['error_handler']->sendError(44);
    }

    deleteOneLink(true, $id);
}
