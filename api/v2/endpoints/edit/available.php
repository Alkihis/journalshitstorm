<?php

// Describe available endpoints in this directory [EDIT]

const AVAILABLE = ['shitstorm.json' => ['name' => 'shitstorm.php', 'usage' => LOGGED, 'rate_limit' => 30, 'method' => 'POST', 'rights' => READ_WRITE],
                   'answer.json' => ['name' => 'answer.php', 'usage' => LOGGED, 'rate_limit' => 30, 'method' => 'POST', 'rights' => READ_WRITE],
                   'waiting.json' => ['name' => 'waiting_shitstorm.php', 'usage' => LOGGED, 'rate_limit' => 30, 'method' => 'POST', 'rights' => READ_WRITE]];
