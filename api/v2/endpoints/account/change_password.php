<?php

// Change password for current user

function loadEndpoint() {
    global $connexion;
    global $user_object;
    global $api_handler;

    $api_handler->addArg('old_password');
    $api_handler->addArg('new_password');

    if($api_handler->getArg('old_password') && $api_handler->getArg('new_password')) {
        // Vérification de l'ancien mot de passe
        $old_psw = $api_handler->getArg('old_password');

        $new_psw = $api_handler->getArg('new_password');

        $old_psw = mysqli_real_escape_string($connexion, $old_psw);

        if($new_psw !== $old_psw){
            if(password_verify($old_psw, $user_object['passw'])){
                $new_psw = mysqli_real_escape_string($connexion, $new_psw);
                if(!preg_match(REGEX_PASSWORD, $new_psw)){
                    $GLOBALS['error_handler']->sendError(27);
                }
                else{
                    $hashnew = password_hash($new_psw, PASSWORD_BCRYPT);
                    $res = mysqli_query($connexion, "UPDATE Users SET passw='$hashnew' WHERE id='{$user_object['id']}';");
                    if($res){
                        require('');

                        return ['update' => 'success', 'user' => getUserJSONCore($user_object['id'], false, false, true)];
                    }
                    else{
                        $GLOBALS['error_handler']->sendError(9);
                    }
                }
            }
            else{
                $GLOBALS['error_handler']->sendError(29);
            }
        }
        else{
            $GLOBALS['error_handler']->sendError(28);
        }
    }
    else {
        $GLOBALS['error_handler']->sendError(14);
    }
}
