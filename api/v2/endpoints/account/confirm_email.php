<?php

// Send confirmation e-mail for user

function loadEndpoint() {
    global $connexion;
    global $user_object;

    $current_date = gmdate('Y-m-d H:i:s');
    $real_token = md5(substr($user_object['passw'], 9, 29-9) . $current_date); // Token généré

    if($user_object['confirmedMail']){
        $GLOBALS['error_handler']->sendError(25);
    }
    else{
        require_once(ROOT . '/inc/send_mail.php');

        if(preg_match(REGEX_MAIL, $user_object['mail'])){
            $dateLast = new DateTime($user_object['lastCMailSent'] . ' UTC');
            $dateLast = $dateLast->add(new DateInterval('PT30M')); // Ajoute 30 minutes à la dateLast
            $dateActual = new DateTime(gmdate('Y-m-d H:i:s e')); // e est la timezone (UTC)

            if($dateLast < $dateActual){ // Si ça fait plus de 30 minutes avant le dernier mail
                $send = sendConfirmationMail($user_object['mail'], $real_token);
                if($send){
                    mysqli_query($connexion, "UPDATE Users SET lastCMailSent='$current_date' WHERE id='{$user_object['id']}';");

                    return ['confirmation_mail' => 'sended', 'mail' => $user_object['mail']];
                }
                else {
                    $GLOBALS['error_handler']->sendError(9);
                }
            }
            else{
                $GLOBALS['error_handler']->sendError(26);
            }
        }
        else {
            $GLOBALS['error_handler']->sendError(19);
        }
    }
}
