<?php

// Change follow mode

function loadEndpoint() {
    global $connexion;
    global $api_handler;
    global $user_object;

    $api_handler->addArg('accept_follow');

    if($api_handler->getArg('accept_follow')) {
        $newStatus = $api_handler->getArg('accept_follow');

        // Transforme la chaîne en valeur acceptable pour la BDD
        if($newStatus === 'followable'){
            $newStatus = 1;
        }
        elseif($newStatus === 'hidden'){
            $newStatus = 0;
        }
        else {
            $GLOBALS['error_handler']->sendError(30);
        }

        // Si le nouveau status est le statut actuel, rien n'est à faire
        if($newStatus == $user_object['followable']) {
            return getUserJSONCore($user_object['id'], false, false, true);
        }
    
        $res = mysqli_query($connexion, "UPDATE Users SET followable='$newStatus' WHERE id='{$user_object['id']}';");
        if($res){
            if($newStatus == 0){ // Si le nouveau status est d'être caché, on supprime tous les abonnés de l'utilisateur
                mysqli_query($connexion, "DELETE FROM Followings WHERE idFollowed='{$user_object['id']}';");
            }
    
            // On renvoie en succès l'user object (il contient de toute manière la clé "followable")
            return getUserJSONCore($user_object['id'], false, false, true);
        }
        else{
            $GLOBALS['error_handler']->sendError(9);
        }
    }
    else {
        $GLOBALS['error_handler']->sendError(14);
    }
}
