<?php

// Obtain user credentials (user object)

function loadEndpoint() {
    global $user_object;

    require_once(API_RESERVED_DIR . 'user_core_json.php');

    return getUserJSONCore($user_object['id'], false, false, true);
}
