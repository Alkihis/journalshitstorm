<?php

// Mettre à jour le profil
// Adresse e-mail, username, realname et genre.

function reverseGender(string $gender) : string {
    switch($gender) {
        case 'male':
            return 'm';
        case 'female':
            return 'f';
        case 'genderqueer':
            return 'b';
        case 'agender':
            return 'a';
        default:
            $GLOBALS['error_handler']->sendError(20);
    }
}

function updateUser(string $new_mail, string $new_gr, string $new_usr, string $new_rn) : void {
    global $connexion;
    global $user_object;
    $usrname_set = false;
    $mail_set = false;
    if(!$new_mail) {
        $new_mail = null;
    }

    $query = "UPDATE Users SET ";
    $set = '';

    if($new_mail !== $user_object['mail']) {
        $new_mail = mysqli_real_escape_string($connexion, $new_mail);

        $set .= "mail='$new_mail', confirmedMail=0";
        $mail_set = true;
    }

    if($new_gr !== $user_object['genre']) {
        $new_gr = mysqli_real_escape_string($connexion, $new_gr);

        if($set) {
            $set .= ", ";
        }
        $set .= "genre='$new_gr'";
    }

    if($new_rn !== $user_object['realname']) {
        $new_rn = mysqli_real_escape_string($connexion, $new_rn);

        if($set) {
            $set .= ", ";
        }
        $set .= "realname='$new_rn'";
    }

    if($new_usr !== $user_object['usrname']) {
        $new_usr = mysqli_real_escape_string($connexion, $new_usr);

        if($set) {
            $set .= ", ";
        }
        $set .= "usrname='$new_usr'";
        $usrname_set = true;
    }

    if($set) {
        $query .= $set . " WHERE id={$user_object['id']};";

        $res = mysqli_query($connexion, $query);

        if($usrname_set && $res) {
            // Modif dans la table Shitstorm du pseudo enregistré
            $res2 = mysqli_query($connexion, "UPDATE Shitstorms SET pseudo='$new_usr' WHERE idUsr='{$user_object['id']}';");

            if(!$res2) {
                $GLOBALS['error_handler']->sendError(9);
            }
        }

        if(!$res) {
            $GLOBALS['error_handler']->sendError(9);
        }
    }
}

function loadEndpoint() {
    global $connexion;
    global $user_object;
    global $api_handler;

    $api_handler->addArg('mail');
    $api_handler->addArg('realname');
    $api_handler->addArg('username');
    $api_handler->addArg('gender');

    $new_mail = $user_object['mail'];
    $new_rn = $user_object['realname'];
    $new_usr = $user_object['usrname'];
    $new_gr = $user_object['genre'];

    if($api_handler->getArg('mail') && $api_handler->getArg('mail') !== $user_object['mail']) {
        if(preg_match(REGEX_MAIL, trim($api_handler->getArg('mail')))) {
            $new_mail = mysqli_real_escape_string($connexion, trim($api_handler->getArg('mail')));

            $res = mysqli_query($connexion, "SELECT id FROM Users WHERE mail='$new_mail';");

            if(mysqli_num_rows($res) > 0){
                $GLOBALS['error_handler']->sendError(23);
            }
        }
        else {
            $GLOBALS['error_handler']->sendError(19);
        }
    }

    if($api_handler->getArg('realname')) {
        if(preg_match(REGEX_REALNAME, trim($api_handler->getArg('realname')))) {
            $new_rn = trim($api_handler->getArg('realname'));
        }
        else {
            $GLOBALS['error_handler']->sendError(21);
        }
    }

    if($api_handler->getArg('gender')) {
        $gender_tmp = $api_handler->getArg('gender');
        $gender_tmp = reverseGender($gender_tmp);

        $new_gr = $gender_tmp;
    }

    if($api_handler->getArg('username') && $api_handler->getArg('username') !== $user_object['usrname']) {
        $new_usr = trim($api_handler->getArg('username'));

        if(preg_match(REGEX_USERNAME, $new_usr)) {
            // Vérif si il est dans la table des références
            include($_SERVER['DOCUMENT_ROOT'] .'/static/pagesreferences.php');

            $forbidden = ['administrateur', 'administrator', 'moderator', 'moderateur', 'alki'];

            if(in_array(strtolower($new_usr), array_map('strtolower', array_values($forbidden))) || in_array(strtolower($new_usr), array_map('strtolower', array_keys(PAGES_REFERENCES)))){
                $GLOBALS['error_handler']->sendError(22);
            }

            // Vérif si il est déjà pris
            $new_usr = mysqli_real_escape_string($connexion, $new_usr);

            $res = mysqli_query($connexion, "SELECT id FROM Users WHERE usrname='$new_usr';");
            if(!$res || @mysqli_num_rows($res)){
                $GLOBALS['error_handler']->sendError(22);
            }
        }
        else {
            $GLOBALS['error_handler']->sendError(18);
        }
    }

    updateUser($new_mail, $new_gr, $new_usr, $new_rn);

    // Retourne l'objet utilisateur actualisé
    return getUserJSONCore($user_object['id'], false, false, true);
}
