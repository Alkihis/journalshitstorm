<?php

// Describe available endpoints in this directory

const AVAILABLE = ['remove_application.json' => ['name' => 'remove_app.php', 'usage' => LOGGED, 'rate_limit' => 15, 'method' => 'POST', 'rights' => READ_WRITE],
                   'update.json' => ['name' => 'update_profile.php', 'usage' => LOGGED, 'rate_limit' => 30, 'method' => 'POST', 'rights' => READ_WRITE],
                   'credentials.json' => ['name' => 'credentials.php', 'usage' => LOGGED, 'rate_limit' => 75, 'method' => 'GET', 'rights' => READ_ONLY],
                   'confirm_email.json' => ['name' => 'confirm_email.php', 'usage' => LOGGED, 'rate_limit' => 1, 'method' => 'GET', 'expiration_time_limit' => 30, 'rights' => READ_WRITE],
                   'visual_status.json' => ['name' => 'visual_status.php', 'usage' => LOGGED, 'rate_limit' => 50, 'method' => 'POST', 'rights' => READ_WRITE],
                   'follow_status.json' => ['name' => 'follow_status.php', 'usage' => LOGGED, 'rate_limit' => 50, 'method' => 'POST', 'rights' => READ_WRITE],
                   'password.json' => ['name' => 'change_password.php', 'usage' => LOGGED, 'rate_limit' => 5, 'method' => 'POST', 'rights' => READ_WRITE_PASSWORD]];
