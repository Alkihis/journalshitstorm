<?php

// Suppression d'une application configurée, avec son UserID

function loadEndpoint() {
    global $connexion;
    global $user_object;
    global $api_handler;

    $api_handler->addArg('application_id');

    if($api_handler->getArg('application_id')) {
        $id = (int)$api_handler->getArg('application_id');

        if($id > 0) {
            // Vérification que l'utilisateur a bien une application correspondant à cet ID configuré
            $res = mysqli_query($connexion, "SELECT idToken FROM Tokens WHERE tokenType='API' AND idApp='$id' AND idUsr='{$user_object['id']}';");

            if($res && mysqli_num_rows($res)) { // Il y a bien une app avec cet ID
                $row = mysqli_fetch_assoc($res);

                $res = mysqli_query($connexion, "DELETE FROM Tokens WHERE idToken={$row['idToken']};");
                if(!$res) {
                    $GLOBALS['error_handler']->sendError(9);
                }
            }
            // Sinon : Application inexistante / déjà supprimée. Ce n'est PAS une erreur.

            return ['deleted_application' => $id];
        }
        else {
            $GLOBALS['error_handler']->sendError(16);
        }
    }
    else {
        $GLOBALS['error_handler']->sendError(8);
    }
}

