<?php

// Change storm or snow

function loadEndpoint() {
    global $api_handler;
    global $connexion;
    global $user_object;

    $api_handler->addArg('storm_mode');
    $api_handler->addArg('snow_mode');

    if($api_handler->getArg('storm_mode') !== null) {
        $newMode = (int)isActive($api_handler->getArg('storm_mode'));

        if($newMode === 0 || $newMode === 1){
            // Actualisation BDD
            $res = mysqli_query($connexion, "UPDATE Users SET storm='$newMode' WHERE id='{$user_object['id']}';");
    
            if($res){
                return getUserJSONCore($user_object['id'], false, false, true);
            }
            else{
                $GLOBALS['error_handler']->sendError(9);
            }
        }
        else{
            $GLOBALS['error_handler']->sendError(31);
        }
    }
    else if($api_handler->getArg('snow_mode') !== null) {
        $newMode = (int)isActive($api_handler->getArg('snow_mode'));

        if($newMode === 0 || $newMode === 1){
            // Actualisation BDD
            $res = mysqli_query($connexion, "UPDATE Users SET snow='$newMode' WHERE id='{$user_object['id']}';");
    
            if($res){
                return getUserJSONCore($user_object['id'], false, false, true);
            }
            else{
                $GLOBALS['error_handler']->sendError(9);
            }
        }
        else{
            $GLOBALS['error_handler']->sendError(31);
        }
    }
    else {
        $GLOBALS['error_handler']->sendError(14);
    }
}
