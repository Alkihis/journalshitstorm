<?php

// Describe available endpoints in this directory [SAVES]

const AVAILABLE = ['get' => ['name' => 'get_save.php', 'usage' => VISITOR, 'rate_limit' => 300, 'method' => 'GET', 'rights' => READ_ONLY],
                   'create.json' => ['name' => 'create.php', 'usage' => LOGGED, 'rate_limit' => 30, 'method' => 'POST', 'rights' => READ_WRITE, 'expiration_time_count' => 180],
                   'orphans.json' => ['name' => 'orphans.php', 'usage' => VISITOR, 'rate_limit' => 60, 'method' => 'GET', 'rights' => READ_ONLY],
                   'delete.json' => ['name' => 'delete.php', 'usage' => MODO, 'rate_limit' => 50, 'method' => 'POST', 'rights' => READ_WRITE]];
