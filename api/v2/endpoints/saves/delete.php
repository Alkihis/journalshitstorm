<?php

// Delete a tweet save

require_once INC_DIR . 'postDelete.php';

function loadEndpoint() {
    global $connexion;
    global $api_handler;

    $api_handler->addArg('id');

    if($api_handler->getArg('id')) {
        $id = (int)$api_handler->getArg('id');

        if($id) {
            // Vérifie qu'une sauvegarde existe, déjà
            $res = mysqli_query($connexion, "SELECT idSave FROM tweetSave WHERE idTweet=$id");

            if($res && mysqli_num_rows($res)) {
                $res = deleteTweetSave($id, true);
                return ['deleted' => (bool)$res];
            }
            else {
                $GLOBALS['error_handler']->sendError(17);
            }
        }
        else {
            $GLOBALS['error_handler']->sendError(16);
        }
    }
    else {
        $GLOBALS['error_handler']->sendError(14);
    }
}
