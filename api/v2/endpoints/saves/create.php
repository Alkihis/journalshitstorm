<?php

// Create a save of a tweet
require_once INC_DIR . 'traitements.php';
require_once INC_DIR . 'generated.php';
require_once INC_DIR . 'postDelete.php';

function loadEndpoint() {
    global $api_handler;
    global $connexion;

    $api_handler->addArg('id_tweet');
    $api_handler->addArg('link');

    $id = null;
    if($api_handler->getArg('id_tweet')) {
        $id = (int)$api_handler->getArg('id_tweet'); // Server must be 64 bits
    }
    else if($api_handler->getArg('link')) {
        $id = (int)getTweetIdFromURL($api_handler->getArg('link'));
    }
    else {
        $GLOBALS['error_handler']->sendError(14);
    }
    
    if($id) {
        // Vérif si la sauvegarde existe
        $tweet = getTweetFromSave($id);

        if($tweet) {
            return $tweet;
        }
        else {
            // Création de la sauvegarde
            createSaveFromTweet($id, true);

            $tweet = getTweetFromSave($id);
            if($tweet) {
                return $tweet;
            }
            else {
                // La sauvegarde n'a pas fonctionné
                $GLOBALS['error_handler']->sendError(9);
            }
        }
    }
    else {
        $GLOBALS['error_handler']->sendError(16);
    }
}
