<?php

// List orphans available tweet saves

require_once INC_DIR . 'generated.php';

function loadEndpoint() {
    global $connexion;
    global $user_object;
    global $api_handler;

    $api_handler->addArg('count', 10);
    $api_handler->addArg('max_id');
    $api_handler->addArg('html', false);

    $count = is_numeric($api_handler->getArg('count')) ? (int)$api_handler->getArg('count') : 10;
    $max_id = is_numeric($api_handler->getArg('max_id')) ? (int)$api_handler->getArg('max_id') : null;

    if($count <= 0 || $count > 50) {
        $count = 10;
    }
    if($max_id <= 0) {
        $max_id = null;
    }

    $as_html = isActive($api_handler->getArg('html'));

    $future = [];

    // Liste les $count derniers tweets
    $res = mysqli_query($connexion, "SELECT idSave, idTweet FROM tweetSave WHERE notLinked=1 ".
                                     ($max_id ? "AND idSave < $max_id" : '')." ORDER BY idSave DESC LIMIT 0,$count;");

    if($res && mysqli_num_rows($res)) {
        $future['count'] = mysqli_num_rows($res);
        $future['tweets'] = [];
        $next = 0;

        while($row = mysqli_fetch_assoc($res)) { // On a trié en DESC, les id vont en descendant
            if($as_html) {
                ob_start();

                $modo = ($user_object ? ($user_object['moderator'] > 0 ? true : false) : false);
    
                generateHTMLTweet((int)$row['idTweet'], false, $modo);
    
                $html_tweet = ob_get_clean();
    
                $future['tweets'][] = ['html' => $html_tweet, 'id_str' => (string)$row['idTweet']];
            }
            else {
                $future['tweets'][] = getTweetFromSave($row['idTweet']);
            }
            $next = (int)$row['idSave'];
        }

        $future['next_max_id'] = $future['count'] === $count ? $next : 0;

        return $future;
    }
    else {
        return ['count' => 0, 'next_max_id' => 0, 'tweets' => []];
    }
}
