<?php

// Get a tweet (HTML format or JSON format)

require_once INC_DIR . 'generated.php';

function loadEndpoint() {
    global $api_handler;
    global $connexion;

    $api_handler->addArg('html');

    $html = isActive($api_handler->getArg('html'));

    $id = $GLOBALS['defined_arg'];
    $id = (int)explode('.json', $id)[0]; // 64 bit obligatoire

    // Cherche le tweet
    if($id) {
        $res = mysqli_query($connexion, "SELECT * FROM tweetSave WHERE idTweet=$id;");

        if($res && mysqli_num_rows($res)) {
            if($html) {
                ob_start();

                generateHTMLTweet($id);

                $html_tweet = ob_get_clean();

                return ['html' => $html_tweet, 'id_str' => (string)$id];
            }
            else {
                return getTweetFromSave($id);
            }
        }
        else {
            $GLOBALS['error_handler']->sendError(17);
        }
    }
    else {
        $GLOBALS['error_handler']->sendError(14);
    }
}
