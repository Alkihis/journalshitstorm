<?php

// Describe available endpoints in this directory

const AVAILABLE = ['picture' => ['name' => 'picture.php', 'usage' => LOGGED, 'rate_limit' => 15, 'method' => 'POST', 'rights' => READ_WRITE],
                   'banner' => ['name' => 'banner.php', 'usage' => LOGGED, 'rate_limit' => 15, 'method' => 'POST', 'rights' => READ_WRITE],
                   'night_mode.json' => ['name' => 'night_mode.php', 'usage' => VISITOR, 'rate_limit' => 50, 'method' => 'GET', 'rights' => READ_ONLY]];
