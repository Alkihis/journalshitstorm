<?php

// Change night_mode in session.

function loadEndpoint() {
    global $api_handler;

    $api_handler->addArg('mode', 0);

    $mode = (bool)(int)$api_handler->getArg('mode');

    $_SESSION['night_mode'] = $mode;

    return ['night_mode' => $mode];
}
