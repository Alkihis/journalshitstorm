<?php

// Add or delete banner picture from profile
// xxx.json est dans $GLOBALS['defined_arg']

require_once(ROOT . '/inc/postDelete.php');
require_once(ROOT .'/inc/traitements.php');

require_once(ROOT . '/inc/image/img_manipulator.php');
// Pour les fonctions de traitement des images

// Sauvegarde une bannière dans un profil à l'id idUsr (l'image étant déjà sur le serveur)
function saveImg($img_name, $idUsr){
    global $connexion;

    // Vérification de l'existance d'une image de profil
    $res = mysqli_query($connexion, "SELECT banner_img FROM Users WHERE id='$idUsr';");
    
    if($res && mysqli_num_rows($res) > 0){
        $row = mysqli_fetch_assoc($res);

        $res2 = mysqli_query($connexion, "UPDATE Users SET banner_img='$img_name' WHERE id='$idUsr';");

        if($res2){
            // Si il y avait déjà une bannière, on a la supprime
            if(preg_match(REGEX_IMG_BANNER, $row['banner_img'])){
                deleteFile($row['banner_img'], 'banner');
            }

            return $img_name;
        }
        else { 
            deleteFile($img_name, 'banner');
            $GLOBALS['error_handler']->sendError(9);
        }
    }
    else{
        deleteFile($img_name, 'banner');

        $GLOBALS['error_handler']->sendError(9);
    }
}

function loadEndpoint() {
    global $api_handler;
    global $connexion;
    global $user_object;

    // Recherche de l'action voulue (Supporté : update.json / from_twitter.json / delete.json)
    // Renvoie : Objet utilisateur

    $action = $GLOBALS['defined_arg'];

    if($action === 'update.json') {
        // On a besoin d'une image via FILES, ou via base64
        $api_handler->addArg('media_data'); // base 64 image

        // On cherche d'abord dans FILES
        if(isset($_FILES['media'])){
            $nom = traitementBannerImg('media', 'banner', true);
            if(!$nom){
                $GLOBALS['error_handler']->sendError(32);
            }
            else{
                saveImg($nom, $user_object['id']);
            }
        }
        else if (is_string($api_handler->getArg('media_data'))) {
            $base64_img = $api_handler->getArg('media_data');

            $nom = traitementImgBase64($base64_img, 'banner');
            if(!$nom){
                $GLOBALS['error_handler']->sendError(32);
            }
            else{
                saveImg($nom, $user_object['id']);
            }
        }
        else {
            $GLOBALS['error_handler']->sendError(14);
        }
    }
    else if ($action === 'from_twitter.json') {
        // on vérifie qu'il y a un compte Twitter configuré
        if($user_object['access_token_twitter'] && $user_object['twitter_id']) {
            $img = getTwitterBannerImg($user_object['access_token_twitter']);
            if($img) {
                $nom = traitementImgBase64($img, 'banner');
                if(!$nom){
                    $GLOBALS['error_handler']->sendError(32);
                }
                else{
                    saveImg($nom, $user_object['id']);
                }
            }
            else {
                $GLOBALS['error_handler']->sendError(33);
            }
        }
        else {
            $GLOBALS['error_handler']->sendError(33);
        }
    }
    else if ($action === 'delete.json') {
        // Vérification de l'existance d'une image de profil
        $res = mysqli_query($connexion, "SELECT banner_img FROM Users WHERE id='{$user_object['id']}';");
            
        if($res && mysqli_num_rows($res) > 0){
            $row = mysqli_fetch_assoc($res);

            $res2 = mysqli_query($connexion, "UPDATE Users SET banner_img=NULL WHERE id='{$user_object['id']}';");

            if($res2){
                // Si il y a une bannière, on la supprime
                if(preg_match(REGEX_IMG_BANNER, $row['banner_img'])){
                    deleteFile($row['banner_img'], 'banner');
                }
            }
            else { 
                $GLOBALS['error_handler']->sendError(9);
            }
        }
        else{
            $GLOBALS['error_handler']->sendError(9);
        }
    }
    else {
        $GLOBALS['error_handler']->sendError(14);
    }

    return getUserJSONCore($user_object['id'], false, false, true);
}
