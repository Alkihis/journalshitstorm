<?php

// Describe available endpoints in this directory [subscriptions]

const AVAILABLE = ['create.json' => ['name' => 'create.php', 'usage' => LOGGED, 'rate_limit' => 150, 'method' => 'POST', 'rights' => READ_WRITE],
                   'destroy.json' => ['name' => 'destroy.php', 'usage' => LOGGED, 'rate_limit' => 150, 'method' => 'POST', 'rights' => READ_WRITE],
                   'ids.json' => ['name' => 'followings.php', 'usage' => LOGGED, 'rate_limit' => 100, 'method' => 'GET', 'rights' => READ_ONLY],
                   'lookup.json' => ['name' => 'followings_lookup.php', 'usage' => LOGGED, 'rate_limit' => 25, 'method' => 'GET', 'rights' => READ_ONLY]];
