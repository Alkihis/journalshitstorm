<?php
// Get followings of a user (IDs (this file) or complete user object (see followings_lookup.php))
// User that this user follows

function loadEndpoint() {
    global $connexion;
    global $api_handler;
    global $user_object;

    $api_handler->addArg('cursor', 0);
    $api_handler->addArg('count', 100);

    $id = (int)$user_object['id'];
    $cursor = $api_handler->getArg('cursor');
    $count = $api_handler->getArg('count');

    if(is_numeric($id) && $id > 0) {
        if(is_numeric($cursor) && $cursor > 0) {
            $cursor = (int)$cursor;
        }
        else {
            $cursor = 0;
        }

        if(is_numeric($count) && $count > 0 && $count <= 1000) {
            $count = (int)$count;
        }
        else {
            $count = 100;
        }

        $res = mysqli_query($connexion, "SELECT idFollowed, idSFollow FROM ShitFollowings WHERE idFollower=$id AND idSFollow > $cursor ORDER BY idSFollow ASC LIMIT 0,$count;");

        if($res && mysqli_num_rows($res)) {
            $future = [];
            $next_id = 0;

            while($row = mysqli_fetch_assoc($res)) {
                $future['subscriptions'][] = (int)$row['idFollowed'];
                $next_id = (int)$row['idSFollow'];
            }

            // Si on a récupéré exactement le nombre de count défini, on suppose qu'il y en a d'autres
            if(count($future['subscriptions']) === $count) {
                $future['next_cursor'] = $next_id;
            }
            else {
                $future['next_cursor'] = 0;
            }

            return $future;
        }
        else {
            return ['subscriptions' => [], 'next_cursor' => 0];
        }
    }
    else {
        $GLOBALS['error_handler']->sendError(16);
    }
}
