<?php

require_once(ROOT . '/inc/traitements.php');
require_once(ROOT . '/inc/postDelete.php');

function loadEndpoint() {
    global $connexion;
    global $api_handler;
    global $user_object;

    $api_handler->addArg('id');
    $api_handler->addArg('with_mail');

    $id = $api_handler->getArg('id');

    if(is_numeric($id) && $id > 0) {
        $id = (int)$id;
        $mail = (int)isActive($api_handler->getArg('with_mail'));

        if(!shitstormExists($id)) { // Vérifie si la shitstorm existe
            $GLOBALS['error_handler']->sendError(17);
        }

        $res1 = mysqli_query($connexion, "SELECT idSFollow FROM ShitFollowings WHERE idFollower='{$user_object['id']}' AND idFollowed='$id';");

        if($res1 && mysqli_num_rows($res1) === 0) {
            $res = mysqli_query($connexion, "INSERT INTO ShitFollowings (idFollower, idFollowed, withMail) VALUES ('{$user_object['id']}', '$id', $mail);");

            if(!$res){
                $GLOBALS['error_handler']->sendError(9);
            }
        }
        else {
            $res = mysqli_query($connexion, "UPDATE ShitFollowings SET withMail=$mail WHERE idFollower='{$user_object['id']}' AND idFollowed='$id';");
        }
        
        return getJSONShitstorm(null, $id);
    }
    else {
        $GLOBALS['error_handler']->sendError(16);
    }
}
