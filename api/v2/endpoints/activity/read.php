<?php

// Read activity (notifications) from user

require(ROOT . '/inc/traitements.php');
require(ROOT . '/inc/postDelete.php');

function loadEndpoint() {
    global $connexion;
    global $api_handler;
    global $user_object;

    $api_handler->addArg('untouch_read_status', 'false', BOOL_URL_VALUES);
    $api_handler->addArg('since_id');
    $api_handler->addArg('max_id');
    $api_handler->addArg('show_old', 'true', BOOL_URL_VALUES);
    $api_handler->addArg('count', 10);
    $api_handler->addArg('order', 'DESC');

    $idUsr = $user_object['id'];

    if($idUsr) {
        $since = 0;
        $max = 0;
        $to_view = 1;
        $set_read = true;
        $asc = 'DESC';

        if(is_numeric($api_handler->getArg('since_id')) && $api_handler->getArg('since_id') > 0){
            $since = (int)$api_handler->getArg('since_id');
        }
        if(is_numeric($api_handler->getArg('max_id')) && $api_handler->getArg('max_id') > 0){
            $max = (int)$api_handler->getArg('max_id');
        }
        if(!isActive($api_handler->getArg('show_old'))){
            $to_view = 0;
        }
        if(isActive($api_handler->getArg('untouch_read_status'))){
            $set_read = false;
        }

        $count = $api_handler->getArg('count');
        if(!is_numeric($count) || $count <= 0 || $count > 50){
            $count = 10;
        }
        else {
            $count = (int)$count;
        }
        if($api_handler->getArg('order') === 'ASC'){
            $asc = 'ASC';
        }
    
        // Récupération
        $future = array();
    
        $res = mysqli_query($connexion, "SELECT n.* 
                                         FROM Notifications n
                                         WHERE idDest='$idUsr' ".
                                         (!$to_view ? 'AND viewed=0' : '')." 
                                         AND idNotif > $since ".
                                         ($max ? "AND idNotif <= $max" : '')." 
                                         ORDER BY idNotif $asc 
                                         LIMIT 0,$count;");
    
        if($res && mysqli_num_rows($res) > 0){
            $future['count'] = mysqli_num_rows($res);
            $future['notifications'] = [];
            $nt = &$future['notifications'];
            $i = 0;
            $tinyest_id = null;

            // Ordonne aux fonctions de JSON de ne pas crasher si élément introuvable (renvoie null à la place)
            $GLOBALS['dont_crash_on_error'] = true;

    
            while($row = mysqli_fetch_assoc($res)) {
                $nt[$i]['id_notification'] = (int)$row['idNotif'];
                $nt[$i]['id_notification_str'] = (string)$row['idNotif'];
                $nt[$i]['id_receiver'] = (int)$row['idDest'];
                $nt[$i]['user'] = (int)$row['idUsr'] > 0 ? getUserJSONCore((int)$row['idUsr'], true) : null;
                $nt[$i]['id_related'] = (int)$row['idRelated'];
                $nt[$i]['notification_type'] = $row['type'];
                $nt[$i]['already_seen'] = (bool)$row['viewed'];
                $nt[$i]['date'] = $row['date'];
                $nt[$i]['deleted'] = false;
    
                if(!$tinyest_id || $tinyest_id > $row['idNotif']){
                    // Si le plus petit ID n'est pas défini ou il est supérieur à l'ID actuel
                    $tinyest_id = $row['idNotif'];
                }
    
                if($row['type'] == 'MENTION_COMMENT' || $row['type'] == 'LIKE_COMMENT' || $row['type'] == 'POST_COMMENT'){ // Récupération des métadonnées du commentaire
                    $nt[$i]['comment'] = getJSONComment(null, $row['idRelated']);

                    if($nt[$i]['comment'] === null) {
                        $nt[$i]['deleted'] = true;
                    }
                    else { // Si le commentaire existe, on inclut la shitstorm avec pour y extraire des informations (sans l'utilisateur pour alléger la charge)
                        $nt[$i]['shitstorm'] = getJSONShitstorm(null, $nt[$i]['comment']['id_shitstorm'], false, true, false, false, false);
                    }
                }
                elseif($row['type'] == 'SHITSTORM' || $row['type'] == 'DISLIKE_SHITSTORM' || $row['type'] == 'LIKE_SHITSTORM' || $row['type'] == 'CONFIRM_SHITSTORM'){
                    $nt[$i]['shitstorm'] = getJSONShitstorm(null, $row['idRelated'], false, true, false, false, false);

                    if($nt[$i]['shitstorm'] === null) {
                        $nt[$i]['deleted'] = true;
                    }
                }
                elseif($row['type'] == 'CONFIRM_ANSWER' || $row['type'] == 'POST_ANSWER'){
                    $nt[$i]['answer'] = getAnswerJSON($row['idRelated']);

                    if($nt[$i]['answer'] === null) {
                        $nt[$i]['deleted'] = true;
                    }
                    else { // Si la réponse existe, on inclut la shitstorm avec pour y extraire des informations (sans l'utilisateur pour alléger la charge)
                        $nt[$i]['shitstorm'] = getJSONShitstorm(null, $nt[$i]['answer']['id_shitstorm'], false, true, false, false, false);
                    }
                }
                elseif($row['type'] == 'MODERATION_ANSWER'){
                    $comment = mysqli_query($connexion, "SELECT a.* FROM WaitAnsw a WHERE idSubA='{$row['idRelated']};'");
    
                    if($comment && mysqli_num_rows($comment) > 0){
                        $rowCom = mysqli_fetch_assoc($comment);
    
                        $resS = mysqli_query($connexion, "SELECT title, pseudo FROM ".($rowCom['fromWaitingShitstorm'] ? 'Waiting' : 'Shitstorms')." WHERE idSub={$rowCom['idSub']};");
    
                        if($resS && mysqli_num_rows($resS)){
                            $rowSub = mysqli_fetch_assoc($resS);
    
                            $nt[$i]['answer'] = [];
                            $co = &$nt[$i]['answer'];
        
                            $co['id_answer'] = (int)$rowCom['idSubA'];
                            $co['id_shitstorm'] = (int)$rowCom['idSub'];
                            $co['title'] = $rowSub['title'];
                            $co['pseudo'] = $rowSub['pseudo'];
                            $co['id_user'] = (int)$rowCom['idUsr'];
                            $co['date'] = $rowCom['dateAn'];
                        }
                        else{ // La réponse n'existe pas -> ici, on supprime direct la notif
                            mysqli_query($connexion, "DELETE FROM Notifications WHERE idNotif={$row['idNotif']};");
                            $i--;
                            $future['count']--;
                        }
                    }
                    else{ // La réponse n'existe pas -> ici, on supprime direct la notif
                        mysqli_query($connexion, "DELETE FROM Notifications WHERE idNotif={$row['idNotif']};");
                        $i--;
                        $future['count']--;
                    }
                }
                elseif($row['type'] == 'MODERATION_SHITSTORM'){
                    $comment = mysqli_query($connexion, "SELECT * FROM Waiting WHERE idSub='{$row['idRelated']};'");
    
                    if($comment && mysqli_num_rows($comment) > 0){
                        $rowCom = mysqli_fetch_assoc($comment);
    
                        $nt[$i]['shitstorm'] = [];
                        $co = &$nt[$i]['shitstorm'];
    
                        $co['id_shitstorm'] = $rowCom['idSub'];
                        $co['content'] = $rowCom['dscr'];
                        $co['title'] = $rowCom['title'];
                        $co['pseudo'] = $rowCom['pseudo'];
                        $co['id_user'] = $rowCom['idUsr'];
                        $co['date'] = $rowCom['dateShit'];
                    }
                    else{ // La shitstorm n'existe pas -> ici, on supprime direct la notif
                        mysqli_query($connexion, "DELETE FROM Notifications WHERE idNotif={$row['idNotif']};");
                        $i--;
                        $future['count']--;
                    }
                }
                $i++;
            }
    
            if($set_read){
                // Actualisation du statut "vu" (la notif est lu et envoyée, donc elle est considérée comme lue?)
                $res = mysqli_query($connexion, "UPDATE Notifications SET viewed=1 WHERE idDest='$idUsr' AND viewed=0 AND idNotif > $since ".($max ? "AND idNotif <= $max" : '')."  LIMIT $count;");
            }
            
            if($i === $count){
                $tinyest_id--;
                $future['next_cursor'] = (int)$tinyest_id;
                $future['next_cursor_str'] = (string)$tinyest_id;
            }
            else{
                $future['next_cursor'] = 0;
                $future['next_cursor_str'] = '0';
            }
            
        }
        else{
            $future['count'] = 0;
        }

        return $future;
    }
    else {
        $GLOBALS['error_handler']->sendError(9);
    }
}
