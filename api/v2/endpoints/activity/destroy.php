<?php

// Delete activity (notifications) from user

require(ROOT . '/inc/traitements.php');
require(ROOT . '/inc/postDelete.php');

function loadEndpoint() {
    global $connexion;
    global $api_handler;
    global $user_object;

    $api_handler->addArg('id');

    $idUsr = $user_object['id'];

    if($idUsr) {
        $id = $api_handler->getArg('id');

        if($id > 0) {      
            $res = mysqli_query($connexion, "DELETE FROM Notifications WHERE idDest='$idUsr' AND idNotif='$id';");
            if($res){
                return ['status' => 'deleted'];
            }
            else{
                $GLOBALS['error_handler']->sendError(9);
            }
        }
        else {
            $GLOBALS['error_handler']->sendError(16);
        }
    }
    else {
        $GLOBALS['error_handler']->sendError(9);
    }
}
