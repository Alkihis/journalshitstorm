<?php

// Return notification count of user

require(ROOT . '/inc/traitements.php');
require(ROOT . '/inc/postDelete.php');

function loadEndpoint() {
    global $user_object;

    if($user_object['id']) {
        return ['count' => hasNotifications($user_object['id'])];
    }
    else {
        $GLOBALS['error_handler']->sendError(9);
    }
}
