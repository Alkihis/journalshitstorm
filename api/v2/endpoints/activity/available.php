<?php

// Describe available endpoints in this directory [ACTIVITY]

const AVAILABLE = ['read.json' => ['name' => 'read.php', 'usage' => LOGGED, 'rate_limit' => 50, 'method' => 'GET', 'rights' => READ_ONLY],
                   'count.json' => ['name' => 'count.php', 'usage' => LOGGED, 'rate_limit' => 500, 'method' => 'GET', 'rights' => READ_ONLY],
                   'update.json' => ['name' => 'update.php', 'usage' => LOGGED, 'rate_limit' => 150, 'method' => 'POST', 'rights' => READ_WRITE],
                   'destroy.json' => ['name' => 'destroy.php', 'usage' => LOGGED, 'rate_limit' => 150, 'method' => 'POST', 'rights' => READ_WRITE]];
