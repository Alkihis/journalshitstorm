<?php

// Read comment from comment ID (mode=id) or shitstorm ID (mode=list)

function loadEndpoint() {
    global $api_handler;
    global $connexion;

    $api_handler->addArg('mode');
    $api_handler->addArg('id');
    $api_handler->addArg('since_id');
    $api_handler->addArg('max_id');
    $api_handler->addArg('skip');
    $api_handler->addArg('order');
    $api_handler->addArg('count');
    $api_handler->addArg('replies_to');
    $api_handler->addArg('include_replies', 'true');
    $api_handler->addArg('fetch_by');

    $mode = $api_handler->getArg('mode');
    $id = (int)$api_handler->getArg('id');

    $since = (int)$api_handler->getArg('since_id');
    
    if($since <= 0) {
        $since = 0;
    }

    $max = (int)$api_handler->getArg('max_id');
    
    if($max <= 0) {
        $max = 0;
    }

    $skip = (int)$api_handler->getArg('skip');
    
    if($skip <= 0) {
        $skip = 0;
    }

    $count = (int)$api_handler->getArg('count');

    if($count <= 0 || $count > 50) {
        $count = 10;
    }

    $reply = (int)$api_handler->getArg('replies_to');

    if($reply <= 0) {
        $reply = 0;
    }

    $order = 'ASC';
    if($api_handler->getArg('order') === 'DESC') {
        $order = 'DESC';
    }

    $fetch_by = 'date';
    if($api_handler->getArg('fetch_by') === 'rate') {
        $fetch_by = 'rate';
    }

    $include_replies = isActive($api_handler->getArg('include_replies'));

    if($mode === 'id') {
        $id = (int)$id;
        if($id > 0) {
            return getJSONComment(null, $id, true, $include_replies);
        }
        else {
            $GLOBALS['error_handler']->sendError(16);
        }
    }
    else if($mode === 'list') {   
        $res = null;

        if($fetch_by === 'date') {
            $res = mysqli_query($connexion, "SELECT co.*, (SELECT COUNT(*) FROM ComLikes WHERE ComLikes.idCom=co.idCom) c, u.usrname FROM Comment co JOIN Users u ON co.idUsr=u.id 
                                         WHERE ". ($max ? "idCom<$max" : "idCom>$since") ." 
                                         ". ($reply ? "AND inReplyToIdMain=$reply " : "AND idSub=$id AND inReplyToIdMain IS NULL ") ." 
                                         ORDER BY idCom $order LIMIT 0,$count;");
        }
        else if($fetch_by === 'rate') {
            $res = mysqli_query($connexion, "SELECT co.*, (SELECT COUNT(*) FROM ComLikes WHERE ComLikes.idCom=co.idCom) c, u.usrname FROM Comment co JOIN Users u ON co.idUsr=u.id 
                                         WHERE ". ($max ? "idCom<$max" : "idCom>$since") ." 
                                         ". ($reply ? "AND inReplyToIdMain=$reply " : "AND idSub=$id AND inReplyToIdMain IS NULL ") ." 
                                         ORDER BY c $order, idCom ASC LIMIT $skip,$count;");
        }
        

        $future = [];
        $future['comments'] = [];

        $next_id = 0;

        if($res && mysqli_num_rows($res)) {
            while($row = mysqli_fetch_assoc($res)) {
                $future['comments'][] = getJSONComment($row, null, true, $include_replies, $count);
                $next_id = (int)$row['idCom'];
            }
        }

        if(count($future['comments']) === $count) {
            if($fetch_by === 'date') {
                $future['next_cursor'] = $next_id;
            }
            else {
                $future['next_cursor'] = $count + $skip;
            }
        }
        else {
            $future['next_cursor'] = 0;
        }

        return $future;
    }
    else {
        $GLOBALS['error_handler']->sendError(14);
    }
}
