<?php

// Destory a comment

require INC_DIR . 'traitements.php';
require INC_DIR . 'postDelete.php';

function loadEndpoint() {
    global $user_object;
    global $api_handler;
    global $connexion;

    $api_handler->addArg('id');

    $id = $api_handler->getArg('id');

    if($id > 0) {
        if(!commentExists($id)) {
            $GLOBALS['error_handler']->sendError(17);
        }
        // Verify ownership if user is not moderator
        if((int)$user_object['moderator'] <= 0) {
            $res = mysqli_query($connexion, "SELECT idUsr FROM Comment WHERE idCom=$id;");

            if($res && mysqli_num_rows($res)) {
                $row = mysqli_fetch_assoc($res);

                if((int)$row['idUsr'] !== (int)$user_object['id']) {
                    $GLOBALS['error_handler']->sendError(41);
                }
            }
        }

        $d = deleteCom($id);

        return ['deleted' => $d];
    }
    else {
        $GLOBALS['error_handler']->sendError(14);
    }
}
