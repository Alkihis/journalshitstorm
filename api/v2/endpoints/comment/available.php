<?php

// Describe available endpoints in this directory [COMMENT]

const AVAILABLE = ['read.json' => ['name' => 'read.php', 'usage' => VISITOR, 'rate_limit' => 300, 'method' => 'GET', 'rights' => READ_ONLY],
                   'destroy.json' => ['name' => 'destroy.php', 'usage' => LOGGED, 'rate_limit' => 150, 'method' => 'POST', 'rights' => READ_WRITE],
                   'create.json' => ['name' => 'create.php', 'usage' => LOGGED, 'rate_limit' => 150, 'method' => 'POST', 'rights' => READ_WRITE]];
