<?php

// Post a new comment

require INC_DIR . 'traitements.php';
require INC_DIR . 'postDelete.php';

function loadEndpoint() {
    global $api_handler;
    global $user_object;

    $api_handler->addArg('id');
    $api_handler->addArg('content');
    $api_handler->addArg('in_reply_to_id');

    $id = (int)$api_handler->getArg('id');

    $in_reply_to_id = (int)$api_handler->getArg('in_reply_to_id');

    $content = $api_handler->getArg('content');

    if($id && $id > 0 && $content) {
        $res = postComment($id, $content, $user_object['id'], $in_reply_to_id);

        if($res > 0) {
            return getJSONComment(null, $res, false, false);
        }
        else {
            switch($res) {
                case -1:
                    $GLOBALS['error_handler']->sendError(10);
                    break;
                case -2:
                    $GLOBALS['error_handler']->sendError(39);
                    break;
                case -3:
                    $GLOBALS['error_handler']->sendError(40);
                    break;
                case -4:
                    $GLOBALS['error_handler']->sendError(9);
                    break;
                default:
                    $GLOBALS['error_handler']->sendError(38);
            }
        }
    }
    else {
        $GLOBALS['error_handler']->sendError(14);
    }
}
