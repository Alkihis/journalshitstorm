<?php

require(PAGES_DIR . 'account/account_pages.php');

function loadEndpoint() {
    $GLOBALS['additionnals'] = null;
    $GLOBALS['url_parms'] = null;
    
    global $api_handler;
    $api_handler->addArg('page');

    $page = (int)$api_handler->getArg('page');

    if($page >= 0) {
        global $user_object;

        ob_start();
    
        switch($page) {
            case 0:
                loadAccountProprieties($user_object);
                break;
            case 1:
                loadAccountPassword($user_object);
                break;
            case 2:
                loadAccountVisual($user_object);
                break;
            case 3:
                loadAccountModificationProfile($user_object, '');
                break;
            case 4:
                require_once(INC_DIR . 'generated.php');
                loadMySubs($user_object);
                break;
            case 5:
                loadAccountOthers($user_object);
                break;
            case 6:
                loadTwitterAccount($user_object);
                break;
            case 7:
                loadAccountLinkedApps($user_object);
                break;
            default:
                ob_end_clean();
                $GLOBALS['error_handler']->sendError(37);
        }

        return ['html' => ob_get_clean()];
    }
    else {
        $GLOBALS['error_handler']->sendError(14);
    }
}
