<?php

// Load unique shitstorm for display in side-shitstorm container

function loadEndpoint() {
    global $api_handler;

    $api_handler->addArg('id');

    $GLOBALS['additionnals'] = null;
    $GLOBALS['url_parms'] = null;
    
    if($api_handler->getArg('id')) {
        $GLOBALS['black'] = $_SESSION['night_mode'] ?? false;
        global $connexion;

        $idSub = (int)$api_handler->getArg('id');
    
        require_once(INC_DIR . 'traitements.php');
        require_once(INC_DIR . 'generated.php');
        require_once(PAGES_DIR . 'shitstorm/unique.php');
    
        ob_start();
        generateUniqueShitstorm($idSub);
        $html = ob_get_clean();
    
        return ['html' => $html, 'id' => $idSub];
    }
    else {
        $GLOBALS['error_handler']->sendError(14);
    }
}
