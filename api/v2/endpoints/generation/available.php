<?php

// Describe available endpoints in this directory [GENERATION]

const AVAILABLE = ['account_pages.json' => ['name' => 'account_page.php', 'usage' => LOGGED, 'rate_limit' => 150, 'method' => 'GET', 'rights' => READ_ONLY],
                   'unique_shitstorm.json' => ['name' => 'unique_shitstorm.php', 'usage' => VISITOR, 'rate_limit' => 300, 'method' => 'GET', 'rights' => READ_ONLY]];
