<?php

// Destroy waiting shitstorms or answers

require INC_DIR . 'traitements.php';
require INC_DIR . 'postDelete.php';

function loadEndpoint() {
    global $api_handler;
    global $connexion;

    $api_handler->addArg('id');

    $id = $api_handler->getArg('id');

    if($GLOBALS['defined_arg'] === 'shitstorm.json') {
        $id = (int)$id;
        if($id > 0) {
            return ['deleted' => (bool)deleteWaitingShitstorm($id)];
        }
        else {
            $GLOBALS['error_handler']->sendError(17);
        }
    }
    else if ($GLOBALS['defined_arg'] === 'answer.json') {
        $id = (int)$id;
        if($id > 0) {
            return ['deleted' => (bool)deleteWaitingAnswer($id)];
        }
        else {
            $GLOBALS['error_handler']->sendError(17);
        }
    }
    else {
        $GLOBALS['error_handler']->sendError(14);
    }
}
