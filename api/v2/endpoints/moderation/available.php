<?php

// Describe available endpoints in this directory [MODERATION]

const AVAILABLE = ['read' => ['name' => 'read.php', 'usage' => MODO, 'rate_limit' => 100, 'method' => 'GET', 'rights' => READ_WRITE],
                   'destroy' => ['name' => 'destroy.php', 'usage' => MODO, 'rate_limit' => 50, 'method' => 'POST', 'rights' => READ_WRITE],
                   'update' => ['name' => 'update.php', 'usage' => MODO, 'rate_limit' => 50, 'method' => 'POST', 'rights' => READ_WRITE]];
