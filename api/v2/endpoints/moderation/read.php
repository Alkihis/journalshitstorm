<?php

// Read shitstorm or answer waiting for moderation

function loadEndpoint() {
    global $api_handler;
    global $connexion;

    $api_handler->addArg('mode');
    $api_handler->addArg('id');
    $api_handler->addArg('since_id');
    $api_handler->addArg('count');

    $mode = $api_handler->getArg('mode');
    $id = $api_handler->getArg('id');

    $since = (int)$api_handler->getArg('since_id');
    
    if($since <= 0) {
        $since = 0;
    }

    $count = (int)$api_handler->getArg('count');

    if($count <= 0 || $count > 50) {
        $count = 10;
    }

    if($GLOBALS['defined_arg'] === 'shitstorm.json') {
        if($mode === 'id') {
            $id = (int)$id;
            if($id > 0) {
                return getJSONWaitingShitstorm($id);
            }
            else {
                $GLOBALS['error_handler']->sendError(16);
            }
        }
        else if($mode === 'list') {   
            $res = mysqli_query($connexion, "SELECT idSub FROM Waiting WHERE idSub>$since ORDER BY idSub ASC LIMIT 0,$count;");
    
            $future = [];
            $future['shitstorms'] = [];
    
            $next_id = 0;
    
            if($res && mysqli_num_rows($res)) {
                while($row = mysqli_fetch_assoc($res)) {
                    $future['shitstorms'][] = getJSONWaitingShitstorm((int)$row['idSub']);
                    $next_id = (int)$row['idSub'];
                }
            }
    
            if(count($future['shitstorms']) === $count) {
                $future['next_cursor'] = $next_id;
            }
            else {
                $future['next_cursor'] = 0;
            }
    
            return $future;
        }
        else {
            $GLOBALS['error_handler']->sendError(14);
        }
    }
    else if ($GLOBALS['defined_arg'] === 'answer.json') {
        if($mode === 'id') {
            $id = (int)$id;
            if($id > 0) {
                $waiting_a = getWaitingAnswerJSON($id);

                if($waiting_a['linked_to_waiting_shitstorm']) { // Si elle est liée à une shitstorm en attente, on la récupère avec
                    $waiting_a['shitstorm'] = getJSONWaitingShitstorm($waiting_a['id_shitstorm']);
                }
                else { // Si elle est liée à une shitstorm 'normale' (pas en attente), on récupère aussi
                    $waiting_a['shitstorm'] = getJSONShitstorm(null, $waiting_a['id_shitstorm'], true, false);
                }

                return $waiting_a;
            }
            else {
                $GLOBALS['error_handler']->sendError(16);
            }
        }
        else if($mode === 'list') {
            $res = mysqli_query($connexion, "SELECT idSubA FROM WaitAnsw WHERE idSubA>$since ORDER BY idSubA ASC LIMIT 0,$count;");
    
            $future = [];
            $future['answers'] = [];
    
            $next_id = 0;
    
            if($res && mysqli_num_rows($res)) {
                while($row = mysqli_fetch_assoc($res)) {
                    $waiting_a = getWaitingAnswerJSON((int)$row['idSubA']);
                    $next_id = (int)$row['idSubA'];

                    if($waiting_a['linked_to_waiting_shitstorm']) { // Si elle est liée à une shitstorm en attente, on la récupère avec
                        $waiting_a['shitstorm'] = getJSONWaitingShitstorm($waiting_a['id_shitstorm']);
                    }
                    else { // Si elle est liée à une shitstorm 'normale' (pas en attente), on récupère aussi
                        $waiting_a['shitstorm'] = getJSONShitstorm(null, $waiting_a['id_shitstorm'], true, false);
                    }

                    $future['answers'][] = $waiting_a;
                }
            }
    
            if(count($future['answers']) === $count) {
                $future['next_cursor'] = $next_id;
            }
            else {
                $future['next_cursor'] = 0;
            }
    
            return $future;
        }
        else {
            $GLOBALS['error_handler']->sendError(14);
        }
    }
    else {
        $GLOBALS['error_handler']->sendError(14);
    }
}
