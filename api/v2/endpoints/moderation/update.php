<?php

// Modify shitstorm or answer waiting for moderation

function loadEndpoint() {
    global $connexion;
    global $api_handler;

    $api_handler->addArg('lock');
    $api_handler->addArg('id');

    $lock = $api_handler->getArg('lock');
    $id = $api_handler->getArg('id');

    if($GLOBALS['defined_arg'] === 'shitstorm.json') {
        $id = (int)$id;
        if($id <= 0) {
            $GLOBALS['error_handler']->sendError(16);
        }

        // Vérif que la shitstorm existe
        if(!waitingShitstormExists($id)) {
            $GLOBALS['error_handler']->sendError(17);
        }
        
        $set = '';

        if($lock !== null) {
            $lock = (int)isActive($lock);
            $set = "locked=$lock ";
        }
        
        // Si les infos de set ont changé, on actualise
        if($set) {
            $res = mysqli_query($connexion, "UPDATE Waiting SET $set WHERE idSub=$id;");
            if(!$res) {
                $GLOBALS['error_handler']->sendError(9);
            }
        }

        return getJSONWaitingShitstorm($id);
    }
    else if ($GLOBALS['defined_arg'] === 'answer.json') {
        $id = (int)$id;
        if($id <= 0) {
            $GLOBALS['error_handler']->sendError(16);
        }

        // Vérifie que la réponse existe
        if(!waitingAnswerExists($id)) {
            $GLOBALS['error_handler']->sendError(17);
        }
        
        $set = '';

        if($lock !== null) {
            $lock = (int)isActive($lock);
            $set = "locked=$lock ";
        }
        
        // Si les infos de set ont changé, on actualise
        if($set) {
            $res = mysqli_query($connexion, "UPDATE WaitAnsw SET $set WHERE idSubA=$id;");
            if(!$res) {
                $GLOBALS['error_handler']->sendError(9);
            }
        }

        $waiting_a = getWaitingAnswerJSON($id);

        if($waiting_a['linked_to_waiting_shitstorm']) { // Si elle est liée à une shitstorm en attente, on la récupère avec
            $waiting_a['shitstorm'] = getJSONWaitingShitstorm($waiting_a['id_shitstorm']);
        }
        else { // Si elle est liée à une shitstorm 'normale' (pas en attente), on récupère aussi
            $waiting_a['shitstorm'] = getJSONShitstorm(null, $waiting_a['id_shitstorm'], true, false);
        }

        return $waiting_a;
    }
    else {
        $GLOBALS['error_handler']->sendError(14);
    }
}
