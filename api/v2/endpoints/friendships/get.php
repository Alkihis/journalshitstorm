<?php

function loadEndpoint() {
    global $connexion;
    global $api_handler;
    global $user_object;

    $api_handler->addArg('target');
    $api_handler->addArg('source');

    $id = $api_handler->getArg('target');
    $id2 = $api_handler->getArg('source');

    if(is_numeric($id) && is_numeric($id2) && $id > 0 && $id2 > 0 && $id != $id2) {
        $id = (int)$id;
        $id2 = (int)$id2;

        $res1 = mysqli_query($connexion, "SELECT idFollow, acceptMail FROM Followings WHERE idUsr='$id' AND idFollowed='$id2';");
        $res2 = mysqli_query($connexion, "SELECT idFollow, acceptMail FROM Followings WHERE idFollowed='$id' AND idUsr='$id2';");

        $future = [];

        $future[$id] = ['following' => $res1 && mysqli_num_rows($res1)];
        $future[$id2] = ['following' => $res2 && mysqli_num_rows($res2)];

        return $future;
    }
    else {
        $GLOBALS['error_handler']->sendError(16);
    }
}
