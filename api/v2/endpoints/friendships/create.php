<?php

require_once(ROOT . '/inc/traitements.php');
require_once(ROOT . '/inc/postDelete.php');

function loadEndpoint() {
    global $connexion;
    global $api_handler;
    global $user_object;

    $api_handler->addArg('id');
    $api_handler->addArg('with_mail');

    $id = $api_handler->getArg('id');

    if(is_numeric($id) && $id > 0) {
        $id = (int)$id;
        $mail = (int)isActive($api_handler->getArg('with_mail'));

        if($id == (int)$user_object['id']) {
            $GLOBALS['error_handler']->sendError(35);
        }
        else if(!isFollowable($id)) { // Empêche de suivre les utilisateurs qui ne le veulent pas et vérifie si l'utilisateur existe
            $GLOBALS['error_handler']->sendError(36);
        }

        $res1 = mysqli_query($connexion, "SELECT idFollow FROM Followings WHERE idUsr='{$user_object['id']}' AND idFollowed='$id';");

        if($res1 && mysqli_num_rows($res1) == 0){
            $res = mysqli_query($connexion, "INSERT INTO Followings (idUsr, idFollowed, acceptMail) VALUES ('{$user_object['id']}', '$id', $mail);");

            sendNotif('follow', $user_object['id'], $id, 0);

            if(!$res){
                $GLOBALS['error_handler']->sendError(9);
            }
        }
        else{
            $res = mysqli_query($connexion, "UPDATE Followings SET acceptMail=$mail WHERE idUsr='{$user_object['id']}' AND idFollowed='$id';");
        }
        
        return getUserJSONCore($id, true);
    }
    else {
        $GLOBALS['error_handler']->sendError(16);
    }
}
