<?php

function loadEndpoint() {
    global $connexion;
    global $api_handler;
    global $user_object;

    $api_handler->addArg('id');

    $id = $api_handler->getArg('id');

    if(is_numeric($id) && $id > 0) {
        $id = (int)$id;

        $res1 = mysqli_query($connexion, "SELECT idFollow FROM Followings WHERE idUsr='{$user_object['id']}' AND idFollowed='$id';");

        if($res1 && mysqli_num_rows($res1) === 0){
            // Il n'y a déjà aucune relation, ne rien faire
        }
        else{
            $res = mysqli_query($connexion, "DELETE FROM Followings WHERE idUsr='{$user_object['id']}' AND idFollowed='$id';");
        }
        
        return getUserJSONCore($id, true);
    }
}
