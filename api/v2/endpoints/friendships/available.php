<?php

// Describe available endpoints in this directory [FRIENDSHIPS]

const AVAILABLE = ['create.json' => ['name' => 'create.php', 'usage' => LOGGED, 'rate_limit' => 150, 'method' => 'POST', 'rights' => READ_WRITE],
                   'destroy.json' => ['name' => 'destroy.php', 'usage' => LOGGED, 'rate_limit' => 150, 'method' => 'POST', 'rights' => READ_WRITE],
                   'get.json' => ['name' => 'get.php', 'usage' => VISITOR, 'rate_limit' => 300, 'method' => 'GET', 'rights' => READ_ONLY],
                   'followers' => ['name' => 'followers.php', 'usage' => VISITOR, 'rate_limit' => 125, 'method' => 'GET', 'rights' => READ_ONLY],
                   'followings' => ['name' => 'followings.php', 'usage' => VISITOR, 'rate_limit' => 125, 'method' => 'GET', 'rights' => READ_ONLY]];
