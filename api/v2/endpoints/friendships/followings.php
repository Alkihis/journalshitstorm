<?php
// Get followings of a user (IDs (this file) or complete user object (see followings_lookup.php))
// User that this user follows

function loadEndpoint() {
    global $connexion;
    global $api_handler;
    global $user_object;

    if($GLOBALS['defined_arg'] === 'ids.json') {
        $api_handler->addArg('id');
        $api_handler->addArg('cursor', 0);
        $api_handler->addArg('count', 100);
    
        $id = $api_handler->getArg('id');
        $cursor = $api_handler->getArg('cursor');
        $count = $api_handler->getArg('count');
    
        if(!$id && $user_object) {
            $id = (int)$user_object['id'];
        }

        if(!is_numeric($id)) {
            $id = getIDFromUsername($id);
        }
    
        if(is_numeric($id) && $id > 0) {
            $id = (int)$id;
    
            if(is_numeric($cursor) && $cursor > 0) {
                $cursor = (int)$cursor;
            }
            else {
                $cursor = 0;
            }
    
            if(is_numeric($count) && $count > 0 && $count <= 1000) {
                $count = (int)$count;
            }
            else {
                $count = 100;
            }
    
            // Vérif que l'user existe avant de chercher à obtenir ses followings
            if(userExists($id)) {
                $res = mysqli_query($connexion, "SELECT idFollowed, idFollow FROM Followings WHERE idUsr=$id AND idFollow > $cursor ORDER BY idFollow ASC LIMIT 0,$count;");
    
                if($res && mysqli_num_rows($res)) {
                    $future = [];
                    $next_id = 0;
    
                    while($row = mysqli_fetch_assoc($res)) {
                        $future['followings'][] = (int)$row['idFollowed'];
                        $next_id = (int)$row['idFollow'];
                    }
    
                    // Si on a récupéré exactement le nombre de count défini, on suppose qu'il y en a d'autres
                    if(count($future['followings']) === $count) {
                        $future['next_cursor'] = $next_id;
                    }
                    else {
                        $future['next_cursor'] = 0;
                    }

                    return $future;
                }
                else {
                    return ['followings' => [], 'next_cursor' => 0];
                }
            }
            else {
                $GLOBALS['error_handler']->sendError(6);
            }
        }
        else {
            $GLOBALS['error_handler']->sendError(16);
        }
    }
    else if ($GLOBALS['defined_arg'] === 'lookup.json') {
        require API_ENDPOINTS_DIR . 'friendships/followings_lookup.php';

        return loadSubEndpoint();
    }
    else {
        $GLOBALS['error_handler']->sendError(14);
    }
}
