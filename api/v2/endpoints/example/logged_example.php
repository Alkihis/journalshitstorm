<?php

function loadEndpoint() {
    global $user_object;

    if($user_object) {
        unset($user_object['token']);
        unset($user_object['passw']);
        unset($user_object['access_token_twitter']);
        unset($user_object['authtoken']);
        return $user_object;
    }
    return ['status' => 'fail'];
}
