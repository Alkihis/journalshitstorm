<?php

// Chaque Endpoint est composé que d'une seule fonction : loadEndpoint qui est appelée par l'handler
// Elle retourne un tableau de JSON

function loadEndpoint() {
    global $api_handler;
    global $connexion;

    $api_handler->addArg('say_hello');

    $msg = $api_handler->getArg('say_hello');

    return ['state' => 'success', 'message' => $msg];
}
