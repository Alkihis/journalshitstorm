<?php

// Describe available dirs in the directory

/**
 * Folders MUST have the following parameters :
 * {key} : name entered by the API user, after endpoint actual directory. For example, for endpoint shitstorm.fr/api/v2/rate/sub/shitstorm.json, key is "sub" (if this file is in endpoint root dir)
 * 'name' : name of directory affiliated to endpoint. Dir MUST have a file named available.php in it.
 * 'usage' : One of the right constant defined in reserved/endpoints.php [ALL, VISITOR, LOGGED, MODO, ADMIN]
 */

$GLOBALS['available_folders'] = ['new_example' => ['path' => 'example_folder', 'usage' => ALL]];
