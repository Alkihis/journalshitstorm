<?php

// Describe available endpoints in this directory

// defined and true of the endpoint should show list of accepted endpoints values if no one is given
const READABLE_ENDPOINT = true;

/**
 * Endpoint MUST have the following parameters :
 * {key} : name entered by the API user, after endpoint directory. For example, for endpoint shitstorm.fr/api/v2/rate/shitstorm.json, key is "shitstorm.json"
 * 'name' : name of PHP file affiliated to endpoint. File MUST have a function named loadEndpoint() in it.
 * 'usage' : One of the right constant defined in reserved/endpoints.php [ALL, VISITOR, LOGGED, MODO, ADMIN]
 * 'rate_limit' : integer of number of request allowed in a certain amount of time (see 'expiration_time_count'). If any, set the null value.
 * 'method' : 'GET' or 'POST'. Arguments given by used well be read from $_GET or $_POST, according to this key
 * 'rights' : One of the access constants defined in reserved/endpoints.php [READ_ONLY, READ_WRITE, READ_WRITE_PASSWORD]
 * 
 * OPTIONAL :
 * 'expiration_time_count' : MINUTES before expiration of previous 'rate_limit' count. For exemple, if 'rate_limit' is 30 and this key is 20,
 *      API user can use the endpoint 30 times each 20 minutes maximum. By default, this key (even if unset) is 15.
 */

const AVAILABLE = ['example.json' => ['name' => 'example.php', 'usage' => ALL, 'rate_limit' => 700, 'method' => 'GET', 'rights' => READ_ONLY],
                   'credentials.json' => ['name' => 'logged_example.php', 'usage' => LOGGED, 'rate_limit' => 75, 'method' => 'GET', 'rights' => READ_ONLY]];

