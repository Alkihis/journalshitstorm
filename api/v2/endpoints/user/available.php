<?php

// Describe available endpoints in this directory [USER]

const AVAILABLE = ['get' => ['name' => 'get.php', 'usage' => VISITOR, 'rate_limit' => 300, 'method' => 'GET', 'rights' => READ_ONLY],
                   'search.json' => ['name' => 'search.php', 'usage' => VISITOR, 'rate_limit' => 100, 'method' => 'GET', 'rights' => READ_ONLY]];

