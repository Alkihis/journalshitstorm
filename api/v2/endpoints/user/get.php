<?php

// Get a specific user by id or username

function loadEndpoint() {
    global $connexion;
    global $api_handler;
    global $user_object;

    if($GLOBALS['defined_arg']) {
        $arg = explode('.json', $GLOBALS['defined_arg'])[0];

        if(!is_numeric($arg)) {
            $user = mysqli_real_escape_string($connexion, $arg);

            $res = mysqli_query($connexion, "SELECT id FROM Users WHERE usrname='$user';");

            if($res && mysqli_num_rows($res)) {
                $row = mysqli_fetch_assoc($res);
                $arg = $row['id'];
            }
            else {
                $GLOBALS['error_handler']->sendError(6);
            }
        }
        
        $arg = (int)$arg;

        return getUserJSONCore($arg, true);
    }
    else {
        $GLOBALS['error_handler']->sendError(14);
    }
}
