<?php

// Search users by screen_name

function loadEndpoint() {
    global $connexion;
    global $api_handler;

    $count = $api_handler->addGetArg('count', 10);
    $query = $api_handler->addGetArg('query', '');

    if($count <= 0 || $count > 50) {
        $count = 10;
    }

    if(strlen($query) > 2) {
        $query = mysqli_real_escape_string($connexion, $query);

        $queries = explode(',', $query);

        if(count($queries) === 1) {
            $res = mysqli_query($connexion, "SELECT id FROM Users
                                            WHERE (usrname LIKE '%$query%' 
                                            OR realname LIKE '%$query%'
                                            OR SOUNDEX(usrname) = SOUNDEX('$query')) 
                                            AND findable=1 
                                            LIMIT 0,$count;");

            $future = [];

            if($res && mysqli_num_rows($res)) {
                while($row = mysqli_fetch_assoc($res)) {
                    $future[] = getUserJSONCore((int)$row['id'], true);
                }
            }

            return $future;
        }
        else {
            $future = [];
            $found = 0;

            foreach($queries as $number => $q) {
                if($number > 10) { // Pas plus de 10 utilisateurs
                    break;
                }
                if(strlen($q) <= 2) { // Pas de recherche si nom trop court
                    continue;
                }

                $res = mysqli_query($connexion, "SELECT id FROM Users
                                                WHERE (usrname LIKE '%$q%' 
                                                OR realname LIKE '%$q%'
                                                OR SOUNDEX(usrname) = SOUNDEX('$q')) 
                                                AND findable=1 
                                                LIMIT 0,$count;");

                if($res && mysqli_num_rows($res)) {
                    $found += mysqli_num_rows($res);

                    while($row = mysqli_fetch_assoc($res)) {
                        if($found > $count) {
                            break 2;
                        }

                        $future[] = getUserJSONCore((int)$row['id'], true);
                    }
                }
            }

            return $future;
        }

        
    }
    else {
        $GLOBALS['error_handler']->sendError(14);
    }
}
