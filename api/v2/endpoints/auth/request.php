<?php

// Request token
// -------------
// Give token linked to app who can request an future access token after approbation of user
// Using https://github.com/lcobucci/jwt/blob/3.2/README.md JWToken generator
// -------------

require_once API_JWT_VENDOR . 'autoload.php';
require_once API_DIR . 'v2/endpoints/auth/JWTfunctions.php';

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

function loadEndpoint() {
    global $connexion;
    global $api_handler;

    $signer = new Sha256();

    $api_handler->addArg('app_token');
    $api_handler->addArg('callback');

    if($api_handler->getArg('app_token') && $api_handler->getArg('callback')) {
        $apptoken = $api_handler->getArg('app_token');
    
        // Vérification si le token de l'app existe
        $apptoken = mysqli_real_escape_string($connexion, $apptoken);
        $res = mysqli_query($connexion, "SELECT * FROM TokensApps WHERE token='$apptoken';");

        if($res && mysqli_num_rows($res)) {
            $row = mysqli_fetch_assoc($res);

            $token_id = bin2hex(random_bytes(8));
            $rights = $row['allowWrite'];
            $expiration = time() + 3600;

            $final_token = buildJWToken($expiration, $token_id, $row['idApp'], 'request', $rights);

            // Vérifications d'usage
            // Parsing :
            /* 
                use Lcobucci\JWT\Parser;

                $token = (new Parser())->parse((string) $token); // Parses from a string
                $token->getHeaders(); // Retrieves the token header
                $token->getClaims(); // Retrieves the token claims

                echo $token->getClaim('uid'); // will print "1"
            */
            // Signature:
            // $token->verify($signer, UNIQUE_SIGNATURE);
            /*
                use Lcobucci\JWT\ValidationData;

                $data = new ValidationData(); // It will use the current time to validate (iat, nbf and exp)
                $data->setIssuer(HTTPS_URL);
                $data->setAudience(HTTPS_URL);
                $data->setId($row['tokenID']);

                var_dump($token->validate($data));
            */

            // Enregistrement du token dans la base de données
            $escaped_token = mysqli_real_escape_string($connexion, $final_token);
            $res = mysqli_query($connexion, "INSERT INTO Tokens (idUsr, token, tokenType, tokenID, expires, idApp) 
                                             VALUES (0, '$escaped_token', 'REQUEST', '$token_id', $expiration, '{$row['idApp']}');");

            return ['request_token' => $final_token, 'callback_url' => HTTPS_URL . "app_auth_callback?auth_token=$final_token&callback_url=" . rawurlencode($api_handler->getArg('callback'))
                    , 'expires' => $expiration];
        }
        else {
            $GLOBALS['error_handler']->sendError(11);
        }
    }
    else {
        $GLOBALS['error_handler']->sendError(11);
    }

    return ['status' => 'fail'];
}
