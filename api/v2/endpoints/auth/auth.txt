----------------------------------------
+ Authentification / Connexion à l'API +
----------------------------------------

---------------------------------------------------
Chemin d'authentification (Authentification Flow) :
---------------------------------------------------

- L'utilisateur crée une application liée à son compte du Journal des Shitstorms et obtient un token d'application

- Appel de l'endpoint POST api.shitstorm.fr/v2/auth/request_token.json avec token d'app + lien de callback vers son site pour obtenir une 'callback url'
                            [Body: app_token={my_app_token}&callback_url={my_callback_url_to_my_site}]
     => {request_token: 'token', callback_url: 'example.com/mycallback?verifier=token}

- Insérer le lien reçu dans callback_url dans un lien sur votre page web pour que l'utilisateur puisse s'identifier

- L'utilisateur est redirigé vers le Journal des Shitstorms où il est amené à s'identifier / utiliser son compte

- Le Journal renvoie vers le lien de callback défini plus tôt dans l'appel à request_token.json avec un paramètre GET ?verifier=

- Envoyez le verifier à POST api.shitstorm.fr/v2/auth/access_token.json
                            [Body: verifier={verifier_received}&app_token={my_app_token}]
     => {access_token: 'token', user_id: :id}

=> Vous pouvez utiliser l'access_token reçu en tant que Bearer dans chaque page de l'API v2
