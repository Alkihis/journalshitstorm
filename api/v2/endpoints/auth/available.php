<?php

// Describe available endpoints in this directory [AUTH]

const AVAILABLE = ['request_token.json' => ['name' => 'request.php', 'usage' => ALL, 'rate_limit' => null, 'method' => 'POST', 'rights' => READ_ONLY],
                   'access_token.json' => ['name' => 'access.php', 'usage' => ALL, 'rate_limit' => null, 'method' => 'POST', 'rights' => READ_ONLY]];

