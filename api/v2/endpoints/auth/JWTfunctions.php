<?php

// Fonctions pour les JSON Web Tokens

require_once API_JWT_VENDOR . 'autoload.php';

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;

function buildJWToken(int $expiration, string $token_id, int $id_application, string $use = 'request', $rights, ?int $id_user = null) : string {
    $signer = new Sha256();

    $token = (new Builder())->setIssuer(HTTPS_URL) // Configures the issuer (iss claim)
                ->setAudience(HTTPS_URL) // Configures the audience (aud claim)
                ->setId($token_id, true) // Configures the id (jti claim), replicating as a header item
                ->setIssuedAt(time()) // Configures the time that the token was issue (iat claim)
                ->setNotBefore(time()) // Configures the time that the token can be used (nbf claim)
                ->setExpiration($expiration) // Configures the expiration time of the token (exp claim) Ici, 1h
                ->set('uid', bin2hex(random_bytes(4))) // Configures a new claim, called "uid"
                ->set('use', $use)
                ->set('application_id', $id_application)
                ->set('write', $rights)
                ->set('user', (int)$id_user)
                ->sign($signer, UNIQUE_SIGNATURE) // creates a signature using "UNIQUE_SIGNATURE" as key
                ->getToken(); // Retrieves the generated token


    $token->getHeaders(); // Retrieves the token headers
    $token->getClaims(); // Retrieves the token claims

    return (string)$token;
}

function verifyJWToken($token, string $token_id) : bool {
    $data = new ValidationData(); // It will use the current time to validate (iat, nbf and exp)
    $data->setIssuer(HTTPS_URL);
    $data->setAudience(HTTPS_URL);
    $data->setId((string)$token_id);

    return (bool)$token->validate($data);
}
