<?php

// Access token
// -------------
// Give token linked to app
// Using https://github.com/lcobucci/jwt/blob/3.2/README.md JWToken generator
// -------------

require_once API_JWT_VENDOR . 'autoload.php';
require_once API_DIR . 'v2/endpoints/auth/JWTfunctions.php';

use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;

function loadEndpoint() { // Reçoit un vérifier, vérifie sa validité, et donne un access token final
    global $connexion;
    global $api_handler;
    global $user_object;

    $api_handler->addArg('verifier');
    $api_handler->addArg('app_token');

    // Étape 1 : Vérifier que le token d'app est valide
    $verifier = $api_handler->getArg('verifier');
    $app_token = $api_handler->getArg('app_token');
    // Récupère les détails de l'app
    if($verifier && $app_token) {

        // Vérifie que l'app existe
        $signer = new Sha256();
        $app_token = mysqli_real_escape_string($connexion, $app_token);
        $res = mysqli_query($connexion, "SELECT * FROM TokensApps WHERE token='$app_token';");

        if($res && mysqli_num_rows($res)) { // L'app existe
            $row = mysqli_fetch_assoc($res);

            $token = (new Parser())->parse($verifier); // Parses from a string
            $token->getHeaders(); // Retrieves the token header
            $token->getClaims(); // Retrieves the token claims

            try {
                $type = $token->getClaim('use');
                // Type du token (request, access, verifier, maintener...)
            
                $app_id = (int)$token->getClaim('application_id');
            
                $rights = $token->getClaim('write');

                $user_id = $token->getClaim('user');
            } catch (Exception $e) {
                $GLOBALS['error_handler']->sendError(12);
            }

            if($row['idApp'] == $app_id && $user_id != 0) { // Si le token est bien lié à l'app choisie
                // Vérification signature
                $signer = new Sha256();
                if($token->verify($signer, UNIQUE_SIGNATURE) && $type === 'verifier') { // La signature est valide
                    $res = mysqli_query($connexion, "SELECT * FROM Tokens WHERE token='$verifier';");
                    
                    mysqli_query($connexion, "DELETE FROM Tokens WHERE expires<".time().";");

                    if($res && mysqli_num_rows($res)) {
                        $rowToken = mysqli_fetch_assoc($res);

                        // Vérifie que le token d'accès n'a pas déjà été généré
                        $resAlready = mysqli_query($connexion, "SELECT * FROM Tokens WHERE tokenID='{$rowToken['tokenID']}' AND tokenType='API';");
                        if($resAlready && mysqli_num_rows($resAlready)) {
                            $GLOBALS['error_handler']->sendError(15);
                        }
                        else if(verifyJWToken($token, $rowToken['tokenID'])) {
                            // Supprime le verifier et le request token existant
                            $res = mysqli_query($connexion, "DELETE FROM Tokens WHERE tokenID='{$rowToken['tokenID']}' AND tokenType<>'API';");

                            // Vérification qu'un access token n'existe pas déjà
                            $resAlready = mysqli_query($connexion, "SELECT * FROM Tokens WHERE idUsr='$user_id' AND idApp='$app_id' AND tokenType='API';");
                            if($resAlready && mysqli_num_rows($resAlready)) { // Si c'est le cas, on renvoie le token existant
                                $rowAlready = mysqli_fetch_assoc($resAlready);
                                return ['access_token' => $rowAlready['token'], 'user_id' => (int)$rowAlready['idUsr']];
                            }

                            // Construction de l'access token
                            $expiration = time() + (3600 * 24 * 365 * 6);
                            $token_access = buildJWToken($expiration, $rowToken['tokenID'], $app_id, 'access', $rights, $user_id);
                            $escaped_token = mysqli_real_escape_string($connexion, $token_access);
                            
                            // Enregistre l'access token
                            $res = mysqli_query($connexion, "INSERT INTO Tokens (idUsr, token, tokenType, tokenID, expires, idApp) 
                                                             VALUES ('$user_id', '$escaped_token', 'API', '{$rowToken['tokenID']}', $expiration, '$app_id');");
                            
                            return ['access_token' => $token_access, 'user_id' => $user_id];
                        }
                        else {
                            $GLOBALS['error_handler']->sendError(12);
                        }
                    }
                    else {
                        $GLOBALS['error_handler']->sendError(12);
                    }
                }
                else {
                    $GLOBALS['error_handler']->sendError(12);
                }
            }
            else {
                $GLOBALS['error_handler']->sendError(12);
            }
        }
        else {
            $GLOBALS['error_handler']->sendError(13);
        }
    }
    else {
        $GLOBALS['error_handler']->sendError(14);
    }

    // Étape 2 : Vérifier le token verifier, à quoi il est lié, à quel utilisateur il est lié

    // Délivre un access token valide pour 'toujours'

    return [];
}
