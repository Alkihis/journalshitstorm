<?php

// Callback used for authentification flow
// Meant to be used with index.php ! 

$GLOBALS['disable_navigation_bar'] = true;

require_once API_JWT_VENDOR . 'autoload.php';
require_once API_DIR . 'v2/endpoints/auth/JWTfunctions.php';

use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;

echo "<p class='flow-text'>";
if(isset($_GET['auth_token'], $_GET['callback_url']) && is_string($_GET['auth_token']) && is_string($_GET['callback_url'])) {
    $str_token = (string)$_GET['auth_token'];
    $token = null;
    try {
        $token = (new Parser())->parse($str_token); // Parses from a string
        $token->getHeaders(); // Retrieves the token header
    }
    catch (Exception $e) {
        echo "Impossible de lire le jeton : malformation.<br>Contactez l'aide sur Twitter si le problème persiste.</p>";
        return;
    }
    
    try {
        $type = $token->getClaim('use');
        // Type du token (request, access, maintener...)
    
        $app_id = (int)$token->getClaim('application_id');
    
        $rights = $token->getClaim('write');
    } catch (Exception $e) {
        echo "Impossible de lire le jeton : Données incorrectes ou incomplètes. Tentez de recommencer la procédure d'association.<br>Contactez l'aide sur Twitter si le problème persiste.</p>";
        return;
    }
    
    // echo '<br>ID unique : ' . $token->getClaim('uid');
    // echo '<br>ID lié au token : ' . $token->getHeader('jti');

    // Vérification signature
    $signer = new Sha256();
    if($token->verify($signer, UNIQUE_SIGNATURE) && $type === 'request') {
        // Signature valide

        // Récupération de l'ID du token enregistré
        global $connexion;

        $str_token = mysqli_real_escape_string($connexion, $str_token);
        $res = mysqli_query($connexion, "SELECT * FROM Tokens WHERE token='$str_token';");

        mysqli_query($connexion, "DELETE FROM Tokens WHERE expires<".time().";");

        if($res && mysqli_num_rows($res)) {
            $row = mysqli_fetch_assoc($res);
            
            if(verifyJWToken($token, $row['tokenID'])) {
                // Demande à l'utilisateur de valider l'association
                $app_id = mysqli_real_escape_string($connexion, $app_id);
                $resApp = mysqli_query($connexion, "SELECT * FROM TokensApps WHERE idApp='$app_id';");

                if($resApp && mysqli_num_rows($resApp)) {
                    $rowApp = mysqli_fetch_assoc($resApp);

                    if(!isLogged()) {
                        $query = http_build_query(['auth_token' => $_GET['auth_token']]) . urlencode('&') . http_build_query(['callback_url' => $_GET['callback_url']]);
                        echo "<a href='".HTTPS_URL."login?redirect=app_auth_callback".urlencode('?')."$query'>Connectez-vous</a> pour continuer";
                    }
                    else {
                        $skip_dialog = false;

                        // Vérifie que le token de vérification n'a pas déjà été généré
                        $resAlready = mysqli_query($connexion, "SELECT * FROM Tokens WHERE tokenID='{$row['tokenID']}' AND tokenType='VERIFIER';");
                        if($resAlready && mysqli_num_rows($resAlready)) { // Si c'est le cas, on supprime les existants
                            mysqli_query($connexion, "DELETE FROM Tokens WHERE tokenID='{$row['tokenID']}' AND tokenType='VERIFIER';");
                        }

                        // Vérifie que le token d'access n'existe pas déjà
                        $resAlready = mysqli_query($connexion, "SELECT * FROM Tokens WHERE idUsr='{$_SESSION['id']}' AND idApp='$app_id' AND tokenType='API';");
                        if($resAlready && mysqli_num_rows($resAlready)) { // Si c'est le cas, on ne demande pas à l'utilisateur les permissions
                            $skip_dialog = true;
                        }

                        // Construction du token servant à vérifier
                        $expiration = time() + 3600;
                        $token_verifier = buildJWToken($expiration, $row['tokenID'], $app_id, 'verifier', $rights, $_SESSION['id']);
                        $escaped_token = mysqli_real_escape_string($connexion, $token_verifier);
                        $res = mysqli_query($connexion, "INSERT INTO Tokens (idUsr, token, tokenType, tokenID, expires, idApp) 
                                                         VALUES ('{$_SESSION['id']}', '$escaped_token', 'VERIFIER', '{$row['tokenID']}', $expiration, $app_id);");

                        if(!$skip_dialog) {
                            echo "Vous allez vous identifier pour l'application <span class='bold'>{$rowApp['name']}</span>, {$_SESSION['username']}.<br>";
                            echo "Cette application sera capable de :</p>";
                            echo "<ul class='black-text'>
                                <li>&rsaquo; Lire les shitstorms que vous avez posté</li>
                                <li>&rsaquo; Lire les commentaires que vous avez posté</li>
                                <li>&rsaquo; Lire les propriétés de votre compte, comme votre adresse e-mail</li>
                                <li>&rsaquo; Consulter vos notifications</li>
                                  </ul>";
                            echo "<p class='flow-text'>";
                            if($rowApp['allowWrite']) {
                                echo "Cette application pourra également :";
                            }
                            else {
                                echo "Cette application ne pourra <span class='underline'>PAS</span> :";
                            }
                            echo "</p><ul class='black-text'>
                                <li>&rsaquo; Modifier les paramètres de votre compte</li>
                                <li>&rsaquo; Poster des shitstorms en votre nom</li>
                                <li>&rsaquo; Poster des commentaires ou des réponses en votre nom</li>
                                <li>&rsaquo; Supprimer des publications de votre compte</li>
                                <li>&rsaquo; Gérer vos notifications</li>";
                            echo "</ul>";

                            echo "<p class='flow-text'>Cette application ne pourra jamais :";
                            echo "</p><ul class='black-text'>";
                            echo "<li>&rsaquo; Modifier votre mot de passe</li>";
                            echo "<p class='flow-text'>";
                            echo "<a class='left' href='" . htmlspecialchars(explode('?', $_GET['callback_url'])[0], ENT_QUOTES) . "?verifier=$token_verifier'>Continuer</a> 
                                <a class='right' href='" . htmlspecialchars(explode('?', $_GET['callback_url'])[0], ENT_QUOTES) . "?cancelled={$row['tokenID']}'>Annuler</a></p>";
                            echo "<div class='clearb' style='padding-bottom: 30px;'></div>";
                            echo "<p class='flow-text'>";
                        }
                        else { // Redirection immédiate
                            header('Location: ' . explode('?', $_GET['callback_url'])[0] . "?verifier=$token_verifier");
                        }
                    }
                }
                else {
                    // L'application n'existe pas
                    echo "L'application liée au token n'existe pas.";
                }
            }
            else {
                // Le token n'est pas enregistré
                echo "Impossible de vérifier le jeton. Il a probablement expiré.";
            }
        }
        else {
            // Le token n'est pas enregistré
            echo "Impossible de lire le jeton. Il n'est pas enregistré ou a expiré.";
        }
    }
    else {
        echo "Votre jeton est invalide. Veuillez recommencer la procédure d'associationhgf.";
    }
}
echo '</p>';
