<?php

// Change password for given user

function loadEndpoint() {
    global $user_object;
    global $api_handler;
    global $connexion;

    $api_handler->addArg('id');
    $api_handler->addArg('password');

    $id = $api_handler->getArg('id');

    if($id && is_numeric($id) && $id > 0) {
        $id = (int)$id;

        // Vérifie que l'utilisateur existe
        $res = mysqli_query($connexion, "SELECT id FROM Users WHERE id=$id;");

        if($res && mysqli_num_rows($res)) {
            $password = $api_handler->getArg('password');

            if(is_string($password)) {
                // On ne vérifie PAS si le mot de passe correspond à une regex.
                // Le but est justement de tout permettre facilement.
    
                $password = trim($password);
    
                $hash = password_hash($password, PASSWORD_BCRYPT);
                $hash = mysqli_real_escape_string($connexion, $hash);
    
                $res = mysqli_query($connexion, "UPDATE Users SET passw='$hash' WHERE id=$id;");

                if($res) {
                    return getUserJSONCore($id, false, false, true);
                }
                else {
                    $GLOBALS['error_handler']->sendError(9);
                }
            }
            else {
                $GLOBALS['error_handler']->sendError(14);
            }
        }
        else {
            $GLOBALS['error_handler']->sendError(17);
        }
    }
    else {
        $GLOBALS['error_handler']->sendError(8);
    }
}
