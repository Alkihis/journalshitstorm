<?php

// List existing users in this website

function loadEndpoint() {
    global $api_handler;
    global $connexion;

    $api_handler->addArg('mode');
    $api_handler->addArg('since_id');
    $api_handler->addArg('count');
    $api_handler->addArg('filter');

    $mode = $api_handler->getArg('mode');

    if($mode === 'id') { // Retourne un utilisateur unique complet
        $api_handler->addArg('id');
        $id = $api_handler->getArg('id');

        if($id && $id > 0) {
            $id = (int)$id;
            return getUserJSONCore($id, true, false, true);
        }
        else {
            $GLOBALS['error_handler']->sendError(16);
        }
    }
    else if($mode === 'list') {
        $since = $api_handler->getArg('since_id');

        if($since > 0) {
            $since = (int)$since;
        }
        else {
            $since = 0;
        }

        $count = $api_handler->getArg('count');

        if($count > 0 && $count <= 50) {
            $count = (int)$count;
        }
        else {
            $count = 10;
        }

        $filter = null;
        if($api_handler->getArg('filter')) {
            $filter = mysqli_real_escape_string($connexion, trim($api_handler->getArg('filter')));
        }

        $res = mysqli_query($connexion, "SELECT id, usrname, realname, genre, moderator FROM Users WHERE id>$since ". 
                           ($filter ? "AND (usrname LIKE '%$filter%' OR SOUNDEX('$filter') = SOUNDEX(usrname))" : '') ." ORDER BY id ASC LIMIT 0,$count;");

        $future = [];
        $future['users'] = [];

        $next_id = 0;

        if($res && mysqli_num_rows($res)) {
            while($row = mysqli_fetch_assoc($res)) {
                $future['users'][] = ['id' => (int)$row['id'], 'username' => $row['usrname'], 'realname' => $row['realname'], 
                                      'status' => (int)$row['moderator'], 'status_str' => getMeaningOfStatus($row['moderator'], $row['genre'])];
                $next_id = (int)$row['id'];
            }
        }

        if(count($future['users']) === $count) {
            $future['next_cursor'] = $next_id;
        }
        else {
            $future['next_cursor'] = 0;
        }

        return $future;
    }
    else {
        $GLOBALS['error_handler']->sendError(14);
    }
}
