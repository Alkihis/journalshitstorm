<?php

// Change attributes for given user

function loadEndpoint() {
    global $api_handler;
    global $connexion;

    $api_handler->addArg('id');
    $api_handler->addArg('mail');
    $api_handler->addArg('realname');
    $api_handler->addArg('gender');

    $id = $api_handler->getArg('id');

    if($id && is_numeric($id) && $id > 0) {
        $id = (int)$id;

        // Vérifie que l'utilisateur existe
        $res = mysqli_query($connexion, "SELECT * FROM Users WHERE id=$id;");

        if($res && mysqli_num_rows($res)) {
            $user_object = mysqli_fetch_assoc($res);

            $new_mail = $user_object['mail'];
            $new_rn = $user_object['realname'];
            $new_gr = $user_object['genre'];

            if($api_handler->getArg('mail') && $api_handler->getArg('mail') !== $user_object['mail']) {
                if(preg_match(REGEX_MAIL, trim($api_handler->getArg('mail')))) {
                    $new_mail = mysqli_real_escape_string($connexion, trim($api_handler->getArg('mail')));

                    $res = mysqli_query($connexion, "SELECT id FROM Users WHERE mail='$new_mail';");

                    if(mysqli_num_rows($res) > 0){
                        $GLOBALS['error_handler']->sendError(23);
                    }
                }
                else {
                    $GLOBALS['error_handler']->sendError(19);
                }
            }

            if($api_handler->getArg('realname')) {
                if(preg_match(REGEX_REALNAME, trim($api_handler->getArg('realname')))) {
                    $new_rn = trim($api_handler->getArg('realname'));
                }
                else {
                    $GLOBALS['error_handler']->sendError(21);
                }
            }

            if($api_handler->getArg('gender')) {
                $gender_tmp = $api_handler->getArg('gender');
                $gender_tmp = reverseGender($gender_tmp);

                $new_gr = $gender_tmp;
            }

            updateUser($new_mail, $new_gr, $new_rn, $user_object);

            // Retourne l'objet utilisateur actualisé
            return getUserJSONCore($id, false, false, true);
        }
        else {
            $GLOBALS['error_handler']->sendError(17);
        }
    }
    else {
        $GLOBALS['error_handler']->sendError(8);
    }
}

function reverseGender(string $gender) : string {
    switch($gender) {
        case 'male':
            return 'm';
        case 'female':
            return 'f';
        case 'genderqueer':
            return 'b';
        case 'agender':
            return 'a';
        default:
            $GLOBALS['error_handler']->sendError(20);
    }
}

function updateUser(string $new_mail, string $new_gr, string $new_rn, $user_object) : void {
    global $connexion;

    $mail_set = false;
    if(!$new_mail) {
        $new_mail = null;
    }

    $query = "UPDATE Users SET ";
    $set = '';

    if($new_mail !== $user_object['mail']) {
        $new_mail = mysqli_real_escape_string($connexion, $new_mail);

        $set .= "mail='$new_mail', confirmedMail=0";
        $mail_set = true;
    }

    if($new_gr !== $user_object['genre']) {
        $new_gr = mysqli_real_escape_string($connexion, $new_gr);

        if($set) {
            $set .= ", ";
        }
        $set .= "genre='$new_gr'";
    }

    if($new_rn !== $user_object['realname']) {
        $new_rn = mysqli_real_escape_string($connexion, $new_rn);

        if($set) {
            $set .= ", ";
        }
        $set .= "realname='$new_rn'";
    }

    if($set) {
        $query .= $set . " WHERE id={$user_object['id']};";

        $res = mysqli_query($connexion, $query);

        if(!$res) {
            $GLOBALS['error_handler']->sendError(9);
        }
    }
}
