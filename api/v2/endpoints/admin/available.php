<?php

// Describe available endpoints in this directory [ADMIN]

const AVAILABLE = ['new_password.json' => ['name' => 'new_password.php', 'usage' => ADMIN, 'rate_limit' => 30, 'method' => 'POST', 'rights' => READ_WRITE],
                   'attributes.json' => ['name' => 'attributes.php', 'usage' => ADMIN, 'rate_limit' => 30, 'method' => 'POST', 'rights' => READ_WRITE],
                   'users.json' => ['name' => 'users.php', 'usage' => ADMIN, 'rate_limit' => 75, 'method' => 'GET', 'rights' => READ_WRITE],
                   'confirmed_mail.json' => ['name' => 'confirmed_mail.php', 'usage' => ADMIN, 'rate_limit' => 60, 'method' => 'POST', 'rights' => READ_WRITE],
                   'status.json' => ['name' => 'status.php', 'usage' => ADMIN, 'rate_limit' => 60, 'method' => 'POST', 'rights' => READ_WRITE]];
