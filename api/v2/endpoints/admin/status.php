<?php

// Change a user status

function loadEndpoint() {
    global $connexion;
    global $api_handler;
    global $user_object;

    $id = $api_handler->addGetArg('id');
    $api_handler->addArg('status', null, ['0', '1', '2']);

    if($id) {
        $status = $api_handler->getArg('status');

        if($status !== null) {
            $status = (int)$status;

            // Vérif si l'user est admin et existe (si c'est le cas, inchangeable)
            $res = mysqli_query($connexion, "SELECT moderator FROM Users WHERE id=$id;");

            if($res && mysqli_num_rows($res)) {
                $row = mysqli_fetch_assoc($res);

                // Si c'est moi, on laisse faire "super-admin"
                if((int)$row['moderator'] > 1 && (int)$user_object['id'] !== 9) {
                    $GLOBALS['error_handler']->sendError(2);
                }

                $res = mysqli_query($connexion, "UPDATE Users SET moderator=$status WHERE id=$id;");

                if($res) {
                    return getUserJSONCore($id);
                }
                else {
                    $GLOBALS['error_handler']->sendError(9);
                }
            }
            else {
                $GLOBALS['error_handler']->sendError(17);
            }
        }
        else {
            $GLOBALS['error_handler']->sendError(14);
        }
    }
    else {
        $GLOBALS['error_handler']->sendError(14);
    }
}
