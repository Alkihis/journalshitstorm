<?php

// Change mail status for given user

function loadEndpoint() {
    global $user_object;
    global $api_handler;
    global $connexion;

    $api_handler->addArg('id');
    $api_handler->addArg('confirmed');

    $id = $api_handler->getArg('id');

    if($id && is_numeric($id) && $id > 0) {
        $id = (int)$id;

        // Vérifie que l'utilisateur existe
        $res = mysqli_query($connexion, "SELECT id FROM Users WHERE id=$id;");

        if($res && mysqli_num_rows($res)) {
            $is_confirmed = (int)isActive($api_handler->getArg('confirmed'));

            $res = mysqli_query($connexion, "UPDATE Users SET confirmedMail='$is_confirmed' WHERE id=$id;");

            if($res) {
                return getUserJSONCore($id, false, false, true);
            }
            else {
                $GLOBALS['error_handler']->sendError(9);
            }
        }
        else {
            $GLOBALS['error_handler']->sendError(17);
        }
    }
    else {
        $GLOBALS['error_handler']->sendError(8);
    }
}
