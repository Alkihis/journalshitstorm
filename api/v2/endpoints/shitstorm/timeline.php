<?php

// Search shitstorms

function loadEndpoint() {
    global $api_handler;
    global $connexion;
    global $user_object;

    $date = $api_handler->addGetArg('min_date');
    $max_date = $api_handler->addGetArg('max_date');
    $q = $api_handler->addGetArg('query');
    $order = $api_handler->addGetArg('order');
    $sort = $api_handler->addGetArg('sort');
    $user = $api_handler->addGetArg('user');
    $max_id = $api_handler->addGetArg('max_id');
    $since_id = $api_handler->addGetArg('since_id');
    $count = $api_handler->addGetArg('count', 5);
    $follow_only = isActive($api_handler->addGetArg('followings_only', "0"));
    $with_me = isActive($api_handler->addGetArg('include_my_shitstorms', "1"));

    if($count <= 0 || $count > 50) {
        $count = 5;
    }

    $query = '';
    $order_by = 'ASC';
    $sorting = 'dateShit';

    if($user) {
        if(!is_numeric($user)) {
            $user = getIDFromUsername($user);
        }
        
        if($user > 0 && userExists($user)) {
            $query .= " AND idUsr=$user ";
        }
    }

    if($sort) {
        if($sort === 'rating') {
            $sorting = 'nbLikes';
        }
        else if ($sort === 'add_date') {
            $sorting = 'idSub';
        }
    }

    if($order) {
        if($order === 'desc') {
            $order_by = 'DESC';
        }
    }

    if($max_id) {
        if(is_numeric($max_id) && $max_id > 0) {
            $max_id = (int)$max_id;
            $query .= " AND idSub <= $max_id ";
        }
    }

    if($since_id !== null) {
        if(is_numeric($since_id) && $since_id >= 0) {
            $since_id = (int)$since_id;
            $query .= " AND idSub > $since_id ";
        }
    }

    if($q) {
        if(strlen($q) > 1 && strlen($q) <= 50) {
            $q = mysqli_real_escape_string($connexion, $q);
            $query .= " AND (dscr LIKE '%$q%' OR title LIKE '%$q%') ";
        }
    }
    
    if($date) {
        $explo = explode('-', $date);

        if(count($explo) === 1) {
            if(is_numeric($explo[0]) && $explo[0] <= date('Y')) {
                $query .= " AND dateShit >= '01-01-{$explo[0]}' ";
            }
        }
        else {
            $d = @strtotime($date);

            if($d !== false) {
                $d = date('Y-m-d', $d);

                $query .= " AND dateShit >= '$d' ";
            }
        }
    }
    if($max_date) {
        $explo = explode('-', $date);

        if(count($explo) === 1) {
            if(is_numeric($explo[0]) && $explo[0] <= date('Y')) {
                $query .= " AND dateShit <= '31-12-{$explo[0]}' ";
            }
        }
        else {
            $d = @strtotime($date);

            if($d !== false) {
                $d = date('Y-m-d', $d);

                $query .= " AND dateShit <= '$d' ";
            }
        }
    }

    if($user_object && $follow_only) {
        $user_filter = ($with_me ? "(f.idUsr={$user_object['id']} OR s.idUsr={$user_object['id']})" : "f.idUsr={$user_object['id']}");

        $query = "SELECT s.idSub 
                  FROM Shitstorms s 
                  JOIN Followings f 
                  ON s.idUsr=f.idFollowed 
                  WHERE $user_filter $query 
                  ORDER BY $sorting $order_by 
                  LIMIT 0,$count";
    }
    else {
        $query = "SELECT idSub FROM Shitstorms WHERE 1=1 $query ORDER BY $sorting $order_by LIMIT 0,$count";
    }      
    
    $res = mysqli_query($connexion, $query);

    $future = [];
    if($res && mysqli_num_rows($res)) {
        while($row = mysqli_fetch_assoc($res)) {
            $future[] = getJSONShitstorm(null, (int)$row['idSub']);
        }
    }

    return $future;
}
