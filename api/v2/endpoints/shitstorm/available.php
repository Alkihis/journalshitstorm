<?php

// Describe available endpoints in this directory [SHITSTORM]

// search.json is a macro for timeline.json

const AVAILABLE = ['generate.json' => ['name' => 'generate.php', 'usage' => ALL, 'rate_limit' => 150, 'method' => 'GET', 'rights' => READ_ONLY],
                   'unique' => ['name' => 'unique.php', 'usage' => ALL, 'rate_limit' => 300, 'method' => 'GET', 'rights' => READ_ONLY],
                   'timeline.json' => ['name' => 'timeline.php', 'usage' => LOGGED, 'rate_limit' => 100, 'method' => 'GET', 'rights' => READ_ONLY],
                   'search.json' => ['name' => 'timeline.php', 'usage' => LOGGED, 'rate_limit' => 15, 'method' => 'GET', 'rights' => READ_ONLY, 'message' => 'Deprecated endpoint, use timeline.json instead']];

