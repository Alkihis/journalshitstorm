<?php

// Génération de shitstorm

function loadEndpoint() {
    global $user_object;
    global $connexion;
    global $api_handler;

    require_once(ROOT .'/inc/traitements.php');
    require_once(ROOT .'/inc/generated.php');
    require_once(ROOT .'/inc/postDelete.php');

    if(isset($_SESSION['night_mode'])){
        $GLOBALS['black'] = $_SESSION['night_mode'];
    }

    $skip = 0;
    $count = 5;
    $future = [];

    // Définition des arguments supportés
    $api_handler->addArg('skip');
    $api_handler->addArg('idUsr');
    $api_handler->addArg('viewLikes');
    $api_handler->addArg('date');
    $api_handler->addArg('mode');
    $api_handler->addArg('infos');
    $api_handler->addArg('order');
    $api_handler->addArg('count');
    $api_handler->addArg('load_until');

    if($api_handler->getArg('skip')){
        if($api_handler->getArg('skip') > 0 && is_numeric($api_handler->getArg('skip'))){
            $skip = (int)$api_handler->getArg('skip');
        }
    }

    if(is_numeric($api_handler->getArg('idUsr')) && $api_handler->getArg('idUsr') > 0){
        $idUsr = (int)$api_handler->getArg('idUsr');

        if($api_handler->getArg('viewLikes')){
            $l = (int)$api_handler->getArg('viewLikes');
            $res = mysqli_query($connexion, "SELECT s.* FROM ShitLikes l JOIN Shitstorms s ON s.idSub=l.idSub WHERE l.idUsr='$idUsr' AND l.isLike='$l' LIMIT $skip,".($count+1).";");
        }
        else{
            $res = mysqli_query($connexion, "SELECT * FROM Shitstorms WHERE idUsr='$idUsr' LIMIT $skip,11");
        }

        $future['shitstorms'] = [];
        
        if($res && mysqli_num_rows($res) > 0){
            $i = 0;
            while(($row = mysqli_fetch_assoc($res)) && $i < $count){
                $unique_id = uniqid();

                ob_start();

                generateShitstormCardFromMysqliRow($row, $unique_id);

                $future['shitstorms'][$i]['html'] = ob_get_contents();
                ob_end_clean();

                $future['shitstorms'][$i]['id'] = (int)$row['idSub'];
                $future['shitstorms'][$i]['unique_id'] = $unique_id;
                
                $i++;
            }
            $future['count'] = $i;

            if(mysqli_num_rows($res) > $count){
                $future['next_skip'] = $skip+$count;
            }
            else{
                $future['next_skip'] = 0;
            }
        }
        else {
            $future['count'] = 0;
            $future['next_skip'] = 0;
        }
        return $future;
    }
    else if($api_handler->getArg('date')) {
        require_once(INC_DIR . 'journal_func.php');

        $order_by = 'DESC';
        $order_by_column = 'dateShit';
        $rated = false;
        $date = true;
        $dateTab = null;
        $dateBase = null;
        $dateNext = null;
        $future['infos'] = false;
        $future['infos_get_all'] = false;

        if($api_handler->getArg('mode') === 'rating'){
            $rated = true;
            $order_by_column = 'nbLikes';
        }
        else if($api_handler->getArg('mode') === 'add_date'){
            $order_by_column = 'approvalDate';
        }

        if($api_handler->getArg('date') === 'all'){
            $date = false;

            if(isActive($api_handler->getArg('infos'))){
                $future['infos'] = true;
                $future['infos_get_all'] = true;

                $res = mysqli_query($connexion, "SELECT COUNT(*) c FROM Shitstorms;");

                $future['infos_count_total'] = 0;
                if($res && mysqli_num_rows($res) > 0){
                    $coS = mysqli_fetch_assoc($res);
                    $future['infos_count_total'] = (int)$coS['c'];
                }
            }
        }

        if($api_handler->getArg('order') === 'asc'){
            $order_by = 'ASC';
        }

        $loadUntil = false;

        if(is_numeric($api_handler->getArg('count'))){
            if($api_handler->getArg('count') > 0 && $api_handler->getArg('count') <= 25){
                $count = (int)$api_handler->getArg('count');
            }
        }
        $countP1 = $count+1;

        if($date){
            $date = mysqli_real_escape_string($connexion, $api_handler->getArg('date'));
            $dateBase = date('Y-m');
            $dateNext = $dateBase;

            // Support des intervalles
            $dateTab = explode(':', $date);
            if(count($dateTab) > 1) {
                $interval = isValidInterval($dateTab[0], $dateTab[1]);
                if($interval['status'] === 'ok') {
                    $dateBase = $interval['start'];
                    $dateNext = $interval['end'];
                }
                else {
                    $GLOBALS['error_handler']->sendError(7, ErrorHandler::CLIENT, ['error' => $interval['error']]);
                }
            }
            else {
                $by_year = false;
                $dateTab = explode('-', $date);
        
                if(count($dateTab) > 1 && is_numeric($dateTab[0]) && is_numeric($dateTab[1])){ // C'est un mois
                    try{
                        $dateT = new DateTime($dateTab[0] . '-' . $dateTab[1]);
                
                        $actual = new DateTime($dateBase);
                        if($dateT > $actual){
                            
                        }
                        else{
                            $dateBase = $dateT->format('Y-m');
                            $dateNext = $dateBase;
                        }
                    } catch (Exception $E) {
                        $GLOBALS['error_handler']->sendError(7);
                    }
                }
                elseif(is_numeric($dateTab[0])){ // Année
                    $actual = date('Y');
                    if($dateTab[0] > $actual){
                        $GLOBALS['error_handler']->sendError(7);
                    }
            
                    $dateBase = $dateTab[0] . '-01';
                    $dateNext = $dateTab[0] . '-12';
                    $by_year = true;
                }
                else{ // Echec
                    $GLOBALS['error_handler']->sendError(7);
                }

                if(isActive($api_handler->getArg('infos'))){
                    $future['infos'] = true;
                    $p = getPastLastLegitMonths($dateBase, date('Y-m'), $by_year);

                    $res = mysqli_query($connexion, "SELECT COUNT(*) c FROM Shitstorms WHERE dateShit BETWEEN '$dateBase-01' AND '$dateNext-31' ORDER BY dateShit DESC;");

                    $future['infos_count_total'] = 0;
                    if($res && mysqli_num_rows($res) > 0){
                        $coS = mysqli_fetch_assoc($res);
                        $future['infos_count_total'] = (int)$coS['c'];
                    }

                    $year = explode("-", $dateBase);
                    $month = '01';
                    if(!$by_year && count($year) > 1){
                        $month = getTextualMonth($year[1]);
                    }
                    else{
                        $by_year = true;
                    }
                    $year = $year[0];

                    $future['infos_by_year'] = $by_year;

                    $future['infos_current_text'] = ($by_year ? $year : "$month $year");

                    $future['infos_prec_link'] = null;
                    $future['infos_prec_text'] = null;

                    if($p['prec_month']){ // Si il y a des shitstorms précédentes
                        $future['infos_prec_link'] = HTTPS_URL."journal/{$p['prec_year']}-{$p['prec_month']}".($by_year ? '/force_year' : '');
                        $future['infos_prec_text'] = ltrim(($by_year ? '' : getTextualMonth($p['prec_month'])) . " {$p['prec_year']}");
                    }

                    $future['infos_next_link'] = null;
                    $future['infos_next_text'] = null;

                    if($p['next_month']){ // Si il y a des shitstorms précédentes
                        $future['infos_next_link'] = HTTPS_URL."journal/{$p['next_year']}-{$p['next_month']}".($by_year ? '/force_year' : '');
                        $future['infos_next_text'] = ltrim(($by_year ? '' : getTextualMonth($p['next_month'])) . " {$p['next_year']}");
                    }
                }
            }
            
        }
        
        if(is_numeric($api_handler->getArg('load_until')) && $api_handler->getArg('load_until') > 0){ // Charge les shitstorms jusqu'à
            $loadUntil = (int)$api_handler->getArg('load_until');
        }

        $res = mysqli_query($connexion, "SELECT * FROM Shitstorms " . ($date ? "WHERE dateShit BETWEEN '$dateBase-01' AND '$dateNext-31'" : '') . 
                                        " ORDER BY $order_by_column $order_by, idSub ASC" . ($loadUntil ? ';' : " LIMIT $skip,$countP1;"));

        $future['shitstorms'] = [];

        $idmax = PHP_INT_MAX;
        
        if($res && mysqli_num_rows($res) > 0){
            $i = 0;
            while(($i < $count || $loadUntil) && ($row = mysqli_fetch_assoc($res))){
                ob_start();

                generateShitstormFlowFromMysqliRow($row, null, false, false, true);

                $future['shitstorms'][$i]['html'] = ob_get_clean();

                $future['shitstorms'][$i]['id'] = (int)$row['idSub'];

                if($row['idSub'] < $idmax){
                    $idmax = $row['idSub'];
                }

                $dateActuelle = $row['dateShit'];
                $expl_date = explode('-', $dateActuelle);

                $future['shitstorms'][$i]['week'] = (int)getWeekNumber($dateActuelle);
                $future['shitstorms'][$i]['month'] = (int)$expl_date[1];
                $future['shitstorms'][$i]['year'] = (int)$expl_date[0];
                $future['shitstorms'][$i]['date'] = $row['dateShit'];
                
                $i++;

                if($loadUntil && $row['idSub'] == $loadUntil){ // Si l'ID de la shitstorm correspond à l'ID à charger jusqu'à, on arrête
                    break;
                }
            }
            $future['count'] = $i;

            if($loadUntil){
                $count = $i;
            }

            if(mysqli_num_rows($res) > $count){ // Il reste des shitstorms à fetch
                $rest = mysqli_fetch_assoc($res);
                $future['next_skip'] = $skip + $count;
                $future['next_skip_id'] = $rest['idSub']; // Id de la shitstorm suivante
                $future['next_skip_date'] = $rest['dateShit'];
            }
            else{
                $future['next_skip'] = false;
            }
        }
        else {
            $future['count'] = 0;
            $future['next_skip'] = false;
        }

        return $future;
    }
}

function isSmallerThanDate($date1, $date2){
    try{
        $dateT = new DateTime($date1);

        $actual = new DateTime($date2);
        if($dateT > $actual){
            return false;
        }
        else{
            return true;
        }
    } catch (Exception $E) {
        return false;
    }
}

function isValidInterval($start, $end) : array {
    $dateStart = null;
    $dateEnd = null;
    $actual = date('Y-m-d');
    $actual_year = date('Y');
    
    $st = explode('-', $end);

    if(count($st) > 1){
        if(is_numeric($st[0]) && is_numeric($st[1])){
            if(!isSmallerThanDate($st[0] . '-' . $st[1], $actual)){
                return ['status' => 'fail', 'error' => 'Ending is superior to actual date'];
            }
        }
        else{
            return ['status' => 'fail', 'error' => 'Numerical error'];
        }

        $dateEnd = $st[0] . '-' . $st[1];
    }
    else if(is_numeric($st[0])){
        if($st[0] > $actual_year){
            return ['status' => 'fail', 'error' => 'Numerical error'];
        }

        $dateEnd = $st[0] . '-12';
    }
    else{
        return ['status' => 'fail', 'error' => 'Numerical error'];
    }

    $st = explode('-', $start);

    if(count($st) > 1){
        if(is_numeric($st[0]) && is_numeric($st[1])){
            if(!isSmallerThanDate($st[0] . '-' . $st[1], $dateEnd)){
                return ['status' => 'fail', 'error' => 'Beginning is superior to ending'];
            }
        }
        else{
            return ['status' => 'fail', 'error' => 'Numerical error'];
        }

        $dateStart = $st[0] . '-' . $st[1];
    }
    else if(is_numeric($st[0])){
        if($st[0] > $actual_year){
            return ['status' => 'fail', 'error' => 'Numerical error'];
        }

        $dateStart = $st[0] . '-01';
    }
    else{
        return ['status' => 'fail', 'error' => 'Numerical error'];
    }

    return ['status' => 'ok', 'start' => $dateStart, 'end' => $dateEnd];
}
