<?php

// Chargement de shitstorm unique (par ID)
// via api/v2/shitstorm/unique/:id.json
// :id.json est stocké dans $GLOBALS['defined_arg']

function loadEndpoint() {
    if($GLOBALS['defined_arg']) {
        $id = explode('.json', $GLOBALS['defined_arg'])[0];

        return getJSONShitstorm(null, (int)$id);
    }
    else {
        $GLOBALS['error_handler']->sendError(8);
    }
}
