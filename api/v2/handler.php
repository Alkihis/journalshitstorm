<?php

// --------------
// Handler global
// --------------

// main.php doit être préchargé et les globales defined_endpoint / endpoint_location / endpoint_name définies
// ---
// GLOBALES
// Pour un GET api/v2/shitstorm/get.json?id=83
// =>
// defined_endpoint == get.json

function loadEndpointHandler(?string $path, string $name, int $pos = 1) : void {
    global $user_object;
    global $user_status;
    global $api_handler;

    // On vérifie que les constantes sont définies et valent quelque chose
    if($GLOBALS['defined_endpoint'] && $path) {
        // On définit par défaut les dossiers disponibles à aucun
        $GLOBALS['available_folders'] = null; 

        if(file_exists($path . 'available_folders.php'))
            require $path . 'available_folders.php';

        // Vérification que l'on ne vise pas un sous dossier
        if($GLOBALS['available_folders'] && array_key_exists($GLOBALS['defined_endpoint'], $GLOBALS['available_folders'])) {
            $exploded_url_string = &$GLOBALS['defined_page'];
            // defined_endpoint is $exploded_url_string[1] at this point.
            
            $destination = $GLOBALS['available_folders'][$GLOBALS['defined_endpoint']];

            if(isset($exploded_url_string[$pos+1])) {
                $GLOBALS['defined_endpoint'] = $exploded_url_string[$pos+1];

                if(isset($exploded_url_string[$pos+2])) {
                    $GLOBALS['defined_arg'] = $exploded_url_string[$pos+2];
                }
                else {
                    $GLOBALS['defined_arg'] = '';
                }
            }
            else {
                $GLOBALS['defined_endpoint'] = '';
            }

            $path .= $destination['path'] . '/';

            if(file_exists($path . 'available.php')) { // Si le fichier available.php existe aussi dans la cible
                $name .= $destination['path'] . '/';

                // On vérifie les droits d'accès à l'endpoint
                if(!hasAccessToEndpoint($user_status, $destination['usage'])) {
                    $GLOBALS['error_handler']->sendError(2);
                }
                            
                loadEndpointHandler($path, $name, $pos+1); return;
            }
            else {
                $GLOBALS['error_handler']->sendError(3);
            }
        }

        // On charge la constante AVAILABLE de l'endpoint en question (puisqu'on sait que ce n'est pas un dossier)
        if($path)
            require $path . 'available.php';

        // Si l'endpoint désiré est disponible pour le dossier chargé
        if(array_key_exists($GLOBALS['defined_endpoint'], AVAILABLE)) {
            // On l'enregistre juste pour la lecture du code
            $endpoint = AVAILABLE[$GLOBALS['defined_endpoint']];

            // Initialise l'handler d'arguments / rate-limit avec le nom complet de l'endpoint "dossier/nom_endpoint" et sa méthode d'appel
            $api_handler = new APIHandler($name . $GLOBALS['defined_endpoint'], $endpoint['method']);

            // On vérifie si l'application a les droits nécessaires pour accéder à l'endpoint (lecture et/ou écriture)
            if(isset($GLOBALS['allow_user_write']) && ($GLOBALS['allow_user_write'] < $endpoint['rights'])) {
                $GLOBALS['error_handler']->sendError(24);
            }

            $rate_limit_expiration = $endpoint['expiration_time_count'] ?? 15;

            // On met à jour le rate-limit et on vérifie si l'utilisateur connecté (ou non) dans $user_object a accès à l'endpoint
            if($api_handler->checkAccessToEndpoint($endpoint['usage'], $endpoint['rate_limit'], $user_object, $rate_limit_expiration)) {

                if(isset($endpoint['message']) && is_string($endpoint['message'])) {
                    header('x-api-warning-message: ' . $endpoint['message']);
                }
                // Avant de charger le fichier contenant l'appeleur loadEndpoint(), on vérifie qu'il existe
                if(file_exists($path . $endpoint['name'])) {
                    // On charge l'appeleur et on affiche son résultat : obligatoirement un tableau, sous forme de JSON
                    require $path . $endpoint['name'];
                    
                    if(function_exists('loadEndpoint')) {
                        try {
                            header('Content-Type: application/json');
                            echo json_encode(loadEndpoint());
                        } catch (Throwable $e) {
                            $GLOBALS['error_handler']->sendError(4, ErrorHandler::SERVER, ['exception_text' => $e->getMessage()]);
                        }
                    }
                    else {
                        $GLOBALS['error_handler']->sendError(3);
                    }
                }
                else {
                    $GLOBALS['error_handler']->sendError(3);
                }
            }
            else {
                $GLOBALS['error_handler']->sendError(2);
            }   
        }
        else {
            $GLOBALS['error_handler']->sendError(3);
        }
    }
    else if (!$GLOBALS['defined_endpoint'] && $path && file_exists($path . 'available.php')) { 
        require $path . 'available.php';

        if(defined('READABLE_ENDPOINT')) {
            // Si on a bien un dossier d'endpoint choisi, mais pas de cible, et que l'endpoint est visible (constante définie)
            header('Content-Type: application/json');

            // Conversion du tableau, suppression des noms internes
            $available_e = AVAILABLE;
            foreach($available_e as $key => $e) {
                if(is_array($e)) {
                    if(array_key_exists('name', $e)) {
                        unset($available_e[$key]['name']);
                    }
                }
            }

            echo json_encode(['supported_endpoints' => $available_e]);
        }
        else {
            $GLOBALS['error_handler']->sendError(34);
        }
    }
    else { // L'endpoint n'est pas défini
        $GLOBALS['error_handler']->sendError(34);
    }
}
