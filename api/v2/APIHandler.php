<?php

class APIHandler {
    protected $args = [];
    protected $user_arguments = [];
    protected $method = '';
    protected $endpoint_name = '';

    /**
     * APIHandler constructor
     *
     * @param string $method
     * Define if user arguments should be read from GET or POST.
     * 'both' imports $_REQUEST value, means that $_COOKIE will be imported with !
     * 
     * @param array $supported_args
     * VALUED ARRAY OF STRING who define basic args supported by running endpoint
     */
    public function __construct(string $endpoint_name, string $method = 'both', array $supported_args = []) {
        $this->args = array_fill_keys($supported_args, null);
        $this->endpoint_name = $endpoint_name;

        $method = strtolower($method);
        if(self::isSupportedMethod($method)) {
            $this->method = $method;

            $this->user_arguments = ($method === 'both' ? $_REQUEST : ($method === 'get' ? $_GET : $_POST));
        }
        else {
            throw new InvalidArgumentException('Invalid argument method');
        }
    }

    /**
     * Add supported argument to API Handler
     *
     * @param string $name
     * Name of the argument supported
     * 
     * @param mixed $default_value
     * @return void
     */
    public function addArg(string $name, $default_value = null, ?array $accepted_values = null) : void {
        if(!array_key_exists($name, $this->args))
            $this->args[$name] = ['default' => $default_value, 'accepted' => $accepted_values];
    }

    /**
     * Delete defined argument
     *
     * @param string $name
     * @return boolean
     * true if argument was existant, false otherwise
     */
    public function delArg(string $name) : bool {
        if(array_key_exists($name, $this->args)) {
            unset($this->args[$name]);
            return true;
        }

        return false;
    }

    /**
     * Get argument
     * If argument is set by user, it returns the defined argument.
     * Otherwise, default value is returned.
     *
     * @param string $name
     * @return mixed
     */
    public function getArg(string $name) {
        if(array_key_exists($name, $this->args)) {
            if(array_key_exists($name, $this->user_arguments)) {
                if($this->args[$name]['accepted']) { // Si il y a un tableau de valeurs acceptées
                    if(in_array($this->user_arguments[$name], $this->args[$name]['accepted'])) {
                        return $this->user_arguments[$name];
                    }
                    else {
                        return $this->args[$name]['default'];
                    }
                }
                return $this->user_arguments[$name];
            }
            else {
                return $this->args[$name]['default'];
            }
        }
        else {
            return null;
        }
    }

    /**
     * add and get argument
     * Add argument to argument and immediatly get the setted arg.
     *
     * @param string $name
     * @param mixed $default_value
     * @param array|null $accepted_values
     * @return mixed
     */
    public function addGetArg(string $name, $default_value = null, ?array $accepted_values = null) {
        $this->addArg($name, $default_value, $accepted_values);
        return $this->getArg($name);
    }

    /**
     * Check if $method is a supported method for checking arguments
     *
     * @param string $method
     * @return boolean
     */
    static protected function isSupportedMethod(string $method) : bool {
        return ($method === 'both' || $method === 'get' || $method === 'post');
    }

    public function checkAccessToEndpoint(int $mode, ?int $endpoint_max_rate, $user_object, int $minutes_before_reset = 15) : bool {
        global $connexion;
        global $user_status;

        if(!hasAccessToEndpoint($user_status, $mode)) {
            // Si on demande un mode supérieur au mode non authentifié (visiteur/tout) et que l'utilisateur n'est pas connecté
            $GLOBALS['error_handler']->sendError(1);
            return false;
        }
        else if ($user_object || isset($GLOBALS['token_visitor_id'])) { // Si un utilisateur est connecté, on lui applique un rate-limit
            $endpoint = $this->endpoint_name;
            $id_user = (int)($user_object['id'] ?? $GLOBALS['token_visitor_id'] ?? null);

            if($id_user) {
                if($endpoint_max_rate > 0) { // Si l'endpoint est rate-limited
                    // Supprime les rates limites qui sont expirés il y a plus d'un jour
                    mysqli_query($connexion, "DELETE FROM RateLimits WHERE expiration < ".(time() - (3600 * 24)).";");
                    // Remise à 0 des timestamp > 15 minutes écoulés
                    mysqli_query($connexion, "UPDATE RateLimits SET count=0 WHERE expiration < UNIX_TIMESTAMP();");
    
                    // On applique un rate limit sur l'application utilisée (0 si session PHP)
                    $q = mysqli_query($connexion, "SELECT * FROM RateLimits WHERE idUsr='$id_user' AND type='$endpoint' AND idApp='{$GLOBALS['token_application_id']}';");
                    if($q && mysqli_num_rows($q)) {
                        $q = mysqli_fetch_assoc($q);
                        if($q['count'] >= $endpoint_max_rate) {
                            header('x-rate-limit-reset: ' . (int)$q['expiration']);
                            $GLOBALS['error_handler']->sendError(88, ErrorHandler::CLIENT, ['retry_after' => (int)$q['expiration']]);
                            return false;
                        }
                        else {
                            $new_c = $q['count'] + 1;
                            mysqli_query($connexion, "UPDATE RateLimits SET count='$new_c'".($q['count'] == 0 ? ", expiration=UNIX_TIMESTAMP() + ($minutes_before_reset*60)" : '')." WHERE idRate='{$q['idRate']}';");
                        }

                        // Set les headers de rate limit
                        header('x-rate-limit-limit: ' . $endpoint_max_rate);
                        header('x-rate-limit-remaining: ' . ($endpoint_max_rate - $new_c));
                        header('x-rate-limit-reset: ' . (int)$q['expiration']);
                    }
                    else {
                        mysqli_query($connexion, "INSERT INTO RateLimits (idUsr, type, count, expiration, idApp) 
                                                  VALUES ('$id_user', '$endpoint', 1, UNIX_TIMESTAMP() + ($minutes_before_reset*60), '{$GLOBALS['token_application_id']}');");
                    }
                }
            }
        }

        return true;
    }
}
