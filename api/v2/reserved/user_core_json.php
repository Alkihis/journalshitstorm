<?php

function getUserJSONCore(int $id, bool $friendship_with = true, bool $short_user = false, bool $show_detailed_infos = false) {
    global $connexion;
    global $user_object;

    $res = mysqli_query($connexion, "SELECT usrname, profile_img, banner_img, realname, mail, lastlogin, registerdate, findable, followable, genre,
                                            confirmedMail, id, moderator, storm, snow, (SELECT COUNT(*) FROM Shitstorms WHERE idUsr=id) c 
                                     FROM Users 
                                     WHERE id='$id';");

    $future = array();

    if(mysqli_num_rows($res) > 0){
        $row = mysqli_fetch_assoc($res);

        // Construction de la base commune avec / sans token
        $future['id'] = (int)$row['id'];
        $future['username'] = $row['usrname'];
        $future['status'] = (int)$row['moderator'];
        $future['status_str'] = getMeaningOfStatus($row['moderator'], $row['genre']);
        $future['realname'] = $row['realname'];
        $future['shitstorm_count'] = (int)$row['c'];
        $future['register_date'] = $row['registerdate'];
        $future['have_storm'] = (bool)$row['storm'];
        $future['have_snow'] = (bool)$row['snow'];

        $future['profile_img'] = $row['profile_img'];
        $future['full_profile_img'] = HTTPS_URL . $row['profile_img'];
        $future['banner_img'] = $row['banner_img'];
        $future['full_banner_img'] = ($row['banner_img'] ? HTTPS_URL . $row['banner_img'] : null);

        $future['pronoun'] = ($row['genre'] === 'f' ? 'elle' : ($row['genre'] === 'b' || $row['genre'] === 'a' ? 'iel' : 'il'));
        $future['gender'] = ($row['genre'] === 'f' ? 'female' : ($row['genre'] === 'b' ? 'genderqueer' : ($row['genre'] === 'a' ? 'agender' : 'male')));

        $future['followable'] = (bool)$row['followable'];
    
        if($user_object && $show_detailed_infos && ($user_object['id'] === $row['id'] || $user_object['moderator'] > 1)) { // Si on est l'utilisateur demandé ou administrateur
            $future['last_login'] = $row['lastlogin'];
            $future['email_address'] = $row['mail'];
            $future['email_confirmed'] = (bool)$row['confirmedMail'];
            $future['findable'] = (bool)$row['findable'];
        }

        if(!$short_user) {
            // Calcul nombre de followings et nombre de followers
            $reqFollowing = mysqli_query($connexion, "SELECT COUNT(*) c FROM Followings WHERE idUsr='{$row['id']}';");
            $future['followings'] = 0;

            if($reqFollowing && mysqli_num_rows($reqFollowing)) {
                $rowFollowing = mysqli_fetch_assoc($reqFollowing);
                $future['followings'] = (int)$rowFollowing['c'];
            }
            $reqFollower = mysqli_query($connexion, "SELECT COUNT(*) c FROM Followings WHERE idFollowed='{$row['id']}';");
            $future['followers'] = 0;

            if($reqFollower && mysqli_num_rows($reqFollower)) {
                $rowFollower = mysqli_fetch_assoc($reqFollower);
                $future['followers'] = (int)$rowFollower['c'];
            }

            if($user_object) {
                if($user_object['id'] === $row['id']) {
                    $future['is_you'] = true;
                }
            }
        }

        if($friendship_with && $user_object){
            $friendship_with = $user_object['id'];
            $reqFollowing = mysqli_query($connexion, "SELECT idFollowed, acceptMail FROM Followings WHERE idFollowed='{$row['id']}' AND idUsr='$friendship_with';");
            // Recherche si l'utilisateur recherché follow $followedId
            $future['following'] = ($reqFollowing && mysqli_num_rows($reqFollowing));

            if($future['following']) {
                $rowFollowing = mysqli_fetch_assoc($reqFollowing);

                $future['follow_with_mail'] = (bool)$rowFollowing['acceptMail'];
            }

            $reqFollowing = mysqli_query($connexion, "SELECT idFollowed FROM Followings WHERE idUsr='{$row['id']}' AND idFollowed='$friendship_with';");
            $future['follows_you'] = ($reqFollowing && mysqli_num_rows($reqFollowing));
        }
    } 
    else {
        if(isset($GLOBALS['dont_crash_on_error'])) {
            return null;
        }
        else {
            $GLOBALS['error_handler']->sendError(6);
        }
    }

    return $future;
}
