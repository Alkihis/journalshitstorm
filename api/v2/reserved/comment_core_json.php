<?php

// Returns comment core via ID / mysqli_row

function getJSONComment($row = null, ?int $id = null, bool $has_fav = true, bool $with_replies = false, int $replies_count = 10) : ?array {
    global $connexion;
    global $user_object;
    global $api_handler;

    $future = [];
    $comment = [];

    if($row === null && $id && is_numeric($id)) {
        $id = (int)$id;
        $res = mysqli_query($connexion, "SELECT c.*, u.usrname FROM Comment c JOIN Users u ON c.idUsr=u.id WHERE idCom='$id';");

        if($res && mysqli_num_rows($res)) {
            $row = mysqli_fetch_assoc($res);
        }
    }

    if($row){
        $comment['id'] = (int)$row['idCom'];

        if(isset($row['c'])) {
            $comment['rating'] = (int)$row['c'];
        }
        else {
            $resLike = mysqli_query($connexion, "SELECT c.* FROM ComLikes c WHERE idCom='{$row['idCom']}';");
            if($resLike){
                $comment['rating'] = (int) mysqli_num_rows($resLike);
            }
            else {
                $GLOBALS['error_handler']->sendError(9);
            }
        }

        $comment['username'] = $row['usrname'];
        $comment['id_user'] = (int)$row['idUsr'];
        $comment['date'] = $row['contentTime'];
        $comment['content'] = $row['content'];
        $comment['id_shitstorm'] = (int)$row['idSub'];
        $comment['in_reply_to_id'] = $row['inReplyToId'];
        $comment['in_reply_to_id_conversation'] = $row['inReplyToIdMain'];

        // Vérifie si l'utilisateur connecté a fav ce commentaire
        if($has_fav && $user_object) {
            $resLike = mysqli_query($connexion, "SELECT * FROM ComLikes WHERE idCom='{$row['idCom']}' AND idUsr={$user_object['id']};");
            $comment['favorited'] = $resLike && mysqli_num_rows($resLike);
        }

        if($with_replies && $row['inReplyToIdMain'] === null) { // Construit les replies à ce commentaire (si il n'est pas une réponse)
            $resRep = mysqli_query($connexion, "SELECT c.*, u.usrname FROM Comment c JOIN Users u ON c.idUsr=u.id WHERE inReplyToIdMain={$comment['id']} LIMIT 0,$replies_count;");

            $comment['replies'] = [];
            $next_id = 0;
            if($resRep && mysqli_num_rows($resRep)) {
                while($rowRep = mysqli_fetch_assoc($resRep)) {
                    $comment['replies'][] = getJSONComment($rowRep);
                    $next_id = (int)$rowRep['idCom'];
                }
            }

            if(count($comment['replies']) === $replies_count) {
                $comment['replies_next_cursor'] = $next_id;
            }
            else {
                $comment['replies_next_cursor'] = 0;
            }
        }

        $comment['can_delete'] = ($user_object && ((int)$row['idUsr'] === (int)$user_object['id'] || (int)$user_object['moderator'] > 0));

        if($row['idUsr'] > 0) {
            $comment['user'] = getUserJSONCore($row['idUsr']);
        }
        else {
            $comment['user'] = null;
        }
    }
    else {
        if(isset($GLOBALS['dont_crash_on_error'])) {
            return null;
        }
        else {
            $GLOBALS['error_handler']->sendError(10);
        }
    }

    return $comment;
}
