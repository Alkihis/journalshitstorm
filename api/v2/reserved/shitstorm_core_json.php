<?php

// Returns shitstorm core via ID / mysqli_row

function getJSONShitstorm($row = null, $id = null, bool $include_user = true, bool $has_rated = true, 
                          bool $full_rating = false, bool $answers = false, bool $user = true) {
    global $connexion;
    global $user_object;
    global $api_handler;

    $future = [];

    $api_handler->addArg('full_rating');
    $api_handler->addArg('include_user');
    $api_handler->addArg('include_answers');

    $withAn = false;
    $withUser = false;
    $withFullRating = isActive($api_handler->getArg('full_rating')) || $full_rating;
    $withAn = isActive($api_handler->getArg('include_answers')) || $answers;

    if($include_user && (isActive($api_handler->getArg('include_user')) || $user)){
        $withUser = true;
    }

    $shitstorm = [];

    if($row === null && $id && is_numeric($id)) {
        $id = (int)$id;
        $res = mysqli_query($connexion, "SELECT s.*, u.usrname FROM Shitstorms s LEFT JOIN Users u ON s.idUsr=u.id WHERE idSub='$id';");

        if($res && mysqli_num_rows($res)) {
            $row = mysqli_fetch_assoc($res);
        }
    }

    if($row){
        $shitstorm['id'] = (int)$row['idSub'];

        if($withFullRating){
            $likecount = 0;
            $dislikecount = 0;

            $resLike = mysqli_query($connexion, "SELECT c.* FROM ShitLikes c WHERE idSub='{$row['idSub']}' AND isLike=1;");
            if($resLike){
                $likecount = mysqli_num_rows($resLike);
            }
            else {
                $GLOBALS['error_handler']->sendError(9, ErrorHandler::SERVER);
            }

            $resLike = mysqli_query($connexion, "SELECT c.* FROM ShitLikes c WHERE idSub='{$row['idSub']}' AND isLike=0;");
            if($resLike){
                $dislikecount = mysqli_num_rows($resLike);
            }
            else {
                $GLOBALS['error_handler']->sendError(9, ErrorHandler::SERVER);
            }

            $shitstorm['total_rates'] = (int)$likecount+$dislikecount;
            $shitstorm['rating'] = (int)$likecount-$dislikecount; 
            $shitstorm['like_count'] = (int)$likecount;
            $shitstorm['dislike_count'] = (int)$dislikecount;
        }
        else{
            $shitstorm['rating'] = (int)$row['nbLikes'];
        }

        // On vérifie si l'utilisateur courant suit la shitstorm
        if($user_object) {
            $friendship_with = $user_object['id'];
            $reqFollowing = mysqli_query($connexion, "SELECT idFollowed, withMail FROM ShitFollowings WHERE idFollowed='{$row['idSub']}' AND idFollower='$friendship_with';");
            // Recherche si l'utilisateur recherché follow $idSub
            $shitstorm['subscribed'] = ($reqFollowing && mysqli_num_rows($reqFollowing));

            if($shitstorm['subscribed']) {
                $rowFollowing = mysqli_fetch_assoc($reqFollowing);

                $shitstorm['subscribe_with_mail'] = (bool)$rowFollowing['withMail'];
            }
        }

        // Si on doit vérifier si l'utilisateur courant a fav la shitstorm
        // et en quel type
        if($user_object && $has_rated) {
            $shitstorm['rated'] = false;

            $resLike = mysqli_query($connexion, "SELECT * FROM ShitLikes WHERE idSub='{$row['idSub']}' AND idUsr={$user_object['id']};");
            if($resLike && mysqli_num_rows($resLike)){
                $rowLike = mysqli_fetch_assoc($resLike);
                $shitstorm['rated'] = true;
                $shitstorm['rate_type'] = (int)$rowLike['isLike'];
                $shitstorm['rate_date'] = $rowLike['likeDate'];
            }
        }

        $shitstorm['username'] = ($row['usrname'] ? $row['usrname'] : $row['pseudo']);
        $shitstorm['id_user'] = (int)$row['idUsr'];
        $shitstorm['date'] = $row['dateShit'];
        $shitstorm['content'] = $row['dscr'];
        $shitstorm['title'] = $row['title'];
        $shitstorm['id_approvator'] = (int)$row['idApprovator'];
        $shitstorm['approval_date'] = $row['approvalDate'];

        // Count des commentaires
        $resC = mysqli_query($connexion, "SELECT COUNT(*) c FROM Comment WHERE idSub='$id';");
        $rowC['c'] = 0;

        if($resC && mysqli_num_rows($resC)) {
            $rowC = mysqli_fetch_assoc($resC);
        }

        $shitstorm['comment_count'] = (int)$rowC['c'];

        // Fin des métadata, début des liens
        $shitstorm['links'] = array();
        $links = &$shitstorm['links'];
        $res2 = mysqli_query($connexion, "SELECT l.idLink, l.link, l.idTweet
                                          FROM Links l 
                                          WHERE l.idSub='{$row['idSub']}';");
        if(!$res2){
            $GLOBALS['error_handler']->sendError(9, ErrorHandler::SERVER);
        }
        $jlink = 0;
        while($rowlink = mysqli_fetch_assoc($res2)){
            $links[$jlink]['id_link'] = (int)$rowlink['idLink'];
            if(preg_match(REGEX_IMG_LINK_UPLOADED, $rowlink['link'])) {
                $links[$jlink]['full_link'] = HTTPS_URL . $rowlink['link'];
            }
            else {
                $links[$jlink]['full_link'] = $rowlink['link'];
            }
            $links[$jlink]['link'] = $rowlink['link'];
            $links[$jlink]['id_tweet'] = $rowlink['idTweet'];
            $links[$jlink]['linked_tweet'] = null;
            
            if($rowlink['idTweet']) {
                $links[$jlink]['linked_tweet'] = getTweetFromSave($rowlink['idTweet']);
            }

            $jlink++;
        }

        if($withUser){ // Le contenu de l'utilisateur est demandé
            if($row['idUsr'] > 0) {
                $shitstorm['user'] = getUserJSONCore($row['idUsr'], true);
            }
            else {
                $shitstorm['user'] = null;
            }
        }

        if($withAn){ // Si les réponses sont demandées
            // Début des réponses
            $shitstorm['answers'] = getAnswersJSON($row['idSub']);
            $shitstorm['answer_count'] = count($shitstorm['answers']);
        }
    }
    else {
        if(isset($GLOBALS['dont_crash_on_error'])) {
            return null;
        }
        else {
            $GLOBALS['error_handler']->sendError(10);
        }
    }

    return $shitstorm;
}

function getAnswersJSON(int $idSub) : array {
    global $connexion;

    $answers = array();

    $res3 = mysqli_query($connexion, "SELECT a.idAn FROM Answers a WHERE a.idSub='$idSub';");
    if(!$res3) {
        $GLOBALS['error_handler']->sendError(9, ErrorHandler::SERVER);
    }

    if(mysqli_num_rows($res3) > 0){
        while($rowansw = mysqli_fetch_assoc($res3)){
            $answers[] = getAnswerJSON($rowansw['idAn']);
        }
    }

    return $answers;
}

function getAnswerJSON(int $idAn) : ?array {
    global $connexion;

    $answer = [];

    $res3 = mysqli_query($connexion, "SELECT a.* FROM Answers a WHERE a.idAn='$idAn';");
    if(!$res3){
        $GLOBALS['error_handler']->sendError(9, ErrorHandler::SERVER);
    }

    if(mysqli_num_rows($res3) > 0){
        $rowansw = mysqli_fetch_assoc($res3);

        $answer['id'] = (int)$rowansw['idAn'];
        $answer['id_user'] = (int)$rowansw['idUsrA'];
        $answer['id_shitstorm'] = (int)$rowansw['idSub'];
        $answer['content'] = $rowansw['dscrA']; 

        if(preg_match(REGEX_IMG_LINK_UPLOADED, $rowansw['linkA'])) {
            $answer['full_link'] = HTTPS_URL . $rowansw['linkA'];
        }
        else {
            $answer['full_link'] = $rowansw['linkA'];
        }
        $answer['link'] = $rowansw['linkA']; 
        $answer['date'] = $rowansw['dateAn'];

        $answer['id_tweet'] = $rowansw['idTweet'];
        $answer['linked_tweet'] = null;
        
        if($rowansw['idTweet']) {
            $answer['linked_tweet'] = getTweetFromSave($rowansw['idTweet']);
        }
    }
    else {
        if(isset($GLOBALS['dont_crash_on_error'])) {
            return null;
        }
        else {
            $GLOBALS['error_handler']->sendError(37);
        }
    }

    return $answer;
}

// ----------------------------
// WAITING SHITSTORM & ANSWER -
// ----------------------------

function getJSONWaitingShitstorm(int $id, bool $include_user = true, bool $include_answers = false) {
    global $connexion;
    global $user_object;
    global $api_handler;

    $future = [];

    $withUser = $include_user;
    $withAn = $include_answers;

    $shitstorm = [];
    $row = null;

    if($id > 0) {
        $res = mysqli_query($connexion, "SELECT s.*, u.usrname FROM Waiting s LEFT JOIN Users u ON s.idUsr=u.id WHERE idSub='$id';");

        if($res && mysqli_num_rows($res)) {
            $row = mysqli_fetch_assoc($res);
        }
    }

    if($row){
        $shitstorm['id'] = (int)$row['idSub'];

        $shitstorm['username'] = ($row['usrname'] ? $row['usrname'] : $row['pseudo']);
        $shitstorm['id_user'] = (int)$row['idUsr'];
        $shitstorm['date'] = $row['dateShit'];
        $shitstorm['content'] = $row['dscr'];
        $shitstorm['title'] = $row['title'];
        $shitstorm['locked'] = (bool)$row['locked'];

        // Fin des métadata, début des liens
        $shitstorm['links'] = array();
        $links = &$shitstorm['links'];
        $res2 = mysqli_query($connexion, "SELECT l.idLink, l.link
                                          FROM WaitingLinks l 
                                          WHERE l.idSub='{$row['idSub']}';");
        if(!$res2){
            $GLOBALS['error_handler']->sendError(9, ErrorHandler::SERVER);
        }
        $jlink = 0;
        while($rowlink = mysqli_fetch_assoc($res2)){
            $links[$jlink]['id_link'] = (int)$rowlink['idLink'];
            if(preg_match(REGEX_IMG_LINK_UPLOADED, $rowlink['link'])) {
                $links[$jlink]['full_link'] = HTTPS_URL . $rowlink['link'];
            }
            else {
                $links[$jlink]['full_link'] = $rowlink['link'];
            }
            $links[$jlink]['link'] = $rowlink['link'];

            $jlink++;
        }

        if($withUser){ // Le contenu de l'utilisateur est demandé
            if($row['idUsr'] > 0) {
                $shitstorm['user'] = getUserJSONCore($row['idUsr'], true);
            }
            else {
                $shitstorm['user'] = null;
            }
        }

        if($withAn) {
            $shitstorm['answers'] = getWaitingAnswersJSON((int)$row['idSub']);
        }
    }
    else {
        if(isset($GLOBALS['dont_crash_on_error'])) {
            return null;
        }
        else {
            $GLOBALS['error_handler']->sendError(10);
        }
    }

    return $shitstorm;
}

function getWaitingAnswersJSON(int $idSub) : array {
    global $connexion;

    $answers = array();

    $res3 = mysqli_query($connexion, "SELECT a.idSubA FROM WaitAnsw a WHERE a.idSub='$idSub';");
    if(!$res3) {
        $GLOBALS['error_handler']->sendError(9, ErrorHandler::SERVER);
    }

    if(mysqli_num_rows($res3) > 0){
        while($rowansw = mysqli_fetch_assoc($res3)){
            $answers[] = getWaitingAnswerJSON($rowansw['idSubA']);
        }
    }

    return $answers;
}

function getWaitingAnswerJSON(int $idAn) : ?array {
    global $connexion;

    $answer = [];

    $res3 = mysqli_query($connexion, "SELECT a.* FROM WaitAnsw a WHERE a.idSubA='$idAn';");
    if(!$res3){
        $GLOBALS['error_handler']->sendError(9, ErrorHandler::SERVER);
    }

    if(mysqli_num_rows($res3) > 0){
        $rowansw = mysqli_fetch_assoc($res3);

        $answer['id'] = (int)$rowansw['idSubA'];
        $answer['id_user'] = (int)$rowansw['idUsrA'];
        $answer['id_shitstorm'] = (int)$rowansw['idSub'];
        $answer['content'] = $rowansw['dscrA']; 

        if(preg_match(REGEX_IMG_LINK_UPLOADED, $rowansw['linkA'])) {
            $answer['full_link'] = HTTPS_URL . $rowansw['linkA'];
        }
        else {
            $answer['full_link'] = $rowansw['linkA'];
        }
        $answer['link'] = $rowansw['linkA']; 
        $answer['date'] = $rowansw['dateAn'];
        $answer['locked'] = (bool)$rowansw['locked'];
        $answer['linked_to_waiting_shitstorm'] = (bool)$rowansw['fromWaitingShitstorm'];
    }
    else {
        if(isset($GLOBALS['dont_crash_on_error'])) {
            return null;
        }
        else {
            $GLOBALS['error_handler']->sendError(37);
        }
    }

    return $answer;
}

