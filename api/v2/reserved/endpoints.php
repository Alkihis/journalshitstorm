<?php

// Constants definition

const BOOL_URL_VALUES = ['false', 'f', '0', 'true', 't', '1'];

const VISITOR = 0;
const LOGGED = 1;
const MODO = 2;
const ADMIN = 3;
const ALL = -1;

const READ_WRITE_PASSWORD = 2;
const READ_WRITE = 1;
const READ_ONLY = 0;

const ENDPOINTS = ['shitstorm' => ['path' => 'shitstorm/', 'usage' => ALL],
                   'comment' => ['path' => 'comment/', 'usage' => VISITOR],
                   'example' => ['path' => 'example/', 'usage' => ALL],
                   'auth' => ['path' => 'auth/', 'usage' => ALL],
                   'user' => ['path' => 'user/', 'usage' => VISITOR],
                   'profile' => ['path' => 'profile/', 'usage' => VISITOR],
                   'account' => ['path' => 'account/', 'usage' => LOGGED],
                   'rate' => ['path' => 'rate/', 'usage' => VISITOR],
                   'saves' => ['path' => 'saves/', 'usage' => VISITOR],
                   'admin' => ['path' => 'admin/', 'usage' => ADMIN],
                   'moderation' => ['path' => 'moderation/', 'usage' => MODO],
                   'friendships' => ['path' => 'friendships/', 'usage' => VISITOR],
                   'subscriptions' => ['path' => 'subscriptions/', 'usage' => LOGGED],
                   'activity' => ['path' => 'activity/', 'usage' => LOGGED],
                   'edit' => ['path' => 'edit/', 'usage' => LOGGED],
                   'generation' => ['path' => 'generation/', 'usage' => VISITOR]];
