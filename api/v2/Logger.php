<?php

require_once API_JWT_VENDOR . 'autoload.php';
require_once API_DIR . 'v2/endpoints/auth/JWTfunctions.php';

use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;

class Logger {
    protected $user_object = null;
    protected $user_sqli_request = null;
    protected $is_visitor = false;
    protected $visitor_id = null;

    /**
     * Logger constructor
     * Log user using Bearer Token or session (by cookie)
     * 
     * @param bool $will_force_connect_if_possible
     * If possible, force connection using checkConnect() function
     */
    public function __construct(bool $will_force_connect_if_possible = false) {
        global $connexion;

        $GLOBALS['allow_user_write'] = 0;

        $token = $this->getBearerToken();
        
        if(!$token) {
            if($will_force_connect_if_possible) {
                checkConnect(null, false);
            }
            
            if(isset($_SESSION['id'])) {
                $this->is_visitor = true;
                $id = (int)$_SESSION['id'];
        
                $user_res = mysqli_query($connexion, "SELECT * FROM Users WHERE id=$id;");
                if($user_res && mysqli_num_rows($user_res)) {
                    $this->user_sqli_request = $user_res;
                    $this->user_object = mysqli_fetch_assoc($user_res);
                    // L'authentification par session a tous les droits, et a pour appID 0
                    $GLOBALS['allow_user_write'] = READ_WRITE_PASSWORD;
                    $GLOBALS['token_application_id'] = 0;
                }
                else { // L'utilisateur connecté n'existe plus
                    session_destroy();
                    session_start();
                }
            }
            else if (isset($_SESSION['visitor'], $_SESSION['visitor_id']) && $_SESSION['visitor'] === true) {
                $this->is_visitor = true;
                $this->visitor_id = $_SESSION['visitor_id'];
                $GLOBALS['token_visitor_id'] = &$this->visitor_id;
                $GLOBALS['token_application_id'] = 0;
                $GLOBALS['allow_user_write'] = READ_WRITE;
            }
            else { // Impossible de connecter l'utilisateur, il est sans doute déconnecté
                // Rien à faire
            }
        }
        else {
            $res = mysqli_query($connexion, "SELECT * FROM Tokens WHERE token='$token' AND tokenType='API';");
            if($res && mysqli_num_rows($res)) {
                $row = mysqli_fetch_assoc($res);
                mysqli_query($connexion, "UPDATE Tokens SET lastUse=CURRENT_TIMESTAMP WHERE idToken='{$row['idToken']}';");

                $token = (new Parser())->parse($token); // Parses from a string
                $token->getHeaders(); // Retrieves the token header
                $token->getClaims(); // Retrieves the token claims
                try {
                    $type = $token->getClaim('use');
                    // Type du token (request, access, maintener...)
                
                    $app_id = $token->getClaim('application_id');

                    $user_id = $token->getClaim('user');
                
                    $GLOBALS['allow_user_write'] = (int)$token->getClaim('write');
                } catch (Exception $e) {
                    $GLOBALS['error_handler']->sendError(5);
                }

                if($user_id && $app_id && $type == 'access') { // Si on a bien l'userID, appID et que le token est un token d'accès,
                    // On autorise la connexion
                    $GLOBALS['token_application_id'] = (int)$app_id;
                    // Vérification que l'application existe
                    $resApp = mysqli_query($connexion, "SELECT idApp, allowWrite FROM TokensApps WHERE idApp={$GLOBALS['token_application_id']};");
                    if($resApp && mysqli_num_rows($resApp)) {
                        // On vérifie les droits de l'app
                        $rowApp = mysqli_fetch_assoc($resApp);
                        if((int)$rowApp['allowWrite'] < (int)$GLOBALS['allow_user_write']) {
                            $GLOBALS['allow_user_write'] = (bool)$rowApp['allowWrite'];
                        }

                        $user_res = mysqli_query($connexion, "SELECT * FROM Users WHERE id='{$user_id}';");
                        if($user_res && mysqli_num_rows($user_res)) {
                            $this->user_sqli_request = $user_res;
                            $this->user_object = mysqli_fetch_assoc($user_res);
                        }
                        else {
                            $GLOBALS['error_handler']->sendError(6);
                        }
                    }
                    else {
                        $GLOBALS['error_handler']->sendError(11);
                    }
                }
                else {
                    $GLOBALS['error_handler']->sendError(5);
                }
            }
            else {
                $GLOBALS['error_handler']->sendError(5);
            }
        }
    }

    public function getUserObject() {
        return $this->user_object;
    }

    public function getUserMysqliRes() {
        return $this->user_sqli_request;
    }

    public function isAuthentificated() : bool {
        return (bool)$this->user_object;
    }

    // Return true if user is a site visitor
    public function isVisitor() : bool {
        return $this->is_visitor;
    }

    public function getVisitorId() : ?int {
        return $this->visitor_id;
    }

    protected function getStatus() : int {
        return ($this->user_object ? (int)$this->user_object['moderator'] : -1);
    }

    // Get status (ALL, VISITOR, LOGGED...) of current API visitor
    public function getUserStatus() : int {
        $status = $this->getStatus();

        if($status >= 0) {
            if($status > 0) {
                return ($status > 1 ? ADMIN : MODO);
            }
            else {
                return LOGGED;
            }
        }
        else {
            return ($this->isVisitor() ? VISITOR : ALL);
        }
    }

    /** 
     * Get header Authorization
     * */
    protected function getAuthorizationHeader(){
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        }
        else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } 
        else if (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            if(isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
        return $headers;
    }
    /**
    * get access token from header
    * */
    public function getBearerToken() {
        $headers = $this->getAuthorizationHeader();
        // HEADER: Get the access token from the header
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }
}
