<?php

function generateUnique(&$row) : string {
    ob_start();

    global $connexion;

    $idSub = 0;
    $pubCom = NULL;

    $year = explode("-", $row['dateShit']);
    $month = getTextualMonth($year[1]);

    // Affichage des boutons shitstorm précédente / suivante
    $idPrevious = getIDFromPreviousSS($idSub, $row['dateShit']);
    $idNext = getIDFromNextSS($idSub, $row['dateShit']);
    ?>

    <!-- Boutons suivant/précédent --> 
    <div class='row'>
        <ul class="pagination center hide-on-small-only">
            <li class="<?= ($idPrevious ? 'waves-effect' : 'disabled') ?>"><a href="<?= ($idPrevious ? "unique?idPubli=$idPrevious" : '#!' ) ?>"><i class="material-icons">chevron_left</i></a></li>
            <li style='vertical-align: sub; font-size: 1.3em; color: #7392ea;'><?= htmlentities($row['title']) ?></li>
            <li class="<?= ($idNext ? 'waves-effect' : 'disabled') ?>"><a href="<?= ($idNext ? "unique?idPubli=$idNext" : '#!' ) ?>"><i class="material-icons">chevron_right</i></a></li>
        </ul>
        <h5 class='center hide-on-med-and-up'><?= htmlentities($row['title']) ?></h5>
        <div class='col s12'>
            <ul class="pagination hide-on-med-and-up">
                <li class="left <?= ($idPrevious ? 'waves-effect' : 'disabled') ?>"><a href="<?= ($idPrevious ? "unique?idPubli=$idPrevious" : '#!' ) ?>"><i class="material-icons">chevron_left</i></a></li>
                <li class="right <?= ($idNext ? 'waves-effect' : 'disabled') ?>"><a href="<?= ($idNext ? "unique?idPubli=$idNext" : '#!' ) ?>"><i class="material-icons">chevron_right</i></a></li>
            </ul>
        </div>
    </div>
    <div class='clearb'></div>

    <?php

    // Récup de(s) possible(s) réponse(s)
    $hasAnswer = count($row['answers']);
    $answer = NULL;

    // Récupération du nombre de likes
    $nbLikes = '0';

    // Récupération si l'utilisateur courant a like ou dislike
    $hasLiked = false;
    $hasDisliked = false;

    // ------------
    // PUBLICATION
    // ------------
    ?>
    <div class="row">
        <div class="col s12 l8 offset-l2">
            <div class="card-new-flow">
                <div>
                    <div class='link center'><?php 
                        $i = 0;
                        foreach($row['links'] as $link){
                            viewLink($link, $i);
                            $i++;
                        }
                    ?></div>
                </div>
                <div class='divider'></div>
                <div class="card-content">
                    <!-- Description -->
                    <p style="margin-bottom: 1em;"><?php echo detectTransformLink(htmlentities($row['dscr'])); ?>

                    <!-- Boutons like/dislike -->
                    <div style="margin-bottom: 1em;">
                        <span class='likeButtons'>
                            <button class="btn-floating orange darken-5 tooltipped" data-position="bottom" data-delay="50" data-tooltip="Note" 
                                style="float: left; margin-bottom: 1em;" id='likecount<?= $idSub ?>' onclick="generateViewRateModal('modalplaceholder', <?= $idSub ?>);"><?= $nbLikes ?>
                            </button> 
                        </span> 
                    </div>
                    </p>

                    <div class='divider clearb'></div>

                    <!-- Informations de la shitstorm -->
                    <div style='margin-bottom: 0;'>
                        <p style="float: left;">Shitstorm du <?php echo formatDate($row['dateShit'], 0); ?></p>
                        <p style="font-style: italic; float: right;">Ajoutée par <?php 
                            if($row['idUsr'] > 0){
                                echo "<a class='notCard' href='profil?userid={$row['idUsr']}'>{$row['pseudo']}</a> le ". formatDate($row['approvalDate'], 1);
                            }
                            else{
                                echo "{$row['pseudo']} le ". formatDate($row['approvalDate'], 1);
                            } 
                        ?> </p>
                    </div>
                    <div class='clearb'></div>
                </div>
            </div>

            <!-- Réponses -->
            <?php if($hasAnswer){
                // REPONSE DU CREATEUR 
                genereReponses(null, $row['answers']);
            } ?>
        </div>  
    </div>
    <?php


    $data = ob_get_contents();
    ob_end_clean();

    return $data;
}

function generateUniquePage(&$row) : string {
    $page = generateUnique($row);

    $name = '';
    $md = md5($page);
    $name = "/var/www/html/img/preview/$md.html";

    file_put_contents($name, $page);

    return $md;
}
