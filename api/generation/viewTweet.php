<?php

require($_SERVER['DOCUMENT_ROOT'] .'/inc/constantes.php');
require($_SERVER['DOCUMENT_ROOT'] .'/inc/traitements.php');
require($_SERVER['DOCUMENT_ROOT'] .'/inc/TwitterBot.php');
require($_SERVER['DOCUMENT_ROOT'] .'/inc/fonctions.php');
require($_SERVER['DOCUMENT_ROOT'] .'/inc/generated.php');
require($_SERVER['DOCUMENT_ROOT'] .'/inc/postDelete.php');

$count = 20;
if(isset($_GET['count']) && is_numeric($_GET['count']) && $_GET['count'] > 0){
    $count = $_GET['count'];
}

if(isset($_GET['id'])){
    $twitterb = new TwitterBot(CONSUMER_KEY, CONSUMER_SECRET);
    $twitterb->setToken(OAUTH_KEY, OAUTH_SECRET);

    $t = $twitterb->getSingleTweet($_GET['id']);
    
    if($t){
        header('HTTP/1.1 200 OK');
        header('Content-Type: application/json');
        echo $t;
    }
    else var_dump($t);
}
elseif(isset($_GET['user_id'])){
    $twitterb = new TwitterBot(CONSUMER_KEY, CONSUMER_SECRET);
    $twitterb->setToken(OAUTH_KEY, OAUTH_SECRET);

    $t = $twitterb->getUserTime($_GET['user_id'], false, $count);
    
    if($t){
        header('HTTP/1.1 200 OK');
        header('Content-Type: application/json');
        echo $t;
    }
    else var_dump($t);
}
elseif(isset($_GET['user_name'])){
    $twitterb = new TwitterBot(CONSUMER_KEY, CONSUMER_SECRET);
    $twitterb->setToken(OAUTH_KEY, OAUTH_SECRET);

    $t = $twitterb->getUserTime($_GET['user_name'], true, $count);
    
    if($t){
        header('HTTP/1.1 200 OK');
        header('Content-Type: application/json');
        echo $t;
    }
    else var_dump($t);
}