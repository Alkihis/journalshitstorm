<?php

global $connexion;

session_start();

$row = [];
$row['links'] = [];
$link = &$row['links'];
$dscr = &$row['dscr'];
$row['dateShit'] = &$row['approvalDate'];
$date = &$row['dateShit'];
$title = &$row['title'];
$row['idSub'] = 0;
$psu = &$row['pseudo'];
$idUsr = &$row['idUsr'];

(!isset($_POST['link']) || empty($_POST['link']) ? '' : $link[] = trim(mysqli_real_escape_string($connexion, $_POST['link'])));
(!isset($_POST['link2']) || empty($_POST['link2']) ? '' : $link[] = trim(mysqli_real_escape_string($connexion, $_POST['link2'])));
(!isset($_POST['link3']) || empty($_POST['link3']) ? '' : $link[] = trim(mysqli_real_escape_string($connexion, $_POST['link3'])));
(!isset($_POST['link4']) || empty($_POST['link4']) ? '' : $link[] = trim(mysqli_real_escape_string($connexion, $_POST['link4'])));
(!isset($_POST['dscr']) || empty($_POST['dscr']) ? $dscr = 'Ceci est une description vide.' : $dscr = trim(mysqli_real_escape_string($connexion, html_entity_decode($_POST['dscr']))));
(!isset($_POST['title']) || empty($_POST['title']) ? $title = 'Titre vide' : $title = trim(mysqli_real_escape_string($connexion, html_entity_decode($_POST['title']))));
$psu = "";
$idUsr = 0;
(isset($_POST['date_real']) ? $date = $_POST['date_real'] : $date = '1970-01-01');
$pass = false;

$dateApp = date('Y-m-d H:i:s');
try{
    $dateT = new DateTime($date);
    $actual = new DateTime($dateApp);

    if($dateT <= $actual){
        $pass = true;
    }
    else{
        echo json_encode(['error_text' => "La date est invalide."]);
        header('HTTP/1.1 400 Bad Request'); exit();
    }
} catch (Exception $E) { 
    echo json_encode(['error_text' => "La date est invalide."]);
    header('HTTP/1.1 400 Bad Request'); exit();
}
if(mb_strlen($title) > 50){
    $pass = false;
    echo json_encode(['error_text' => "Le titre est trop long."]);
    header('HTTP/1.1 400 Bad Request'); exit();
}
if(mb_strlen($dscr) > 2000){
    $pass = false;
    echo json_encode(['error_text' => "La description est trop longue."]);
    header('HTTP/1.1 400 Bad Request');
    exit();
}

foreach($link as $key => $l){
    if($l != ''){
        if(!preg_match(REGEX_TWITTER_LINK, $l) && !preg_match(REGEX_MASTODON_LINK, $l)){
            header('HTTP/1.1 400 Bad Request');
            echo json_encode(['error_text' => "Le lien ". ($key+1) ." est invalide"]);
            exit();
        }
    }
    else unset($link[$key]);
}
// Les liens valides ne valent désormais pas NULL, ceux vides oui

// Traitement du possible fichier uniquement si il n'y a aucun lien
$nom = 1;
if(isset($_FILES['images'], $_FILES['images']['name'][0]) && $pass && !empty($_FILES['images']['name'][0])){
    for($i = 0; $i < count($_FILES['images']['name']) && $i < 5; $i++){
        $nom = traitementFichier('', 'preview', true, 
                                ['name' => $_FILES['images']['name'][$i], 'type' => $_FILES['images']['type'][$i],
                                 'tmp_name' => $_FILES['images']['tmp_name'][$i], 'size' => $_FILES['images']['size'][$i],
                                 'error' => $_FILES['images']['error'][$i]]);
        if($nom){
            $link[] = $nom;
        }
        else{
            header('HTTP/1.1 400 Bad Request');
            echo json_encode(['error_text' => "Le fichier est invalide"]);
            $pass = false;
        }
    }
    
}
// Vérification si au moins un lien est valide, ou alors une image est valide
if($pass){ // Si tout est déjà bon
    $pass = (!empty($link) && count($link) < 6);
    if(!$pass){
        header('HTTP/1.1 400 Bad Request');
        echo json_encode(['error_text' => "Aucun lien valide n'a été précisé, ou le nombre d'images + liens cumulés dépasse 5"]);
    }
}

$answers = [];
$valid_answers = [];
$row['answers'] = &$valid_answers;
if(isset($_SESSION['connected'], $_SESSION['id']) && $_SESSION['connected']){
    // Ajout des réponses
    if(isset($_POST['linkA1'], $_POST['dateA1_real'], $_POST['dscrA1'])){
        addAnswer($answers, $_POST['dateA1_real'], $_POST['dscrA1'], $_POST['linkA1'], 'imgA1', $_SESSION['id']);
    }
    if(isset($_POST['linkA2'], $_POST['dateA2_real'], $_POST['dscrA2'])){
        addAnswer($answers, $_POST['dateA2_real'], $_POST['dscrA2'], $_POST['linkA2'], 'imgA2', $_SESSION['id']);
    }
    if(isset($_POST['linkA3'], $_POST['dateA3_real'], $_POST['dscrA3'])){
        addAnswer($answers, $_POST['dateA3_real'], $_POST['dscrA3'], $_POST['linkA3'], 'imgA3', $_SESSION['id']);
    }
}

foreach($answers as $key => $an){
    $check = verifyAnswerValidy($date, $an['date'], $an['dscr'], $an['link'], $an['filename'], $key);

    if($check !== 0){
        $pass = false;
        break;
    }
    else{ // Réponse valide, on remplit valid answers
        $nomA = $an['link'];
        if($an['link'] == ''){
            // Traitement du possible fichier
            $nomA = 1;
            if(isset($_FILES[$an['filename']]) && $pass && !empty($_FILES[$an['filename']]['name'])){
                $nomA = traitementFichier($an['filename'], 'preview', true);
            }
            if(!$nomA || $nomA === 1){
                header('HTTP/1.1 400 Bad Request');
                echo json_encode(['error_text' => "Le fichier est invalide pour la réponse $key"]);
                $pass = false;
            }
            // Si aucun fichier, $nomA vaut 1 (null si fichier pas valide)
        }
        // nomA vaut désormais soit le lien, soit le lien vers l'image

        $valid_answers[] = ['linkA' => $nomA, 'dscrA' => $an['dscr'], 'dateAn' => $an['date'], 'idUsrA' => $an['idUsr']];
    }
}

if($pass && $nom){
    if(isset($_POST['pseudo'])){
        $psu = mysqli_real_escape_string($connexion, htmlspecialchars($_POST['pseudo']));
    }

    if(isset($_SESSION['connected']) && $_SESSION['connected']){
        $psu = $_SESSION['username'];
        $idUsr = $_SESSION['id'];
    }

    ob_start();
    generateShitstormCardFromMysqliRow($row, NULL, true);

    $content = ob_get_contents();
    ob_end_clean();

    include(ROOT .'/api/generation/preview_unique.php');

    echo json_encode(['card' => $content, 'uniqueID' => generateUniquePage($row)]);
}
else {
    foreach($link as $nom){
        deleteFile($nom, 'preview');
    }

    foreach($valid_answers as $an){
        deleteFile($an['linkA']);
    }
}

