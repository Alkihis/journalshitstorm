$(document).ready(function(){
    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 4, // Creates a dropdown to control year
        today: 'Aujourd\'hui',
        clear: 'Réinitialiser',
        close: 'Confirmer',
        closeOnSelect: false, // Close upon selecting a date
        // Strings and translations
        monthsFull: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        monthsShort: ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Jun', 'Jul', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec'],
        weekdaysFull: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
        weekdaysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
        labelMonthNext: 'Mois suivant',
        labelMonthPrev: 'Mois précédent',
        labelMonthSelect: 'Mois',
        labelYearSelect: 'Année',
        // Formats
        format: 'dddd dd mmmm yyyy',
        formatSubmit: 'yyyy-mm-dd',
        hiddenSuffix: '_real',
    
        min: new Date(2010, 0, 01),
        max: Date.now(),
      });
});

function changeMinDate(newYear, newMonth, newDay, classe = 'datepicker'){
    newMonth = parseInt(newMonth);
    $(document).ready(() => {
        var $input = $('.'+classe).pickadate();
        var picker = $input.pickadate('picker');
        if(picker) {
            picker.set('min', new Date(newYear, newMonth-1, newDay));
        }
    });
}