function loadOrphanTweets(maxid = 0) {
    let main_placeholder = document.getElementById('main_tweet_ph');
    let pre = document.createElement('div');
    pre.innerHTML = preloader;
    pre.id = 'preloader_tweets';
    main_placeholder.appendChild(pre);

    $.get(
        https_url+"api/v2/saves/orphans.json",
        {max_id: maxid, html: true, count: 5},
        function(data) {
            let json = JSON.parse(data);

            let preloader_tweet = document.getElementById('preloader_tweets');
            if(preloader_tweet) {
                preloader_tweet.parentElement.removeChild(preloader_tweet);
            }

            let old_more = document.getElementById('more_tweets');
            if(old_more) {
                old_more.parentElement.removeChild(old_more);
            }

            if(json.count) {
                json.tweets.forEach(function (tweet) {
                    let d = document.createElement('div');
                    d.innerHTML = tweet.html;
                    main_placeholder.appendChild(d);
                });

                if(json.next_max_id) {
                    let new_more = document.createElement('div');
                    new_more.innerHTML = `<div class='flow-text black-text center'><a class='card-pointer'>Charger plus</a></div>`;
                    new_more.onclick = function () {
                        loadOrphanTweets(json.next_max_id);
                    };
                    new_more.id = 'more_tweets';

                    main_placeholder.appendChild(new_more);
                }
            }

            initDeleteTweetButton();
        },
        "text"
    ) .fail(function(jqXHR) {
        let preloader_tweet = document.getElementById('preloader_tweets');
        if(preloader_tweet) {
            preloader_tweet.parentElement.removeChild(preloader_tweet);
        }

        if(maxid === 0) { // C'est le chargement initial
            let old_more = document.getElementById('more_tweets');
            if(old_more) {
                old_more.parentElement.removeChild(old_more);
            }

            let new_more = document.createElement('div');
            new_more.innerHTML = `<div class='flow-text black-text center'><a class='card-pointer'>Réessayer</a></div>`;
            new_more.onclick = function () {
                loadOrphanTweets();
            };
            new_more.id = 'more_tweets';

            main_placeholder.appendChild(new_more);
        }

        if(jqXHR.status === 429){
            Materialize.toast('Vous avez dépassé le quota de chargement de tweets. Réessayez ultérieurement.', 10000, 'rounded');
        }
        else {
            Materialize.toast('<span class="error bold">Erreur réseau</span>', 6000, 'rounded');
        }
    });
}

function initDeleteTweetButton() {
    let $but = $('.delete-save-button');
    $but.off();

    $but.on('click', function () {
        let id = this.dataset.id;
        if(id) {
            deleteById(id, this.parentElement.parentElement.parentElement);
        }
    });
}

function deleteById(idTweet, docFrag) {
    $.post(
        https_url+"api/v2/saves/delete.json",
        {id: idTweet},
        function(data) {
            let json = JSON.parse(data);

            if(json.deleted) {
                Materialize.toast('Suppression effectuée avec succès.', 6000, 'rounded');
            }
            else {
                Materialize.toast('Suppression non effectué. Il a sans doute été associé à une shitstorm entre temps.', 6000, 'rounded');
            }

            docFrag.parentElement.removeChild(docFrag);
        },
        "text"
    ) .fail(function(jqXHR) {
        if(jqXHR.status === 429){
            Materialize.toast('Vous avez dépassé le quota de suppression de tweets. Réessayez ultérieurement.', 10000, 'rounded');
        }
        else {
            Materialize.toast('Une erreur est survenue. Réessayez ultérieurement.', 6000, 'rounded');
        }
    });
}

$(document).ready(function () {
    loadOrphanTweets();
});
