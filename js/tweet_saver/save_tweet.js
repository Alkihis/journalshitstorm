// Tweet saver JavaScript functions

function saveAndGetTweet(id) {
    $.post(
        https_url+"api/v2/saves/create.json",
        {id_tweet: id},
        function(){
            Materialize.toast('Le tweet a été sauvegardé.', 6000, 'rounded');
            // Le tweet a été sauvegardé, on charge la sauvegarde
            getSavedTweet(id, 'placeholdertweet');
        },
        "text"
    ) .fail(function(jqXHR) {
            let json = JSON.parse(jqXHR.responseText);

            if(("error_code" in json && json.error_code === 26) || jqXHR.status === 429){
                Materialize.toast('Vous avez dépassé le quota d\'envoi de tweets. Réessayez ultérieurement.', 10000, 'rounded');
            }
            else {
                Materialize.toast('Une erreur est survenue. Réessayez ultérieurement.', 6000, 'rounded');
            }
        });
}

$(document).ready(function () {
    $('.btn-save-tweet').on('click', function () {
        let link = document.getElementById('tweetID').value;

        if(link.match(/twitter\.com\//)) {
            link = link.replace(/(https?:\/\/)?twitter\.com\/.+\/status\/([0-9]+)(\?.+)?/, '$2');
        }
        
        console.log(link);
        if (link.match(/^[0-9]+$/)) {
            // L'ID est valide
            saveAndGetTweet(link);
        }
        else {
            Materialize.toast('Le format de l\'identifiant ou du lien est invalide.', 6000, 'rounded');
        }
    });
});
