function createDeleteCommentModal(idModal, idCom, token, count){
    document.getElementById(idModal).innerHTML = `
        <div class='modal-content'>
            <h4>Voulez-vous vraiment supprimer ce commentaire ?</h4>
        </div>
        <div class='modal-footer' style='padding-left: 1.5em;'>
            <a href='${https_url}compte/mysubs/comments?id=${idCom}&del=${token}&count=${count}' class='modal-action modal-close waves-effect waves-green btn-flat col s3 l1 red lighten-1 left white-text'>
                <i class='material-icons left'>clear</i>Oui
            </a>
            <a href='#!' class='modal-action modal-close waves-effect waves-green btn-flat col s3 l1 blue lighten-1 left white-text' style='margin-left : 2em;'>
                <i class='material-icons left'>keyboard_capslock</i>Non
            </a>
            <div class='clearb'></div>
        </div>`;
    $('#'+idModal).modal('open');
}

function createDeleteCommentModalUniqueS(idModal, idCom){
    document.getElementById(idModal).innerHTML = `
        <div class='modal-content'>
            <h4>Voulez-vous vraiment supprimer ce commentaire ?</h4>
        </div>
        <div class='modal-footer' style='padding-left: 1.5em;'>
            <button id='confirmDeletion' type='submit' class='modal-action modal-close waves-effect waves-green btn-flat col s3 l1 red lighten-1 left white-text'>
                <i class='material-icons left'>clear</i>Oui
            </button>
            <a href='#!' class='modal-action modal-close waves-effect waves-green btn-flat col s3 l1 blue lighten-1 left white-text' style='margin-left : 2em;'>
                <i class='material-icons left'>keyboard_capslock</i>Non
            </a>
            <div class='clearb'></div>
        </div>`;
    $('#'+idModal).modal('open');

    document.getElementById('confirmDeletion').onclick = () => {
        $.post(
            https_url+"api/v2/comment/destroy.json",
            {id: idCom},
            function(data){
                var json = JSON.parse(data);
                
                if('deleted' in json && json.deleted){
                    $('#commentTotal'+idCom).hide(220, function() { $(this).remove(); });

                    // -1 au compteur de commentaire
                    let comment_counter = document.getElementById('comment_tab_trigger');
                    let countCom = Number(comment_counter.dataset.commentCount);
                    countCom -= 1;
                    if(countCom < 0){
                        countCom = 0;
                    }
                    comment_counter.dataset.commentCount = countCom;
                    comment_counter.innerText = `Commentaires (${countCom})`;
                }
                else{
                    Materialize.toast('La suppression de votre commentaire a échoué.', 6000, 'rounded');
                }
            },
            "text"
        )
        .fail((answer, text, other) => {
            Materialize.toast('La suppression de votre commentaire a échoué.', 6000, 'rounded');
            console.log(answer, text, other);
        });
    };
}

function createModalStatusUser(idModal, idUsr, token, newStatus){
    let str = `<div class='modal-content'><h4>`;
    let param = '';
    let value = '0';
    let is_good = false;

    if(newStatus == 0){ // Dégrader
        str += `Voulez-vous vraiment rétrograder cet utilisateur ?</h4></div>`;

        param = 'degrade';
    }
    else if(newStatus == 1){ // Elever
        str += `Voulez-vous vraiment élever cet utilisateur ?</h4></div>`;

        param = 'grantM';
        is_good = true;
    }
    else if(newStatus == 2){ // Elever admin
        str += `Voulez-vous vraiment élever ce modérateur en tant qu'administrateur ?</h4></div>`;

        param = 'grantA';
        is_good = true;
    }
    else if(newStatus == -1){ // Bannir
        str += `Voulez-vous vraiment bannir cet utilisateur ?</h4></div>`;

        param = 'ban';
    }
    
    document.getElementById(idModal).innerHTML = str + `
        <div class='modal-footer' style='padding-left: 1.5em;'>
            <a href='${https_url}usermgmt?${param}=${value}&idUsr=${idUsr}&sure=${token}' class='modal-action modal-close waves-effect waves-green btn-flat col s3 l1 `
            + (is_good ? 'green lighten-2' : `red lighten-1`) + `left white-text'>
                <i class='material-icons left'>` + (is_good ? 'check' : `clear`) + `</i>Oui
            </a>
            <a href='#!' class='modal-action modal-close waves-effect waves-green btn-flat col s3 l1 blue lighten-1 left white-text' style='margin-left : 2em;'>
                <i class='material-icons left'>keyboard_capslock</i>Non
            </a>
            <div class='clearb'></div>
        </div>`;
    $('#'+idModal).modal('open');
}

function generateViewFavModal(idModal, idCom, idButton = ''){
    var modal = document.getElementById(idModal);

    var $modal = $(modal);

    if(modal.innerHTML){ // Si le modal n'est pas vide
        var new_modal = modal.cloneNode(true);
        new_modal.id = modal.id+random_id();

        new_modal.innerHTML = '';

        // Insertion du nouveau noeud
        modal.parentElement.appendChild(new_modal);

        var z = 1006;
        if($modal.css('z-index')){
            z = parseInt($('#'+idModal).css('z-index')) + 1;
        }

        modal = document.getElementById(new_modal.id);
        modal.style.zIndex = z;

        idModal = new_modal.id;

        $modal = $('#' + idModal);

        $modal.modal({complete: function() {  // Supprime le modal du DOM à la fermeture
            var actualnode = document.getElementById(idModal);
            var parent = actualnode.parentElement;
            parent.removeChild(actualnode);
        }});
    }

    $modal.modal('open');

    var modalstr = '<div class="modal-content" id="modal-content-comment">';
    modalstr += preloader;
    modalstr += `</div>
                <div class="modal-footer">
                        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fermer</a>
                </div>`;
    modal.innerHTML = modalstr;
    
    getNextCommentFavers(document.getElementById('modal-content-comment'), idCom, 0, idButton, true);
}

function getNextCommentFavers(documentFragment, idCom, idNext, idButton = '', initial = false) {
    let more_box = document.getElementById('modalNextMaxId');
    if(more_box) {
        var previous_text = more_box.innerHTML;
        more_box.innerHTML = "<a href='#!'>Chargement...</a>";
    }
    
    $.get(
        https_url+"api/v2/rate/fetch/comment.json",
        {id: idCom, mode: 'extended', max_id: idNext, count: 10},
        function(data){
            var json = JSON.parse(data);

            var modalstr = '';

            if("favorites" in json && "rate" in json) {
                var count = json.rate;
                // On en profite pour actualiser le nombre de favs
                document.getElementById("nb"+idCom+idButton).innerHTML = count;
    
                if(initial) {
                    // Si c'est le chargement initial, on remplit le modal de texte
                    if(count > 0){
                        if(count > 1){
                            modalstr += '<h5>Personnes ayant mis en favori le commentaire</h5>';
                        }
                        else {
                            modalstr += '<h5>Personne ayant mis en favori le commentaire</h5>';
                        }
    
                        modalstr += '<div class="comment-likers" id="comment-likers'+ idCom +'">';

                        json.favorites.forEach(function(element) {
                            modalstr += `
                            <div class="chip">
                                <img src="${element.user.full_profile_img}" alt="Favori">
                                <a target='_blank' href='${https_url}profil/${element.user.id}' class='modalFav'>${element.user.username}</a>
                            </div>`;
                        });

                        // Si il y a un next_max_id, alors on ajoute un petit bouton plus
                        if(json.next_max_id) {
                            modalstr += `
                            <div class="chip" id='modalNextMaxId'>
                                <a href='#!'
                                onclick="getNextCommentFavers(document.getElementById('comment-likers${idCom}'), ${idCom}, ${json.next_max_id});">
                                +</a>
                            </div>`;
                        }
                        modalstr += '</div>';
                    }
                    else {
                        modalstr += '<h5 class="empty">Ce commentaire n\'a aucun favori.</h5>';   
                    }

                    documentFragment.innerHTML = modalstr;
                }
                else {
                    // Sinon, on ajoute juste des chips en plus
                    json.favorites.forEach(function(element) {
                        modalstr += `
                        <div class="chip">
                            <img src="${element.user.full_profile_img}" alt="Favori">
                            <a target='_blank' href='${https_url}profil/${element.user.id}' class='modalFav'>${element.user.username}</a>
                        </div>`;
                    });

                    // On efface l'ancienne croix
                    $('#modalNextMaxId').remove();

                    // Si il y a un next_max_id, alors on ajoute un petit bouton plus
                    if(json.next_max_id) {
                        modalstr += `
                            <div class="chip" id='modalNextMaxId'>
                                <a href='#!'
                                onclick="getNextCommentFavers(document.getElementById('comment-likers${idCom}'), ${idCom}, ${json.next_max_id});">
                                +</a>
                            </div>`;
                    }

                    documentFragment.innerHTML += modalstr;
                }
            }
        },
        "text")
        .fail(() => {
            if(initial) {
                documentFragment.innerHTML = "<h5 class='error'>Impossible de charger les données.</h5>";
            }
            else {
                Materialize.toast('<span class="bold error">Erreur réseau</span>');
                if(more_box) {
                    more_box.innerHTML = previous_text;
                }
            } 
        });
}

function generateViewRateModal(idModal, idSub){
    var modal = document.getElementById(idModal);

    if(modal.innerHTML){ // Si le modal n'est pas vide
        var new_modal = modal.cloneNode(true);
        new_modal.id = modal.id+random_id();

        new_modal.innerHTML = '';

        // Insertion du nouveau noeud
        modal.parentElement.appendChild(new_modal);

        var z = 1006;
        if($('#'+idModal).css('z-index')){
            z = parseInt($('#'+idModal).css('z-index')) + 1;
        }

        modal = document.getElementById(new_modal.id);
        modal.style.zIndex = z;

        idModal = new_modal.id;

        $('#'+idModal).modal({complete: function() {  // Supprime le modal du DOM à la fermeture
            var actualnode = document.getElementById(idModal);
            var parent = actualnode.parentElement;
            parent.removeChild(actualnode);
        }});
    }

    var modal_id = 'shitstorm-'+ idSub +'-rates';
    var modalstr = '<div class="modal-content" id="'+ modal_id +'">';

    modalstr += preloader + `</div>
                <div class="modal-footer">`;
            
    modalstr += `<a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fermer</a>
            </div>`;

    modal.innerHTML = modalstr;

    modal = document.getElementById(modal_id);

    modalstr = '';

    $('#'+idModal).modal('open');

    $.get(
        https_url+"api/v2/rate/fetch/shitstorm.json",
        {id: idSub, mode: 'extended', count: 20},
        function(data){
            var json = JSON.parse(data);

            var totalcount = json.positive_rates + json.negative_rates;
            let next_max_id = json.next_max_id;

            // Actualisation de la note
            $("#likecount"+idSub).text(json.rate);

            if(totalcount > 0){
                if(json.positive_rates > 0){ // Si il y a des votes positifs
                    modalstr += '<h5 class="upvote">Personnes ayant noté positivement la shitstorm</h5>';
                    modalstr += '<div id="shitstorm-likers'+ idSub + '">';

                    json.likes.forEach(function(element){
                        modalstr += `
                        <div class="chip">
                            <img src="${element.user.full_profile_img}" alt="Vote positif">
                            <a target='_blank' href='${https_url}profil/${element.user.id}' class='modalLike'>${element.user.username}</a>
                        </div>`;
                    });

                    if(next_max_id) {
                        modalstr += `
                        <div class="chip" id='modalPositiveNextMaxId'>
                            <a href='#!' title='Likes suivants'
                            onclick="getNextShitstormRates(document.getElementById('shitstorm-likers${idSub}'), ${idSub}, ${next_max_id}, 'likes');">
                            +</a>
                        </div>`;
                    }

                    modalstr += '</div>';

                    if(json.negative_rates > 0){
                        modalstr += `<div class='clearb'></div>

                        <div class="divider" style='margin-top: .5em;'></div>`;
                    }
                }
                if(json.negative_rates > 0){
                    modalstr += '<h5 class="downvote">Personnes ayant noté négativement la shitstorm</h5>';
                    modalstr += '<div id="shitstorm-dislikers'+ idSub + '">';

                    json.dislikes.forEach(function(element){
                        modalstr += `
                        <div class="chip">
                            <img src="${element.user.full_profile_img}" alt="Vote négatif">
                            <a target='_blank' href='${https_url}profil/${element.user.id}' class='modalDislike'>${element.user.username}</a>
                        </div>`;
                    });

                    if(next_max_id) {
                        modalstr += `
                        <div class="chip" id='modalNegativeNextMaxId'>
                            <a href='#!' title='Dislikes suivants'
                            onclick="getNextShitstormRates(document.getElementById('shitstorm-dislikers${idSub}'), ${idSub}, ${next_max_id}, 'dislikes');">
                            +</a>
                        </div>`;
                    }
                }
            }
            else {
                modalstr += '<h5 class="empty">Cette shitstorm n\'a reçu aucune note.</h5>';
            }

            modal.innerHTML = modalstr;
        },
        "text")
        .fail(() => {
            modal.innerHTML = '<h5 class="error">Impossible de charger les données</h5>';
    });
}

function getNextShitstormRates(documentFragment, idSub, idMax, rateType) {
    let more_box = document.getElementById((rateType === 'likes' ? 'modalPositiveNextMaxId' : 'modalNegativeNextMaxId'));
    if(more_box) {
        var previous_text = more_box.innerHTML;
        more_box.innerHTML = "<a href='#!'>Chargement...</a>";
    }

    $.get(
        https_url+"api/v2/rate/fetch/shitstorm.json",
        {id: idSub, mode: 'extended', max_id: idMax, type: rateType, count: 10},
        function(data){
            let modalstr = '';
            var json = JSON.parse(data);

            let next_max_id = json.next_max_id;

            if(more_box) {
                $(more_box).remove();
            }

            if(json.likes.length > 0){ // Si il y a des votes positifs
                json.likes.forEach(function(element){
                    modalstr += `
                    <div class="chip">
                        <img src="${element.user.full_profile_img}" alt="Vote positif">
                        <a target='_blank' href='${https_url}profil/${element.user.id}' class='modalLike'>${element.user.username}</a>
                    </div>`;
                });

                if(next_max_id) {
                    modalstr += `
                    <div class="chip" id='modalPositiveNextMaxId'>
                        <a href='#!' title='Likes suivants'
                        onclick="getNextShitstormRates(document.getElementById('shitstorm-likers${idSub}'), ${idSub}, ${next_max_id}, 'likes');">
                        +</a>
                    </div>`;
                }
            }
            else if(json.dislikes.length > 0){
                json.dislikes.forEach(function(element){
                    modalstr += `
                    <div class="chip">
                        <img src="${element.user.full_profile_img}" alt="Vote négatif">
                        <a target='_blank' href='${https_url}profil/${element.user.id}' class='modalDislike'>${element.user.username}</a>
                    </div>`;
                });

                if(next_max_id) {
                    modalstr += `
                    <div class="chip" id='modalNegativeNextMaxId'>
                        <a href='#!' title='Dislikes suivants'
                        onclick="getNextShitstormRates(document.getElementById('shitstorm-dislikers${idSub}'), ${idSub}, ${next_max_id}, 'dislikes');">
                        +</a>
                    </div>`;
                }
            }

            documentFragment.innerHTML += modalstr;
        },
        "text")
        .fail(() => {
            Materialize.toast('<span class="bold error">Erreur réseau</span>');
            if(more_box) {
                more_box.innerHTML = previous_text;
            }
        }
    );
}

/**
 * DEPRECATED FUNCTION. WAITING FOR REPLACEMENT
 * @param {*} idModal 
 * @param {*} idUsr 
 * @param {*} is_liked 
 */
function generateLikedDislikedShitstorms(idModal, idUsr, is_liked){
    var modal = document.getElementById(idModal);
    modal.innerHTML = `
    <div class="modal-content">
        <h4 class="center">Shitstorms notées `+(is_liked ? 'positivement' : 'négativement')+`</h4>
        <div id="shitmainblock">
            <div id="shitblock">`+ preloader +
            `</div>
        </div>
    </div>
    <div class="modal-footer">
            <button class="modal-action modal-close waves-effect waves-blue btn-flat">Fermer</button>
    </div>`;
    $('#'+idModal).modal('open');
    getLikedShitstorm(idUsr, 0, is_liked);
}

/**
 * DEPRECATED FUNCTION. WAITING FOR REPLACEMENT
 * @param {*} id_user 
 * @param {*} new_count 
 * @param {*} is_liked 
 * @param {*} removeid 
 */
function getLikedShitstorm(id_user, new_count, is_liked, removeid = false){
    let shitb = document.getElementById('shitblock');
    shitb.innerHTML = preloader;
    $.get(
        https_url+"api/v2/generation/shitstorm.json",
        {idUsr: id_user, skip: new_count, viewLikes: is_liked},
        function(data){
            var json = JSON.parse(data);

            // Suppression de l'ancien bouton si demandé
            if(removeid){
                document.getElementById("shitb"+removeid).removeChild(document.getElementById("buttonshitstorm"+removeid));
            }

            if(json.count > 0){
                var string = '';
                var rand_id = 0;

                json.shitstorms.forEach((value) => {
                    string += value.html;
                    rand_id = value.id;
                });

                if(json.next_skip > 0){
                    string += "<button style='margin-top: 1em; margin-bottom: 1em;' id='buttonshitstorm"+ (rand_id) +"' class='btn col s10 offset-s1 teal darken-1'>Shitstorms suivantes</button><div class='clearb'></div>";
                }

                shitb.innerHTML = string;

                let $rates = $('.shitstorm-rates-button');
                $rates.unbind('click');

                $rates.on('click', function () {
                    generateViewRateModal('modalphfixed', this.dataset.shitstormId);
                });

                let $like = $('.like-button');
                $like.unbind('click');

                $like.on('click', function () {
                    likeDislike(this.dataset.shitstormId, true);
                });

                let $dlike = $('.dislike-button');
                $dlike.unbind('click');

                $dlike.on('click', function () {
                    likeDislike(this.dataset.shitstormId, false);
                });

                if(json.next_skip > 0){
                    document.getElementById(`buttonshitstorm${rand_id}`).onclick = () => {
                        getLikedShitstorm(id_user, json.next_skip, is_liked, rand_id);
                    };
                }

                $("#shitblock").find("script").each(function(){
                    eval($(this).text());
                }); // Lecture des scripts
                showTwitterLinks();

                // Modification de l'ID de l'élément shitblock
                shitb.id = "shitb"+rand_id;

                var g = document.createElement('div');
                g.setAttribute("id", "shitblock");

                document.getElementById('shitmainblock').appendChild(g);

                initFancyBox();
                $('.tooltipped').tooltip();
                initCommentTextAera();
            }
            else{
                shitb.innerHTML = `<div class='center' style='margin-bottom: 1.5em;'>Cet utilisateur n'a laissé aucune note `+(is_liked ? 'posi' : 'néga')+`tive.</div>`;
            }
        },
        "text"
    )
    .fail(() => {
        shitb.innerHTML = `<h5 class='error center' style='margin-bottom: 1.5em;'>Impossible de charger les shitstorms.</h5>`;
    });
}
