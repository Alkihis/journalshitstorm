function getTextByType(element){
    let type = element.notification_type;
    
    if(type == 'MENTION_COMMENT'){
        return `<a href='${https_url}profil/${element.id_emitter}' class='notification-bold-text'>${element.user.realname}</a> vous a mentionné dans un commentaire à propos de la shitstorm
        <a href='${https_url}shitstorm/${element.comment.id_shitstorm}#comments_b' class='notification-bold-text'>${element.shitstorm.title}</a>.`;
    }
    else if(type == 'LIKE_COMMENT'){
        return `<a href='${https_url}profil/${element.id_emitter}' class='notification-bold-text'>${element.user.realname}</a> a mis en favori votre commentaire à propos de la shitstorm
        <a href='${https_url}shitstorm/${element.comment.id_shitstorm}#comments_b' class='notification-bold-text'>${element.shitstorm.title}</a>.`;
    }
    else if(type == 'LIKE_SHITSTORM'){
        return `<a href='${https_url}profil/${element.id_emitter}' class='notification-bold-text'>${element.user.realname}</a> a noté positivement votre shitstorm
        <a href='${https_url}shitstorm/${element.shitstorm.id}' class='notification-bold-text'>${element.shitstorm.title}</a>.`;
    }
    else if(type == 'DISLIKE_SHITSTORM'){
        return `<a href='${https_url}profil/${element.id_emitter}' class='notification-bold-text'>${element.user.realname}</a> a noté négativement votre shitstorm
        <a href='${https_url}shitstorm/${element.shitstorm.id}' class='notification-bold-text'>${element.shitstorm.title}</a>.`;
    }
    else if(type == 'SHITSTORM'){
        return `<a href='${https_url}profil/${element.id_emitter}' class='notification-bold-text'>${element.user.realname}</a> a posté une nouvelle shitstorm !
        (<a href='${https_url}shitstorm/${element.shitstorm.id}' class='notification-bold-text'>${element.shitstorm.title}</a>)`;
    }
    else if(type == 'CONFIRM_SHITSTORM'){
        return `Votre shitstorm <a href='${https_url}shitstorm/${element.shitstorm.id}' class='notification-bold-text'>${element.shitstorm.title}</a>
        a été validée par <a href='${https_url}profil/${element.id_emitter}' class='notification-bold-text'>${element.user.realname}</a> !`;
    }
    else if(type == 'FOLLOW'){
        return `<a href='${https_url}profil/${element.id_emitter}' class='notification-bold-text'>${element.user.realname}</a> vous a suivi.`;
    }
    else if(type == 'POST_COMMENT'){
        return `<a href='${https_url}profil/${element.id_emitter}' class='notification-bold-text'>${element.user.realname}</a> a posté un commentaire à propos d'une shitstorm
        (<a href='${https_url}shitstorm/${element.comment.id_shitstorm}#comments_b' class='notification-bold-text'>${element.shitstorm.title}</a>) que vous suivez.`;
    }
    else if(type == 'POST_ANSWER'){
        return `<a href='${https_url}profil/${element.id_emitter}' class='notification-bold-text'>${element.user.realname}</a> a posté une réponse sur une shitstorm
        (<a href='${https_url}shitstorm/${element.answer.id_shitstorm}#answer_b' class='notification-bold-text'>${element.shitstorm.title}</a>) que vous suivez.`;
    }
    else if(type == 'CONFIRM_ANSWER'){
        return `Votre réponse sous la shitstorm <a href='${https_url}shitstorm/${element.answer.id_shitstorm}#answer_b' class='notification-bold-text'>${element.shitstorm.title}</a>
        a été validée par <a href='${https_url}profil/${element.id_emitter}' class='notification-bold-text'>${element.user.realname}</a> !`;
    }
    else if(type == 'MODERATION_ANSWER'){
        return `Une nouvelle réponse pour la shitstorm <a href='${https_url}shitstorm/${element.answer.id_shitstorm}#answer_b' class='notification-bold-text'>${element.answer.title}</a>
        postée par <a href='${https_url}profil/${element.id_emitter}' class='notification-bold-text'>${element.user.realname}</a> est disponible pour modération.`;
    }
    else if(type == 'MODERATION_SHITSTORM'){
        return `Une nouvelle shitstorm (<a href='${https_url}shitstorm/${element.shitstorm.id}' class='notification-bold-text'>${element.shitstorm.title}</a>)
        postée par <a href='${https_url}profil/${element.id_emitter}' class='notification-bold-text'>${element.user.realname}</a> est disponible pour modération.`;
    }
    else{
        return 'unknown notification type (' + type + ')';
    }
}

const NOTIF_BUTTON = "<button style='margin-top: 1em; margin-bottom: 1em;' class='btn col s10 offset-s1 teal darken-1 notif-next-button waves-effect waves-light btn-personal-mini'>Notifications suivantes</button>";

function loadNotifications(ignoreButton = false, sinceId = 0, maxId = 0, setRead = true, showAll = true){ // Return json of notifications
    var mod = document.getElementById('notbcurrent');
    mod.innerHTML = preloader;
    
    $.get(
        https_url+"api/v2/activity/read.json",
        {since_id: sinceId, max_id: maxId, untouch_read_status: !setRead, show_old: showAll, count: 10},
        function(data){
            var json = JSON.parse(data);

            // Suppression de l'ancien bouton si demandé
            $but = $('.notif-next-button');
            if($but){
                $but.remove();
            }

            let string = '';
            let rand_id = random_id();

            if("count" in json && json.count > 0){
                document.getElementById('notifMenuButtonMobile').innerHTML = `<i class='material-icons black-text'>notifications_none</i>Notifications (0)</a>`;
                if(!ignoreButton){
                    document.getElementById('notifMenuButton').dataset.tooltip = 'Aucune nouvelle notification';
                    document.getElementById('notifMenuButtonContent').innerHTML = 'notifications_none';
                    $('#notifMenuButtonContent').removeClass('red-text');
                    $('#notifMenuButtonContent').addClass('white-text');
                }

                // Prochain maxID a appeler
                maxId = json.next_cursor_str;

                string += '<div class="col s12">';
                for(let i = 0; i < json.count; i++) {
                    let inside = '';
                    let link = '';

                    let type = getNotifType(json.notifications[i].notification_type);

                    if(json.notifications[i].deleted){
                        inside = 'Cette notification fait référence à un' + (type ? (type == 3 ? 'e réponse' : ' commentaire') : 'e shitstorm') + 
                            ' supprimé' + (type == 1 ? '' : 'e') + '. Vous pouvez supprimer cette notification.';
                    }
                    else{
                        inside += getTextByType(json.notifications[i]);
                        link = getLinkFromNotification(json.notifications[i]);
                    }

                    let profile_img = json.notifications[i].user != null ? json.notifications[i].user.full_profile_img : '';

                    let date = getRealDate(json.notifications[i].date);
                    string += `<div class="card notif-color card-border `+(json.notifications[i].already_seen ? 'seen' : '')+`" id='notification${json.notifications[i].id_notification_str}'>
                            <div class="card-content notification-card">
                                <span class='col m2 hide-on-small-only'>
                                    <a href='${https_url}profil/${json.notifications[i].id_emitter}'><img id='profile_img' class='circle card-pointer valign-wrapper mauto'
                                    width="55" height='55' src="${profile_img}"></a>
                                </span>
                                <div class='col s12 m10'>
                                    <div onclick='location.href="${link}";' class='col s11 valign-wrapper notification-text'><p>${inside}</p></div>
                                    <div class='right-close hide-on-med-and-up'><i class='material-icons black-text card-pointer' id='del${json.notifications[i].id_notification_str}'>close</i></div>
                                    <div class='col s1 hide-on-small-only'><i class='material-icons right black-text card-pointer' id='delS${json.notifications[i].id_notification_str}'>close</i></div>
                                    <div class='col s12 right-align black-text'>${date}</div>
                                </div>
                                <div class='clearb'></div>
                            </div>
                        </div>`;
                }
                string += '</div>';
                if(maxId != '0'){ // Si il y a un next cursor
                    string += NOTIF_BUTTON + "<div class='clearb'></div>";
                }

                // Ajout dans l'HTML
                mod.innerHTML = string;

                // Ajout des onclicks
                for(let i = 0; i < json.count; i++){
                    document.getElementById('del' + json.notifications[i].id_notification_str).onclick = () => {
                        deleteNotification(json.notifications[i].id_notification_str, 'notification' + json.notifications[i].id_notification_str);
                    }
                    document.getElementById('delS' + json.notifications[i].id_notification_str).onclick = () => {
                        deleteNotification(json.notifications[i].id_notification_str, 'notification' + json.notifications[i].id_notification_str);
                    }
                }

                mod.id = "notb"+rand_id;

                let g = document.createElement('div');
                g.setAttribute("id", "notbcurrent");

                document.getElementById('notifblock').appendChild(g);

                initFancyBox();
                $('.tooltipped').tooltip();

                if(maxId != '0'){
                    $but = $('.notif-next-button');
                    if($but) {
                        $but.on('click', function() {
                            loadNotifications(ignoreButton, 0, maxId, true, true);
                        });
                    }
                }
            }
            else {
                mod.innerHTML = "<h5 class='center'>Aucune notification.</h5>";
            }
        },
        "text"
    )
    .fail(() => {
        mod.innerHTML += `<h5 class='error center' style='margin-bottom: 1.5em;'>Impossible d'obtenir les notifications.</h5>`;
    });
}

// Notification modal
function initNotificationModal(){
    document.getElementById(fixed_modal).innerHTML = `
    <div class="modal-content">
        <h4 class="center">Notifications</h4>
        <div id="notifblock">
            <div id="notbcurrent">`+ preloader +
            `</div>
        </div>
    </div>
    <div class="modal-footer">
            <button class="modal-action modal-close waves-effect waves-blue btn-flat">Fermer</button>
    </div>`;
    $('#'+fixed_modal).modal('open');
    loadNotifications();
}

function deleteNotification(idNotif, notificationDocumentElementID){
    $('#' + notificationDocumentElementID).hide(220, () => {  
        $.post(
            https_url+"api/v2/activity/destroy.json",
            {id: idNotif},
            function(data){
                var json = JSON.parse(data);

                if("status" in json && json.status == 'deleted'){
                    $('#'+notificationDocumentElementID).remove();
                }
                else{
                    $('#'+notificationDocumentElementID).show();
                }
            },
            "text"
        )
        .fail(() => {
            $('#'+notificationDocumentElementID).show();
        });
    });
}

function getNotifType(type){ // 0 for shitstorm, 1 for comment
    if(type == 'MENTION_COMMENT' || type == 'LIKE_COMMENT' || type == 'POST_COMMENT'){
        return 1;
    }
    else if(type == 'FOLLOW'){
        return 2;
    }
    else if(type == 'CONFIRM_ANSWER' || type == 'POST_ANSWER' || type == 'MODERATION_ANSWER'){
        return 3;
    }
    return 0;
}

function getLinkFromNotification(element){ // 0 for shitstorm, 1 for comment
    if(element.notification_type == 'MENTION_COMMENT' || element.notification_type == 'LIKE_COMMENT' || element.notification_type == 'POST_COMMENT'){
        return `${https_url}shitstorm/${element.shitstorm.id}#comments_b`;
    }
    else if(element.notification_type == 'FOLLOW'){
        return `${https_url}profil/${element.user.id}`;
    }
    else if(element.notification_type == 'CONFIRM_ANSWER' || element.notification_type == 'POST_ANSWER'){
        return `${https_url}shitstorm/${element.shitstorm.id}#answer_b`;
    }
    else if(element.notification_type == 'MODERATION_SHITSTORM' || element.notification_type == 'MODERATION_ANSWER'){
        return `${https_url}moderation`;
    }
    return `${https_url}shitstorm/${element.shitstorm.id}`;
}
