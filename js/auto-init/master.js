$(document).ready(function(){
    $('.dropdown-button-menu').dropdown({
        hover: true, // Activate on hover
        stopPropagation: true,
        constrainWidth: false,
        belowOrigin: true,
      }
    );
    
	$(".button-collapse").sideNav();

	$('input#input_text, textarea#textarea1').characterCounter();

	$(".btn").click(function() {
        $("#modal1").modal();
    });
    
    $('select').material_select();

    $('.modal').modal();

    // $('.materialboxed').materialbox();

    initFancyBox();
    // Initialise fancybox

    $('.tooltipped').tooltip();

    $('ul.tabs').tabs(
        {'swipeable': false}
    );
          
    $('.parallax').parallax();
    
    initCommentTextAera();
    initLikeButtons();

    window.setInterval(function(){
        let connectedUser = document.getElementById('connected-user').getAttribute('value');

        if(connectedUser == ''){
            connectedUser = undefined; return;
        }
        
        $.get(
            https_url+"api/v2/activity/count.json",
            {},
            function(data){
                let json = JSON.parse(data);

                if("count" in json) {
                    let mobile = document.getElementById('notifMenuButtonMobile');
                    let btnmenu = document.getElementById('notifMenuButton');
                    let btnct = document.getElementById('notifMenuButtonContent');
                    if(json.count > 0){
                        mobile.innerHTML = `<i class='material-icons black-text'>notifications_active</i>Notifications (${json.count})</a>`;
                        if(btnmenu !== null){
                            btnmenu.dataset.tooltip = `${json.count} nouvelle` + (json.count > 1 ? 's' : '') + ' notification' + (json.count > 1 ? 's' : '');
                            btnct.innerHTML = 'notifications_active';
                            $(btnct).removeClass('white-text');
                            $(btnct).addClass('red-text');
                        }
                    }
                    else{
                        mobile.innerHTML = `<i class='material-icons black-text'>notifications_none</i>Notifications (0)</a>`;
                        if(btnmenu !== null){
                            btnmenu.dataset.tooltip = 'Aucune nouvelle notification';
                            btnct.innerHTML = 'notifications_none';
                            $(btnct).addClass('white-text');
                            $(btnct).removeClass('red-text');
                        }
                    }

                    $('.tooltipped').tooltip();
                }
            },
            "text"
        );
    }, 1000 * 120);
});

var in_loading_tweets = {};
var in_loading_toots = {};

// Loader wrapper
var preloader = `
<div class="preloader-wrapper active modal-preloader">
    <div class="spinner-layer spinner-blue-only">
        <div class="circle-clipper left">
            <div class="circle"></div>
        </div><div class="gap-patch">
            <div class="circle"></div>
        </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
    </div>
</div>`;

var modal_fail = `
<div class='modal-content'>
    <h5 class='error'>Impossible de charger les données.</h5>
</div>
<div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fermer</a>
</div>`;

// Script for Twitter Widgets
window.twttr = (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0],
        t = window.twttr || {};
    if (d.getElementById(id)) return t;
    js = d.createElement(s);
    js.id = id;
    js.src = "https://platform.twitter.com/widgets.js";
    fjs.parentNode.insertBefore(js, fjs);

    t._e = [];
    t.ready = function(f) {
        t._e.push(f);
    };

    return t;
}(document, "script", "twitter-wjs"));

function initFancyBox(){
    $('.fancy-boxed').fancybox({});
}

function refreshSelect() {
    let selects = $('select');
    selects.material_select('destroy');
    selects.material_select();
}

function random_id(){
    let randLetter = String.fromCharCode(65 + Math.floor(Math.random() * 26));
    return randLetter + Date.now();
}

function htmlEntities(str){
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/\n/g, '<br>');
}

function htmlSpeChars(str){
    return String(str).replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&quot;');
}

function noScript(strCode){
    let html = $(strCode.bold());
    html.find('script').remove();
    return html.html();
}

// Custom side shitstorm functions
function openShitstorm() {
    let cont = document.getElementById('shitstorm-side-container');
    let $shits = $(cont);
    if($shits.hasClass('opened')){
        return;
    }

    createCloseShitstormButton(cont);

    cont.style.cssText = 'transform: translateX(0);';
    $shits.addClass('opened');
    document.getElementById('main-container').style.cssText = 'transform: translateX(-180px) scale(0.95);';
    document.getElementsByTagName('body')[0].style.cssText = 'overflow: hidden; width:' + document.documentElement.clientWidth + 'px;';

    $('html').addClass('low-brightness disable-selecting');
    document.getElementsByTagName('html')[0].onclick = function(e) {
        // Ne ferme pas si c'est l'élément de base side-shitstorm ou l'overlay d'un modal
        if(!$(e.target).hasClass('side-shitstorm') && !$(e.target).hasClass('modal-overlay')) { 
            let c_e = e.target;
            let close = true;
            while(c_e.tagName != 'HTML') {
                if(cont.dataset.currentlyLoaded && c_e.dataset.shitstormId && cont.dataset.currentlyLoaded === c_e.dataset.shitstormId) {
                    close = false;
                    break;
                }

                if(c_e.id && (c_e.id === 'shitstorm-side-container' || c_e.id === 'main-modal-placeholder')) {
                    close = false;
                    break;
                }

                if(c_e.classList.contains('fancybox-container')) { // Si on est dans une vue d'image fancybox
                    close = false;
                    break;
                }
                c_e = c_e.parentElement;
            }  
            if(close) { 
                closeShitstorm(); 
            }    
        }
    };

    // Bind la touche échap pour fermer
    $(document).on('keyup', function(e) {
        if (e.keyCode === 27) closeShitstorm();
    });
}

function closeShitstorm(fromHistory = false) {
    document.getElementById('shitstorm-side-container').style.cssText = 'transform: translateX(100%);';
    document.getElementById('main-container').style.cssText = '';
    document.getElementsByTagName('body')[0].style.cssText = '';
    document.getElementsByTagName('html')[0].onclick = undefined;
    $('html').removeClass('low-brightness disable-selecting');
    $('#shitstorm-side-container').removeClass('opened');

    $(document).unbind('keyup');

    $('#close-side-shitstorm').hide(400, function () {
        let c = document.getElementById('close-side-shitstorm');
        c.parentElement.removeChild(c);
    });

    if(!fromHistory && typeof on_journal !== 'undefined' && on_journal) {
        // Enregistrement de l'historique
        window.history.pushState({opened_container: false}, "", "/journal/" + choosen_date);
    }
}

function createCloseShitstormButton(cont) {
    // Création d'une croix
    let closer = document.getElementById('close-side-shitstorm');
    if(!closer) {
        closer = document.createElement('div');
        closer.id = 'close-side-shitstorm';
        closer.classList.add('close-shitstorm-button');
        closer.innerHTML = '<i class="material-icons" style="font-size: 3.5rem; color: white;">close</i>';
        closer.onclick = function () {
            closeShitstorm();
        };

        cont.parentElement.appendChild(closer);
    }
}

function loadShitstormInContainer(id) {
    let cont = document.getElementById('shitstorm-side-container');

    $(document).ready(function () {
        openShitstorm();
    });

    if(!cont.dataset.currentlyLoaded || cont.dataset.currentlyLoaded != id) {
        cont.innerHTML = '<div style="margin-top:40px;">' + preloader + '</div><p class="center flow-text black-text">Chargement...</p>';
        // Attend que l'animation soit presque finie avant de charger quelque chose,
        // pour éviter de ralentir
        sleep(450).then(() => {
            $.get(
                https_url+"api/v2/generation/unique_shitstorm.json",
                {id: id},
                function(data){
                    var json = JSON.parse(data);
            
                    if("html" in json){
                        cont.innerHTML = json.html;
                        cont.dataset.currentlyLoaded = id;
            
                        $(cont).find("script").each(function() {
                            eval($(this).text());
                        });
            
                        $('.tooltipped').tooltip();
                        showTwitterLinks();
                        showMastodonLinks();
                        initLikeButtons();

                        $('ul.tabs').tabs(
                            {'swipeable': false}
                        );
                        
                        initCommentTextAera();

                        if(typeof on_journal !== 'undefined' && on_journal) {
                            // Enregistrement de l'historique
                            let current_state = {opened_container: true, id_shitstorm: id, container: cont.innerHTML};
                            window.history.pushState(current_state, $('.title-unique-shit').text(), "/shitstorm/" + id);
                        }
                    }
                },
                "text"
            )
            .fail(function (jqxhr) {
                console.log(jqxhr);
                cont.innerHTML = '<div class="error flow-text center" style="margin-top: 50px;">Impossible de charger la shitstorm</div>';
            });
        });
    }
    else if (typeof on_journal !== 'undefined' && on_journal) {
        // Enregistrement de l'historique : Shitsotrm déjà chargée
        let current_state = {opened_container: true, id_shitstorm: cont.dataset.currentlyLoaded, container: cont.innerHTML};
        window.history.pushState(current_state, $('.title-unique-shit').text(), "/shitstorm/" + cont.dataset.currentlyLoaded);
    }
}

function detectTransformLink(str){
    return str.replace(/(https?:\/\/((www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6})\b([-a-zA-Z0-9@:%_\+.~#?&\/=]*))/g,
        "<a class='linkCard notMargin red-text text-lighten-4' target='_blank' href='$1'>$2/...</a>");
}

function turnContainerOnOff(status) {
    if(status) {
        $('#main-container').addClass('container');
        document.getElementById('clear-margin').style.cssText = 'margin-bottom: 20px';
    }
    else {
        $('#main-container').removeClass('container');
        document.getElementById('clear-margin').style.cssText = 'margin-bottom: 0px';
    }
}

function isContainerActive() {
    return $('#main-container').hasClass('container');
}

function initCommentTextAera(){
    $(".comment-textaera").on('input change keyup paste', (i) => {
        if(i.target.value.trim() == ''){
            let id = i.target.dataset.idsub;
            document.getElementById('reply_to'+id).value = '';
        }
    });
}

//** LIKES ET DISLIKES DE SHITSTORM **//

function initLikeButtons() {
    let $rates = $('.shitstorm-rates-button');
    $rates.unbind('click');

    $rates.on('click', function () {
        generateViewRateModal('modalplaceholder', this.dataset.shitstormId);
    });

    let $like = $('.like-button');
    $like.unbind('click');

    $like.on('click', function () {
        likeDislike(this.dataset.shitstormId, true);
    });

    let $dlike = $('.dislike-button');
    $dlike.unbind('click');

    $dlike.on('click', function () {
        likeDislike(this.dataset.shitstormId, false);
    });
}

function likeDislike(idSub, isALike){
    // Modif immédiate du bouton
    var $count = $('.shitstorm-rates-button[data-shitstorm-id='+idSub+']');
    var actualCount = parseInt($count.html());
    var $lik = $('.like-button[data-shitstorm-id='+idSub+']');
    var $dis = $('.dislike-button[data-shitstorm-id='+idSub+']');

    // Cas 1 : C'est une suppression d'action : Bouton réappuyée sur action déjà effectuée
    if($lik.hasClass('liked') && isALike){ // Si la classe liked est déjà présente et on veut liker -> annulation like
        $lik.removeClass('liked');
        actualCount -= 1;
    }
    else if($dis.hasClass('disliked') && !isALike){ // Si la classe disliked est déjà présente et on veut disliker -> annulation dislike
        $dis.removeClass('disliked');
        actualCount += 1;
    }

    // Cas 2 : C'est un update OU une insertion
    else if(($dis || !$lik.hasClass('liked')) && isALike){ // Si (classe disliked déjà présente || pas de liked) et on veut liker
        if($dis.hasClass('disliked'))
            actualCount += 2;
        else
            actualCount += 1;

        $lik.addClass('liked');
        $dis.removeClass('disliked');
    }
    else if(($lik.hasClass('liked') || !$dis.hasClass('disliked')) && !isALike){ // Si (classe liked déjà présente || pas de disliked) et on veut disliker
        if($lik.hasClass('liked'))
            actualCount -= 2;
        else
            actualCount -= 1;

        $lik.removeClass('liked');
        $dis.addClass('disliked');
    }
    $count.html(actualCount);

    // Appel BDD
    $.post(
        https_url+"api/v2/rate/shitstorm/"+ (isALike ? 'like' : 'dislike') +".json",
        {id: idSub, isLike: isALike},
        function(data){
            let res = JSON.parse(data);

            if("rated" in res) {
                let status = (!res.rated ? 'unrated' : (res.rate_type ? 'liked' : 'disliked'));
                let actualCount = res.rating;

                if(status === 'unrated'){ // Suppression
                    $dis.removeClass('disliked');
                    $lik.removeClass('liked');
                }
                else if(status === 'liked'){ // Update en like OU insertion d'un like
                    $lik.addClass('liked');
                    $dis.removeClass('disliked');
                }
                else if(status === 'disliked'){ // Update en dislike OU insertion d'un dislike
                    $lik.removeClass('liked');
                    $dis.addClass('disliked');
                }

                $count.text(actualCount);
            }
            else{
                console.error("Erreur : JS (" + data + ")");
            }
        },
        "text"
    );
}

function like(idCom, idButton = ''){
    var isLiked = document.getElementById("inner"+idCom+idButton).innerHTML !== "star";

    var actualCount = parseInt(document.getElementById("nb"+idCom+idButton).innerHTML);
    if(isLiked){
        document.getElementById("inner"+idCom+idButton).innerHTML = "star";
        document.getElementById("nb"+idCom+idButton).innerHTML = actualCount+1;
    }
    else{
        document.getElementById("inner"+idCom+idButton).innerHTML = "star_border";
        document.getElementById("nb"+idCom+idButton).innerHTML = actualCount-1;
    }

    $.post(
        https_url+"api/v2/rate/comment/"+ (isLiked ? 'fav' : 'unfav') +".json",
        {id: idCom},
        function(data){
            var res = JSON.parse(data);
            if("favorited" in res && "rating" in res) {
                var isLiked = res.favorited;
                var actualCount = res.rating;

                if(isLiked) {
                    $("#inner"+idCom+idButton).text("star");
                }
                else {
                    $("#inner"+idCom+idButton).text("star_border");
                }
                $("#nb"+idCom+idButton).text(actualCount);
            }
            else{
                console.error("Erreur : JS (" + res + ")");
            }
        },
        "text"
    );
}

function closeAndOpenModal(idModalC, idModalO){
    $('#'+idModalC).modal('close');
    $('#'+idModalO).modal('open');
}

function getCurrentAnchorId(){
    var url = window.location.href;
    var hash = url.substring(url.indexOf("#")+1);
    if(hash.match(/^pub.*/)){
        return hash;
    }
    else return false;
}

function getIDByHash(){
    let hash = getCurrentAnchorId();

    if(hash){
        hash = hash.replace(/^pub([0-9]*)/, '$1');
        return hash;
    }
    
    return null;
}

function getDefinedShitstormID() {
    return (typeof load_shitstorm_until_id !== 'undefined' ? load_shitstorm_until_id : null);
}

function moveToAnchor(id){
    $('html, body').animate({
        scrollTop: $("#"+id).offset().top
    }, 500);
}

// * sleep function equivalent * //
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function toggleDarkMode(to_fill_check = false, id_check = ''){
    var newMode = 0;

    var styleEl = document.createElement('style');
    styleEl.id = 'tmp_style';
    styleEl.innerHTML = `p, select {
        transition: 0.3s ease;
    }`;

    // Append style element to head
    document.head.appendChild(styleEl);

    if($("#" + 'dark_mode_css').length == 0) {
        var fileref = document.createElement("link");
        fileref.setAttribute("rel", "stylesheet");
        fileref.setAttribute("type", "text/css");
        fileref.setAttribute("id", "dark_mode_css");
        fileref.setAttribute("href", 'https://shitstorm.fr/css/dark.css');
        document.head.appendChild(fileref);

        newMode = 1;
    }
    else{
        document.getElementById('dark_mode_css').remove();
    }

    await sleep(300);
    document.getElementById('tmp_style').parentElement.removeChild(styleEl);
    
    $.get(
        https_url+"api/v2/profile/night_mode.json",
        {mode: newMode},
        (data) => {
            var json = JSON.parse(data);
            if(!("night_mode" in json)){
                Materialize.toast('Impossible d\'actualiser.', 6000, 'rounded');
            }
            else{
                if(to_fill_check){
                    let ele = document.getElementById(id_check);
                    ele.checked = !!json.night_mode;
                }
            }
        },
        "text"
    );

    showTwitterLinks(true, (newMode ? 'dark' : 'light')); //Force l'actualisation de tous les liens
}

function getSavedTweet(linkID, documentPlaceHolderID){
    $.get(
        https_url+"api/v2/saves/get/" + linkID + ".json",
        {html: true},
        (data) => {
            var json = JSON.parse(data);
            if("html" in json){
                document.getElementById(documentPlaceHolderID).innerHTML = json.html;
                initFancyBox();
            }
        },
        "text"
    );
}

function showTwitterLinks(forceActualisation = false, forceTheme = undefined){
    $('.twitter-link').each(function () {
        // in_loading_tweets empêche les tweets de se charger deux fois si deux appels sont trop proches

        if(forceActualisation || in_loading_tweets[this.dataset.linkId] !== this){
            if(forceActualisation){
                this.innerHTML = '';
            }
            var tweetTheme = (forceTheme !== undefined ? forceTheme : this.dataset.theme);
    
            if(this.innerHTML.trim() == ''){ // Si le lien n'a pas déjà été chargé
                in_loading_tweets[this.dataset.linkId] = this;

                var check = (this.hasAttribute('data-check') ? this.dataset.check : false);
    
                twttr.widgets.createTweet(
                    this.dataset.linkId,
                    this,
                    {
                        align: 'center', theme: tweetTheme
                    }
                )
                .then((value) => {
                    if(value === undefined && check){
                        console.log(this.id + " tweet (id [" + this.dataset.linkId + "]) could not be loaded : Looking for a tweet save");
                        getSavedTweet(this.dataset.linkId, this.id);
                    } 
            
                    if((this.dataset.pubId) == ('pub' + getDefinedShitstormID())){
                        moveToAnchor(this.dataset.pubId);
                        // On écrase le contenu de la variable pour éviter de s'en resservir
                        // au prochain chargement de shitstorm
                        load_shitstorm_until_id = null;
                    }
    
                    in_loading_tweets[this.dataset.linkId] = 0;
                });
            }
        }
        else{
            console.log('Trying to load tweet [' + this.dataset.linkId + '], but tweet is already loading.');
        }
    });
}

function showMastodonLinks(forceActualisation = false){
    $('.mastodon-link').each((index, element) => {
        // in_loading_toots empêche les toots de se charger deux fois si deux appels sont trop proches

        if(forceActualisation || in_loading_toots[element.dataset.linkId] !== 1){
            if(forceActualisation){
                element.innerHTML = '';
            }

            let instance = (element.hasAttribute('data-instance') ? element.dataset.instance : false);
            let call = 'https://' + instance + '/api/v1/statuses/' + element.dataset.linkId;
    
            if(element.innerHTML.trim() == '' && instance){ // Si le lien n'a pas déjà été chargé
                in_loading_toots[element.dataset.linkId] = 1;

                // Appel à Masto
                $.get(
                    call,
                    {},
                    (data) => {
                        var json = JSON.parse(data);
                        // console.log(json);

                        if("error" in json){
                            element.innerHTML = `<h5 class='error center' style='margin-bottom: 1.5em;'>Impossible de charger le toot toot.</h5>`;
                        }
                        else{
                            // On ne sait pas si l'instance est réelle, la source fiable : On supprime toute balise de script sur chaque str
                            let date_toot = getRealDate(json.created_at, true);
                            let toot_realname = (json.account.display_name ? htmlSpeChars(json.account.display_name) : htmlSpeChars(json.account.username));

                            let pic_num = 0;
                            if("media_attachments" in json){
                                pic_num = json.media_attachments.length;
                            }

                            let pic_string = '';
                            let j = pic_num;
                            while(j > 0){
                                if(j-2 >= 0){ // Si il y a au moins deux images
                                    pic_string += `<div class='col s12 xl6'>
                                        <a class='fancy-boxed' href='${htmlSpeChars(json.media_attachments[pic_num-j].url)}' data-fancybox>
                                            <img class='responsive-img toot-image' src='${htmlSpeChars(json.media_attachments[pic_num-j].preview_url)}'>
                                        </a>
                                    </div>
                                    <div class='col s12 xl6'>
                                        <a class='fancy-boxed' href='${htmlSpeChars(json.media_attachments[(pic_num-j)+1].url)}' data-fancybox>
                                            <img class='responsive-img toot-image' src='${htmlSpeChars(json.media_attachments[(pic_num-j)+1].preview_url)}'>
                                        </a>
                                    </div>
                                    <div class='clearb'></div>`;
                                    j -= 2;
                                }
                                else{
                                    pic_string += `
                                    <div class='col s12'>
                                        <a class='fancy-boxed' href='${htmlSpeChars(json.media_attachments[pic_num-j].url)}' data-fancybox>
                                            <img class='responsive-img toot-image' src='${htmlSpeChars(json.media_attachments[pic_num-j].preview_url)}'>
                                        </a>
                                    </div>
                                    <div class='clearb'></div>`;
                                    j--;
                                }
                            }

                            element.innerHTML = `
                            <div class='mastodon-toot'>
                                <div class='mastodon-pp'>
                                    <a title='${toot_realname}' target='_blank' href='${htmlSpeChars(json.account.url)}'>
                                        <img class='toot-profile-image image-cursor-overlay' alt='Mastodon avatar' src='${htmlSpeChars(json.account.avatar_static)}'>
                                    </a>
                                </div>
                                <div class='mastodon-profile'>
                                    <p>
                                        <a target='_blank' href='${htmlSpeChars(json.account.url)}'>${toot_realname}</a><br>
                                        @${htmlSpeChars(json.account.username)}@${instance}<br>
                                        <a target='_blank' href='${htmlSpeChars(json.url)}'>${date_toot}</a>
                                    </p>
                                </div>
                                <div class='clearb divider'></div>
                                <div class='mastodon-content'>${noScript(json.content)}</div>
                                <div class='mastodon-images'>${pic_string}</div>
                            </div>`;
                        }
                        in_loading_toots[element.dataset.linkId] = 0;
                    },
                    "text"
                )
                .fail(() => {
                    element.innerHTML = `<h5 class='error center' style='margin-bottom: 1.5em;'>Impossible de charger le toot toot.</h5>`;
                    in_loading_toots[element.dataset.linkId] = 0;
                });
            }
        }
        else{
            console.log('Trying to load toot [' + element.dataset.linkId + '], but toot is already loading.');
        }
    });
}

// Link initialisation
twttr.ready(() => {
    showTwitterLinks();
});

$(document).ready(function () {
    showMastodonLinks();
});

function getRealDate(date, isAlreadyFormatted = false){
    let real_date = date;

    if(!isAlreadyFormatted){
        // Date parse
        real_date = date.split(' ');
        if(real_date.length > 1){
            let part1 = real_date[0] + 'T';
            let part2 = real_date[1] + 'Z';
            real_date = part1 + part2;
        }
        else{
            real_date = real_date[0];
        }
    }

    let d = new Date(real_date);

    let current = new Date();

    if(d.getFullYear() < current.getFullYear()){
        return ((current.getFullYear() - d.getFullYear()) == 1 ? "L'année dernière" : "Il y a " + (current.getFullYear() - d.getFullYear()) + " ans");
    }

    let month = (d.getMonth() < 9 ? "0"+String(d.getMonth()+1) : String(d.getMonth()+1));
    let day = String(d.getDate() < 10 ? "0"+String(d.getDate()) : String(d.getDate()));

    let min = (d.getMinutes() < 10 ? "0"+String(d.getMinutes()) : String(d.getMinutes()));
    let hour = String(d.getHours());

    return `${day}/${month} à ${hour}h${min}`;   
}

function formatRealDate(date, isAlreadyFormatted = false){
    let real_date = date;

    if(!isAlreadyFormatted){
        // Date parse
        real_date = date.split(' ');
        if(real_date.length > 1){
            let part1 = real_date[0] + 'T';
            let part2 = real_date[1] + 'Z';
            real_date = part1 + part2;
        }
        else{
            real_date = real_date[0];
        }
    }

    let d = new Date(real_date);

    let month = (d.getMonth() < 9 ? "0"+String(d.getMonth()+1) : String(d.getMonth()+1));
    let day = String(d.getDate() < 10 ? "0"+String(d.getDate()) : String(d.getDate()));

    return `${day}/${month}/${d.getFullYear()}`;   
}

// Reply to a comment : automatically place @username in textaera
function replyToCom(username, idSub = '', idCom = ''){
    let textaera = document.getElementById('contentCom'+idSub);
    let commentText = document.getElementById('commentText'+idCom);
    let connectedUser = document.getElementById('connected-user').getAttribute('value');

    if(connectedUser == ''){
        connectedUser = undefined; return;
    }

    // Recherche si il y a des mentions dans le commentaire auquel répondre
    let re_all = />(@[a-zA-Z0-9_-]{3,16})<\/a>/g;
    let matches = [];

    let corres = re_all.exec(commentText.innerHTML);
    while(corres){
        matches.push(corres[1]);
        corres = re_all.exec(commentText.innerHTML);
    }

    // Rajoute les mentions dans le textaera directement
    for(let i = 0; i < matches.length; i++){
        // Si le nom d'utilisateur matché n'est PAS dans le textaera et si ce n'est PAS l'utilisateur connecté actuel
        if(!textaera.value.match(new RegExp(matches[i], 'i')) && !matches[i].match(new RegExp('^@'+connectedUser+'$', 'i')) ){
            textaera.value = matches[i].trim() + ' ' + textaera.value.trim();
        }
    }

    let re = new RegExp("^(.*)(@"+username+")(.+)$", 'i');
    // Recherche le nom d'utilisateur à mentionner obligatoirement

    let m = textaera.value.match(re);
    if(m){
        // Si il y est, on le met en première position
        let ending = m[3].trim();
        if(ending){
            ending += ' ';
        }
        let center = m[1].trim();
        if(center){
            center += ' ';
        }
        textaera.value = m[2] + ' ' + center + ending;
    }
    else{
        // Sinon, on l'ajoute au début
        let ending = textaera.value.trim();
        if(ending){
            ending += ' ';
        }
        textaera.value = '@' + username + ' ' + ending;
    }

    document.getElementById('reply_to'+idSub).value = idCom;

    textaera.focus();

    if(idSub){ // Si on est sur le journal
        $('#reveal'+idSub).animate({
            scrollTop: $('#contentCom'+idSub).offset().top
        }, 2000);
    }
    else{ // Si on est sur la page unique
        $('html, body').animate({
            scrollTop: $('#contentCom').offset().top
        }, 800);
    }   
}

function foldShitstorm(dataID){
    let element = $(".hidden-link[data-shitstorm-id=" + dataID + "]");

    if(element.is(":visible")){
        $(".link-arrow[data-shitstorm-id=" + dataID + "]").removeClass('unfolded');
    }
    else{
        $(".link-arrow[data-shitstorm-id=" + dataID + "]").addClass('unfolded');
    }

    element.slideToggle(140);
}

// Generate User Card (new gen)
function generateUserFlow(json) {
    let str = '';
    let suf2 = json.gender === 'female' ? 'e' : (json.gender === 'male' ? '' : '.e');
    let suf = json.gender === 'female' ? 'rice' : (json.gender === 'male' ? 'eur' : 'eur.rice');
    let isLogged = true;

    let connectedUser = document.getElementById('connected-user').getAttribute('value');

    if(connectedUser == ''){
        isLogged = false;
    }

    if(isLogged && !json.is_you && json.followable) {
        str += `<ul id='follow_user_drop${json.id}' class='dropdown-content' data-realname='${htmlSpeChars(json.realname)}'>
            <li>
                <span>
                    <input class='follow-activate follow-checkbox' data-userid='${json.id}' type='checkbox' id='followed${json.id}' ${json.following ? 'checked' : ''}>
                    <label class='black-text' for="followed${json.id}">${json.following ? 'Suivi' : 'Suivre'}</label>
                </span>
            </li>
            <li>
                <span>
                    <input class='follow-activate email-checkbox' data-userid='${json.id}' type='checkbox' id='followed_with_mail${json.id}' ${json.follow_with_mail ? 'checked' : ''}>
                    <label class='black-text' for="followed_with_mail${json.id}">E-mails</label>
                </span>
            </li>
        </ul>`;
    } 

    str += `<div class='col s12 l5-mini card-user-new border-user user-card-borders'>
        
        <a href='${https_url}profil/${json.username}' class='background-full default-color background-user-card block' style="` +
            (json.full_banner_img ? `background-image: url('${json.full_banner_img}');` : '') + `
        "></a>
        
        <div class='user-card-padding'>
            <a href='${https_url}profil/${json.username}'>
                <img id='profile_img' class='user-card-image' src="${json.full_profile_img}">
            </a>

            <h5><a class='underline-hover no-text-color' href='${https_url}profil/${json.id}'>${htmlSpeChars(json.realname)}</a></h5>

            <div class='black-text' style='margin-left: 1em;'>
                <a class='underline-hover no-text-color' href='${https_url}profil/${json.username}'>@${json.username}</a>
            </div>
            <div class='black-text' style='margin-left: 1em;'>
                Inscrit${suf2} le ${formatRealDate(json.register_date)}
            </div>` +
            (json.follows_you ? "<div class='notification-bold-text' style='margin-left: 1em;'>Vous suit</div>" : '') + `

            <div class='clearb'></div>
        </div> 

        <div class='buttons-user'>` + 
            (json.is_you ? `<a href='${https_url}compte' class='btn-floating tooltipped orange lighten-1 left profile_button'
                data-position='bottom' data-delay='10' data-tooltip='Mon compte'>
                    <i class='material-icons'>settings</i>
                </a>` : '') +

            (json.have_storm ? `<button class='btn-floating tooltipped purple darken-2 left profile_button'
                data-position='bottom' data-delay='10' data-tooltip='Des ÉCLAIRS'>
                    <i class='material-icons'>flash_on</i>
                </button>` : '') +

            (json.have_snow ? `<button class='btn-floating tooltipped blue lighten-3 left profile_button'
                data-position='bottom' data-delay='10' data-tooltip='Neige !'>
                    <i class='material-icons'>ac_unit</i>
                </button>` : '') + `

            <button class='btn-floating tooltipped cyan white-text darken-` + (json.status ? (json.status == 1 ? '2' : '3') : '1') + ` left profile_button'
            data-position='bottom' data-delay='10' data-tooltip='` + (json.status ? (json.status == 1 ? "Modérat" + suf : "Administrat" + suf) : "Utilisat" + suf) + `'>
                <i class='material-icons'>` + (json.status ? (json.status == 1 ? 'group' : 'supervisor_account') : 'person') + `</i>
            </button>`;
            if(isLogged && !json.is_you && json.followable){
                str += `<button id='follow_user_button${json.id}' class='btn-floating ` + (json.following ? 'green' : 'grey') + ` left profile_button dropdown-button'
                    data-activates='follow_user_drop${json.id}'>
                    <i id='follow_user_bell${json.id}' class='material-icons'>` + (json.following ? 'notifications' : 'notifications_off') + `</i>
                </button>`;
            }
            str += `<div class='clearb'></div>
        </div>
    </div>`;

    return str;
}
