var preview_link1 = document.getElementById('shit_link_1');
var preview_link2 = document.getElementById('shit_link_2');
var preview_link3 = document.getElementById('shit_link_3');
var preview_link4 = document.getElementById('shit_link_4');
var preview_images = document.getElementById('shit_images');
var preview_title = document.getElementById('shit_title');
var preview_dscr = document.getElementById('shit_content');
var preview_date = document.getElementById('shit_date');

var open_new_tab = '<i class="material-icons left">open_in_new</i>';

function verifyNbLinks(){
    var len = $("#imagesInput")[0].files.length;
    var str = '';
    for(var i = 0; i < len; i++){
        str += '<div class="image-file-resume">' + $("#imagesInput")[0].files[i].name + '</div>';
    }
    preview_images.innerHTML = str;

    var li = document.getElementById('link');
    if(li.value.trim() != ''){
        len += 1;
        preview_link1.innerHTML = open_new_tab + '<a href="'+li.value+'" target="_blank">'+li.value+'</a>';
    }
    else{
        preview_link1.innerHTML = '';
    }
    li = document.getElementById('link2');
    if(li.value.trim() != ''){
        len += 1;
        preview_link2.innerHTML = open_new_tab + '<a href="'+li.value+'" target="_blank">'+li.value+'</a>';
    }
    else{
        preview_link2.innerHTML = '';
    }
    li = document.getElementById('link3');
    if(li.value.trim() != ''){
        len += 1;
        preview_link3.innerHTML = open_new_tab + '<a href="'+li.value+'" target="_blank">'+li.value+'</a>';
    }
    else{
        preview_link3.innerHTML = '';
    }
    li = document.getElementById('link4');
    if(li.value.trim() != ''){
        len += 1;
        preview_link4.innerHTML = open_new_tab + '<a href="'+li.value+'" target="_blank">'+li.value+'</a>';
    }
    else{
        preview_link4.innerHTML = '';
    }

    if(len > 5){
        return false;
    }
    return true;
}

var max_input = "<h5 class='error'>Il y a plus de 5 liens cumulés saisis. Retirez un(des) lien(s) ou image(s).</h5>";
var msg_box = document.getElementById('msg');

// Alertes de formulaire
$("#imagesInput").on("change", function() {
    if(!verifyNbLinks()){
        msg_box.innerHTML = max_input;
    }
    else{
        msg_box.innerHTML = '';
    }
});

$(".link-soumission").on("input change keyup paste", function() {
    if(!verifyNbLinks()){
        msg_box.innerHTML = max_input;
    }
    else{
        msg_box.innerHTML = '';
    }
});

$("#title").on("input change keyup paste", function() {
    console.log(this);
    preview_title.innerHTML = htmlEntities(this.value);
});

$("#dscr").on("input change keyup paste", function() {
    preview_dscr.innerHTML = htmlEntities(this.value);
});

$("#datepicker").on("change", function() {
    preview_date.innerHTML = this.value;
});

