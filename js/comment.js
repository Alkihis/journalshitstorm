var sorting_comments = 'desc';
var sort_type = 'date';
var already_posting = false;

function replaceUsernames(string){
    return string.replace(/(@([a-zA-Z0-9_-]{3,16}))/g, `<a class='linkCard notMargin arobase blue-text text-lighten-4' href='${https_url}profil/$2'>$1</a>`);
}

function commentCore(value, connected, hidden = false){
    let liked_pic = (value.favorited ? 'star' : 'star_border');
    string = `
        <div class="commentaire" id="commentTotal${value.id}" style="padding-top: .5em;`+ (hidden ? 'display: none;' : '') +`">
            <div class="card card-border commentBack">
                <div class="card-content white-text">
                    <p style="margin-bottom: 1.5em;" id="commentText${value.id}">`+ replaceUsernames(detectTransformLink(htmlEntities(value.content))) +`</p>
                    <div class="row card-action commentaireUnique">`;
                    if(connected){
                        string += `
                        <span class="likeButtons" style="margin-top: .3em;">
                            <button type="button" class="btn-floating blue darken-3 tooltipped" data-position="bottom" data-delay="10" data-tooltip="Répondre"
                                style="float: left; margin-right: 10px;" id="reply${value.id}">
                                <i class="material-icons">reply</i>
                            </button>
                            <form>
                                <button type="button" class="btn-floating blue lighten-1 tooltipped" data-position="bottom" data-delay="10" data-tooltip="Favori"
                                    style="float: left; margin-right: 10px;" name="favCom" id="fav${value.id}">
                                    <i class="material-icons" id="inner${value.id}">${liked_pic}</i>
                                </button> 
                            </form>
                        </span>`;
                    }
                    string += `
                        <button class="btn-floating orange darken-5 tooltipped" data-position="bottom" data-delay="10" data-tooltip="Nombre de favs"
                            style="float: left; text-align: center;" id="nb${value.id}">${value.rating}
                        </button>`;
                    if(connected && value.can_delete){
                        string += `
                        <button id="delC${value.id}" class="btn-floating red lighten-1 tooltipped" data-position="bottom" data-delay="10" data-tooltip="Supprimer" type="submit"
                            style="float: left !important; margin-left: 10px;" name="delCom">
                            <i class="material-icons">delete_forever</i>
                        </button>`;
                    }
                    string += `
                        <div style="font-style: italic; text-align: right;">
                        <div class="chip tooltipped comment-chip" data-position="bottom" data-delay="10" data-tooltip="Posté le `+ getRealDate(value.date) +`">
                            <img class="circle" src="${value.user.full_profile_img}" width="32" height="32">
                            <a class="notCardCommentLink" href="${https_url}profil/${value.user.id}">${value.user.username}</a></div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>`;
    return string;
}

function buildReplies(json, documentFragment, idCom){
    let connected = !(document.getElementById('connected-user') == '');

    if(json.comments.length > 0){
        var string = '';
        var rand_id = random_id();
        let next_cursor = json.next_cursor;

        json.comments.forEach((value) => {
            string += commentCore(value, connected);
        });

        documentFragment.innerHTML = string;

        json.comments.forEach((value) => {
            if(connected){
                if($('#reply'+value.id).length != 0){
                    document.getElementById('reply'+value.id).onclick = () => {
                        replyToCom(value.username, "", value.id);
                    }
                }
                if($('#fav'+value.id).length != 0){
                    document.getElementById('fav'+value.id).onclick = () => {
                        like(value.id);
                    }
                }
            }
            
            if($('#nb'+value.id).length != 0){
                document.getElementById('nb'+value.id).onclick = () => {
                    generateViewFavModal('modalplaceholder', value.id)
                }
            }

            if(value.can_delete){
                if($('#delC'+value.id).length != 0){
                    document.getElementById('delC'+value.id).onclick = () => {
                        createDeleteCommentModalUniqueS('modalphbottom', value.id);
                    }
                }
            }
        });

        // Modification de l'ID de l'élément shitblock
        documentFragment.id = "comment_replies_block_previous"+rand_id;

        var g = document.createElement('div');
        g.setAttribute("id", "comment_replies_"+idCom+"_main");

        if(next_cursor > 0){
            g.innerHTML = "<button style='margin-top: 1em; margin-bottom: 1em;' id='comment_replies_next_button"+ (rand_id) +"' class='btn col s10 offset-s1 teal darken-1'>Réponses suivantes</button><div class='clearb'></div>";
        }

        document.getElementById('comment_replies_'+idCom+'_parent').appendChild(g);
        // Met le nouveau bloc dans le bloc maître des réponses du commentaire idCom

        let next_button = document.getElementById(`comment_replies_next_button${rand_id}`);
        if(next_cursor > 0 && next_button){
            next_button.onclick = () => {
                loadReplies(document.getElementById("comment_replies_"+idCom+"_main"), idCom, next_cursor);
            };
        }

        $('.tooltipped').tooltip();
    }
    else{ documentFragment.innerHTML = '' }
}

function loadReplies(documentFragment, idCom, nextCursor, fromJsonFragment = undefined){
    documentFragment.innerHTML = preloader;
    if(!fromJsonFragment) {
        $.get(
            https_url+"api/v2/comment/read.json",
            {mode: 'list', replies_to: idCom, fetch_by: 'date', since_id: nextCursor, count: 10, order: 'ASC'},
            function(data){
                var json = JSON.parse(data);
                buildReplies(json, documentFragment, idCom);
            },
            "text"
        )
        .fail(() => {
            documentFragment.innerHTML = `<h5 class='error center' style='margin-bottom: 1.5em;'>Impossible de charger les réponses.</h5>`;
        });
    }
    else{
        let dic = {'comments': fromJsonFragment.replies, 'next_cursor': fromJsonFragment.replies_next_cursor};
        buildReplies(dic, documentFragment, idCom);
    }
}

function loadComments(documentFragment, idSub, nextCursor, sortReplyTo = true){
    documentFragment.innerHTML = preloader;

    let dic = {mode: 'list', id: idSub, fetch_by: sort_type, count: 10};

    if(dic.fetch_by === 'date') {
        if(sorting_comments === 'asc') {
            dic.order = 'ASC';
            dic.since_id = nextCursor;
        }
        else {
            dic.order = 'DESC';
            dic.max_id = nextCursor;
        }
    }
    else {
        if(sorting_comments === 'asc') {
            dic.order = 'ASC';
        }
        else {
            dic.order = 'DESC';
        }
        dic.skip = nextCursor;
    }

    $.get(
        https_url+"api/v2/comment/read.json",
        dic,
        function(data){
            var json = JSON.parse(data);
            let connected = !(document.getElementById('connected-user') == '');

            if(json.comments.length > 0){
                var string = '';
                var rand_id = random_id();
                let next_cursor = json.next_cursor;

                json.comments.forEach((value) => {
                    string += commentCore(value, connected) + `
                    <div id='comment_replies_${value.id}_parent' class='col s11 offset-s1 no-right-padding'>
                        <div id='comment_replies_${value.id}_main'></div>
                    </div>
                    <div class='clearb'></div>`;
                });

                documentFragment.innerHTML = string;

                json.comments.forEach((value) => {
                    // Charge les réponses
                    loadReplies(document.getElementById(`comment_replies_${value.id}_main`), value.id, 0, value);

                    if(connected){
                        if($('#reply'+value.id).length != 0){
                            document.getElementById('reply'+value.id).onclick = () => {
                                replyToCom(value.username, "", value.id);
                            }
                        }
                        if($('#fav'+value.id).length != 0){
                            document.getElementById('fav'+value.id).onclick = () => {
                                like(value.id);
                            }
                        }
                    }

                    if($('#nb'+value.id).length != 0){
                        document.getElementById('nb'+value.id).onclick = () => {
                            generateViewFavModal('modalplaceholder', value.id)
                        }
                    }

                    if(value.can_delete){
                        if($('#delC'+value.id).length != 0){
                            document.getElementById('delC'+value.id).onclick = () => {
                                createDeleteCommentModalUniqueS('modalphbottom', value.id);
                            }
                        }
                    }
                });

                // Modification de l'ID de l'élément shitblock
                documentFragment.id = "comment_block_previous"+rand_id;

                var g = document.createElement('div');
                g.setAttribute("id", "comment_main_block");

                if(next_cursor > 0){
                    g.innerHTML = "<button style='margin-top: 1em; margin-bottom: 1em;' id='comment_next_button"+ (rand_id) +"' class='btn col s10 offset-s1 teal darken-1'>Commentaires suivants</button><div class='clearb'></div>";
                }

                document.getElementById('comment_parent_block').appendChild(g);

                let next_button = document.getElementById(`comment_next_button${rand_id}`);
                if(next_cursor > 0 && next_button){
                    next_button.onclick = () => {
                        loadComments(document.getElementById('comment_main_block'), idSub, next_cursor, sortReplyTo);
                    };
                }

                $('.tooltipped').tooltip();

                if(!$('#comment_parent_block').is(':visible')){
                    $('#comment_parent_block').show(220);
                }

                if(!$('#sorting_comment_block').is(':visible')){
                    $('#sorting_comment_block').show();
                    $('select').material_select();
                }
            }
            else{ documentFragment.innerHTML = '' }
        },
        "text"
    )
    .fail(() => {
        documentFragment.innerHTML = `<h5 class='error center' style='margin-bottom: 1.5em;'>Impossible de charger les commentaires.</h5>`;
    });
}

function initComment(idSub){
    $('#comment_parent_block').hide(220, () => {
        document.getElementById('comment_parent_block').innerHTML = "<div id='comment_main_block'></div>";
        loadComments(document.getElementById('comment_main_block'), idSub, 0);
    });
}

function postComment(idSub){
    // Commentaire à poster
    var text_aera = document.getElementById('contentCom');
    var connected = !(document.getElementById('connected-user') == '');
    var comment_counter = document.getElementById('comment_tab_trigger');
    var reply = document.getElementById('reply_to').value;

    let dic = {id: idSub, content: text_aera.value};
    if(reply){
        dic.in_reply_to_id = reply;
    }

    if(!already_posting && connected){
        already_posting = true;
        $.post(
            https_url+"api/v2/comment/create.json",
            dic,
            function(data){
                var json = JSON.parse(data);
                
                if('id' in json){
                    let new_comment = json;
                    text_aera.value = '';
                    reply.value = '';

                    let comment_str = commentCore(new_comment, connected, true);

                    let is_reply = new_comment.in_reply_to_id_conversation;

                    if(is_reply){ // Regarde si le commentaire auquel il répond est présent sur la page
                        let normal_reply_block = document.getElementById('comment_replies_'+ is_reply +'_main');

                        if(normal_reply_block){ // On met le commentaire dans un nouveau block juste avant le block actuel
                            let g = document.createElement('div');
                            g.setAttribute("id", "comment_replies_previous"+random_id());
                            g.innerHTML = comment_str;

                            document.getElementById('comment_replies_'+ is_reply +'_parent').insertBefore(g, normal_reply_block);
                        }
                        else{ // La tentative de mettre en réponse a échoué
                            is_reply = false;
                        }
                    }

                    if(!is_reply){ // On met le commentaire dans un nouveau block juste avant le block actuel
                        let normal_block = document.getElementById('comment_main_block');

                        if(normal_block){ // On le place au bout de ce block                           
                            let g = document.createElement('div');
                            g.setAttribute("id", 'comment_previous_block'+random_id());
                            g.innerHTML = comment_str;

                            document.getElementById('comment_parent_block').insertBefore(g, normal_block);
                        }
                    }

                    if(connected){
                        if($('#reply'+new_comment.id).length != 0){
                            document.getElementById('reply'+new_comment.id).onclick = () => {
                                replyToCom(new_comment.user.username, "", new_comment.id);
                            }
                        }
                        if($('#fav'+new_comment.id).length != 0){
                            document.getElementById('fav'+new_comment.id).onclick = () => {
                                like(new_comment.id);
                            }
                        }
                    }

                    if($('#nb'+new_comment.id).length != 0){
                        document.getElementById('nb'+new_comment.id).onclick = () => {
                            generateViewFavModal('modalplaceholder', new_comment.id)
                        }
                    }

                    if(new_comment.can_delete){
                        if($('#delC'+new_comment.id).length != 0){
                            document.getElementById('delC'+new_comment.id).onclick = () => {
                                createDeleteCommentModalUniqueS('modalphbottom', new_comment.id);
                            }
                        }
                    }
                    $('#commentTotal'+new_comment.id).show(220);

                    // Ajout +1 au compteur de commentaire
                    let countCom = Number(comment_counter.dataset.commentCount);
                    countCom += 1;
                    comment_counter.dataset.commentCount = countCom;
                    comment_counter.innerText = `Commentaires (${countCom})`;

                    
                    if(!$('#comment_parent_block').is(':visible')){
                        $('#comment_parent_block').show(220);
                    }
                    if(!$('#sorting_comment_block').is(':visible')){
                        $('#sorting_comment_block').show(220);
                    }
                }
                else{
                    Materialize.toast('Le post de votre commentaire a échoué.', 6000, 'rounded');
                }
                already_posting = false;
                $('.tooltipped').tooltip();
            },
            "text"
        )
        .fail((answer, text, other) => {
            Materialize.toast('Le post de votre commentaire a échoué.', 6000, 'rounded');
            already_posting = false;
        });
    }
    else{
        Materialize.toast('Un commentaire est déjà en cours de soumission.', 6000, 'rounded');
    }
}
