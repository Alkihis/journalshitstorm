var open_new_tab = '<i class="material-icons left">open_in_new</i>';
var clearb =  "<div class='clearb'></div>";

function actualiseSS(isAnswer, idWaiting){
    var prefix = (isAnswer ? 'A' : '');
    var typeSS = (isAnswer ? 'answer' : 'shitstorm');

    var dscr = document.getElementById('dscr'+prefix+idWaiting);
    var date = document.getElementById('date'+prefix+idWaiting);
    var links = document.getElementById('link'+prefix+idWaiting);

    $.get(
        https_url+"api/v2/moderation/read/" + typeSS + ".json",
        {mode: 'id', id: idWaiting},
        function(data){
            let json = JSON.parse(data);

            if("id" in json){
                let shitst = json; // La shitstorm / réponse est le JSON parsé

                dscr.innerHTML = htmlSpeChars(shitst.content);
                var dateObj = formatRealDate(shitst.date);

                date.innerHTML = (isAnswer ? 'Réponse' : 'Shitstorm') + ' du ' + dateObj;

                // Liens
                if(!isAnswer){ // Shitstorm : Liens dans un tableau
                    document.getElementById('title'+idWaiting).innerHTML = htmlSpeChars(shitst.title);
                    links.innerHTML = '';

                    for(let i = 0; i < shitst.links.length; i++){
                        links.innerHTML += `<div class='black-text'>${open_new_tab}<a href='${shitst.links[i].link}' target='_blank'>${shitst.links[i].link}</a></div>${clearb}`;
                    }
                }
                else{
                    links.innerHTML = `<div class='black-text'>${open_new_tab}<a href='${shitst.link}' target='_blank'>${shitst.link}</a></div>`;
                }

                if("locked" in shitst){
                    let link = document.getElementById('setActive'+idWaiting+(isAnswer ? 'A' : ''));
                    if(shitst.locked){
                        link.innerHTML = `Définir comme éditable`;
                        link.onclick = () => {changeVisibility(idWaiting, isAnswer, 0);}
                    }
                    else{
                        link.innerHTML = `Définir comme non-éditable`;
                        link.onclick = () => {changeVisibility(idWaiting, isAnswer, 1);}
                    }
                }

                Materialize.toast('Les informations de la '+(isAnswer ? 'réponse' : 'shitstorm')+' en attente (ID='+idWaiting+') ont été actualisées.', 6000, 'rounded');
            }
            else{
                Materialize.toast('La soumission n\'existe plus ou le serveur n\'a pas répondu correctement.', 6000, 'rounded');
            }
        },
        "text"
    ) .fail(function(jqXHR, textStatus, errorThrown) {
        console.log("Data : ", JSON.parse(jqXHR.responseText));
        Materialize.toast('Impossible d\'actualiser.', 6000, 'rounded');
    });
}

function changeVisibility(idSub, isAnswer, newVisibility){
    $.post(
        https_url+"api/v2/moderation/update/"+ (isAnswer ? 'answer' : 'shitstorm') +".json",
        {lock: newVisibility, id: idSub},
        function(data){
            let json = JSON.parse(data);

            if("locked" in json){
                let link = document.getElementById('setActive'+idSub+(isAnswer ? 'A' : ''));
                if(json.locked){
                    link.innerHTML = `Définir comme éditable`;
                    link.onclick = () => {changeVisibility(idSub, isAnswer, 0);}
                }
                else{
                    link.innerHTML = `Définir comme non-éditable`;
                    link.onclick = () => {changeVisibility(idSub, isAnswer, 1);}
                }

                Materialize.toast('Les informations de la '+(isAnswer ? 'réponse' : 'shitstorm')+' en attente ont été actualisées.', 6000, 'rounded');
            }
            else{
                Materialize.toast('La soumission n\'existe plus ou le serveur n\'a pas répondu correctement.', 6000, 'rounded');
            }
        },
        "text"
    ) .fail(function(jqXHR, textStatus, errorThrown) {
        console.log("Data : ", JSON.parse(jqXHR.responseText));
        Materialize.toast('Impossible d\'actualiser.', 6000, 'rounded');
    });
}
