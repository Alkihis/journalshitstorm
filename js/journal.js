let byYear = 0;
let choosen_date = '';
let choosen_mode = '';
let choosen_order = '';
let choosen_visibility = '';
const on_journal = true;

function getTextualMonth(month) {
    switch(month){
        case 1:
            return "Janvier";
        case 2:
            return "Février";
        case 3:
            return "Mars";
        case 4:
            return "Avril";
        case 5:
            return "Mai";
        case 6:
            return "Juin";
        case 7:
            return "Juillet";
        case 8:
            return "Août";
        case 9:
            return "Septembre";
        case 10:
            return "Octobre";
        case 11:
            return "Novembre";
        case 12:
            return "Décembre";
        default:
            return "Unknown";
    }
}

function generateJournalShits(idPlaceHolder, curr_date, by_year = false, view_mode = 'date', order = 'desc', vis = 'month'){
    let shitstorm_div = document.getElementById(idPlaceHolder);
    shitstorm_div.innerHTML = `<div id="shitblock"></div>`;
    choosen_date = curr_date;

    if(choosen_mode === '' && localStorage.getItem('shitstorm_view_mode')) { // Si c'est le chargement initial de la page et que l'utilisateur a un mode déjà choisi
        choosen_mode = view_mode = localStorage.getItem('shitstorm_view_mode');

        // Il faut actualiser le select
        document.getElementById('selmode').value = view_mode;
        refreshSelect();
    }
    else {
        choosen_mode = view_mode;
    }

    choosen_order = order;
    if(!choosen_visibility){
        choosen_visibility = vis;
    }

    sessionStorage.setItem('loadedShitJournal', 0);
    sessionStorage.setItem('onLoadShitJournal', 0);
    sessionStorage.setItem('currentWeekJournal', 0);
    sessionStorage.setItem('currentMonthJournal', 0);

    let until = getDefinedShitstormID();
    if(!until){
        until = false;
    }
    byYear = by_year;

    getJournalShits(curr_date, until, view_mode, order);
}

function getJournalShits(curr_date, until = false, view_mode = 'date', shitstorm_order = 'desc', next_skip = 0){
    if(Number(sessionStorage.getItem('onLoadShitJournal')) == 1){
        // Un chargement est déjà en cours
        return;
    }

    let get_infos = Boolean(Number(sessionStorage.getItem('getInfosJournal')));
    // Permet de savoir si on doit charger les infos de shitstorm (page suivante, page précédente, texte d'affichage...)
    if(get_infos){
        sessionStorage.setItem('getInfosJournal', 0);
    }

    let newMode = 1;
    // Active les shitstorms par cards (! risque de cassage de features !)
    if(localStorage.getItem('shitstorm_flow_off') && localStorage.getItem('shitstorm_flow_off') == "true"){
        newMode = 0;
    }

    // Nombre de shitstorms à fetch par .get
    let shitstorm_count = 5;
    if(localStorage.getItem('count_shitstorm_per_load')){
        shitstorm_count = Number(localStorage.getItem('count_shitstorm_per_load'));
    }

    // Permet de savoir si le scrollfire est désactivé (1 si désactivé)
    var disable_auto_scroll = Number(localStorage.getItem('disable_auto_scroll'));

    shitb = document.getElementById('shitblock');

    shitb.innerHTML += '<div id="preloader">' + preloader + '</div>';

    sessionStorage.setItem('onLoadShitJournal', 1);

    $.get(
        https_url+"api/v2/shitstorm/generate.json",
        {skip: next_skip, date: curr_date, count: shitstorm_count, load_until: until, new_model: newMode, mode: view_mode, order: shitstorm_order, infos: get_infos},
        function(data){
            let json = JSON.parse(data);

            // On vérifie qu'il y a des shitstorms renvoyées
            if(json.count > 0){
                // Initialisation valeurs de base (+ reprise des anciennes)
                let string = '';
                let rand_id = 0;
                let week_number = Number(sessionStorage.getItem('currentWeekJournal'));
                let month_number = Number(sessionStorage.getItem('currentMonthJournal'));
                let begin = !Boolean(Number(sessionStorage.getItem('loadedShitJournal')));

                // Pour chaque shitstorm, on ajoute dans la chaîne à insérer l'html, un divider si besoin et un numéro de semaine / mois / année
                json.shitstorms.forEach((value) => {
                    if(!begin){
                        string += `<div class='clearb'></div><div class='col s12 l10 offset-l1'><div class='divider shitstorm-divider'></div></div><div class='clearb'></div>`;
                    }
                    else{
                        begin = false;
                    }

                    if(byYear){
                        if(value.month !== month_number){
                            month_number = value.month;
                            string += `<h1 id="month${month_number}" class="header">` + getTextualMonth(month_number) + (choosen_visibility === 'all' ? ' ' + value.year : '') + `</h1>`;
                        }

                        sessionStorage.setItem('currentMonthJournal', month_number);
                    }

                    if(value.week !== week_number){
                        week_number = value.week;
                        string += `<h2 id="week${week_number}" class="header">Semaine ${week_number}</h2>`;
                    }
                    
                    string += "<div class='col s12 l10 offset-l1'>" + value.html + "</div>";
                    rand_id = value.id;
                });

                // On insère dans le bloc
                shitb.innerHTML = string;

                sessionStorage.setItem('currentWeekJournal', week_number);

                $("#shitblock").find("script").each(function(){
                    eval($(this).text());
                }); // Lecture des scripts

                twttr.ready(() => {
                    showTwitterLinks();
                });
                showMastodonLinks();

                // Modification de l'ID de l'élément shitblock
                shitb.id = "shitb"+rand_id;

                // On crée un nouveau block vide pour le chargement suivant
                let g = document.createElement('div');
                g.setAttribute("id", "shitblock");

                // On crée le lien "voir plus" si jamais il y a plus à charger
                if(json.next_skip){
                    g.innerHTML = `
                        <div class='col s12 center flow-text' style='margin-top: 1em; margin-bottom: 1em;'>
                            <a class='btnshitload' href='#!' id='buttonshitstorm' onclick='getJournalShits("${curr_date}", false, "${view_mode}", "${shitstorm_order}", ${json.next_skip})'>Shitstorms suivantes</a>
                        </div>
                        <div class='clearb'></div>`;
                }

                // On insère l'élément dans le block conteneur
                document.getElementById('shitmainblock').appendChild(g);

                if(json.next_skip && !disable_auto_scroll){
                    $(`#buttonshitstorm`).scrollfire({
                        // Offsets
                        offset: 0,
                        topOffset: 0,
                        bottomOffset: 0,
                        // Fires once when element begins to come in from the bottom
                        onBottomIn: function(elm, distance_scrolled) {
                            getJournalShits(curr_date, false, view_mode, shitstorm_order, json.next_skip);
                        },
                    });
                }

                // On réinitialise fancybox pour l'affichage des images
                initFancyBox();
                initLikeButtons();

                // Si jamais le bloc était masqué, on le raffiche
                if(!$('#shitmainblock').is(':visible')){
                    let speed = 600;
                    if(json.count){
                        speed = json.count * 250;
                    }

                    $('#shitmainblock').slideDown(speed);
                }

                // On finalise en signalant que le chargement est terminé
                sessionStorage.setItem('loadedShitJournal', 1);

                if(until) { // Si il y avait un "load until", on charge la shitstorm concernée
                    loadShitstormInContainer(until);
                }
            }
            else{
                if(Number(sessionStorage.getItem('loadedShitJournal')) == 0){
                    shitb.innerHTML = `<h4 class='noShitstorm' style='margin-bottom: 1.5em;'>Il n'y a aucune shitstorm disponible pour l'intervalle sélectionné.</h4>`;
                }
                else{
                    shitb.innerHTML = '';
                }
            }

            if(json.infos){ // Si les infos sont à actualiser
                let nav = $('.navigation-journal');
                if(json.infos_get_all){ // Si on est mode 'tout voir'
                    document.getElementById('shit_number').innerHTML = (json.infos_count_total <= 1 ? (json.infos_count_total == 1 ? 'Une' : 'Aucune') : json.infos_count_total) + " shitstorm" +
                        (json.infos_count_total > 1 ? 's' : '') + ' au total.';

                    if(nav.is(':visible')){
                        nav.slideUp(200);
                    }
                }
                else{
                    // On est en vue avec un intervalle de date
                    $('.tooltipped').tooltip('remove');

                    if(!nav.is(':visible')){
                        nav.slideDown(200);
                    }

                    let prec = $('.prec-journal-button');
                    if(json.infos_prec_link){ // Si il y a un lien
                        $(".prec-journal-link").attr("href", json.infos_prec_link);
                        prec.attr('data-tooltip', json.infos_prec_text);
                        prec.addClass('tooltipped');
                        prec.removeClass('disabled');
                    }
                    else{
                        $(".prec-journal-link").attr("href", '#!');
                        prec.addClass('disabled');
                        prec.removeClass('tooltipped');
                    }
    
                    prec = $('.next-journal-button');
                    if(json.infos_next_link){ // Si il y a un lien dispo pour page suivante
                        $(".next-journal-link").attr("href", json.infos_next_link);
                        prec.attr('data-tooltip', json.infos_next_text);
                        prec.addClass('tooltipped');
                        prec.removeClass('disabled');
                    }
                    else{
                        $(".next-journal-link").attr("href", '#!');
                        prec.addClass('disabled');
                        prec.removeClass('tooltipped');
                    }
    
                    $('.current-journal-text').text(json.infos_current_text);
    
                    document.getElementById('shit_number').innerHTML = (json.infos_count_total <= 1 ? (json.infos_count_total == 1 ? 'Une' : 'Aucune') : json.infos_count_total) + " shitstorm" +
                        (json.infos_count_total > 1 ? 's' : '') + ' ce' + (json.infos_by_year ? 'tte année' : ' mois-ci') + '.';
    
                    $('#shit_number').show(140);
                }
            }

            $('.tooltipped').tooltip();
            sessionStorage.setItem('onLoadShitJournal', 0);
        },
        "text"
    )
    .fail(() => {
        Materialize.toast('<span style="font-weight: bold;" class="error">Erreur réseau</span>', 4000);

        var pre = document.getElementById('preloader');
        pre.parentElement.removeChild(pre);

        if(get_infos) {
            sessionStorage.setItem('getInfosJournal', 1);
            shitb.innerHTML = "<div class='flow-text center error'>Impossible de charger les shitstorms</div><div class='flow-text center'><a href='#!' onclick='refreshShitstorms()'>Réessayer</a></div>";

            // Si jamais le bloc était masqué, on le raffiche
            if(!$('#shitmainblock').is(':visible')){
                $('#shitmainblock').slideDown(200);
            }
        }
        if(!$(shitb).is(':visible')){
            $(shitb).show(220);
        }

        sessionStorage.setItem('onLoadShitJournal', 0);
    });
}

function reloadShitstorms(curr_date = 2018, by_year = false, view_mode = 'date', order = 'desc'){
    let button = $('#buttonshitstorm');
    if(button){
        button.scrollfire('remove');
    }
    $('#shitmainblock').slideUp(400, function () {
        generateJournalShits('shitmainblock', curr_date, by_year, view_mode, order);
    });
}

function refreshShitstorms() {
    reloadShitstorms(choosen_date, $('input[name="radio_year"]:checked').val() !== 'month', choosen_mode, choosen_order);
}

function checkForm() {
    let visibility = $('input[name="radio_year"]:checked').val();
    let byy = true;
    let mo = document.getElementById('selmonth');
    let mont = mo.value;
    if(mont < 10){
        mont = "0" + String(mont);
    }

    let ye = document.getElementById('selyear');
    let da = 'all';
    if(visibility === 'year'){ // Vue par année
        da = ye.value;
        mo.disabled = true;
        ye.disabled = false;
    }
    else if(visibility === 'month'){ // Vue par Moism
        da = ye.value + '-' + mont;
        byy = false;
        mo.disabled = false;
        ye.disabled = false;
    }
    else{ // Vue générale
        mo.disabled = true;
        ye.disabled = true;
    }

    let mod = document.getElementById('selmode').value;

    let ord = document.getElementById('selorder').value;

    if(da !== choosen_date || mod !== choosen_mode || ord !== choosen_order){
        choosen_visibility = visibility;

        if(da !== choosen_date || visibility !== choosen_visibility){
            sessionStorage.setItem('getInfosJournal', 1);
        }
        // Actualisation des select si changement
        refreshSelect();

        reloadShitstorms(da, byy, mod, ord);
    }
}

$(document).ready(function () {
    $("#selmode, #selorder, #selyear, #selmonth, input[name='radio_year']").on('change', function () {
        checkForm();
    });

    // Enregistrement de l'historique
    let current_state = {opened_container: false};
    window.history.replaceState(current_state, "");

    // Historique précédent
    window.onpopstate = function(event) {
        // Restaure la page à où elle en était
        if(event.state && "opened_container" in event.state) {
            if(event.state.opened_container) {
                $(function () {
                    openShitstorm();
                });
                let cont = document.getElementById('shitstorm-side-container');

                if("container" in event.state && "id_shitstorm" in event.state) {
                    cont.innerHTML = event.state.container;
                    cont.dataset.currentlyLoaded = event.state.id_shitstorm;
        
                    $(cont).find("script").each(function() {
                        eval($(this).text());
                    });
        
                    $('.tooltipped').tooltip();
                    showTwitterLinks();
                    showMastodonLinks();
                    initLikeButtons();
    
                    $('ul.tabs').tabs(
                        {'swipeable': false}
                    );
                    
                    initCommentTextAera();
                }
            }
            else {
                $(function () {
                    closeShitstorm(true);
                });
            }
        }
    };
});
