function initAutoScrollButton() {
    let button_scroll = document.getElementById('disable_auto_scroll_button');
    if(button_scroll) {
        let current_opt = 0;

        if(localStorage.getItem('disable_auto_scroll') !== null){
            if(Number(localStorage.getItem('disable_auto_scroll')) === 1){
                current_opt = 1;
            }
        }
    
        if(current_opt){
            $(button_scroll).addClass('light-blue');
            button_scroll.dataset.value = 1;
            button_scroll.innerHTML = ''
        }
        else{
            $(button_scroll).addClass('cyan');
            button_scroll.dataset.value = 0;
            button_scroll.innerHTML = 'Dés'
        }
    
        button_scroll.innerHTML += 'activer le chargement continu des shitstorms';
    
        button_scroll.onclick = function() {
            let val = Number(this.dataset.value);
    
            if(val === 1){
                localStorage.setItem('disable_auto_scroll', 0);
    
                $(this).removeClass('light-blue');
                $(this).addClass('cyan');
                this.dataset.value = 0;
                this.innerHTML = 'Dés';
    
                Materialize.toast('Chargement automatique activé.', 6000, 'rounded');
            }
            else{
                localStorage.setItem('disable_auto_scroll', 1);
    
                $(this).addClass('light-blue');
                $(this).removeClass('cyan');
                this.dataset.value = 1;
                this.innerHTML = '';
                Materialize.toast('Chargement automatique désactivé.', 6000, 'rounded');
            }
            this.innerHTML += 'activer le chargement continu des shitstorms';
        }
    }
    
}
