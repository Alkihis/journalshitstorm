function createDeleteLinkModal(idModal, idSub, idLink, isWait){
    document.getElementById(idModal).innerHTML = `
        <div class='modal-content'>
            <h4>Voulez-vous vraiment supprimer ce lien ?</h4>
        </div>
        <div class='modal-footer' style='padding-left: 1.5em;'>
            <a id='clickDelete' href='#!' class='modal-action modal-close waves-effect waves-green btn-flat col s3 l1 red lighten-1 left white-text'>
                <i class='material-icons left'>clear</i>Oui
            </a>
            <a href='#!' class='modal-action modal-close waves-effect waves-green btn-flat col s3 l1 blue lighten-1 left white-text' style='margin-left : 2em;'>
                <i class='material-icons left'>keyboard_capslock</i>Non
            </a>
            <div class='clearb'></div>
        </div>`;
    document.getElementById('clickDelete').onclick = () => {deleteLinkFromShitstorm(idSub, idLink, isWait);};
    $('#'+idModal).modal('open');
}

function deleteLinkFromShitstorm(idSub, idLink, isWait) {
    $.post(
        https_url+"api/v2/edit/"+ (isWait ? 'waiting_' : '') +"shitstorm.json",
        {remove_link: idLink, id: idSub},
        "text"
    )
    .done(() => {
        document.getElementById("linkID"+idLink).remove();
        if(countLinks() < 5){
            $('#add_link').show();
        }
        else{
            $('#add_link').hide();
        }
    })
    .fail((data) => {
        console.log("Error data : ", data.responseJSON);
        Materialize.toast('Impossible de supprimer le lien.', 6000, 'rounded');
    });
}

function countLinks(){
    return $('#link_collapsible li').length;
}
