$(document).ready(function () {
    $('.dropdown-button').dropdown({
        hover: true, // Activate on hover
        stopPropagation: true,
        constrainWidth: false,
      }
    );

    $("#selecttype").on('change', function () {
        if(sort_type != this.value){
            sort_type = this.value;
            initComment(this.dataset.sub);
        }
    });

    $("#selectsort").on('change', function () {
        if(sorting_comments != this.value){
            sorting_comments = this.value;
            initComment(this.dataset.sub);
        }
    });

    $('.dropdown-button + .dropdown-content').on('click', function(event) {
        event.stopPropagation();
    });

    $('.follow-activate').on('change', function() {
        let id = this.dataset.idsub;

        let checkBoxFollow = document.getElementById('followed_shit'+id);
        let checkBoxMail = document.getElementById('followed_shit_with_mail'+id);

        let title = document.getElementById('follow_shit_drop'+id).dataset.title;

        let buttonFollow = document.getElementById('follow_shit_button'+id);
        let bellFollow = document.getElementById('follow_shit_bell'+id);

        let dic = {};
        let mode = "";

        if(checkBoxMail.checked && !checkBoxFollow.checked && $(this).hasClass('email-shitstorm-checkbox')){
            // Si on a appuyé sur la checkbox email (on l'active), et que la checkbox suivre est décochée
            checkBoxFollow.checked = true;
        }

        if(checkBoxFollow.checked) {
            mode = "create";
            dic = {id: id, with_mail: checkBoxMail.checked};
        }
        else {
            mode = "destroy";
            dic = {id: id};
            checkBoxMail.checked = false;
        }
        $.post(
            https_url+"api/v2/subscriptions/"+ mode +".json",
            dic,
            function(data){
                let json = JSON.parse(data);
                if("subscribed" in json){
                    let ac = json.subscribed;
                    
                    if(ac){
                        let w_mail = json.subscribe_with_mail;
                        if(w_mail){
                            checkBoxMail.checked = true;
                            Materialize.toast('Notifications et mails activés pour '+ title + '.', 6000, 'rounded');
                        }
                        else{
                            checkBoxMail.checked = false;
                            Materialize.toast('Notifications activées pour '+ title + '.', 6000, 'rounded');
                        }
                        checkBoxFollow.checked = true;
                        checkBoxFollow.labels[0].innerHTML = 'Suivie';
                        $(buttonFollow).removeClass('grey');
                        $(buttonFollow).addClass('green');
                        bellFollow.innerHTML = 'notifications';
                    }
                    else{
                        checkBoxFollow.checked = false;
                        checkBoxMail.checked = false;
                        checkBoxFollow.labels[0].innerHTML = 'Suivre';
                        Materialize.toast('Notifications désactivées pour '+ title + '.', 6000, 'rounded');
                        $(buttonFollow).addClass('grey');
                        $(buttonFollow).removeClass('green');
                        bellFollow.innerHTML = 'notifications_off';
                    }
                }
                else{
                    console.log("Erreur : JS ", json);
                    Materialize.toast('Impossible d\'effectuer l\'action.', 6000, 'rounded');
                }
            },
            "text"
        ).fail((i, e, a) => {
            console.error(i, e, a);
            Materialize.toast('Impossible d\'effectuer l\'action.', 6000, 'rounded');
        });
    });
});

