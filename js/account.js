// *****************
// ** ENABLE LABS **
// *****************

function enableDisableLab() {
    // if(localStorage.getItem('new_shitstorm_loading') === 'on') {
    //     disableLab();
    // }
    // else {
    //     enableLab();
    // }

    Materialize.toast('Aucune fonctionnalité n\'est disponible', 6000, 'rounded');
}

function enableLab() {
    // localStorage.setItem('new_shitstorm_loading', 'on');
    Materialize.toast('Fonctionnalité de test activée', 6000, 'rounded');
}

function disableLab() {
    // localStorage.setItem('new_shitstorm_loading', 'off');
    Materialize.toast('Fonctionnalité de test désactivée', 6000, 'rounded');
}


// ***********
// ** EMAIL **
// ***********

function confirmEmail(){
    var jqxhr = $.post(
        https_url+"api/v2/account/confirm_email.json",
        {},
        function(data){
            var json = JSON.parse(data);
            if("confirmation_mail" in json){
                Materialize.toast('L\'e-mail de confirmation a bien été envoyé. Vérifiez vos spams si l\'e-mail n\'apparaît pas dans votre boîte de réception.', 12000, 'rounded');
            }
            else {
                Materialize.toast('Une erreur est survenue. Réessayez ultérieurement.', 6000, 'rounded');
            }
        },
        "text"
    ) .fail(function(jqXHR, textStatus, errorThrown) {
            var json = JSON.parse(jqXHR.responseText);

            if(("error_code" in json && json.error_code == 26) || jqXHR.status === 429){
                Materialize.toast('Vous avez déjà demandé un e-mail de confirmation récemment. Réessayez plus tard.', 10000, 'rounded');
            }
            else if("error_code" in json){
                if(json.error_code == 25){
                    Materialize.toast('Votre e-mail est déjà confirmé.', 10000, 'rounded');
                }
                else {
                    Materialize.toast('Impossible d\'envoyer l\'e-mail. Avez-vous défini une adresse ?', 10000, 'rounded');
                }
            }
            else {
                Materialize.toast('Une erreur est survenue. Réessayez ultérieurement.', 6000, 'rounded');
            }
        });
}

// ***********************
// ** ATTRIBUTS VISUELS **
// ***********************

function changePerPage(){
    let newPerPage = Number($('#perpagecount option:selected').val());

    if(newPerPage > 0 && newPerPage <= 25) {
        localStorage.setItem('count_shitstorm_per_load', newPerPage);
        Materialize.toast('Il y aura désormais ' + newPerPage + ' publication'+ (newPerPage > 1 ? 's' : '') 
        +' affichée'+ (newPerPage > 1 ? 's' : '') +' par chargement.', 6000, 'rounded');
    }
    else {
        Materialize.toast('<span class="bold error">Votre choix est invalide.</span>', 6000, 'rounded');
    }
}

function changeStormMode(newStorm){
    newStorm = String(newStorm);

    var jqxhr = $.post(
        https_url+"api/v2/account/visual_status.json",
        {storm_mode: newStorm},
        function(data){
            var json = JSON.parse(data);
            if("have_storm" in json){
                var nv = 0;
                let str = '';
                let mode = '';

                if(!json.have_storm){
                    nv = 1;
                    str += 'Activer';
                    mode = 'désactivés';

                    $("#stormbtn").removeClass('amber darken-3');
                    $("#stormbtn").addClass('indigo darken-4');
                }
                else {
                    str += 'Désactiver';
                    mode = 'activés';

                    $("#stormbtn").addClass('amber darken-3');
                    $("#stormbtn").removeClass('indigo darken-4');
                }

                let button = document.getElementById('stormbtn');
                button.onclick = function(){ changeStormMode(nv) };
                button.innerHTML = str + " les éclairs";

                Materialize.toast('Les éclairs sur le journal sont désormais ' + mode + '.', 6000, 'rounded');
            }
        },
        "text"
    ) .fail(function(jqXHR, textStatus, errorThrown) {
        console.log("Error : " + jqXHR.responseText);
    });
}

function changeSnowMode(newSnow){
    newSnow = String(newSnow);

    var jqxhr = $.post(
        https_url+"api/v2/account/visual_status.json",
        {snow_mode: newSnow},
        function(data){
            var json = JSON.parse(data);

            if("have_snow" in json){
                var nv = 0;
                let str = '';
                let mode = '';

                if(!json.have_snow){
                    nv = 1;
                    str += 'Activer';
                    mode = 'dés';

                    $("#main-block").removeClass('snow-main');

                    $("#snowbtn").addClass('blue');
                    $("#snowbtn").removeClass('purple');
                }
                else {
                    str += 'Désactiver';

                    $("#main-block").addClass('snow-main');

                    $("#snowbtn").addClass('purple');
                    $("#snowbtn").removeClass('blue');
                }

                let button = document.getElementById('snowbtn');
                button.onclick = function(){ changeSnowMode(nv) };
                button.innerHTML = str + " la neige";

                Materialize.toast('La neige sur le site est désormais ' + mode + 'activée.', 6000, 'rounded');
            }
        },
        "text"
    ) .fail(function(jqXHR, textStatus, errorThrown) {
        console.log("Error : " + jqXHR.responseText);
    });
}

// *************************
// ** ATTRIBUTS DU COMPTE **
// *************************

function changeAttributs(){ // Change username, realname and mail
    let newMail = $('#mail').val();
    let newUsr = $('#usrname').val();
    let newRN = $('#realname').val();
    let newGender = $('#gender').val();

    var jqxhr = $.post(
        https_url+"api/v2/account/update.json",
        {mail: newMail, realname: newRN, username: newUsr, gender: newGender},
        function(data){
            let json = JSON.parse(data);

            if("gender" in json && "username" in json && "realname" in json){
                document.getElementById('textAccount').innerHTML = json.username + ' (' + htmlEntities(json.realname) + ')';
            }

            Materialize.toast('Les modifications ont été prises en compte.', 6000, 'rounded');
        },
        "text"
    ) .fail(function(jqXHR, textStatus, errorThrown) {
        pass = false;
        console.log("Data : ", JSON.parse(jqXHR.responseText));
        let json = JSON.parse(jqXHR.responseText);
        
        // Cas des emails
        if(json.error_code == 23){
            Materialize.toast('Cette adresse e-mail est déjà utilisée.', 6000, 'rounded');
        }
        else if (json.error_code == 19) {
            Materialize.toast('Le format de l\'e-mail est incorrect.', 6000, 'rounded');
        }

        // Realname
        if(json.error_code == 21){
            Materialize.toast('Le nom d\'usage comporte des caractères invalides.', 6000, 'rounded');
        }

        // Username
        if(json.error_code == 22){
            Materialize.toast('Le nom d\'utilisateur est réservé ou est déjà utilisé.', 6000, 'rounded');
        }
        else if (json.error_code == 18) {
            Materialize.toast('Le nom d\'utilisateur est déjà utilisé.', 6000, 'rounded');
        }

        // Gender
        if(json.error_code == 20){
            Materialize.toast('Le genre/pronom choisi est invalide.', 6000, 'rounded');
        }
    });
}

// *******************************
// ** IMAGES PROFIL ET BANNIERE **
// *******************************

var lastAccountStatusCode = 200;

function setDefaultImg(){
    $.post(
        https_url+"api/v2/profile/picture/delete.json",
        {},
        function(data){
            let json_html = JSON.parse(data);

            if('full_profile_img' in json_html){
                // Changement dans les menus sidenav
                $('.side-nav-pp').attr('src', json_html.full_profile_img);

                $('#modaldelpp').modal('close');

                Materialize.toast('Votre image a réinitialisée.', 6000, 'rounded');
            }
        },
        "text"
    ) .fail(function(jqXHR) {
        console.log("Error : " + jqXHR.responseText);
        Materialize.toast('Impossible de sauvegarder vos paramètres.', 6000, 'rounded');
    });
}

function setDefaultBanner(){
    $.post(
        https_url+"api/v2/profile/banner/delete.json",
        {},
        function(data){
            let json_html = JSON.parse(data);

            if('banner_img' in json_html){
                $('#modaldelbanner').modal('close');

                document.getElementById('banner_profile_user_style').innerHTML = '';

                Materialize.toast('Votre bannière a été réinitialisée.', 6000, 'rounded');
            }
        },
        "text"
    ) .fail(function(jqXHR) {
        console.log("Error : " + jqXHR.responseText);
        Materialize.toast('Impossible de sauvegarder vos paramètres.', 6000, 'rounded');
    });
}

var $uploadCrop;

function readFile(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();          
        reader.onload = function (e) {
            $uploadCrop.croppie('bind', {
                url: e.target.result
            });
            $('#upload-demo').addClass('ready');
            $('#upload-demo').show();
        }           
        reader.readAsDataURL(input.files[0]);
    }
}

function changeImg(){
    let img = document.getElementById('profile_img');
    var idModal = "modalplaceholder";
    let img_src = img.getAttribute('src');

    var modal = document.getElementById(idModal);

    let string = `
    <form id='file' name="fileinfo">
        <div class="modal-content">
            <img width="75" height='75' style='float: left; border-radius: 5px; margin-bottom: 1em;' src="${img_src}">
            <h4 class='annonceMois' style='text-align: center; vertical-align: sub;'>Modifier l'image de profil</h4>
            <input id='imagebase64' name="imagebase64" value='' type="hidden">
            <div class='clearb'></div>
            <div class='divider'></div>
            <div id="upload-demo" style='display: none; border-radius: 15px; margin-top: 15px;'></div>

            <div class="file-field input-field">
                <input type='hidden' name='MAX_FILE_SIZE' value='1048576'>
                <div class="btn">
                    <span>Image</span>
                    <input id="myfile" type="file" name='new_img_profile' accept="image/png, image/jpeg" required>
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text" placeholder="1 Mo max, type PNG/JPG">
                </div>
            </div>
        </div>
        <div class='clearb'></div>
        <div class="modal-footer">
            <button type="submit" id='saveimgpro' name='saveimgpro' class="form_submit waves-effect waves-green btn-flat right" style="margin-bottom: 1em">Modifier</button>
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Annuler</a>
        </div>
    </form>`;

    modal.innerHTML = string;

    let wid = 400;
    let hei = 300;

    if($(window).width() < 996){ // Mode mobile
        wid = 250;
        hei = 210;
    }

    $uploadCrop = $('#upload-demo').croppie({
        enableExif: true,
        viewport: {
            width: 200,
            height: 200,
            type: 'square',
        },
        boundary: {
            width: wid,
            height: hei
        }
        });

    $('#myfile').on('change', function () { readFile(this); });
    $('.form_submit').on('click', function (ev) {
        $uploadCrop.croppie('result', {
            type: 'canvas',
            format: 'jpeg',
            size: { width: 200, height: 200 }
        }).then(function (resp) {
            document.getElementById('imagebase64').value = resp;

            changeProfileAPICall("media_data", document.getElementById('imagebase64').value, idModal, 'Impossible d\'importer l\'image. Vérifiez que celle-ci fait moins de 1 Mo.');
        });
        return false;
    });

    $('#'+idModal).modal('open');
}

function changeProfileAPICall(par_name, par_value, idModal = null, errorMsg = '', fromTwitter = false) {
    var dic = {};
    dic[par_name] = par_value;
    let endpoint = (fromTwitter ? 'from_twitter.json' : 'update.json');
    return $.post(
        https_url+"api/v2/profile/picture/" + endpoint,
        dic,
        function(data){
            lastAccountStatusCode = 200;
            let json_html = JSON.parse(data);

            if('full_profile_img' in json_html){
                document.getElementById('profile_img').setAttribute('src', json_html.full_profile_img);

                // Changement dans les menus sidenav
                $('.side-nav-pp').attr('src', json_html.full_profile_img);

                if(idModal) {
                    $('#'+idModal).modal('close');
                }

                Materialize.toast('Votre image a été mise à jour avec succès.', 6000, 'rounded');
            }
        },
        "text"
    ) .fail(function(jqXHR, textStatus, errorThrown) {
        console.log("Error : " + jqXHR.responseText);
        Materialize.toast(errorMsg, 6000, 'rounded');
        lastAccountStatusCode = jqXHR.status;
    });
}

function setTwitterProfileImg() { // Ecrase l'image de profil actuelle !
    Materialize.toast('Téléchargement de l\'image depuis votre compte Twitter...', 6000, 'rounded');
    changeProfileAPICall('none', true, null, 'Impossible d\'importer l\'image. Vérifiez que votre compte Twitter est associé et a une image de profil.', true);
}

function changeBannerImg(){
    var idModal = "modalplaceholder";

    var modal = document.getElementById(idModal);

    let string = `
    <form id='file' name="fileinfo">
        <div class="modal-content">
            <h4 class='annonceMois' style='text-align: center; vertical-align: sub;'>Modifier la bannière</h4>
            <input id='imagebase64_banner' name="imagebase64_banner" value='' type="hidden">
            <div class='clearb'></div>
            <div class='divider'></div>
            <div id="upload-demo" style='display: none; border-radius: 15px; margin-top: 15px;'></div>

            <div class="file-field input-field">
                <input type='hidden' name='MAX_FILE_SIZE' value='1048576'>
                <div class="btn">
                    <span>Image</span>
                    <input id="file_banner" type="file" name='new_img_banner' accept="image/png, image/jpeg" required>
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text" placeholder="2 Mo max, type PNG/JPG">
                </div>
            </div>
        </div>
        <div class='clearb'></div>
        <div class="modal-footer">
            <button type="submit" id='saveimgpro' name='saveimgpro' class="form_submit_banner waves-effect waves-green btn-flat right" style="margin-bottom: 1em">Modifier l'image</button>
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Annuler</a>
        </div>
    </form>`;

    modal.innerHTML = string;

    let wid = 550;
    let hei = 300;
    let vwid = 500;
    let vhei = 175;

    if($(window).width() < 996){ // Mode mobile
        wid = 250;
        hei = 210;

        vwid = 230;
        vhei = 80;
    }

    $uploadCrop = $('#upload-demo').croppie({
        enableExif: true,
        viewport: {
            width: vwid,
            height: vhei,
            type: 'square',
        },
        boundary: {
            width: wid,
            height: hei
        }
    });

    $('#file_banner').on('change', function () { readFile(this); });
    $('.form_submit_banner').on('click', function (ev) {
        $uploadCrop.croppie('result', {
            type: 'canvas',
            format: 'jpeg',
            size: { width: 2000, height: 700 }
        }).then(function (resp) {
            document.getElementById('imagebase64_banner').value = resp;

            changeBannerAPICall("media_data", document.getElementById('imagebase64_banner').value, idModal, 'Impossible d\'importer l\'image. Vérifiez que celle-ci fait moins de 2 Mo.');
        });
        return false;
    });

    $('#'+idModal).modal('open');
}

function changeBannerAPICall(par_name, par_value, idModal = null, errorMsg = '', fromTwitter = false) {
    var dic = {};
    dic[par_name] = par_value;
    let endpoint = (fromTwitter ? 'from_twitter.json' : 'update.json');
    return $.post(
        https_url+"api/v2/profile/banner/" + endpoint,
        dic,
        function(data){
            lastAccountStatusCode = 200;
            let json_html = JSON.parse(data);

            if('full_banner_img' in json_html){
                if(idModal) {
                    $('#'+idModal).modal('close');
                }

                document.getElementById('banner_profile_user_style').innerHTML = `
                    .side-nav .user-view .background, .side-nav .userView .background, .banner-background{
                        background-image: url("${json_html.full_banner_img}");
                    }`;

                Materialize.toast('Votre bannière a été mise à jour avec succès.', 6000, 'rounded');
            }
        },
        "text"
    ) .fail(function(jqXHR, textStatus, errorThrown) {
        console.log("Error : " + jqXHR.responseText + ' s:' + textStatus + ' e:' + errorThrown);
        Materialize.toast(errorMsg, 6000, 'rounded');
        lastAccountStatusCode = jqXHR.status;
    });
}

function setTwitterBannerImg() { // Ecrase l'image de bannière actuelle !
    Materialize.toast('Téléchargement de l\'image depuis votre compte Twitter...', 6000, 'rounded');
    changeBannerAPICall('none', true, null, 'Impossible d\'importer l\'image. Vérifiez que votre compte Twitter est associé et a une bannière.', true);
}

// *****************
// ** SUIVABILITE **
// *****************

function changeFollowVisiblity(newVisibility){
    newVisibility = String(newVisibility);

    var jqxhr = $.post(
        https_url+"api/v2/account/follow_status.json",
        {accept_follow: newVisibility},
        function(data){
            var json = JSON.parse(data);
            if("followable" in json){
                var nv = 'hidden';
                let str = '';
                let text_h = document.getElementById('follow_status_text');

                if(!json.followable){
                    nv = 'followable';
                    str += 'Me rendre suivable';
                    text_h.innerHTML = "Voulez-vous activer les abonnements à votre compte ?";

                    $("#followbtn").removeClass('grey');
                    $("#followbtn").addClass('green');
                    $('#follow_status_par').hide();
                }
                else {
                    str += 'Empêcher les abonnements à mon profil';
                    text_h.innerHTML = "Voulez-vous désactiver les abonnements à votre compte ?";

                    $("#followbtn").removeClass('green');
                    $("#followbtn").addClass('grey');
                    $('#follow_status_par').show();
                }

                let button = document.getElementById('followbtn_confirm_modal');
                button.onclick = function(){ changeFollowVisiblity(nv) };
                document.getElementById('followbtn').innerHTML = str;

                Materialize.toast('Vous '+ (json.followable ? 'acceptez' : 'refusez') +' désormais les abonnements à votre profil.', 6000, 'rounded');
            }
        },
        "text"
    ) .fail(function(jqXHR, textStatus, errorThrown) {
        console.log("Error : " + jqXHR.responseText);
    });
}

// ***********************
// ** BOUTON AUTOSCROLL **
// ***********************

function initAutoScrollButton() {
    let button_scroll = document.getElementById('disable_auto_scroll_button');
    if(button_scroll) {
        let current_opt = 0;

        if(localStorage.getItem('disable_auto_scroll') !== null){
            if(Number(localStorage.getItem('disable_auto_scroll')) === 1){
                current_opt = 1;
            }
        }
    
        if(current_opt){
            $(button_scroll).addClass('light-blue');
            button_scroll.dataset.value = 1;
            button_scroll.innerHTML = ''
        }
        else{
            $(button_scroll).addClass('cyan');
            button_scroll.dataset.value = 0;
            button_scroll.innerHTML = 'Dés'
        }
    
        button_scroll.innerHTML += 'activer le chargement continu des éléments';
    
        button_scroll.onclick = function() {
            let val = Number(this.dataset.value);
    
            if(val === 1){
                localStorage.setItem('disable_auto_scroll', 0);
    
                $(this).removeClass('light-blue');
                $(this).addClass('cyan');
                this.dataset.value = 0;
                this.innerHTML = 'Dés';
    
                Materialize.toast('Chargement automatique activé.', 6000, 'rounded');
            }
            else{
                localStorage.setItem('disable_auto_scroll', 1);
    
                $(this).addClass('light-blue');
                $(this).removeClass('cyan');
                this.dataset.value = 1;
                this.innerHTML = '';
                Materialize.toast('Chargement automatique désactivé.', 6000, 'rounded');
            }
            this.innerHTML += 'activer le chargement continu des éléments';
        }
    }
}


// *******************************
// ** SUPPRIMER UNE APPLICATION **
// *******************************

function deleteApplication(documentFragment) {
    let app_id = documentFragment.dataset.appId;

    if(app_id) {
        $.post(
            https_url+"api/v2/account/remove_application.json",
            {application_id: app_id},
            function(data){
                var json = JSON.parse(data);
                if("deleted_application" in json){
                    $('.application-description[data-app-id='+ json.deleted_application +']').hide(230, function () {
                        $(this).remove();
                    });
                }
            },
            "text"
        ) .fail(function(jqXHR, textStatus, errorThrown) {
            Materialize.toast('<span class="error">Une erreur est survenue</span>');
        });
    }
    else {
        console.log('app_id is not valid : ' + app_id);
    }
}

// *********************
// ** CHANGER DE PAGE **
// *********************

/**
 * Load the following page in the page container
 * @param {int} page_number 
 */
function changeAccountPage(page_number) {
    turnContainerOnOff(false);
    var co = document.getElementById('account-container');
    var pr = document.getElementById('profile-modification-container');

    co.innerHTML = preloader;
    pr.innerHTML = '';
    page_number = Number(page_number);

    $.get(
        https_url+"api/v2/generation/account_pages.json",
        {page: page_number},
        function(data) {
            data = JSON.parse(data);

            if("html" in data) {
                if(page_number != 3 && page_number != 4) {
                    co.innerHTML = data.html;
                    pr.innerHTML = '';
                    turnContainerOnOff(true);
                }
                else {
                    co.innerHTML = '';
                    pr.innerHTML = data.html;
                    turnContainerOnOff(false);
                }
    
                let nav_element = document.getElementById('navbar_2');
    
                let current_state = {acc_contain: co.innerHTML, pro_contain: pr.innerHTML, pg_number: page_number, container: isContainerActive()};
    
                switch(page_number) {
                    case 0:
                        nav_element.innerHTML = current_state.name = 'Propriétés';
                        nav_element.href = current_state.href = https_url + 'compte/';
                        window.history.pushState(current_state, "Compte utilisateur - Propriétés", "/compte/");
                        break;
                    case 1:
                        nav_element.innerHTML = current_state.name = 'Mot de passe';
                        nav_element.href = current_state.href = https_url + 'compte/password';
                        window.history.pushState(current_state, "Compte utilisateur - Mot de passe", "/compte/password");
                        break;
                    case 2:
                        nav_element.innerHTML = current_state.name = 'Visuel';
                        nav_element.href = current_state.href = https_url + 'compte/visual';
                        window.history.pushState(current_state, "Compte utilisateur - Visuel", "/compte/visual");
                        break;
                    case 3:
                        nav_element.innerHTML = current_state.name = 'Modification profil';
                        nav_element.href = current_state.href = https_url + 'compte/profile';
                        window.history.pushState(current_state, "Compte utilisateur - Modification du profil", "/compte/profile");
                        break;
                    case 4:
                        nav_element.innerHTML = current_state.name = 'Commentaires';
                        nav_element.href = current_state.href = https_url + 'compte/mysubs';
                        window.history.pushState(current_state, "Compte utilisateur - Commentaires", "/compte/mysubs");
                        break;
                    case 5:
                        nav_element.innerHTML = current_state.name = 'Autres';
                        nav_element.href = current_state.href = https_url + 'compte/others';
                        window.history.pushState(current_state, "Compte utilisateur - Autres", "/compte/others");
                        break;
                    case 6:
                        nav_element.innerHTML = current_state.name = 'Compte Twitter';
                        nav_element.href = current_state.href = https_url + 'compte/twitter';
                        window.history.pushState(current_state, "Compte utilisateur - Compte Twitter", "/compte/twitter");
                        break;
                    case 7:
                        nav_element.innerHTML = current_state.name = 'Applications liées';
                        nav_element.href = current_state.href = https_url + 'compte/apps';
                        window.history.pushState(current_state, "Compte utilisateur - Applications liées", "/compte/apps");
                        break;
                }
    
                $('select').material_select();
                $('.tooltipped').tooltip();
                Materialize.updateTextFields();
                $('input#input_text, textarea#textarea1').characterCounter();
                if(page_number == 2) {
                    initAutoScrollButton();
                }
            }
        },
        "text"
    )
    .fail(() => {
        co.innerHTML = `<h5 class='error center' style='margin-bottom: 1.5em;'>Impossible de charger la page.</h5>`;
    });
}

$(document).ready(function () {
    $.initialize('#view_mode', function() {
        this.onchange = function() {
            localStorage.setItem('shitstorm_view_mode', this.value);
            Materialize.toast('Votre préférence a été enregistrée', 8000);
        };

        if(localStorage.getItem('shitstorm_view_mode')) { // Si l'utilisateur a un mode déjà choisi
            // Il faut actualiser le select
            this.value = localStorage.getItem('shitstorm_view_mode');
            refreshSelect();
        }
    });

    var pr_c = document.getElementById('profile-modification-container');
    var ac_c = document.getElementById('account-container');

    initAutoScrollButton();
    $('.account-menu-element').on('click', function () {
        let page_number = this.dataset.ele;
        if($(this).hasClass('active')) {
            return;
        }

        $('.account-menu-element').removeClass('active');
        $(this).addClass('active');

        changeAccountPage(page_number);
    });

    let page_number = $(".account-menu-element.active").attr("data-ele");
    let navbar_element = document.getElementById('navbar_2');

    // Sauvegarde de l'état initial de la page
    let current_state = {acc_contain: ac_c.innerHTML, pro_contain: pr_c.innerHTML, 
                         pg_number: page_number, container: isContainerActive(),
                         href: navbar_element.href, name: navbar_element.innerHTML};
    window.history.replaceState(current_state, "");

    window.onpopstate = function(event) {
        // Restaure la page à où elle en était
        if(event.state && "container" in event.state) {
            if("acc_contain" in event.state) {
                ac_c.innerHTML = event.state.acc_contain;
            }
            if("pro_contain" in event.state) {
                pr_c.innerHTML = event.state.pro_contain;
            }
            $('.account-menu-element').removeClass('active');
            if("pg_number" in event.state) {
                $('.account-menu-element[data-ele="'+ event.state.pg_number +'"]').addClass('active');
                if(Number(event.state.pg_number) == 2) {
                    initAutoScrollButton();
                }
            }
            if("href" in event.state) {
                navbar_element.href = event.state.href;
            }
            if("name" in event.state) {
                navbar_element.innerHTML = event.state.name;
            }

            turnContainerOnOff(event.state.container);

            $('select').material_select();
            $('.tooltipped').tooltip();
            Materialize.updateTextFields();
        }
    };
});
