function modifyUser(id) {
    console.log(id);

    let modal = document.getElementById('modal-user-modification');

    let realname = document.getElementById('modal-realname');
    let mail = document.getElementById('modal-mail');
    let password = document.getElementById('modal-password');
    let gender = document.getElementById('modal-gender');
    let conf = document.getElementById('modal-confirmed-mail');
    let form = document.getElementById('modal-form');
    let sta = document.getElementById('modal-status');

    $.get(
        https_url+"api/v2/admin/users.json",
        {mode: 'id', id: id},
        (data) => {
            var json = JSON.parse(data);

            form.dataset.id = json.id;

            conf.checked = (json.email_confirmed ? 'true' : false);

            $(gender).val(json.gender);

            password.value = '';

            mail.value = (json.email_address ? json.email_address : '');

            realname.value = json.realname;

            sta.value = json.status;

            $(modal).modal('open');

            $('select').material_select();
            Materialize.updateTextFields();
        },
        "text"
    ).fail((jx) => {
        Materialize.toast('Impossible de charger (' + JSON.parse(jx.responseText).message + ')', 10000); 
    });
}

function modifyAttibutes() {
    let realname = document.getElementById('modal-realname');
    let mail = document.getElementById('modal-mail');
    let gender = document.getElementById('modal-gender');
    let id = document.getElementById('modal-form').dataset.id;

    $.post(
        https_url+"api/v2/admin/attributes.json",
        {id: id, mail: mail.value, realname: realname.value, gender: $(gender).val()},
        (data) => {
            Materialize.toast('Actualisation réalisée avec succès', 10000); 
        },
        "text"
    ).fail((jx) => {
        Materialize.toast('Impossible d\'actualiser (' + JSON.parse(jx.responseText).message + ')', 10000); 
    });
}

function modifyPassword() {
    let password = document.getElementById('modal-password');
    let id = document.getElementById('modal-form').dataset.id;

    $.post(
        https_url+"api/v2/admin/new_password.json",
        {id: id, password: password.value},
        (data) => {
            Materialize.toast('Actualisation réalisée avec succès', 10000); 
        },
        "text"
    ).fail((jx) => {
        Materialize.toast('Impossible d\'actualiser (' + JSON.parse(jx.responseText).message + ')', 10000); 
    });
}

function modifyConfirm() {
    let conf = document.getElementById('modal-confirmed-mail');
    let id = document.getElementById('modal-form').dataset.id;

    $.post(
        https_url+"api/v2/admin/confirmed_mail.json",
        {id: id, confirmed: conf.checked},
        (data) => {
            Materialize.toast('Actualisation réalisée avec succès', 10000); 
        },
        "text"
    ).fail((jx) => {
        Materialize.toast('Impossible d\'actualiser (' + JSON.parse(jx.responseText).message + ')', 10000); 
    });
}

function modifyStatus() {
    let sta = document.getElementById('modal-status');
    let id = document.getElementById('modal-form').dataset.id;

    $.post(
        https_url+"api/v2/admin/status.json",
        {id: id, status: sta.value},
        (data) => {
            Materialize.toast('Actualisation réalisée avec succès', 10000); 
        },
        "text"
    ).fail((jx) => {
        Materialize.toast('Impossible d\'actualiser (' + JSON.parse(jx.responseText).message + ')', 10000); 
    });
}


function loadUsers(since = 0, filter = '') {
    let placeholder = document.getElementById('user-admin-placeholder');

    let pre = document.createElement('div');
    pre.innerHTML = preloader;
    pre.id = 'preloader-user';

    placeholder.appendChild(pre);

    $.get(
        https_url+"api/v2/admin/users.json",
        {mode: 'list', since_id: since, filter: filter},
        (data) => {
            var json = JSON.parse(data);

            let next_button = document.getElementById('next-user-button');

            if(next_button) {
                $(next_button).remove();
            }

            let preloader_icon = document.getElementById('preloader-user');

            if(preloader_icon) {
                $(preloader_icon).remove();
            }

            if("users" in json) {
                json.users.forEach(function (e) {
                    let d = document.createElement('div');
                    d.dataset.id = e.id;
                    d.innerHTML = `<p class='flow-text'>
                        <span class='username-user'>${e.username}</span> <span class='realname-user'>(${htmlSpeChars(e.realname)})</span>
                    </p>
                    <div class='status-user black-text left'>${e.status_str}</div><div class='modify-user card-pointer underline-hover black-text right'>Modifier</div>
                    <div class='clearb'></div><div class='divider divider-user'></div>`;
                    placeholder.appendChild(d);
                });

                $mod = $('.modify-user');
                $mod.unbind('click');
                $mod.on('click', function() {
                    modifyUser(this.parentElement.dataset.id);
                });

                if(json.next_cursor) {
                    let e = document.createElement('div');

                    e.id = 'next-user-button';
                    e.innerHTML = "<a class='flow-text center block card-pointer'>Utilisateurs suivants</a>";
                    e.onclick = function () {
                        loadUsers(json.next_cursor);
                    };

                    placeholder.appendChild(e);
                }
            }
        },
        "text"
    ).fail((jx) => {
        Materialize.toast('Impossible de charger.', 6000);
        let preloader_icon = document.getElementById('preloader-user');

        if(preloader_icon) {
            $(preloader_icon).remove();
        }
    });
}

function applyFilter(filter) {
    $("#user-admin-placeholder div:not(:first-child)").remove();

    // Sélection de tous les éléments excepté le texte d'erreur et supprime
    loadUsers(0, filter);
}

$(document).ready(function() {
    loadUsers();

    $('#filter_users').on('input change paste', function() {
        let current_value = this.value;
        
        setTimeout(function() { 
            let new_value = document.getElementById('filter_users').value;

            if(new_value === current_value) {
                applyFilter(new_value);
            }
            else {
                // Do nothing
            }
        }, 350);
    });

    $('#modal-realname-button').on('click', function () {
        modifyAttibutes();
    });

    $('#modal-password-button').on('click', function () {
        modifyPassword();
    });

    $('#modal-confirmed-mail-button').on('click', function () {
        modifyConfirm();
    });

    $('#modal-status-button').on('click', function () {
        modifyStatus();
    });
});
