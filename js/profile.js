// Permet de savoir si le scrollfire est désactivé (1 si désactivé)
var disable_auto_scroll = Number(localStorage.getItem('disable_auto_scroll'));

$(document).ready(function () {
    let d = $.initialize('.dropdown-button', function() {
        $(this).off('click');

        $(this).dropdown({
            hover: true, // Activate on hover
            stopPropagation: true,
            constrainWidth: false,
        });

        $(this).on('click', function(event) {
            event.stopPropagation();
        });
    });

    let mixed = $.initialize('.dropdown-content', function() {
        $(this).off('click');

        $(this).on('click', function(event) {
            event.stopPropagation();
        });
    });

    let fol_active = $.initialize('.follow-activate', function() {
        $(this).off('change');

        $(this).on('change', function() {
            let id = this.dataset.userid;

            let checkBoxFollow = document.getElementById('followed'+id);
            let checkBoxMail = document.getElementById('followed_with_mail'+id);

            let realname = document.getElementById('follow_user_drop'+id).dataset.realname;

            let buttonFollow = document.getElementById('follow_user_button'+id);
            let bellFollow = document.getElementById('follow_user_bell'+id);

            let dic = {};
            let mode = "";

            if(checkBoxMail.checked && !checkBoxFollow.checked && $(this).hasClass('email-checkbox')){
                // Si on a appuyé sur la checkbox email (on l'active), et que la checkbox suivre est décochée
                checkBoxFollow.checked = true;
            }

            if(checkBoxFollow.checked){
                mode = "create";
                dic = {id: id, with_mail: checkBoxMail.checked};
            }
            else{
                mode = "destroy";
                dic = {id: id};
                checkBoxMail.checked = false;
            }

            $.post(
                https_url+"api/v2/friendships/"+ mode +".json",
                dic,
                function(data){
                    let json = JSON.parse(data);
                    if("following" in json){
                        let ac = json.following;
                        
                        if(ac){
                            let w_mail = json.follow_with_mail;
                            if(w_mail){
                                checkBoxMail.checked = true;
                                Materialize.toast('Notifications et mails activés pour '+ realname + '.', 6000, 'rounded');
                            }
                            else{
                                checkBoxMail.checked = false;
                                Materialize.toast('Notifications activées pour '+ realname + '.', 6000, 'rounded');
                            }
                            checkBoxFollow.checked = true;
                            checkBoxFollow.labels[0].innerHTML = 'Suivi';
                            $(buttonFollow).removeClass('grey');
                            $(buttonFollow).addClass('green');
                            bellFollow.innerHTML = 'notifications';
                        }
                        else{
                            checkBoxFollow.checked = false;
                            checkBoxMail.checked = false;
                            checkBoxFollow.labels[0].innerHTML = 'Suivre';
                            Materialize.toast('Notifications désactivées pour '+ realname + '.', 6000, 'rounded');
                            $(buttonFollow).addClass('grey');
                            $(buttonFollow).removeClass('green');
                            bellFollow.innerHTML = 'notifications_off';
                        }
                    }
                    else{
                        console.log("Erreur : JS " + data);
                        Materialize.toast('Impossible d\'effectuer l\'action.', 6000, 'rounded');
                    }
                },
                "text"
            ).fail(() => {
                Materialize.toast('Impossible d\'effectuer l\'action.', 6000, 'rounded');
            });
        });
    });
});

function loadCardsOfUsers(documentFragment, json) {
    let str = '';

    json.forEach(function (ele) {
        str += generateUserFlow(ele);
    });

    documentFragment.innerHTML += str;
}

function loadFollowers(documentFragment, id, since = 0) {
    let placeholder = documentFragment;

    let pre = document.createElement('div');
    pre.innerHTML = preloader;
    pre.id = 'preloader-user';

    placeholder.appendChild(pre);

    let button = $('#next-user-button');
    if(button){
        button.scrollfire('remove');
    }

    $.get(
        https_url+"api/v2/friendships/followers/lookup.json",
        {id: id, cursor: since, count: 6},
        (data) => {
            var json = JSON.parse(data);

            let next_button = document.getElementById('next-user-button');

            if(button) {
                button.remove();
            }

            let preloader_icon = document.getElementById('preloader-user');

            if(preloader_icon) {
                $(preloader_icon).remove();
            }

            if("followers" in json) {
                loadCardsOfUsers(documentFragment, json.followers);

                let $mod = $('.modify-user');
                $mod.unbind('click');
                $mod.on('click', function() {
                    modifyUser(this.parentElement.dataset.id);
                });

                if(json.next_cursor) {
                    let e = document.createElement('div');

                    e.id = 'next-user-button';
                    e.innerHTML = "<a class='flow-text center block card-pointer'>Utilisateurs suivants</a>";
                    e.classList.add('clearb');
                    e.style.marginTop = '10px';
                    e.onclick = function () {
                        loadFollowers(documentFragment, id, json.next_cursor);
                    };

                    placeholder.appendChild(e);

                    if(!disable_auto_scroll) {
                        $(`#next-user-button`).scrollfire({
                            // Offsets
                            offset: 0,
                            topOffset: 0,
                            bottomOffset: 0,
                            // Fires once when element begins to come in from the bottom
                            onBottomIn: function(elm, distance_scrolled) {
                                loadFollowers(documentFragment, id, json.next_cursor);
                            },
                        });
                    }
                }
            }
        },
        "text"
    ).fail((jx) => {
        Materialize.toast('Impossible de charger.', 6000);
        let preloader_icon = document.getElementById('preloader-user');

        if(preloader_icon) {
            $(preloader_icon).remove();
        }
    });
}

function loadFollowings(documentFragment, id, since = 0) {
    let placeholder = documentFragment;

    let pre = document.createElement('div');
    pre.innerHTML = preloader;
    pre.id = 'preloader-user';

    placeholder.appendChild(pre);

    let button = $('#next-user-button');
    if(button){
        button.scrollfire('remove');
    }

    $.get(
        https_url+"api/v2/friendships/followings/lookup.json",
        {id: id, cursor: since, count: 6},
        (data) => {
            var json = JSON.parse(data);

            if(button) {
                button.remove();
            }

            let preloader_icon = document.getElementById('preloader-user');

            if(preloader_icon) {
                $(preloader_icon).remove();
            }

            if("followings" in json) {
                loadCardsOfUsers(documentFragment, json.followings);

                let $mod = $('.modify-user');
                $mod.unbind('click');
                $mod.on('click', function() {
                    modifyUser(this.parentElement.dataset.id);
                });

                if(json.next_cursor) {
                    let e = document.createElement('div');

                    e.id = 'next-user-button';
                    e.classList.add('clearb');
                    e.style.marginTop = '10px';
                    e.innerHTML = "<a class='flow-text center block card-pointer'>Utilisateurs suivants</a>";
                    e.onclick = function () {
                        loadFollowings(documentFragment, id, json.next_cursor);
                    };

                    placeholder.appendChild(e);

                    if(!disable_auto_scroll) {
                        $(`#next-user-button`).scrollfire({
                            // Offsets
                            offset: 0,
                            topOffset: 0,
                            bottomOffset: 0,
                            // Fires once when element begins to come in from the bottom
                            onBottomIn: function(elm, distance_scrolled) {
                                loadFollowings(documentFragment, id, json.next_cursor);
                            },
                        });
                    }
                }
            }
        },
        "text"
    ).fail((jx) => {
        Materialize.toast('Impossible de charger.', 6000);
        let preloader_icon = document.getElementById('preloader-user');

        if(preloader_icon) {
            $(preloader_icon).remove();
        }
    });
}
