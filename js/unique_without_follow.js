$(document).ready(function () {
    $("#selecttype").on('change', function () {
        if(sort_type != this.value){
            sort_type = this.value;
            initComment(this.dataset.sub);
        }
    });

    $("#selectsort").on('change', function () {
        if(sorting_comments != this.value){
            sorting_comments = this.value;
            initComment(this.dataset.sub);
        }
    });
});
