<?php
// --------------------------------------------------------
// FICHIER D'EXECUTION PRINCIPAL : TOUT DOIT PASSER PAR ICI
// --------------------------------------------------------

ini_set('display_errors','on');
error_reporting(E_ALL);

// Charge les constantes (obligatoire)
require($_SERVER['DOCUMENT_ROOT'] . '/inc/constantes.php'); // Fichier de constantes BDD et Twitter

// Initialise le cookie de session pour tous les sous-domaines
$currentCookieParams = session_get_cookie_params(); 

session_set_cookie_params( 
    $currentCookieParams["lifetime"], 
    $currentCookieParams["path"], 
    DOT_DOMAIN, 
    $currentCookieParams["secure"], 
    $currentCookieParams["httponly"] 
); 

session_start();

$GLOBALS['is_included'] = true;
$GLOBALS['black'] = false;

try {
    // Charge le minimum nécessaire
    require(ROOT . '/inc/fonctions.php');
    require(ROOT . '/inc/Route.php');

    connectBD();

    loadSharedSettings();

    if((isset($_GET['fetch_503']) && isActive($_GET['fetch_503'])) || (!$GLOBALS['settings']['allowWebsiteAccess'] && !isAdmin())) {
        header($_SERVER['SERVER_PROTOCOL'] . ' 503 Service Unavailable');
        require(ROOT . '/reserved/not_available.html');
        exit();
    }

    if(isset($_GET['kill_page']) && isActive($_GET['kill_page'])) {
        throw new Exception("Server error asked by user", 102);
    }

    // Charge tout le reste
    require(ROOT . '/inc/traitements.php');
    require(ROOT . '/inc/generated.php');
    require(ROOT . '/inc/NavBar.php');
    include(ROOT . '/static/pagesreferences.php');

    $connect = checkConnect();

    activeDesactiveNightMode();

    if(isset($_SESSION['night_mode'])){
        $GLOBALS['black'] = $_SESSION['night_mode'];
    }
    else{
        $_SESSION['night_mode'] = false;
    }

    if(isset($_GET['logout']) && $_GET['logout'] == 1){
        disconnect();
    }

    // Récupération de la page à afficher
    $nomPage = 'php/journal.php'; // page par défaut
    $titrePage = 'Le Journal des Shitstorms'; // titre par défaut
    $GLOBALS['page'] = 'journal';
    $GLOBALS['additionnals'] = null; // "/$currentPage/'options'/'additionnals'/'url_params'
    $GLOBALS['options'] = null;
    $GLOBALS['url_params'] = null;

    if(isset($_GET['page']) && is_string($_GET['page']) && $_GET['page'] != '') { // verification du parametre "page"
        if(file_exists(addslashes('php/'.$_GET['page']))) // le fichier existe
            $nomPage = addslashes(ROOT . '/php/'.$_GET['page']);
    }
    else if(defined('PAGES_REFERENCES') && defined('PAGES_NAME')){
        if(isset($_SERVER['REDIRECT_URL'])){
            $route = new Route('', 'journal');

            $GLOBALS['current_page'] = $route->getCompleteParameters();
            $currentPage = $route->getPage();
            $GLOBALS['options'] = $route->getMainUrlOption();
            $GLOBALS['additionnals'] = $route->getAdditionalUrlOption();
            $GLOBALS['url_params'] = $route->getDeepUrlOption();
        }
        else{ // Charge la page par défaut
            $currentPage = 'journal';
        }
        
        if($currentPage != '' && $currentPage != 'index.php'){
            $GLOBALS['page'] = $currentPage;

            if(array_key_exists($currentPage, PAGES_REFERENCES)){
                if(file_exists(addslashes('php/'.PAGES_REFERENCES[$currentPage]))) // le fichier existe
                    $nomPage = addslashes('php/'.PAGES_REFERENCES[$currentPage]);
            }
            else{
                $nomPage = addslashes('php/'.PAGES_REFERENCES['404']);
            }

            if(array_key_exists($currentPage, PAGES_NAME)){
                $titrePage = PAGES_NAME[$currentPage] . ' - Journal des Shitstorms';
            }
        }
    }

    ob_start();

    $GLOBALS['navbar'] = new NavBar();

    require($nomPage); // Enregistre le contenu de la page incluse dans le buffer

    $actualPage = ob_get_clean();

    ?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <meta name="connected-user" id="connected-user" value="<?= (isset($_SESSION['username']) ? $_SESSION['username'] : '') ?>">

        <!-- Chrome / Firefox OS color adressbar -->
        <meta name="theme-color" content="<?= ($GLOBALS['black'] ? '#03064B' : '#00796b') ?>"/>

        <!-- Safari for iOS adressbar -->
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

        <!-- Twitter meta tags -->
        <meta name="twitter:card" content="summary"/>
        <meta name="twitter:site" content="@j_shitstorm"/>
        <meta name="twitter:creator" content="@alkihis"/>
        <?php
            if(isset($GLOBALS['addMeta'])){ // Si on définit du meta perso
                echo $GLOBALS['addMeta'];
            }
            else { ?>
                <meta name="twitter:title" content="Journal des Shitstorms"/>
                <meta name="twitter:description" content="Référence les shitstorms de Twitter depuis janvier 2018."/>
                <meta name="twitter:image" content="<?= DEFAULT_CARD_IMG ?>"/>
            <?php } 
        ?>
        <title><?= (isset($GLOBALS['pageTitle']) ? $GLOBALS['pageTitle'] : htmlentities($titrePage)) ?></title>

        <?php if(isset($GLOBALS['add_meta_description'])) { ?>
            <!-- Description -->
            <meta name='description' content='<?= htmlspecialchars($GLOBALS['add_meta_description'], ENT_QUOTES) ?>'>
        <?php } ?>

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="icon" type="image/png" href="<?= HTTPS_URL ?>img/favicon.png" />

        <link rel="stylesheet" href="<?= HTTPS_URL ?>css/normalize.css">
        <link rel="stylesheet" href="<?= HTTPS_URL ?>css/materialize.min.css">

        <link href="<?= HTTPS_URL ?>css/stylesJ.css" rel="stylesheet" media="all" type="text/css">
        <?= ($GLOBALS['black'] ? '<link href="' . HTTPS_URL . 'css/dark.css" id="dark_mode_css" rel="stylesheet" media="all" type="text/css">' : '') ?>
        <script src="<?= HTTPS_URL ?>js/auto-init/jquery-3.3.1.min.js"></script>

        <link rel="stylesheet" href="<?= HTTPS_URL ?>css/jquery.fancybox.min.css">
        
        <script src="<?= HTTPS_URL ?>js/auto-init/jquery.fancybox.min.js"></script>
        <script src="<?= HTTPS_URL ?>js/auto-init/jquery.scrollfire.min.js"></script>
        <script src="<?= HTTPS_URL ?>js/auto-init/modernizr-webp.js"></script>
        <script src="<?= HTTPS_URL ?>js/auto-init/jquery.initialize.min.js"></script>

        <script src="<?= HTTPS_URL ?>js/auto-init/materialize.min.js"></script>
        <script src="<?= HTTPS_URL ?>js/auto-init/const.js"></script>
        <script src="<?= HTTPS_URL ?>js/auto-init/master.js"></script>
        <script src="<?= HTTPS_URL ?>js/auto-init/modals.js" async></script>
        <script src="<?= HTTPS_URL ?>js/auto-init/notifs.js" async></script>
    </head>

    <body>
        <header>
            <?php if(!isset($GLOBALS['disable_navigation_bar']) || $GLOBALS['disable_navigation_bar'] !== true) {
                require(STATIC_DIR . 'menu.php');

                echo $GLOBALS['nav_tab'] ?? '';
            } ?>
        </header>

        <main id='main-block' class='<?= (isset($_SESSION['snow']) && $_SESSION['snow'] ? 'snow-main ' : '') . (!isset($GLOBALS['turn_off_background']) ? "" : 'no-background') ?>'>
            
            <div id='clear-margin' class='clearb' style='margin-bottom: <?= (isContainerActive()  ? "2" : '') ?>0px;'></div>
            <div id='main-container' class="<?= (isContainerActive() ? "container" : '') ?>">
                <?= $actualPage ?>
            </div>
        </main>

        <!-- Modaux -->
        <div class='row' id='main-modal-placeholder' style='margin-bottom: 0;'>
            <div id="modalplaceholder" class="modal">
            </div>
            <div id="modalphfixed" class="modal modal-fixed-footer">
            </div>
            <div id="modalphbottom" class="modal bottom-sheet">
            </div>
        </div>

        <footer id='footer' class="page-footer footer-color">
            <?php require(STATIC_DIR . 'footer.php'); ?>
        </footer>
        <!-- <span>Temps total de génération : <?= microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"] ?> secondes.</span> -->
        <!-- <span>Page demandée : <?= $_SERVER["REQUEST_URI"] ?></span> -->
            
        <!-- Script pour l'initialisation des commentaires -->
        <script src="<?= HTTPS_URL ?>js/comment.js"></script>
    </body>
    </html>
    <?php
    deconnectBD();
} catch (Throwable $e) { // Catche les exceptions et les erreurs
    // Vide le tampon si jamais
    @ob_end_clean();

    $GLOBALS['exception_catched'] = ['text' => $e->getMessage(),
                                     'file' => $e->getFile(),
                                     'code' => $e->getCode(),
                                     'line' => $e->getLine(),
                                     'trace' => $e->getTraceAsString()];
    // Logging exception
    if(!defined('ENABLE_LOG') || ENABLE_LOG)
        file_put_contents('/var/www/logFiles/' . date('Y-m-d') . '.log', 
                      '---- ' . date('H:i:s') . " ----\n" . json_encode($GLOBALS['exception_catched']) . "\n", 
                      FILE_APPEND);

    header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error');
    require(ROOT . '/reserved/internal_error.php');
    exit();
}
